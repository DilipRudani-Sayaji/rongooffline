package com.rongoapp.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.rongoapp.R;
import com.rongoapp.activities.MainActivity;
import com.rongoapp.controller.Constants;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            Map<String, String> data = remoteMessage.getData();
            String title = data.get("title");
            String message = data.get("message");
            String id = data.get("id");
            String type = data.get("type");

            createNotification(message, title, id, type);
        }

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }

    @Override
    public void onNewToken(String token) {
        Log.d("Refreshed token:", "Refreshed token: " + token);
        SharedPreferences sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.SharedPreferences_FCM_TOKEN, token);
        editor.commit();
    }

    public void createNotification(String aMessage, String title, String id_type, String type) {
        //final int NOTIFY_ID = (int)(Math.random()*9000)+1000;
        final int NOTIFY_ID = 1;

        NotificationManager notifManager = null;

        // There are hardcoding only for show it's just strings
        String name = title;
        String id = title; // The user-visible name of the channel.
        String description = aMessage; // The user-visible description of the channel.
        Log.v("asdasdasdsd", title + " : " + description + " : " + id_type);

        Intent intent = null;
        PendingIntent pendingIntent;
        NotificationCompat.Builder builder = null;

        if (notifManager == null) {
            notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }

        intent = new Intent(this, MainActivity.class);
        intent.putExtra("type", type);
        intent.putExtra("id", id_type);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = notifManager.getNotificationChannel(id);
            if (mChannel == null) {
                mChannel = new NotificationChannel(id, name, importance);
                mChannel.setDescription(description);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

                notifManager.createNotificationChannel(mChannel);
            }
            builder = new NotificationCompat.Builder(this, id);

//            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            builder.setContentTitle(title)  // required
                    .setSmallIcon(getNotificationIcon())
                    //   .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))// required
                    .setContentText(aMessage)  // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setTicker(aMessage)
                    .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        } else {
            builder = new NotificationCompat.Builder(this);

            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            builder.setContentTitle(title)                           // required
                    .setSmallIcon(getNotificationIcon()) // required
                    .setContentText(aMessage)  // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setTicker(aMessage)
                    .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                    .setPriority(Notification.PRIORITY_HIGH);
        } // else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {


        builder.setColor(getResources().getColor(R.color.colorPrimary));

        Notification notification = builder.build();
//        Notification notification = new Notification.InboxStyle(builder)
//                .build();

        notifManager.notify(NOTIFY_ID, notification);
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_notif_trans : R.mipmap.ic_launcher;
    }

}
