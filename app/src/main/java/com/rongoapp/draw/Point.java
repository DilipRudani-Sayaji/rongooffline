package com.rongoapp.draw;

public class Point {

    public float x;
    public float y;

    @Override
    public String toString() {
        return x + ", " + y;
    }

}