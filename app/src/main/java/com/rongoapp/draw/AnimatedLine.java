package com.rongoapp.draw;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PointF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import com.rongoapp.R;

import java.util.ArrayList;

public class AnimatedLine extends View {

    private final Paint mPaint;
    public Canvas mCanvas;
    AnimationListener animationListener;

    Path path;
    private static long animSpeedInMs = 2000;
    private static final long animMsBetweenStrokes = 100;
    private long animLastUpdate;
    private boolean animRunning = true;
    private int animCurrentCountour;
    private float animCurrentPos;
    private Path animPath;
    private PathMeasure animPathMeasure;

    float pathLength;
    float distance = 0;
    float[] pos;
    float[] tan;
    Matrix matrix;
    Bitmap bm;

    public AnimatedLine(Context context) {
        this(context, null);
        mCanvas = new Canvas();
    }

    public AnimatedLine(Context context, AttributeSet attrs) {
        super(context, attrs);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(15);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setColor(context.getResources().getColor(R.color.color_red));

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            setLayerType(LAYER_TYPE_SOFTWARE, mPaint);
        }
        bm = BitmapFactory.decodeResource(getResources(), R.drawable.placeholderbg);
        bm = Bitmap.createScaledBitmap(bm, 20, 20, false);
        distance = 0;
        pos = new float[2];
        tan = new float[2];

        matrix = new Matrix();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mCanvas = canvas;
        if (path != null) {
            if (animRunning) {
                drawAnimation(mCanvas);
            } else {
                drawStatic(mCanvas);
            }
        }
    }

    /**
     * draw Path With Animation
     *
     * @param time in milliseconds
     */
    public void drawWithAnimation(ArrayList<PointF> points, long time, AnimationListener animationListener) {
        animRunning = true;
        animPathMeasure = null;
        animSpeedInMs = time;
        setPath(points);
        setAnimationListener(animationListener);
        invalidate();
    }

    public void setPath(ArrayList<PointF> points) {
        if (points.size() < 2) {
            throw new IllegalStateException("Pass atleast two points.");
        }
        path = new Path();
        path.moveTo(points.get(0).x, points.get(0).y);
        path.lineTo(points.get(1).x, points.get(1).y);
    }

    private void drawAnimation(Canvas canvas) {
        if (animPathMeasure == null) {
            // Start of animation. Set it up.
            animationListener.onAnimationStarted();
            animPathMeasure = new PathMeasure(path, false);
            animPathMeasure.nextContour();
            animPath = new Path();
            animLastUpdate = System.currentTimeMillis();
            animCurrentCountour = 0;
            animCurrentPos = 0.0f;

            pathLength = animPathMeasure.getLength();
        } else {
            // Get time since last frame
            long now = System.currentTimeMillis();
            long timeSinceLast = now - animLastUpdate;

            if (animCurrentPos == 0.0f) {
                timeSinceLast -= animMsBetweenStrokes;
            }

            if (timeSinceLast > 0) {
                // Get next segment of path
                float newPos = (float) (timeSinceLast) / (animSpeedInMs / pathLength) + animCurrentPos;
                boolean moveTo = (animCurrentPos == 0.0f);
                animPathMeasure.getSegment(animCurrentPos, newPos, animPath, moveTo);
                animCurrentPos = newPos;
                animLastUpdate = now;

                //start draw bitmap along path
                animPathMeasure.getPosTan(newPos, pos, tan);
                matrix.reset();
                matrix.postTranslate(pos[0], pos[1]);
                canvas.drawBitmap(bm, matrix, null);
                //end drawing bitmap

                //take current position
                animationListener.onAnimationUpdate(pos);

                // If this stroke is done, move on to next
                if (newPos > pathLength) {
                    animCurrentPos = 0.0f;
                    animCurrentCountour++;
                    boolean more = animPathMeasure.nextContour();
                    // Check if finished
                    if (!more) {
                        animationListener.onAnimationEnd();
                        animRunning = false;
                    }
                }
            }

            // Draw path
            canvas.drawPath(animPath, mPaint);
        }

        invalidate();
    }

    private void drawStatic(Canvas canvas) {
        canvas.drawPath(path, mPaint);
        canvas.drawBitmap(bm, matrix, null);
    }

    public void setAnimationListener(AnimationListener animationListener) {
        this.animationListener = animationListener;
    }

    public interface AnimationListener {
        void onAnimationStarted();

        void onAnimationEnd();

        void onAnimationUpdate(float[] pos);
    }

}