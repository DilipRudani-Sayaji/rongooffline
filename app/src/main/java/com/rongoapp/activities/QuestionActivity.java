package com.rongoapp.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rongoapp.R;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.LocaleManager;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.DailyQuizStatusData;
import com.rongoapp.data.OfflineQuizData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuestionActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    int isApiCount = 1;
    boolean isClick = false;

    TextView txtQuestion, tvOne, tvTwo, tvThree, tvFour, tvAdviceTxt;
    LinearLayout llSeeAdvisory;
    ImageView ivBack;
    Button btnNext;
    String isPosition = "", isAnswer = "", isAnswerValue = "", QuizId = "";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(this);
        Functions.setFirebaseLogEventTrack(QuestionActivity.this, Constants.Screen_Track, "Daily Quiz", "", "");

        setInitView();
        setListner();
        setDailyQuestion();
    }

    private void setInitView() {
        ivBack = (ImageView) findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        txtQuestion = (TextView) findViewById(R.id.txtQuestion);
        tvOne = (TextView) findViewById(R.id.tvOne);
        tvTwo = (TextView) findViewById(R.id.tvTwo);
        tvThree = (TextView) findViewById(R.id.tvThree);
        tvFour = (TextView) findViewById(R.id.tvFour);

        tvOne.setBackground(getResources().getDrawable(R.drawable.rounded_white_border));
        tvTwo.setBackground(getResources().getDrawable(R.drawable.rounded_white_border));
        tvThree.setBackground(getResources().getDrawable(R.drawable.rounded_white_border));
        tvFour.setBackground(getResources().getDrawable(R.drawable.rounded_white_border));

        tvAdviceTxt = (TextView) findViewById(R.id.tvAdviceTxt);

        btnNext = (Button) findViewById(R.id.btnNext);

        int cropWeek = RongoApp.getCropWeekDaysFunction();
        if (cropWeek > 0 && cropWeek < Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"))) {
            List<DailyQuizStatusData> dailyQuizStatus = appDatabase.dailyQuizStatusDao().getDailyQuizStatus(
                    sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0"),
                    sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, "0"),
                    sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, "0"));
            if (dailyQuizStatus != null && dailyQuizStatus.size() >= 1) {
//                cardAajKaQuestion.setVisibility(View.GONE);
                isClick = false;
                btnNext.setAlpha(0.2f);
                btnNext.setEnabled(false);
                tvAdviceTxt.setText(R.string.str_quiz_redirect_msg);
            } else {
//                cardAajKaQuestion.setVisibility(View.VISIBLE);
                isClick = true;
                btnNext.setAlpha(1f);
                btnNext.setEnabled(true);
                tvAdviceTxt.setText(R.string.str_ques_answer_is_salah_me_he);
            }
        } else {
            finish();
        }
    }

    private void setListner() {
        tvOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPosition = "0";
                setSelection();
            }
        });

        tvTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPosition = "1";
                setSelection();
            }
        });

        tvThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPosition = "2";
                setSelection();
            }
        });

        tvFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isPosition = "3";
                setSelection();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(isPosition)) {
                    if (isClick) {
                        showDialog(isPosition);
                    }
                } else {
                    Functions.showSnackBar(findViewById(android.R.id.content), getString(R.string.str_ques_answer_de));
                }
            }
        });

        llSeeAdvisory = (LinearLayout) findViewById(R.id.llSeeAdvisory);
        llSeeAdvisory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private void setDailyQuestion() {
        try {
            RongoApp.getCropWeekDaysFunction();
            String quizData = sharedPreferences.getString(Constants.SharedPreferences_Quiz, "");
            if (!TextUtils.isEmpty(quizData)) {
                //  Parse Data
//                JSONArray jsonArray = new JSONArray(quizData);
//                Log.e("+++++++++", "" + quizData);
                JSONObject QuizData = new JSONObject(quizData);
                if (QuizData != null && QuizData.length() > 0) {
                    parseQuizDataBothSide(QuizData);
                }
            } else {
                //  Call API
                if (isApiCount > 1) {
                    return;
                }
                getQuizData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parseQuizDataBothSide(JSONObject jsonObj) {
        try {
            QuizId = jsonObj.getString("id");
            JSONObject json_data = jsonObj.getJSONObject("json_data");
            if (json_data != null && json_data.length() > 0) {
                Iterator<String> keys = json_data.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    String value = json_data.optString(key);
                    if (key.equalsIgnoreCase(RongoApp.getSelectedCrop())) {
                        JSONObject jsonLang = new JSONObject(value);
                        if (jsonLang != null && jsonLang.length() > 0) {
                            Iterator<String> keyLang = jsonLang.keys();
                            while (keyLang.hasNext()) {
                                String key1 = keyLang.next();
                                String value1 = jsonLang.optString(key1);
//                                Log.e("Jsonparsing++", "key= " + key1 + "\n Value=" + value1);
                                if (key1.equalsIgnoreCase(RongoApp.getSeasonName())) {
//                                    Log.e("+++++++", "key= " + key1 +" - "+RongoApp.getSeasonName());
                                    JSONObject jsonLang1 = new JSONObject(value1);
                                    if (jsonLang1 != null && jsonLang1.length() > 0) {
                                        Iterator<String> keyLang1 = jsonLang1.keys();
                                        while (keyLang1.hasNext()) {
                                            String key2 = keyLang1.next();
                                            String value2 = jsonLang1.optString(key2);
//                                            Log.e("Jsonparsing+1+", "key= " + key2 + "\n Value=" + value2);
                                            if (key2.equalsIgnoreCase(RongoApp.getDefaultLanguage())) {
                                                JSONArray jsonArray = new JSONArray(value2);
//                                                Log.e("parseQuizData1", "" + jsonArray);
                                                JSONObject resultData = jsonArray.getJSONObject(Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "2")) - 1);
                                                JSONObject jsonObject = resultData.getJSONObject(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, "0"));
                                                isAnswer = jsonObject.getString("answer");
                                                txtQuestion.setText("" + jsonObject.getString("question"));

                                                JSONArray array = jsonObject.optJSONArray("options");
                                                List<String> list = new Gson().fromJson(array.toString(), new TypeToken<List<String>>() {
                                                }.getType());

                                                tvOne.setText(" (A)  " + list.get(0));
                                                tvTwo.setText(" (B)  " + list.get(1));
                                                tvThree.setText(" (C)  " + list.get(2));
                                                tvFour.setText(" (D)  " + list.get(3));

                                                isAnswerValue = list.get(Integer.parseInt(isAnswer));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setSelection() {
        try {
            if (isClick) {
                showDialog(isPosition);
            }

            tvOne.setEnabled(false);
            tvTwo.setEnabled(false);
            tvThree.setEnabled(false);
            tvFour.setEnabled(false);

            tvOne.setBackground(getResources().getDrawable(R.drawable.rounded_white_border));
            tvTwo.setBackground(getResources().getDrawable(R.drawable.rounded_white_border));
            tvThree.setBackground(getResources().getDrawable(R.drawable.rounded_white_border));
            tvFour.setBackground(getResources().getDrawable(R.drawable.rounded_white_border));

            if (isAnswer.equalsIgnoreCase(isPosition)) {
                if (isPosition.equalsIgnoreCase("0")) {
                    tvOne.setBackground(getResources().getDrawable(R.drawable.rounded_green_light_border));
                } else if (isPosition.equalsIgnoreCase("1")) {
                    tvTwo.setBackground(getResources().getDrawable(R.drawable.rounded_green_light_border));
                } else if (isPosition.equalsIgnoreCase("2")) {
                    tvThree.setBackground(getResources().getDrawable(R.drawable.rounded_green_light_border));
                } else if (isPosition.equalsIgnoreCase("3")) {
                    tvFour.setBackground(getResources().getDrawable(R.drawable.rounded_green_light_border));
                }
            } else {
                if (isAnswer.equalsIgnoreCase("0")) {
                    tvOne.setBackground(getResources().getDrawable(R.drawable.rounded_green_light_border));
                } else if (isAnswer.equalsIgnoreCase("1")) {
                    tvTwo.setBackground(getResources().getDrawable(R.drawable.rounded_green_light_border));
                } else if (isAnswer.equalsIgnoreCase("2")) {
                    tvThree.setBackground(getResources().getDrawable(R.drawable.rounded_green_light_border));
                } else if (isAnswer.equalsIgnoreCase("3")) {
                    tvFour.setBackground(getResources().getDrawable(R.drawable.rounded_green_light_border));
                }

                if (isPosition.equalsIgnoreCase("0")) {
                    tvOne.setBackground(getResources().getDrawable(R.drawable.rounded_red_light_border));
                } else if (isPosition.equalsIgnoreCase("1")) {
                    tvTwo.setBackground(getResources().getDrawable(R.drawable.rounded_red_light_border));
                } else if (isPosition.equalsIgnoreCase("2")) {
                    tvThree.setBackground(getResources().getDrawable(R.drawable.rounded_red_light_border));
                } else if (isPosition.equalsIgnoreCase("3")) {
                    tvFour.setBackground(getResources().getDrawable(R.drawable.rounded_red_light_border));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showDialog(final String value) {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        View sheetView = this.getLayoutInflater().inflate(R.layout.question_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        AppCompatTextView txtOfflineSMS = sheetView.findViewById(R.id.txtOfflineSMS);
        if (InternetConnection.checkConnectionForFasl(QuestionActivity.this)) {
            txtOfflineSMS.setVisibility(View.GONE);
        } else {
            txtOfflineSMS.setVisibility(View.VISIBLE);
        }

        TextView tvRewardText = (TextView) sheetView.findViewById(R.id.tvRewardText);
        tvRewardText.setText(RewardPoints.getDailyQuizRewardPoint(sharedPreferences) + " " + getResources().getString(R.string.str_get_more_reward));

        ImageView ivTagImage = (ImageView) sheetView.findViewById(R.id.ivTagImage);
        TextView tvTagText = (TextView) sheetView.findViewById(R.id.tvTagText);
        TextView tvTagDecs = (TextView) sheetView.findViewById(R.id.tvTagDecs);
        TextView tvRewardPoint = (TextView) sheetView.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText(RewardPoints.getDailyQuizRewardPoint(sharedPreferences));

        Button btnReward = (Button) sheetView.findViewById(R.id.btnReward);
        LinearLayout llSalahView = (LinearLayout) sheetView.findViewById(R.id.llSalahView);
        LinearLayout llRewardView = (LinearLayout) sheetView.findViewById(R.id.llRewardView);

        if (value.equalsIgnoreCase(isAnswer)) {
            llRewardView.setVisibility(View.VISIBLE);
            llSalahView.setVisibility(View.GONE);
            btnReward.setVisibility(View.VISIBLE);
            btnReward.setText(R.string.str_apne_rewad_points_dekhe);

            ivTagImage.setImageResource(R.mipmap.green_yes);
            tvTagText.setText(R.string.str_aapka_utar_sahi_he);
            tvTagText.setTextColor(getResources().getColor(R.color.GREEN));

            tvTagDecs.setVisibility(View.GONE);
            tvTagDecs.setText(R.string.str_aaj_10_reward_jite_he);

            Functions.updateRewardPoints(sharedPreferences, RewardPoints.getDailyQuizRewardPoint(sharedPreferences));

            updateQuizResult("1");
        } else {
            llRewardView.setVisibility(View.GONE);
            llSalahView.setVisibility(View.VISIBLE);
            btnReward.setVisibility(View.GONE);
            btnReward.setText(R.string.str_salah_dekhe);

            ivTagImage.setImageResource(R.mipmap.red_no);

            tvTagText.setText(R.string.str_utar_galt_he);
            tvTagText.setTextColor(getResources().getColor(R.color.color_red));

            tvTagDecs.setVisibility(View.VISIBLE);
            tvTagDecs.setText(R.string.str_jankari_ke_liye_salah_pade);

            updateQuizResult("0");
        }

        llSalahView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                finish();
            }
        });

        btnReward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
                if (value.equalsIgnoreCase(isAnswer)) {
                    Intent i = new Intent(QuestionActivity.this, RewardActivity.class);
                    startActivity(i);
                }
                finish();
            }
        });

        ImageView ivClose = (ImageView) sheetView.findViewById(R.id.ivClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
                finish();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        ((View) sheetView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();
    }


    void getQuizData() {
        isApiCount++;
        RetrofitClient.getInstance().getApi().getAllDailyQuiz(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                SharedPreferencesUtils.getUserId(QuestionActivity.this)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String r = response.body().string();
                        if (!TextUtils.isEmpty(r)) {
                            JSONObject jsonObject = new JSONObject(r);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject data = jsonObject.optJSONObject("data");
                                    if (data != null && data.length() > 0) {
                                        JSONObject jsonArray = data.optJSONObject("result");
                                        if (jsonArray != null && jsonArray.length() > 0) {
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Quiz, "" + jsonArray);
                                        }
                                        setDailyQuestion();
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void updateQuizResult(final String status) {
        try {
            Functions.setFirebaseLogEventTrack(QuestionActivity.this, Constants.Click_Event, "Daily Quiz", status, "updateQuizResult");

            if (InternetConnection.checkConnectionForFasl(QuestionActivity.this)) {
                RetrofitClient.getInstance().getApi().updateDailyQuizResponse(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                        SharedPreferencesUtils.getUserId(QuestionActivity.this), Functions.getDeviceid(QuestionActivity.this),
                        QuizId, sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                        sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                        sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0"),
                        sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, "0"),
                        isAnswerValue, status, getDailyQuizRewardPoint(status)).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response != null && response.isSuccessful()) {
                            setDailyQuizStatus(status);
//                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Question_DATE, Functions.getCurrentDate());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
            } else {
                setDailyQuizStatus(status);
                ArrayList<OfflineQuizData> offlineQuizData = new ArrayList<OfflineQuizData>();
                OfflineQuizData offlineQuiz = new OfflineQuizData();
                offlineQuiz.set_id(0);
                offlineQuiz.setQuizID(QuizId);
                offlineQuiz.setCropId(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""));
                offlineQuiz.setPcropId(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""));
                offlineQuiz.setCropWeek(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""));
                offlineQuiz.setCropDay(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, ""));
                offlineQuiz.setAnsValue(isAnswerValue);
                offlineQuiz.setIs_true(status);
                offlineQuiz.setMetaData(getDailyQuizRewardPoint(status));
                offlineQuizData.add(offlineQuiz);
                appDatabase.offlineQuizDao().insertAll(offlineQuizData);
//                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Question_DATE, Functions.getCurrentDate());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setDailyQuizStatus(String status) {
        try {
            ArrayList<DailyQuizStatusData> dailyQuizStatusData = new ArrayList<DailyQuizStatusData>();
            DailyQuizStatusData dailyQuizStatus = new DailyQuizStatusData();
            dailyQuizStatus.set_id(0);
            dailyQuizStatus.setQuizID(QuizId);
            dailyQuizStatus.setCropId(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""));
            dailyQuizStatus.setPcropId(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""));
            dailyQuizStatus.setCropWeek(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""));
            dailyQuizStatus.setCropDay(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, ""));
            dailyQuizStatus.setAnsValue(isAnswerValue);
            dailyQuizStatus.setMetaData(getDailyQuizRewardPoint(status));
            dailyQuizStatusData.add(dailyQuizStatus);
            appDatabase.dailyQuizStatusDao().insertAll(dailyQuizStatusData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getDailyQuizRewardPoint(String status) {
        String rewardPoint = "";
        try {
            if (status.equalsIgnoreCase("1")) {
                JSONArray jsonArray = new JSONArray();
                JSONObject obj = new JSONObject();
                obj.put("type", "point");
                obj.put("value", RewardPoints.getDailyQuizRewardPoint(sharedPreferences));
                jsonArray.put(obj);
                rewardPoint = jsonArray.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rewardPoint;
    }

}