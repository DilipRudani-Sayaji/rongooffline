package com.rongoapp.activities;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.rongoapp.controller.Constants.CAMERA_REQUEST_CODE;
import static com.rongoapp.controller.Constants.GALLERY_REQUEST_CODE;
import static com.rongoapp.controller.Constants.PERMISSION_READ_STORAGE;
import static com.rongoapp.controller.Constants.providerAuthority;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.rongoapp.R;
import com.rongoapp.adapter.CropAreaAdapter;
import com.rongoapp.adapter.CropProblemAdapter;
import com.rongoapp.adapter.ThumbnailsAdapter;
import com.rongoapp.controller.AudioPlayerTask;
import com.rongoapp.controller.AudioRecorderCallback;
import com.rongoapp.controller.AudioRecorderDialog;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.GlideApp;
import com.rongoapp.controller.InitInterface;
import com.rongoapp.controller.LocaleManager;
import com.rongoapp.controller.PermissionUtils;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.CropAreaFocusedData;
import com.rongoapp.data.InsertHomeFeedData;
import com.rongoapp.data.MasterCropData;
import com.rongoapp.data.OfflineData;
import com.rongoapp.data.PostData;
import com.rongoapp.data.ProblemRelatedData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.imagecompressor.ImageZipper;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.GridSpacingItemDecoration;
import com.rongoapp.views.Log;
import com.rongoapp.views.ProgressBarDialog;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostQuestionActivity extends AppCompatActivity implements InitInterface {

    AppDatabase appDatabase;
    SharedPreferences sharedPreferences;
    long mLastLongClick = 0;
    ProgressBarDialog progressBarDialog;

    FusedLocationProviderClient fusedLocationClient;
    SettingsClient mSettingsClient;
    LocationSettingsRequest mLocationSettingsRequest;
    LocationManager locationManager;
    double latitude = 0.0, longitude = 0.0;

    AppCompatEditText editTextDescription;
    AppCompatTextView txtDarjKare, txtUpload, txtDescription, txtUploadMore;
    TextView mFileNameTextView;
    ImageView imgBack, mPlayButton, imgGuideline, imgNext;

    ArrayList<String> imagesPath = new ArrayList<>();
    List<CropAreaFocusedData> cropAreaFocusedData = new ArrayList<>();
    CropAreaFocusedData mSelectedList;

    RecyclerView rvFasal, rcvProblem, rvAllPhotos, rcvCrop;
    SelectCropAdapter cropAdapter;
    CropProblemAdapter cropProblemAdapter;
    CropAreaAdapter cropAreaAdapter;
    ThumbnailsAdapter thumbnailsAdapter;

    CardView mediaPlayer;
    LinearLayout llVoiceRecordView, llPhotoView, llMorePhotoView, llCropAreaView;

    String filePath, mCurrentPhotoPath = "", problem_related = "1", keyword_id = "", postType = "1";

    AudioPlayerTask audioPlayerTask;
    AudioRecorderDialog audioRecorderDialog;
    AudioRecorderCallback audioRecorderCallback;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    BroadcastReceiver AUDIO_RECORD_RECEIVER = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent != null) {
                    String fileName = intent.getStringExtra("mFileName");
                    filePath = intent.getStringExtra("mFilePath");

                    llVoiceRecordView.setVisibility(View.GONE);
                    mediaPlayer.setVisibility(View.VISIBLE);
                    audioPlayerTask.onPlayMedia(filePath);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_problem_question);

        registerReceiver(AUDIO_RECORD_RECEIVER, new IntentFilter(Constants.BROADCAST_RECEIVER.AUDIO_RECORD_RECEIVER));
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(this);
        RongoApp.logVisitedPageTabEvent("PostQuestion");
        Functions.setUserScreenTrackEvent(appDatabase, "PostQuestion", "" + keyword_id);
        Functions.setFirebaseLogEventTrack(PostQuestionActivity.this, Constants.Screen_Track, "PostQuestion", keyword_id, "");
        progressBarDialog = new ProgressBarDialog(PostQuestionActivity.this);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            if (b.containsKey("keyword_id")) {
                keyword_id = b.getString("keyword_id");
            }
            if (b.containsKey("type")) {
                postType = b.getString("type");
            }
            if (b.containsKey("problem_related")) {
                problem_related = b.getString("problem_related");
            }
        }

        init();
        setListener();
        audioPlayerTask = new AudioPlayerTask(PostQuestionActivity.this, mPlayButton);
        cropAreaFocusedData = SharedPreferencesUtils.getCropArea(this, Constants.SharedPreferences_CROP_FOCUSED_AREA);
        if (cropAreaFocusedData != null && cropAreaFocusedData.size() > 0) {
            bindData();
        } else {
            RongoApp.getMasterDropDownData(true);
            bindData();
        }

        checkGPSStatus();
        if (!PermissionUtils.isReadStorageGranted(PostQuestionActivity.this)) {
            PermissionUtils.checkPermission(PostQuestionActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE, PERMISSION_READ_STORAGE);
        }
    }

    @Override
    public void init() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        mSettingsClient = LocationServices.getSettingsClient(this);
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000);
        locationRequest.setFastestInterval(2 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        mLocationSettingsRequest = builder.build();
        builder.setAlwaysShow(true);

        imgBack = findViewById(R.id.imgBack);

        rcvCrop = findViewById(R.id.rcvCrop);
        setCropsData();

        txtDarjKare = findViewById(R.id.txtDarjKare);
        txtDarjKare.setAlpha(1f);
        txtDarjKare.setEnabled(true);

        editTextDescription = findViewById(R.id.editTextDescription);

        txtUpload = findViewById(R.id.txtUpload);
        txtDescription = findViewById(R.id.txtDescription);
        txtUploadMore = findViewById(R.id.txtUploadMore);

        imgGuideline = findViewById(R.id.imgGuideline);
        imgNext = findViewById(R.id.imgNext);

        rvFasal = findViewById(R.id.rvFasal);
        rvFasal.addItemDecoration(new GridSpacingItemDecoration(3, (int) Functions.convertDpToPixel(10, this), false));
        rvFasal.setNestedScrollingEnabled(false);
        rvFasal.setLayoutManager(new GridLayoutManager(this, 3));

        rcvProblem = findViewById(R.id.rcvProblem);
        rcvProblem.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
//        rcvProblem.addItemDecoration(new GridSpacingItemDecoration(5, (int) Functions.convertDpToPixel(10, this), false));
//        rcvProblem.setNestedScrollingEnabled(false);
//        rcvProblem.setLayoutManager(new GridLayoutManager(this, 5));

        llPhotoView = (LinearLayout) findViewById(R.id.llPhotoView);
        llMorePhotoView = (LinearLayout) findViewById(R.id.llMorePhotoView);
        llPhotoView.setVisibility(View.VISIBLE);
        llMorePhotoView.setVisibility(View.GONE);

        llCropAreaView = (LinearLayout) findViewById(R.id.llCropAreaView);

        rvAllPhotos = findViewById(R.id.rvAllPhotos);
        rvAllPhotos.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        llVoiceRecordView = (LinearLayout) findViewById(R.id.llVoiceRecordView);
        llVoiceRecordView.setVisibility(View.VISIBLE);

        mediaPlayer = (CardView) findViewById(R.id.mediaPlayer);
        mediaPlayer.setVisibility(View.GONE);
        mPlayButton = (ImageView) findViewById(R.id.ivPlay);
        mPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaPlayer.setVisibility(View.VISIBLE);
                audioPlayerTask.onPlayMedia(filePath);
            }
        });

        mFileNameTextView = (TextView) findViewById(R.id.file_name_text_view);

        TextView tvChangeAudio = (TextView) findViewById(R.id.tvChangeAudio);
        tvChangeAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                filePath = "";
                mediaPlayer.setVisibility(View.GONE);
                mPlayButton.setImageResource(R.mipmap.play);

                audioRecorderCallback = status -> {
                    Log.v("status-", "" + status);
                    if (status.equalsIgnoreCase("1")) {
                        mPlayButton.setImageResource(R.mipmap.play);
                        llVoiceRecordView.setVisibility(View.GONE);
                        mediaPlayer.setVisibility(View.VISIBLE);
                    } else {
                        mediaPlayer.setVisibility(View.GONE);
                        llVoiceRecordView.setVisibility(View.VISIBLE);
                    }
                };
                audioRecorderDialog = new AudioRecorderDialog(audioRecorderCallback);
                audioRecorderDialog.show(getSupportFragmentManager(), "AudioRecorderDialog");
            }
        });
    }

    @Override
    public void setListener() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txtDarjKare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (problem_related.equalsIgnoreCase("0")) {
                    Functions.showSnackBar(findViewById(android.R.id.content), getString(R.string.str_please_select_option));
                    return;
                }
                if (!problem_related.equalsIgnoreCase("5")) {
                    if (mSelectedList == null) {
                        Functions.showSnackBar(findViewById(android.R.id.content), getString(R.string.str_please_select_fasal));
                        return;
                    }
                }

                if (imagesPath != null && imagesPath.size() > 0) {
                    Functions.hideKeyboard(PostQuestionActivity.this);
                    Functions.setFirebaseLogEventTrack(PostQuestionActivity.this, Constants.Click_Event, "PostQuestion", keyword_id, "Post Question");
                    progressBarDialog.showProgressDialogWithTitle(getResources().getString(R.string.app_name));

                    txtDarjKare.setAlpha(0.2f);
                    txtDarjKare.setEnabled(false);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (InternetConnection.checkConnectionForFasl(PostQuestionActivity.this)) {
                                if (InternetConnection.getNetworkClass(PostQuestionActivity.this).equalsIgnoreCase(Constants.NETWORK_TYPE)) {

                                    postQuestionDelayedSMS();
                                    saveOfflineData();
                                } else {
                                    postQuestion();
                                }
                            } else {
                                saveOfflineData();
                            }
                        }
                    }, 100);
                } else {
                    Functions.showSnackBar(findViewById(android.R.id.content), getString(R.string.str_upload_phasal_photo));
                }

            }
        });

        txtUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (problem_related.equalsIgnoreCase("0")) {
                    Functions.showSnackBar(findViewById(android.R.id.content), getString(R.string.str_please_select_option));
                    return;
                }
                if (!problem_related.equalsIgnoreCase("5")) {
                    if (mSelectedList == null) {
                        Functions.showSnackBar(findViewById(android.R.id.content), getString(R.string.str_please_select_fasal));
                        return;
                    }
                }

                accessPermission();
            }
        });

        llVoiceRecordView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                audioRecorderCallback = status -> {
                    Log.v("status-", "" + status);
                    if (status.equalsIgnoreCase("1")) {
                        mPlayButton.setImageResource(R.mipmap.play);
                        llVoiceRecordView.setVisibility(View.GONE);
                        mediaPlayer.setVisibility(View.VISIBLE);
                    } else {
                        mediaPlayer.setVisibility(View.GONE);
                        llVoiceRecordView.setVisibility(View.VISIBLE);
                    }
                };
                audioRecorderDialog = new AudioRecorderDialog(audioRecorderCallback);
                audioRecorderDialog.show(getSupportFragmentManager(), "AudioRecorderDialog");
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            audioPlayerTask.stopPlaying();
            unregisterReceiver(AUDIO_RECORD_RECEIVER);
            System.gc();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setCropsData() {
        int select = 0;
        rcvCrop.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        List<MasterCropData> masterCropList = Functions.getMasterCropList(sharedPreferences);
        List<MasterCropData> masterCropDatas = new ArrayList<MasterCropData>();
        if (masterCropList != null && masterCropList.size() > 0) {
            for (int j = 0; j < masterCropList.size(); j++) {
                if (masterCropList.get(j).getCrop_id().equalsIgnoreCase(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, "1"))) {
                    masterCropDatas.add(new MasterCropData(masterCropList.get(j).getCrop_name(), masterCropList.get(j).getCrop_id(),
                            masterCropList.get(j).getCrop_image(), masterCropList.get(j).getDisplay_name(), "1"));
                    select = j;
                } else {
                    masterCropDatas.add(new MasterCropData(masterCropList.get(j).getCrop_name(), masterCropList.get(j).getCrop_id(),
                            masterCropList.get(j).getCrop_image(), masterCropList.get(j).getDisplay_name(), "0"));
                }
            }
        }
        masterCropDatas.add(new MasterCropData(getString(R.string.str_other), "0", "", getString(R.string.str_other), "0"));

        cropAdapter = new SelectCropAdapter(masterCropDatas);
        rcvCrop.setAdapter(cropAdapter);
        rcvCrop.getLayoutManager().scrollToPosition(select);
//            String cropId = cropAdapter.getSelectedCropId();
    }

    public class SelectCropAdapter extends RecyclerView.Adapter<SelectCropAdapter.ViewHolder> {
        List<MasterCropData> cropData;

        public SelectCropAdapter(List<MasterCropData> cropData) {
            this.cropData = cropData;
        }

        public String getSelectedCropId() {
            String cropId = "";
            for (int i = 0; i < cropData.size(); i++) {
                if (cropData.get(i).getStatus().equalsIgnoreCase("1")) {
                    cropId = cropData.get(i).getCrop_id();
                }
            }
            return cropId;
        }

        @NotNull
        @Override
        public SelectCropAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.post_crops_list_items, parent, false);
            SelectCropAdapter.ViewHolder viewHolder = new SelectCropAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final SelectCropAdapter.ViewHolder holder, final int position) {
            try {
                holder.tvTitle.setText(cropData.get(position).getDisplay_name());

                holder.llCropView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (cropData.get(position).getStatus().equalsIgnoreCase("1")) {
                            cropData.get(position).setStatus("0");
                        } else {
                            for (int i = 0; i < cropData.size(); i++) {
                                if (cropData.get(position).getCrop_id().equalsIgnoreCase(cropData.get(i).getCrop_id())) {
                                    cropData.get(i).setStatus("1");
                                } else {
                                    cropData.get(i).setStatus("0");
                                }
                            }
                        }
                        notifyDataSetChanged();
                    }
                });

                if (cropData.get(position).getStatus().equalsIgnoreCase("1")) {
                    holder.llCropView.setBackground(getResources().getDrawable(R.drawable.rounded_layout_selected));
                    holder.ivTick.setVisibility(View.VISIBLE);
                    holder.tvTitle.setTextColor(getResources().getColor(R.color.bunai_yes));
                } else {
                    holder.llCropView.setBackground(getResources().getDrawable(R.drawable.rounded_layout_unselect));
                    holder.ivTick.setVisibility(View.GONE);
                    holder.tvTitle.setTextColor(getResources().getColor(R.color.ekad));
                }

                if (cropData.get(position).getCrop_id().equalsIgnoreCase("1")) {
                    holder.ivCrop.setImageResource(R.mipmap.maize);
                } else if (cropData.get(position).getCrop_id().equalsIgnoreCase("2")) {
                    holder.ivCrop.setImageResource(R.mipmap.rice);
                } else if (cropData.get(position).getCrop_id().equalsIgnoreCase("3")) {
                    holder.ivCrop.setImageResource(R.mipmap.cotton);
                } else if (cropData.get(position).getCrop_id().equalsIgnoreCase("4")) {
                    holder.ivCrop.setImageResource(R.mipmap.wheat);
                } else if (cropData.get(position).getCrop_id().equalsIgnoreCase("5")) {
                    holder.ivCrop.setImageResource(R.mipmap.okra);
                } else if (cropData.get(position).getCrop_id().equalsIgnoreCase("6")) {
                    holder.ivCrop.setImageResource(R.mipmap.pearmillet);
                } else if (cropData.get(position).getCrop_id().equalsIgnoreCase("7")) {
                    holder.ivCrop.setImageResource(R.mipmap.corinder);
                } else if (cropData.get(position).getCrop_id().equalsIgnoreCase("8")) {
                    holder.ivCrop.setImageResource(R.mipmap.bittergourd);
                } else if (cropData.get(position).getCrop_id().equalsIgnoreCase("9")) {
                    holder.ivCrop.setImageResource(R.mipmap.bottlegourd);
                } else if (cropData.get(position).getCrop_id().equalsIgnoreCase("10")) {
                    holder.ivCrop.setImageResource(R.mipmap.watermelon);
                } else if (cropData.get(position).getCrop_id().equalsIgnoreCase("11")) {
                    holder.ivCrop.setImageResource(R.mipmap.groundnut);
                } else if (cropData.get(position).getCrop_id().equalsIgnoreCase("12")) {
                    holder.ivCrop.setImageResource(R.mipmap.soyabean);
                } else if (cropData.get(position).getCrop_id().equalsIgnoreCase("13")) {
                    holder.ivCrop.setImageResource(R.mipmap.caster);
                } else if (cropData.get(position).getCrop_id().equalsIgnoreCase("0")) {
                    holder.ivCrop.setImageResource(R.mipmap.anya);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return cropData.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvTitle;
            ImageView ivCrop, ivTick;
            RelativeLayout llCropView;

            public ViewHolder(View itemView) {
                super(itemView);
                this.llCropView = (RelativeLayout) itemView.findViewById(R.id.llCropView);
                this.tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
                this.ivCrop = (ImageView) itemView.findViewById(R.id.ivCrop);
                this.ivTick = (ImageView) itemView.findViewById(R.id.ivTick);
            }
        }
    }

    void bindData() {
        try {
            List<ProblemRelatedData> options = SharedPreferencesUtils.getProblemRelated(this, Constants.SharedPreferences_OPTIONS);
            if (options != null && options.size() > 0) {
                cropProblemAdapter = new CropProblemAdapter(this, options, problem_related);
                rcvProblem.setAdapter(cropProblemAdapter);
            }

            cropAreaFocusedData = SharedPreferencesUtils.getCropArea(this, Constants.SharedPreferences_CROP_FOCUSED_AREA);
            if (!cropAreaFocusedData.isEmpty()) {
                cropAreaAdapter = new CropAreaAdapter(this, cropAreaFocusedData, mSelectedList, Functions.getFromSdcard());
                rvFasal.setAdapter(cropAreaAdapter);
            }

            if (problem_related.equalsIgnoreCase("5")) {
                llCropAreaView.setVisibility(View.GONE);
            } else {
                llCropAreaView.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getSelectedProblem(String problem_related) {
        this.problem_related = problem_related;
        Log.v("problem_related", "" + problem_related);
        if (problem_related.equalsIgnoreCase("5")) {
            llCropAreaView.setVisibility(View.GONE);
        } else {
            llCropAreaView.setVisibility(View.VISIBLE);
        }

        mSelectedList = null;
        cropAreaFocusedData = SharedPreferencesUtils.getCropArea(this, Constants.SharedPreferences_CROP_FOCUSED_AREA);
        if (!cropAreaFocusedData.isEmpty()) {
            cropAreaAdapter = new CropAreaAdapter(this, cropAreaFocusedData, mSelectedList, Functions.getFromSdcard());
            rvFasal.setAdapter(cropAreaAdapter);
        }

        GlideApp.with(this).load("")
                .apply(Functions.getCropAreaPlaceholder(0))
                .error(Functions.getCropAreaPlaceholderDrawble(0, this)).into(imgGuideline);
        txtDescription.setText(R.string.str_post_ques_in);
    }

    public void getSelectedArea(CropAreaFocusedData mCropAreaFocusedData, int pos, boolean isOpen) {
        this.mSelectedList = mCropAreaFocusedData;
        Log.v("mSelectedList-", pos + " - " + mSelectedList.getImage());
        if (mSelectedList != null) {
            if (InternetConnection.checkConnectionForFasl(this)) {
                GlideApp.with(this).load(mSelectedList.getImage())
                        .apply(Functions.getCropAreaPlaceholder(0))
                        .error(Functions.getCropAreaPlaceholderDrawble(0, this)).into(imgGuideline);
            } else {
                GlideApp.with(this).load("")
                        .apply(Functions.getCropAreaPlaceholder(pos))
                        .error(Functions.getCropAreaPlaceholderDrawble(pos, this)).into(imgGuideline);
            }
            txtDescription.setText(mSelectedList.getDescription());
        } else {
            GlideApp.with(this).load("")
                    .apply(Functions.getCropAreaPlaceholder(0))
                    .error(Functions.getCropAreaPlaceholderDrawble(0, this)).into(imgGuideline);
            txtDescription.setText(R.string.str_post_ques_in);
        }

//        if (isOpen) {
//            accessPermission();
//        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == CAMERA_REQUEST_CODE || requestCode == GALLERY_REQUEST_CODE) {
                if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
                    if (data != null && data.hasExtra("mCurrentPhotoPath")) {
                        mCurrentPhotoPath = data.getStringExtra("mCurrentPhotoPath");
                    }
                    File f = new File(mCurrentPhotoPath);
                    Uri contentUri = Uri.fromFile(f);
                    mCurrentPhotoPath = contentUri.getPath();
                }
                if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    mCurrentPhotoPath = c.getString(columnIndex);
                    c.close();
                    if (mCurrentPhotoPath == null) {
                        Bitmap googlepath = Functions.getBitmapFromUri(selectedImage, this);
                        mCurrentPhotoPath = Functions.getGooglePhotoImagePath(googlepath, this);
                    }
                    if (mCurrentPhotoPath.contains("/root")) {
                        mCurrentPhotoPath = mCurrentPhotoPath.replace("/root", "");
                    }
                }

                if (mCurrentPhotoPath != null && !TextUtils.isEmpty(mCurrentPhotoPath)) {
//                  imagesPath.add(Functions.compressImage(PostQuestionActivity.this, mCurrentPhotoPath));
                    Bitmap myBitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
                    if (myBitmap != null) {
                        imagesPath.add(mCurrentPhotoPath);
                        mCurrentPhotoPath = "";
                        setMultiPhotoUpload();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void accessPermission() {
        Dexter.withContext(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    void showImagePickerOptions() {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        View sheetView = getLayoutInflater().inflate(R.layout.bottom_photo_uplaod_options, null);
        mBottomSheetDialog.setContentView(sheetView);

        sheetView.findViewById(R.id.imgClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        LinearLayout linearCamera = sheetView.findViewById(R.id.linearCamera);
        LinearLayout linearGallery = sheetView.findViewById(R.id.linearGallery);

        linearCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                    if (PermissionUtils.isCameraGranted(PostQuestionActivity.this)) {
                        if (PermissionUtils.isReadStorageGranted(PostQuestionActivity.this)) {
                            if (PermissionUtils.isWriteStorageGranted(PostQuestionActivity.this)) {
                                dispatchTakePictureIntent();
                            } else {
                                PermissionUtils.checkPermission(PostQuestionActivity.this, WRITE_EXTERNAL_STORAGE, PERMISSION_READ_STORAGE);
                            }
                        } else {
                            PermissionUtils.checkPermission(PostQuestionActivity.this, READ_EXTERNAL_STORAGE, PERMISSION_READ_STORAGE);
                        }
                    } else {
                        PermissionUtils.checkPermission(PostQuestionActivity.this, Manifest.permission.CAMERA, PERMISSION_READ_STORAGE);
                    }
                } else {
                    Toast.makeText(PostQuestionActivity.this, R.string.str_camera_not_support, Toast.LENGTH_LONG).show();
                }
            }
        });

        linearGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();

                if (PermissionUtils.isCameraGranted(PostQuestionActivity.this)) {
                    if (PermissionUtils.isReadStorageGranted(PostQuestionActivity.this)) {
                        if (PermissionUtils.isWriteStorageGranted(PostQuestionActivity.this)) {
                            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(intent, GALLERY_REQUEST_CODE);
                        } else {
                            PermissionUtils.checkPermission(PostQuestionActivity.this, WRITE_EXTERNAL_STORAGE, PERMISSION_READ_STORAGE);
                        }
                    } else {
                        PermissionUtils.checkPermission(PostQuestionActivity.this, READ_EXTERNAL_STORAGE, PERMISSION_READ_STORAGE);
                    }
                } else {
                    PermissionUtils.checkPermission(PostQuestionActivity.this, Manifest.permission.CAMERA, PERMISSION_READ_STORAGE);
                }
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        ((View) sheetView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        mBottomSheetDialog.show();
    }

    void dispatchTakePictureIntent() {
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File photoFile = createImageFile();
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(PostQuestionActivity.this, providerAuthority, photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                List<ResolveInfo> resolvedIntentActivities = this.getPackageManager().queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolvedIntentInfo : resolvedIntentActivities) {
                    String packageName = resolvedIntentInfo.activityInfo.packageName;
                    this.grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    void setMultiPhotoUpload() {
        try {
            llPhotoView.setVisibility(View.GONE);
            llMorePhotoView.setVisibility(View.VISIBLE);

            if (imagesPath.size() >= 3) {
                imgNext.setVisibility(View.VISIBLE);
            } else {
                imgNext.setVisibility(View.GONE);
            }

            if (imagesPath.size() > 0) {
                thumbnailsAdapter = new ThumbnailsAdapter(imagesPath);
                rvAllPhotos.setAdapter(thumbnailsAdapter);
                rvAllPhotos.setNestedScrollingEnabled(false);
            }

            imgNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    rvAllPhotos.post(new Runnable() {
                        @Override
                        public void run() {
                            if (imagesPath != null && imagesPath.size() > 0) {
                                rvAllPhotos.scrollToPosition(imagesPath.size() - 1);
                            }
                        }
                    });
                }
            });

            txtUploadMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    accessPermission();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void saveOfflineData() {
        try {
            String picture_area = "";
            if (mSelectedList != null) {
                picture_area = mSelectedList.getType();
            }

            List<OfflineData> offlineData = new ArrayList<>();
            String finalPicture_area = picture_area;

            offlineData.add(new OfflineData(new Random().nextInt(), keyword_id, problem_related, finalPicture_area,
                    editTextDescription.getText().toString().trim(), latitude + "", longitude + "",
                    "" + Functions.getCurrentDateTime(),
                    sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""),
                    sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                    sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                    sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                    postType, "1", filePath, imagesPath));
            appDatabase.offlineDao().insertAll(offlineData);

            /*
             * TODO Change by Dilip to Show Offline Question
             * */
            List<PostData> postData = new ArrayList<>();
            postData.add(new PostData(new Random().nextInt(), String.valueOf(new Random().nextInt()), keyword_id, problem_related,
                    "" + imagesPath, editTextDescription.getText().toString().trim(), "", "0", "0",
                    "0", "0", "", sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""),
                    "" + Functions.getCurrentDateTime(), "1", "", SharedPreferencesUtils.getUserId(PostQuestionActivity.this),
                    "", filePath, RongoApp.getUserFullName(), "", sharedPreferences.getString(Constants.SharedPreference_New_City, ""),
                    "", "", "0", "0", ""));

            appDatabase.postDao().insertAll(postData);
            afterRedirectToCommunity();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void afterRedirectToCommunity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBarDialog.hideProgressDialogWithTitle();
                Intent i = new Intent();
                setResult(Activity.RESULT_OK, i);
                finish();
            }
        }, 500);
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logSubmitQueryToExpertEvent(String datetime, String user_id, String season_id, String keyword) {
        Bundle params = new Bundle();
        params.putString(Constants.EVENT_PARAM_DATE_TIME, datetime);
        params.putString(Constants.EVENT_PARAM_USER_ID, user_id);
        params.putString(Constants.EVENT_PARAM_KEYWORD, keyword);
        AppEventsLogger.newLogger(this).logEvent(Constants.EVENT_NAME_ASK_TO_EXPERT, params);
    }


    RequestBody getRequestImageFile(File file) {
        RequestBody fbody = RequestBody.create(file, MediaType.parse("image/*"));
        return fbody;
    }

    RequestBody getRequestAudioFile(File file) {
        RequestBody fbody = RequestBody.create(file, MediaType.parse("audio/*"));
        return fbody;
    }

    void postQuestion() {
        logSubmitQueryToExpertEvent(Functions.getCurrentDateTime(), SharedPreferencesUtils.getUserId(this),
                sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""), keyword_id);

        RequestBody requestBodyId = RequestBody.create(SharedPreferencesUtils.getUserId(this).trim(), MediaType.parse("text/plain"));
        RequestBody seasonIdBody = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyCropId = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyPCropId = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyCropWeek = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""), MediaType.parse("text/plain"));
        RequestBody requestBodykeyword = RequestBody.create(keyword_id.trim(), MediaType.parse("text/plain"));
        RequestBody requestBodyProblemRelated = RequestBody.create(problem_related + "", MediaType.parse("text/plain"));
        String picture_area = "";
        if (mSelectedList != null) {
            picture_area = mSelectedList.getType();
        }
        RequestBody requestBodyPictureArea = RequestBody.create(picture_area, MediaType.parse("text/plain"));
        RequestBody requestBodyDescription = RequestBody.create(editTextDescription.getText().toString().trim(), MediaType.parse("text/plain"));
        RequestBody requestBodyGeoLatitude = RequestBody.create(latitude + "", MediaType.parse("text/plain"));
        RequestBody requestBodyGeloLongitude = RequestBody.create(longitude + "", MediaType.parse("text/plain"));
        RequestBody requestBodyDeviceId = RequestBody.create(Functions.getDeviceid(this), MediaType.parse("text/plain"));
        RequestBody requestBodyFCMToken = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyAppVersion = RequestBody.create(RongoApp.getAppVersion(), MediaType.parse("text/plain"));
        RequestBody requestBodyType = RequestBody.create(postType, MediaType.parse("text/plain"));
        RequestBody requestBodyAppLanguage = RequestBody.create(RongoApp.getDefaultLanguage(), MediaType.parse("text/plain"));
        RequestBody requestBodyAppSeason = RequestBody.create(RongoApp.getSeasonName(), MediaType.parse("text/plain"));

        Map<String, RequestBody> params = new HashMap<>();
        params.put("user_id", requestBodyId);
        params.put("season_id", seasonIdBody);
        params.put("crop_id", requestBodyCropId);
        params.put("p_crop_id", requestBodyPCropId);
        params.put("crop_week", requestBodyCropWeek);
        params.put("keyword", requestBodykeyword);
        params.put("problem_related", requestBodyProblemRelated);
        params.put("picture_area_focused", requestBodyPictureArea);
        params.put("description", requestBodyDescription);
        params.put("geo_latitude", requestBodyGeoLatitude);
        params.put("geo_longitude", requestBodyGeloLongitude);
        params.put("device_id", requestBodyDeviceId);
        params.put("fcm_token", requestBodyFCMToken);
        params.put("app_version", requestBodyAppVersion);
        params.put("type", requestBodyType);
        params.put("app_language", requestBodyAppLanguage);
        params.put("season_name", requestBodyAppSeason);

        if (!TextUtils.isEmpty(filePath)) {
            RequestBody requestBodyPath = RequestBody.create(filePath, MediaType.parse("text/plain"));
            params.put("recording_key", requestBodyPath);
            RequestBody requestBodyAudioPath = RequestBody.create(filePath, MediaType.parse("text/plain"));
            params.put("audio_path[]", requestBodyAudioPath);
            File auFile = new File(filePath);
            params.put("audio[]" + "\"; filename=\"" + auFile.getName(), getRequestAudioFile(auFile));
        }

        if (imagesPath != null && imagesPath.size() > 0) {
            RequestBody requestBodyImageUploadedPath = RequestBody.create(imagesPath.toString(), MediaType.parse("text/plain"));
            params.put("picture_key_local", requestBodyImageUploadedPath);
            for (int i = 0; i < imagesPath.size(); i++) {
                File file = new File(imagesPath.get(i).toString().trim());
                try {
                    File compressedImages = new ImageZipper(this).compressToFile(file);
                    params.put("picture[]" + "\"; filename=\"" + file.getName(), getRequestImageFile(compressedImages));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        RetrofitClient.getInstance().getApi().postQuestion(params).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String str = response.body().string();
                        if (!TextUtils.isEmpty(str)) {
                            JSONObject jsonObject = new JSONObject(str);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    imagesPath = new ArrayList<>();
                                    mSelectedList = null;
                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");
                                    SharedPreferencesUtils.storeStateLong(sharedPreferences, Constants.SharedPreference_Time_FOR_API_CALL, System.currentTimeMillis());
//                                    txtDarjKare.setAlpha(1f);
//                                    txtDarjKare.setEnabled(true);
                                    RongoApp.getPosts();
                                    getProfile();
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressBarDialog.hideProgressDialogWithTitle();
                        txtDarjKare.setAlpha(1f);
                        txtDarjKare.setEnabled(true);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
                txtDarjKare.setAlpha(1f);
                txtDarjKare.setEnabled(true);
            }
        });
    }

    void getProfile() {
        RetrofitClient.getInstance().getApi().getProfile(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(this),
                sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), SharedPreferencesUtils.getUserId(this)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String r_str = response.body().string();
                        if (!TextUtils.isEmpty(r_str)) {
                            JSONObject jb = new JSONObject(r_str);
                            if (jb != null && jb.length() > 0) {
                                new InsertHomeFeedData(jb).insertData();
                            }
                        }
                        afterRedirectToCommunity();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                android.util.Log.v("ASASAS", t.getMessage());
            }
        });
    }

    void postQuestionDelayedSMS() {
        RetrofitClient.getInstance().getApi().postQuestionDelayedSMS(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                SharedPreferencesUtils.getUserId(PostQuestionActivity.this)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void accessLocation() {
        Dexter.withContext(this).withPermissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION).withListener(new MultiplePermissionsListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report) {
                if (report.areAllPermissionsGranted()) {
                    getCurrentLocation();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                token.continuePermissionRequest();
            }
        }).check();
    }

    void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        fusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }
            }
        });
    }

    void checkGPSStatus() {
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                    .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                        @SuppressLint("MissingPermission")
                        @Override
                        public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                            accessLocation();
                        }
                    })
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            int statusCode = ((ApiException) e).getStatusCode();
                            switch (statusCode) {
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    try {
                                        ResolvableApiException rae = (ResolvableApiException) e;
                                        rae.startResolutionForResult(PostQuestionActivity.this, 202);
                                    } catch (IntentSender.SendIntentException sie) {
                                        android.util.Log.i("TAGGING", "PendingIntent unable to execute request.");
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    String errorMessage = "Location settings are inadequate, and cannot be " + "fixed here. Fix in Settings.";
                                    android.util.Log.e("TAGGING_ERROR", errorMessage);
                                    Functions.showSnackBar(findViewById(android.R.id.content), errorMessage);
                            }
                        }
                    });
        } else {
            accessLocation();
        }
    }

}