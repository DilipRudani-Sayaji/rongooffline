package com.rongoapp.activities;

import static com.rongoapp.controller.RongoApp.mAppOpenData;
import static com.rongoapp.utilities.AppSignatureHelper.hash;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.rongoapp.R;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.InitInterface;
import com.rongoapp.controller.LocaleManager;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.AddCropStatusData;
import com.rongoapp.data.InsertHomeFeedData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.otp.MySMSBroadcastReceiver;
import com.rongoapp.otp.OtpView;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.ProgressBarDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPActivity extends AppCompatActivity implements InitInterface, MySMSBroadcastReceiver.Listener {

    AppDatabase appDatabase;
    SharedPreferences sharedPreferences;
    long mLastLongClick = 0;
    ProgressBarDialog progressBarDialog;
    OtpView ediTextOTP;
    AppCompatEditText editTextPhoneNumber;
    AppCompatTextView txtVerify, txtResendOTP, txtChangeNumber, txtCountryCode;
    String mobileNumber = "", fromSplash = "N", fromOTP = "", networkType = Constants.NETWORK_TYPE;

    MySMSBroadcastReceiver mySMSBroadcastReceiver = new MySMSBroadcastReceiver();

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verify);

        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(this);
        Functions.setUserScreenTrackEvent(appDatabase, "OTP", "0");

        Bundle b = getIntent().getExtras();
        if (b != null) {
            if (b.containsKey("mobileNumber")) {
                mobileNumber = b.getString("mobileNumber");
            }
            if (b.containsKey("fromSplash")) {
                fromSplash = b.getString("fromSplash");
            }
            if (b.containsKey("otp")) {
                fromOTP = b.getString("otp");
            }
        }

        Functions.setFirebaseLogEventTrack(OTPActivity.this, Constants.Screen_Track, "OTP", mobileNumber, fromOTP);
        init();
        setListener();
    }

    @Override
    public void init() {
        editTextPhoneNumber = findViewById(R.id.editTextPhonenumber);
        ediTextOTP = findViewById(R.id.ediTextOTP);
        txtVerify = findViewById(R.id.txtVerify);
        txtResendOTP = findViewById(R.id.txtResendOTP);
        txtChangeNumber = findViewById(R.id.txtChangeNumber);
        txtCountryCode = findViewById(R.id.txtCountryCode);
        ImageView imgLogo = findViewById(R.id.imgLogo);
        Functions.setRongoLogo(imgLogo);

        editTextPhoneNumber.setText(sharedPreferences.getString(Constants.SharedPreferences_MOBILE_NO, ""));

        if (!TextUtils.isEmpty(RongoApp.getUserId())) {
            if (fromSplash.equalsIgnoreCase("Y")) {
                if (InternetConnection.checkConnection(OTPActivity.this)) {
                    changeNumber();
                }
            }
            getSMS();
            networkType = sharedPreferences.getString(Constants.SharedPreference_NetworkType, Constants.NETWORK_TYPE);
        } else {
            networkType = Constants.NETWORK_OFFLINE;
            registerAPI();
        }
    }

    @Override
    public void setListener() {
        txtVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()) {
                    if (mobileNumber.equalsIgnoreCase("9408482953")) {
                        ProgressBarDialog progressBarDialog = new ProgressBarDialog(OTPActivity.this);
                        progressBarDialog.showProgressDialogWithTitle(getResources().getString(R.string.app_name));
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_VERIFY_OTP, "Y");
                        getProfile();
                    } else {
                        if (InternetConnection.checkConnection(OTPActivity.this)) {
                            if (Functions.doubleTapCheck(mLastLongClick)) {
                                return;
                            }
                            mLastLongClick = SystemClock.elapsedRealtime();

                            verifyOTP();
                        }
                    }
                }
            }
        });

        txtChangeNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                editTextPhoneNumber.getText().clear();
            }
        });

        txtResendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                if (!TextUtils.isEmpty(editTextPhoneNumber.getText().toString().trim())) {
                    if (InternetConnection.checkConnection(OTPActivity.this)) {
                        changeNumber();
                    }
                } else {
                    Functions.showSnackBar(findViewById(android.R.id.content), getString(R.string.please_enter_your_number));
                }
            }
        });

        editTextPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!TextUtils.isEmpty(charSequence.toString()) && charSequence.length() > 10) {
                    if (InternetConnection.checkConnection(OTPActivity.this)) {
                        changeNumber();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    void verifyOTP() {
        Functions.setFirebaseLogEventTrack(OTPActivity.this, Constants.Click_Event, "OTP",
                SharedPreferencesUtils.getUserId(this), "verifyOTP");

        ProgressBarDialog progressBarDialog = new ProgressBarDialog(OTPActivity.this);
        progressBarDialog.showProgressDialogWithTitle(getResources().getString(R.string.app_name));

        RetrofitClient.getInstance().getApi().verifyOTP(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                Functions.getDeviceid(this), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                editTextPhoneNumber.getText().toString().trim(), SharedPreferencesUtils.getUserId(this), networkType).enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBarDialog.showProgressDialogWithTitle(getResources().getString(R.string.app_name));
                if (response != null) {
                    try {
                        String responseString = response.body().string();
                        if (!TextUtils.isEmpty(responseString)) {
                            JSONObject jsonObject = new JSONObject(responseString);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_VERIFY_OTP, "Y");
                                    setUserData();
                                } else if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                                    Functions.showSnackBar(findViewById(android.R.id.content), TextUtils.htmlEncode(jsonObject.optString("status_message")));
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        });
    }

    void setUserData() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                try {
                    if (networkType.equalsIgnoreCase(Constants.NETWORK_OFFLINE)) {
                        List<AddCropStatusData> addCrop = appDatabase.addCropStatusDao().getAllDataByStatus("1");
                        if (addCrop != null && addCrop.size() > 0) {
                            for (int i = 0; i < addCrop.size(); i++) {
                                Log.i("new crop:-", i + "-" + addCrop.size());
                                if (i == (addCrop.size() - 1)) {
                                    startNewSession(addCrop.get(i).getSowingDate(), addCrop.get(i).getCropId(), true);
                                } else {
                                    startNewSession(addCrop.get(i).getSowingDate(), addCrop.get(i).getCropId(), false);
                                }
                            }
                        } else {
                            getProfile();
                        }
                    } else {
                        getProfile();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void changeNumber() {
        Functions.setFirebaseLogEventTrack(OTPActivity.this, Constants.Click_Event, "OTP",
                editTextPhoneNumber.getText().toString().trim(), "changeNumber");

        progressBarDialog = new ProgressBarDialog(OTPActivity.this);
        progressBarDialog.showProgressDialogWithTitle(getResources().getString(R.string.app_name));

        RetrofitClient.getInstance().getApi().updatePhoneNumber(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                RongoApp.getAppVersion(), Functions.getDeviceid(this), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                editTextPhoneNumber.getText().toString().trim(), SharedPreferencesUtils.getUserId(this), networkType).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null) {
                    try {
                        String responseStrig = response.body().string();
                        if (!TextUtils.isEmpty(responseStrig)) {
                            JSONObject jb = new JSONObject(responseStrig);
                            if (jb != null && jb.length() > 0) {
                                if (jb.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject jData = jb.optJSONObject("data");
                                    if (jData != null && jData.length() > 0) {
                                        JSONObject jResult = jData.optJSONObject("result");
                                        if (jResult != null && jResult.length() > 0) {
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_UserId, jResult.optString("user_id"));
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_MOBILE_NO, editTextPhoneNumber.getText().toString().trim());
                                        }
                                    }
                                } else if (jb.optString("status_code").equalsIgnoreCase("1")) {
                                    String message = jb.optString("status_message");
                                    Functions.showSnackBar(findViewById(android.R.id.content), TextUtils.htmlEncode(message));
                                }
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                progressBarDialog.hideProgressDialogWithTitle();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        });
    }

    boolean validate() {
        if (TextUtils.isEmpty(editTextPhoneNumber.getText().toString().trim())) {
            Functions.showSnackBar(findViewById(android.R.id.content), getString(R.string.please_enter_your_number));
            return false;
        }
        if (TextUtils.isEmpty(ediTextOTP.getText().toString().trim())) {
            Functions.showSnackBar(findViewById(android.R.id.content), getString(R.string.str_please_enter_otp));
            return false;
        }
        if (!TextUtils.isEmpty(fromOTP)) {
            if (!ediTextOTP.getText().toString().trim().equalsIgnoreCase(fromOTP)) {
                if (!ediTextOTP.getText().toString().trim().equalsIgnoreCase(Functions.getCurrentDDMM())) {
                    Functions.showSnackBar(findViewById(android.R.id.content), getString(R.string.str_please_enter_valid_otp));
                    return false;
                }
            }
        }
        return true;
    }

    void getSMS() {
        Log.v("getAppSignatures", "" + getAppSignatures());
        SmsRetrieverClient client = SmsRetriever.getClient(this /* context */);
        Task<Void> task = client.startSmsRetriever();
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mySMSBroadcastReceiver.bindListener(OTPActivity.this);
                registerReceiver(mySMSBroadcastReceiver, new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION));
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.v("TImeout", "onFailure()");
            }
        });
    }

    @Override
    public void onSMSReceived(String otp) {
        try {
            ediTextOTP.getText().clear();
            ediTextOTP.setText(otp);

            if (validate()) {
                if (InternetConnection.checkConnection(OTPActivity.this)) {
                    verifyOTP();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTimeOut() {
        Log.v("Timeout", "onTimeOut()");
    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        unregisterReceiver(mySMSBroadcastReceiver);
//    }

    /**
     * Get all the app signatures for the current package
     *
     * @return
     */
    public ArrayList<String> getAppSignatures() {
        ArrayList<String> appCodes = new ArrayList<>();
        try {
            // Get all package signatures for the current package
            String packageName = getPackageName();
            PackageManager packageManager = getPackageManager();
            Signature[] signatures = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES).signatures;

            // For each signature create a compatible hash
            for (Signature signature : signatures) {
                String hash = hash(packageName, signature.toCharsString());
                if (hash != null) {
                    appCodes.add(String.format("%s", hash));
                    Log.v("OTP HASHTAG", hash);
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("TAG", "Unable to find package to obtain hash.", e);
        }
        return appCodes;
    }


    JSONArray getDeviceInfoJson() {
        JSONArray jsonArray = new JSONArray();
        try {
            JSONObject obj = new JSONObject();
            obj.put("model_no_name", mAppOpenData.getmModel());
            obj.put("device_os", mAppOpenData.getmOsVersion());
            obj.put("device_brand_name", mAppOpenData.getmBrand());
            obj.put("device_id", mAppOpenData.getAndroidId());
            jsonArray.put(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    void registerAPI() {
        String referred_by = "", referral_source = "";
        try {
            String deepLink = sharedPreferences.getString(Constants.SharedPreference_DynamicLink, "");
            if (!TextUtils.isEmpty(deepLink)) {
//                https://rongoapp.in/app/i/MTUw
                String[] separated = deepLink.toString().split("app/");
                if (!TextUtils.isEmpty(separated[1])) {
                    String[] sep = separated[1].toString().split("/");
                    referral_source = sep[0];
                    try {
                        referred_by = Functions.decodeToString(sep[1]);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        progressBarDialog = new ProgressBarDialog(OTPActivity.this);
        progressBarDialog.showProgressDialogWithTitle(getResources().getString(R.string.app_name));

        RetrofitClient.getInstance().getApi().register(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                RongoApp.getAppVersion(), Functions.getDeviceid(this), RongoApp.getFCMToken(),
                sharedPreferences.getString(Constants.SharedPreference_First_Name, ""),
                sharedPreferences.getString(Constants.SharedPreference_Last_Name, ""),
                sharedPreferences.getString(Constants.SharedPreferences_MOBILE_NO, ""),
                RongoApp.getDefaultLanguage(),
                sharedPreferences.getString(Constants.SharedPreference_New_City, ""),
                sharedPreferences.getString(Constants.SharedPreference_New_State, ""),
                sharedPreferences.getString(Constants.SharedPreference_Country, ""),
                sharedPreferences.getString(Constants.SharedPreference_Lat, ""),
                sharedPreferences.getString(Constants.SharedPreference_Long, ""),
                sharedPreferences.getString(Constants.SharedPreference_State, ""),
                sharedPreferences.getString(Constants.SharedPreference_City, ""),
                sharedPreferences.getString(Constants.SharedPreference_Country, ""),
                getDeviceInfoJson().toString(), referred_by, referral_source, "Offline").enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    editTextPhoneNumber.setText(sharedPreferences.getString(Constants.SharedPreferences_MOBILE_NO, ""));
                    getSMS();
                    if (!TextUtils.isEmpty(response.toString()) && response != null) {
                        String responseStr = response.body().string();
                        if (!TextUtils.isEmpty(responseStr)) {
                            JSONObject jb = new JSONObject(responseStr);
                            if (jb != null && jb.length() > 0) {
                                JSONObject jData = jb.optJSONObject("data");
                                if (jData != null && jData.length() > 0) {
                                    JSONObject jResult = jData.optJSONObject("result");
                                    if (jResult != null && jResult.length() > 0) {
                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                                Constants.SharedPreference_UserId, jResult.optString("user_id"));
                                        fromOTP = jResult.optString("otp");
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                progressBarDialog.hideProgressDialogWithTitle();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        });
    }


    void startNewSession(String dateOfSowing, String cropId, boolean isLoad) {
        String is_sowing_done = "", s = "0000-00-00 00:00:00", es = "0000-00-00 00:00:00";
        long dTime = Functions.covertToMili("dd-MM-yyyy", dateOfSowing);
        if (System.currentTimeMillis() > dTime) {
            is_sowing_done = "yes";
            s = dateOfSowing;
        } else {
            is_sowing_done = "no";
            es = dateOfSowing;
        }

        RetrofitClient.getInstance().getApi().startNewCropSeason(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                RongoApp.getAppVersion(), SharedPreferencesUtils.getUserId(this),
                sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                "0", cropId, is_sowing_done, s, es).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_CROP_ID, "" + cropId);
                        String r = response.body().string();
                        if (!TextUtils.isEmpty(r)) {
                            JSONObject jsonObject = new JSONObject(r);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject data = jsonObject.optJSONObject("data");
                                    if (data != null && data.length() > 0) {
                                        String p_crop_id = data.optString("p_crop_id");
                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_P_CROP_ID, "" + p_crop_id);

                                        saveSowingInfo(dateOfSowing);
                                        updateWeeklyCropInfo(dateOfSowing);

                                        if (isLoad) {
                                            appDatabase.addCropStatusDao().nukeTable();
                                            getProfile();
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressBarDialog.hideProgressDialogWithTitle();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        });
    }

    void saveSowingInfo(String dateOfSowing) {
        String is_sowing_done = "", seedType = "", s = "0000-00-00 00:00:00", es = "0000-00-00 00:00:00";
        long dTime = Functions.covertToMili("dd-MM-yyyy", dateOfSowing);
        if (System.currentTimeMillis() > dTime) {
            is_sowing_done = "yes";
            s = dateOfSowing;
        } else {
            is_sowing_done = "no";
            es = dateOfSowing;
        }
        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Sowing_Done, is_sowing_done);

        RetrofitClient.getInstance().getApi().saveSowingInfo(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                Functions.getDeviceid(this), RongoApp.getFCMToken(), SharedPreferencesUtils.getUserId(this),
                sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""), is_sowing_done, s, es, seedType,
                sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, "")).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        });
    }

    void updateWeeklyCropInfo(String dateOfSowing) {
        String infoType = "sowing", infoText = getResources().getString(R.string.str_buvay), metaData = getResources().getString(R.string.str_ha);
        long dTime = Functions.covertToMili("dd-MM-yyyy", dateOfSowing);
        if (System.currentTimeMillis() > dTime) {
            metaData = getResources().getString(R.string.str_ha);
        } else {
            metaData = getResources().getString(R.string.str_na);
        }

        RetrofitClient.getInstance().getApi().updateWeeklyCropInfo(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                RongoApp.getAppVersion(), Functions.getDeviceid(this), SharedPreferencesUtils.getUserId(this),
                sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_Field_ID, ""),
                infoType, infoText, dateOfSowing, metaData).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        });
    }


    void getProfile() {
        RetrofitClient.getInstance().getApi().getProfile(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                Functions.getDeviceid(this), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                SharedPreferencesUtils.getUserId(this)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String r_str = response.body().string();
                        if (!TextUtils.isEmpty(r_str)) {
                            JSONObject jsonObject = new JSONObject(r_str);
                            if (jsonObject != null && jsonObject.length() > 0) {

                                new InsertHomeFeedData(jsonObject).insertData();

                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject jData = jsonObject.optJSONObject("data");
                                    if (jData != null && jData.length() > 0) {
                                        JSONObject jresult = jData.optJSONObject("result");
                                        if (jresult != null && jresult.length() > 0) {
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_App_Version, "" + jresult.optString("is_new_version_available"));
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Salah_Counter, "" + jresult.optString("view_pending_count"));
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_RongoHelpLineNumber, "" + jresult.optString("helpline_number"));

                                            JSONObject jseason = jresult.optJSONObject("season");
                                            if (jseason != null && jseason.length() > 0) {
                                                String season_id = jseason.optString("season_id");
                                                JSONArray jsonArray = jseason.optJSONArray("crops");
                                                if (jsonArray != null && jsonArray.length() > 0) {
                                                    JSONObject jb_season = jsonArray.optJSONObject(0);
                                                    String field_id = jb_season.optString("field_id");
                                                    String p_crop_id = jb_season.optString("p_crop_id");
                                                    String is_sowing_done = jb_season.optString("is_sowing_done");
                                                    String date_of_sowing = jb_season.optString("date_of_sowing");
                                                    String crop_id = jb_season.optString("crop_id");

                                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_SowingDate, date_of_sowing);
                                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Sowing_Done, is_sowing_done);
                                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_P_CROP_ID, p_crop_id);
                                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_ID, season_id);
                                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_CROP_ID, crop_id);
                                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Field_ID, field_id);
                                                }
                                            }
                                        }
                                    }
                                }

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent intent = new Intent();
                                        intent.setAction(Constants.BROADCAST_RECEIVER.UPDATE_CROP_INFO);
                                        sendBroadcast(intent);
                                    }
                                }, 100);

                            }
                        }

                        Intent i = new Intent(OTPActivity.this, HomeActivity.class);
                        i.putExtra("fromLaunch", "N");
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finishAffinity();
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressBarDialog.hideProgressDialogWithTitle();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        });
    }

}
