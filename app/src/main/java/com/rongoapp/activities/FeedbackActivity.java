package com.rongoapp.activities;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.speech.RecognizerIntent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.android.play.core.review.testing.FakeReviewManager;
import com.google.android.play.core.tasks.Task;
import com.rongoapp.R;
import com.rongoapp.controller.AudioPlayerTask;
import com.rongoapp.controller.AudioRecorderCallback;
import com.rongoapp.controller.AudioRecorderDialog;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.InitInterface;
import com.rongoapp.controller.LocaleManager;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.FeedbackData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.Log;
import com.rongoapp.views.ProgressBarDialog;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackActivity extends AppCompatActivity implements InitInterface {

    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    ProgressBarDialog progressBarDialog;
    long mLastLongClick = 0;

    ImageView imgBack, mPlayButton, ivReview;
    AppCompatTextView txtEnter, tvDarjKare, tvDarjKare1;
    EditText editTextDescription;
    CardView mediaplayer_view;
    LinearLayout llVoiceRecordView, llTopView, llBackview;
    TextView mFileNameTextView;
    String filePath, mSpeechText = "", type = "0", post_id = "";

    AudioPlayerTask audioPlayerTask;
    AudioRecorderDialog audioRecorderDialog;
    AudioRecorderCallback audioRecorderCallback;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    BroadcastReceiver AUDIO_RECORD_RECEIVER = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent != null) {
                    String fileName = intent.getStringExtra("mFileName");
                    filePath = intent.getStringExtra("mFilePath");

                    llVoiceRecordView.setVisibility(View.GONE);
                    mediaplayer_view.setVisibility(View.VISIBLE);
                    audioPlayerTask.onPlayMedia(filePath);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerReceiver(AUDIO_RECORD_RECEIVER, new IntentFilter(Constants.BROADCAST_RECEIVER.AUDIO_RECORD_RECEIVER));

        setContentView(R.layout.activity_feedback);

        progressBarDialog = new ProgressBarDialog(FeedbackActivity.this);
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(this);
        Functions.setUserScreenTrackEvent(appDatabase, "FeedBack", "0");

        Bundle b = getIntent().getExtras();
        if (b != null) {
            if (b.containsKey("type")) {
                type = b.getString("type");
            }
            if (b.containsKey("post_id")) {
                post_id = b.getString("post_id");
            }
        }
        Functions.setFirebaseLogEventTrack(FeedbackActivity.this, Constants.Screen_Track, "FeedBack", post_id, type);

        init();
        setListener();
        audioPlayerTask = new AudioPlayerTask(FeedbackActivity.this, mPlayButton);
    }

    @Override
    public void init() {
        ivReview = findViewById(R.id.ivReview);
        imgBack = findViewById(R.id.imgBack);
        txtEnter = findViewById(R.id.txtEnter);
        editTextDescription = findViewById(R.id.editTextDescription);

        tvDarjKare = findViewById(R.id.tvDarjKare);
        tvDarjKare1 = findViewById(R.id.tvDarjKare1);

        llTopView = findViewById(R.id.llTopView);
        llBackview = findViewById(R.id.llBackview);
        llTopView.setVisibility(View.VISIBLE);
        llBackview.setVisibility(View.GONE);

        llVoiceRecordView = (LinearLayout) findViewById(R.id.llVoiceRecordView);
        llVoiceRecordView.setVisibility(View.VISIBLE);

        mediaplayer_view = (CardView) findViewById(R.id.mediaplayer_view);
        mediaplayer_view.setVisibility(View.GONE);
        mPlayButton = (ImageView) findViewById(R.id.ivPlay);
        mFileNameTextView = (TextView) findViewById(R.id.file_name_text_view);

        TextView tvChangeAudio = (TextView) findViewById(R.id.tvChangeAudio);
        tvChangeAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                filePath = "";
                mediaplayer_view.setVisibility(View.GONE);
                mPlayButton.setImageResource(R.mipmap.play);

                audioRecorderCallback = status -> {
                    Log.v("status-", "" + status);
                    if (status.equalsIgnoreCase("1")) {
                        mPlayButton.setImageResource(R.mipmap.play);
                        llVoiceRecordView.setVisibility(View.GONE);
                        mediaplayer_view.setVisibility(View.VISIBLE);
                    } else {
                        mediaplayer_view.setVisibility(View.GONE);
                        llVoiceRecordView.setVisibility(View.VISIBLE);
                    }
                };
                audioRecorderDialog = new AudioRecorderDialog(audioRecorderCallback);
                audioRecorderDialog.show(getSupportFragmentManager(), "AudioRecorderDialog");
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 101: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    mSpeechText = result.get(0);
                    Log.v("mSpeechText111", "" + mSpeechText);
                }
                break;
            }
        }
    }

    @Override
    public void setListener() {
        ivReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReviewClickView();
            }
        });

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvDarjKare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llTopView.setVisibility(View.GONE);
                llBackview.setVisibility(View.VISIBLE);
            }
        });

        tvDarjKare1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                llTopView.setVisibility(View.GONE);
//                llBackview.setVisibility(View.VISIBLE);

                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                Functions.setFirebaseLogEventTrack(FeedbackActivity.this, Constants.Click_Event, "FeedBack", "", "Ask Expert");
                Intent i = new Intent(FeedbackActivity.this, PostQuestionActivity.class);
                i.putExtra("keyword_id", "0");
                i.putExtra("type", "1");
                activityResultLauncher.launch(i);
            }
        });

        txtEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                if (TextUtils.isEmpty(editTextDescription.getText().toString().trim())) {
                    Functions.showSnackBar(findViewById(android.R.id.content), getString(R.string.str_apni_patikriya_likhe));
                } else {
                    if (InternetConnection.checkConnection(FeedbackActivity.this)) {
                        if (InternetConnection.getNetworkClass(FeedbackActivity.this).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                            saveOfflineData();
                        } else {
                            uploadFeedback();

                            txtEnter.setAlpha(0.2f);
                            txtEnter.setEnabled(false);
                        }
                    } else {
                        saveOfflineData();
                    }
                }
            }
        });

        llVoiceRecordView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                audioRecorderCallback = status -> {
                    Log.v("status-", "" + status);
                    if (status.equalsIgnoreCase("1")) {
                        mPlayButton.setImageResource(R.mipmap.play);
                        llVoiceRecordView.setVisibility(View.GONE);
                        mediaplayer_view.setVisibility(View.VISIBLE);
                    } else {
                        mediaplayer_view.setVisibility(View.GONE);
                        llVoiceRecordView.setVisibility(View.VISIBLE);
                    }
                };
                audioRecorderDialog = new AudioRecorderDialog(audioRecorderCallback);
                audioRecorderDialog.show(getSupportFragmentManager(), "AudioRecorderDialog");
            }
        });

        mPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaplayer_view.setVisibility(View.VISIBLE);
                audioPlayerTask.onPlayMedia(filePath);
            }
        });
    }

    ActivityResultLauncher<Intent> activityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Functions.openPostQuestionSuccessDialog(FeedbackActivity.this, appDatabase,
                                RewardPoints.getPostQuestionRewardPoint(sharedPreferences),
                                RewardPoints.getInviteRewardPoint(sharedPreferences));
                    }
                }
            });

    void saveOfflineData() {
        try {
            List<FeedbackData> feedbackData = new ArrayList<>();
            feedbackData.add(new FeedbackData(0, SharedPreferencesUtils.getUserId(FeedbackActivity.this),"", editTextDescription.getText().toString().trim(), filePath));
            appDatabase.feedbackDao().insertAll(feedbackData);
            editTextDescription.getText().clear();
            progressBarDialog.hideProgressDialogWithTitle();
            openDhanyawad();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    RequestBody getRequestAudioFile(File file) {
        RequestBody fbody = RequestBody.create(file, MediaType.parse("audio/*"));
        return fbody;
    }

    void uploadFeedback() {
        progressBarDialog.showProgressDialogWithTitle(getResources().getString(R.string.app_name));
        RequestBody requestBodyId = RequestBody.create(SharedPreferencesUtils.getUserId(this).trim(), MediaType.parse("text/plain"));
        RequestBody requestBodyDeviceId = RequestBody.create(Functions.getDeviceid(this), MediaType.parse("text/plain"));
        RequestBody requestBodyFCMToken = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyAppVersion = RequestBody.create(RongoApp.getAppVersion(), MediaType.parse("text/plain"));
        RequestBody requestBodyFeedBack = RequestBody.create(editTextDescription.getText().toString(), MediaType.parse("text/plain"));
        RequestBody requestBodyType = RequestBody.create(type, MediaType.parse("text/plain"));
        RequestBody requestBodyPost_id = RequestBody.create(post_id, MediaType.parse("text/plain"));
        RequestBody requestBodyAppLanguage = RequestBody.create(RongoApp.getDefaultLanguage(), MediaType.parse("text/plain"));
        RequestBody requestBodyAppSeason = RequestBody.create(RongoApp.getSeasonName(), MediaType.parse("text/plain"));

        Map<String, RequestBody> params = new HashMap<>();
        params.put("user_id", requestBodyId);
        params.put("feedback", requestBodyFeedBack);
        params.put("device_id", requestBodyDeviceId);
        params.put("fcm_token", requestBodyFCMToken);
        params.put("app_version", requestBodyAppVersion);
        params.put("type", requestBodyType);
        params.put("post_id", requestBodyPost_id);
        params.put("app_language", requestBodyAppLanguage);
        params.put("season_name", requestBodyAppSeason);

        if (filePath != null) {
            RequestBody requestBodyAudioPath = RequestBody.create(filePath, MediaType.parse("text/plain"));
            params.put("audio_path[]", requestBodyAudioPath);
            File auFile = new File(filePath);
            params.put("audio[]" + "\"; filename=\"" + auFile.getName(), getRequestAudioFile(auFile));
        }

        RetrofitClient.getInstance().getApi().uploadFeedBack(params).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response != null && response.isSuccessful()) {
                        String str = response.body().string();
                        if (!TextUtils.isEmpty(str)) {
                            JSONObject jsonObject = new JSONObject(str);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");
                                    editTextDescription.getText().clear();
                                    progressBarDialog.hideProgressDialogWithTitle();
                                    txtEnter.setAlpha(1f);
                                    txtEnter.setEnabled(true);
                                    openDhanyawad();
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressBarDialog.hideProgressDialogWithTitle();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        });
    }

    void openDhanyawad() {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
        View view = LayoutInflater.from(this).inflate(R.layout.bottom_dhanyawad, null);
        bottomSheetDialog.setContentView(view);

        AppCompatTextView txtDesc = view.findViewById(R.id.txtDesc);
        txtDesc.setVisibility(View.VISIBLE);

        AppCompatTextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));

        AppCompatTextView tvShareWhatapp = view.findViewById(R.id.tvShareWhatapp);
        tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Feedback", "WhatsApp");
                    Functions.shareWhatAppIntent(FeedbackActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AppCompatTextView tvShareCopy = view.findViewById(R.id.tvShareCopy);
        tvShareCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Feedback", "SMS");
                    Functions.shareSMSIntent(FeedbackActivity.this, RongoApp.getReferralText(FeedbackActivity.this));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AppCompatTextView tvSharefacebook = view.findViewById(R.id.tvSharefacebook);
        tvSharefacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.setUserScreenTrackEvent(appDatabase, "Feedback", "Facebook");
                Functions.shareFacebookIntent(FeedbackActivity.this);
            }
        });

        AppCompatTextView tvShareMore = view.findViewById(R.id.tvShareMore);
        tvShareMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.setUserScreenTrackEvent(appDatabase, "Feedback", "ShareMore");
                Functions.performShare(FeedbackActivity.this, appDatabase);
            }
        });

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                finish();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            audioPlayerTask.stopPlaying();
            unregisterReceiver(AUDIO_RECORD_RECEIVER);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ReviewClickView() {
        try {
//            ReviewManager manager = ReviewManagerFactory.create(FeedbackActivity.this);
            ReviewManager manager = new FakeReviewManager(FeedbackActivity.this);
            Task<ReviewInfo> request = manager.requestReviewFlow();
            request.addOnCompleteListener(task -> {
                try {
                    if (task.isSuccessful()) {
                        ReviewInfo reviewInfo = task.getResult();
                        Task<Void> flow = manager.launchReviewFlow(FeedbackActivity.this, reviewInfo);
                        flow.addOnCompleteListener(task2 -> {
                            // The flow has finished. The API does not indicate whether the user
                            // reviewed or not, or even whether the review dialog was shown. Thus, no
                            // matter the result, we continue our app flow.
                            Log.v("In-app review :", "" + task2.getResult());
                        });
                    } else {
                        // There was some problem, continue regardless of the result.
                        final String appPackageName = this.getPackageName();
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        } catch (ActivityNotFoundException exception) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                        }
                    }
                } catch (Exception ex) {
                    Log.v("Exception from openReview():", "" + ex);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}