package com.rongoapp.activities;

import static com.rongoapp.controller.Constants.CAMERA_REQUEST_CODE;
import static com.rongoapp.controller.Constants.GALLERY_REQUEST_CODE;
import static com.rongoapp.controller.Constants.providerAuthority;
import static com.rongoapp.utilities.AppSignatureHelper.hash;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.card.MaterialCardView;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.novoda.merlin.Connectable;
import com.novoda.merlin.Merlin;
import com.rongoapp.R;
import com.rongoapp.adapter.CropAreaForHomeAdapter;
import com.rongoapp.adapter.SpinnerHintArrayAdapter;
import com.rongoapp.adapter.ThumbnailsAdapter;
import com.rongoapp.audioPlayerTTS.TTS;
import com.rongoapp.audioPlayerTTS.TTSAudioPlayer;
import com.rongoapp.bottombar.SpaceItem;
import com.rongoapp.bottombar.SpaceNavigationView;
import com.rongoapp.bottombar.SpaceOnClickListener;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.FetchJsonFromAssetsFile;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.GlideApp;
import com.rongoapp.controller.InitInterface;
import com.rongoapp.controller.LocaleManager;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.AddCropStatusData;
import com.rongoapp.data.AdvisoryPhotoData;
import com.rongoapp.data.CropAreaFocusedData;
import com.rongoapp.data.CropWeekDetails;
import com.rongoapp.data.CropsData;
import com.rongoapp.data.DailyRewardData;
import com.rongoapp.data.FeedbackData;
import com.rongoapp.data.InsertHomeFeedData;
import com.rongoapp.data.JankariFeedBackData;
import com.rongoapp.data.KhetOfflineData;
import com.rongoapp.data.LuckyDrawData;
import com.rongoapp.data.MasterCropData;
import com.rongoapp.data.OfflineCommentData;
import com.rongoapp.data.OfflineData;
import com.rongoapp.data.OfflineGrowthData;
import com.rongoapp.data.OfflineLikeData;
import com.rongoapp.data.OfflineQuizData;
import com.rongoapp.data.OfflineRewardData;
import com.rongoapp.data.OfflineWeeklyCropInfoData;
import com.rongoapp.data.ProfileOfflineData;
import com.rongoapp.data.ProfileResultData;
import com.rongoapp.data.SeasonData;
import com.rongoapp.data.UserInvite;
import com.rongoapp.data.UserProfileData;
import com.rongoapp.data.WeeklyCropInfo;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.fragments.AdhikFragment;
import com.rongoapp.fragments.CommunityFragment;
import com.rongoapp.fragments.PhasalFragment;
import com.rongoapp.fragments.PragatiFragment;
import com.rongoapp.imagecompressor.ImageZipper;
import com.rongoapp.network.DroidListener;
import com.rongoapp.network.DroidNet;
import com.rongoapp.otp.MySMSBroadcastReceiver;
import com.rongoapp.services.FeedBackService;
import com.rongoapp.services.FileUploadINGrowthService;
import com.rongoapp.services.FileUploadLuckyDrawService;
import com.rongoapp.services.FileUploadService;
import com.rongoapp.services.JankariFeedbackService;
import com.rongoapp.services.OfflineCommentService;
import com.rongoapp.services.OfflineLikeService;
import com.rongoapp.services.OfflineQuizService;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.services.WeeklyCropInfoService;
import com.rongoapp.spinWheel.LuckyItem;
import com.rongoapp.spinWheel.LuckyWheelView;
import com.rongoapp.tooltipView.SimpleTooltip;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.ProgressBarDialog;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.Executors;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements InitInterface, DroidListener, MySMSBroadcastReceiver.Listener {

    public static SpaceNavigationView mCircleNavigationView;
    public static TTS tts;
    public static HomeActivity homeActivity;
    public DroidNet mDroidNet;
    AppUpdateManager mAppUpdateManager;
    boolean isRewardApi = true;

    long mLastLongClick = 0;
    AppDatabase appDatabase;
    static SharedPreferences sharedPreferences;
    FragmentManager fragmentManager;
    Merlin merlin;
    ProgressBarDialog progressBarDialog;

    Bundle savedInstanceStates;
    Dialog BuvayDateDialog;
    BottomSheetDialog bottomSheetDialog;

    LinearLayout lin, rlGiftView, llExpertView;
    AppCompatEditText editTextSowingDate;
    TextView tvSeasonValidation, tvSeasonValidationtxt;
    int mYear = 0, mMonth = 0, mDay = 0;
    String fromLaunch = "Y", sowingDate = "0000-00-00 00:00:00", expectedSowingDate = "0000-00-00 00:00:00", fromDatePicker = "N", rewardId = "";
    List<LuckyItem> data = new ArrayList<>();
    List<MasterCropData> masterCropList;

    MySMSBroadcastReceiver mySMSBroadcastReceiver = new MySMSBroadcastReceiver();

    void getSMS() {
        Log.v("SMSOTP", "" + getAppSignatures());
        SmsRetrieverClient client = SmsRetriever.getClient(this);
        client.startSmsRetriever().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mySMSBroadcastReceiver.bindListener(HomeActivity.this);
                registerReceiver(mySMSBroadcastReceiver, new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION));
            }
        });
    }

    @Override
    public void onSMSReceived(String otp) {
        try {
            Log.d("++ OTP ++", "" + otp);
            Toast.makeText(homeActivity, "" + otp, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTimeOut() {
        Log.v("Timeout", "onTimeOut()");
    }

    BroadcastReceiver UPDATE_TOOLTIP_VIEW = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent != null) {
//                    String fileName = intent.getStringExtra("mFileName");
                    showTooltipView();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    ArrayList<String> getAppSignatures() {
        ArrayList<String> appCodes = new ArrayList<>();
        try {
            // Get all package signatures for the current package
            String packageName = getPackageName();
            PackageManager packageManager = getPackageManager();
            Signature[] signatures = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES).signatures;

            // For each signature create a compatible hash
            for (Signature signature : signatures) {
                String hash = hash(packageName, signature.toCharsString());
                if (hash != null) {
                    appCodes.add(String.format("%s", hash));
                    Log.v("OTP HASHTAG", hash);
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("TAG", "Unable to find package to obtain hash.", e);
        }
        return appCodes;
    }

    public interface IOnBackPressed {
        /**
         * If you return true the back press will not be taken into account, otherwise the activity will act naturally
         *
         * @return true if your processing has priority if not false
         */
        boolean onBackPressed();
    }

    public static void setTextToSpeechInit() {
        try {
            if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.GUJARATI)) {
                tts = new TTS(homeActivity, new Locale(Constants.Gujarati_Language_code));
            } else if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.MARATHI)) {
                tts = new TTS(homeActivity, new Locale(Constants.Marathi_Language_code));
            } else {
                tts = new TTS(homeActivity, new Locale(Constants.Hindi_Language_code));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        RongoApp.hideSystemUI(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            getWindow().getAttributes().layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        }
        registerReceiver(UPDATE_TOOLTIP_VIEW, new IntentFilter(Constants.BROADCAST_RECEIVER.UPDATE_TOOLTIP_VIEW));
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home_feed);

        getSMS();
        RongoApp.getSeasonNameFunction();
        DroidNet.init(this);
        mDroidNet = DroidNet.getInstance();
        mDroidNet.addInternetConnectivityListener(this);
        progressBarDialog = new ProgressBarDialog(HomeActivity.this);
        savedInstanceStates = savedInstanceState;
        homeActivity = this;
        appDatabase = AppDatabase.getDatabase(this);
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
        merlin = new Merlin.Builder().withConnectableCallbacks().build(this);
        Functions.setUserScreenTrackEvent(appDatabase, "Home", "0");
        Functions.setFirebaseLogEventTrack(HomeActivity.this, Constants.Screen_Track, "Home", SharedPreferencesUtils.getUserId(this), "");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            if (bundle.containsKey("fromLaunch")) {
                fromLaunch = bundle.getString("fromLaunch");
            }
        }

        fragmentManager = getSupportFragmentManager();
        mCircleNavigationView = findViewById(R.id.navigation);
        mCircleNavigationView.setCentreButtonSelectable(true);
        mCircleNavigationView.initWithSaveInstanceState(savedInstanceStates);
        mCircleNavigationView.addSpaceItem(new SpaceItem(getResources().getString(R.string.phasal), R.mipmap.plant));
        mCircleNavigationView.addSpaceItem(new SpaceItem(getResources().getString(R.string.growth), R.mipmap.growth));
        mCircleNavigationView.addSpaceItem(new SpaceItem(getResources().getString(R.string.salah), R.mipmap.ci));
        mCircleNavigationView.addSpaceItem(new SpaceItem(getResources().getString(R.string.adhik), R.mipmap.more));

        masterCropList = Functions.getMasterCropList(sharedPreferences);
        if (masterCropList != null && masterCropList.size() == 0) {
            RongoApp.getMasterCropData(true);
        }

        ProfileResultData profileResultData = Functions.getData();
        if (profileResultData == null && InternetConnection.checkConnectionForFasl(HomeActivity.this)) {
            getProfile();
        }

        init();
        setListener();
        setTextToSpeechInit();

        String SowingDone = sharedPreferences.getString(Constants.SharedPreference_Sowing_Done, "");
        if (!TextUtils.isEmpty(SowingDone)) {
            setInitView();
        } else {
            setBuvayDateDialog();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getFirebaseToken();
                setInAppUpdate();
            }
        }, 60000);

        RongoApp.getUserStories();
    }

    String valid(String number) {
        if (number.startsWith("+")) {
            if (number.length() == 13) {
                number = number.substring(3);
            } else if (number.length() == 14) {
                number = number.substring(4);
            }
        }
        return number;
    }

    void getFirebaseToken() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String number = Functions.getMobileNumber("" + telephonyManager.getLine1Number());
            if (!TextUtils.isEmpty(number)) {
                number = valid(number);
                if (!RongoApp.getUserNumber().equalsIgnoreCase(number)) {
                    if (!sharedPreferences.getString(Constants.SharedPreferences_MOBILE_NO, "").equalsIgnoreCase("9408482953")) {
                        if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreference_Sowing_Done, ""))) {
                            if (TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_Number_Change, ""))) {
                                if (InternetConnection.checkConnectionForFasl(HomeActivity.this)) {
                                    if (sharedPreferences.getString(Constants.SharedPreferences_Number_Update_Dialog, "N").equalsIgnoreCase("Y")) {
                                        changeNumberDialog(number);
                                    }
                                }
                            }
                        }
                    }
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Number_Update_Dialog, "Y");
                }
                Log.e("deviceInfo:-", "" + number);

                FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (task.isSuccessful()) {
//                            Log.v("deviceInfo token11:", task.getResult());
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_FCM_TOKEN, "" + task.getResult());
                            if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                                RetrofitClient.getInstance().getApi().updateFCMToken(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                                        RongoApp.getAppVersion(), Functions.getDeviceid(HomeActivity.this), task.getResult(),
                                        Build.MODEL, Build.VERSION.RELEASE, Build.BRAND).enqueue(new Callback<ResponseBody>() {
                                    @Override
                                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    }
                                });
                            }
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void changeNumberDialog(String number) {
        Dialog bottomSheetDialog = new Dialog(homeActivity, R.style.NewDialog);
        bottomSheetDialog.setContentView(R.layout.update_number_dialog);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.setCancelable(false);
        bottomSheetDialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = bottomSheetDialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        ImageView imgClose = bottomSheetDialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        ImageView ivLogo = bottomSheetDialog.findViewById(R.id.ivLogo);
        ivLogo.setImageResource(R.mipmap.number_change);

        TextView txtTitle = bottomSheetDialog.findViewById(R.id.txtTitle);
        txtTitle.setText(R.string.str_update_number);
        TextView tvNumber = bottomSheetDialog.findViewById(R.id.tvNumber);
        tvNumber.setText(number);

        LinearLayout llNumberChange = bottomSheetDialog.findViewById(R.id.llNumberChange);
        llNumberChange.setVisibility(View.VISIBLE);

        LinearLayout llSuccessView = bottomSheetDialog.findViewById(R.id.llSuccessView);
        llSuccessView.setVisibility(View.GONE);

        TextView tvDarjKare = bottomSheetDialog.findViewById(R.id.tvDarjKare);
        TextView tvDarjKare1 = bottomSheetDialog.findViewById(R.id.tvDarjKare1);

        TextView txtYes = bottomSheetDialog.findViewById(R.id.txtYes);
        txtYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateNumber(number);

                llNumberChange.setVisibility(View.GONE);
                llSuccessView.setVisibility(View.VISIBLE);
                ivLogo.setImageResource(R.mipmap.n_done);

                txtTitle.setText(R.string.str_update_number_text);
                tvNumber.setVisibility(View.GONE);

                tvDarjKare.setVisibility(View.VISIBLE);
                tvDarjKare1.setVisibility(View.GONE);
            }
        });

        TextView txtNo = bottomSheetDialog.findViewById(R.id.txtNo);
        txtNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llNumberChange.setVisibility(View.GONE);
                llSuccessView.setVisibility(View.VISIBLE);
                ivLogo.setImageResource(R.mipmap.n_success);

                txtTitle.setText(R.string.str_update_number_text1);
                tvNumber.setVisibility(View.GONE);

                tvDarjKare.setVisibility(View.GONE);
                tvDarjKare1.setVisibility(View.VISIBLE);
            }
        });

        tvDarjKare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Number_Change, "1");
                llNumberChange.setVisibility(View.GONE);
                llSuccessView.setVisibility(View.VISIBLE);
                ivLogo.setImageResource(R.mipmap.n_success);

                tvNumber.setVisibility(View.GONE);
                bottomSheetDialog.dismiss();

//                txtTitle.setText(R.string.str_update_number_text1);
//                tvDarjKare.setVisibility(View.GONE);
//                tvDarjKare1.setVisibility(View.VISIBLE);
            }
        });

        tvDarjKare1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Number_Change, "1");
                llNumberChange.setVisibility(View.GONE);
                llSuccessView.setVisibility(View.VISIBLE);
                tvNumber.setVisibility(View.GONE);
                bottomSheetDialog.dismiss();
            }
        });
    }

    void updateNumber(String number) {
        RetrofitClient.getInstance().getApi().changePhoneNumber(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                RongoApp.getAppVersion(), Functions.getDeviceid(HomeActivity.this), RongoApp.getUserId(),
                number, sharedPreferences.getString(Constants.SharedPreferences_MOBILE_NO, "")).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }


    void setInitView() {
        try {
            Intent intent = getIntent();
            if (intent != null) {
                if (intent.hasExtra("pageType")) {
                    String pageType = intent.getStringExtra("pageType");

                    if (pageType.equalsIgnoreCase(Constants.SALAH)) {
                        ProfileResultData profileResultData = Functions.getData();
                        if (profileResultData != null) {
                            if (profileResultData.getView_pending_count() > 0) {
                                mCircleNavigationView.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        mCircleNavigationView.hideBadgeAtIndex(2);
                                    }
                                }, 1000);
                            }
                        }
                        mCircleNavigationView.changeCurrentItem(2);
                        setExpertVisible(false);
                        setRlGiftView(false);
                    }

                    if (pageType.equalsIgnoreCase(Constants.ADHIK)) {
                        mCircleNavigationView.changeCurrentItem(3);
                        setExpertVisible(false);
                        setRlGiftView(false);
                    }

                    if (pageType.equalsIgnoreCase(Constants.PRAGATI)) {
                        mCircleNavigationView.changeCurrentItem(1);
                        setExpertVisible(true);
                        setRlGiftView(false);
                    }
                } else {
                    fragmentManager.beginTransaction().replace(R.id.frame_container_details, PhasalFragment.getInstance(), "1").commit();
                }
            } else {
                fragmentManager.beginTransaction().replace(R.id.frame_container_details, PhasalFragment.getInstance(), "1").commit();
            }

            setAlreadyCropAdd();
            if (InternetConnection.checkConnectionForFasl(HomeActivity.this)) {
                if (TextUtils.isEmpty(RongoApp.getWeeklyCropQuestion())) {
                    RongoApp.getAllCropWeeklyQuestion(true);
                }
            }

            if (appDatabase.postDao().getPosts().size() == 0) {
                RongoApp.getPosts();
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (progressBarDialog != null) {
                        progressBarDialog.hideProgressDialogWithTitle();
                    }
                }
            }, 1000);

//            HighlightManager highlightManager = new HighlightManager(this);
//            highlightManager.reshowAllHighlights();
//            highlightManager.addView(R.id.text1).setTitle(R.string.str_rongo_help_line_number)
//                    .setDescriptionId(R.string.str_rongo_help_line_number);
//            highlightManager.addView(R.id.text2).setTitle(R.string.atr_aapke_reward)
//                    .setDescriptionId(R.string.str_you_get_reward);
//            highlightManager.addView(R.id.text3).setTitle(R.string.str_phasal_jankari)
//                    .setDescriptionId(R.string.str_you_see_salah_our_tag);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setAlreadyCropAdd() {
        try {
            List<CropsData> cropsData = Functions.getCropDataByID(RongoApp.getSelectedCrop());
            if (cropsData.size() > 0) {
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_SowingDate, cropsData.get(0).getDate_of_sowing());
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_CROP_ID, cropsData.get(0).getCrop_id());
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_P_CROP_ID, cropsData.get(0).getP_crop_id());
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Field_ID, cropsData.get(0).getField_id());
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Sowing_Done, cropsData.get(0).getIs_sowing_done());
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_season_end_crop_week, cropsData.get(0).getSeason_end_crop_week());
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_crop_status, cropsData.get(0).getCrop_status());
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_season_feedback, cropsData.get(0).getSeason_feedback());

                mCircleNavigationView.setCenterView(RongoApp.getSelectedCrop());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mCircleNavigationView.onSaveInstanceState(outState);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    void showTooltipView() {
        try {
            View view = findViewById(R.id.view);
            if (!sharedPreferences.getString(Constants.SharedPreference_Multi_Crop_Info_status, "").equalsIgnoreCase("1")) {
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Multi_Crop_Info_status, "1");

                new SimpleTooltip.Builder(this)
                        .anchorView(view)
                        .gravity(Gravity.TOP)
                        .animated(true)
//                    .setHeight(500)
                        .setWidth(750)
                        .showArrow(false)
                        .contentView(R.layout.tooltip_view)
                        .transparentOverlay(false)
                        .overlayWindowBackgroundColor(Color.BLACK)
                        .build()
                        .show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() {
        ProfileResultData profileResultDataMie = Functions.getData();
        String expiryContent = "", profieExpiry = "";
        if (profileResultDataMie != null) {
            expiryContent = profileResultDataMie.getApi_content_expiry();
            profieExpiry = profileResultDataMie.getApi_profile_expiry();
        }
        if (!TextUtils.isEmpty(expiryContent)) {
            long expiryContentLong = Functions.covertToMili("yyyy-MM-dd HH:mm:ss", expiryContent);
            if ((System.currentTimeMillis() > expiryContentLong)) {
                SharedPreferencesUtils.storeStateLong(sharedPreferences, Constants.SharedPreference_Time_FOR_API_CALL, System.currentTimeMillis());
            }
        } else {
            SharedPreferencesUtils.storeStateLong(sharedPreferences, Constants.SharedPreference_Time_FOR_API_CALL, System.currentTimeMillis());
        }

        if (!TextUtils.isEmpty(profieExpiry)) {
            long expiryContentLong = Functions.covertToMili("yyyy-MM-dd HH:mm:ss", profieExpiry);
            if ((System.currentTimeMillis() > expiryContentLong)) {
                SharedPreferencesUtils.storeStateLong(sharedPreferences, Constants.SharedPreference_Time_FOR_API_CALL_PROFILE, System.currentTimeMillis());
            }
        } else {
            SharedPreferencesUtils.storeStateLong(sharedPreferences, Constants.SharedPreference_Time_FOR_API_CALL_PROFILE, System.currentTimeMillis());
        }

        lin = findViewById(R.id.lin);
        llExpertView = findViewById(R.id.llExpertView);
        rlGiftView = findViewById(R.id.rlGiftView);
        rlGiftView.setVisibility(View.GONE);
        TextView tvRewardNew = findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getPostQuestionRewardPoint(sharedPreferences));
        ImageView ivGift = findViewById(R.id.ivGift);
        Animation shake = AnimationUtils.loadAnimation(HomeActivity.this, R.anim.shakeanimation);
        ivGift.setAnimation(shake);

        if (InternetConnection.checkConnectionForFasl(HomeActivity.this)
                && !InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
            getContestData();
            if (TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_Quiz, ""))) {
                RongoApp.getAllCropWeeklyQuestion(true);
            }
            if (TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_Pending_Reward, ""))) {
                getPendingRewardsData();
            }
            if (TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_Earned_Reward, ""))) {
                getEarnedRewardsData();
            }
        }
        setDailyLoginAlertFunction();
    }

    @Override
    public void setListener() {
        llExpertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                Functions.setFirebaseLogEventTrack(HomeActivity.this, Constants.Click_Event, "Home", "", "ExpertAdvise");
                Intent i = new Intent(HomeActivity.this, PostQuestionActivity.class);
                i.putExtra("keyword_id", "0");
                startActivityForResult(i, Constants.POST_QUESTION_CODE);
            }
        });

        rlGiftView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                Intent i = new Intent(homeActivity, RewardActivity.class);
                startActivity(i);
//                openSpinWheelDialog(sharedPreferences.getString(Constants.SharedPreferences_CONTEST_ID, "0"), "200");
            }
        });

        mCircleNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                Log.v("onItemClick", itemIndex + " : " + itemName);
                if (itemIndex == 0) {
                    fragmentManager.beginTransaction().replace(R.id.frame_container_details, PhasalFragment.getInstance(), "1").commit();
                    setExpertVisible(true);

                    setRlGiftView(true);
                    HomeActivity.stopAudioService();
                } else if (itemIndex == -1) {
//                    fragmentManager.beginTransaction().add(R.id.frame_container_details, PhasalFragment.getInstance(), "1").commit();
                    setExpertVisible(true);
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SessionEndCropID, "");
                    /*
                     * @TODO Add Crop Dialog Open
                     * */
                    openCropSelectionDialog();

                    setRlGiftView(true);
                    HomeActivity.stopAudioService();
                } else if (itemIndex == 1) {
                    fragmentManager.beginTransaction().replace(R.id.frame_container_details, PragatiFragment.getInstance(), "2").commit();
                    setExpertVisible(true);
                    setRlGiftView(false);
                    HomeActivity.stopAudioService();
                } else if (itemIndex == 2) {
                    ProfileResultData profileResultData = Functions.getData();
                    if (profileResultData != null) {
                        if (profileResultData.getView_pending_count() > 0) {
                            mCircleNavigationView.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mCircleNavigationView.hideBadgeAtIndex(2);
                                }
                            }, 1000);
                        }
                    }

                    fragmentManager.beginTransaction().replace(R.id.frame_container_details, CommunityFragment.getInstance(), "3").commit();
                    setExpertVisible(false);
                    setRlGiftView(false);
                    HomeActivity.stopAudioService();
                } else if (itemIndex == 3) {
                    setExpertVisible(false);
                    setRlGiftView(false);
                    fragmentManager.beginTransaction().replace(R.id.frame_container_details, AdhikFragment.getInstance(), "4").commit();
                    HomeActivity.stopAudioService();
                }

            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {
            }
        });
    }


    void setExpertVisible(boolean isVisible) {
        try {
            if (isVisible) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        llExpertView.setVisibility(View.VISIBLE);
                    }
                }, 600);
            } else {
                llExpertView.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void unSelectCropsDialog() {
        BottomSheetDialog dialog = new BottomSheetDialog(HomeActivity.this);
        View view = LayoutInflater.from(this).inflate(R.layout.unselected_crop_dialog, null);
        dialog.setContentView(view);

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                mCircleNavigationView.changeCurrentItem(0);
            }
        });

        RecyclerView rcvCrop = dialog.findViewById(R.id.rcvCrop);
        rcvCrop.setLayoutManager(new GridLayoutManager(this, 3));

        List<String> profileCropList = Functions.getSelectedCropIdList(sharedPreferences);
        List<MasterCropData> masterCropData = new ArrayList<MasterCropData>();
        if (masterCropList != null && masterCropList.size() > 0) {
            for (int j = 0; j < masterCropList.size(); j++) {
                if (!profileCropList.contains(masterCropList.get(j).getCrop_id())) {
                    masterCropData.add(new MasterCropData(masterCropList.get(j).getCrop_name(), masterCropList.get(j).getCrop_id(),
                            masterCropList.get(j).getCrop_image(), masterCropList.get(j).getDisplay_name(), "0"));
                }
            }
        }

        SelectCropAdapter cropAdapter = new SelectCropAdapter(masterCropData);
        rcvCrop.setAdapter(cropAdapter);

        TextView tvDarjKare = dialog.findViewById(R.id.tvDarjKare);
        tvDarjKare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cropId = cropAdapter.getSelectedCropId();
                addCropDialog(cropId);
                dialog.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        dialog.show();
    }


    public void openCropSelectionDialog() {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(HomeActivity.this);
        View view = LayoutInflater.from(this).inflate(R.layout.bottom_dialog_crop_view, null);
        bottomSheetDialog.setContentView(view);

        RelativeLayout relativeArrow = view.findViewById(R.id.relativeArrow);
        RecyclerView rcvCrop = view.findViewById(R.id.rcvCrop);
//        rcvCrop.setLayoutManager(new GridLayoutManager(this, 3));
        rcvCrop.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        List<String> profileCropList = Functions.getSelectedCropIdList(sharedPreferences);
        List<MasterCropData> masterCropData = new ArrayList<MasterCropData>();
        if (masterCropList != null && masterCropList.size() > 0) {
            for (int j = 0; j < masterCropList.size(); j++) {
                if (profileCropList.contains(masterCropList.get(j).getCrop_id())) {
                    if (masterCropList.get(j).getCrop_id().equalsIgnoreCase(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, "1"))) {
                        masterCropData.add(new MasterCropData(masterCropList.get(j).getCrop_name(), masterCropList.get(j).getCrop_id(),
                                masterCropList.get(j).getCrop_image(), masterCropList.get(j).getDisplay_name(), "1"));
                    } else {
                        masterCropData.add(new MasterCropData(masterCropList.get(j).getCrop_name(), masterCropList.get(j).getCrop_id(),
                                masterCropList.get(j).getCrop_image(), masterCropList.get(j).getDisplay_name(), "0"));
                    }
                }
            }
        } else {
            masterCropData.add(new MasterCropData("Maize", "1", "", "मक्का", "0"));
        }

        ImageView imgPrevios = view.findViewById(R.id.imgPrevios);
        imgPrevios.setVisibility(View.GONE);
        ImageView imgNext = view.findViewById(R.id.imgNext);
        imgNext.setVisibility(View.VISIBLE);

        rcvCrop.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (masterCropData.size() > 3) {
                    if (((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition() > 2) {
                        imgPrevios.setVisibility(View.VISIBLE);
                    } else {
                        imgPrevios.setVisibility(View.INVISIBLE);
                    }

                    if (((LinearLayoutManager) recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition() > (masterCropData.size() - 3)) {
                        imgNext.setVisibility(View.INVISIBLE);
                    } else {
                        imgNext.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        imgPrevios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (rcvCrop.getAdapter() == null)
                        return;

                    int position = getCurrentItem(rcvCrop);
                    Log.e("position:", "" + position);
                    if (position > 0)
                        setCurrentItem(rcvCrop, position - 1, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (rcvCrop.getAdapter() == null)
                        return;

                    int position = getCurrentItem(rcvCrop);
                    Log.e("position1:", "" + position);
                    setCurrentItem(rcvCrop, position + 1, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        if (masterCropData.size() > 2) {
            relativeArrow.setVisibility(View.VISIBLE);
        } else {
            relativeArrow.setVisibility(View.GONE);
        }

        CropAdapter cropAdapter = new CropAdapter(masterCropData);
        rcvCrop.setAdapter(cropAdapter);

        TextView tvAddCrop = view.findViewById(R.id.tvAddCrop);
        tvAddCrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unSelectCropsDialog();
                bottomSheetDialog.dismiss();
            }
        });

        TextView txtEnter = view.findViewById(R.id.txtEnter);
        txtEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cropId = cropAdapter.getSelectedCropId();
                if (!TextUtils.isEmpty(cropId)) {
                    if (!RongoApp.getSelectedCrop().equalsIgnoreCase(cropId)) {

                        SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                Constants.SharedPreferences_story_savdhani_count, "0");

                        if (InternetConnection.checkConnectionForFasl(HomeActivity.this)) {
                            List<CropsData> cropsData = Functions.getCropDataByID(cropId);
                            if (cropsData.size() > 0) {
                                String dateOfSowing = cropsData.get(0).getDate_of_sowing();
                                Log.i("hahaha1:-", "" + dateOfSowing);
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_CROP_ID, cropId);
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_SowingDate, dateOfSowing);
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_P_CROP_ID, cropsData.get(0).getP_crop_id());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Field_ID, cropsData.get(0).getField_id());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Sowing_Done, cropsData.get(0).getIs_sowing_done());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_season_end_crop_week, cropsData.get(0).getSeason_end_crop_week());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_crop_status, cropsData.get(0).getCrop_status());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_season_feedback, cropsData.get(0).getSeason_feedback());

                                if (sharedPreferences.getString(Constants.SharedPreference_Multi_Language_Info_status, "").equalsIgnoreCase("1")) {
                                    new loadAllData(cropId, dateOfSowing).execute("");
                                } else {
                                    if (cropId.equalsIgnoreCase(Constants.MAIZE) || cropId.equalsIgnoreCase(Constants.WHEAT)) {
                                        if (RongoApp.getSelectedState().equalsIgnoreCase(Constants.MAHARASHTRA_STATE)) {
                                            if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.HINDI)) {
                                                openLanguageHintDialog(cropId, dateOfSowing, LocaleManager.MARATHI);
                                            } else {
                                                new loadAllData(cropId, dateOfSowing).execute("");
                                            }
                                        } else {
                                            new loadAllData(cropId, dateOfSowing).execute("");
                                        }
                                    } else if (cropId.equalsIgnoreCase(Constants.RICE)) {
                                        new loadAllData(cropId, dateOfSowing).execute("");
                                    } else if (cropId.equalsIgnoreCase(Constants.COTTON) || cropId.equalsIgnoreCase(Constants.PEARMILLET)) {
                                        if (RongoApp.getSelectedState().equalsIgnoreCase(Constants.GUJARAT_STATE)) {
                                            if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.HINDI)) {
                                                openLanguageHintDialog(cropId, dateOfSowing, LocaleManager.GUJARATI);
                                            } else {
                                                new loadAllData(cropId, dateOfSowing).execute("");
                                            }
                                        } else {
                                            new loadAllData(cropId, dateOfSowing).execute("");
                                        }
                                    } else {
                                        new loadAllData(cropId, dateOfSowing).execute("");
                                    }
                                }
                            } else {
                                addCropDialog(cropId);
                            }
                        } else {
                            List<AddCropStatusData> cropsData = appDatabase.addCropStatusDao().getAllData(cropId);
                            if (cropsData.size() > 0) {
                                String dateOfSowing = cropsData.get(0).getSowingDate();
                                Log.i("hahaha1:-", "" + dateOfSowing);
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_CROP_ID, cropId);
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_SowingDate, dateOfSowing);
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_P_CROP_ID, cropsData.get(0).getPcropIds());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Field_ID, cropsData.get(0).getFieldId());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Sowing_Done, cropsData.get(0).getSowingDone());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_season_end_crop_week, cropsData.get(0).getSessionEndTotal());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_crop_status, cropsData.get(0).getStatus());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_season_feedback, cropsData.get(0).getSessionEnd());

                                if (sharedPreferences.getString(Constants.SharedPreference_Multi_Language_Info_status, "").equalsIgnoreCase("1")) {
                                    new loadAllData(cropId, dateOfSowing).execute("");
                                } else {
                                    if (cropId.equalsIgnoreCase(Constants.MAIZE) || cropId.equalsIgnoreCase(Constants.WHEAT)) {
                                        if (RongoApp.getSelectedState().equalsIgnoreCase(Constants.MAHARASHTRA_STATE)) {
                                            if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.HINDI)) {
                                                openLanguageHintDialog(cropId, dateOfSowing, LocaleManager.MARATHI);
                                            } else {
                                                new loadAllData(cropId, dateOfSowing).execute("");
                                            }
                                        } else {
                                            new loadAllData(cropId, dateOfSowing).execute("");
                                        }
                                    } else if (cropId.equalsIgnoreCase(Constants.RICE)) {
                                        new loadAllData(cropId, dateOfSowing).execute("");
                                    } else if (cropId.equalsIgnoreCase(Constants.COTTON) || cropId.equalsIgnoreCase(Constants.PEARMILLET)) {
                                        if (RongoApp.getSelectedState().equalsIgnoreCase(Constants.GUJARAT_STATE)) {
                                            if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.HINDI)) {
                                                openLanguageHintDialog(cropId, dateOfSowing, LocaleManager.GUJARATI);
                                            } else {
                                                new loadAllData(cropId, dateOfSowing).execute("");
                                            }
                                        } else {
                                            new loadAllData(cropId, dateOfSowing).execute("");
                                        }
                                    } else {
                                        new loadAllData(cropId, dateOfSowing).execute("");
                                    }
                                }
                            } else {
                                addCropDialog(cropId);
                            }
                        }

                        mCircleNavigationView.changeCurrentItem(0);
                    } else {
                        mCircleNavigationView.changeCurrentItem(mCircleNavigationView.lastSelectedItem);
                    }

                    bottomSheetDialog.dismiss();
                } else {
                    Toast.makeText(HomeActivity.this, getString(R.string.str_please_select_fasal), Toast.LENGTH_SHORT).show();
                }
            }
        });

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCircleNavigationView.changeCurrentItem(mCircleNavigationView.lastSelectedItem);
                bottomSheetDialog.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    int getCurrentItem(RecyclerView rcv) {
        return ((LinearLayoutManager) rcv.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
    }

    void setCurrentItem(RecyclerView rcv, int position, boolean smooth) {
        if (smooth)
            rcv.smoothScrollToPosition(position);
        else
            rcv.scrollToPosition(position);
    }

    class CropAdapter extends RecyclerView.Adapter<CropAdapter.ViewHolder> {
        List<MasterCropData> listdata;
        List<String> profileCropList = Functions.getSelectedCropIdList(sharedPreferences);

        public CropAdapter(List<MasterCropData> listdata) {
            this.listdata = listdata;
        }

        public String getSelectedCropId() {
            String cropId = "";
            for (int i = 0; i < listdata.size(); i++) {
                if (listdata.get(i).getStatus().equalsIgnoreCase("1")) {
                    cropId = listdata.get(i).getCrop_id();
                }
            }
            return cropId;
        }

        @NotNull
        @Override
        public CropAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.crop_list_item, parent, false);
            CropAdapter.ViewHolder viewHolder = new CropAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final CropAdapter.ViewHolder holder, final int position) {
            try {
                holder.tvTitle.setText(listdata.get(position).getDisplay_name());

                holder.llCropView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listdata.get(position).getStatus().equalsIgnoreCase("1")) {
                            listdata.get(position).setStatus("0");
                        } else {
                            for (int i = 0; i < listdata.size(); i++) {
                                if (listdata.get(position).getCrop_id().equalsIgnoreCase(listdata.get(i).getCrop_id())) {
                                    listdata.get(i).setStatus("1");
                                } else {
                                    listdata.get(i).setStatus("0");
                                }
                            }
                        }
                        notifyDataSetChanged();
                    }
                });

                if (listdata.get(position).getStatus().equalsIgnoreCase("1")) {
                    holder.llCropView.setBackground(homeActivity.getResources().getDrawable(R.drawable.rounded_layout_selected));
                    holder.ivTick.setVisibility(View.VISIBLE);
                    holder.ivTick.setImageResource(R.mipmap.c_tick);
                } else {
                    holder.llCropView.setBackground(getResources().getDrawable(R.drawable.rounded_layout_unselect));
                    holder.ivTick.setVisibility(View.GONE);

                    if (profileCropList.size() > 0) {
                        if (!profileCropList.contains(listdata.get(position).getCrop_id())) {
                            holder.llCropView.setBackground(getResources().getDrawable(R.drawable.rounded_layout_unselect));
                            holder.ivTick.setVisibility(View.VISIBLE);
                            holder.ivTick.setImageResource(R.mipmap.c_add);
                        } else {
                            holder.ivTick.setVisibility(View.VISIBLE);
                            holder.ivTick.setImageResource(R.mipmap.c_tick);
                            holder.llCropView.setBackground(getResources().getDrawable(R.drawable.rounded_layout_unselect));
                        }
                    } else {
                        List<AddCropStatusData> addCropStatusData = appDatabase.addCropStatusDao().getAllData(listdata.get(position).getCrop_id());
                        if (addCropStatusData.size() > 0) {
                            holder.ivTick.setVisibility(View.VISIBLE);
                            holder.ivTick.setImageResource(R.mipmap.c_tick);
                            holder.llCropView.setBackground(getResources().getDrawable(R.drawable.rounded_layout_unselect));
                        } else {
                            holder.llCropView.setBackground(getResources().getDrawable(R.drawable.rounded_layout_unselect));
                            holder.ivTick.setVisibility(View.VISIBLE);
                            holder.ivTick.setImageResource(R.mipmap.c_add);
                        }
                    }

                    if (RongoApp.getSessionEndCropId().equalsIgnoreCase(listdata.get(position).getCrop_id())) {
                        holder.llCropView.setBackground(getResources().getDrawable(R.drawable.rounded_layout_unselect));
                        holder.ivTick.setVisibility(View.VISIBLE);
                        holder.ivTick.setImageResource(R.mipmap.c_add);
                    }
                }

                if (!TextUtils.isEmpty(listdata.get(position).getCrop_image()) && InternetConnection.checkConnectionForFasl(HomeActivity.this)) {
                    GlideApp.with(HomeActivity.this).load(listdata.get(position).getCrop_image())
                            .apply(Functions.getPlaceholder(listdata.get(position).getCrop_id())).into(holder.ivCrop);
                } else {
                    if (listdata.get(position).getCrop_id().equalsIgnoreCase("1")) {
                        holder.ivCrop.setImageResource(R.mipmap.maize);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("2")) {
                        holder.ivCrop.setImageResource(R.mipmap.rice);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("3")) {
                        holder.ivCrop.setImageResource(R.mipmap.cotton);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("4")) {
                        holder.ivCrop.setImageResource(R.mipmap.wheat);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("5")) {
                        holder.ivCrop.setImageResource(R.mipmap.okra);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("6")) {
                        holder.ivCrop.setImageResource(R.mipmap.pearmillet);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("7")) {
                        holder.ivCrop.setImageResource(R.mipmap.corinder);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("8")) {
                        holder.ivCrop.setImageResource(R.mipmap.bittergourd);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("9")) {
                        holder.ivCrop.setImageResource(R.mipmap.bottlegourd);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("10")) {
                        holder.ivCrop.setImageResource(R.mipmap.watermelon);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("11")) {
                        holder.ivCrop.setImageResource(R.mipmap.groundnut);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("12")) {
                        holder.ivCrop.setImageResource(R.mipmap.soyabean);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("13")) {
                        holder.ivCrop.setImageResource(R.mipmap.caster);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return listdata.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvTitle;
            ImageView ivCrop, ivTick;
            RelativeLayout llCropView;

            public ViewHolder(View itemView) {
                super(itemView);
                this.llCropView = (RelativeLayout) itemView.findViewById(R.id.llCropView);
                this.tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
                this.ivCrop = (ImageView) itemView.findViewById(R.id.ivCrop);
                this.ivTick = (ImageView) itemView.findViewById(R.id.ivTick);
            }
        }
    }

    public void addCropDialog(String crop_id) {
        String[] is_sowing_done = {""};
        bottomSheetDialog = new BottomSheetDialog(this);
        View view = LayoutInflater.from(this).inflate(R.layout.bottom_add_crop_dialog, null);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setCanceledOnTouchOutside(false);

        AppCompatTextView txtOfflineSMS = view.findViewById(R.id.txtOfflineSMS);
        txtOfflineSMS.setVisibility(View.GONE);
        if (!InternetConnection.checkConnectionForFasl(HomeActivity.this)) {
            txtOfflineSMS.setVisibility(View.VISIBLE);
        }

        LinearLayout llSeasonValidation = view.findViewById(R.id.llSeasonValidation);
        llSeasonValidation.setVisibility(View.GONE);

        tvSeasonValidation = view.findViewById(R.id.tvSeasonValidation);
        tvSeasonValidationtxt = view.findViewById(R.id.tvSeasonValidationtxt);

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        TextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getSowingDateRewardPoint(sharedPreferences));

        LinearLayout llStatusBtn = view.findViewById(R.id.llStatusBtn);
        llStatusBtn.setVisibility(View.VISIBLE);
        LinearLayout llDateView = view.findViewById(R.id.llDateView);
        llDateView.setVisibility(View.GONE);

        TextView txtYourSowingText = view.findViewById(R.id.txtYourSowingText);
        AppCompatEditText edtSowingDate = view.findViewById(R.id.editTextSowingDate);

        TextView txtYes = view.findViewById(R.id.txtYes);
        TextView txtNo = view.findViewById(R.id.txtNo);
        TextView txtEnter = view.findViewById(R.id.txtEnter);

        txtYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                llStatusBtn.setVisibility(View.GONE);
                llDateView.setVisibility(View.VISIBLE);
                txtYourSowingText.setText(R.string.str_apni_buvay_ki_date_darj_kare);
                edtSowingDate.setHint(R.string.str_date_darj_kare);
                is_sowing_done[0] = "yes";
            }
        });

        txtNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                llStatusBtn.setVisibility(View.GONE);
                llDateView.setVisibility(View.VISIBLE);
                txtYourSowingText.setText(R.string.str_apni_expe_buvay_date_dark_kare);
                edtSowingDate.setHint(R.string.str_date_darj_kare);
                is_sowing_done[0] = "no";
            }
        });

        edtSowingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                openDatePicker(edtSowingDate, is_sowing_done[0], llSeasonValidation, crop_id);
            }
        });

        txtEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(crop_id)) {
                    if (!TextUtils.isEmpty(edtSowingDate.getText().toString().trim())) {
                        bottomSheetDialog.dismiss();
                        if (InternetConnection.checkConnection(HomeActivity.this)) {
                            txtOfflineSMS.setVisibility(View.GONE);
                            progressBarDialog.showProgressDialogWithTitle("1");
                            startNewSession(edtSowingDate, crop_id, "0");
                        } else {
                            txtOfflineSMS.setVisibility(View.VISIBLE);
                            addOfflineCrop(crop_id, edtSowingDate.getText().toString().trim(), true);
                        }
                    } else {
                        Toast.makeText(HomeActivity.this, getString(R.string.str_apni_buvay_ki_date_darj_kare), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(HomeActivity.this, getString(R.string.str_please_select_fasal), Toast.LENGTH_SHORT).show();
                }

            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    public void startNewSession(EditText editTextSowingDate, String cropId, String sessionEndCropId) {
        String is_sowing_done = "", s = "0000-00-00 00:00:00", es = "0000-00-00 00:00:00";
        String dateOfSowing = editTextSowingDate.getText().toString().trim();
        long dTime = Functions.covertToMili("dd-MM-yyyy", dateOfSowing);
        if (System.currentTimeMillis() > dTime) {
            is_sowing_done = "yes";
            s = Functions.converOneDateFormatToOther("dd-MM-yyyy", "yyyy-MM-dd hh:mm:ss", dateOfSowing);
        } else {
            is_sowing_done = "no";
            es = Functions.converOneDateFormatToOther("dd-MM-yyyy", "yyyy-MM-dd hh:mm:ss", dateOfSowing);
        }

        RetrofitClient.getInstance().getApi().startNewCropSeason(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                SharedPreferencesUtils.getUserId(this), sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                sessionEndCropId, cropId, is_sowing_done, s, es).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_CROP_ID, "" + cropId);
                        String r = response.body().string();
                        if (!TextUtils.isEmpty(r)) {
                            JSONObject jsonObject = new JSONObject(r);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {

                                    JSONObject data = jsonObject.optJSONObject("data");
                                    if (data != null && data.length() > 0) {
                                        String p_crop_id = data.optString("p_crop_id");
                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_P_CROP_ID, "" + p_crop_id);

                                        getProfile();
                                        saveSowingInfo(editTextSowingDate);
                                        updateWeeklyCropInfo(editTextSowingDate);
                                    }

                                    if (RongoApp.getSessionEndCropId().equalsIgnoreCase(cropId)) {
                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SessionEndCropID, "");
                                    }

                                    if (sharedPreferences.getString(Constants.SharedPreference_Multi_Language_Info_status, "").equalsIgnoreCase("1")) {
                                        new loadAllData(cropId, dateOfSowing).execute("");
                                    } else {
                                        if (cropId.equalsIgnoreCase(Constants.MAIZE) || cropId.equalsIgnoreCase(Constants.WHEAT)) {

                                            if (RongoApp.getSelectedState().equalsIgnoreCase(Constants.MAHARASHTRA_STATE)) {
                                                if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.HINDI)) {
                                                    openLanguageHintDialog(cropId, dateOfSowing, LocaleManager.MARATHI);
                                                } else {
                                                    new loadAllData(cropId, dateOfSowing).execute("");
                                                }
                                            } else {
                                                new loadAllData(cropId, dateOfSowing).execute("");
                                            }

//                                            new loadAllData(cropId, dateOfSowing).execute("");
                                        } else if (cropId.equalsIgnoreCase(Constants.RICE)) {
                                            new loadAllData(cropId, dateOfSowing).execute("");
                                        } else if (cropId.equalsIgnoreCase(Constants.COTTON) || cropId.equalsIgnoreCase(Constants.PEARMILLET)) {
                                            if (RongoApp.getSelectedState().equalsIgnoreCase(Constants.GUJARAT_STATE)) {
                                                if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.HINDI)) {
                                                    openLanguageHintDialog(cropId, dateOfSowing, LocaleManager.GUJARATI);
                                                } else {
                                                    new loadAllData(cropId, dateOfSowing).execute("");
                                                }
                                            } else {
                                                new loadAllData(cropId, dateOfSowing).execute("");
                                            }
                                        } else {
                                            new loadAllData(cropId, dateOfSowing).execute("");
                                        }
                                    }

                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                progressBarDialog.hideProgressDialogWithTitle();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        });
    }


    public void endCurrentSession(String crop_id, String sessionEndCropId) {
        RetrofitClient.getInstance().getApi().endCurrentCropSeason(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                SharedPreferencesUtils.getUserId(this), sessionEndCropId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response != null && response.isSuccessful()) {
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SessionEndCropID, "");
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_CROP_ID, "" + crop_id);
                        new loadAllData(crop_id, "").execute("");
                    }
                    progressBarDialog.hideProgressDialogWithTitle();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        });
    }

    public class loadAllData extends AsyncTask<String, String, String> {
        ProgressDialog pDialog;
        boolean isLoad = false;
        String crop_id = "", date = "";

        public loadAllData(String crop_id, String date) {
            this.crop_id = crop_id;
            this.date = date;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            RongoApp.getCropWeekDaysFunction();
            if (!RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.HINDI)) {
                RongoApp.setNewLocale(HomeActivity.this, LocaleManager.HINDI);
                isLoad = false;
            } else {
                isLoad = true;
            }

            if (isLoad) {
                pDialog = new ProgressDialog(HomeActivity.this);
                pDialog.setMessage(getString(R.string.str_wait_msg));
                pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
                pDialog.show();
            }
        }

        protected String doInBackground(String... args) {
            if (isLoad) {
                if (InternetConnection.checkConnectionForFasl(HomeActivity.this) &&
                        !InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                mCircleNavigationView.setCenterView(RongoApp.getSelectedCrop());
                                RongoApp.getFertilizerInfo(false);
                                RongoApp.getAllCropWeeklyQuestion(false);
                                RongoApp.getDailyQuizData(false);
                                RongoApp.getMasterDropDownData(true);
                                RongoApp.getWeeklyAdvisory();
                                RongoApp.getWeeklyWeather();
                                RongoApp.getSessionEndData();
                                getContestData();
                                getProfileNew();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            }
            return null;
        }

        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            try {
                if (!InternetConnection.checkConnectionForFasl(homeActivity)) {
                    addOfflineCrop(crop_id, date, false);

                    if (progressBarDialog != null) {
                        progressBarDialog.hideProgressDialogWithTitle();
                    }
                    if (pDialog != null) {
                        if (pDialog.isShowing()) {
                            pDialog.dismiss();   //This is line 624!
                        }
                    }
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (isLoad) {
                                Intent intent = new Intent();
                                intent.setAction(Constants.BROADCAST_RECEIVER.UPDATE_CROP_INFO);
                                sendBroadcast(intent);

                                mCircleNavigationView.changeCurrentItem(0);
                                setBuvayDialogClose();

                                if (progressBarDialog != null) {
                                    progressBarDialog.hideProgressDialogWithTitle();
                                }
                                if (pDialog != null) {
                                    if (pDialog.isShowing()) {
                                        pDialog.dismiss();   //This is line 624!
                                    }
                                }
                            }
                        }
                    }, 4500);
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (progressBarDialog != null) {
                    progressBarDialog.hideProgressDialogWithTitle();
                }
                if (pDialog != null) {
                    pDialog.dismiss();
                    pDialog.cancel();
                }
            }
        }
    }


    void openLanguageHintDialog(String cropId, String dateOfSowing, String lang) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
        View view = LayoutInflater.from(this).inflate(R.layout.bottom_dialog_language_view, null);
        bottomSheetDialog.setContentView(view);

        TextView txtTitle = bottomSheetDialog.findViewById(R.id.txtTitle);
        if (lang.equalsIgnoreCase(LocaleManager.GUJARATI)) {
            txtTitle.setText(homeActivity.getResources().getString(R.string.str_crop_lang_gu_text));
        } else if (lang.equalsIgnoreCase(LocaleManager.MARATHI)) {
            txtTitle.setText(homeActivity.getResources().getString(R.string.str_crop_lang_mr_text));
        }

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                    new loadAllData(cropId, dateOfSowing).execute("");
                } else {
                    updateDataOffline();
                }
                setBuvayDialogClose();
            }
        });

        TextView txtNo = bottomSheetDialog.findViewById(R.id.txtNo);
        txtNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Multi_Language_Info_status, "1");
                bottomSheetDialog.dismiss();
                if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                    new loadAllData(cropId, dateOfSowing).execute("");
                } else {
                    updateDataOffline();
                }
                setBuvayDialogClose();
            }
        });

        TextView txtYes = bottomSheetDialog.findViewById(R.id.txtYes);
        txtYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Multi_Language_Info_status, "1");

                if (lang.equalsIgnoreCase(LocaleManager.GUJARATI)) {
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Default_Language, LocaleManager.GUJARATI);
                    RongoApp.setNewLocale(HomeActivity.this, LocaleManager.GUJARATI);
                } else if (lang.equalsIgnoreCase(LocaleManager.MARATHI)) {
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Default_Language, LocaleManager.MARATHI);
                    RongoApp.setNewLocale(HomeActivity.this, LocaleManager.MARATHI);
                }

                bottomSheetDialog.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    void setBuvayDialogClose() {
        try {
            setInitView();
            if (BuvayDateDialog != null) {
                BuvayDateDialog.hide();
                BuvayDateDialog.dismiss();
                BuvayDateDialog.cancel();
            }
            if (bottomSheetDialog != null) {
                bottomSheetDialog.dismiss();
                bottomSheetDialog.cancel();
                bottomSheetDialog.hide();
            }

            if (progressBarDialog != null) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void setRlGiftView(boolean isView) {
        if (rlGiftView != null) {
            if (isView) {
                if (Functions.getContestAvailable(sharedPreferences).equalsIgnoreCase("1")) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            rlGiftView.setVisibility(View.VISIBLE);
                        }
                    }, 1000);
                } else {
                    rlGiftView.setVisibility(View.GONE);
                }
            } else {
                rlGiftView.setVisibility(View.GONE);
            }
        }
    }

    void getContestData() {
        RetrofitClient.getInstance().getApi().getContestData(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), SharedPreferencesUtils.getUserId(this),
                Functions.getDeviceid(this), sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0")).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String r = response.body().string();
                        if (!TextUtils.isEmpty(r)) {
                            JSONObject jsonObject = new JSONObject(r);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject data = jsonObject.optJSONObject("data");
                                    if (data != null && data.length() > 0) {
                                        JSONArray jsonArray = data.optJSONArray("result");
                                        if (jsonArray != null && jsonArray.length() > 0) {
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                                    Constants.SharedPreferences_CONTEST, "" + jsonArray);

                                            setRlGiftView(true);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    public static void stopAudioService() {
        try {
            TTSAudioPlayer.getInstance().stopAll();
            if (tts != null && tts.isSpeaking()) {
                tts.stop();
            }
            if (tts != null) {
                tts.shutdown();
            }
            tts = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        Log.e("HomeActivity", "onDestroy");
        deleteCache(HomeActivity.this);
        stopAudioService();
        super.onDestroy();
        unregisterReceiver(UPDATE_TOOLTIP_VIEW);
        System.gc();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("HomeActivity", "onStart");
        fragmentManager = getSupportFragmentManager();
        DroidNet.getInstance().addInternetConnectivityListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopAudioService();
        Log.e("HomeActivity", "onStop");
        DroidNet.getInstance().removeInternetConnectivityChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("HomeActivity", "onPause");
        merlin.unbind();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myLocalBroadcastReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(myPragatiListenr);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mFeedbackListener);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mLuckyDrawLister);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mJannkariListener);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mQuizListener);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mWeeklyCropInfoListener);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mCommentListener);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mLikeListener);
    }

    @Override
    protected void onResume() {
        try {
            Log.e("HomeActivity", "onResume");
            HomeActivity.stopAudioService();
            super.onResume();
            setRlGiftView(true);
            if (merlin == null) {
                merlin = new Merlin.Builder().withConnectableCallbacks().build(this);
                merlin.bind();
            }

            IntentFilter intentFilters = new IntentFilter(Constants.WEEKLY_CROP_INFO_QUE);
            LocalBroadcastManager.getInstance(this).registerReceiver(mWeeklyCropInfoListener, intentFilters);

            IntentFilter intentFilter = new IntentFilter(Constants.BROADCAST_QUE);
            LocalBroadcastManager.getInstance(this).registerReceiver(myLocalBroadcastReceiver, intentFilter);

            IntentFilter intentFilterPragati = new IntentFilter(Constants.PRAGATI_QUE);
            LocalBroadcastManager.getInstance(this).registerReceiver(myPragatiListenr, intentFilterPragati);

            IntentFilter ifFeedback = new IntentFilter(Constants.FEEDBACK_QUE);
            LocalBroadcastManager.getInstance(this).registerReceiver(mFeedbackListener, ifFeedback);

            IntentFilter iLuckyDraw = new IntentFilter(Constants.LUCKY_DRAW_QUE);
            LocalBroadcastManager.getInstance(this).registerReceiver(mLuckyDrawLister, iLuckyDraw);

            IntentFilter iJankari = new IntentFilter(Constants.JANKARI_QUE);
            LocalBroadcastManager.getInstance(this).registerReceiver(mJannkariListener, iJankari);

            IntentFilter iOfflineQuiz = new IntentFilter(Constants.QUIZ_QUE);
            LocalBroadcastManager.getInstance(this).registerReceiver(mQuizListener, iOfflineQuiz);

            IntentFilter iOfflineComment = new IntentFilter(Constants.COMMENT_QUE);
            LocalBroadcastManager.getInstance(this).registerReceiver(mCommentListener, iOfflineComment);

            IntentFilter iOfflineLike = new IntentFilter(Constants.LIKE_QUE);
            LocalBroadcastManager.getInstance(this).registerReceiver(mLikeListener, iOfflineLike);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    String getDailyLoginReward() {
        String userdata = "";
        try {
            List<DailyRewardData> dailyRewardData = appDatabase.dailyRewardDao().getAll();
            if (dailyRewardData.size() > 0) {
                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < dailyRewardData.size(); i++) {
                    JSONObject obj = new JSONObject();
                    obj.put("reward_points", dailyRewardData.get(i).getReward_points());
                    obj.put("metadata", dailyRewardData.get(i).getMetadata());
                    obj.put("created_on", dailyRewardData.get(i).getCreated_on());
                    jsonArray.put(obj);
                }
                userdata = jsonArray.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userdata;
    }

    void updateRewardPoints() {
        try {
            RetrofitClient.getInstance().getApi().updateRewardPoints(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    SharedPreferencesUtils.getUserId(this), getDailyLoginReward()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    appDatabase.dailyRewardDao().nukeTable();
                    Log.e("DailyLogin", "" + response);
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void updateOfflineReward(List<OfflineRewardData> offlineRewardData) {
        try {
            RetrofitClient.getInstance().getApi().updateContestResponse(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    SharedPreferencesUtils.getUserId(HomeActivity.this), offlineRewardData.get(0).getContestID(), "",
                    getSpinWheelMetaData(offlineRewardData.get(0).getRewardID(), offlineRewardData.get(0).getText(), offlineRewardData.get(0).getIs_true(),
                            offlineRewardData.get(0).getValue())).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        try {
                            String r = response.body().string();
                            if (!TextUtils.isEmpty(r)) {
                                JSONObject jsonObject = new JSONObject(r);
                                if (jsonObject != null && jsonObject.length() > 0) {
                                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                        JSONObject data = jsonObject.optJSONObject("data");
                                        if (data != null && data.length() > 0) {
                                            JSONObject resultData = data.optJSONObject("result");
                                            rewardId = resultData.optString("reward_id");
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
//        @TODO onInternetConnectivityChanged
        Log.i("HomeActivity Net", "" + isConnected);
        if (isConnected) {
            try {
                if (!TextUtils.isEmpty(RongoApp.getUserId())) {
                    if (merlin == null) {
                        merlin = new Merlin.Builder().withConnectableCallbacks().build(this);
                        merlin.bind();
                    }

                    setOfflineDataUploadToServerService();

                    if (appDatabase != null && sharedPreferences != null) {
                        if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_Submit_SessionEnd_DATA, ""))) {
                            submitSessionEndData();
                        }
                        setOfflineInvitePeopleAPI();
                        setOfflineSettingUpdateAPI();
                        setOfflineKhetLocation();
                        List<OfflineRewardData> offlineRewardData = appDatabase.offlineRewardDataDao().getAllReward();
                        if (offlineRewardData.size() > 0) {
                            updateOfflineReward(offlineRewardData);
                        }
                        List<DailyRewardData> dailyRewardData = Objects.requireNonNull(appDatabase).dailyRewardDao().getAll();
                        if (dailyRewardData != null && dailyRewardData.size() > 0) {
                            if (isRewardApi) {
                                isRewardApi = false;
                                updateRewardPoints();
                            }
                        }
                    }
                } else {
//                    getRequestProtocol();
                    Intent i = new Intent(HomeActivity.this, OTPActivity.class);
                    i.putExtra("otp", "");
                    i.putExtra("mobileNumber", "");
                    startActivity(i);
                    finishAffinity();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (appDatabase != null && sharedPreferences != null) {
                ProfileResultData profileResultData = Functions.getData();
                if (profileResultData != null) {
                    SeasonData seasonData = profileResultData.getSeasonData();
                    if (seasonData != null) {
                        if (seasonData.getCropsDataList() != null && seasonData.getCropsDataList().size() > 0) {
                            List<CropsData> cropsData = seasonData.getCropsDataList();
                            if (cropsData != null && cropsData.size() > 0) {
                                for (int i = 0; i < cropsData.size(); i++) {
                                    String cropIdAlready = cropsData.get(i).getCrop_id();

                                    List<AddCropStatusData> addCropStatus = appDatabase.addCropStatusDao().getAllData(cropIdAlready);
                                    AddCropStatusData addCropStatusData = new AddCropStatusData();
                                    addCropStatusData.setIds(new Random().nextInt());
                                    addCropStatusData.setPcropIds(cropsData.get(i).getP_crop_id());
                                    addCropStatusData.setCropId(cropIdAlready);
                                    addCropStatusData.setSowingDone(cropsData.get(i).getIs_sowing_done());
                                    addCropStatusData.setSowingExpDate(cropsData.get(i).getExpected_date_of_sowing());
                                    addCropStatusData.setSessionId("0");
                                    addCropStatusData.setFieldId("0");
                                    addCropStatusData.setSessionEndTotal("" + (cropsData.get(i).getCropWeekDetailsLis().size() + 1));
                                    addCropStatusData.setSowingDate(cropsData.get(i).getDate_of_sowing());
                                    addCropStatusData.setStatus("0");

                                    if (addCropStatus.size() > 0) {
                                        for (int k = 0; k < addCropStatus.size(); k++) {
                                            if (!addCropStatus.get(k).getCropId().equalsIgnoreCase(cropIdAlready)) {
                                                appDatabase.addCropStatusDao().insertAll(addCropStatusData);
                                            }
                                        }
                                    } else {
                                        appDatabase.addCropStatusDao().insertAll(addCropStatusData);
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
    }

    void setOfflineDataUploadToServerService() {
        try {
            if (merlin != null) {
                Log.e("HomeActivity", "merlin Start");
                merlin.registerConnectable(new Connectable() {
                    @Override
                    public void onConnect() {
                        List<OfflineData> offlineData = appDatabase.offlineDao().getAllData();
                        if (offlineData != null && offlineData.size() > 0) {
                            Intent mIntent = new Intent(HomeActivity.this, FileUploadService.class);
                            mIntent.putExtra(Constants.OFFLINE_DATA, (Serializable) offlineData);
                            FileUploadService.enqueueWork(HomeActivity.this, mIntent);
                        }

                        List<OfflineGrowthData> offlineGrowthData = appDatabase.offlineGrowthDao().getAllData();
                        if (offlineGrowthData != null && offlineGrowthData.size() > 0) {
                            Intent mIntent = new Intent(HomeActivity.this, FileUploadINGrowthService.class);
                            mIntent.putExtra(Constants.OFFLINE_GROWTH_DATA, (Serializable) offlineGrowthData);
                            FileUploadINGrowthService.enqueueWork(HomeActivity.this, mIntent);
                        }

                        List<OfflineWeeklyCropInfoData> weeklyCropInfoData = appDatabase.offlineWeeklyCropInfoDao().getWeeklyInfo();
                        if (weeklyCropInfoData != null && weeklyCropInfoData.size() > 0) {
                            Intent mIntent = new Intent(HomeActivity.this, WeeklyCropInfoService.class);
                            mIntent.putExtra(Constants.WEEKLY_CROP_INFO_DATA, (Serializable) weeklyCropInfoData);
                            WeeklyCropInfoService.enqueueWork(HomeActivity.this, mIntent);
                        }

                        List<FeedbackData> feedbackData = appDatabase.feedbackDao().getAllData();
                        if (feedbackData != null && feedbackData.size() > 0) {
                            Intent mIntent = new Intent(HomeActivity.this, FeedBackService.class);
                            mIntent.putExtra(Constants.FEEDBACK_DATA, (Serializable) feedbackData);
                            FeedBackService.enqueueWork(HomeActivity.this, mIntent);
                        }

                        List<LuckyDrawData> luckyDrawData = appDatabase.luckyDrawOfflineDao().getAllData();
                        if (luckyDrawData != null && luckyDrawData.size() > 0) {
                            Intent mIntent = new Intent(HomeActivity.this, FileUploadLuckyDrawService.class);
                            mIntent.putExtra(Constants.LUCKY_DRAW_DATA, (Serializable) luckyDrawData);
                            FileUploadLuckyDrawService.enqueueWork(HomeActivity.this, mIntent);
                        }

                        List<JankariFeedBackData> jankariFeedBackData = appDatabase.jankariFeedbackDao().getAllData();
                        if (jankariFeedBackData != null && jankariFeedBackData.size() > 0) {
                            Intent mIntent = new Intent(HomeActivity.this, JankariFeedbackService.class);
                            mIntent.putExtra(Constants.JANKARI_FEEDBACK_DATA, (Serializable) jankariFeedBackData);
                            JankariFeedbackService.enqueueWork(HomeActivity.this, mIntent);
                        }

                        List<OfflineQuizData> offlineQuizData = appDatabase.offlineQuizDao().getAllQuizData();
                        if (offlineQuizData != null && offlineQuizData.size() > 0) {
                            Intent mIntent = new Intent(HomeActivity.this, OfflineQuizService.class);
                            mIntent.putExtra(Constants.OFFLINE_QUIZ_DATA, (Serializable) offlineQuizData);
                            OfflineQuizService.enqueueWork(HomeActivity.this, mIntent);
                        }

                        List<OfflineCommentData> offlineCommentData = appDatabase.offlineCommentDao().getAllComment();
                        if (offlineCommentData != null && offlineCommentData.size() > 0) {
                            Intent mIntent = new Intent(HomeActivity.this, OfflineCommentService.class);
                            mIntent.putExtra(Constants.OFFLINE_COMMENT_DATA, (Serializable) offlineCommentData);
                            OfflineCommentService.enqueueWork(HomeActivity.this, mIntent);
                        }

                        List<OfflineLikeData> offlineLikeData = appDatabase.offlineLikeDao().getAllLike();
                        if (offlineLikeData != null && offlineLikeData.size() > 0) {
                            Intent mIntent = new Intent(HomeActivity.this, OfflineLikeService.class);
                            mIntent.putExtra(Constants.OFFLINE_LIKE_DATA, (Serializable) offlineLikeData);
                            OfflineLikeService.enqueueWork(HomeActivity.this, mIntent);
                        }

                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_container_details);
        if (!(fragment instanceof IOnBackPressed) || !((IOnBackPressed) fragment).onBackPressed()) {
            super.onBackPressed();
        }
    }

    void submitSessionEndData() {
        RetrofitClient.getInstance().getApi().submitSeasonEndFeedback(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                SharedPreferencesUtils.getUserId(this), sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_Submit_SessionEnd_DATA, "")).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SessionEndCropID, RongoApp.getSelectedCrop());
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Submit_SessionEnd_DATA, "");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    public void setOfflineInvitePeopleAPI() {
        try {
            List<UserInvite> usersInvites = new ArrayList<>();
            usersInvites = appDatabase.userInviteDao().getAll();
            if (usersInvites != null && usersInvites.size() > 0) {
//                Log.e("App Invite Size:", "" + usersInvites.size());
                offlineInvitePeople(usersInvites);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void offlineInvitePeople(final List<UserInvite> usersInvites) {
        final int[] i = {0};
        RetrofitClient.getInstance().getApi().invitePeople(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(this), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                SharedPreferencesUtils.getUserId(this), usersInvites.get(i[0]).getUserName(), usersInvites.get(i[0]).getPhoneNumber()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String str = response.body().string();
                        if (!TextUtils.isEmpty(str)) {
                            JSONObject jsonObject = new JSONObject(str);
                            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
//                                Log.e("App Delete id", "" + usersInvites.get(i[0]).getUid());
                                appDatabase.userInviteDao().delete(usersInvites.get(i[0]).getUid());

                                usersInvites.remove(i[0]);
                                if (usersInvites.size() > 0) {
                                    offlineInvitePeople(usersInvites);
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    public void setOfflineSettingUpdateAPI() {
        try {
            RongoApp.getCropWeekDaysFunction();
            List<ProfileOfflineData> profileOfflineData = new ArrayList<>();
            profileOfflineData = appDatabase.ProfileOfflineDao().getAll();
            if (profileOfflineData != null && profileOfflineData.size() > 0) {
//                Log.e("profileOffline Size", "" + profileOfflineData.size());
                offlineSettingsUpdateAPI(profileOfflineData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void offlineSettingsUpdateAPI(final List<ProfileOfflineData> profileOfflineData) {
        final int[] i = {0};
        RetrofitClient.getInstance().getApi().updateSetting(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(this),
                sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                SharedPreferencesUtils.getUserId(this),
                profileOfflineData.get(i[0]).getFirstName(), profileOfflineData.get(i[0]).getLastName(),
                profileOfflineData.get(i[0]).getPhoneNumber(), profileOfflineData.get(i[0]).getCity(),
                profileOfflineData.get(i[0]).getState(), "", "", "",
                "", "", "", "",
                profileOfflineData.get(i[0]).getFieldId(), "acre", profileOfflineData.get(i[0]).getFieldSize(),
                profileOfflineData.get(i[0]).getP_crop_id(), profileOfflineData.get(i[0]).getIs_sowing_done(),
                profileOfflineData.get(i[0]).getSowingDate(), profileOfflineData.get(i[0]).getExpectedSowingDate(),
                profileOfflineData.get(i[0]).getSeedType()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String str = response.body().string();
                        if (!TextUtils.isEmpty(str)) {
                            JSONObject jsonObject = new JSONObject(str);
                            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_SowingDate, profileOfflineData.get(i[0]).getSowingDate());

                                Log.v("App delete profile id", "" + profileOfflineData.get(i[0]).getUid());
                                appDatabase.ProfileOfflineDao().delete(profileOfflineData.get(i[0]).getUid());

                                profileOfflineData.remove(i[0]);
                                if (profileOfflineData.size() > 0) {
                                    offlineSettingsUpdateAPI(profileOfflineData);
                                }

                                if (!InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                    getProfile();
                                    RongoApp.getWeeklyWeather();
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void getProfile() {
        RetrofitClient.getInstance().getApi().getProfile(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                RongoApp.getAppVersion(), Functions.getDeviceid(this),
                sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                SharedPreferencesUtils.getUserId(this)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String r_str = response.body().string();
                        if (!TextUtils.isEmpty(r_str)) {
                            JSONObject jb = new JSONObject(r_str);
                            if (jb != null && jb.length() > 0) {
                                new InsertHomeFeedData(jb).insertData();
                            }
                        }

                        if (InternetConnection.checkConnectionForFasl(homeActivity) && InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                            Intent intent = new Intent();
                            intent.setAction(Constants.BROADCAST_RECEIVER.UPDATE_CROP_INFO);
                            sendBroadcast(intent);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    public void setOfflineKhetLocation() {
        try {
            List<KhetOfflineData> khetOfflineData = new ArrayList<KhetOfflineData>();
            khetOfflineData = appDatabase.khetOfflineDao().getAll();
            if (khetOfflineData != null && khetOfflineData.size() > 0) {
                updateFieldLocation(khetOfflineData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void updateFieldLocation(List<KhetOfflineData> khetOfflineData) {
        RetrofitClient.getInstance().getApi().updateFieldLocation(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(this),
                sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), SharedPreferencesUtils.getUserId(this),
                khetOfflineData.get(0).getSeason_id(), khetOfflineData.get(0).getP_crop_id(), khetOfflineData.get(0).getFieldId(),
                khetOfflineData.get(0).getLatitude() + "", khetOfflineData.get(0).getLongitude() + "",
                khetOfflineData.get(0).getKhet_address()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                appDatabase.khetOfflineDao().nukeTable();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.v("updateFieldLocation", t.getMessage());
            }
        });
    }

    private BroadcastReceiver mLikeListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                int count = 0;
                if (intent.hasExtra(Constants.QUE_LIKE_COUNT)) {
                    count = intent.getIntExtra(Constants.QUE_LIKE_COUNT, 0);
                }

                List<OfflineLikeData> offlineLikeData = new ArrayList<OfflineLikeData>();
                if (intent.hasExtra(Constants.OFFLINE_LIKE_DATA)) {
                    offlineLikeData = (List<OfflineLikeData>) intent.getSerializableExtra(Constants.OFFLINE_LIKE_DATA);
                }

                if (count == offlineLikeData.size()) {
                    appDatabase.offlineLikeDao().nukeTable();
                }
            }
        }
    };

    private BroadcastReceiver mCommentListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                int count = 0;
                if (intent.hasExtra(Constants.QUE_COMMENT_COUNT)) {
                    count = intent.getIntExtra(Constants.QUE_COMMENT_COUNT, 0);
                }

                List<OfflineCommentData> offlineCommentData = new ArrayList<OfflineCommentData>();
                if (intent.hasExtra(Constants.OFFLINE_COMMENT_DATA)) {
                    offlineCommentData = (List<OfflineCommentData>) intent.getSerializableExtra(Constants.OFFLINE_COMMENT_DATA);
                }

                if (count == offlineCommentData.size()) {
                    appDatabase.offlineCommentDao().nukeTable();
                }
            }
        }
    };

    private BroadcastReceiver mWeeklyCropInfoListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                int count = 0;
                if (intent.hasExtra(Constants.QUE_WEEKLY_COUNT)) {
                    count = intent.getIntExtra(Constants.QUE_WEEKLY_COUNT, 0);
                }

                List<OfflineWeeklyCropInfoData> weeklyCropInfoData = new ArrayList<OfflineWeeklyCropInfoData>();
                if (intent.hasExtra(Constants.WEEKLY_CROP_INFO_DATA)) {
                    weeklyCropInfoData = (List<OfflineWeeklyCropInfoData>) intent.getSerializableExtra(Constants.WEEKLY_CROP_INFO_DATA);
                }

                if (count == weeklyCropInfoData.size()) {
                    appDatabase.offlineWeeklyCropInfoDao().nukeTable();
                }
            }
        }
    };

    private BroadcastReceiver mQuizListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                int count = 0;
                if (intent.hasExtra(Constants.QUE_QUIZ_COUNT)) {
                    count = intent.getIntExtra(Constants.QUE_QUIZ_COUNT, 0);
                }

                List<OfflineQuizData> offlineQuizData = new ArrayList<OfflineQuizData>();
                if (intent.hasExtra(Constants.OFFLINE_QUIZ_DATA)) {
                    offlineQuizData = (List<OfflineQuizData>) intent.getSerializableExtra(Constants.OFFLINE_QUIZ_DATA);
                }

                if (count == offlineQuizData.size()) {
                    appDatabase.offlineQuizDao().nukeTable();
                }
            }
        }
    };

    private BroadcastReceiver mJannkariListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                int count = 0;
                if (intent.hasExtra(Constants.QUE_JANKARI_COUNT)) {
                    count = intent.getIntExtra(Constants.QUE_JANKARI_COUNT, 0);
                }

                List<JankariFeedBackData> jankariFeedBackData = new ArrayList<>();
                if (intent.hasExtra(Constants.JANKARI_FEEDBACK_DATA)) {
                    jankariFeedBackData = (List<JankariFeedBackData>) intent.getSerializableExtra(Constants.JANKARI_FEEDBACK_DATA);
                }

                if (count == jankariFeedBackData.size()) {
                    appDatabase.jankariFeedbackDao().nukeTable();
                }
            }
        }
    };


    private BroadcastReceiver myLocalBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                int count = 0;
                if (intent.hasExtra(Constants.QUE_OFFLINE_COUNT)) {
                    count = intent.getIntExtra(Constants.QUE_OFFLINE_COUNT, 0);
                }

                List<OfflineData> offlineData = new ArrayList<>();
                if (intent.hasExtra(Constants.OFFLINE_DATA)) {
                    offlineData = (List<OfflineData>) intent.getSerializableExtra(Constants.OFFLINE_DATA);
                }

                if (count == offlineData.size()) {
                    appDatabase.offlineDao().nukeTable();
                }
            }
        }
    };


    private BroadcastReceiver myPragatiListenr = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                int count = 0;
                if (intent.hasExtra(Constants.QUE_PRAGATI_COUNT)) {
                    count = intent.getIntExtra(Constants.QUE_PRAGATI_COUNT, 0);
                }

                List<OfflineGrowthData> offlineData = new ArrayList<>();
                if (intent.hasExtra(Constants.OFFLINE_GROWTH_DATA)) {
                    offlineData = (List<OfflineGrowthData>) intent.getSerializableExtra(Constants.OFFLINE_GROWTH_DATA);
                }

                if (count == offlineData.size()) {
                    appDatabase.offlineGrowthDao().nukeTable();
                }
            }
        }
    };

    private BroadcastReceiver mFeedbackListener = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                int count = 0;
                if (intent.hasExtra(Constants.QUE_FEEDBACK_COUNT)) {
                    count = intent.getIntExtra(Constants.QUE_FEEDBACK_COUNT, 0);
                }

                List<FeedbackData> feedbackData = new ArrayList<>();
                if (intent.hasExtra(Constants.FEEDBACK_DATA)) {
                    feedbackData = (List<FeedbackData>) intent.getSerializableExtra(Constants.FEEDBACK_DATA);
                }

                if (count == feedbackData.size()) {
                    appDatabase.feedbackDao().nukeTable();
                }
            }

        }
    };

    private BroadcastReceiver mLuckyDrawLister = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                int count = 0;
                if (intent.hasExtra(Constants.QUE_LUCKY_DRAW_COUNT)) {
                    count = intent.getIntExtra(Constants.QUE_LUCKY_DRAW_COUNT, 0);
                }

                List<LuckyDrawData> luckyDrawData = new ArrayList<>();
                if (intent.hasExtra(Constants.LUCKY_DRAW_DATA)) {
                    luckyDrawData = (List<LuckyDrawData>) intent.getSerializableExtra(Constants.LUCKY_DRAW_DATA);
                }

                if (count == luckyDrawData.size()) {
                    appDatabase.luckyDrawOfflineDao().nukeTable();
                }
            }
        }
    };

    private void openSpinWheelDialog(String contestID, String point) {
        Functions.setFirebaseLogEventTrack(HomeActivity.this, Constants.Click_Event, "Home", contestID, "SpinWheel");

        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        View sheetView = LayoutInflater.from(this).inflate(R.layout.spin_wheel_contest_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        TextView txtTitle = (TextView) sheetView.findViewById(R.id.txtTitle);
        TextView txtDesc = (TextView) sheetView.findViewById(R.id.txtDesc);

        TextView tvRewardPoint = (TextView) sheetView.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText(point + " " + getResources().getString(R.string.str_points));

        TextView tvRedMsg = (TextView) sheetView.findViewById(R.id.tvRedMsg);
        tvRedMsg.setVisibility(View.GONE);

        Button btnSpinPlay = (Button) sheetView.findViewById(R.id.btnSpinPlay);

        TextView tvOffline = (TextView) sheetView.findViewById(R.id.tvOffline);
        tvOffline.setVisibility(View.GONE);
        if (InternetConnection.checkConnectionForFasl(this)) {
            btnSpinPlay.setAlpha(1.0f);
            tvOffline.setVisibility(View.GONE);
        } else {
            btnSpinPlay.setAlpha(0.5f);
            tvOffline.setVisibility(View.VISIBLE);
        }

        final LuckyWheelView luckyWheelView = (LuckyWheelView) sheetView.findViewById(R.id.luckyWheel);
        setSpinnerWheelView();
        luckyWheelView.setData(data);
        luckyWheelView.setRound(3);
        luckyWheelView.setTouchEnabled(false);

        btnSpinPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (InternetConnection.checkConnection(HomeActivity.this)) {
                    if (Integer.parseInt(Functions.updateRewardPoints(sharedPreferences, "0")) >= Integer.parseInt(point)) {
                        tvRedMsg.setVisibility(View.GONE);
                        luckyWheelView.startLuckyWheelWithTargetIndex(Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_SPIN_RESULT, "0")));

                        claimRewardUsingPoints("0", point);
                    } else {
                        tvRedMsg.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        luckyWheelView.setLuckyRoundItemSelectedListener(new LuckyWheelView.LuckyRoundItemSelectedListener() {
            @Override
            public void LuckyRoundItemSelected(int index) {
                if (InternetConnection.checkConnectionForFasl(HomeActivity.this)) {
                    showSpinRewardDialog(data.get(index).getId(), "" + data.get(index).getTopText(),
                            "" + data.get(index).getValue(), "" + data.get(index).getType());
                    btnSpinPlay.setAlpha(1.0f);
                    tvOffline.setVisibility(View.GONE);
                } else {
                    btnSpinPlay.setAlpha(0.5f);
                    tvOffline.setVisibility(View.VISIBLE);
                }
                mBottomSheetDialog.dismiss();
            }
        });

        ImageView imgClose = (ImageView) sheetView.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        ((View) sheetView.getParent()).setBackgroundColor(this.getResources().getColor(android.R.color.transparent));
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();
    }

    void setSpinnerWheelView() {
        try {
            data = new ArrayList<>();
            String contestsData = sharedPreferences.getString(Constants.SharedPreferences_CONTEST, "");
            if (!TextUtils.isEmpty(contestsData)) {
                JSONArray jsonArray = new JSONArray(contestsData);
                if (jsonArray != null && jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject resultData = jsonArray.getJSONObject(i);
                        if (resultData.optString("type").equalsIgnoreCase("spin")) {
                            JSONArray spinArray = resultData.getJSONArray("json_data");
                            if (spinArray != null && spinArray.length() > 0) {
                                for (int j = 0; j < spinArray.length(); j++) {
                                    JSONObject jsonData = spinArray.getJSONObject(j);
                                    JSONArray spinDataArray = new JSONArray(jsonData.getString("spin"));
                                    if (spinDataArray != null && spinDataArray.length() > 0) {
                                        for (int k = 0; k < spinDataArray.length(); k++) {
                                            JSONObject spinData = spinDataArray.getJSONObject(k);

                                            LuckyItem luckyItem1 = new LuckyItem();
                                            if (k == 0) {
//                                                luckyItem1.setIcon(R.mipmap.thumb_1);
                                                luckyItem1.setSecondaryText("");
                                            } else if (k == 1) {
                                                luckyItem1.setIcon(R.mipmap.gift_r);
                                                luckyItem1.setSecondaryText(homeActivity.getResources().getString(R.string.str_jackpot));
                                            } else if (k == 2) {
                                                luckyItem1.setSecondaryText("");
//                                                luckyItem1.setIcon(R.mipmap.thumb_2);
                                            } else if (k == 3) {
                                                luckyItem1.setSecondaryText("");
//                                                luckyItem1.setIcon(R.mipmap.thumb_3);
                                            } else if (k == 4) {
                                                luckyItem1.setIcon(R.mipmap.gift_s);
                                                luckyItem1.setSecondaryText(homeActivity.getResources().getString(R.string.str_jackpot));
                                            } else if (k == 5) {
                                                luckyItem1.setSecondaryText("");
//                                                luckyItem1.setIcon(R.mipmap.thumb_4);
                                            }

                                            luckyItem1.setType(spinData.getString("type"));
                                            luckyItem1.setTopText(spinData.getString("text"));
                                            luckyItem1.setId(spinData.getString("id"));
                                            luckyItem1.setValue(spinData.getString("value"));
                                            luckyItem1.setColor(Functions.getRandomColor(k));
                                            data.add(luckyItem1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    String getMetaData(String id) {
        String metaData = "";
        try {
            JSONObject obj = new JSONObject();
            obj.put("is_true", "1");
            obj.put("value", homeActivity.getResources().getString(R.string.str_jackpot));
            obj.put("type", "jackpot");
            obj.put("text", homeActivity.getResources().getString(R.string.str_jack_txt));
            obj.put("reward_id", id);
            obj.put("status", "1");
            metaData = obj.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return metaData;
    }

    void claimRewardUsingPoints(String id, String points) {
        try {
            RetrofitClient.getInstance().getApi().claimRewardUsingPoints(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    SharedPreferencesUtils.getUserId(homeActivity), id, "", points, getMetaData(id)).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        Functions.deductRewardPoints(sharedPreferences, points);
                        getContestData();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showSpinRewardDialog(String id, final String text, String value, String type) {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        View sheetView = LayoutInflater.from(this).inflate(R.layout.spin_wheel_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        ImageView ivGift = (ImageView) sheetView.findViewById(R.id.ivGift);
        final TextView tvRewardMsg = (TextView) sheetView.findViewById(R.id.tvRewardMsg);
        TextView tvDecs = (TextView) sheetView.findViewById(R.id.tvDecs);
        TextView textView = (TextView) sheetView.findViewById(R.id.text);

        TextView tvQuizReward = (TextView) sheetView.findViewById(R.id.tvQuizReward);
        tvQuizReward.setText(RewardPoints.getDailyQuizRewardPoint(sharedPreferences));
        TextView tvUploadPhotoReward = (TextView) sheetView.findViewById(R.id.tvUploadPhotoReward);
        tvUploadPhotoReward.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences));

        LinearLayout llSuccessView = (LinearLayout) sheetView.findViewById(R.id.llSuccessView);
        LinearLayout llFailView = (LinearLayout) sheetView.findViewById(R.id.llFailView);

        LinearLayout llJackPotTryAgain = (LinearLayout) sheetView.findViewById(R.id.llJackPotTryAgain);
        llJackPotTryAgain.setVisibility(View.GONE);

        RelativeLayout rlDailyQuiz = (RelativeLayout) sheetView.findViewById(R.id.rlDailyQuiz);
        RelativeLayout rlPhotoUpload = (RelativeLayout) sheetView.findViewById(R.id.rlPhotoUpload);

        rlDailyQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
//                Intent i = new Intent(HomeActivity.this, QuestionActivity.class);
//                startActivity(i);

                Intent intent = null;
                try {
                    if (RongoApp.getSelectedCrop().equalsIgnoreCase("11")) {
                        intent = new Intent(homeActivity, QuestionSampleActivity.class);
                    } else {
                        if (Functions.getQuizIntent(homeActivity, sharedPreferences)) {
                            intent = new Intent(homeActivity, QuizActivity.class);
                        } else {
                            intent = new Intent(homeActivity, QuestionSampleActivity.class);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                startActivity(intent);
            }
        });

        rlPhotoUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                openCrops();
            }
        });

        TextView tvRechargeTxt = (TextView) sheetView.findViewById(R.id.tvRechargeTxt);
        tvRechargeTxt.setVisibility(View.GONE);

        if (type.equalsIgnoreCase("recharge_coupon")) {
            tvRechargeTxt.setVisibility(View.VISIBLE);
        } else {
            tvRechargeTxt.setVisibility(View.GONE);
        }

        logPlayedspinthewheelEvent(Functions.getCurrentDateTime(), value);

        if (id.equalsIgnoreCase("0")) {
            llFailView.setVisibility(View.VISIBLE);
            llSuccessView.setVisibility(View.GONE);
            llJackPotTryAgain.setVisibility(View.VISIBLE);
            ivGift.setVisibility(View.GONE);
        } else {
            ivGift.setVisibility(View.VISIBLE);
            llSuccessView.setVisibility(View.VISIBLE);
            llJackPotTryAgain.setVisibility(View.GONE);
            llFailView.setVisibility(View.GONE);
            tvDecs.setText(getResources().getText(R.string.str_aapka_inam));
            textView.setText(text);
            ivGift.setImageResource(R.mipmap.spin_icon);
        }

        TextView tvBack = (TextView) sheetView.findViewById(R.id.tvBack);
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });

        final TextView tvOffline = (TextView) sheetView.findViewById(R.id.tvOffline);
        tvOffline.setVisibility(View.GONE);
        if (InternetConnection.checkConnectionForFasl(HomeActivity.this)) {
            tvRewardMsg.setAlpha(1.0f);
            tvOffline.setVisibility(View.GONE);
        } else {
            tvRewardMsg.setAlpha(0.5f);
            tvOffline.setVisibility(View.VISIBLE);
        }

        String finalValue = value;
        tvRewardMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (InternetConnection.checkConnectionForFasl(HomeActivity.this)) {
//                    updateRewardStatus();
                    tvRewardMsg.setAlpha(1.0f);
                    tvOffline.setVisibility(View.GONE);
                    mBottomSheetDialog.dismiss();
                } else {
                    tvRewardMsg.setAlpha(0.5f);
                    tvOffline.setVisibility(View.VISIBLE);
                }

                showSpinDateTimeDialog(id, finalValue);
            }
        });

        if (InternetConnection.checkConnectionForFasl(this)) {
            tvRewardMsg.setAlpha(1.0f);
            tvOffline.setVisibility(View.GONE);
            updateContestResult(id, text, value, type);
        } else {
            tvRewardMsg.setAlpha(0.5f);
            tvOffline.setVisibility(View.VISIBLE);
            ArrayList<OfflineRewardData> offlineRewardData = new ArrayList<OfflineRewardData>();
            OfflineRewardData offlineReward = new OfflineRewardData();
            offlineReward.set_id(0);
            offlineReward.setRewardID(id);
            offlineReward.setContestID(sharedPreferences.getString(Constants.SharedPreferences_CONTEST_ID, ""));
            offlineReward.setText(text);
            offlineReward.setType(type);
            offlineReward.setValue(value);
            if (id.equalsIgnoreCase("0")) {
                offlineReward.setIs_true("0");
            } else {
                offlineReward.setIs_true("1");
            }
            offlineRewardData.add(offlineReward);
            appDatabase.offlineRewardDataDao().insertAll(offlineRewardData);
        }

        setRlGiftView(false);

        ImageView ivClose = (ImageView) sheetView.findViewById(R.id.imgClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        ((View) sheetView.getParent()).setBackgroundColor(this.getResources().getColor(android.R.color.transparent));
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();
    }

    public void logPlayedspinthewheelEvent(String datetime, String reward) {
        Bundle params = new Bundle();
        params.putString(Constants.EVENT_PARAM_DATE_TIME, datetime);
        params.putString(Constants.EVENT_PARAM_REWARD, reward);
        AppEventsLogger.newLogger(this).logEvent(Constants.EVENT_NAME_PLAYED_SPIN_WHEEL, params);
    }


    String getSpinWheelMetaData(String id, String text, String value, String type) {
        String metaData = "";
        try {
            JSONObject obj = new JSONObject();
            if (id.equalsIgnoreCase("0")) {
                obj.put("is_true", "0");
            } else {
                obj.put("is_true", "1");
            }
            obj.put("value", value);
            obj.put("type", type);
            obj.put("text", text);
            obj.put("reward_id", id);
            obj.put("status", "0");
            metaData = obj.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return metaData;
    }

    void updateContestResult(String id, String text, String value, String type) {
        try {
            RetrofitClient.getInstance().getApi().updateContestResponse(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    SharedPreferencesUtils.getUserId(HomeActivity.this),
                    sharedPreferences.getString(Constants.SharedPreferences_CONTEST_ID, ""), "",
                    getSpinWheelMetaData(id, text, value, type)).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        try {
                            String r = response.body().string();
                            if (!TextUtils.isEmpty(r)) {
                                JSONObject jsonObject = new JSONObject(r);
                                if (jsonObject != null && jsonObject.length() > 0) {
                                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                        JSONObject data = jsonObject.optJSONObject("data");
                                        if (data != null && data.length() > 0) {
                                            JSONObject resultData = data.optJSONObject("result");
                                            rewardId = resultData.optString("reward_id");
                                        }
                                    }
                                }
                            }
                            getContestData();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void showSpinDateTimeDialog(String id, final String text) {
        final String[] sDay = {""}, sTime = {""};
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        View sheetView = LayoutInflater.from(this).inflate(R.layout.spin_wheel_date_time_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        ImageView ivClose = (ImageView) sheetView.findViewById(R.id.imgClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });

        TextView tvTitleMsg = (TextView) sheetView.findViewById(R.id.tvTitleMsg);
        TextView tvDarjKare = (TextView) sheetView.findViewById(R.id.tvDarjKare);

        List<String> timeList = new ArrayList<String>(Arrays.asList(this.getResources().getStringArray(R.array.times)));
        Spinner spinnerTime = (Spinner) sheetView.findViewById(R.id.spinnerTime);
        SpinnerHintArrayAdapter spinnerArrayAdapter = new SpinnerHintArrayAdapter(this, R.layout.spinner_item_list, timeList);
        spinnerArrayAdapter.add(this.getResources().getString(R.string.str_time_select));
        spinnerTime.setAdapter(spinnerArrayAdapter);
        spinnerTime.setSelection(spinnerArrayAdapter.getCount());
        spinnerTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                if (!item.equalsIgnoreCase(getResources().getString(R.string.str_time_select))) {
                    sTime[0] = item;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        List<String> dayList = new ArrayList<String>(Arrays.asList(this.getResources().getStringArray(R.array.days)));
        Spinner spinnerDay = (Spinner) sheetView.findViewById(R.id.spinnerDay);
        SpinnerHintArrayAdapter spinnerHintArrayAdapter = new SpinnerHintArrayAdapter(this, R.layout.spinner_item_list, dayList);
        spinnerHintArrayAdapter.add(this.getResources().getString(R.string.str_day_select));
        spinnerDay.setAdapter(spinnerHintArrayAdapter);
        spinnerDay.setSelection(spinnerHintArrayAdapter.getCount());
        spinnerDay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                if (!item.equalsIgnoreCase(getResources().getString(R.string.str_day_select))) {
                    sDay[0] = item;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        tvDarjKare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(sDay[0])) {
                    if (!TextUtils.isEmpty(sTime[0])) {
                        updateRewardStatus(id, sTime[0], sDay[0]);
                        mBottomSheetDialog.dismiss();
                    } else {
                        Toast.makeText(HomeActivity.this, getResources().getString(R.string.str_please_select_time), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(HomeActivity.this, getResources().getString(R.string.str_please_select_day), Toast.LENGTH_SHORT).show();
                }
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        ((View) sheetView.getParent()).setBackgroundColor(this.getResources().getColor(android.R.color.transparent));
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();
    }

    String getJackpotData(String time, String weekday) {
        String metaData = "";
        try {
            JSONObject obj = new JSONObject();
            obj.put("timeslot", time);
            obj.put("weekday", weekday);
            metaData = obj.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return metaData;
    }

    void updateRewardStatus(String id, String time, String weekday) {
        try {
            RetrofitClient.getInstance().getApi().updateRewardStatus(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    SharedPreferencesUtils.getUserId(this), rewardId, "1", getJackpotData(time, weekday)).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        openThankYou();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openThankYou() {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
        View view = LayoutInflater.from(this).inflate(R.layout.reward_thank_you_dialog, null);
        bottomSheetDialog.setContentView(view);

        AppCompatTextView txtTitle = view.findViewById(R.id.txtTitle);

        ImageView ivBanner = view.findViewById(R.id.ivBanner);
        ivBanner.setImageResource(R.mipmap.expert_speak);

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        AppCompatTextView txtEnter = view.findViewById(R.id.txtEnter);
        txtEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(this.getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }


    class SelectCropAdapter extends RecyclerView.Adapter<SelectCropAdapter.ViewHolder> {
        List<MasterCropData> listdata;

        public SelectCropAdapter(List<MasterCropData> listdata) {
            this.listdata = listdata;
        }

        public String getSelectedCropId() {
            String cropId = "";
            for (int i = 0; i < listdata.size(); i++) {
                if (listdata.get(i).getStatus().equalsIgnoreCase("1")) {
                    cropId = listdata.get(i).getCrop_id();
                }
            }
            return cropId;
        }

        @NotNull
        @Override
        public SelectCropAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.crop_list_item, parent, false);
            SelectCropAdapter.ViewHolder viewHolder = new SelectCropAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final SelectCropAdapter.ViewHolder holder, final int position) {
            try {
                holder.tvTitle.setText(listdata.get(position).getDisplay_name());

                holder.llCropView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listdata.get(position).getStatus().equalsIgnoreCase("1")) {
                            listdata.get(position).setStatus("0");
                        } else {
                            for (int i = 0; i < listdata.size(); i++) {
                                if (listdata.get(position).getCrop_id().equalsIgnoreCase(listdata.get(i).getCrop_id())) {
                                    listdata.get(i).setStatus("1");
                                } else {
                                    listdata.get(i).setStatus("0");
                                }
                            }
                        }
                        notifyDataSetChanged();
                    }
                });

                if (listdata.get(position).getStatus().equalsIgnoreCase("1")) {
                    holder.llCropView.setBackground(getResources().getDrawable(R.drawable.rounded_layout_selected));
                    holder.ivTick.setVisibility(View.VISIBLE);
                } else {
                    holder.llCropView.setBackground(getResources().getDrawable(R.drawable.rounded_layout_unselect));
                    holder.ivTick.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(listdata.get(position).getCrop_image()) && InternetConnection.checkConnectionForFasl(HomeActivity.this)) {
                    GlideApp.with(HomeActivity.this).load(listdata.get(position).getCrop_image())
                            .apply(Functions.getPlaceholder(listdata.get(position).getCrop_id())).into(holder.ivCrop);
                } else {
                    if (listdata.get(position).getCrop_id().equalsIgnoreCase("1")) {
                        holder.ivCrop.setImageResource(R.mipmap.maize);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("2")) {
                        holder.ivCrop.setImageResource(R.mipmap.rice);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("3")) {
                        holder.ivCrop.setImageResource(R.mipmap.cotton);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("4")) {
                        holder.ivCrop.setImageResource(R.mipmap.wheat);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("5")) {
                        holder.ivCrop.setImageResource(R.mipmap.okra);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("6")) {
                        holder.ivCrop.setImageResource(R.mipmap.pearmillet);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("7")) {
                        holder.ivCrop.setImageResource(R.mipmap.corinder);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("8")) {
                        holder.ivCrop.setImageResource(R.mipmap.bittergourd);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("9")) {
                        holder.ivCrop.setImageResource(R.mipmap.bottlegourd);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("10")) {
                        holder.ivCrop.setImageResource(R.mipmap.watermelon);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("11")) {
                        holder.ivCrop.setImageResource(R.mipmap.groundnut);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("12")) {
                        holder.ivCrop.setImageResource(R.mipmap.soyabean);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("13")) {
                        holder.ivCrop.setImageResource(R.mipmap.caster);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return listdata.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvTitle;
            ImageView ivCrop, ivTick;
            RelativeLayout llCropView;

            public ViewHolder(View itemView) {
                super(itemView);
                this.llCropView = (RelativeLayout) itemView.findViewById(R.id.llCropView);
                this.tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
                this.ivCrop = (ImageView) itemView.findViewById(R.id.ivCrop);
                this.ivTick = (ImageView) itemView.findViewById(R.id.ivTick);
            }
        }
    }

    void setBuvayDateDialog() {
        progressBarDialog.hideProgressDialogWithTitle();
        final String[] is_sowing_done = {""}, cropId = {""};

        BuvayDateDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        BuvayDateDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        BuvayDateDialog.setContentView(R.layout.activity_jankari);
        BuvayDateDialog.setCanceledOnTouchOutside(false);
        BuvayDateDialog.setCancelable(false);
        BuvayDateDialog.show();

        Window window = BuvayDateDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_BLUR_BEHIND;
        window.setAttributes(wlp);
        BuvayDateDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);

        /*
         * TODO Update Multi Crop View
         * */
        RelativeLayout rlCropView = BuvayDateDialog.findViewById(R.id.rlCropView);
        rlCropView.setVisibility(View.VISIBLE);

        RelativeLayout llSowingDate = BuvayDateDialog.findViewById(R.id.rlSowingDate);
        llSowingDate.setVisibility(View.GONE);

        RecyclerView rcvCrop = BuvayDateDialog.findViewById(R.id.rcvCrop);
        rcvCrop.setLayoutManager(new GridLayoutManager(this, 3));
//        rcvCrop.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        List<MasterCropData> masterCropData = new ArrayList<MasterCropData>();
        if (masterCropList.size() > 0) {
            for (int j = 0; j < masterCropList.size(); j++) {
                if (masterCropList.get(j).getCrop_id().equalsIgnoreCase(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""))) {
                    masterCropData.add(new MasterCropData(masterCropList.get(j).getCrop_name(), masterCropList.get(j).getCrop_id(),
                            masterCropList.get(j).getCrop_image(), masterCropList.get(j).getDisplay_name(), "1"));
                } else {
                    masterCropData.add(new MasterCropData(masterCropList.get(j).getCrop_name(), masterCropList.get(j).getCrop_id(),
                            masterCropList.get(j).getCrop_image(), masterCropList.get(j).getDisplay_name(), "0"));
                }
            }
        } else {
            masterCropData.add(new MasterCropData("Maize", "1", "", "मक्का", "0"));
        }
        SelectCropAdapter cropAdapter = new SelectCropAdapter(masterCropData);
        rcvCrop.setAdapter(cropAdapter);

        TextView tvDarjKare = BuvayDateDialog.findViewById(R.id.tvDarjKare);
        tvDarjKare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cropId[0] = cropAdapter.getSelectedCropId();
                if (!TextUtils.isEmpty(cropId[0])) {
                    rlCropView.setVisibility(View.GONE);
                    llSowingDate.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(HomeActivity.this, getString(R.string.str_please_select_fasal), Toast.LENGTH_SHORT).show();
                }
            }
        });

        LinearLayout llSeasonValidation = BuvayDateDialog.findViewById(R.id.llSeasonValidation);
        llSeasonValidation.setVisibility(View.GONE);

        tvSeasonValidation = BuvayDateDialog.findViewById(R.id.tvSeasonValidation);
        tvSeasonValidationtxt = BuvayDateDialog.findViewById(R.id.tvSeasonValidationtxt);

        TextView tvRewardNew = BuvayDateDialog.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getSowingDateRewardPoint(sharedPreferences));

        TextView tvPointsSowingInfo = BuvayDateDialog.findViewById(R.id.tvPointsSowingInfo);
        tvPointsSowingInfo.setText(RewardPoints.getSowingDateRewardPoint(sharedPreferences) + " " + getResources().getString(R.string.str_points));

        TextView tvPointsSowingInfo1 = BuvayDateDialog.findViewById(R.id.tvPointsSowingInfo1);
        tvPointsSowingInfo1.setText(RewardPoints.getSowingDateRewardPoint(sharedPreferences) + " " + getResources().getString(R.string.str_points));

        LinearLayout llStatusBtn = BuvayDateDialog.findViewById(R.id.llStatusBtn);
        LinearLayout llDateView = BuvayDateDialog.findViewById(R.id.llDateView);
        llDateView.setVisibility(View.GONE);
        TextView txtYourSowingText = BuvayDateDialog.findViewById(R.id.txtYourSowingText);
        editTextSowingDate = BuvayDateDialog.findViewById(R.id.editTextSowingDate);

        TextView txtYes = BuvayDateDialog.findViewById(R.id.txtYes);
        TextView txtNo = BuvayDateDialog.findViewById(R.id.txtNo);

        final TextView txtEnter = BuvayDateDialog.findViewById(R.id.txtEnter);
        txtEnter.setVisibility(View.GONE);

        txtYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                txtEnter.setVisibility(View.VISIBLE);
                llStatusBtn.setVisibility(View.GONE);
                llDateView.setVisibility(View.VISIBLE);
                txtYourSowingText.setText(R.string.str_apni_buvay_ki_date_darj_kare);
                editTextSowingDate.setHint(R.string.str_date_darj_kare);
                is_sowing_done[0] = "yes";
            }
        });

        txtNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                txtEnter.setVisibility(View.VISIBLE);
                llStatusBtn.setVisibility(View.GONE);
                llDateView.setVisibility(View.VISIBLE);
                txtYourSowingText.setText(R.string.str_apni_expe_buvay_date_dark_kare);
                editTextSowingDate.setHint(R.string.str_date_darj_kare);
                is_sowing_done[0] = "no";
            }
        });

        editTextSowingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                openDatePicker(editTextSowingDate, is_sowing_done[0], llSeasonValidation, cropId[0]);
            }
        });

        txtEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                if (!TextUtils.isEmpty(editTextSowingDate.getText().toString().trim())) {
                    BuvayDateDialog.dismiss();
                    if (InternetConnection.checkConnection(HomeActivity.this)) {
                        progressBarDialog.showProgressDialogWithTitle("2");
                        startNewSession(editTextSowingDate, cropId[0], "0");
                    } else {
                        addOfflineCrop(cropId[0], editTextSowingDate.getText().toString().trim(), true);
                    }
                }

            }
        });
    }

    public String getTotalWeeks(String cropId) {
        String season_end_crop_week = "";
        try {
            JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.getProfileDataJSONFromAsset(HomeActivity.this));
            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                JSONObject jData = jsonObject.optJSONObject("data");
                JSONObject jresult = jData.optJSONObject("result");
                JSONObject jseason = jresult.optJSONObject("season");
                JSONArray jsonArray = jseason.optJSONArray("crops");
                if (jsonArray != null && jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jb_season = jsonArray.optJSONObject(i);
                        if (cropId.equalsIgnoreCase(jb_season.optString("crop_id"))) {
                            if (RongoApp.getDefaultLanguage().equalsIgnoreCase(jb_season.optString("app_language"))) {
                                if (RongoApp.getSeasonName().equalsIgnoreCase(jb_season.optString("season_name"))) {
                                    season_end_crop_week = jb_season.optString("season_end_crop_week");
                                    break;
                                } else {
                                    season_end_crop_week = jb_season.optString("season_end_crop_week");
                                }
                            } else {
                                season_end_crop_week = jb_season.optString("season_end_crop_week");
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return season_end_crop_week;
    }

    public void insertData(JSONObject jresult, String cropId, boolean isAdd, String s, String es, String done) {
        try {
            Log.e("Add New Crop json:-", isAdd + " - " + jresult);
            UserProfileData userProfileData = null;
            SeasonData seasonData = null;
            if (jresult != null && jresult.length() > 0) {
                String last_survey_id = jresult.optString("last_survey_id");
                String invite_people_count = jresult.optString("invite_people_count");
                String display_content_expiry = jresult.optString("display_content_expiry");
                String api_content_expiry = jresult.optString("api_content_expiry");
                String api_profile_expiry = jresult.optString("api_profile_expiry");
                int view_pending_count = jresult.optInt("view_pending_count");

                JSONObject jprofile = jresult.optJSONObject("profile");
                if (jprofile != null && jprofile.length() > 0) {
                    String user_id = jprofile.optString("user_id");
                    String first_name = jprofile.optString("first_name");
                    String last_name = jprofile.optString("last_name");
                    String phone_number = jprofile.optString("phone_number");
                    String status = jprofile.optString("status");
                    String preference = jprofile.optString("preference");
                    String location_city = jprofile.optString("location_city");
                    String location_state = jprofile.optString("location_state");
                    String location_area_city = jprofile.optString("location_area_city");
                    String location_area_state = jprofile.optString("location_area_state");
                    String location_area = jprofile.optString("location_area");

                    userProfileData = new UserProfileData(user_id, first_name, last_name, phone_number, status, preference,
                            location_city, location_state, location_area_city, location_area_state, location_area);
                }

                JSONObject jseason = jresult.optJSONObject("season");
                if (jseason != null && jseason.length() > 0) {
                    String season_id = jseason.optString("season_id");
                    String season_start_date = jseason.optString("season_start_date");
                    String season_end_date = jseason.optString("season_end_date");

//                    JSONArray jsonArrayNew = getCropDetail(cropId);
                    JSONArray jsonArray = jseason.optJSONArray("crops");
                    SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                            Constants.SharedPreference_profileResultData, "");

                    List<CropsData> cropsDataList = new ArrayList<>();
                    List<AddCropStatusData> addCrop = appDatabase.addCropStatusDao().getAllData();
                    if (addCrop != null && addCrop.size() > 0) {
                        for (int k = 0; k < addCrop.size(); k++) {
                            if (jsonArray != null && jsonArray.length() > 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jb_season = jsonArray.optJSONObject(i);
                                    if (addCrop.get(k).getCropId().equalsIgnoreCase(jb_season.optString("crop_id"))) {
                                        String field_id = jb_season.optString("field_id");
                                        String geo_latitude = jb_season.optString("geo_latitude");
                                        String geo_longitude = jb_season.optString("geo_longitude");
                                        String geo_metadata = jb_season.optString("geo_metadata");
                                        String area_unit = jb_season.optString("area_unit");
                                        String field_size = jb_season.optString("field_size");
                                        String p_crop_id = jb_season.optString("p_crop_id");
                                        String seed_type = jb_season.optString("seed_type");
                                        String crop_in_days = jb_season.optString("crop_in_days");
                                        String crop_in_weeks = jb_season.optString("crop_in_weeks");
                                        String crop_id = jb_season.optString("crop_id");
                                        String app_language = jb_season.optString("app_language");
                                        String seedMeta = jb_season.optString("seed_metadata");
                                        String crop_status = jb_season.optString("crop_status");
                                        String season_feedback = jb_season.optString("season_feedback");
                                        String season_end_crop_week = jb_season.optString("season_end_crop_week");

                                        List<CropWeekDetails> cropWeekDetailsList = new ArrayList<>();
                                        JSONArray cropWeekDetails = jb_season.getJSONArray("crop_week_detail");
                                        if (cropWeekDetails != null && cropWeekDetails.length() > 0) {
                                            for (int j = 0; j < cropWeekDetails.length(); j++) {
                                                JSONObject jb = cropWeekDetails.optJSONObject(j);
                                                int crop_week = jb.optInt("crop_week");
                                                String stage_id = jb.optString("stage_id");
                                                String stage_name = jb.optString("stage_name");
                                                String display_name = jb.optString("display_name");
                                                String stage_start_week = jb.optString("stage_start_week");
                                                String stage_end_week = jb.optString("stage_end_week");
                                                int is_new_stage = jb.optInt("is_new_stage");
                                                cropWeekDetailsList.add(new CropWeekDetails(crop_week, stage_id, stage_name, display_name, stage_start_week, stage_end_week, is_new_stage));
                                            }
                                        }
                                        Log.e("+++lang3333++",""+app_language);
                                        cropsDataList.add(new CropsData(field_id, area_unit, geo_latitude, geo_longitude, geo_metadata,
                                                field_size, p_crop_id, addCrop.get(k).getSowingDone(), addCrop.get(k).getSowingExpDate(),
                                                addCrop.get(k).getSowingDate(), seed_type, crop_in_days, crop_in_weeks, crop_id,
                                                cropWeekDetailsList, seedMeta, crop_status, season_feedback, season_end_crop_week, app_language));
                                    }
                                }
                            }
                        }
                    } else {
                        if (jsonArray != null && jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jb_season = jsonArray.optJSONObject(i);
                                if (cropId.equalsIgnoreCase(jb_season.optString("crop_id"))) {
                                    String field_id = jb_season.optString("field_id");
                                    String geo_latitude = jb_season.optString("geo_latitude");
                                    String geo_longitude = jb_season.optString("geo_longitude");
                                    String geo_metadata = jb_season.optString("geo_metadata");
                                    String area_unit = jb_season.optString("area_unit");
                                    String field_size = jb_season.optString("field_size");
                                    String p_crop_id = jb_season.optString("p_crop_id");
                                    String seed_type = jb_season.optString("seed_type");
                                    String crop_in_days = jb_season.optString("crop_in_days");
                                    String crop_in_weeks = jb_season.optString("crop_in_weeks");
                                    String crop_id = jb_season.optString("crop_id");
                                    String app_language = jb_season.optString("app_language");
                                    String seedMeta = jb_season.optString("seed_metadata");
                                    String crop_status = jb_season.optString("crop_status");
                                    String season_feedback = jb_season.optString("season_feedback");
                                    String season_end_crop_week = jb_season.optString("season_end_crop_week");

                                    List<CropWeekDetails> cropWeekDetailsList = new ArrayList<>();
                                    JSONArray cropWeekDetails = jb_season.getJSONArray("crop_week_detail");
                                    if (cropWeekDetails != null && cropWeekDetails.length() > 0) {
                                        for (int j = 0; j < cropWeekDetails.length(); j++) {
                                            JSONObject jb = cropWeekDetails.optJSONObject(j);
                                            int crop_week = jb.optInt("crop_week");
                                            String stage_id = jb.optString("stage_id");
                                            String stage_name = jb.optString("stage_name");
                                            String display_name = jb.optString("display_name");
                                            String stage_start_week = jb.optString("stage_start_week");
                                            String stage_end_week = jb.optString("stage_end_week");
                                            int is_new_stage = jb.optInt("is_new_stage");
                                            cropWeekDetailsList.add(new CropWeekDetails(crop_week, stage_id, stage_name, display_name, stage_start_week, stage_end_week, is_new_stage));
                                        }
                                    }
                                    Log.e("+++lang333++",""+app_language);
                                    cropsDataList.add(new CropsData(field_id, area_unit, geo_latitude, geo_longitude, geo_metadata,
                                            field_size, p_crop_id, done, es, s, seed_type, crop_in_days, crop_in_weeks, crop_id,
                                            cropWeekDetailsList, seedMeta, crop_status, season_feedback, season_end_crop_week,
                                            app_language));
                                }
                            }
                        }
                    }
                    seasonData = new SeasonData(season_id, season_start_date, season_end_date, cropsDataList);
                    ProfileResultData profileResultData = new ProfileResultData(last_survey_id, invite_people_count, display_content_expiry, userProfileData, seasonData, view_pending_count, api_content_expiry, api_profile_expiry);
                    SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                            Constants.SharedPreference_profileResultData, (new Gson()).toJson(profileResultData));

                    setAlreadyCropAdd();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addOfflineCrop(String cropId, String sowingDate, boolean isChange) {
        try {
            Log.e("isChange:", "" + isChange);
            String season_end_crop_week = getTotalWeeks(cropId);
            progressBarDialog.showProgressDialogWithTitle("11");
            String UserName = sharedPreferences.getString(Constants.SharedPreference_First_Name, "");
            String State = sharedPreferences.getString(Constants.SharedPreference_New_State, "");
            String City = sharedPreferences.getString(Constants.SharedPreference_New_City, "");

            String is_sowing_done = "", s = "0000-00-00 00:00:00", es = "0000-00-00 00:00:00";
            if (isChange) {
                long dTime = Functions.covertToMili("dd-MM-yyyy", sowingDate);
                if (System.currentTimeMillis() > dTime) {
                    is_sowing_done = "yes";
                    s = Functions.converOneDateFormatToOther("dd-MM-yyyy", "yyyy-MM-dd hh:mm:ss", sowingDate);
                } else {
                    is_sowing_done = "no";
                    es = Functions.converOneDateFormatToOther("dd-MM-yyyy", "yyyy-MM-dd hh:mm:ss", sowingDate);
                }
            } else {
                s = sowingDate;
                is_sowing_done = "yes";
            }
            Log.e("Done", cropId + "-" + is_sowing_done + " - " + s);

            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_SowingDate, s);
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Expected_SowingDate, es);
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_CROP_ID, cropId);
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Sowing_Done, is_sowing_done);
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_ID, "0");
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_P_CROP_ID, "0");
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Field_ID, "0");
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_season_end_crop_week, season_end_crop_week);
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_crop_status, "active");
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_season_feedback, "");
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_First_Name, UserName);

            RongoApp.getAllCropWeeklyQuestion(false);
            RongoApp.getFertilizerInfo(false);
            RongoApp.getDailyQuizData(false);

            List<AddCropStatusData> addCropStatus = appDatabase.addCropStatusDao().getAllData(cropId);
            if (addCropStatus.size() == 0) {
                AddCropStatusData addCropStatusData = new AddCropStatusData();
                addCropStatusData.setIds(new Random().nextInt());
                addCropStatusData.setPcropIds(cropId);
                addCropStatusData.setCropId(cropId);
                addCropStatusData.setSowingDone(is_sowing_done);
                addCropStatusData.setSowingExpDate(es);
                addCropStatusData.setSessionId("0");
                addCropStatusData.setFieldId("0");
                addCropStatusData.setSessionEndTotal(season_end_crop_week);
                addCropStatusData.setSowingDate(s);
                addCropStatusData.setStatus("1");
                appDatabase.addCropStatusDao().insertAll(addCropStatusData);
            }

            JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.getProfileDataJSONFromAsset(HomeActivity.this));
            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                JSONObject jData = jsonObject.optJSONObject("data");
                JSONObject jresult = jData.optJSONObject("result");
                JSONObject jseason = jresult.optJSONObject("season");
                JSONArray jsonArray = jseason.optJSONArray("crops");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jb_season = jsonArray.optJSONObject(i);
                    if (cropId.equalsIgnoreCase(jb_season.optString("crop_id"))) {
                        if (RongoApp.getDefaultLanguage().equalsIgnoreCase(jb_season.optString("app_language"))) {
                            if (RongoApp.getSeasonName().equalsIgnoreCase(jb_season.optString("season_name"))) {
                                insertData(jresult, cropId, isChange, s, es, is_sowing_done);
                                break;
                            }
                        }
                    }
                }
            }

            ProfileOfflineData profileOfflineData = new ProfileOfflineData();
            profileOfflineData.setUid(new Random().nextInt());
            profileOfflineData.setFirstName(UserName);
            profileOfflineData.setLastName("");
            profileOfflineData.setPhoneNumber(sharedPreferences.getString(Constants.SharedPreferences_MOBILE_NO, ""));
            profileOfflineData.setCity(City);
            profileOfflineData.setState(State);
            profileOfflineData.setStatus("active");
            profileOfflineData.setCountry(sharedPreferences.getString(Constants.SharedPreference_Country, ""));
            profileOfflineData.setFieldId("0");
            profileOfflineData.setFieldSize("0");
            profileOfflineData.setP_crop_id(cropId);
            profileOfflineData.setCrop_id(cropId);
            profileOfflineData.setIs_sowing_done(is_sowing_done);
            profileOfflineData.setSowingDate(s);
            profileOfflineData.setExpectedSowingDate(es);
            profileOfflineData.setSeedType("Sayaji");
            profileOfflineData.setSeason_end_crop_week(season_end_crop_week);
            appDatabase.ProfileOfflineDao().getOfflineProfileCropId(cropId);

            List<ProfileOfflineData> data = appDatabase.ProfileOfflineDao().getOfflineProfileCropId(cropId);
            if (data.size() > 0) {
                appDatabase.ProfileOfflineDao().update(s, es, is_sowing_done, cropId);
            } else {
                appDatabase.ProfileOfflineDao().insertAll(profileOfflineData);
            }

            if (sharedPreferences.getString(Constants.SharedPreference_Multi_Language_Info_status, "0").equalsIgnoreCase("0")) {
                if (cropId.equalsIgnoreCase(Constants.MAIZE) || cropId.equalsIgnoreCase(Constants.WHEAT)) {
                    if (RongoApp.getSelectedState().equalsIgnoreCase(Constants.MAHARASHTRA_STATE)) {
                        if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.HINDI)) {
                            openLanguageHintDialog(cropId, s, LocaleManager.MARATHI);
                        } else {
                            updateDataOffline();
                        }
                    } else {
                        updateDataOffline();
                    }
                } else if (cropId.equalsIgnoreCase(Constants.COTTON) || cropId.equalsIgnoreCase(Constants.PEARMILLET)) {
                    if (RongoApp.getSelectedState().equalsIgnoreCase(Constants.GUJARAT_STATE)) {
                        if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.HINDI)) {
                            openLanguageHintDialog(cropId, s, LocaleManager.GUJARATI);
                        } else {
                            updateDataOffline();
                        }
                    } else {
                        updateDataOffline();
                    }
                } else {
                    updateDataOffline();
                }
            } else {
                updateDataOffline();
            }

            mCircleNavigationView.setCenterView(cropId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void updateDataOffline() {
        try {
            RongoApp.getCropWeekDaysFunction();
            mCircleNavigationView.changeCurrentItem(0);
            setBuvayDialogClose();
            setAlreadyCropAdd();

            if (progressBarDialog != null) {
                progressBarDialog.hideProgressDialogWithTitle();
            }

            Intent intent = new Intent();
            intent.setAction(Constants.BROADCAST_RECEIVER.UPDATE_CROP_INFO);
            sendBroadcast(intent);

            if (!RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.HINDI)) {
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Default_Language, LocaleManager.HINDI);
                RongoApp.setNewLocale(homeActivity, LocaleManager.HINDI);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void openDatePicker(final AppCompatEditText appCompatEditText, final String YesOrNo, LinearLayout llSeasonValidation, String cropID) {
        llSeasonValidation.setVisibility(View.GONE);
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                c.set(Calendar.YEAR, year);
                c.set(Calendar.MONTH, monthOfYear);

                fromDatePicker = "Y";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                String formattedDate = simpleDateFormat.format(c.getTime());
                if (YesOrNo.equalsIgnoreCase("Yes")) {
                    sowingDate = formattedDate;
                } else {
                    expectedSowingDate = formattedDate;
                }

                try {
                    String sessionName = Functions.getSeasonName(homeActivity, String.valueOf(monthOfYear));
                    Log.e("sessionName: ", cropID + " - " + sessionName);
                    if (cropID.equalsIgnoreCase("2")) {
                        if (sessionName.equalsIgnoreCase(Constants.KHARIF_SEASON)) {
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                            appCompatEditText.setText(formattedDate);
                        } else {
                            String sm = "<b>" + getString(R.string.str_rice) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                            String sm1 = "<b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_rice) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_kharif_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                            tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                            tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                            llSeasonValidation.setVisibility(View.VISIBLE);
                            appCompatEditText.setText("");
                        }
                    } else if (cropID.equalsIgnoreCase("3")) {
                        if (sessionName.equalsIgnoreCase(Constants.KHARIF_SEASON)) {
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                            appCompatEditText.setText(formattedDate);
                        } else {
                            String sm = "<b>" + getString(R.string.str_cotton) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                            String sm1 = "<b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_cotton) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_kharif_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                            tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                            tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                            llSeasonValidation.setVisibility(View.VISIBLE);
                            appCompatEditText.setText("");
                        }
                    } else if (cropID.equalsIgnoreCase("4")) {
                        if (sessionName.equalsIgnoreCase(Constants.RABI_SEASON)) {
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                            appCompatEditText.setText(formattedDate);
                        } else {
//                        Toast.makeText(homeActivity, "False - " + sessionName, Toast.LENGTH_SHORT).show();
                            String sm = "<b>" + getString(R.string.str_wheat) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                            String sm1 = "<b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_wheat) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_rabi_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                            tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                            tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                            llSeasonValidation.setVisibility(View.VISIBLE);
                            appCompatEditText.setText("");
                        }
                    } else if (cropID.equalsIgnoreCase("6")) {
                        if (sessionName.equalsIgnoreCase(Constants.KHARIF_SEASON)) {
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                            appCompatEditText.setText(formattedDate);
                        } else {
                            String sm = "<b>" + getString(R.string.str_pearlmillet) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                            String sm1 = "<b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_pearlmillet) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_kharif_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                            tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                            tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                            llSeasonValidation.setVisibility(View.VISIBLE);
                            appCompatEditText.setText("");
                        }
                    } else if (cropID.equalsIgnoreCase("7")) {
                        if (sessionName.equalsIgnoreCase(Constants.RABI_SEASON)) {
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                            appCompatEditText.setText(formattedDate);
                        } else {
                            String sm = "<b>" + getString(R.string.str_coriander) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                            String sm1 = "<b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_coriander) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_rabi_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                            tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                            tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                            llSeasonValidation.setVisibility(View.VISIBLE);
                            appCompatEditText.setText("");
                        }
                    } else if (cropID.equalsIgnoreCase("8")) {
                        if (sessionName.equalsIgnoreCase(Constants.RABI_SEASON)) {
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                            appCompatEditText.setText(formattedDate);
                        } else {
                            String sm = "<b>" + getString(R.string.str_bittergourd) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                            String sm1 = "<b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_bittergourd) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_rabi_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                            tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                            tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                            llSeasonValidation.setVisibility(View.VISIBLE);
                            appCompatEditText.setText("");
                        }
                    } else if (cropID.equalsIgnoreCase("9")) {
                        if (sessionName.equalsIgnoreCase(Constants.RABI_SEASON)) {
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                            appCompatEditText.setText(formattedDate);
                        } else {
                            String sm = "<b>" + getString(R.string.str_bottlegourd) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                            String sm1 = "<b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_bottlegourd) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_rabi_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                            tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                            tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                            llSeasonValidation.setVisibility(View.VISIBLE);
                            appCompatEditText.setText("");
                        }
                    } else if (cropID.equalsIgnoreCase("10")) {
                        if (sessionName.equalsIgnoreCase(Constants.RABI_SEASON)) {
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                            appCompatEditText.setText(formattedDate);
                        } else {
                            String sm = "<b>" + getString(R.string.str_watermelon) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                            String sm1 = "<b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_watermelon) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_rabi_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                            tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                            tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                            llSeasonValidation.setVisibility(View.VISIBLE);
                            appCompatEditText.setText("");
                        }
                    } else if (cropID.equalsIgnoreCase("11")) {
                        if (sessionName.equalsIgnoreCase(Constants.KHARIF_SEASON)) {
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                            appCompatEditText.setText(formattedDate);
                        } else {
                            String sm = "<b>" + getString(R.string.str_groundnut) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                            String sm1 = "<b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_groundnut) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_kharif_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                            tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                            tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                            llSeasonValidation.setVisibility(View.VISIBLE);
                            appCompatEditText.setText("");
                        }
                    } else if (cropID.equalsIgnoreCase("12")) {
                        if (sessionName.equalsIgnoreCase(Constants.KHARIF_SEASON)) {
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                            appCompatEditText.setText(formattedDate);
                        } else {
                            String sm = "<b>" + getString(R.string.str_soyabean) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                            String sm1 = "<b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_soyabean) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_kharif_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                            tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                            tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                            llSeasonValidation.setVisibility(View.VISIBLE);
                            appCompatEditText.setText("");
                        }
                    } else if (cropID.equalsIgnoreCase("13")) {
                        if (sessionName.equalsIgnoreCase(Constants.KHARIF_SEASON)) {
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                            appCompatEditText.setText(formattedDate);
                        } else {
                            String sm = "<b>" + getString(R.string.str_castor) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                            String sm1 = "<b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_castor) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_kharif_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                            tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                            tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                            llSeasonValidation.setVisibility(View.VISIBLE);
                            appCompatEditText.setText("");
                        }
                    } else {
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                        appCompatEditText.setText(formattedDate);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logSubmittedCropSowingDetailsEvent(String datetime, String crop, String sowingDate) {
        Bundle params = new Bundle();
        params.putString(Constants.EVENT_PARAM_DATE_TIME, datetime);
        params.putString(Constants.EVENT_PARAM_CROP, crop);
        params.putString(Constants.EVENT_PARAM_SOWING_DATE, sowingDate);
        AppEventsLogger.newLogger(this).logEvent(Constants.EVENT_NAME_SOWING_DETAIL, params);
    }

    void saveSowingInfo(EditText editTextSowingDate) {
        String is_sowing_done = "", seedType = "", s = "0000-00-00 00:00:00", es = "0000-00-00 00:00:00";

        long dTime = Functions.covertToMili("dd-MM-yyyy", editTextSowingDate.getText().toString().trim());
        if (System.currentTimeMillis() > dTime) {
            is_sowing_done = "yes";
            s = Functions.converOneDateFormatToOther("dd-MM-yyyy", "yyyy-MM-dd hh:mm:ss", editTextSowingDate.getText().toString().trim());
        } else {
            is_sowing_done = "no";
            es = Functions.converOneDateFormatToOther("dd-MM-yyyy", "yyyy-MM-dd hh:mm:ss", editTextSowingDate.getText().toString().trim());
        }
        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Sowing_Done, is_sowing_done);

        logSubmittedCropSowingDetailsEvent(Functions.getCurrentDateTime(), "Maize", "" + s + " - " + es);

        RetrofitClient.getInstance().getApi().saveSowingInfo(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                Functions.getDeviceid(this), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                SharedPreferencesUtils.getUserId(this), sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""), is_sowing_done, s, es, seedType,
                sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, "")).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                setBuvayDialogClose();
            }
        });
    }

    void updateWeeklyCropInfo(EditText editTextSowingDate) {
        String infoType = "sowing", infoText = getResources().getString(R.string.str_buvay), infoValue = "", metaData = getResources().getString(R.string.str_ha);
        long dTime = Functions.covertToMili("dd-MM-yyyy", editTextSowingDate.getText().toString().trim());
        String date = Functions.converOneDateFormatToOther("dd-MM-yyyy", "yyyy-MM-dd hh:mm:ss", editTextSowingDate.getText().toString().trim());
        if (System.currentTimeMillis() > dTime) {
            metaData = getResources().getString(R.string.str_ha);
        } else {
            metaData = getResources().getString(R.string.str_na);
        }

        RetrofitClient.getInstance().getApi().updateWeeklyCropInfo(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                Functions.getDeviceid(this), SharedPreferencesUtils.getUserId(this),
                sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_Field_ID, ""),
                infoType, infoText, date, metaData).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    public void setDailyLoginAlertFunction() {
        try {
            if (!TextUtils.isEmpty(SharedPreferencesUtils.getUserId(this))) {
                String date = sharedPreferences.getString(Constants.SharedPreference_DailyLogin_Date, "");
                if (!TextUtils.isEmpty(date)) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                    Date checkDate = sdf.parse(date);
                    Date currentDate = sdf.parse(Functions.getCurrentDate());
                    if (Objects.requireNonNull(currentDate).after(checkDate)) {
//                        setCustomToastBar();
                        Functions.setDailyReward(appDatabase, RewardPoints.getDailyLoginPoint(sharedPreferences));
                    }
                } else {
//                    setCustomToastBar();
                    Functions.setDailyReward(appDatabase, RewardPoints.getDailyLoginPoint(sharedPreferences));
                }

//                if (InternetConnection.checkConnectionForFasl(HomeActivity.this)) {
//                    List<DailyRewardData> dailyRewardData = appDatabase.dailyRewardDao().getAll();
//                    if (dailyRewardData.size() > 0) {
//                        updateRewardPoints();
//                    }
//                }

                SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                        Constants.SharedPreference_DailyLogin_Date, Functions.getCurrentDate());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setCustomToastBar() {
        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_DailyLogin_Date, Functions.getCurrentDate());
        LayoutInflater layoutInflater = getLayoutInflater();
        View layout = layoutInflater.inflate(R.layout.customtoast, (ViewGroup) findViewById(R.id.custom_toast_layout));
        final Toast toast = new Toast(HomeActivity.this);
        TextView textView = (TextView) layout.findViewById(R.id.tvTitle);
        textView.setText(Html.fromHtml(getString(R.string.str_aaj_login) + RewardPoints.getDailyLoginPoint(sharedPreferences) + " " + getString(R.string.str_aaj_login_text)));
        toast.setGravity(Gravity.BOTTOM | Gravity.FILL_HORIZONTAL, 0, 0);
        toast.setView(layout);
        new CountDownTimer(Math.max(5000 - 2000, 1000), 1000) {
            @Override
            public void onFinish() {
                toast.show();
            }

            @Override
            public void onTick(long millisUntilFinished) {
                toast.show();
            }
        }.start();
    }


    void getEarnedRewardsData() {
        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Earned_Reward, "");
        RetrofitClient.getInstance().getApi().getEarnedRewards(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), SharedPreferencesUtils.getUserId(HomeActivity.this),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0")).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String r = response.body().string();
                        if (!TextUtils.isEmpty(r)) {
                            JSONObject jsonObject = new JSONObject(r);
//                            Log.v("getEarnedRewardsData: ", "" + jsonObject.toString());
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject data = jsonObject.optJSONObject("data");
                                    if (data != null && data.length() > 0) {
                                        JSONObject result = data.optJSONObject("result");
                                        if (result != null && result.length() > 0) {
                                            JSONArray jsonArray = result.optJSONArray("rewards");
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Earned_Reward, "" + jsonArray);

                                            JSONArray how_to_earn_list = result.optJSONArray("how_to_earn_list");
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_How_Earn_Reward, "" + how_to_earn_list);
                                        }
                                    }

                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void getPendingRewardsData() {
        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Pending_Reward, "");
        RetrofitClient.getInstance().getApi().getPendingRewards(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), SharedPreferencesUtils.getUserId(HomeActivity.this),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0")).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String r = response.body().string();
                        if (!TextUtils.isEmpty(r)) {
                            JSONObject jsonObject = new JSONObject(r);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject data = jsonObject.optJSONObject("data");
                                    if (data != null && data.length() > 0) {
                                        JSONArray jsonArray = data.optJSONArray("result");
                                        if (jsonArray != null && jsonArray.length() > 0) {
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Pending_Reward, "" + jsonArray);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }


    void getProfileNew() {
        RetrofitClient.getInstance().getApi().getProfile(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                Functions.getDeviceid(HomeActivity.this), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                SharedPreferencesUtils.getUserId(HomeActivity.this)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String r_str = response.body().string();
                        if (!TextUtils.isEmpty(r_str)) {
                            JSONObject jb = new JSONObject(r_str);
                            if (jb != null && jb.length() > 0) {
                                JSONObject jData = jb.optJSONObject("data");
                                if (jData != null && jData.length() > 0) {
                                    JSONObject jresult = jData.optJSONObject("result");
                                    if (jresult != null && jresult.length() > 0) {
                                        JSONObject jprofile = jresult.optJSONObject("profile");
                                        if (jprofile == null) {
                                            SharedPreferencesUtils.logout(sharedPreferences);
                                            Intent i = new Intent(HomeActivity.this, LoginOptionsActivity.class);
                                            startActivity(i);
                                            finish();
                                        } else {
                                            ProfileResultData profileResultData = new InsertHomeFeedData(jb).insertData();
                                            if (profileResultData != null) {
                                                RongoApp.getSeasonNameFunction();
                                                if (profileResultData.getView_pending_count() > 0) {
                                                    RongoApp.getPosts();
                                                }
                                            }
                                            getPragatiData();
                                        }
                                    }
                                }

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void getPragatiData() {
        RetrofitClient.getInstance().getApi().getCropPragatiTimeline(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                SharedPreferencesUtils.getUserId(HomeActivity.this), sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                Functions.getCommaSepPCropId(sharedPreferences)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String r = response.body().string();
                        if (!TextUtils.isEmpty(r)) {
                            JSONObject jsonObject = new JSONObject(r);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject data = jsonObject.optJSONObject("data");
                                    if (data != null && data.length() > 0) {
                                        JSONObject resultData = data.optJSONObject("result");
                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Pragati, "" + resultData);
                                        setPragatiData();
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void setPragatiData() {
        try {
            String resultData = sharedPreferences.getString(Constants.SharedPreferences_Pragati, "");
            if (!TextUtils.isEmpty(resultData)) {
                JSONObject result = new JSONObject(resultData);
                if (result != null && result.length() > 0) {
                    JSONObject weekly_info = result.optJSONObject("weekly_info");
                    if (weekly_info != null && weekly_info.length() > 0) {
                        Object weeklyinfojson = new JSONTokener(weekly_info.getString(RongoApp.getSelectedPCrop())).nextValue();
                        if (weeklyinfojson != null) {
                            JSONObject jsonObject = weekly_info.optJSONObject(RongoApp.getSelectedPCrop());
                            if (jsonObject != null && jsonObject.length() > 0) {

                                for (int i = 0; i < RongoApp.getSessionEndWeek(); i++) {
                                    JSONObject jd = jsonObject.optJSONObject(String.valueOf(i));
                                    if (jd != null && jd.length() > 0) {
                                        JSONArray jsonArray = new JSONArray(jd.optString("crop_info"));
                                        if (jsonArray != null && jsonArray.length() > 0) {
                                            for (int k = 0; k < jsonArray.length(); k++) {
                                                JSONObject object = jsonArray.optJSONObject(k);
                                                String info_type = object.optString("info_type");
//                                                Log.e("+++info_type", String.valueOf(i) + " - " + info_type);
                                                List<WeeklyCropInfo> weeklyCropInfoCheck = appDatabase.weeklyCropInfoDao().getCropInfoByWeek(
                                                        String.valueOf(i), RongoApp.getSelectedPCrop(), info_type);
                                                if (weeklyCropInfoCheck.size() > 0) {

                                                } else {
                                                    List<WeeklyCropInfo> infoList = new ArrayList<WeeklyCropInfo>();
                                                    WeeklyCropInfo cropInfo = new WeeklyCropInfo();
                                                    cropInfo.set_id(new Random().nextInt());
                                                    cropInfo.setCropDay(String.valueOf(k));
                                                    cropInfo.setCropWeek(String.valueOf(i));
                                                    cropInfo.setCropId(RongoApp.getSelectedCrop());
                                                    cropInfo.setPcropId(RongoApp.getSelectedPCrop());
                                                    cropInfo.setIsDone("1");
                                                    cropInfo.setStatus("1");
                                                    cropInfo.setInfoTypo(info_type);
                                                    infoList.add(cropInfo);
                                                    appDatabase.weeklyCropInfoDao().insertAll(infoList);
                                                }

                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    ArrayList<String> imagesPath = new ArrayList<>();
    String mCurrentPhotoPath = "";
    BottomSheetDialog bottomSheetDialogCrop;
    CropAreaForHomeAdapter cropAreaAdapter;
    CropAreaFocusedData mSelectedList;

    public void openCrops() {
        bottomSheetDialogCrop = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_lucky_draw_crops, null);
        bottomSheetDialogCrop.setContentView(view);
        bottomSheetDialogCrop.setCancelable(false);
        bottomSheetDialogCrop.setCanceledOnTouchOutside(false);

        LinearLayout llUploadView = (LinearLayout) view.findViewById(R.id.llUploadView);
        llUploadView.setVisibility(View.GONE);

        MaterialCardView cardHealthlly = (MaterialCardView) view.findViewById(R.id.cardHealthlly);
        cardHealthlly.setVisibility(View.VISIBLE);

        AppCompatTextView tvPoint = view.findViewById(R.id.tvPoint);
        tvPoint.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences));

        AppCompatTextView tvUpload = view.findViewById(R.id.tvUpload);
        tvUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardHealthlly.setVisibility(View.GONE);
                llUploadView.setVisibility(View.VISIBLE);
            }
        });

        AppCompatTextView tvPointsUploadPicture = view.findViewById(R.id.tvPointsUploadPicture);
        tvPointsUploadPicture.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences) + " " + getString(R.string.str_point_earned));

        RecyclerView rvCrops = view.findViewById(R.id.rvCrops);
        rvCrops.setLayoutManager(new LinearLayoutManager(homeActivity, RecyclerView.HORIZONTAL, false));
        rvCrops.setNestedScrollingEnabled(false);
        rvCrops.setHasFixedSize(true);
        List<CropAreaFocusedData> cropAreaFocusedData = SharedPreferencesUtils.getCropArea(homeActivity, Constants.SharedPreferences_CROP_FOCUSED_AREA);
        if (cropAreaFocusedData != null && cropAreaFocusedData.size() > 0) {
            rvCrops.setAdapter(new CropAreaForHomeAdapter(homeActivity, cropAreaFocusedData, mSelectedList));
        }

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogCrop.dismiss();
            }
        });

        ImageView imgNext = view.findViewById(R.id.nextImg);
        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rvCrops.post(new Runnable() {
                    @Override
                    public void run() {
                        if (cropAreaFocusedData != null && cropAreaFocusedData.size() > 0) {
                            rvCrops.scrollToPosition(cropAreaFocusedData.size() - 1);
                        }
                    }
                });
            }
        });

        AppCompatTextView txtUpload = view.findViewById(R.id.txtUpload);
        txtUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogCrop.dismiss();
                openGuidlinesDialog();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(homeActivity.getResources().getColor(android.R.color.transparent));
        bottomSheetDialogCrop.show();
    }

    public void getSelectedArea(CropAreaFocusedData mCropAreaFocusedData) {
        this.mSelectedList = mCropAreaFocusedData;
    }

    BottomSheetDialog bottomSheetThumbnail;
    ThumbnailsAdapter thumbnailsAdapter;

    void openThumbnailDialog() {
        bottomSheetThumbnail = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_upload_more_photos, null);
        bottomSheetThumbnail.setContentView(view);

        AppCompatTextView tvPointsUploadPicture = view.findViewById(R.id.tvPointsUploadPicture);
        tvPointsUploadPicture.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences) + getString(R.string.str_point_earned));

        final ImageView imgNext = view.findViewById(R.id.imgNext);
        final ProgressBar pb = view.findViewById(R.id.pb);
        final LinearLayout linearLuckYDraw = view.findViewById(R.id.linearLuckYDraw);
        linearLuckYDraw.setVisibility(View.VISIBLE);
        bottomSheetThumbnail.setCanceledOnTouchOutside(false);
        bottomSheetThumbnail.setCancelable(false);

        if (imagesPath.size() == 1) {
            imgNext.setVisibility(View.GONE);
        } else {
            imgNext.setVisibility(View.VISIBLE);
        }

        RecyclerView rvAllPhotos = view.findViewById(R.id.rvAllPhotos);
        rvAllPhotos.setLayoutManager(new LinearLayoutManager(homeActivity, RecyclerView.HORIZONTAL, false));

        thumbnailsAdapter = new ThumbnailsAdapter(imagesPath);
        rvAllPhotos.setAdapter(thumbnailsAdapter);
        rvAllPhotos.setNestedScrollingEnabled(false);

        AppCompatTextView txtUploadMore = view.findViewById(R.id.txtUploadMore);
        final AppCompatTextView txtGoOn = view.findViewById(R.id.txtGoOn);
        txtGoOn.setText(getResources().getString(R.string.str_darj_kare));

        final ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetThumbnail.dismiss();

                if (bottomSheetDialogCrop != null) {
                    bottomSheetDialogCrop.show();
                }

                if (imagesPath != null && imagesPath.size() > 0) {
                    imagesPath.clear();
                }
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));

        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rvAllPhotos.post(new Runnable() {
                    @Override
                    public void run() {
                        if (imagesPath != null && imagesPath.size() > 0) {
                            rvAllPhotos.scrollToPosition(imagesPath.size() - 1);
                        }
                    }
                });
            }
        });

        txtUploadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetThumbnail.dismiss();
                accessPermission();
            }
        });

        txtGoOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                tvRewardPoint.setText(Functions.updateRewardPoints(sharedPreferences, getUploadPictureRewardPoint()));

                if (imagesPath.size() > 0) {
                    List<AdvisoryPhotoData> advisoryPhotoData = new ArrayList<AdvisoryPhotoData>();
                    advisoryPhotoData.add(new AdvisoryPhotoData(0,
                            sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, "0"),
                            RongoApp.getSelectedCrop(), RongoApp.getSelectedPCrop(), "1"));
                    appDatabase.advisoryPhotoDao().insertAll(advisoryPhotoData);
                }

                if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                    if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {

                        String focus_area = "";
                        if (mSelectedList != null) {
                            focus_area = mSelectedList.getType();
                        }

                        final String finalFocus_area = focus_area;
                        Executors.newSingleThreadExecutor().execute(new Runnable() {
                            @Override
                            public void run() {
                                List<LuckyDrawData> luckyDrawData = new ArrayList<>();
                                luckyDrawData.add(new LuckyDrawData(0, sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                                        sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                                        sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                                        sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""),
                                        finalFocus_area, "", "", imagesPath));
                                appDatabase.luckyDrawOfflineDao().insertAll(luckyDrawData);
                            }
                        });

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Functions.hideProgress(homeActivity, pb);
                                txtGoOn.setVisibility(View.VISIBLE);
                                mSelectedList = null;
                                mCurrentPhotoPath = "";
                                imagesPath.clear();
                                if (bottomSheetThumbnail != null && bottomSheetThumbnail.isShowing()) {
                                    bottomSheetThumbnail.dismiss();
                                }

                                openSuccessForHealthy();
                            }
                        }, 1000);

                    } else {
                        uploadWeeklyPicture(pb, txtGoOn);
                    }

                } else {
                    String focus_area = "";
                    if (mSelectedList != null) {
                        focus_area = mSelectedList.getType();
                    }

                    final String finalFocus_area = focus_area;
                    Executors.newSingleThreadExecutor().execute(new Runnable() {
                        @Override
                        public void run() {
                            List<LuckyDrawData> luckyDrawData = new ArrayList<>();
                            luckyDrawData.add(new LuckyDrawData(0, sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                                    sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                                    sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                                    sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""),
                                    finalFocus_area, "", "", imagesPath));
                            appDatabase.luckyDrawOfflineDao().insertAll(luckyDrawData);
                        }
                    });

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Functions.hideProgress(homeActivity, pb);
                            txtGoOn.setVisibility(View.VISIBLE);
                            mSelectedList = null;
                            mCurrentPhotoPath = "";
                            imagesPath.clear();
                            if (bottomSheetThumbnail != null && bottomSheetThumbnail.isShowing()) {
                                bottomSheetThumbnail.dismiss();
                            }

                            openSuccessForHealthy();
                        }
                    }, 1000);

                }
            }
        });

        bottomSheetThumbnail.show();
    }

    BottomSheetDialog bottomSheetDialogGuideLine;

    void openGuidlinesDialog() {
        bottomSheetDialogGuideLine = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_guideline_for_photo_capture, null);
        bottomSheetDialogGuideLine.setContentView(view);
        bottomSheetDialogGuideLine.setCanceledOnTouchOutside(false);
        bottomSheetDialogGuideLine.setCancelable(false);

        AppCompatTextView txtUpload = view.findViewById(R.id.txtUpload);
        AppCompatTextView txtLuckyDraw = view.findViewById(R.id.txtLuckyDraw);
        AppCompatTextView txtDescription = view.findViewById(R.id.txtDescription);
        AppCompatImageView imgGuideline = view.findViewById(R.id.imgGuideline);
        txtLuckyDraw.setVisibility(View.GONE);

        if (mSelectedList != null) {
            Functions.displayImage(mSelectedList.getImage(), imgGuideline);
            txtDescription.setText(mSelectedList.getDescription());
        }

        final ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogGuideLine.dismiss();
                if (bottomSheetDialogCrop != null) {
                    bottomSheetDialogCrop.show();
                }
            }
        });

        txtUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogGuideLine.dismiss();
                accessPermission();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(homeActivity.getResources().getColor(android.R.color.transparent));

        bottomSheetDialogGuideLine.show();
    }

    BottomSheetDialog bottomSheetDialogLabel;
    AppCompatTextView txtDarjKare;
    String forPhotoLabel = "N";
    ImageView imgPhotoLabel;

    void openPhotoLabelDialog(String path) {
        bottomSheetDialogLabel = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_photo_label_upload, null);
        bottomSheetDialogLabel.setContentView(view);
        bottomSheetDialogLabel.setCancelable(false);
        bottomSheetDialogLabel.setCanceledOnTouchOutside(false);

        AppCompatTextView txtUpload = view.findViewById(R.id.txtUpload);
        txtDarjKare = view.findViewById(R.id.txtDarjKare);
        final LinearLayout linearCapture = view.findViewById(R.id.linearCapture);
        final ProgressBar pb = view.findViewById(R.id.pb);
        final RelativeLayout relativeLabelUpload = view.findViewById(R.id.relativeLabelUpload);
        imgPhotoLabel = view.findViewById(R.id.imgPhotoLabel);
        ImageView imgDelete = view.findViewById(R.id.imgDelete);

        if (forPhotoLabel.equalsIgnoreCase("Y")) {
            linearCapture.setVisibility(View.GONE);
            relativeLabelUpload.setVisibility(View.VISIBLE);
            Functions.displayImage(path, imgPhotoLabel);
        } else {
            linearCapture.setVisibility(View.VISIBLE);
            relativeLabelUpload.setVisibility(View.GONE);
        }

        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forPhotoLabel = "N";

                imgPhotoLabel.setImageResource(0);
                imgPhotoLabel.setImageBitmap(null);

                linearCapture.setVisibility(View.VISIBLE);
                relativeLabelUpload.setVisibility(View.GONE);
            }
        });

        final ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogLabel.dismiss();
                forPhotoLabel = "N";
            }
        });

        txtUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forPhotoLabel = "Y";

                bottomSheetDialogLabel.dismiss();
                accessPermission();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(homeActivity.getResources().getColor(android.R.color.transparent));

        txtDarjKare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forPhotoLabel = "N";
                bottomSheetDialogLabel.dismiss();
            }
        });
        bottomSheetDialogLabel.show();
    }

    RequestBody getRequestFile(File file) {
        RequestBody fbody = RequestBody.create(file, MediaType.parse("image/*"));
        return fbody;
    }

    void uploadWeeklyPicture(final ProgressBar pb,
                             final AppCompatTextView appCompatTextView) {
        appCompatTextView.setVisibility(View.GONE);
        Functions.showProgress(homeActivity, pb);
        String focus_area = "";
        if (mSelectedList != null) {
            focus_area = mSelectedList.getType();
        }
        RequestBody requestBodyId = RequestBody.create(SharedPreferencesUtils.getUserId(homeActivity).trim(), MediaType.parse("text/plain"));
        RequestBody seasonIdBody = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyCropId = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyPCropId = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyCropWeek = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyDeviceId = RequestBody.create(Functions.getDeviceid(homeActivity), MediaType.parse("text/plain"));
        RequestBody requestBodyFCMToken = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyPictureArea = RequestBody.create(focus_area, MediaType.parse("text/plain"));
        RequestBody requestBodylatitude = RequestBody.create("", MediaType.parse("text/plain"));
        RequestBody requestBodylongitude = RequestBody.create("", MediaType.parse("text/plain"));
        RequestBody requestBodyAppVersion = RequestBody.create(RongoApp.getAppVersion(), MediaType.parse("text/plain"));
        RequestBody requestBodyAppLanguage = RequestBody.create(RongoApp.getDefaultLanguage(), MediaType.parse("text/plain"));
        RequestBody requestBodyAppSeason = RequestBody.create(RongoApp.getSeasonName(), MediaType.parse("text/plain"));

        Map<String, RequestBody> params = new HashMap<>();
        params.put("user_id", requestBodyId);
        params.put("season_id", seasonIdBody);
        params.put("crop_id", requestBodyCropId);
        params.put("p_crop_id", requestBodyPCropId);
        params.put("crop_week", requestBodyCropWeek);
        params.put("picture_area_focused", requestBodyPictureArea);
        params.put("geo_latitude", requestBodylatitude);
        params.put("geo_longitude", requestBodylongitude);
        params.put("device_id", requestBodyDeviceId);
        params.put("fcm_token", requestBodyFCMToken);
        params.put("app_version", requestBodyAppVersion);
        params.put("app_language", requestBodyAppLanguage);
        params.put("season_name", requestBodyAppSeason);

        if (imagesPath != null && imagesPath.size() > 0) {
            RequestBody requestBodyImageUploadedPath = RequestBody.create(imagesPath.toString(), MediaType.parse("text/plain"));
            params.put("picture_key_local", requestBodyImageUploadedPath);

            for (int i = 0; i < imagesPath.size(); i++) {
                File file = new File(imagesPath.get(i).toString().trim());
                try {
                    File compressedImages = new ImageZipper(homeActivity).compressToFile(file);
                    params.put("picture[]" + "\"; filename=\"" + file.getName(), getRequestFile(compressedImages));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        RetrofitClient.getInstance().getApi().uploadWeeklyPicture(params).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    Functions.hideProgress(homeActivity, pb);
                    appCompatTextView.setVisibility(View.VISIBLE);
                    String str = "";
                    try {
                        str = response.body().string();
                        if (!TextUtils.isEmpty(str)) {
                            JSONObject jsonObject = new JSONObject(str);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {

                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");
                                    mSelectedList = null;

                                    openSuccessForHealthy();
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Functions.hideProgress(homeActivity, pb);
                        appCompatTextView.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Functions.hideProgress(homeActivity, pb);
                appCompatTextView.setVisibility(View.GONE);
            }
        });
    }

    void accessPermission() {
        Dexter.withContext(homeActivity)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    void showImagePickerOptions() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(homeActivity);
        View sheetView = homeActivity.getLayoutInflater().inflate(R.layout.bottom_photo_uplaod_options, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);

        LinearLayout linearCamera = sheetView.findViewById(R.id.linearCamera);
        LinearLayout linearGallery = sheetView.findViewById(R.id.linearGallery);
        sheetView.findViewById(R.id.imgClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                if (bottomSheetDialogGuideLine != null) {
                    bottomSheetDialogGuideLine.show();
                }
            }
        });

        linearCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                dispatchTakePictureIntent();
            }
        });

        linearGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, GALLERY_REQUEST_CODE);
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        ((View) sheetView.getParent()).setBackgroundColor(homeActivity.getResources().getColor(android.R.color.transparent));
        mBottomSheetDialog.show();
    }

    private void dispatchTakePictureIntent() {
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File photoFile = createImageFile();
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(HomeActivity.this, providerAuthority, photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                List<ResolveInfo> resolvedIntentActivities = this.getPackageManager().queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolvedIntentInfo : resolvedIntentActivities) {
                    String packageName = resolvedIntentInfo.activityInfo.packageName;
                    this.grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_container_details);
        Objects.requireNonNull(fragment).onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.WHATS_APP_SHARE_CODE) {
                Log.e("WHATS_APP_SHARE_CODE", "" + Constants.WHATS_APP_SHARE_CODE);
            }

            if (requestCode == Constants.FACEBOOK_SHARE_CODE) {
                Log.e("FACEBOOK_SHARE_CODE", "" + Constants.FACEBOOK_SHARE_CODE);
            }

            if (requestCode == Constants.MORE_SHARE_CODE) {
                Log.e("MORE_SHARE_CODE", "" + Constants.MORE_SHARE_CODE);
            }

            if (requestCode == Constants.SMS_SHARE_CODE) {
                Log.e("SMS_SHARE_CODE", "" + Constants.SMS_SHARE_CODE);
            }
        }

        if (requestCode == Constants.POST_QUESTION_CODE && resultCode == Activity.RESULT_OK) {
            Functions.openPostQuestionSuccessDialog(homeActivity, appDatabase, RewardPoints.getPostQuestionRewardPoint(sharedPreferences), RewardPoints.getInviteRewardPoint(sharedPreferences));
        }

        if (requestCode == CAMERA_REQUEST_CODE || requestCode == GALLERY_REQUEST_CODE) {
            try {
                if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
//                    Uri uri;
//                    if (Build.VERSION.SDK_INT >= 24) {
//                        uri = FileProvider.getUriForFile(homeActivity, BuildConfig.APPLICATION_ID + ".provider", new File(mCurrentPhotoPath));
//                    } else {
//                        uri = Uri.fromFile(new File(mCurrentPhotoPath));
//                    }
//                    mCurrentPhotoPath = uri.getPath();
//                    if (mCurrentPhotoPath.contains("/root")) {
//                        mCurrentPhotoPath = mCurrentPhotoPath.replace("/root", "");
//                    }
                    if (data != null && data.hasExtra("mCurrentPhotoPath")) {
                        mCurrentPhotoPath = data.getStringExtra("mCurrentPhotoPath");
                    }
                    File f = new File(mCurrentPhotoPath);
                    Uri contentUri = Uri.fromFile(f);
                    mCurrentPhotoPath = contentUri.getPath();
                }

                if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {
//                    Uri selectedImage = data.getData();
//                    String[] filePath = {MediaStore.Images.Media.DATA};
//                    Cursor c = homeActivity.getContentResolver().query(selectedImage, filePath, null, null, null);
//                    c.moveToFirst();
//                    int columnIndex = c.getColumnIndex(filePath[0]);
//                    mCurrentPhotoPath = c.getString(columnIndex);
//                    c.close();
//                    if (mCurrentPhotoPath == null) {
//                        Bitmap googlepath = Functions.getBitmapFromUri(selectedImage, homeActivity);
//                        mCurrentPhotoPath = Functions.getGooglePhotoImagePath(googlepath, homeActivity);
//                    }
//                    if (mCurrentPhotoPath.contains("/root")) {
//                        mCurrentPhotoPath = mCurrentPhotoPath.replace("/root", "");
//                    }
                    Uri selectedImage = data.getData();
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    mCurrentPhotoPath = c.getString(columnIndex);
                    c.close();
                    if (mCurrentPhotoPath == null) {
                        Bitmap googlepath = Functions.getBitmapFromUri(selectedImage, this);
                        mCurrentPhotoPath = Functions.getGooglePhotoImagePath(googlepath, this);
                    }
                    if (mCurrentPhotoPath.contains("/root")) {
                        mCurrentPhotoPath = mCurrentPhotoPath.replace("/root", "");
                    }
                }

                if (forPhotoLabel.equalsIgnoreCase("Y")) {
                    openPhotoLabelDialog(mCurrentPhotoPath);
                } else {
                    imagesPath.add(mCurrentPhotoPath);
                    openThumbnailDialog();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    void openSuccessForHealthy() {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_success_for_healthy, null);
        bottomSheetDialog.setContentView(view);

        AppCompatTextView txtOfflineSMS = view.findViewById(R.id.txtOfflineSMS);
        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
            txtOfflineSMS.setVisibility(View.GONE);
        } else {
            txtOfflineSMS.setVisibility(View.VISIBLE);
        }

        AppCompatTextView tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText(RewardPoints.getCropHealthRewardPoint(sharedPreferences));

        AppCompatTextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getCropHealthRewardPoint(sharedPreferences));

        AppCompatTextView tvShareWhatapp = view.findViewById(R.id.tvShareWhatapp);
        tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "WhatsApp");
                    Functions.shareWhatAppIntent(homeActivity);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(homeActivity, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        AppCompatTextView tvShareCopy = view.findViewById(R.id.tvShareCopy);
        tvShareCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "SMS");
                    Functions.shareSMSIntent(homeActivity, RongoApp.getReferralText(homeActivity));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AppCompatTextView tvSharefacebook = view.findViewById(R.id.tvSharefacebook);
        tvSharefacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.setUserScreenTrackEvent(appDatabase, "Invite", "Facebook");
                Functions.shareFacebookIntent(homeActivity);
            }
        });

        AppCompatTextView tvShareMore = view.findViewById(R.id.tvShareMore);
        tvShareMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.performShare(homeActivity, appDatabase);
            }
        });

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }


    public void setInAppUpdate() {
        // Creates instance of the manager.
        mAppUpdateManager = AppUpdateManagerFactory.create(this);
        // Returns an intent object that you use to check for an update.
        com.google.android.play.core.tasks.Task<AppUpdateInfo> appUpdateInfoTask = mAppUpdateManager.getAppUpdateInfo();
        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(new com.google.android.play.core.tasks.OnSuccessListener<AppUpdateInfo>() {
            @Override
            public void onSuccess(AppUpdateInfo appUpdateInfo) {
                Log.i("AVAILABLE_VERSION_CODE", appUpdateInfo.availableVersionCode() + "");
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                        // For a flexible update, use AppUpdateType.FLEXIBLE
                        && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                    // Request the update.
                    try {
                        mAppUpdateManager.startUpdateFlowForResult(
                                // Pass the intent that is returned by 'getAppUpdateInfo()'.
                                appUpdateInfo,
                                // Or 'AppUpdateType.FLEXIBLE' for flexible updates.
                                AppUpdateType.IMMEDIATE,
                                // The current activity making the update request.
                                HomeActivity.this,
                                // Include a request code to later monitor this update request.
                                1100);
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        mAppUpdateManager.registerListener(installStateUpdatedListener);
    }

    InstallStateUpdatedListener installStateUpdatedListener = new InstallStateUpdatedListener() {
        @Override
        public void onStateUpdate(InstallState state) {
            if (state.installStatus() == InstallStatus.DOWNLOADED) {
                if (mAppUpdateManager != null) {
                    mAppUpdateManager.completeUpdate();
                }
                Log.i("mAppUpdateManager", "InstallStateUpdatedListener: state: Done" + state.installStatus());
            } else if (state.installStatus() == InstallStatus.INSTALLED) {
                if (mAppUpdateManager != null) {
                    mAppUpdateManager.unregisterListener(installStateUpdatedListener);
                }
            }
        }
    };

    public static void setWhatsAppNotification() {
        Dialog whatsAppDialog = new Dialog(homeActivity, R.style.NewDialog);
        whatsAppDialog.setContentView(R.layout.whatsapp_notification_dialog);
        whatsAppDialog.setCanceledOnTouchOutside(false);
        whatsAppDialog.setCancelable(false);
        whatsAppDialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = whatsAppDialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        ImageView imgClose = whatsAppDialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                whatsAppDialog.dismiss();
            }
        });

        AppCompatTextView tvHa = whatsAppDialog.findViewById(R.id.tvHa);
        tvHa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                whatsAppDialog.dismiss();
                updateUserConsent("1");
            }
        });

        AppCompatTextView tvNa = whatsAppDialog.findViewById(R.id.tvNa);
        tvNa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                whatsAppDialog.dismiss();
                updateUserConsent("0");
            }
        });
    }

    public static void updateUserConsent(String status) {
        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
            RetrofitClient.getInstance().getApi().updateUserConsent(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    RongoApp.getUserId(), "whatsapp", status).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response != null && response.isSuccessful()) {
                            String r = response.body().string();
                            if (!TextUtils.isEmpty(r)) {
                                JSONObject jsonObject = new JSONObject(r);
                                if (jsonObject != null && jsonObject.length() > 0) {
                                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                        JSONArray jsonData = new JSONArray(jsonObject.optString("data"));
                                        if (jsonData != null && jsonData.length() > 0) {
                                            JSONObject object = jsonData.optJSONObject(0);
                                            String whatsApp = object.optString("whatsapp");
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_WhatsApp_Notif, "" + whatsApp);

                                            updateWhatsAppStatusDialog(status);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        }
    }

    public static void updateWhatsAppStatusDialog(String whatsAppStatus) {
        Dialog dialog = new Dialog(homeActivity, R.style.NewDialog);
        dialog.setContentView(R.layout.whatsapp_status_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);

        ImageView imgClose = dialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        TextView tvDone = dialog.findViewById(R.id.tvDone);
        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        ImageView ivWhatsAppStatus = dialog.findViewById(R.id.ivWhatsAppStatus);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);

        if (whatsAppStatus.equalsIgnoreCase("1")) {
            tvTitle.setText(homeActivity.getResources().getString(R.string.str_whatapp_status_yes_txt));
            ivWhatsAppStatus.setImageResource(R.mipmap.whatsapp_yes_btn);
        } else {
            tvTitle.setText(homeActivity.getResources().getString(R.string.str_whatapp_status_no_txt));
            ivWhatsAppStatus.setImageResource(R.mipmap.whatsapp_no_btn);

            TextView view = new TextView(homeActivity);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(10, 2, 20, 10);
            view.setLayoutParams(params);
            view.setTag(0);
            view.setText(homeActivity.getResources().getString(R.string.str_whatapp_status_no_txt));

            String originalText = (String) view.getText();
            int startPosition = 14;
            int endPosition = startPosition + 8;
            SpannableString spannableStr = new SpannableString(originalText);
            UnderlineSpan underlineSpan = new UnderlineSpan();
            spannableStr.setSpan(underlineSpan, startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            ForegroundColorSpan backgroundColorSpan = new ForegroundColorSpan(Color.rgb(255, 69, 0));
            spannableStr.setSpan(backgroundColorSpan, startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            spannableStr.setSpan(new RelativeSizeSpan(1.1f), startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

            tvTitle.setText(spannableStr);
        }
    }


    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

}