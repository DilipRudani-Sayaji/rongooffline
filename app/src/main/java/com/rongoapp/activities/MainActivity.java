package com.rongoapp.activities;

import static com.rongoapp.controller.RongoApp.mAppOpenData;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.messaging.FirebaseMessaging;
import com.rongoapp.R;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.CropSwitchCallback;
import com.rongoapp.controller.CropSwitchTask;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.LocaleManager;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.TrackUserData;
import com.rongoapp.data.TrackUserOfflineData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    String type = "", id = "";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        LocalNotificationAlert.scheduleNotification(MainActivity.this, "no_action", 10000);

        appDatabase = AppDatabase.getDatabase(this);
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
//        Log.e("FacebookHashKey: ", "" + AppSignatureHelper.generateFacebookHashKey(MainActivity.this));

        ImageView ivLogo = (ImageView) findViewById(R.id.ivLogo);
        Functions.setRongoLogo(ivLogo);

        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        Functions.setFirebaseLogEventTrack(MainActivity.this, Constants.Screen_Track, "Main", "", "");
        RongoApp.getInstance().mFirebaseAnalytics.setUserProperty("UserID", SharedPreferencesUtils.getUserId(MainActivity.this));
        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_App_Update, "1");
        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Salah_Community, "");

        RongoApp.getSeasonNameFunction();
        setDynamicLink();
        init();

        /*
         * TODO Check: Assets Json File validator Function
         * */
//        DataValidator.parseCropDataFormat(MainActivity.this);
    }

    void init() {
        try {
            RongoApp.getMasterCropData(true);
            RongoApp.getCityStateData(true);
            RongoApp.getMasterRewardPoints(false);
            getFirebaseToken();
            setUserTrackEventFunction();
            if (!TextUtils.isEmpty(SharedPreferencesUtils.getUserId(this))) {
                RongoApp.getCropWeekDaysFunction();
                if (InternetConnection.checkConnectionForFasl(MainActivity.this)) {
                    if (!InternetConnection.getNetworkClass(MainActivity.this).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                        setNextEventFunction();
                    }
                    Intent in = getIntent();
                    if (in != null && in.getData() != null) {
                        Uri data = in.getData();
                        if (data != null) {
                            Log.v("trackUserLink", "" + data);
                            String[] separated = data.toString().split("app/");
                            if (!TextUtils.isEmpty(separated[1])) {
//                                Log.e("track--", "" + separated[1]);
                                String[] sep = separated[1].toString().split("/");
//                                Log.v("track", "" + sep[0] + " - " + Functions.decodeToString(sep[1]) + " - " + Functions.decodeToString(sep[2]));
//                                Log.v("track", "" + sep.length);
                                if (sep.length == 3) {
                                    Log.v("track3", "" + sep[0]);
                                    Log.v("track3", "" + sep[1]);
                                    Log.v("track3", "" + sep[2]);
                                    submitLinkLog(sep[0], Functions.decodeToString(sep[1]), Functions.decodeToString(sep[2]));
                                } else if (sep.length == 2) {
                                    Log.v("track2", "" + sep[0]);
                                    Log.v("track2", "" + sep[1]);
                                    submitLinkLog(sep[0], Functions.decodeToString(sep[1]), "");
                                } else {
                                    submitLinkLog(sep[0], "0", "");
                                }
                            }
                        }
                    }
                }

                Bundle b = getIntent().getExtras();
                if (b != null) {
                    if (b.containsKey("type")) {
                        type = b.getString("type");
                    }
                    if (b.containsKey("id")) {
                        id = b.getString("id");
                    }
                    Intent intent;
                    if (!TextUtils.isEmpty(type) && !TextUtils.isEmpty(id)) {
                        if (type.equalsIgnoreCase(Constants.POST)) {
                            intent = new Intent(this, CommunityDetailsActivity.class);
                            intent.putExtra("post_id", id);
                            intent.putExtra("fromNotification", "Y");
                        } else if (type.equalsIgnoreCase(Constants.KEYWORD)) {
                            intent = new Intent(this, SavdhaniDetailsActivity.class);
                            intent.putExtra("keyword_id", id);
                            intent.putExtra("fromNotification", "Y");
                        } else if (type.equalsIgnoreCase(Constants.WEATHER)) {
                            intent = new Intent(this, WeatherDetailsActivity.class);
                            intent.putExtra("weather_alert", id);
                            intent.putExtra("fromNotification", "Y");
                        } else if (type.equalsIgnoreCase(Constants.PAGE)) {
                            intent = new Intent(this, HomeActivity.class);
                            intent.putExtra("pageType", id);
                            intent.putExtra("fromLaunch", "Y");
                        } else {
                            intent = new Intent(this, HomeActivity.class);
                            intent.putExtra("fromLaunch", "Y");
                        }
                        startActivity(intent);
                        finish();
                    } else {
                        nextScreen();
                    }
                } else {
                    nextScreen();
                }
            } else {
                nextScreen();
            }
        } catch (Exception e) {
            e.printStackTrace();
            nextScreen();
        }
    }

    void setDynamicLink() {
        try {
            FirebaseDynamicLinks.getInstance()
                    .getDynamicLink(getIntent())
                    .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                        @Override
                        public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                            // Get deep link from result (may be null if no link is found)
                            Uri deepLink = null;
                            if (pendingDynamicLinkData != null) {
                                deepLink = pendingDynamicLinkData.getLink();
                            }

                            if (deepLink != null && !TextUtils.isEmpty(deepLink.toString())) {
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_DynamicLink, "" + deepLink);
                                Functions.setUserScreenTrackEvent(appDatabase, "deepLink", "" + deepLink);
                            }
                        }
                    })
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w("TAG", "getDynamicLink:onFailure", e);
                        }
                    });

            Intent in = getIntent();
            if (in != null && in.getData() != null) {
                Uri deepLink = in.getData();
                if (deepLink != null && !TextUtils.isEmpty(deepLink.toString())) {
                    Log.e("trackUserLink", "" + deepLink);
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_DynamicLink, "" + deepLink);
                    Functions.setUserScreenTrackEvent(appDatabase, "deepLink", "" + deepLink);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void getFirebaseToken() {
        try {
            if (TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""))) {
                FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (task.isSuccessful()) {
                            Log.v("deviceInfo token11:", task.getResult());
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_FCM_TOKEN, "" + task.getResult());
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void nextScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!TextUtils.isEmpty(SharedPreferencesUtils.getUserId(MainActivity.this)) && sharedPreferences.getString(Constants.SharedPreferences_VERIFY_OTP, "N").equalsIgnoreCase("Y")) {
                    Intent i = new Intent(MainActivity.this, HomeActivity.class);
                    i.putExtra("fromLaunch", "Y");
                    startActivity(i);
                    finish();
                } else if (!TextUtils.isEmpty(SharedPreferencesUtils.getUserId(MainActivity.this)) && sharedPreferences.getString(Constants.SharedPreferences_VERIFY_OTP, "N").equalsIgnoreCase("N")) {
                    Intent i = new Intent(MainActivity.this, OTPActivity.class);
                    i.putExtra("fromSplash", "Y");
                    startActivity(i);
                    finish();
                } else {
                    if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_MOBILE_NO, ""))) {
                        if (InternetConnection.checkConnectionForFasl(MainActivity.this)) {
                            if (sharedPreferences.getString(Constants.SharedPreferences_VERIFY_OTP, "N").equalsIgnoreCase("N")) {
                                Intent i = new Intent(MainActivity.this, OTPActivity.class);
                                i.putExtra("fromSplash", "Y");
                                startActivity(i);
                                finish();
                            } else {
                                Intent i = new Intent(MainActivity.this, HomeActivity.class);
                                i.putExtra("fromLaunch", "Y");
                                startActivity(i);
                                finish();
                            }
                        } else {
                            Intent i = new Intent(MainActivity.this, HomeActivity.class);
                            i.putExtra("fromLaunch", "Y");
                            startActivity(i);
                            finish();
                        }
                    } else {
                        Intent i = new Intent(MainActivity.this, LoginOptionsActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
            }
        }, 2800);
    }


    void submitLinkLog(String medium, String source, String resource) {
        RetrofitClient.getInstance().getApi().submitLinkLog(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                SharedPreferencesUtils.getUserId(MainActivity.this), source, medium, resource).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!TextUtils.isEmpty(resource)) {
                    Intent intent = new Intent(MainActivity.this, CommunityDetailsActivity.class);
                    intent.putExtra("post_id", resource);
                    intent.putExtra("type", "0");
                    intent.putExtra("fromNotification", "Y");
                    startActivity(intent);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void setNextEventFunction() {
        try {
            CropSwitchTask cropSwitchTask = new CropSwitchTask(this, "", (CropSwitchCallback) result -> {
                Log.e("Api Call Done", "" + result);
            });
            cropSwitchTask.executeAsync("Test");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void setUserTrackEventFunction() {
        try {
            if (!TextUtils.isEmpty(SharedPreferencesUtils.getUserId(this))) {
                String appSpentTime = "", userTrackData = "";
                List<TrackUserData> trackUserData = appDatabase.trackUserDao().getAll();
                if (trackUserData.size() > 1) {
                    appSpentTime = getAppSpentTimeInMintues();
                    userTrackData = getUserTrackData();

                    if (InternetConnection.checkConnectionForFasl(MainActivity.this)) {
                        updateAppActivity(appSpentTime, userTrackData);
                    } else {
                        TrackUserOfflineData user = new TrackUserOfflineData();
                        user.setUid(new Random().nextInt());
                        if (sharedPreferences.getString(Constants.SharedPreference_IsAction, "N").equalsIgnoreCase("Y")) {
                            user.setAction(true);
                        } else {
                            user.setAction(false);
                        }
                        user.setLastLoginTime(sharedPreferences.getString(Constants.SharedPreference_LastLoginTime, ""));
                        user.setUserId(SharedPreferencesUtils.getUserId(this));
                        user.setAppTimeSpent(appSpentTime);
                        user.setUserTrackEvent(userTrackData);
                        appDatabase.trackUserOfflineDao().insertAll(user);
                    }
                    Functions.setLastAppOpenTime(sharedPreferences);
                    appDatabase.trackUserDao().nukeTable();
                    Functions.setUserScreenTrackEvent(appDatabase, "Main", "0");
                } else {
                    Functions.setLastAppOpenTime(sharedPreferences);
                    appDatabase.trackUserDao().nukeTable();
                    Functions.setUserScreenTrackEvent(appDatabase, "Main", "0");
                }
            } else {
                Functions.setLastAppOpenTime(sharedPreferences);
                Functions.setUserScreenTrackEvent(appDatabase, "Main", "0");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String getUserTrackData() {
        String userdata = "";
        try {
            List<TrackUserData> trackUserData = appDatabase.trackUserDao().getAll();
            if (trackUserData.size() > 0) {
                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < trackUserData.size(); i++) {
                    JSONObject obj = new JSONObject();
                    obj.put("screen_name", trackUserData.get(i).getScreenName());
                    obj.put("id", trackUserData.get(i).getActivityId());
                    obj.put("time", trackUserData.get(i).getDateTime());
                    jsonArray.put(obj);
                }
                userdata = jsonArray.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userdata;
    }

    String getAppSpentTimeInMintues() {
        String mintes = "0";
        Date startDate = null, endDate = null;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        List<TrackUserData> trackUserData = appDatabase.trackUserDao().getAll();
        if (trackUserData.size() > 1) {
            try {
                startDate = dateFormat.parse(sharedPreferences.getString(Constants.SharedPreference_LastLoginTime, ""));
                endDate = dateFormat.parse(trackUserData.get(trackUserData.size() - 1).getDateTime());

                //milliseconds
                long different = endDate.getTime() - startDate.getTime();

                System.out.println("startDate : " + startDate);
                System.out.println("endDate : " + endDate);
                System.out.println("different : " + different);

                long secondsInMilli = 1000;
                long minutesInMilli = secondsInMilli * 60;
                long hoursInMilli = minutesInMilli * 60;
                long daysInMilli = hoursInMilli * 24;

                long elapsedDays = different / daysInMilli;
                different = different % daysInMilli;
                long elapsedHours = different / hoursInMilli;
                different = different % hoursInMilli;
                long elapsedMinutes = different / minutesInMilli;
                System.out.println("elapsedMinutes : " + elapsedMinutes);

                if (elapsedMinutes < 0)
                    elapsedMinutes = -elapsedMinutes;

                mintes = String.valueOf(elapsedMinutes);
            } catch (Exception e) {
                e.printStackTrace();
                return mintes;
            }
        }
        return mintes;
    }

    void updateAppActivity(String appSpentTime, String userTrackData) {
        try {
            boolean isAction = false;
            if (sharedPreferences.getString(Constants.SharedPreference_IsAction, "N").equalsIgnoreCase("Y")) {
                isAction = true;
            }

            JSONArray jsonArray = new JSONArray();
            JSONObject obj = new JSONObject();
            obj.put("model_no_name", mAppOpenData.getmModel());
            obj.put("device_os", mAppOpenData.getmOsVersion());
            obj.put("device_brand_name", mAppOpenData.getmBrand());
            obj.put("device_id", mAppOpenData.getAndroidId());
            obj.put("error_log", sharedPreferences.getString(Constants.SharedPreference_Error_Log, ""));
            jsonArray.put(obj);

            RetrofitClient.getInstance().getApi().updateAppActivity(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), SharedPreferencesUtils.getUserId(this),
                    sharedPreferences.getString(Constants.SharedPreference_LastLoginTime, ""), appSpentTime,
                    sharedPreferences.getString(Constants.SharedPreference_NetworkType, ""),
                    sharedPreferences.getString(Constants.SharedPreference_NetworkSpeed, ""),
                    isAction, userTrackData, jsonArray.toString()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }

}