package com.rongoapp.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.rongoapp.R;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.InitInterface;
import com.rongoapp.controller.LocaleManager;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.network.DroidListener;
import com.rongoapp.network.DroidNet;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginOptionsActivity extends AppCompatActivity implements InitInterface, DroidListener {

    DroidNet mDroidNet;
    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    AppCompatTextView txtRegister, txtLogin;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_register_options);

        DroidNet.init(this);
        mDroidNet = DroidNet.getInstance();
        mDroidNet.addInternetConnectivityListener(this);
        Functions.setFirebaseLogEventTrack(LoginOptionsActivity.this, Constants.Screen_Track, "landing_screen", "", "");
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(this);
        Functions.setUserScreenTrackEvent(appDatabase, "landing_screen", "0");

        accessStoragePermission();
        init();
        setListener();
    }

    void accessStoragePermission() {
        Dexter.withContext(LoginOptionsActivity.this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_SMS, Manifest.permission.READ_PHONE_NUMBERS)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    @Override
    public void init() {
        txtRegister = findViewById(R.id.txtRegister);
        txtLogin = findViewById(R.id.txtLogin);
        ImageView ivLogo = (ImageView) findViewById(R.id.ivLogo);
        Functions.setRongoLogo(ivLogo);

        if (!InternetConnection.checkConnectionForFasl(LoginOptionsActivity.this)) {
            txtLogin.setAlpha(0.2f);
            txtLogin.setEnabled(false);
        } else {
            txtLogin.setAlpha(1f);
            txtLogin.setEnabled(true);
        }

        if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""))) {
            if (InternetConnection.checkConnectionForFasl(this)) {
                getFirebaseToken();
            }
        }
    }

    @Override
    public void setListener() {
        txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Functions.setFirebaseLogEventTrack(LoginOptionsActivity.this, Constants.Click_Event, "landing_screen", "", "Login");
                Intent i = new Intent(LoginOptionsActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        txtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Functions.setFirebaseLogEventTrack(LoginOptionsActivity.this, Constants.Click_Event, "landing_screen", "", "register");
                Intent i = new Intent(LoginOptionsActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });
    }

    void getFirebaseToken() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            Log.e("deviceInfo1:-", "" + Build.BRAND);
            Log.e("deviceInfo2:-", "" + Build.MODEL);
            Log.e("deviceInfo3:-", "" + Build.DEVICE);
            Log.e("deviceInfo4:-", "" + Build.VERSION.RELEASE);
            Log.e("deviceInfo5:-", "" + telephonyManager.getNetworkOperatorName());
//                String number = "" + telephonyManager.getLine1Number();
//                Log.e("deviceInfo6:-", "" + number);

            FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
                @Override
                public void onComplete(@NonNull Task<String> task) {
                    if (task.isSuccessful()) {
                        Log.v("deviceInfo token11:", task.getResult());
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_FCM_TOKEN, "" + task.getResult());

                        if (InternetConnection.checkConnectionForFasl(LoginOptionsActivity.this)) {
                            RetrofitClient.getInstance().getApi().updateFCMToken(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                                    RongoApp.getAppVersion(), Functions.getDeviceid(LoginOptionsActivity.this), task.getResult(),
                                    Build.MODEL, Build.VERSION.RELEASE, Build.BRAND).enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                }
                            });
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        Log.i("LoginOption:-", "" + isConnected);
        if (txtLogin != null) {
            if (isConnected) {
                txtLogin.setAlpha(1f);
                txtLogin.setEnabled(true);
            } else {
                txtLogin.setAlpha(0.2f);
                txtLogin.setEnabled(false);
            }
        }
    }

}
