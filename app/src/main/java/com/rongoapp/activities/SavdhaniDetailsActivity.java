package com.rongoapp.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.card.MaterialCardView;
import com.rongoapp.R;
import com.rongoapp.adapter.ChemicalNameAdapter;
import com.rongoapp.adapter.CropProblemPostQuesAdapter;
import com.rongoapp.adapter.ImagesSavdhaniAdapter;
import com.rongoapp.adapter.ManagementAdapter;
import com.rongoapp.adapter.SymptomsAdapter;
import com.rongoapp.adapter.TextAdvisesAdapter;
import com.rongoapp.audioPlayerTTS.Speakerbox;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.FetchJsonFromAssetsFile;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.InitInterface;
import com.rongoapp.controller.LocaleManager;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.ChemicalNameModel;
import com.rongoapp.data.JankariFeedBackData;
import com.rongoapp.data.ProblemRelatedData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.GridSpacingItemDecoration;
import com.rongoapp.views.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SavdhaniDetailsActivity extends AppCompatActivity implements InitInterface {

    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    long mLastLongClick = 0;

    ProgressBar pbSuzav, pbLaxan, pbSalah;
    MaterialCardView cardView;
    FrameLayout frameLayout;
    NestedScrollView nsv;
    ViewPager pager;
    ImageView imgPrevios, imgNext, imgBack, imgPlaySuzav, imgPlaySalah, imgPlay;
    RelativeLayout relLaxan, relAdvise, relSuzav;
    RecyclerView rvSuzav, rvLaxan, rvGeneralAdvise;
    AppCompatTextView txtTitle, tvAlertTag, tvPoints, txtYes, txtNo, tvPointsAdvisoryFeedback, txtChecmicalName;
    LinearLayout llExpertView;

    int currentPositionTop = 0;
    String keyword_id = "", risk = "0", image_path = "", p_crop_id = "0", fromNotification = "N";

    List<String> imagesList = new ArrayList<>();
    List<String> symptomsList = new ArrayList<>();
    List<String> ADVISORY_FEEDBACK_KIDS_Array = new ArrayList<>();
    JSONArray chemical_info;

    Speakerbox speakerbox;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_savdhani_details);

        speakerbox = new Speakerbox(getApplication());
        speakerbox.setActivity(this);
        speakerbox.setLanguage(Functions.getLocaleLanguage());

        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(this);
        ADVISORY_FEEDBACK_KIDS_Array = SharedPreferencesUtils.getArrayList(SavdhaniDetailsActivity.this, Constants.ADVISORY_FEEDBACK_KIDS);
        p_crop_id = Functions.getP_Crop_Id(sharedPreferences);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            if (b.containsKey("keyword_id")) {
                keyword_id = b.getString("keyword_id");
            }
            if (b.containsKey("risk")) {
                risk = b.getString("risk");
            }
            if (b.containsKey("fromNotification")) {
                fromNotification = b.getString("fromNotification");
            }
        }
        Log.v("keyword_id", "" + keyword_id);
        Functions.setFirebaseLogEventTrack(SavdhaniDetailsActivity.this, Constants.Screen_Track, "Savdhani Detail", keyword_id, "");
        Functions.setUserScreenTrackEvent(appDatabase, "Savdhani Detail", "" + keyword_id);

        try {
            init();
            setListener();
            setLocalData();
            HomeActivity.setTextToSpeechInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() {
        pbLaxan = findViewById(R.id.pbLaxan);
        pbSuzav = findViewById(R.id.pbSuzav);
        pbSalah = findViewById(R.id.pbSalah);

        cardView = findViewById(R.id.cardView);
        nsv = findViewById(R.id.nsv);
        frameLayout = findViewById(R.id.frameLayout);
        imgBack = findViewById(R.id.imgBack);
        imgPlaySuzav = findViewById(R.id.imgPlaySuzav);
        imgPlaySalah = findViewById(R.id.imgPlaySalah);
        imgPlay = findViewById(R.id.imgPlay);
        imgPrevios = findViewById(R.id.imgPrevios);
        imgNext = findViewById(R.id.imgNext);
        pager = findViewById(R.id.pager);
        relLaxan = findViewById(R.id.relLaxan);
        relAdvise = findViewById(R.id.relAdvise);
        relSuzav = findViewById(R.id.relSuzav);
        rvSuzav = findViewById(R.id.rvSuzav);
        rvGeneralAdvise = findViewById(R.id.rvGeneralAdvise);
        rvLaxan = findViewById(R.id.rvLaxan);
        txtTitle = findViewById(R.id.txtTitle);
        txtYes = findViewById(R.id.txtYes);
        txtNo = findViewById(R.id.txtNo);
        rvLaxan.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rvSuzav.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rvGeneralAdvise.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        tvPointsAdvisoryFeedback = findViewById(R.id.tvPointsAdvisoryFeedback);
        tvPointsAdvisoryFeedback.setText(RewardPoints.getAdvisoryFeedbackRewardPoint(sharedPreferences));

        tvPoints = findViewById(R.id.tvPoints);
        tvPoints.setText(RewardPoints.getAdvisoryFeedbackRewardPoint(sharedPreferences));
        tvAlertTag = findViewById(R.id.tvAlertTag);
        llExpertView = findViewById(R.id.llExpertView);
        if (risk.equalsIgnoreCase("1")) {
            llExpertView.setVisibility(View.VISIBLE);
            tvAlertTag.setVisibility(View.VISIBLE);
        } else {
            llExpertView.setVisibility(View.GONE);
            tvAlertTag.setVisibility(View.GONE);
        }

        txtChecmicalName = findViewById(R.id.txtChecmicalName);
        txtChecmicalName.setVisibility(View.GONE);
        txtChecmicalName.setPaintFlags(txtChecmicalName.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        if (ADVISORY_FEEDBACK_KIDS_Array != null && ADVISORY_FEEDBACK_KIDS_Array.size() > 0) {
            if (ADVISORY_FEEDBACK_KIDS_Array.contains(keyword_id)) {
                cardView.setVisibility(View.GONE);
            } else {
                cardView.setVisibility(View.VISIBLE);
            }
        } else {
            ADVISORY_FEEDBACK_KIDS_Array = new ArrayList<>();
            cardView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setListener() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fromNotification.equalsIgnoreCase("Y")) {
                    Intent i = new Intent(SavdhaniDetailsActivity.this, HomeActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finishAffinity();
                    overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
                } else {
                    finish();
                }
            }
        });

        llExpertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                Functions.setFirebaseLogEventTrack(SavdhaniDetailsActivity.this, Constants.Click_Event, "Reward", "", "ExpertAdvise");
                Intent i = new Intent(SavdhaniDetailsActivity.this, PostQuestionActivity.class);
                i.putExtra("keyword_id", "0");
                startActivityForResult(i, Constants.POST_QUESTION_CODE);
            }
        });

        txtChecmicalName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                Functions.setFirebaseLogEventTrack(SavdhaniDetailsActivity.this, Constants.Click_Event, "Savdhani Detail", "", "Ask Expert");
                Intent i = new Intent(SavdhaniDetailsActivity.this, PostQuestionActivity.class);
                i.putExtra("keyword_id", "0");
                i.putExtra("type", "1");
                startActivityForResult(i, Constants.POST_QUESTION_CODE);
            }
        });

        txtChecmicalName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                openChemicalNameDialog();
            }
        });

        txtNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                if (InternetConnection.checkConnection(SavdhaniDetailsActivity.this)) {
                    if (InternetConnection.getNetworkClass(SavdhaniDetailsActivity.this).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                        setYesNoBtn("N");
                    } else {
                        saveAdvisoryFeedback("0");
                    }
                } else {
                    setYesNoBtn("N");
                }
            }
        });

        txtYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                if (InternetConnection.checkConnection(SavdhaniDetailsActivity.this)) {
                    if (InternetConnection.getNetworkClass(SavdhaniDetailsActivity.this).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                        setYesNoBtn("Y");
                    } else {
                        saveAdvisoryFeedback("1");
                    }
                } else {
                    setYesNoBtn("Y");
                }
            }
        });

        imgPrevios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pager.setCurrentItem(getNextPossibleItemIndex(-1));
            }
        });

        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pager.setCurrentItem(getNextPossibleItemIndex(1));
            }
        });

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                currentPositionTop = position;
//                if (currentPositionTop == 0) {
//                    imgNext.setVisibility(View.VISIBLE);
//                    imgPrevios.setVisibility(View.INVISIBLE);
//                } else if (currentPositionTop == imagesList.size() - 1) {
//                    imgNext.setVisibility(View.INVISIBLE);
//                    imgPrevios.setVisibility(View.VISIBLE);
//                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


        relLaxan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtChecmicalName.setVisibility(View.GONE);
                if (rvLaxan.getVisibility() == View.VISIBLE) {
                    rvLaxan.setVisibility(View.GONE);
                } else {
                    rvLaxan.setVisibility(View.VISIBLE);
                    rvGeneralAdvise.setVisibility(View.GONE);
                    rvSuzav.setVisibility(View.GONE);
                }
            }
        });

        relSuzav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rvSuzav.getVisibility() == View.VISIBLE) {
                    rvSuzav.setVisibility(View.GONE);
                    txtChecmicalName.setVisibility(View.GONE);
                } else {
                    rvLaxan.setVisibility(View.GONE);
                    rvGeneralAdvise.setVisibility(View.GONE);
                    rvSuzav.setVisibility(View.VISIBLE);

                    if (chemical_info != null && chemical_info.length() > 0) {
                        txtChecmicalName.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        relAdvise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtChecmicalName.setVisibility(View.GONE);
                if (rvGeneralAdvise.getVisibility() == View.VISIBLE) {
                    rvGeneralAdvise.setVisibility(View.GONE);
                } else {
                    rvLaxan.setVisibility(View.GONE);
                    rvGeneralAdvise.setVisibility(View.VISIBLE);
                    rvSuzav.setVisibility(View.GONE);
                }
            }
        });
    }

    void openChemicalNameDialog() {
        try {
            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(SavdhaniDetailsActivity.this);
            View view = LayoutInflater.from(SavdhaniDetailsActivity.this).inflate(R.layout.bottom_dialog_chemical_name, null);
            bottomSheetDialog.setContentView(view);

            ImageView imgClose = view.findViewById(R.id.imgClose);
            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bottomSheetDialog.dismiss();
                }
            });

            ArrayList<ChemicalNameModel> chemicalNameModels = new ArrayList<ChemicalNameModel>();
            for (int i = 0; i < chemical_info.length(); i++) {
                JSONObject object = chemical_info.optJSONObject(i);
                try {
                    chemicalNameModels.add(new ChemicalNameModel(object.optString("technical"),
                            Functions.getString(object.getJSONArray("normal"))));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            RecyclerView rcvChemicalName = view.findViewById(R.id.rcvChemicalName);
            rcvChemicalName.setLayoutManager(new LinearLayoutManager(this));
            rcvChemicalName.setAdapter(new ChemicalNameAdapter(chemicalNameModels));

            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
            ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
            bottomSheetDialog.show();
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    void setYesNoBtn(String text) {
        try {
            Functions.updateRewardPoints(sharedPreferences, RewardPoints.getAdvisoryFeedbackRewardPoint(sharedPreferences));
            Functions.setFirebaseLogEventTrack(SavdhaniDetailsActivity.this, Constants.Click_Event, "Savdhani Detail", keyword_id, text);

            List<JankariFeedBackData> jankariFeedBackData = new ArrayList<>();
            if (text.equalsIgnoreCase("Y")) {
                jankariFeedBackData.add(new JankariFeedBackData(0, Constants.saveAdvisoryFeedback, "1", p_crop_id, keyword_id, "0", ""));
                appDatabase.jankariFeedbackDao().insertAll(jankariFeedBackData);
                cardView.setVisibility(View.GONE);
                ADVISORY_FEEDBACK_KIDS_Array.add(keyword_id);
                SharedPreferencesUtils.saveArrayList(SavdhaniDetailsActivity.this, ADVISORY_FEEDBACK_KIDS_Array, Constants.ADVISORY_FEEDBACK_KIDS);
                openThankYou();
            } else {
                jankariFeedBackData.add(new JankariFeedBackData(0, Constants.saveAdvisoryFeedback, "0", p_crop_id, keyword_id, "0", ""));
                appDatabase.jankariFeedbackDao().insertAll(jankariFeedBackData);
                cardView.setVisibility(View.GONE);
                ADVISORY_FEEDBACK_KIDS_Array.add(keyword_id);
                SharedPreferencesUtils.saveArrayList(SavdhaniDetailsActivity.this, ADVISORY_FEEDBACK_KIDS_Array, Constants.ADVISORY_FEEDBACK_KIDS);
                openProblem();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setAudioView(boolean isPlay, String textVoice, ImageView imgPlay, ProgressBar progressBar) {
        try {
            if (isPlay) {
                Runnable onStart = new Runnable() {
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        imgPlay.setImageResource(R.mipmap.pause);
                    }
                };
                Runnable onDone = new Runnable() {
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        imgPlay.setImageResource(R.mipmap.play);
                    }
                };
                Runnable onError = new Runnable() {
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        imgPlay.setImageResource(R.mipmap.play);
                    }
                };
                speakerbox.play(textVoice, onStart, onDone, onError);
            } else {
                speakerbox.isMuted();
                speakerbox.stop();
                speakerbox.shutdown();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void getKeywordAdvisory() {
        RetrofitClient.getInstance().getApi().getKeywordAdvisory(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                RongoApp.getAppVersion(), SharedPreferencesUtils.getUserId(this),
                keyword_id, RongoApp.getSelectedCrop(), p_crop_id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String str = response.body().string();
                    if (!TextUtils.isEmpty(str)) {
                        JSONObject jsonObject = new JSONObject(str);
                        if (jsonObject != null && jsonObject.length() > 0) {
                            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {

                                JSONObject data = jsonObject.optJSONObject("data");
                                if (data != null && data.length() > 0) {
                                    JSONObject jsonObj = data.optJSONObject("result");
                                    if (jsonObj != null && jsonObj.length() > 0) {
                                        JSONObject jb = jsonObj.optJSONObject(keyword_id);
                                        if (jb != null && jb.length() > 0) {

                                            String name = jb.optString("name");
                                            txtTitle.setText(name);

                                            symptomsList = new ArrayList<>();
                                            JSONArray laxan = jb.optJSONArray("symptoms");
                                            String laxanString = "", newLaxan = "";
                                            if (laxan != null && laxan.length() > 0) {
                                                for (int i = 0; i < laxan.length(); i++) {
                                                    laxanString += Functions.fromHtml(laxan.optString(i));
                                                    symptomsList.add(laxan.optString(i));

                                                    JSONObject jObject = new JSONObject(laxan.optString(i));
                                                    newLaxan += Functions.getConvertHtml(jObject.optString("advisory"));
                                                    newLaxan += Functions.getConvertHtml(jObject.optString("note"));
                                                    newLaxan += Functions.getConvertHtml(jObject.optString("chemical_usage"));
                                                }
                                            }

                                            SymptomsAdapter symptomsAdapter = new SymptomsAdapter(symptomsList, SavdhaniDetailsActivity.this);
                                            rvLaxan.setAdapter(symptomsAdapter);

                                            String finalLaxanString = laxanString;
                                            String finalNewLaxan = newLaxan;
                                            imgPlay.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    pbLaxan.setVisibility(View.VISIBLE);
                                                    if (!TextUtils.isEmpty(finalNewLaxan)) {
                                                        setAudioView(true, finalNewLaxan, imgPlay, pbLaxan);
                                                    } else {
                                                        setAudioView(true, finalLaxanString, imgPlay, pbLaxan);
                                                    }
                                                }
                                            });

                                            chemical_info = jb.optJSONArray("chemical_info");
                                            if (chemical_info != null && chemical_info.length() > 0) {
                                                txtChecmicalName.setVisibility(View.VISIBLE);
                                            }

                                            List<String> managmentList = new ArrayList<>();
                                            String suzav = "", newSuzav = "";
                                            JSONArray management = jb.optJSONArray("management");
                                            if (management != null && management.length() > 0) {
                                                for (int i = 0; i < management.length(); i++) {
                                                    suzav += Functions.fromHtml(management.optString(i));
                                                    managmentList.add(management.optString(i));

                                                    JSONObject jObject = new JSONObject(management.optString(i));
                                                    newSuzav += Functions.getConvertHtml(jObject.optString("advisory"));
                                                    newSuzav += Functions.getConvertHtml(jObject.optString("note"));
                                                    newSuzav += Functions.getConvertHtml(jObject.optString("chemical_usage"));
                                                }
                                            }

                                            ManagementAdapter managementAdapter = new ManagementAdapter(SavdhaniDetailsActivity.this, managmentList);
                                            rvSuzav.setAdapter(managementAdapter);

                                            String finalSuzav = suzav;
                                            String finalNewSuzav = newSuzav;
                                            imgPlaySuzav.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    pbSuzav.setVisibility(View.VISIBLE);

                                                    if (!TextUtils.isEmpty(finalNewSuzav)) {
                                                        setAudioView(true, finalNewSuzav, imgPlaySuzav, pbSuzav);
                                                    } else {
                                                        setAudioView(true, finalSuzav, imgPlaySuzav, pbSuzav);
                                                    }
                                                }
                                            });

                                            if (managmentList != null && managmentList.size() > 0) {
                                                rvLaxan.setVisibility(View.GONE);
                                                rvGeneralAdvise.setVisibility(View.GONE);
                                                rvSuzav.setVisibility(View.VISIBLE);
                                            } else {
                                                rvLaxan.setVisibility(View.GONE);
                                                rvGeneralAdvise.setVisibility(View.GONE);
                                                rvSuzav.setVisibility(View.GONE);
                                            }

                                            image_path = jb.optString("image_path");
                                            List<String> advisory = new ArrayList<>();

                                            String generalAdvise = "";
                                            JSONArray jsonArray = jb.optJSONArray("advisory");
                                            if (jsonArray != null && jsonArray.length() > 0) {
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    generalAdvise += Functions.fromHtml(jsonArray.optString(i));
                                                    advisory.add(jsonArray.optString(i));
                                                }
                                            }

                                            TextAdvisesAdapter textAdvisesAdapter = new TextAdvisesAdapter(advisory, SavdhaniDetailsActivity.this);
                                            rvGeneralAdvise.setAdapter(textAdvisesAdapter);

                                            String finalGeneralAdvise = generalAdvise;
                                            imgPlaySalah.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    if (HomeActivity.tts != null) {
                                                        HomeActivity.tts.speak(finalGeneralAdvise, imgPlaySalah, pbSalah);
                                                    } else {
                                                        HomeActivity.setTextToSpeechInit();
                                                        HomeActivity.tts.speak(finalGeneralAdvise, imgPlaySalah, pbSalah);
                                                    }
                                                }
                                            });

                                            imagesList = new ArrayList<>();
                                            JSONObject images = jb.optJSONObject("images");
                                            if (images != null && images.length() > 0) {
                                                JSONObject stage_1 = images.optJSONObject("stage_1");
                                                if (stage_1 != null && stage_1.length() > 0) {
                                                    JSONArray image = stage_1.optJSONArray("image");
                                                    if (image != null && image.length() > 0) {
                                                        for (int i = 0; i < image.length(); i++) {
                                                            imagesList.add(image.optString(i));
                                                        }
                                                    }
                                                }
                                            }
                                            if (imagesList.size() == 0) {
                                                frameLayout.setVisibility(View.GONE);
                                            } else {
                                                frameLayout.setVisibility(View.VISIBLE);
                                                if (imagesList.size() == 1) {
                                                    imgNext.setVisibility(View.GONE);
                                                    imgPrevios.setVisibility(View.GONE);
                                                } else {
                                                    imgNext.setVisibility(View.VISIBLE);
                                                    imgPrevios.setVisibility(View.VISIBLE);
                                                }
                                            }

                                            ImagesSavdhaniAdapter imagesSavdhaniAdapter = new ImagesSavdhaniAdapter(SavdhaniDetailsActivity.this, imagesList, image_path, "1");
                                            pager.setAdapter(imagesSavdhaniAdapter);
                                            pager.setClipToPadding(false);
                                            pager.setOffscreenPageLimit(imagesList.size());
                                        }
                                    }
                                }

                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(SavdhaniDetailsActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    void setLocalData() {
        try {
            Log.v("Local keyword_id", "" + keyword_id);
            JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.loadAdvisoryKeywordJSONFromAsset(SavdhaniDetailsActivity.this, keyword_id));
            if (jsonObject != null && jsonObject.length() > 0) {

                String name = jsonObject.optString("name");
                txtTitle.setText(name);

                symptomsList = new ArrayList<>();
                JSONArray laxan = jsonObject.optJSONArray("symptoms");
                String laxanString = "", newLaxan = "";
                if (laxan != null && laxan.length() > 0) {
                    for (int i = 0; i < laxan.length(); i++) {
                        laxanString += Functions.fromHtml(laxan.optString(i));
                        symptomsList.add(laxan.optString(i));

                        try {
                            if (Functions.isJSONValid(laxan.optString(i))) {
                                JSONObject jObject = new JSONObject(laxan.optString(i));
                                if (!TextUtils.isEmpty(jObject.optString("advisory"))) {
                                    newLaxan += Functions.getConvertHtml(jObject.optString("advisory"));
                                    newLaxan += Functions.getConvertHtml(jObject.optString("note"));
                                    newLaxan += Functions.getConvertHtml(jObject.optString("chemical_usage"));
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                SymptomsAdapter symptomsAdapter = new SymptomsAdapter(symptomsList, SavdhaniDetailsActivity.this);
                rvLaxan.setAdapter(symptomsAdapter);

                String finalLaxanString = laxanString;
                String finalNewLaxan = newLaxan;
                imgPlay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pbLaxan.setVisibility(View.VISIBLE);
                        if (!TextUtils.isEmpty(finalNewLaxan)) {
                            setAudioView(true, finalNewLaxan, imgPlay, pbLaxan);
                        } else {
                            setAudioView(true, finalLaxanString, imgPlay, pbLaxan);
                        }
//                        if (HomeActivity.tts != null) {
//                            HomeActivity.tts.speak(finalLaxanString, imgPlay, pbLaxan);
//                        } else {
//                            HomeActivity.setTextToSpeechInit();
//                            HomeActivity.tts.speak(finalLaxanString, imgPlay, pbLaxan);
//                        }
                    }
                });

                chemical_info = jsonObject.optJSONArray("chemical_info");
                if (chemical_info != null && chemical_info.length() > 0) {
                    txtChecmicalName.setVisibility(View.VISIBLE);
                }

                List<String> managmentList = new ArrayList<>();
                String suzav = "", newSuzav = "";
                JSONArray management = jsonObject.optJSONArray("management");
                if (management != null && management.length() > 0) {
                    for (int i = 0; i < management.length(); i++) {
                        suzav += Functions.fromHtml(management.optString(i));
                        managmentList.add(management.optString(i));

                        if (Functions.isJSONValid(management.optString(i))) {
                            JSONObject jObject = new JSONObject(management.optString(i));
                            newSuzav += Functions.getConvertHtml(jObject.optString("advisory"));
                            newSuzav += Functions.getConvertHtml(jObject.optString("note"));
                            newSuzav += Functions.getConvertHtml(jObject.optString("chemical_usage"));
                        }
                    }
                }
                rvSuzav.setAdapter(new ManagementAdapter(SavdhaniDetailsActivity.this, managmentList));

                String finalSuzav = suzav;
                String finalNewSuzav = newSuzav;
                imgPlaySuzav.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!TextUtils.isEmpty(finalNewSuzav)) {
                            setAudioView(true, finalNewSuzav, imgPlaySuzav, pbSuzav);
                        } else {
                            setAudioView(true, finalSuzav, imgPlaySuzav, pbSuzav);
                        }
//                        if (HomeActivity.tts != null) {
//                            HomeActivity.tts.speak(finalSuzav, imgPlaySuzav, pbSuzav);
//                        } else {
//                            HomeActivity.setTextToSpeechInit();
//                            HomeActivity.tts.speak(finalSuzav, imgPlaySuzav, pbSuzav);
//                        }
                    }
                });

                if (managmentList != null && managmentList.size() > 0) {
                    rvLaxan.setVisibility(View.GONE);
                    rvGeneralAdvise.setVisibility(View.GONE);
                    rvSuzav.setVisibility(View.VISIBLE);
                } else {
                    rvLaxan.setVisibility(View.GONE);
                    rvGeneralAdvise.setVisibility(View.GONE);
                    rvSuzav.setVisibility(View.GONE);
                }

                image_path = jsonObject.optString("image_path");
                List<String> advisory = new ArrayList<>();

                String generalAdvise = "";
                JSONArray jsonArray = jsonObject.optJSONArray("advisory");
                if (jsonArray != null && jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        generalAdvise += Functions.fromHtml(jsonArray.optString(i));
                        advisory.add(jsonArray.optString(i));
                    }
                }

                TextAdvisesAdapter textAdvisesAdapter = new TextAdvisesAdapter(advisory, SavdhaniDetailsActivity.this);
                rvGeneralAdvise.setAdapter(textAdvisesAdapter);

                String finalGeneralAdvise = generalAdvise;
                imgPlaySalah.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try {
                            if (HomeActivity.tts != null) {
                                HomeActivity.tts.speak(finalGeneralAdvise, imgPlaySalah, pbSalah);
                            } else {
                                HomeActivity.setTextToSpeechInit();
                                HomeActivity.tts.speak(finalGeneralAdvise, imgPlaySalah, pbSalah);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });

                imagesList = new ArrayList<>();
                JSONObject images = jsonObject.optJSONObject("images");
                if (images != null && images.length() > 0) {
                    JSONObject stage_1 = images.optJSONObject("stage_1");
                    if (stage_1 != null && stage_1.length() > 0) {
                        JSONArray image = stage_1.optJSONArray("image");
                        if (image != null && image.length() > 0) {
                            for (int i = 0; i < image.length(); i++) {
                                imagesList.add(image.optString(i));
                            }
                        }
                    }
                }
                if (imagesList.size() == 0) {
                    frameLayout.setVisibility(View.GONE);
                } else {
                    frameLayout.setVisibility(View.VISIBLE);
                    if (imagesList.size() == 1) {
                        imgNext.setVisibility(View.GONE);
                        imgPrevios.setVisibility(View.GONE);
                    } else {
                        imgNext.setVisibility(View.VISIBLE);
                        imgPrevios.setVisibility(View.VISIBLE);
                    }
                }

                ImagesSavdhaniAdapter imagesSavdhaniAdapter = new ImagesSavdhaniAdapter(SavdhaniDetailsActivity.this, imagesList, image_path, "1");
                pager.setAdapter(imagesSavdhaniAdapter);
                pager.setClipToPadding(false);
                pager.setOffscreenPageLimit(imagesList.size());
            } else {
                getKeywordAdvisory();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            frameLayout.setVisibility(View.GONE);
        }
    }

    int getNextPossibleItemIndex(int change) {
        int currentIndex = pager.getCurrentItem();
        int total = 0;

        if (pager.getAdapter() != null) {
            total = pager.getAdapter().getCount();
        }
        if (currentPositionTop + change < 0) {
            return 0;
        }

        return Math.abs((currentIndex + change) % total);
    }


    void saveAdvisoryFeedback(final String isUseful) {
        RetrofitClient.getInstance().getApi().saveAdvisoryFeedback(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                Functions.getDeviceid(this), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                SharedPreferencesUtils.getUserId(this), sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0"), p_crop_id, keyword_id, isUseful).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String str = response.body().string();
                    if (!TextUtils.isEmpty(str)) {
                        JSONObject jsonObject = new JSONObject(str);
                        if (jsonObject != null && jsonObject.length() > 0) {
                            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {

                                cardView.setVisibility(View.GONE);
                                ADVISORY_FEEDBACK_KIDS_Array.add(keyword_id);
                                SharedPreferencesUtils.saveArrayList(SavdhaniDetailsActivity.this, ADVISORY_FEEDBACK_KIDS_Array, Constants.ADVISORY_FEEDBACK_KIDS);
                                Functions.updateRewardPoints(sharedPreferences, RewardPoints.getAdvisoryFeedbackRewardPoint(sharedPreferences));
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");

                                if (isUseful.equalsIgnoreCase("1")) {
                                    openThankYou();
                                } else {
                                    openProblem();
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(SavdhaniDetailsActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.POST_QUESTION_CODE && resultCode == Activity.RESULT_OK) {
            Functions.openPostQuestionSuccessDialog(SavdhaniDetailsActivity.this, appDatabase,
                    RewardPoints.getPostQuestionRewardPoint(sharedPreferences), RewardPoints.getInviteRewardPoint(sharedPreferences));
        }
    }

    void openThankYou() {
        try {
            final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
            View view = LayoutInflater.from(this).inflate(R.layout.bottom_thank_you_savdhani_dialog, null);
            bottomSheetDialog.setContentView(view);

            AppCompatTextView tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
            tvRewardPoint.setText(RewardPoints.getAdvisoryFeedbackRewardPoint(sharedPreferences));

            AppCompatTextView tvTitle = view.findViewById(R.id.tvTitle);
            tvTitle.setText(R.string.str_share_savdhani_text);

            AppCompatTextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
            tvRewardNew.setText(RewardPoints.getAdvisoryShareRewardPoint(sharedPreferences));

            ImageView imgClose = view.findViewById(R.id.imgClose);
            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bottomSheetDialog.dismiss();
                }
            });

            AppCompatTextView tvShareWhatapp = view.findViewById(R.id.tvShareWhatapp);
            tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Functions.setUserScreenTrackEvent(appDatabase, "Invite", "WhatsApp");
                        if (InternetConnection.checkConnectionForFasl(SavdhaniDetailsActivity.this)) {
                            Functions.setUserScreenTrackEvent(appDatabase, "Invite", "More");
                            if (TextUtils.isEmpty(image_path)) {
                                image_path = Constants.KEYWORD_IMAGE_PATH + RongoApp.getSelectedCrop();
                            }
                            String url = image_path + "/" + imagesList.get(0);
                            Functions.shareWhatAppUrlIntent(SavdhaniDetailsActivity.this, url);
                        } else {
                            Functions.shareDefaultInviteIntent(SavdhaniDetailsActivity.this);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Functions.shareDefaultInviteIntent(SavdhaniDetailsActivity.this);
                    }
                }
            });

            AppCompatTextView tvShareCopy = view.findViewById(R.id.tvShareCopy);
            tvShareCopy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Functions.setUserScreenTrackEvent(appDatabase, "Invite", "SMS");
                        Functions.shareSMSIntent(SavdhaniDetailsActivity.this, RongoApp.getReferralText(SavdhaniDetailsActivity.this));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            AppCompatTextView tvSharefacebook = view.findViewById(R.id.tvSharefacebook);
            tvSharefacebook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "Facebook");
                    Functions.shareFacebookIntent(SavdhaniDetailsActivity.this);
                }
            });

            AppCompatTextView tvShareMore = view.findViewById(R.id.tvShareMore);
            tvShareMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Functions.performShare(SavdhaniDetailsActivity.this, appDatabase);
                }
            });

            AppCompatTextView txtOfflineSMS = view.findViewById(R.id.txtOfflineSMS);
            if (InternetConnection.checkConnectionForFasl(SavdhaniDetailsActivity.this)) {
                txtOfflineSMS.setVisibility(View.GONE);
            } else {
                txtOfflineSMS.setVisibility(View.VISIBLE);
            }

            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
            ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
            bottomSheetDialog.show();
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    void openProblem() {
        try {
            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(SavdhaniDetailsActivity.this);
            View view = LayoutInflater.from(SavdhaniDetailsActivity.this).inflate(R.layout.bottom_dialog_problem, null);
            bottomSheetDialog.setContentView(view);

            ImageView imgClose = view.findViewById(R.id.imgClose);
            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bottomSheetDialog.dismiss();
                }
            });

            AppCompatTextView tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
            tvRewardPoint.setText(RewardPoints.getAdvisoryFeedbackRewardPoint(sharedPreferences));

            TextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
            tvRewardNew.setText(RewardPoints.getPostQuestionRewardPoint(sharedPreferences));

            RecyclerView rcvProblem = view.findViewById(R.id.rcvProblem);
            rcvProblem.addItemDecoration(new GridSpacingItemDecoration(3, (int) Functions.convertDpToPixel(5, SavdhaniDetailsActivity.this), false));
            rcvProblem.setNestedScrollingEnabled(false);
            rcvProblem.setLayoutManager(new GridLayoutManager(SavdhaniDetailsActivity.this, 3));
            List<ProblemRelatedData> options = SharedPreferencesUtils.getProblemRelated(SavdhaniDetailsActivity.this, Constants.SharedPreferences_OPTIONS);
            if (options != null && options.size() > 0) {
                rcvProblem.setAdapter(new CropProblemPostQuesAdapter(this, options, bottomSheetDialog));
            }

            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
            ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
            bottomSheetDialog.show();
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
//        nsv.post(new Runnable() {
//            @Override
//            public void run() {
//                nsv.fullScroll(NestedScrollView.FOCUS_UP);
//            }
//        });
    }

    @Override
    public void onBackPressed() {
        if (fromNotification.equalsIgnoreCase("Y")) {
            Intent i = new Intent(SavdhaniDetailsActivity.this, HomeActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finishAffinity();
            overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
        } else {
            finish();
        }
    }

    @Override
    protected void onStop() {
        HomeActivity.stopAudioService();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        HomeActivity.stopAudioService();
        super.onDestroy();
        System.gc();
    }

}
