package com.rongoapp.activities;

import static com.rongoapp.controller.Constants.CAMERA_REQUEST_CODE;
import static com.rongoapp.controller.Constants.GALLERY_REQUEST_CODE;
import static com.rongoapp.controller.Constants.providerAuthority;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.FileProvider;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.card.MaterialCardView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.rongoapp.R;
import com.rongoapp.adapter.CropAreaForRewardAdapter;
import com.rongoapp.adapter.EarnedRewardAdapter;
import com.rongoapp.adapter.PendingRewardAdapter;
import com.rongoapp.adapter.ThumbnailsAdapter;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.LocaleManager;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.AdvisoryPhotoData;
import com.rongoapp.data.CropAreaFocusedData;
import com.rongoapp.data.LuckyDrawData;
import com.rongoapp.data.OfflineRewardData;
import com.rongoapp.data.RewardData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.fragments.QuizFragment;
import com.rongoapp.fragments.QuizOLDFragment;
import com.rongoapp.imagecompressor.ImageZipper;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Executors;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RewardActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    RewardActivity homeActivity;
    long mLastLongClick = 0;
    ShimmerFrameLayout shimmerFrameLayout, shimmerLayoutEarn;

    BottomSheetDialog bottomSheetDialogCrop;
    BottomSheetDialog bottomSheetDialogGuideLine;
    CropAreaFocusedData mSelectedList;
    ArrayList<String> imagesPath = new ArrayList<>();
    String mCurrentPhotoPath = "", forPhotoLabel = "N";

    TextView tvRewardPoint, tvShareReward, tvExpertReward, tvPhotoReward, tvQuizReward, textEarnedReward;
    ImageView ivBack, imgNext, imgPrevios;
    RelativeLayout rlEarnedRewardView, rlPendingRewardView, rlDailyQuiz, rlPhotoUpload, rlExpertCall, rlShare;
    RecyclerView rcvEarnedReward, rcvPendingReward, rcvHowEarn;

    LinearLayoutManager layoutManager;
    ArrayList<RewardData> earnedRewardArray = new ArrayList<>();
    EarnedRewardAdapter earnedRewardAdapter;
    ArrayList<RewardData> pendingRewardArray = new ArrayList<>();
    PendingRewardAdapter pendingRewardAdapter;
    ArrayList<String> howEarnArray = new ArrayList<>();
    HowEarnAdapter howEarnAdapter;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward);

        Functions.setFirebaseLogEventTrack(RewardActivity.this, Constants.Screen_Track, "Reward", "", "");
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(this);
        homeActivity = this;

        setInitView();
        getRewardUpdate();
    }

    void setShimmerGone() {
        shimmerFrameLayout.stopShimmer();
        shimmerFrameLayout.setVisibility(View.GONE);
    }

    void setInitView() {
        try {
            ivBack = (ImageView) findViewById(R.id.ivBack);
            ivBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

            shimmerFrameLayout = findViewById(R.id.shimmerLayout);
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmer();

            shimmerLayoutEarn = findViewById(R.id.shimmerLayoutEarn);
            shimmerLayoutEarn.setVisibility(View.VISIBLE);

            rlDailyQuiz = (RelativeLayout) findViewById(R.id.rlDailyQuiz);
            rlPhotoUpload = (RelativeLayout) findViewById(R.id.rlPhotoUpload);
            rlExpertCall = (RelativeLayout) findViewById(R.id.rlExpertCall);
            rlShare = (RelativeLayout) findViewById(R.id.rlShare);

            rlDailyQuiz.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    if (Functions.getQuizIntent(homeActivity, sharedPreferences)) {
                        DialogFragment newFragment = QuizFragment.newInstance();
                        newFragment.show(ft, "dialog");
                    } else {
                        DialogFragment newFragment = QuizOLDFragment.newInstance();
                        newFragment.show(ft, "QuizOLDFragment");
                    }
                }
            });

            rlPhotoUpload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openCrops();
                }
            });

            rlExpertCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Functions.setFirebaseLogEventTrack(RewardActivity.this, Constants.Click_Event, "Reward", "", "ExpertAdvise");
                    Intent i = new Intent(RewardActivity.this, PostQuestionActivity.class);
                    i.putExtra("keyword_id", "0");
                    startActivityForResult(i, Constants.POST_QUESTION_CODE);
                }
            });

            rlShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Functions.performShare(homeActivity, appDatabase);
                }
            });

            tvShareReward = (TextView) findViewById(R.id.tvShareReward);
            tvShareReward.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));
            tvExpertReward = (TextView) findViewById(R.id.tvExpertReward);
            tvExpertReward.setText(RewardPoints.getPostQuestionRewardPoint(sharedPreferences));
            tvPhotoReward = (TextView) findViewById(R.id.tvPhotoReward);
            tvPhotoReward.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences));
            tvQuizReward = (TextView) findViewById(R.id.tvQuizReward);
            tvQuizReward.setText(RewardPoints.getDailyQuizRewardPoint(sharedPreferences));

            rcvPendingReward = (RecyclerView) findViewById(R.id.rcvPendingReward);
            imgNext = (ImageView) findViewById(R.id.imgNext);
            imgPrevios = (ImageView) findViewById(R.id.imgPrevios);

            imgNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (pendingRewardArray.size() > 0) {
                        int firstElementPosition = layoutManager.findLastCompletelyVisibleItemPosition();
                        rcvPendingReward.scrollToPosition(firstElementPosition + 1);
                    }
                }
            });

            imgPrevios.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (pendingRewardArray.size() > 0) {
                        int firstElementPosition = layoutManager.findFirstCompletelyVisibleItemPosition();
                        rcvPendingReward.scrollToPosition(firstElementPosition - 1);
                    }
                }
            });

            tvRewardPoint = (TextView) findViewById(R.id.tvRewardPoint);
            tvRewardPoint.setText(Functions.updateRewardPoints(sharedPreferences, "0"));

            textEarnedReward = (TextView) findViewById(R.id.textEarnedReward);
            textEarnedReward.setVisibility(View.GONE);

            rcvHowEarn = (RecyclerView) findViewById(R.id.rcvHowEarn);
            rcvHowEarn.setLayoutManager(new GridLayoutManager(this, 2));

            rlEarnedRewardView = (RelativeLayout) findViewById(R.id.rlEarnedRewardView);
            rlPendingRewardView = (RelativeLayout) findViewById(R.id.rlPendingRewardView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getRewardUpdate() {
        try {
            tvRewardPoint.setText(Functions.updateRewardPoints(sharedPreferences, "0"));
            if (InternetConnection.checkConnectionForFasl(this)) {
                if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                    checkAPICall();
                } else {
                    RongoApp.getContestData();
                    getPendingRewardsData();
                    getEarnedRewardsData();
                }
            } else {
                checkAPICall();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void checkAPICall() {
        try {
            if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_How_Earn_Reward, ""))) {
                setHowEarnView(new JSONArray(sharedPreferences.getString(Constants.SharedPreferences_How_Earn_Reward, "")));
            }

            if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_Pending_Reward, ""))) {
                setPendingRewardView(new JSONArray(sharedPreferences.getString(Constants.SharedPreferences_Pending_Reward, "")));
            } else {
                if (InternetConnection.checkConnectionForFasl(RewardActivity.this)) {
                    getPendingRewardsData();
                }
            }

            if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_Earned_Reward, ""))) {
                setEarnedRewardView(new JSONArray(sharedPreferences.getString(Constants.SharedPreferences_Earned_Reward, "")));
            } else {
                if (InternetConnection.checkConnectionForFasl(RewardActivity.this)) {
                    getEarnedRewardsData();
                }

                List<OfflineRewardData> offlineRewardData = appDatabase.offlineRewardDataDao().getAllReward();
                if (offlineRewardData.size() > 0 && offlineRewardData.get(0).getIs_true().equalsIgnoreCase("1")) {
                    RewardData rewardData = new RewardData();
                    rewardData.set_id(Integer.parseInt(offlineRewardData.get(0).getContestID()));
                    rewardData.setRewardId(offlineRewardData.get(0).getRewardID());
                    rewardData.setRewardTitle(offlineRewardData.get(0).getText());
                    rewardData.setStatus("0");
                    rewardData.setType(offlineRewardData.get(0).getType());
                    rewardData.setMetadata(offlineRewardData.get(0).getValue());
                    rewardData.setCropWeek(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0"));
                    earnedRewardArray.add(rewardData);
                }
                if (earnedRewardArray.size() > 0) {
                    textEarnedReward.setVisibility(View.GONE);
                    rcvEarnedReward = (RecyclerView) findViewById(R.id.rcvEarnedReward);
                    rcvEarnedReward.setLayoutManager(new LinearLayoutManager(this));
                    earnedRewardAdapter = new EarnedRewardAdapter(RewardActivity.this, earnedRewardArray);
                    rcvEarnedReward.setAdapter(earnedRewardAdapter);
                } else {
                    textEarnedReward.setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void getPendingRewardsData() {
        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Pending_Reward, "");
        RetrofitClient.getInstance().getApi().getPendingRewards(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), SharedPreferencesUtils.getUserId(RewardActivity.this),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0")).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String r = response.body().string();
                        if (!TextUtils.isEmpty(r)) {
                            JSONObject jsonObject = new JSONObject(r);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject data = jsonObject.optJSONObject("data");
                                    if (data != null && data.length() > 0) {
                                        JSONArray jsonArray = data.optJSONArray("result");
                                        if (jsonArray != null && jsonArray.length() > 0) {
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Pending_Reward, "" + jsonArray);
                                            setPendingRewardView(jsonArray);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(RewardActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    void setPendingRewardView(JSONArray jsonArray) {
        try {
            setShimmerGone();
            pendingRewardArray = new ArrayList<>();
            if (jsonArray != null && jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jb = jsonArray.optJSONObject(i);

                    if (jb.optString("type").equalsIgnoreCase("jackpot")) {
                        if (!Functions.getContestAvailable(sharedPreferences).equalsIgnoreCase("0")) {
                            RewardData rewardData = new RewardData();
                            rewardData.set_id(i);
                            rewardData.setRewardId(jb.optString("id"));
                            rewardData.setRewardTitle(jb.optString("title"));
                            rewardData.setDescription(jb.optString("description"));
                            rewardData.setMetadata(jb.optString("reward"));
                            rewardData.setType(jb.optString("type"));
                            rewardData.setStatus(jb.optString("required_points"));
                            pendingRewardArray.add(rewardData);
                        }
                    } else {
                        RewardData rewardData = new RewardData();
                        rewardData.set_id(i);
                        rewardData.setRewardId(jb.optString("id"));
                        rewardData.setRewardTitle(jb.optString("title"));
                        rewardData.setDescription(jb.optString("description"));
                        rewardData.setMetadata(jb.optString("reward"));
                        rewardData.setType(jb.optString("type"));
                        rewardData.setStatus(jb.optString("required_points"));
                        pendingRewardArray.add(rewardData);
                    }
                }
            }

            if (pendingRewardArray.size() > 0) {
                if (pendingRewardArray.size() == 1) {
                    imgNext.setVisibility(View.GONE);
                    imgPrevios.setVisibility(View.GONE);
                } else {
                    imgNext.setVisibility(View.VISIBLE);
                    imgPrevios.setVisibility(View.VISIBLE);
                }

                Collections.sort(pendingRewardArray, new Comparator<RewardData>() {
                    public int compare(RewardData o1, RewardData o2) {
                        return o1.getStatus().compareTo(o2.getStatus());
                    }
                });

                layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
                rcvPendingReward.setLayoutManager(layoutManager);
                pendingRewardAdapter = new PendingRewardAdapter(RewardActivity.this, pendingRewardArray, sharedPreferences.getString(Constants.SharedPreferences_CONTEST_ID, ""));
                rcvPendingReward.setAdapter(pendingRewardAdapter);
            } else {
                imgNext.setVisibility(View.GONE);
                imgPrevios.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void getEarnedRewardsData() {
        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Earned_Reward, "");
        RetrofitClient.getInstance().getApi().getEarnedRewards(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), SharedPreferencesUtils.getUserId(RewardActivity.this),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0")).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String r = response.body().string();
                        if (!TextUtils.isEmpty(r)) {
                            JSONObject jsonObject = new JSONObject(r);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject data = jsonObject.optJSONObject("data");
                                    if (data != null && data.length() > 0) {
                                        JSONObject result = data.optJSONObject("result");
                                        if (result != null && result.length() > 0) {

                                            JSONArray jsonArray = result.optJSONArray("rewards");
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Earned_Reward, "" + jsonArray);
                                            setEarnedRewardView(jsonArray);

                                            JSONArray how_to_earn_list = result.optJSONArray("how_to_earn_list");
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_How_Earn_Reward, "" + how_to_earn_list);
                                            setHowEarnView(how_to_earn_list);
                                        }
                                    }

                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    String message = "Socket Time out. Please try again.";
                    Toast.makeText(RewardActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    void setEarnedRewardView(JSONArray jsonArray) {
        try {
            earnedRewardArray = new ArrayList<>();
            if (jsonArray != null && jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jb = jsonArray.optJSONObject(i);
                    RewardData rewardData = new RewardData();
                    rewardData.set_id(i);
//                    rewardData.setRewardId(jb.optString("id"));
                    rewardData.setRewardId(jb.optString("reward_id"));
                    rewardData.setRewardTitle(jb.optString("reward_title"));
                    rewardData.setStatus(jb.optString("status"));
                    rewardData.setType(jb.optString("type"));
                    rewardData.setMetadata(jb.optString("metadata"));
                    rewardData.setCropWeek(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0"));
                    earnedRewardArray.add(rewardData);
                }
            }

            List<OfflineRewardData> offlineRewardData = appDatabase.offlineRewardDataDao().getAllReward();
            if (offlineRewardData.size() > 0 && offlineRewardData.get(0).getIs_true().equalsIgnoreCase("1")) {
                RewardData rewardData = new RewardData();
                rewardData.set_id(Integer.parseInt(offlineRewardData.get(0).getContestID()));
                rewardData.setRewardId(offlineRewardData.get(0).getRewardID());
                rewardData.setRewardTitle(offlineRewardData.get(0).getText());
                rewardData.setStatus("0");
                rewardData.setType(offlineRewardData.get(0).getType());
                rewardData.setMetadata(offlineRewardData.get(0).getValue());
                rewardData.setCropWeek(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0"));
                earnedRewardArray.add(rewardData);
            }

            if (earnedRewardArray.size() > 0) {
                textEarnedReward.setVisibility(View.GONE);
                rcvEarnedReward = (RecyclerView) findViewById(R.id.rcvEarnedReward);
                rcvEarnedReward.setLayoutManager(new LinearLayoutManager(this));
                earnedRewardAdapter = new EarnedRewardAdapter(RewardActivity.this, earnedRewardArray);
                rcvEarnedReward.setAdapter(earnedRewardAdapter);
            } else {
                textEarnedReward.setVisibility(View.VISIBLE);
            }

            shimmerLayoutEarn.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void setHowEarnView(JSONArray jsonArray) {
        try {
            howEarnArray = new ArrayList<>();
            if (jsonArray != null && jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    howEarnArray.add(jsonArray.getString(i));
                }
            }
            howEarnAdapter = new HowEarnAdapter(howEarnArray);
            rcvHowEarn.setAdapter(howEarnAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class HowEarnAdapter extends RecyclerView.Adapter<HowEarnAdapter.ViewHolder> {
        List<String> listdata;

        public HowEarnAdapter(List<String> listdata) {
            this.listdata = listdata;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.how_earned_list_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.tvTitle.setText(listdata.get(position));
        }

        @Override
        public int getItemCount() {
            return listdata.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvTitle;

            public ViewHolder(View itemView) {
                super(itemView);
                this.tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            }
        }
    }


    public void openCrops() {
        bottomSheetDialogCrop = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_lucky_draw_crops, null);
        bottomSheetDialogCrop.setContentView(view);
        bottomSheetDialogCrop.setCancelable(false);
        bottomSheetDialogCrop.setCanceledOnTouchOutside(false);

        LinearLayout llUploadView = (LinearLayout) view.findViewById(R.id.llUploadView);
        llUploadView.setVisibility(View.GONE);

        MaterialCardView cardHealthlly = (MaterialCardView) view.findViewById(R.id.cardHealthlly);
        cardHealthlly.setVisibility(View.VISIBLE);

        AppCompatTextView tvPoint = view.findViewById(R.id.tvPoint);
        tvPoint.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences));

        AppCompatTextView tvUpload = view.findViewById(R.id.tvUpload);
        tvUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardHealthlly.setVisibility(View.GONE);
                llUploadView.setVisibility(View.VISIBLE);
            }
        });

        AppCompatTextView tvPointsUploadPicture = view.findViewById(R.id.tvPointsUploadPicture);
        tvPointsUploadPicture.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences) + " " + getString(R.string.str_point_earned));

        RecyclerView rvCrops = view.findViewById(R.id.rvCrops);
        rvCrops.setLayoutManager(new LinearLayoutManager(homeActivity, RecyclerView.HORIZONTAL, false));
        rvCrops.setNestedScrollingEnabled(false);
        rvCrops.setHasFixedSize(true);
        List<CropAreaFocusedData> cropAreaFocusedData = SharedPreferencesUtils.getCropArea(homeActivity, Constants.SharedPreferences_CROP_FOCUSED_AREA);
        if (cropAreaFocusedData != null && cropAreaFocusedData.size() > 0) {
            rvCrops.setAdapter(new CropAreaForRewardAdapter(homeActivity, cropAreaFocusedData, mSelectedList));
        }

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogCrop.dismiss();
            }
        });

        ImageView imgNext = view.findViewById(R.id.nextImg);
        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rvCrops.post(new Runnable() {
                    @Override
                    public void run() {
                        if (cropAreaFocusedData != null && cropAreaFocusedData.size() > 0) {
                            rvCrops.scrollToPosition(cropAreaFocusedData.size() - 1);
                        }
                    }
                });
            }
        });

        AppCompatTextView txtUpload = view.findViewById(R.id.txtUpload);
        txtUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogCrop.dismiss();
                openGuidlinesDialog();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(homeActivity.getResources().getColor(android.R.color.transparent));
        bottomSheetDialogCrop.show();
    }

    public void getSelectedArea(CropAreaFocusedData mCropAreaFocusedData) {
        this.mSelectedList = mCropAreaFocusedData;
    }


    void openThumbnailDialog() {
        BottomSheetDialog bottomSheetThumbnail = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_upload_more_photos, null);
        bottomSheetThumbnail.setContentView(view);

        AppCompatTextView tvPointsUploadPicture = view.findViewById(R.id.tvPointsUploadPicture);
        tvPointsUploadPicture.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences) + getString(R.string.str_point_earned));
        ImageView imgNext = view.findViewById(R.id.imgNext);
        ProgressBar pb = view.findViewById(R.id.pb);
        pb.setVisibility(View.GONE);
        LinearLayout linearLuckYDraw = view.findViewById(R.id.linearLuckYDraw);
        linearLuckYDraw.setVisibility(View.VISIBLE);
        bottomSheetThumbnail.setCanceledOnTouchOutside(false);
        bottomSheetThumbnail.setCancelable(false);

        if (imagesPath.size() == 1) {
            imgNext.setVisibility(View.GONE);
        } else {
            imgNext.setVisibility(View.VISIBLE);
        }

        RecyclerView rvAllPhotos = view.findViewById(R.id.rvAllPhotos);
        rvAllPhotos.setLayoutManager(new LinearLayoutManager(homeActivity, RecyclerView.HORIZONTAL, false));
        rvAllPhotos.setAdapter(new ThumbnailsAdapter(imagesPath));
        rvAllPhotos.setNestedScrollingEnabled(false);

        AppCompatTextView txtUploadMore = view.findViewById(R.id.txtUploadMore);
        AppCompatTextView txtGoOn = view.findViewById(R.id.txtGoOn);
        txtGoOn.setText(getResources().getString(R.string.str_darj_kare));

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetThumbnail.dismiss();

                if (bottomSheetDialogCrop != null) {
                    bottomSheetDialogCrop.show();
                }

                if (imagesPath != null && imagesPath.size() > 0) {
                    imagesPath.clear();
                }
            }
        });

        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rvAllPhotos.post(new Runnable() {
                    @Override
                    public void run() {
                        if (imagesPath != null && imagesPath.size() > 0) {
                            rvAllPhotos.scrollToPosition(imagesPath.size() - 1);
                        }
                    }
                });
            }
        });

        txtUploadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetThumbnail.dismiss();
                accessPermission();
            }
        });

        txtGoOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                if (imagesPath.size() > 0) {
                    List<AdvisoryPhotoData> advisoryPhotoData = new ArrayList<AdvisoryPhotoData>();
                    advisoryPhotoData.add(new AdvisoryPhotoData(0, sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, "0"),
                            RongoApp.getSelectedCrop(), RongoApp.getSelectedPCrop(), "1"));
                    appDatabase.advisoryPhotoDao().insertAll(advisoryPhotoData);
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                            if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                setOfflineData(bottomSheetThumbnail);
                            } else {
                                uploadWeeklyPicture(bottomSheetThumbnail);
                            }
                        } else {
                            setOfflineData(bottomSheetThumbnail);
                        }
                    }
                }, 100);
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetThumbnail.show();
    }

    void setOfflineData(BottomSheetDialog bottomSheetThumbnail) {
        try {
            String focus_area = "";
            if (mSelectedList != null) {
                focus_area = mSelectedList.getType();
            }
            String finalFocus_area = focus_area;
            Executors.newSingleThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    List<LuckyDrawData> luckyDrawData = new ArrayList<>();
                    luckyDrawData.add(new LuckyDrawData(0, sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                            sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                            sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                            sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""),
                            finalFocus_area, "", "", imagesPath));
                    appDatabase.luckyDrawOfflineDao().insertAll(luckyDrawData);
                }
            });
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
//                    progressBar.setVisibility(View.GONE);
                    mSelectedList = null;
                    mCurrentPhotoPath = "";
                    imagesPath = new ArrayList<>();
                    bottomSheetThumbnail.dismiss();
                    openSuccessForHealthy();
                }
            }, 500);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void openGuidlinesDialog() {
        bottomSheetDialogGuideLine = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_guideline_for_photo_capture, null);
        bottomSheetDialogGuideLine.setContentView(view);
        bottomSheetDialogGuideLine.setCanceledOnTouchOutside(false);
        bottomSheetDialogGuideLine.setCancelable(false);

        AppCompatTextView txtUpload = view.findViewById(R.id.txtUpload);
        AppCompatTextView txtLuckyDraw = view.findViewById(R.id.txtLuckyDraw);
        AppCompatTextView txtDescription = view.findViewById(R.id.txtDescription);
        AppCompatImageView imgGuideline = view.findViewById(R.id.imgGuideline);
        txtLuckyDraw.setVisibility(View.GONE);

        if (mSelectedList != null) {
            Functions.displayImage(mSelectedList.getImage(), imgGuideline);
            txtDescription.setText(mSelectedList.getDescription());
        }

        final ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogGuideLine.dismiss();
                if (bottomSheetDialogCrop != null) {
                    bottomSheetDialogCrop.show();
                }
            }
        });

        txtUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogGuideLine.dismiss();
                accessPermission();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(homeActivity.getResources().getColor(android.R.color.transparent));
        bottomSheetDialogGuideLine.show();
    }

    void openPhotoLabelDialog(String path) {
        BottomSheetDialog bottomSheetDialogLabel = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_photo_label_upload, null);
        bottomSheetDialogLabel.setContentView(view);
        bottomSheetDialogLabel.setCancelable(false);
        bottomSheetDialogLabel.setCanceledOnTouchOutside(false);

        AppCompatTextView txtUpload = view.findViewById(R.id.txtUpload);
        AppCompatTextView txtDarjKare = view.findViewById(R.id.txtDarjKare);
        LinearLayout linearCapture = view.findViewById(R.id.linearCapture);
        ProgressBar pb = view.findViewById(R.id.pb);
        RelativeLayout relativeLabelUpload = view.findViewById(R.id.relativeLabelUpload);
        ImageView imgPhotoLabel = view.findViewById(R.id.imgPhotoLabel);
        ImageView imgDelete = view.findViewById(R.id.imgDelete);

        if (forPhotoLabel.equalsIgnoreCase("Y")) {
            linearCapture.setVisibility(View.GONE);
            relativeLabelUpload.setVisibility(View.VISIBLE);
            Functions.displayImage(path, imgPhotoLabel);
        } else {
            linearCapture.setVisibility(View.VISIBLE);
            relativeLabelUpload.setVisibility(View.GONE);
        }

        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forPhotoLabel = "N";

                imgPhotoLabel.setImageResource(0);
                imgPhotoLabel.setImageBitmap(null);

                linearCapture.setVisibility(View.VISIBLE);
                relativeLabelUpload.setVisibility(View.GONE);
            }
        });

        final ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogLabel.dismiss();
                forPhotoLabel = "N";
            }
        });

        txtUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forPhotoLabel = "Y";

                bottomSheetDialogLabel.dismiss();
                accessPermission();
            }
        });

        txtDarjKare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forPhotoLabel = "N";
                bottomSheetDialogLabel.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(homeActivity.getResources().getColor(android.R.color.transparent));
        bottomSheetDialogLabel.show();
    }

    RequestBody getRequestFile(File file) {
        RequestBody fbody = RequestBody.create(file, MediaType.parse("image/*"));
        return fbody;
    }

    void uploadWeeklyPicture(BottomSheetDialog bottomSheetThumbnail) {
        String focus_area = "";
        if (mSelectedList != null) {
            focus_area = mSelectedList.getType();
        }
        RequestBody requestBodyId = RequestBody.create(SharedPreferencesUtils.getUserId(homeActivity).trim(), MediaType.parse("text/plain"));
        RequestBody seasonIdBody = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyCropId = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyPCropId = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyCropWeek = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyDeviceId = RequestBody.create(Functions.getDeviceid(homeActivity), MediaType.parse("text/plain"));
        RequestBody requestBodyFCMToken = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyPictureArea = RequestBody.create(focus_area, MediaType.parse("text/plain"));
        RequestBody requestBodylatitude = RequestBody.create("", MediaType.parse("text/plain"));
        RequestBody requestBodylongitude = RequestBody.create("", MediaType.parse("text/plain"));
        RequestBody requestBodyAppVersion = RequestBody.create(RongoApp.getAppVersion(), MediaType.parse("text/plain"));
        RequestBody requestBodyAppLanguage = RequestBody.create(RongoApp.getDefaultLanguage(), MediaType.parse("text/plain"));
        RequestBody requestBodyAppSeason = RequestBody.create(RongoApp.getSeasonName(), MediaType.parse("text/plain"));

        Map<String, RequestBody> params = new HashMap<>();
        params.put("user_id", requestBodyId);
        params.put("season_id", seasonIdBody);
        params.put("crop_id", requestBodyCropId);
        params.put("p_crop_id", requestBodyPCropId);
        params.put("crop_week", requestBodyCropWeek);
        params.put("picture_area_focused", requestBodyPictureArea);
        params.put("geo_latitude", requestBodylatitude);
        params.put("geo_longitude", requestBodylongitude);
        params.put("device_id", requestBodyDeviceId);
        params.put("fcm_token", requestBodyFCMToken);
        params.put("app_version", requestBodyAppVersion);
        params.put("app_language", requestBodyAppLanguage);
        params.put("season_name", requestBodyAppSeason);

        if (imagesPath != null && imagesPath.size() > 0) {
            RequestBody requestBodyImageUploadedPath = RequestBody.create(imagesPath.toString(), MediaType.parse("text/plain"));
            params.put("picture_key_local", requestBodyImageUploadedPath);

            for (int i = 0; i < imagesPath.size(); i++) {
                File file = new File(imagesPath.get(i).toString().trim());
                try {
                    File compressedImages = new ImageZipper(homeActivity).compressToFile(file);
                    params.put("picture[]" + "\"; filename=\"" + file.getName(), getRequestFile(compressedImages));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        RetrofitClient.getInstance().getApi().uploadWeeklyPicture(params).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String str = response.body().string();
                        if (!TextUtils.isEmpty(str)) {
                            JSONObject jsonObject = new JSONObject(str);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");

                                    mSelectedList = null;
                                    mCurrentPhotoPath = "";
                                    imagesPath = new ArrayList<>();
                                    bottomSheetThumbnail.dismiss();
                                    openSuccessForHealthy();
                                }
                            }
                        }
//                        progressBar.setVisibility(View.GONE);
                    } catch (Exception e) {
                        e.printStackTrace();
//                        progressBar.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                progressBar.setVisibility(View.GONE);
            }
        });
    }

    void accessPermission() {
        Dexter.withContext(homeActivity)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    void showImagePickerOptions() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(homeActivity);
        View sheetView = homeActivity.getLayoutInflater().inflate(R.layout.bottom_photo_uplaod_options, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);

        LinearLayout linearCamera = sheetView.findViewById(R.id.linearCamera);
        LinearLayout linearGallery = sheetView.findViewById(R.id.linearGallery);
        sheetView.findViewById(R.id.imgClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                if (bottomSheetDialogGuideLine != null) {
                    bottomSheetDialogGuideLine.show();
                }
            }
        });

        linearCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                dispatchTakePictureIntent();
            }
        });

        linearGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, GALLERY_REQUEST_CODE);
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        ((View) sheetView.getParent()).setBackgroundColor(homeActivity.getResources().getColor(android.R.color.transparent));
        mBottomSheetDialog.show();
    }

    void dispatchTakePictureIntent() {
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File photoFile = createImageFile();
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(RewardActivity.this, providerAuthority, photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                List<ResolveInfo> resolvedIntentActivities = this.getPackageManager().queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolvedIntentInfo : resolvedIntentActivities) {
                    String packageName = resolvedIntentInfo.activityInfo.packageName;
                    this.grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.POST_QUESTION_CODE && resultCode == Activity.RESULT_OK) {
            Functions.openPostQuestionSuccessDialog(homeActivity, appDatabase,
                    RewardPoints.getPostQuestionRewardPoint(sharedPreferences), RewardPoints.getInviteRewardPoint(sharedPreferences));
        }
        if (requestCode == CAMERA_REQUEST_CODE || requestCode == GALLERY_REQUEST_CODE) {
            try {
                if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
                    if (data != null && data.hasExtra("mCurrentPhotoPath")) {
                        mCurrentPhotoPath = data.getStringExtra("mCurrentPhotoPath");
                    }
                    File f = new File(mCurrentPhotoPath);
                    Uri contentUri = Uri.fromFile(f);
                    mCurrentPhotoPath = contentUri.getPath();
                }

                if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {
                    Uri selectedImage = data.getData();
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    mCurrentPhotoPath = c.getString(columnIndex);
                    c.close();
                    if (mCurrentPhotoPath == null) {
                        Bitmap googlepath = Functions.getBitmapFromUri(selectedImage, this);
                        mCurrentPhotoPath = Functions.getGooglePhotoImagePath(googlepath, this);
                    }
                    if (mCurrentPhotoPath.contains("/root")) {
                        mCurrentPhotoPath = mCurrentPhotoPath.replace("/root", "");
                    }
                }

                if (forPhotoLabel.equalsIgnoreCase("Y")) {
                    openPhotoLabelDialog(mCurrentPhotoPath);
                } else {
                    imagesPath.add(mCurrentPhotoPath);
                    openThumbnailDialog();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    void openSuccessForHealthy() {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_success_for_healthy, null);
        bottomSheetDialog.setContentView(view);

        AppCompatTextView txtOfflineSMS = view.findViewById(R.id.txtOfflineSMS);
        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
            txtOfflineSMS.setVisibility(View.GONE);
        } else {
            txtOfflineSMS.setVisibility(View.VISIBLE);
        }

        AppCompatTextView tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences));

        AppCompatTextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));

        AppCompatTextView tvShareWhatapp = view.findViewById(R.id.tvShareWhatapp);
        tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "WhatsApp");
                    Functions.shareWhatAppIntent(homeActivity);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(homeActivity, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        AppCompatTextView tvShareCopy = view.findViewById(R.id.tvShareCopy);
        tvShareCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "SMS");
                    Functions.shareSMSIntent(homeActivity, RongoApp.getReferralText(homeActivity));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AppCompatTextView tvSharefacebook = view.findViewById(R.id.tvSharefacebook);
        tvSharefacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.setUserScreenTrackEvent(appDatabase, "Invite", "Facebook");
                Functions.shareFacebookIntent(homeActivity);
            }
        });

        AppCompatTextView tvShareMore = view.findViewById(R.id.tvShareMore);
        tvShareMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.performShare(homeActivity, appDatabase);
            }
        });

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

}