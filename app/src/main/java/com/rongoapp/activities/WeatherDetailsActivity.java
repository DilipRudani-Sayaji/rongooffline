package com.rongoapp.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.card.MaterialCardView;
import com.rongoapp.R;
import com.rongoapp.adapter.CropProblemPostQuesAdapter;
import com.rongoapp.adapter.ManagementAdapter;
import com.rongoapp.adapter.TextAdvisesAdapter;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.InitInterface;
import com.rongoapp.controller.LocaleManager;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.JankariFeedBackData;
import com.rongoapp.data.ProblemRelatedData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.GridSpacingItemDecoration;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherDetailsActivity extends AppCompatActivity implements InitInterface {

    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    long mLastLongClick = 0;

    NestedScrollView nsv;
    FrameLayout frameLayout;
    MaterialCardView cardView;
    RelativeLayout relAdvise;
    ProgressBar pbSuzav, pbLaxan, pbSalah;
    RecyclerView rvSuzav, rvLaxan, rvGeneralAdvise;
    AppCompatTextView txtTitle, txtYes, txtNo, tvPointsAdvisoryFeedback;
    LinearLayout lineaeADVISE, linearLAXAN, linearSUZAV, relLaxan, relSuzav, llNoDataAvailable;
    ImageView imgBack, imgPlay, imgPlaySuzav, imgPlaySalah, imgAdvisoryImg;

    String fromNotification = "N", weather_alert = "", image_path = "", advisory_image = "", p_crop_id = "";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_details);

        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(this);
        p_crop_id = Functions.getP_Crop_Id(sharedPreferences);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            if (b.containsKey("fromNotification")) {
                fromNotification = b.getString("fromNotification");
            }
            if (b.containsKey("weather_alert")) {
                weather_alert = b.getString("weather_alert");
            }
        }
        Functions.setFirebaseLogEventTrack(WeatherDetailsActivity.this, Constants.Screen_Track, "Weather Detail", weather_alert, "");
        Functions.setUserScreenTrackEvent(appDatabase, "Weather Detail", "" + weather_alert);

        try {
            init();
            setListener();
            bindData();
            HomeActivity.setTextToSpeechInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() {
        txtYes = findViewById(R.id.txtYes);
        cardView = findViewById(R.id.cardView);
        cardView.setVisibility(View.GONE);

        llNoDataAvailable = findViewById(R.id.llNoDataAvailable);

        lineaeADVISE = findViewById(R.id.linearADVISE);
        linearLAXAN = findViewById(R.id.linearLAXAN);
        linearSUZAV = findViewById(R.id.linearSUZAV);

        pbLaxan = findViewById(R.id.pbLaxan);
        pbSuzav = findViewById(R.id.pbSuzav);
        pbSalah = findViewById(R.id.pbSalah);

        relAdvise = findViewById(R.id.relAdvise);
        nsv = findViewById(R.id.nsv);
        txtNo = findViewById(R.id.txtNo);
        imgPlay = findViewById(R.id.imgPlay);
        imgPlaySuzav = findViewById(R.id.imgPlaySuzav);
        imgPlaySalah = findViewById(R.id.imgPlaySalah);
        frameLayout = findViewById(R.id.frameLayout);
        imgBack = findViewById(R.id.imgBack);
        imgAdvisoryImg = findViewById(R.id.imgAdvisoryImg);
        relLaxan = findViewById(R.id.relLaxan);
        relSuzav = findViewById(R.id.relSuzav);
        rvSuzav = findViewById(R.id.rvSuzav);
        rvGeneralAdvise = findViewById(R.id.rvGeneralAdvise);
        rvLaxan = findViewById(R.id.rvLaxan);
        txtTitle = findViewById(R.id.txtTitle);
        rvLaxan.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rvSuzav.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rvGeneralAdvise.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        tvPointsAdvisoryFeedback = findViewById(R.id.tvPointsAdvisoryFeedback);
        tvPointsAdvisoryFeedback.setText(RewardPoints.getAdvisoryFeedbackRewardPoint(sharedPreferences));
    }

    @Override
    public void setListener() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fromNotification.equalsIgnoreCase("Y")) {
                    Intent i = new Intent(WeatherDetailsActivity.this, HomeActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finishAffinity();
                    overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
                } else {
                    finish();
                }
            }
        });

        txtNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Weather_Show_DATE, Functions.getCurrentDate());
                Functions.updateRewardPoints(sharedPreferences, RewardPoints.getAdvisoryFeedbackRewardPoint(sharedPreferences));
                if (InternetConnection.checkConnection(WeatherDetailsActivity.this)) {
                    saveAdvisoryFeedback("0");
                } else {
                    saveYesNoData("0");
                    openProblem();
                }
            }
        });

        txtYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Weather_Show_DATE, Functions.getCurrentDate());
                Functions.updateRewardPoints(sharedPreferences, RewardPoints.getAdvisoryFeedbackRewardPoint(sharedPreferences));
                if (InternetConnection.checkConnection(WeatherDetailsActivity.this)) {
                    saveAdvisoryFeedback("1");
                } else {
                    saveYesNoData("1");
                    openThankYou();
                }
            }
        });

        relLaxan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rvLaxan.getVisibility() == View.VISIBLE) {
                    rvLaxan.setVisibility(View.GONE);
                } else {
                    rvLaxan.setVisibility(View.VISIBLE);
                    rvGeneralAdvise.setVisibility(View.GONE);
                    rvSuzav.setVisibility(View.GONE);
                }
            }
        });

        relSuzav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rvSuzav.getVisibility() == View.VISIBLE) {
                    rvSuzav.setVisibility(View.GONE);
                } else {
                    rvLaxan.setVisibility(View.GONE);
                    rvGeneralAdvise.setVisibility(View.GONE);
                    rvSuzav.setVisibility(View.VISIBLE);
                }
            }
        });

        relAdvise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (rvGeneralAdvise.getVisibility() == View.VISIBLE) {
                    rvGeneralAdvise.setVisibility(View.GONE);
                } else {
                    rvLaxan.setVisibility(View.GONE);
                    rvGeneralAdvise.setVisibility(View.VISIBLE);
                    rvSuzav.setVisibility(View.GONE);
                }
            }
        });
    }

    void saveYesNoData(String yesNo) {
        try {
            List<JankariFeedBackData> jankariFeedBackData = new ArrayList<>();
            jankariFeedBackData.add(new JankariFeedBackData(0, Constants.saveAdvisoryFeedback, yesNo, p_crop_id, "0", "0", ""));
            appDatabase.jankariFeedbackDao().insertAll(jankariFeedBackData);
            cardView.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void bindData() {
        try {
            List<String> weatherList = SharedPreferencesUtils.getArrayList(this, Constants.SharedPreferences_WEATHER_ALERT);
            weatherList.add(weather_alert);
            if (!weatherList.isEmpty()) {
                JSONObject weather_advisory = new JSONObject(sharedPreferences.getString(Constants.SharedPreferences_WEATHER_ADVISORY, ""));
                if (weather_advisory != null && weather_advisory.length() > 0) {
                    image_path = weather_advisory.optString("image_path");

                    JSONArray jsonArray = weather_advisory.optJSONArray("general_advice");
                    List<String> generalAdvisory = new ArrayList<>();
                    String g_str = "";
                    if (jsonArray != null && jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            g_str += Functions.fromHtml(jsonArray.optString(i));
                            generalAdvisory.add(jsonArray.optString(i));
                        }
                    }

                    rvGeneralAdvise.setAdapter(new TextAdvisesAdapter(generalAdvisory, WeatherDetailsActivity.this));

                    if (sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0").equalsIgnoreCase("0")) {
                        if (generalAdvisory != null && generalAdvisory.size() > 0) {
                            lineaeADVISE.setVisibility(View.VISIBLE);
                            rvGeneralAdvise.setVisibility(View.VISIBLE);
                        } else {
                            lineaeADVISE.setVisibility(View.GONE);
                        }
                    } else {
                        if (generalAdvisory != null && generalAdvisory.size() > 0) {
                            lineaeADVISE.setVisibility(View.VISIBLE);
                        } else {
                            lineaeADVISE.setVisibility(View.GONE);
                        }
                    }

                    String finalG_str = g_str;
                    imgPlaySalah.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (HomeActivity.tts != null) {
                                HomeActivity.tts.speak(finalG_str, imgPlaySalah, pbSalah);
                            } else {
                                HomeActivity.setTextToSpeechInit();
                                HomeActivity.tts.speak(finalG_str, imgPlaySalah, pbSalah);
                            }
                        }
                    });

                    Iterator<String> keys = weather_advisory.keys();
                    while (keys.hasNext()) {
                        String key = keys.next();
                        JSONObject jb = weather_advisory.optJSONObject(key);
                        for (int i = 0; i < weatherList.size(); i++) {
                            if (weatherList.get(i).contains(key)) {
                                String name = jb.optString("display_name");
                                txtTitle.setText(name);

                                List<String> symptomsList = new ArrayList<>();
                                JSONArray laxan = jb.optJSONArray("symptoms");
                                String content = "";
                                if (laxan != null && laxan.length() > 0) {
                                    for (int M = 0; M < laxan.length(); M++) {
                                        content += Functions.fromHtml(laxan.getString(M));
                                        symptomsList.add(laxan.getString(M));
                                    }
                                }

                                String finalContent = content;
                                imgPlay.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if (HomeActivity.tts != null) {
                                            HomeActivity.tts.speak(finalContent, imgPlay, pbLaxan);
                                        } else {
                                            HomeActivity.setTextToSpeechInit();
                                            HomeActivity.tts.speak(finalContent, imgPlay, pbLaxan);
                                        }
                                    }
                                });

                                rvLaxan.setAdapter(new TextAdvisesAdapter(symptomsList, WeatherDetailsActivity.this));
                                if (symptomsList != null && symptomsList.size() > 0) {
                                    linearLAXAN.setVisibility(View.VISIBLE);
                                } else {
                                    linearLAXAN.setVisibility(View.GONE);
                                }

                                List<String> managmentList = new ArrayList<>();
                                JSONArray management = jb.optJSONArray("management");
                                String contentSuzav = "";
                                if (management != null && management.length() > 0) {
                                    for (int K = 0; K < management.length(); K++) {
                                        contentSuzav += Functions.fromHtml(management.getString(K));
                                        managmentList.add(management.getString(K));
                                    }
                                }

                                String finalContentSuzav = contentSuzav;
                                imgPlaySuzav.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        if (HomeActivity.tts != null) {
                                            HomeActivity.tts.speak(finalContentSuzav, imgPlaySuzav, pbSuzav);
                                        } else {
                                            HomeActivity.setTextToSpeechInit();
                                            HomeActivity.tts.speak(finalContentSuzav, imgPlaySuzav, pbSuzav);
                                        }
                                    }
                                });

                                rvSuzav.setAdapter(new ManagementAdapter(WeatherDetailsActivity.this, managmentList));
                                if (managmentList != null && managmentList.size() > 0) {
                                    linearSUZAV.setVisibility(View.VISIBLE);
                                    rvSuzav.setVisibility(View.VISIBLE);
                                    llNoDataAvailable.setVisibility(View.GONE);
                                    setFeedbackCard();
                                } else {
                                    linearSUZAV.setVisibility(View.GONE);
                                    rvSuzav.setVisibility(View.GONE);
                                    llNoDataAvailable.setVisibility(View.VISIBLE);
                                }

                                advisory_image = jb.optString("advisory_image");
                                if (!TextUtils.isEmpty(advisory_image)) {
                                    frameLayout.setVisibility(View.VISIBLE);
                                    if (!InternetConnection.checkConnectionForFasl(WeatherDetailsActivity.this)) {
                                        if (RongoApp.getSelectedCrop().equalsIgnoreCase("2") || RongoApp.getSelectedCrop().equalsIgnoreCase("3")) {
                                            imgAdvisoryImg.setImageDrawable(Functions.getAssetsWeatherImage(WeatherDetailsActivity.this,
                                                    Constants.ASSETS_ADVISORY_WEATHER_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + advisory_image));
                                        } else {
                                            imgAdvisoryImg.setImageDrawable(Functions.getAssetsWeatherImage(WeatherDetailsActivity.this,
                                                    Constants.ASSETS_ADVISORY_WEATHER_IMAGE_PATH + "1" + "/" + advisory_image));
                                        }
                                    } else {
                                        Functions.displayImage(image_path + "/" + advisory_image, imgAdvisoryImg);
                                    }
                                } else {
                                    frameLayout.setVisibility(View.GONE);
                                }

                                break;
                            }
                        }
                    }
                }
            } else {
                getWeeklyWeather();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setFeedbackCard() {
        if (Functions.showWeatherView(sharedPreferences.getString(Constants.SharedPreferences_Weather_Show_DATE, ""))) {
            cardView.setVisibility(View.VISIBLE);
        }
    }

    void getWeeklyWeather() {
        RetrofitClient.getInstance().getApi().getWeeklyWeatherData(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                RongoApp.getAppVersion(), SharedPreferencesUtils.getUserId(this), RongoApp.getSelectedCrop(),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0")).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        List<String> weather_alert_list;
                        String r = response.body().string();
                        if (!TextUtils.isEmpty(r)) {
                            JSONObject jsonObject = new JSONObject(r);
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_WeeklyWeatherData, "" + jsonObject.toString());
                            JSONObject data = jsonObject.optJSONObject("data");
                            if (data != null && data.length() > 0) {
//                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, "weather", "" + data.toString());
                                JSONObject result = data.optJSONObject("result");
                                if (result != null && result.length() > 0) {
                                    weather_alert_list = new ArrayList<>();
                                    JSONArray weather_alert = result.optJSONArray("weather_alert");
                                    if (weather_alert != null && weather_alert.length() > 0) {
                                        for (int i = 0; i < weather_alert.length(); i++) {
                                            weather_alert_list.add(weather_alert.optString(i));
                                            SharedPreferencesUtils.saveArrayList(WeatherDetailsActivity.this, weather_alert_list, Constants.SharedPreferences_WEATHER_ALERT);
                                        }
                                    }

                                    JSONObject weather_advisory = result.optJSONObject("weather_advisory");
                                    if (weather_advisory != null && weather_advisory.length() > 0) {
                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_WEATHER_ADVISORY, weather_advisory.toString());
                                        if (weather_advisory != null && weather_advisory.length() > 0) {
                                            image_path = weather_advisory.optString("image_path");

                                            JSONArray jsonArray = weather_advisory.optJSONArray("general_advice");
                                            List<String> generalAdvisory = new ArrayList<>();
                                            String g_str = "";
                                            if (jsonArray != null && jsonArray.length() > 0) {
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    g_str += Functions.fromHtml(jsonArray.optString(i));
                                                    generalAdvisory.add(jsonArray.optString(i));
                                                }
                                            }

                                            rvGeneralAdvise.setAdapter(new TextAdvisesAdapter(generalAdvisory, WeatherDetailsActivity.this));

                                            if (sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0").equalsIgnoreCase("0")) {
                                                if (generalAdvisory != null && generalAdvisory.size() > 0) {
                                                    lineaeADVISE.setVisibility(View.VISIBLE);
                                                    rvGeneralAdvise.setVisibility(View.VISIBLE);
                                                } else {
                                                    lineaeADVISE.setVisibility(View.GONE);
                                                }
                                            } else {
                                                if (generalAdvisory != null && generalAdvisory.size() > 0) {
                                                    lineaeADVISE.setVisibility(View.VISIBLE);
                                                } else {
                                                    lineaeADVISE.setVisibility(View.GONE);
                                                }
                                            }

                                            String finalG_str = g_str;
                                            imgPlaySalah.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    if (HomeActivity.tts != null) {
                                                        HomeActivity.tts.speak(finalG_str, imgPlaySalah, pbSalah);
                                                    } else {
                                                        HomeActivity.setTextToSpeechInit();
                                                        HomeActivity.tts.speak(finalG_str, imgPlaySalah, pbSalah);
                                                    }
                                                }
                                            });

                                            Iterator<String> keys = weather_advisory.keys();
                                            while (keys.hasNext()) {
                                                String key = keys.next();
                                                JSONObject jb = weather_advisory.optJSONObject(key);
                                                for (int i = 0; i < weather_alert_list.size(); i++) {
                                                    if (weather_alert_list.get(i).contains(key)) {
                                                        String name = jb.optString("display_name");
                                                        txtTitle.setText(name);

                                                        List<String> symptomsList = new ArrayList<>();
                                                        JSONArray laxan = jb.optJSONArray("symptoms");
                                                        String content = "";
                                                        if (laxan != null && laxan.length() > 0) {
                                                            for (int M = 0; M < laxan.length(); M++) {
                                                                content += Functions.fromHtml(laxan.getString(M));
                                                                symptomsList.add(laxan.getString(M));
                                                            }
                                                        }

                                                        String finalContent = content;
                                                        imgPlay.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View view) {
                                                                if (HomeActivity.tts != null) {
                                                                    HomeActivity.tts.speak(finalContent, imgPlay, pbLaxan);
                                                                } else {
                                                                    HomeActivity.setTextToSpeechInit();
                                                                    HomeActivity.tts.speak(finalContent, imgPlay, pbLaxan);
                                                                }
                                                            }
                                                        });

                                                        rvLaxan.setAdapter(new TextAdvisesAdapter(symptomsList, WeatherDetailsActivity.this));

                                                        if (symptomsList != null && symptomsList.size() > 0) {
                                                            linearLAXAN.setVisibility(View.VISIBLE);
                                                        } else {
                                                            linearLAXAN.setVisibility(View.GONE);
                                                        }

                                                        List<String> managmentList = new ArrayList<>();
                                                        JSONArray management = jb.optJSONArray("management");
                                                        String contentSuzav = "";
                                                        if (management != null && management.length() > 0) {
                                                            for (int K = 0; K < management.length(); K++) {
                                                                contentSuzav += Functions.fromHtml(management.getString(K));
                                                                managmentList.add(management.getString(K));
                                                            }
                                                        }

                                                        String finalContentSuzav = contentSuzav;
                                                        imgPlaySuzav.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View view) {
                                                                if (HomeActivity.tts != null) {
                                                                    HomeActivity.tts.speak(finalContentSuzav, imgPlaySuzav, pbSuzav);
                                                                } else {
                                                                    HomeActivity.setTextToSpeechInit();
                                                                    HomeActivity.tts.speak(finalContentSuzav, imgPlaySuzav, pbSuzav);
                                                                }
                                                            }
                                                        });

                                                        rvSuzav.setAdapter(new ManagementAdapter(WeatherDetailsActivity.this, managmentList));
                                                        if (managmentList != null && managmentList.size() > 0) {
                                                            linearSUZAV.setVisibility(View.VISIBLE);
                                                            rvSuzav.setVisibility(View.VISIBLE);
                                                            llNoDataAvailable.setVisibility(View.GONE);
                                                            setFeedbackCard();
                                                        } else {
                                                            linearSUZAV.setVisibility(View.GONE);
                                                            rvSuzav.setVisibility(View.GONE);
                                                            llNoDataAvailable.setVisibility(View.VISIBLE);
                                                        }

                                                        advisory_image = jb.optString("advisory_image");
                                                        if (!TextUtils.isEmpty(advisory_image)) {
                                                            frameLayout.setVisibility(View.VISIBLE);
                                                            if (!InternetConnection.checkConnectionForFasl(WeatherDetailsActivity.this)) {
                                                                if (RongoApp.getSelectedCrop().equalsIgnoreCase("2") || RongoApp.getSelectedCrop().equalsIgnoreCase("3")) {
                                                                    imgAdvisoryImg.setImageDrawable(Functions.getAssetsWeatherImage(WeatherDetailsActivity.this,
                                                                            Constants.ASSETS_ADVISORY_WEATHER_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + advisory_image));
                                                                } else {
                                                                    imgAdvisoryImg.setImageDrawable(Functions.getAssetsWeatherImage(WeatherDetailsActivity.this,
                                                                            Constants.ASSETS_ADVISORY_WEATHER_IMAGE_PATH + "1" + "/" + advisory_image));
                                                                }
                                                            } else {
                                                                Functions.displayImage(image_path + "/" + advisory_image, imgAdvisoryImg);
                                                            }
                                                        } else {
                                                            frameLayout.setVisibility(View.GONE);
                                                        }

                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void saveAdvisoryFeedback(final String isUseful) {
        Functions.setFirebaseLogEventTrack(WeatherDetailsActivity.this, Constants.Click_Event, "Weather Detail", weather_alert, isUseful);

        RetrofitClient.getInstance().getApi().saveAdvisoryFeedback(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                RongoApp.getAppVersion(), Functions.getDeviceid(this), RongoApp.getFCMToken(),
                SharedPreferencesUtils.getUserId(this),
                sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                RongoApp.getCurrentCropWeek(), p_crop_id, "0", isUseful).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String str = response.body().string();
                    if (!TextUtils.isEmpty(str)) {
                        JSONObject jsonObject = new JSONObject(str);
                        if (jsonObject != null && jsonObject.length() > 0) {
                            cardView.setVisibility(View.GONE);
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");
                            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                if (isUseful.equalsIgnoreCase("1")) {
                                    openThankYou();
                                } else {
                                    openProblem();
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(WeatherDetailsActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.POST_QUESTION_CODE && resultCode == Activity.RESULT_OK) {
            Functions.openPostQuestionSuccessDialog(WeatherDetailsActivity.this, appDatabase,
                    RewardPoints.getPostQuestionRewardPoint(sharedPreferences), RewardPoints.getInviteRewardPoint(sharedPreferences));
        }
    }


    void openThankYou() {
        try {
            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
            View view = LayoutInflater.from(this).inflate(R.layout.bottom_thank_you_savdhani_dialog, null);
            bottomSheetDialog.setContentView(view);

            AppCompatTextView tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
            tvRewardPoint.setText(RewardPoints.getAdvisoryFeedbackRewardPoint(sharedPreferences));

            AppCompatTextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
            tvRewardNew.setText(RewardPoints.getAdvisoryShareRewardPoint(sharedPreferences));

            ImageView imgClose = view.findViewById(R.id.imgClose);
            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bottomSheetDialog.dismiss();
                }
            });

            AppCompatTextView tvShareWhatapp = view.findViewById(R.id.tvShareWhatapp);
            tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Functions.setUserScreenTrackEvent(appDatabase, "Invite", "WhatsApp");
                        if (InternetConnection.checkConnectionForFasl(WeatherDetailsActivity.this)) {
                            Functions.setUserScreenTrackEvent(appDatabase, "Invite", "More");
                            String url = image_path + advisory_image;
                            Functions.shareWhatAppUrlIntent(WeatherDetailsActivity.this, url);
                        } else {
                            Functions.shareDefaultInviteIntent(WeatherDetailsActivity.this);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Functions.shareDefaultInviteIntent(WeatherDetailsActivity.this);
                        Toast.makeText(WeatherDetailsActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            AppCompatTextView tvShareCopy = view.findViewById(R.id.tvShareCopy);
            tvShareCopy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Functions.setUserScreenTrackEvent(appDatabase, "Invite", "SMS");
                        Functions.shareSMSIntent(WeatherDetailsActivity.this, RongoApp.getReferralText(WeatherDetailsActivity.this));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            AppCompatTextView tvSharefacebook = view.findViewById(R.id.tvSharefacebook);
            tvSharefacebook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "Facebook");
                    Functions.shareFacebookIntent(WeatherDetailsActivity.this);
                }
            });

            AppCompatTextView tvShareMore = view.findViewById(R.id.tvShareMore);
            tvShareMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Functions.performShare(WeatherDetailsActivity.this, appDatabase);
                }
            });

            AppCompatTextView txtOfflineSMS = view.findViewById(R.id.txtOfflineSMS);
            if (InternetConnection.checkConnectionForFasl(WeatherDetailsActivity.this)) {
                txtOfflineSMS.setVisibility(View.GONE);
            } else {
                txtOfflineSMS.setVisibility(View.VISIBLE);
            }

            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
            ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
            bottomSheetDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void openProblem() {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(WeatherDetailsActivity.this);
        View view = LayoutInflater.from(WeatherDetailsActivity.this).inflate(R.layout.bottom_dialog_problem, null);
        bottomSheetDialog.setContentView(view);

        ImageView ivBanner = view.findViewById(R.id.ivBanner);
        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        AppCompatTextView tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText(RewardPoints.getAdvisoryFeedbackRewardPoint(sharedPreferences));

        AppCompatTextView txtAskExpert = view.findViewById(R.id.txtAskExpert);
        txtAskExpert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();

                Intent i = new Intent(WeatherDetailsActivity.this, PostQuestionActivity.class);
                i.putExtra("keyword_id", "0");
                startActivityForResult(i, Constants.POST_QUESTION_CODE);
            }
        });

        TextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getPostQuestionRewardPoint(sharedPreferences));

        RecyclerView rcvProblem = view.findViewById(R.id.rcvProblem);
        rcvProblem.addItemDecoration(new GridSpacingItemDecoration(3, (int) Functions.convertDpToPixel(5, WeatherDetailsActivity.this), false));
        rcvProblem.setNestedScrollingEnabled(false);
        rcvProblem.setLayoutManager(new GridLayoutManager(WeatherDetailsActivity.this, 3));
        List<ProblemRelatedData> options = SharedPreferencesUtils.getProblemRelated(WeatherDetailsActivity.this, Constants.SharedPreferences_OPTIONS);
        if (options != null && options.size() > 0) {
            rcvProblem.setAdapter(new CropProblemPostQuesAdapter(this, options, bottomSheetDialog));
        }

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }


    @Override
    public void onBackPressed() {
        if (fromNotification.equalsIgnoreCase("Y")) {
            Intent i = new Intent(WeatherDetailsActivity.this, HomeActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finishAffinity();
            overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
        } else {
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        nsv.post(new Runnable() {
            @Override
            public void run() {
                nsv.fullScroll(NestedScrollView.FOCUS_UP);
            }
        });
    }

    @Override
    protected void onStop() {
        HomeActivity.stopAudioService();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        HomeActivity.stopAudioService();
        super.onDestroy();
    }

}
