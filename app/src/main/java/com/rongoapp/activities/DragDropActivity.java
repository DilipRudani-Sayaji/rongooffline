package com.rongoapp.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.rongoapp.R;
import com.rongoapp.dragLine.DrawLine;
import com.rongoapp.draw.Point;
import com.rongoapp.views.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DragDropActivity extends AppCompatActivity {

    TextView txtRedShape, txtGreenShape, txtDropZone;
    List<Point> points = new ArrayList<Point>();
    Point pointStart = new Point();
    Point pointEnd = new Point();
    RelativeLayout rlDrawView;

    Button playBtn, pauseBtn;
    MediaPlayer mediaPlayer;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);

        rlDrawView = (RelativeLayout) findViewById(R.id.rlDrawView);

        // initialize views to drag
        txtRedShape = (TextView) findViewById(R.id.viewRed);
        txtRedShape.setTag("Red");

        txtGreenShape = (TextView) findViewById(R.id.viewGreen);
        txtGreenShape.setTag("Green");

        // initialize dropzone view
        txtDropZone = (TextView) findViewById(R.id.viewDropZone);

        // initialize listeners
        txtRedShape.setOnTouchListener(new TouchListener());
        txtGreenShape.setOnTouchListener(new TouchListener());

        // handle the drop event on drop zone
        txtDropZone.setOnDragListener(new OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                // TODO Auto-generated method stub
                int action = event.getAction();
                switch (action) {
                    case DragEvent.ACTION_DRAG_STARTED: {
                        return true;
                    }
                    case DragEvent.ACTION_DRAG_LOCATION: {
                        return true;
                    }
                    case DragEvent.ACTION_DRAG_ENTERED: {
                        return true;
                    }
                    case DragEvent.ACTION_DRAG_EXITED: {
                        return true;
                    }
                    case DragEvent.ACTION_DROP: {
                        View source = (View) event.getLocalState();
                        TextView tv = (TextView) source;
                        String tag = (String) tv.getTag();
                        Toast.makeText(getApplicationContext(), tag, Toast.LENGTH_SHORT).show();
                        //v.invalidate();

                        pointEnd = new Point();
                        pointEnd.x = event.getX();
                        pointEnd.y = event.getY();
                        return true;
                    }
                    case DragEvent.ACTION_DRAG_ENDED: {

                        if (event.getResult()) {
                            pointEnd = new Point();
                            pointEnd.x = event.getX();
                            pointEnd.y = event.getY();
                            Log.e("points1", "" + pointEnd.x + "=" + pointEnd.y);

                            DrawLine drawLine = new DrawLine(DragDropActivity.this,
                                    pointStart.x, pointStart.y, pointEnd.x, pointEnd.y);
                            rlDrawView.addView(drawLine);
                        }
                        return true;
                    }

                    default: {
                        return false;
                    }
                }
            }
        });


        // initializing our buttons
        playBtn = findViewById(R.id.idBtnPlay);
        pauseBtn = findViewById(R.id.idBtnPause);

        // setting on click listener for our play and pause buttons.
        playBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calling method to play audio.
                playAudio();
            }
        });

        pauseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // checking the media player
                // if the audio is playing or not.
                if (mediaPlayer.isPlaying()) {
                    // pausing the media player if media player
                    // is playing we are calling below line to
                    // stop our media player.
                    mediaPlayer.stop();
                    mediaPlayer.reset();
                    mediaPlayer.release();

                    // below line is to display a message
                    // when media player is paused.
                    Toast.makeText(DragDropActivity.this, "Audio has been paused", Toast.LENGTH_SHORT).show();
                } else {
                    // this method is called when media
                    // player is not playing.
                    Toast.makeText(DragDropActivity.this, "Audio has not played", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void playAudio() {

        String audioUrl = "https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3";

        // initializing media player
        mediaPlayer = new MediaPlayer();

        // below line is use to set the audio
        // stream type for our media player.
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        // below line is use to set our
        // url to our media player.
        try {
            mediaPlayer.setDataSource(audioUrl);
            // below line is use to prepare
            // and start our media player.
            mediaPlayer.prepare();
            mediaPlayer.start();

        } catch (IOException e) {
            e.printStackTrace();
        }
        // below line is use to display a toast message.
        Toast.makeText(this, "Audio started playing..", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Exit App")
                .setMessage("Do you want to close the application ?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    @SuppressLint("ClickableViewAccessibility")
    private final class TouchListener implements OnTouchListener {

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            pointStart = new Point();
            pointStart.x = event.getX();
            pointStart.y = event.getY();
            Log.e("points++", "" + pointStart.x + "=" + pointStart.y);

//            Point point = new Point();
//            point.x = event.getX();
//            point.y = event.getY();
//            points.add(point);
//
//            DrawView drawView = new DrawView(DragDropActivity.this);
//            rlDrawView.addView(drawView);
//            drawView.requestFocus();
//            drawView.invalidate();

            // TODO Auto-generated method stub
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //setup drag
                ClipData data = ClipData.newPlainText("", "");
                DragShadowBuilder shadowBuilder = new DragShadowBuilder(v);

                //start dragging the item touched
                v.startDrag(data, shadowBuilder, v, 0);

                return true;
            } else {
                return false;
            }
        }

    }

}
