package com.rongoapp.activities;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import com.rongoapp.R;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.InitInterface;
import com.rongoapp.controller.LocaleManager;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.ProgressBarDialog;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements InitInterface {

    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    AppCompatTextView txtRegisterLink, txtLogin;
    AppCompatEditText editTextPhonenumber;
    ImageView imgLogo;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Functions.setFirebaseLogEventTrack(LoginActivity.this, Constants.Screen_Track, "Login", "", "");
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(this);
        Functions.setUserScreenTrackEvent(appDatabase, "Login", "0");

        requestPhoneNumber();
        init();
        setListener();
    }

    @Override
    public void init() {
        txtRegisterLink = findViewById(R.id.txtRegisterLink);
        txtLogin = findViewById(R.id.txtLogin);
        editTextPhonenumber = findViewById(R.id.editTextPhonenumber);

        imgLogo = findViewById(R.id.imgLogo);
        Functions.setRongoLogo(imgLogo);
    }

    @Override
    public void setListener() {
        txtRegisterLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Functions.setFirebaseLogEventTrack(LoginActivity.this, Constants.Click_Event, "Login",
                        editTextPhonenumber.getText().toString().trim(), "Register");
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });

        txtLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fieldsValidation()) {
                    if (InternetConnection.checkConnection(LoginActivity.this)) {
                        login();
                    }
                }
            }
        });
    }

    void login() {
        Functions.setFirebaseLogEventTrack(LoginActivity.this, Constants.Click_Event, "Login",
                editTextPhonenumber.getText().toString().trim(), "");

        ProgressBarDialog progressBarDialog = new ProgressBarDialog(LoginActivity.this);
        progressBarDialog.showProgressDialogWithTitle(getResources().getString(R.string.app_name));

        RetrofitClient.getInstance().getApi().login(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(this),
                sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), editTextPhonenumber.getText().toString().trim(),
                sharedPreferences.getString(Constants.SharedPreference_NetworkType, Constants.NETWORK_TYPE)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBarDialog.hideProgressDialogWithTitle();
                if (response != null) {
                    try {
                        String responseStrig = response.body().string();
                        if (!TextUtils.isEmpty(responseStrig)) {
                            JSONObject jb = new JSONObject(responseStrig);
                            if (jb != null && jb.length() > 0) {
                                if (jb.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject jData = jb.optJSONObject("data");
                                    if (jData != null && jData.length() > 0) {
                                        JSONObject jResult = jData.optJSONObject("result");
                                        if (jResult != null && jResult.length() > 0) {
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_UserId, jResult.optString("user_id"));
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_MOBILE_NO, editTextPhonenumber.getText().toString().trim());

                                            Intent i = new Intent(LoginActivity.this, OTPActivity.class);
                                            i.putExtra("otp", jResult.optString("otp"));
                                            i.putExtra("mobileNumber", editTextPhonenumber.getText().toString().trim());
                                            startActivity(i);
                                        }
                                    }
                                } else if (jb.optString("status_code").equalsIgnoreCase("1")) {
                                    String message = jb.optString("status_message");
                                    Functions.showSnackBar(findViewById(android.R.id.content), TextUtils.htmlEncode(message));
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        });
    }

    boolean fieldsValidation() {
        if (TextUtils.isEmpty(editTextPhonenumber.getText().toString().trim())) {
            Functions.showSnackBar(findViewById(android.R.id.content), getString(R.string.str_please_enter_you_number));
            return false;
        }

        if (!TextUtils.isEmpty(editTextPhonenumber.getText().toString().trim()) && editTextPhonenumber.getText().toString().trim().length() < 10) {
            Functions.showSnackBar(findViewById(android.R.id.content), getString(R.string.str_please_enter_valid_number));
            return false;
        }

        if (!TextUtils.isEmpty(editTextPhonenumber.getText().toString().trim()) && !validCellPhone(editTextPhonenumber.getText().toString().trim())) {
            Functions.showSnackBar(findViewById(android.R.id.content), getString(R.string.str_please_enter_valid_number));
            return false;
        }

        return true;
    }

    public boolean validCellPhone(String number) {
        return android.util.Patterns.PHONE.matcher(number).matches();
    }

    public void requestPhoneNumber() {
        try {
            GoogleApiClient googleApiClient = new GoogleApiClient.Builder(LoginActivity.this).addApi(Auth.CREDENTIALS_API).build();
            HintRequest hintRequest = new HintRequest.Builder().setPhoneNumberIdentifierSupported(true).build();
            PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(googleApiClient, hintRequest);
            startIntentSenderForResult(intent.getIntentSender(), 1231, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            Log.e("TAG", "Could not start hint picker Intent", e);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1231 && resultCode == RESULT_OK) {
            Credential cred = data.getParcelableExtra(Credential.EXTRA_KEY);
            Log.i("phoneNumberCallback", "" + cred.getId());
            String phoneNumber = cred.getId();
            if (!TextUtils.isEmpty(phoneNumber) && phoneNumber.startsWith("+")) {
                if (phoneNumber.length() == 13) {
                    String str_getMOBILE = phoneNumber.substring(3);
                    editTextPhonenumber.setText(str_getMOBILE);
                } else if (phoneNumber.length() == 14) {
                    String str_getMOBILE = phoneNumber.substring(4);
                    editTextPhonenumber.setText(str_getMOBILE);
                }
            } else {
                editTextPhonenumber.setText(phoneNumber);
            }
        }
    }


}
