package com.rongoapp.activities;

import static com.rongoapp.controller.RongoApp.mAppOpenData;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.rongoapp.R;
import com.rongoapp.adapter.SpinnerHintArrayAdapter;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.FetchJsonFromAssetsFile;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.GeocoderHandler;
import com.rongoapp.controller.InitInterface;
import com.rongoapp.controller.LocaleManager;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.StateCityDataModel;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.utilities.ServiceUtility;
import com.rongoapp.views.CustomSnackbar;
import com.rongoapp.views.ProgressBarDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity implements InitInterface {

    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    GoogleApiClient googleApiClient;
    AppCompatEditText editTextFirstName, editTextPhonenumber;
    AppCompatTextView txtRegister, txtLoginHyperLink, txtAddress, tvTerms;
    RelativeLayout rlCitySpinner, rlStateSpinner;
    LinearLayout llInfo, llOffline, llTermsCondition;

    ArrayList<StateCityDataModel> stateCityDataModels = new ArrayList<StateCityDataModel>();
    SpinnerHintArrayAdapter spinnerArrayAdapter;

    double latitude = 0.0, longitude = 0.0;
    String city = "", state = "", country = "", area = "", newCity = "", newState = "", referred_by = "", referral_source = "";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Functions.setFirebaseLogEventTrack(RegisterActivity.this, Constants.Screen_Track, "Register", "", "");
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(this);
        Functions.setUserScreenTrackEvent(appDatabase, "Register", "0");

        requestPhoneNumber();
        accessPermission();
        init();
        setListener();
        setDynamicLink();
    }

    void accessPermission() {
        Dexter.withContext(RegisterActivity.this).withPermissions(Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO,
                Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    void setDynamicLink() {
        try {
            String deepLink = sharedPreferences.getString(Constants.SharedPreference_DynamicLink, "");
            if (!TextUtils.isEmpty(deepLink)) {
//                https://rongoapp.in/app/i/MTUw
                String[] separated = deepLink.toString().split("app/");
                if (!TextUtils.isEmpty(separated[1])) {
                    String[] sep = separated[1].toString().split("/");
                    referral_source = sep[0];
                    try {
                        referred_by = Functions.decodeToString(sep[1]);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() {
        editTextFirstName = findViewById(R.id.editTextFirstName);
        editTextPhonenumber = findViewById(R.id.editTextPhonenumber);
        txtAddress = findViewById(R.id.txtAddress);
        txtRegister = findViewById(R.id.txtRegister);
        txtLoginHyperLink = findViewById(R.id.txtLoginHyperLink);

        rlCitySpinner = findViewById(R.id.rlCitySpinner);
        rlCitySpinner.setVisibility(View.GONE);
        rlStateSpinner = findViewById(R.id.rlStateSpinner);

        ImageView imgLogo = findViewById(R.id.imgLogo);
        Functions.setRongoLogo(imgLogo);

        llTermsCondition = findViewById(R.id.llTermsCondition);
        tvTerms = findViewById(R.id.tvTerms);

        llInfo = findViewById(R.id.llInfo);
        llOffline = findViewById(R.id.llOffline);
//        if (InternetConnection.checkConnectionForFasl(RegisterActivity.this)) {
//            llInfo.setVisibility(View.VISIBLE);
//            llOffline.setVisibility(View.GONE);
//        } else {
//            llInfo.setVisibility(View.GONE);
//            llOffline.setVisibility(View.VISIBLE);
//        }

        try {
            int start = getResources().getString(R.string.str_uses_terms_1).length();
            SpannableString spannableStr = new SpannableString(getResources().getString(R.string.str_uses_terms));
            spannableStr.setSpan(new UnderlineSpan(), start, start + 16, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            spannableStr.setSpan(new StyleSpan(Typeface.BOLD), start, start + 16, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//          spannableStr.setSpan(new ForegroundColorSpan(Color.rgb(55, 55, 55)),start, start + 16, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            tvTerms.setText(spannableStr);

            llTermsCondition.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://rongoapp.in/in/legal"));
                    startActivity(browserIntent);
                }
            });

            setUpCityStateData(new JSONObject(FetchJsonFromAssetsFile.getCityStateListJSONFromAsset(RegisterActivity.this)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setListener() {
        txtLoginHyperLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Functions.setFirebaseLogEventTrack(RegisterActivity.this, Constants.Click_Event, "Register", "", "Login");
                Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        txtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fieldsValidation()) {
                    if (InternetConnection.checkConnectionForFasl(RegisterActivity.this)) {
                        logSubmitFBEvent("online");
                        register();
                    } else {
                        logSubmitFBEvent("Offline");
                        RongoApp.getMasterRewardPoints(false);
                        RongoApp.getMasterCropData(false);

                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_First_Name, editTextFirstName.getText().toString().trim());
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_MOBILE_NO, editTextPhonenumber.getText().toString().trim());
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_State, state);
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_City, city);
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_New_State, newState);
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_New_City, newCity);
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Country, country);
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Lat, "" + latitude);
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Long, "" + longitude);
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_VERIFY_OTP, "N");
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_RongoHelpLineNumber, "079-48061880");
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_User_State, newState);

                        Functions.updateRewardPoints(sharedPreferences, RewardPoints.getDailyLoginPoint(sharedPreferences));
                        Intent i = new Intent(RegisterActivity.this, HomeActivity.class);
                        i.putExtra("fromLaunch", "N");
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                        finishAffinity();
                    }
                }
            }
        });

        txtAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSearchCalled();
            }
        });
    }

    public void requestPhoneNumber() {
        try {
            googleApiClient = new GoogleApiClient.Builder(RegisterActivity.this).addApi(Auth.CREDENTIALS_API).build();
            HintRequest hintRequest = new HintRequest.Builder().setPhoneNumberIdentifierSupported(true).build();
            PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(googleApiClient, hintRequest);
            startIntentSenderForResult(intent.getIntentSender(), 1231, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            Log.e("TAG", "Could not start hint picker Intent", e);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1231 && resultCode == RESULT_OK) {
            Credential cred = Objects.requireNonNull(data).getParcelableExtra(Credential.EXTRA_KEY);
            Log.i("phoneNumberCallback", "" + cred.getId());
            String phoneNumber = cred.getId();
            if (!TextUtils.isEmpty(phoneNumber) && phoneNumber.startsWith("+")) {
                if (phoneNumber.length() == 13) {
                    String str_getMOBILE = phoneNumber.substring(3);
                    editTextPhonenumber.setText(str_getMOBILE);
                } else if (phoneNumber.length() == 14) {
                    String str_getMOBILE = phoneNumber.substring(4);
                    editTextPhonenumber.setText(str_getMOBILE);
                }
            } else {
                editTextPhonenumber.setText(phoneNumber);
            }
        }

        if (requestCode == 202 && resultCode == RESULT_OK) {
            Place place = Autocomplete.getPlaceFromIntent(data);
            Log.i("TAG", "Place: " + place.getName() + ", " + place.getId() + ", " + place.getAddress());
            txtAddress.setText(place.getAddress());
            area = place.getAddress();
            latitude = Objects.requireNonNull(place.getLatLng()).latitude;
            longitude = place.getLatLng().longitude;
            Log.i("TAG", "Lat-Long: " + latitude + ", " + longitude);
            getAddressFromLocation(latitude, longitude, RegisterActivity.this, new GeocoderHandler());
        } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
            Status status = Autocomplete.getStatusFromIntent(data);
            Toast.makeText(RegisterActivity.this, "Error: " + status.getStatusMessage(), Toast.LENGTH_LONG).show();
        }
    }

    void onSearchCalled() {
        if (!Places.isInitialized()) {
            Places.initialize(RegisterActivity.this, getResources().getString(R.string.place_key));
        }
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).setCountry("IN").build(this);
        startActivityForResult(intent, 202);
    }

    void getAddressFromLocation(final double latitude, final double longitude, final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.US);
                String result = null;
                try {
                    List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);
                    if (addressList != null && addressList.size() > 0) {
                        Address address = addressList.get(0);
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                            sb.append(address.getAddressLine(i));
                        }
                        String street = address.getThoroughfare();
                        if (!TextUtils.isEmpty(street)) {
                            sb.append(street).append(", ");
                        }
                        if (!TextUtils.isEmpty(address.getLocality())) {
                            sb.append(address.getLocality()).append(", ");
                            city = address.getLocality();
                        }
                        if (!TextUtils.isEmpty(address.getAdminArea())) {
                            sb.append(address.getAdminArea()).append(", ");
                            state = address.getAdminArea();
                        }
                        if (!TextUtils.isEmpty(address.getCountryName())) {
                            country = address.getCountryName();
                            if (!TextUtils.isEmpty(address.getPostalCode())) {
                                sb.append(address.getCountryName()).append(", ");
                            } else {
                                sb.append(address.getCountryName()).append("");
                            }
                        }
                        if (!TextUtils.isEmpty(address.getPostalCode())) {
                            sb.append(address.getPostalCode()).append("");
                            String pincode = address.getPostalCode();
                        }
                        result = sb.toString();
//                        area = result;

                        if (!TextUtils.isEmpty(state) && stateCityDataModels.size() > 0) {
                            for (int i = 0; i < stateCityDataModels.size(); i++) {
                                if (state.trim().equalsIgnoreCase(stateCityDataModels.get(i).getState_english().trim())) {
                                    newState = stateCityDataModels.get(i).getState_hindi();
                                    break;
                                } else {
                                    newState = "Other";
                                }
                            }
                            Log.e("+++ state1 ++", "" + newState + "-" + city);
                        }
                    }
                } catch (IOException e) {
                    Log.e("Location Address Loader", "Unable connect to Geocoder", e);
                } finally {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    if (result != null) {
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        bundle.putString("address", result);
                        message.setData(bundle);
                    } else {
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        result = " Unable to get address for this location.";
                        bundle.putString("address", result);
                        message.setData(bundle);
                    }
                    message.sendToTarget();
                }
            }
        };
        thread.start();
    }


    void setUpCityStateData(JSONObject jsonObject) {
        try {
            if (jsonObject != null && jsonObject.length() > 0) {
                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                    JSONObject data = jsonObject.optJSONObject("data");
                    if (data != null && data.length() > 0) {
                        stateCityDataModels = new ArrayList<StateCityDataModel>();
                        JSONArray result1 = data.optJSONArray("result");
                        if (result1 != null && result1.length() > 0) {
                            //                            Log.e("+++1++", "" + result.optJSONObject(0));
//                            Log.e("+++2++", "" + result.optJSONObject(0).optString("hi"));
//                            Log.e("+++3++", "" + result.optJSONObject(0).optString("gu"));

                            JSONArray result = new JSONArray(result1.optJSONObject(0).optString(RongoApp.getDefaultLanguage()));
                            for (int i = 0; i < result.length(); i++) {
                                JSONObject object = result.optJSONObject(i);

                                JSONObject state = object.optJSONObject("state");
                                if (state != null && state.length() > 0) {
                                    parseJsonState(i, state);
                                }

                                JSONObject district = object.optJSONObject("district");
                                if (district != null && district.length() > 0) {
                                    parseJsonCity(i, district);
                                }
                            }
                        }
                    }
                    setStateListData();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void parseJsonState(int id, JSONObject data) {
        StateCityDataModel stateCityDataModel = new StateCityDataModel();
        if (data != null) {
            Iterator<String> it = data.keys();
            while (it.hasNext()) {
                String key = it.next();
                try {
                    if (data.get(key) instanceof JSONArray) {
                        JSONArray arry = data.getJSONArray(key);
                        int size = arry.length();
                        for (int i = 0; i < size; i++) {
                            parseJsonState(i, arry.getJSONObject(i));
                        }
                    } else if (data.get(key) instanceof JSONObject) {
                        parseJsonState(0, data.getJSONObject(key));
                    } else {
//                        System.out.println("" + key + "-" + id + " : " + data.optString(key));
                        stateCityDataModel.setId(String.valueOf(id));
                        stateCityDataModel.setState_english(key);
                        stateCityDataModel.setState_hindi(data.optString(key));
                        stateCityDataModels.add(stateCityDataModel);
                    }
                } catch (Throwable e) {
//                    System.out.println("" + key + " : " + data.optString(key));
                    e.printStackTrace();
                }
            }
        }
    }

    void parseJsonCity(int id, JSONObject data) {
        ArrayList<StateCityDataModel.CityListModel> cityListModels = new ArrayList<>();
        if (data != null) {
            Iterator<String> it = data.keys();
            while (it.hasNext()) {
                String key = it.next();
                try {
                    if (data.get(key) instanceof JSONArray) {
                        JSONArray arry = data.getJSONArray(key);
                        int size = arry.length();
                        for (int i = 0; i < size; i++) {
                            parseJsonState(i, arry.getJSONObject(i));
                        }
                    } else if (data.get(key) instanceof JSONObject) {
                        parseJsonState(0, data.getJSONObject(key));
                    } else {
//                        System.out.println("" + key + "-" + id + " : " + data.optString(key));
                        StateCityDataModel.CityListModel cityListModel = new StateCityDataModel.CityListModel();
                        cityListModel.setId(String.valueOf(id));
                        cityListModel.setCity_english(key);
                        cityListModel.setCity_hindi(data.optString(key));
                        cityListModels.add(cityListModel);
                    }
                } catch (Throwable e) {
//                    System.out.println("" + key + " : " + data.optString(key));
                    e.printStackTrace();
                }
            }
            stateCityDataModels.get(id).setCityList(cityListModels);
        }
    }


    public void setStateListData() {
        ArrayList<String> stateList = new ArrayList<>();
        for (int i = 0; i < stateCityDataModels.size(); i++) {
            stateList.add(stateCityDataModels.get(i).getState_hindi());
        }

        Spinner spin = (Spinner) findViewById(R.id.spinnerState);
        spinnerArrayAdapter = new SpinnerHintArrayAdapter(this, R.layout.spinner_item_list, stateList);
        spinnerArrayAdapter.add(getString(R.string.str_select_state));
        spin.setAdapter(spinnerArrayAdapter);
        spin.setSelection(spinnerArrayAdapter.getCount());
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Functions.hideKeyboard(RegisterActivity.this);
                String item = parent.getItemAtPosition(position).toString();
                if (!item.equalsIgnoreCase(getString(R.string.str_select_state))) {
                    if (item.equalsIgnoreCase(getString(R.string.str_other))) {
                        txtAddress.setVisibility(View.VISIBLE);
                        rlCitySpinner.setVisibility(View.GONE);
                        newCity = "Other";
                        newState = "Other";
                    } else {
                        newCity = "";
                        rlCitySpinner.setVisibility(View.VISIBLE);
                        txtAddress.setVisibility(View.GONE);
                        setCityListData(item);

                        for (int i = 0; i < stateCityDataModels.size(); i++) {
                            if (item.equalsIgnoreCase(stateCityDataModels.get(i).getState_hindi())) {
                                newState = stateCityDataModels.get(i).getState_english();
                            }
                        }
                    }
                } else {
                    newState = "";
                    rlCitySpinner.setVisibility(View.GONE);
                    txtAddress.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void setCityListData(final String state) {
        ArrayList<String> cityList = new ArrayList<>();
        for (int i = 0; i < stateCityDataModels.size(); i++) {
            if (state.equalsIgnoreCase(stateCityDataModels.get(i).getState_hindi())) {
                for (int j = 0; j < stateCityDataModels.get(i).getCityList().size(); j++) {
                    cityList.add(stateCityDataModels.get(i).getCityList().get(j).getCity_hindi());
                }
            }
        }

        Spinner spin = (Spinner) findViewById(R.id.spinnerCity);
        spinnerArrayAdapter = new SpinnerHintArrayAdapter(this, R.layout.spinner_item_list, cityList);
        spinnerArrayAdapter.add(getString(R.string.str_select_jila));
        spin.setAdapter(spinnerArrayAdapter);
        spin.setSelection(spinnerArrayAdapter.getCount());
        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                if (!item.equalsIgnoreCase(getString(R.string.str_select_jila))) {
                    if (item.equalsIgnoreCase(getString(R.string.str_other))) {
                        txtAddress.setVisibility(View.VISIBLE);
                        newCity = "Other";
                    } else {
                        txtAddress.setVisibility(View.GONE);
                        for (int i = 0; i < stateCityDataModels.size(); i++) {
                            if (state.equalsIgnoreCase(stateCityDataModels.get(i).getState_hindi())) {
                                for (int j = 0; j < stateCityDataModels.get(i).getCityList().size(); j++) {
                                    if (item.equalsIgnoreCase(stateCityDataModels.get(i).getCityList().get(j).getCity_hindi())) {
                                        newCity = stateCityDataModels.get(i).getCityList().get(j).getCity_english();
                                    }
                                }
                            }
                        }
                    }
                } else {
                    newCity = "";
                    txtAddress.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logSubmitFBEvent(String internet) {
        Bundle params = new Bundle();
        params.putString(Constants.FB_EVENT_PARAM_REGISTRATION_METHOD, "Phone");
//        params.putString(Constants.FB_EVENT_PARAM_NUMBER, "" + editTextPhonenumber.getText().toString().trim());
        AppEventsLogger.newLogger(this).logEvent(Constants.FB_EVENT_NAME_REGISTRATION_METHOD, params);
    }

    String getEnglishState(String state) {
        String stateEng = state;
        try {
            if (!TextUtils.isEmpty(state) && stateCityDataModels.size() > 0) {
                for (int i = 0; i < stateCityDataModels.size(); i++) {
                    if (newState.equalsIgnoreCase(stateCityDataModels.get(i).getState_hindi())) {
                        stateEng = stateCityDataModels.get(i).getState_english();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stateEng;
    }

    String getEnglishStateCity(String state, String city) {
        String cityEng = city;
        try {
            if (!TextUtils.isEmpty(state) && stateCityDataModels.size() > 0) {
                for (int i = 0; i < stateCityDataModels.size(); i++) {
                    if (state.equalsIgnoreCase(stateCityDataModels.get(i).getState_hindi())) {
                        for (int j = 0; j < stateCityDataModels.get(i).getCityList().size(); j++) {
                            if (city.equalsIgnoreCase(stateCityDataModels.get(i).getCityList().get(j).getCity_hindi())) {
                                cityEng = stateCityDataModels.get(i).getCityList().get(j).getCity_english();
                            }
                        }
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cityEng;
    }

    JSONArray getDeviceInfoJson() {
        JSONArray jsonArray = new JSONArray();
        try {
            JSONObject obj = new JSONObject();
            obj.put("model_no_name", mAppOpenData.getmModel());
            obj.put("device_os", mAppOpenData.getmOsVersion());
            obj.put("device_brand_name", mAppOpenData.getmBrand());
            obj.put("device_id", mAppOpenData.getAndroidId());
            jsonArray.put(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    void register() {
        Functions.setFirebaseLogEventTrack(RegisterActivity.this, Constants.Click_Event, "Register", editTextPhonenumber.getText().toString().trim(), "Register");

        ProgressBarDialog progressBarDialog = new ProgressBarDialog(RegisterActivity.this);
        progressBarDialog.showProgressDialogWithTitle(getResources().getString(R.string.app_name));

        RetrofitClient.getInstance().getApi().register(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                Functions.getDeviceid(this), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                editTextFirstName.getText().toString().trim(), "", editTextPhonenumber.getText().toString().trim(),
                RongoApp.getDefaultLanguage(), newCity, getEnglishState(newState), country, latitude + "", longitude + "",
                getEnglishState(state), getEnglishStateCity(state, city), area, getDeviceInfoJson().toString(),
                referred_by, referral_source, sharedPreferences.getString(Constants.SharedPreference_NetworkType, "2G")).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (!TextUtils.isEmpty(response.toString()) && response != null) {
                    try {
                        String responseStrig = response.body().string();
                        if (!TextUtils.isEmpty(responseStrig)) {
                            JSONObject jb = new JSONObject(responseStrig);
                            if (jb != null && jb.length() > 0) {
                                if (jb.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject jData = jb.optJSONObject("data");
                                    if (jData != null && jData.length() > 0) {
                                        JSONObject jResult = jData.optJSONObject("result");
                                        if (jResult != null && jResult.length() > 0) {
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_UserId, jResult.optString("user_id"));
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_MOBILE_NO, editTextPhonenumber.getText().toString().trim());

                                            Intent i = new Intent(RegisterActivity.this, OTPActivity.class);
                                            i.putExtra("otp", jResult.optString("otp"));
                                            i.putExtra("mobileNumber", editTextPhonenumber.getText().toString().trim());
                                            startActivity(i);
                                        }
                                    }
                                } else if (jb.optString("status_code").equalsIgnoreCase("1")) {
                                    String message = jb.optString("status_message");
                                    if (message.equalsIgnoreCase(getString(R.string.str_number_already_register))) {
                                        String htmlMessage = TextUtils.htmlEncode(message);
                                        String content = htmlMessage + " " + getResources().getString(R.string.str_login);
                                        CustomSnackbar customSnackbar = CustomSnackbar.make((ViewGroup) findViewById(android.R.id.content), CustomSnackbar.LENGTH_LONG);
                                        ClickableSpan clickableSpan = new ClickableSpan() {
                                            @Override
                                            public void onClick(View view) {
                                                customSnackbar.dismiss();
                                                Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                                                startActivity(i);
                                            }

                                            @Override
                                            public void updateDrawState(TextPaint ds) {
                                                super.updateDrawState(ds);
                                                int color = ContextCompat.getColor(RegisterActivity.this, R.color.white);
                                                ds.setColor(color);
                                                ds.setTypeface(ResourcesCompat.getFont(RegisterActivity.this, R.font.roboto_bold));
                                                ds.setUnderlineText(true);
                                            }
                                        };
                                        SpannableStringBuilder ssBuilder = new SpannableStringBuilder(content);
                                        ssBuilder.setSpan(clickableSpan, content.indexOf(getResources().getString(R.string.str_login)), content.indexOf(getResources().getString(R.string.str_login)) + String.valueOf(getResources().getString(R.string.str_login)).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                                        View view1 = customSnackbar.getView();
                                        customSnackbar.getView().setPadding(0, 0, 0, 0);
                                        AppCompatTextView appCompatTextView = view1.findViewById(R.id.snackbar_text);
                                        appCompatTextView.setText(ssBuilder);
                                        appCompatTextView.setMovementMethod(LinkMovementMethod.getInstance());
                                        appCompatTextView.setHighlightColor(Color.TRANSPARENT);
                                        customSnackbar.show();
                                    }

                                    setAlarmNotification();
                                } else if (jb.optString("status_code").equalsIgnoreCase("2")) {
                                    Functions.showSnackBar(findViewById(android.R.id.content), "" + jb.optString("status_message"));
                                    setAlarmNotification();
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                progressBarDialog.hideProgressDialogWithTitle();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        });
    }

    boolean fieldsValidation() {
        if (TextUtils.isEmpty(editTextFirstName.getText().toString().trim())) {
            Functions.showSnackBar(findViewById(android.R.id.content), getString(R.string.str_please_enter_first_name));
            return false;
        }
        if (TextUtils.isEmpty(editTextPhonenumber.getText().toString().trim())) {
            Functions.showSnackBar(findViewById(android.R.id.content), getString(R.string.str_please_enter_you_number));
            return false;
        }
        if (!TextUtils.isEmpty(editTextPhonenumber.getText().toString().trim()) && editTextPhonenumber.getText().toString().trim().length() < 10) {
            Functions.showSnackBar(findViewById(android.R.id.content), getString(R.string.str_please_enter_valid_number));
            return false;
        }
        if (TextUtils.isEmpty(newState)) {
            Functions.showSnackBar(findViewById(android.R.id.content), getString(R.string.str_please_enter_state));
            return false;
        }
        if (TextUtils.isEmpty(newCity)) {
            Functions.showSnackBar(findViewById(android.R.id.content), getString(R.string.str_please_enter_jilla));
            return false;
        }
        return true;
    }

    void setAlarmNotification() {
        Intent notificationIntent = new Intent(RegisterActivity.this, RegisterActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notificationIntent.setAction(Long.toString(System.currentTimeMillis()));

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(notificationIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "com.rongoapp.channelId")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(getString(R.string.str_notification_fail_to_register)))
                .setAutoCancel(true)
                .setChannelId("com.rongoapp.channelId")
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.mipmap.ic_launcher))
                .setSound(defaultSoundUri)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setSmallIcon(R.mipmap.ic_launcher)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String description = "Notifications";
            NotificationChannel mChannel = new NotificationChannel("com.rongoapp.channelId", "Product", NotificationManager.IMPORTANCE_HIGH);
            mChannel.setDescription(description);
            mChannel.enableLights(true);
            mChannel.setShowBadge(true);
            mChannel.setLightColor(Color.RED);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(mChannel);
        }
        notificationManager.notify(ServiceUtility.randInt(0, 9999999), notificationBuilder.build());
    }

}