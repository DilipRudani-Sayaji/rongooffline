package com.rongoapp.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.text.HtmlCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.card.MaterialCardView;
import com.rongoapp.R;
import com.rongoapp.adapter.CommunityQuesDetailsAdapter;
import com.rongoapp.adapter.ImagesSavdhaniAdapter;
import com.rongoapp.controller.AudioPlayerTask;
import com.rongoapp.controller.AudioRecorderCallback;
import com.rongoapp.controller.AudioRecorderDialog;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.InitInterface;
import com.rongoapp.controller.LocaleManager;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.ExpertResponceModel;
import com.rongoapp.data.InsertHomeFeedData;
import com.rongoapp.data.JankariFeedBackData;
import com.rongoapp.data.OfflineCommentData;
import com.rongoapp.data.OfflineLikeData;
import com.rongoapp.data.PostData;
import com.rongoapp.data.ProblemRelatedData;
import com.rongoapp.data.StateCityDataModel;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.Log;
import com.rongoapp.views.ProgressBarDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommunityDetailsActivity extends AppCompatActivity implements InitInterface {

    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    ProgressBarDialog progressBarDialog;

    long mLastLongClick = 0;
    int currentPositionTop = 0;
    ProgressBar progressBar, progress;
    ViewPager pager;
    FrameLayout frameLayout;
    NestedScrollView nsv;

    MaterialCardView cardViewCommunityOpen, cardViewFeedBack;
    RelativeLayout rlChatView, rlAudioPathView;
    AppCompatEditText edtComment;
    LinearLayout llAudioView, llUserDetail;
    ImageView imgBack, imgPlay, imgPrevios, imgNext, mPlayButton, ivIsExpert, ivVoiceRecorder, ivIsReport;
    AppCompatTextView txtQuestionRelated, txtDate, txtDescription, tvSendComment, tvUserName, tvUserLocation, tvLike,
            tvShare, tvView, tvCropWeekName, tvCommentCount, tvCommentCount1, tvOffline, tvNoComment, txtYes, txtNo,
            tvAudioPath, txtYes1, txtNo1;
    RecyclerView rvDetails;
    CommunityQuesDetailsAdapter salahQuestionDetailsAdapter;

    PostData postData;
    List<String> pictures = new ArrayList<>();
    List<ExpertResponceModel> expertResponceModels = new ArrayList<ExpertResponceModel>();

    String post_id = "", filePath = "", audio_Path = "", imagePath = "";
    String fromNotification = "N", p_crop_id = "0", type = "0", fileName = "";
    int optionType;

    AudioPlayerTask audioPlayerTask;
    AudioRecorderDialog audioRecorderDialog;
    AudioRecorderCallback audioRecorderCallback;

    BroadcastReceiver AUDIO_RECORD_RECEIVER = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent != null) {
                    fileName = intent.getStringExtra("mFileName");
                    filePath = intent.getStringExtra("mFilePath");
                    if (audioRecorderCallback != null) {
                        audioRecorderCallback.onDone("1");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerReceiver(AUDIO_RECORD_RECEIVER, new IntentFilter(Constants.BROADCAST_RECEIVER.AUDIO_RECORD_RECEIVER));
        setContentView(R.layout.activity_question_details);

        progressBarDialog = new ProgressBarDialog(CommunityDetailsActivity.this);
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(this);
        p_crop_id = Functions.getP_Crop_Id(sharedPreferences);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            if (b.containsKey("postData")) {
                postData = (PostData) b.getSerializable("postData");
            }
            if (b.containsKey("imagePath")) {
                imagePath = b.getString("imagePath");
            }
            if (b.containsKey("post_id")) {
                post_id = b.getString("post_id");
            }
            if (b.containsKey("fromNotification")) {
                fromNotification = b.getString("fromNotification");
            }
            if (b.containsKey("type")) {
                type = b.getString("type");
            }
        }

        Functions.setFirebaseLogEventTrack(CommunityDetailsActivity.this, Constants.Screen_Track, "Salah Detail", post_id, "");
        Functions.setUserScreenTrackEvent(appDatabase, "Salah Detail", "" + post_id);
        audioPlayerTask = new AudioPlayerTask(CommunityDetailsActivity.this, mPlayButton);

        init();
        setListener();

        if (postData != null) {
            bindData();
            Log.e("postData: ", "" + postData.getPosted_user_name());
        }
        if (InternetConnection.checkConnection(this)) {
            if (!TextUtils.isEmpty(post_id)) {
                getSingleCommunityPost();
            }
            if (type.equalsIgnoreCase("0")) {
                viewExpertResponse();
            }
            viewCommunityPost();
        }
    }

    @Override
    public void init() {
        cardViewCommunityOpen = findViewById(R.id.cardViewCommunityOpen);
        cardViewCommunityOpen.setVisibility(View.GONE);
        txtYes = findViewById(R.id.txtYes);
        txtNo = findViewById(R.id.txtNo);

        cardViewFeedBack = findViewById(R.id.cardViewFeedBack);
        cardViewFeedBack.setVisibility(View.GONE);
        txtYes1 = findViewById(R.id.txtYes1);
        txtNo1 = findViewById(R.id.txtNo1);

        progressBar = findViewById(R.id.progressBar);
        progress = findViewById(R.id.progress);
        progress.setVisibility(View.GONE);
        nsv = findViewById(R.id.nsv);
        txtDescription = findViewById(R.id.txtDescription);
        imgPrevios = findViewById(R.id.imgPrevios);
        frameLayout = findViewById(R.id.frameLayout);
        imgNext = findViewById(R.id.imgNext);
        pager = findViewById(R.id.pager);
        imgPlay = findViewById(R.id.imgPlay);
        imgBack = findViewById(R.id.imgBack);
        rvDetails = findViewById(R.id.rvDetails);
        txtQuestionRelated = findViewById(R.id.txtQuestionRelated);

        llUserDetail = findViewById(R.id.llUserDetail);

        txtDate = findViewById(R.id.txtDate);
        llAudioView = findViewById(R.id.llAudioView);
        llAudioView.setVisibility(View.GONE);

        rvDetails.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));

        rlChatView = findViewById(R.id.rlChatView);
        edtComment = findViewById(R.id.edtComment);
        tvSendComment = findViewById(R.id.tvSendComment);

        tvUserName = findViewById(R.id.tvUserName);
        tvUserLocation = findViewById(R.id.tvUserLocation);
        tvLike = findViewById(R.id.tvLike);
        tvShare = findViewById(R.id.tvShare);
        tvView = findViewById(R.id.tvView);
        tvCropWeekName = findViewById(R.id.tvCropWeekName);
        tvCommentCount = findViewById(R.id.tvCommentCount);
        tvCommentCount1 = findViewById(R.id.tvCommentCount1);
        tvCommentCount1.setText("0 " + getResources().getString(R.string.str_comments_txt));
        tvOffline = findViewById(R.id.tvOffline);
        tvOffline.setVisibility(View.GONE);
        tvNoComment = findViewById(R.id.tvNoComment);

        ivIsExpert = findViewById(R.id.ivIsExpert);
        ivVoiceRecorder = findViewById(R.id.ivVoiceRecorder);
        tvAudioPath = findViewById(R.id.tvAudioPath);
        rlAudioPathView = findViewById(R.id.rlAudioPathView);

        ivIsReport = findViewById(R.id.ivIsReport);
        ivIsReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openReport(post_id);
            }
        });
    }

    @Override
    public void setListener() {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fromNotification.equalsIgnoreCase("Y")) {
                    Intent i = new Intent(CommunityDetailsActivity.this, HomeActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                    finishAffinity();
                    overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
                } else {
                    finish();
                }
            }
        });

        tvLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                if (!TextUtils.isEmpty(postData.getVote_type())) {
                    if (postData.getVote_type().equalsIgnoreCase("1")) {
                        tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.like, 0, 0, 0);
                    } else {
                        tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.unlike, 0, 0, 0);
                        likeCommunityPost();
                    }
                } else {
                    tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.unlike, 0, 0, 0);
                    likeCommunityPost();
                }
            }
        });

        tvShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    progressBarDialog.showProgressDialogWithTitle(getResources().getString(R.string.app_name));
                    String test = getResources().getString(R.string.str_rongo_community) + postData.getDescription() + "</br> <br>" + postData.getShare_link() + "</br>";
                    String shareTxt = "" + HtmlCompat.fromHtml(test, HtmlCompat.FROM_HTML_MODE_LEGACY);

                    if (InternetConnection.checkConnectionForFasl(CommunityDetailsActivity.this)) {
                        JSONArray jsonArray = new JSONArray(postData.getPicture_key());
                        if (jsonArray != null && jsonArray.length() > 0) {
                            if (TextUtils.isEmpty(imagePath)) {
                                imagePath = Constants.IMAGE_PATH_LINK;
                            }

                            Functions.shareCommunity(CommunityDetailsActivity.this, shareTxt, imagePath + jsonArray.opt(0).toString());

                        } else {
                            Functions.shareCommunity(CommunityDetailsActivity.this, shareTxt, "");
                        }
                    } else {
                        Functions.shareCommunity(CommunityDetailsActivity.this, shareTxt, "");
                    }
                    progressBarDialog.hideProgressDialogWithTitle();
                } catch (Exception e) {
                    e.printStackTrace();
                    progressBarDialog.hideProgressDialogWithTitle();
                    Functions.showSnackBar(findViewById(android.R.id.content), "" + e.getMessage());
                }
            }
        });

        ivVoiceRecorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                audioRecorderCallback = status -> {
                    if (status.equalsIgnoreCase("1")) {
                        tvAudioPath.setText(fileName);
                        ivVoiceRecorder.setVisibility(View.VISIBLE);
                        rlAudioPathView.setVisibility(View.VISIBLE);
                    }
                };
                audioRecorderDialog = new AudioRecorderDialog(audioRecorderCallback);
                audioRecorderDialog.show(getSupportFragmentManager(), "AudioRecorderDialog");
            }
        });

        tvSendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                if (!TextUtils.isEmpty(edtComment.getText().toString().trim())) {
                    rlAudioPathView.setVisibility(View.GONE);
                    tvAudioPath.setText("");
                    if (InternetConnection.checkConnectionForFasl(CommunityDetailsActivity.this)) {
                        if (InternetConnection.getNetworkClass(CommunityDetailsActivity.this).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                            saveCommentDataOffline(true, true);
                            Toast.makeText(CommunityDetailsActivity.this, "" + getResources().getString(R.string.str_internet_comment_msg), Toast.LENGTH_SHORT).show();
                        } else {
                            sendPostComment(post_id, edtComment.getText().toString());
                        }
                    } else {
                        saveCommentDataOffline(true, true);
                        Toast.makeText(CommunityDetailsActivity.this, "" + getResources().getString(R.string.str_internet_comment_msg), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Functions.showSnackBar(findViewById(android.R.id.content), "" + getResources().getString(R.string.str_feedback_edittext));
                }
            }
        });

        imgPrevios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    pager.setCurrentItem(getNextPossibleItemIndex(-1));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    pager.setCurrentItem(getNextPossibleItemIndex(1));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                currentPositionTop = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        llAudioView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                progress.setVisibility(View.VISIBLE);
                audioPlayerTask.onPlayMedia(audio_Path);
            }
        });

        mPlayButton = (ImageView) findViewById(R.id.ivPlay);
        mPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                progress.setVisibility(View.VISIBLE);
                audioPlayerTask.onPlayMedia(audio_Path);
            }
        });

        txtNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                updateCommunityVisibility("0");
            }
        });

        txtYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                updateCommunityVisibility("1");
            }
        });

        txtNo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                if (InternetConnection.checkConnection(CommunityDetailsActivity.this)) {
                    saveExpertResponseFeedback("2");
                } else {
                    saveYesNoData("0");
                    openThankYou("Y");
                }
            }
        });

        txtYes1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                if (InternetConnection.checkConnection(CommunityDetailsActivity.this)) {
                    saveExpertResponseFeedback("1");
                } else {
                    saveYesNoData("1");
                    openThankYou("Y");
                }
            }
        });
    }


    void openReport(String id) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(CommunityDetailsActivity.this);
        View view = LayoutInflater.from(this).inflate(R.layout.bottom_report_option, null);
        bottomSheetDialog.setContentView(view);

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        RelativeLayout rlReportView = view.findViewById(R.id.rlReportView);
        rlReportView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                reportUpdateDialog(id);
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    void reportUpdateDialog(String id) {
        try {
            Dialog dialog = new Dialog(CommunityDetailsActivity.this, R.style.NewDialog);
            dialog.setContentView(R.layout.report_update_dialog);
            dialog.show();

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);

            ImageView imgClose = dialog.findViewById(R.id.imgClose);
            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            RecyclerView rcvReport = dialog.findViewById(R.id.rcvReport);
            ArrayList<String> typeArray = new ArrayList<>();
            try {
                JSONArray jsonArray = new JSONArray(sharedPreferences.getString(Constants.SharedPreferences_ReportPost, null));
                if (jsonArray != null && jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        typeArray.add("" + jsonArray.get(i));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            rcvReport.setLayoutManager(new LinearLayoutManager(CommunityDetailsActivity.this, RecyclerView.VERTICAL, false));
            rcvReport.setAdapter(new TypesAdapter(typeArray));

            AppCompatTextView txtDarjKare = dialog.findViewById(R.id.txtDarjKare);
            txtDarjKare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!TextUtils.isEmpty(String.valueOf(optionType))) {
                        dialog.dismiss();
                        Functions.setCustomBlackToastBar(CommunityDetailsActivity.this, getResources().getString(R.string.str_report_darj));
                        RongoApp.reportUserContent(String.valueOf(optionType), Constants.POST, id);
                    } else {
                        Functions.setCustomBlackToastBar(CommunityDetailsActivity.this, getResources().getString(R.string.str_option_select));
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class TypesAdapter extends RecyclerView.Adapter<TypesAdapter.ViewHolder> {
        List<String> listdata;

        public TypesAdapter(List<String> listdata) {
            this.listdata = listdata;
        }

        @Override
        public TypesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_option_type, parent, false);
            TypesAdapter.ViewHolder viewHolder = new TypesAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(TypesAdapter.ViewHolder holder, int position) {
            holder.textView.setText(listdata.get(position));

            if (position == optionType) {
                holder.imgSelection.setImageResource(R.mipmap.uria_selected);
            } else {
                holder.imgSelection.setImageResource(R.mipmap.uria_unselected);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    optionType = position;
                    notifyDataSetChanged();
                }
            });
        }

        @Override
        public int getItemCount() {
            return listdata.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView textView;
            ImageView imgSelection;

            public ViewHolder(View itemView) {
                super(itemView);
                this.textView = itemView.findViewById(R.id.txtSeedName);
                this.imgSelection = itemView.findViewById(R.id.imgSelection);
            }
        }
    }

    void saveExpertResponseFeedback(String isApplied) {
        RetrofitClient.getInstance().getApi().saveExpertResponseFeedback(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                Functions.getDeviceid(this), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                post_id, isApplied, isApplied, SharedPreferencesUtils.getUserId(this)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (fromNotification.equalsIgnoreCase("Y")) {
            Intent i = new Intent(CommunityDetailsActivity.this, HomeActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finishAffinity();
            overridePendingTransition(R.anim.activity_back_in, R.anim.activity_back_out);
        } else {
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        nsv.post(new Runnable() {
//            @Override
//            public void run() {
//                nsv.fullScroll(NestedScrollView.FOCUS_UP);
//            }
//        });
    }

    @Override
    protected void onDestroy() {
        try {
            if (audioPlayerTask != null) {
                audioPlayerTask.stopPlaying();
            }
            unregisterReceiver(AUDIO_RECORD_RECEIVER);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    int getNextPossibleItemIndex(int change) {
        int currentIndex = pager.getCurrentItem();
        int total = 0;

        if (pager.getAdapter() != null) {
            total = pager.getAdapter().getCount();
        }
        if (currentPositionTop + change < 0) {
            return 0;
        }

        return Math.abs((currentIndex + change) % total);
    }

    void openThankYou(String str) {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
        View view = LayoutInflater.from(this).inflate(R.layout.bottom_thank_you_dialog, null);
        bottomSheetDialog.setContentView(view);

        AppCompatTextView tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText(RewardPoints.getPostQuestionRewardPoint(sharedPreferences));

        AppCompatTextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));

        AppCompatTextView txtTitle = view.findViewById(R.id.txtTitle);

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        AppCompatTextView txtEnter = view.findViewById(R.id.txtEnter);
        txtEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                bottomSheetDialog.dismiss();
                Functions.performShare(CommunityDetailsActivity.this, appDatabase);
            }
        });

        if (str.equalsIgnoreCase("Y")) {
            txtTitle.setText(getString(R.string.str_thank_you));
        } else {
            txtTitle.setText(R.string.str_thank_for_giving_responce);
        }

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 67 && resultCode == Activity.RESULT_OK) {
            openThankYou("N");
        }
    }


    void viewCommunityPost() {
        RetrofitClient.getInstance().getApi().viewCommunityPost(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                SharedPreferencesUtils.getUserId(this), post_id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void likeCommunityPost() {
        if (InternetConnection.checkConnectionForFasl(this)) {
            progressBarDialog.showProgressDialogWithTitle(getResources().getString(R.string.app_name));
            RetrofitClient.getInstance().getApi().likeCommunityPost(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    SharedPreferencesUtils.getUserId(this), postData.getPost_id(), "", "1").enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        setLikeEvent(false);
                        progressBarDialog.hideProgressDialogWithTitle();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressBarDialog.hideProgressDialogWithTitle();
                }
            });
        } else {
            setLikeEvent(true);
        }
    }

    void setLikeEvent(boolean isInsert) {
        try {
            if (isInsert) {
                final List<OfflineLikeData> offlineLikeData = new ArrayList<OfflineLikeData>();
                offlineLikeData.add(new OfflineLikeData(new Random().nextInt(), postData.getPost_id(), "1", "", Functions.getCurrentDateTime()));
                appDatabase.offlineLikeDao().insertAll(offlineLikeData);
            }

            int count = Integer.parseInt(postData.getLikes());
            postData.setLikes(String.valueOf(count + 1));
            postData.setVote_type("1");
            bindData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    RequestBody getRequestAudioFile(File file) {
        RequestBody fbody = RequestBody.create(file, MediaType.parse("audio/*"));
        return fbody;
    }

    void sendPostComment(String postID, String comment) {
        RequestBody requestBodyId = RequestBody.create(SharedPreferencesUtils.getUserId(this).trim(), MediaType.parse("text/plain"));
        RequestBody requestBodyDeviceId = RequestBody.create(Functions.getDeviceid(this), MediaType.parse("text/plain"));
        RequestBody requestBodyFCMToken = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyAppVersion = RequestBody.create(RongoApp.getAppVersion(), MediaType.parse("text/plain"));
        RequestBody requestBodyAppLanguage = RequestBody.create(RongoApp.getDefaultLanguage(), MediaType.parse("text/plain"));
        RequestBody requestBodyPost_id = RequestBody.create(postID, MediaType.parse("text/plain"));
        RequestBody requestBodyType = RequestBody.create(edtComment.getText().toString(), MediaType.parse("text/plain"));
        RequestBody requestBodyAppSeason = RequestBody.create(RongoApp.getSeasonName(), MediaType.parse("text/plain"));

        Map<String, RequestBody> params = new HashMap<>();
        params.put("user_id", requestBodyId);
        params.put("device_id", requestBodyDeviceId);
        params.put("fcm_token", requestBodyFCMToken);
        params.put("app_version", requestBodyAppVersion);
        params.put("app_language", requestBodyAppLanguage);
        params.put("post_id", requestBodyPost_id);
        params.put("comment_text", requestBodyType);
        params.put("season_name", requestBodyAppSeason);

        if (!TextUtils.isEmpty(filePath)) {
            RequestBody requestBodyPath = RequestBody.create(filePath, MediaType.parse("text/plain"));
            params.put("recording_key", requestBodyPath);
            params.put("audio_path[]", requestBodyPath);
            File auFile = new File(filePath);
            params.put("audio[]" + "\"; filename=\"" + auFile.getName(), getRequestAudioFile(auFile));
        }

        RetrofitClient.getInstance().getApi().savePostComment(params).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                saveCommentDataOffline(false, true);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }


    void viewExpertResponse() {
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(post_id);

        RetrofitClient.getInstance().getApi().viewExpertResponse(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(this),
                sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), "" + jsonArray,
                SharedPreferencesUtils.getUserId(this)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String str = response.body().string();
                        if (!TextUtils.isEmpty(str)) {
                            JSONObject jsonObject = new JSONObject(str);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    SharedPreferencesUtils.storeStateLong(sharedPreferences, Constants.SharedPreference_Time_FOR_API_CALL, System.currentTimeMillis());
                                    if (InternetConnection.checkConnectionForFasl(CommunityDetailsActivity.this)) {
                                        if (!InternetConnection.getNetworkClass(CommunityDetailsActivity.this).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                            getProfile();
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    void getProfile() {
        RetrofitClient.getInstance().getApi().getProfile(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(this),
                sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), SharedPreferencesUtils.getUserId(this)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String r_str = response.body().string();
                        if (!TextUtils.isEmpty(r_str)) {
                            JSONObject jb = new JSONObject(r_str);
                            if (jb != null && jb.length() > 0) {
                                new InsertHomeFeedData(jb).insertData();
                                // For Showing Badge
                                if (HomeActivity.mCircleNavigationView != null) {
                                    HomeActivity.mCircleNavigationView.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            int salahCount = Integer.parseInt(sharedPreferences.getString(Constants.SharedPreference_Salah_Counter, "0"));
                                            if (salahCount > 0) {
                                                HomeActivity.mCircleNavigationView.showBadgeAtIndex(2, salahCount, Color.RED);
                                                HomeActivity.mCircleNavigationView.shouldShowFullBadgeText(true);
                                            } else {
                                                HomeActivity.mCircleNavigationView.hideBadgeAtIndex(2);
                                            }
                                        }
                                    }, 1000);
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }


    void getSingleCommunityPost() {
        RetrofitClient.getInstance().getApi().getSingleCommunityPost(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                SharedPreferencesUtils.getUserId(this), post_id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String str = response.body().string();
                        if (!TextUtils.isEmpty(str)) {
                            JSONObject jsonObject = new JSONObject(str);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject data = jsonObject.optJSONObject("data");
                                    if (data != null && data.length() > 0) {
                                        JSONObject result = data.optJSONObject("result");
                                        if (result != null && result.length() > 0) {
                                            imagePath = result.optString("image_path");
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_AUDIO_PATH, result.optString("audio_path"));

                                            JSONArray jsonArray = result.optJSONArray("posts");
                                            if (jsonArray != null && jsonArray.length() > 0) {
                                                postData = Functions.getPostDataByPostId(CommunityDetailsActivity.this, jsonArray, post_id);
                                                bindData();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(CommunityDetailsActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    void saveYesNoData(String yesNo) {
        try {
            List<JankariFeedBackData> jankariFeedBackData = new ArrayList<>();
            jankariFeedBackData.add(new JankariFeedBackData(0, Constants.saveExpertResponseFeedback, yesNo, p_crop_id, "0", post_id, yesNo));
            appDatabase.jankariFeedbackDao().insertAll(jankariFeedBackData);
            cardViewFeedBack.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void bindData() {
        try {
            if (postData != null) {
                post_id = postData.getPost_id();
                Log.v("post_id: ", "" + post_id);
//                tvCropWeekName.setText("" + postData.getCrop_age() + " " + getResources().getString(R.string.saptah));

                if (postData.getPosted_user_location() != null && !TextUtils.isEmpty(postData.getPosted_user_location())) {
                    tvUserLocation.setText("" + postData.getPosted_user_location());
                    String[] locationName = postData.getPosted_user_location().split(",");
                    if (InternetConnection.checkConnection(this)) {
                        if (locationName[1] != null && locationName[1].length() > 0) {
                            List<StateCityDataModel> parseStateCityListData = Functions.parseStateCityListData(sharedPreferences);
                            for (int i = 0; i < parseStateCityListData.size(); i++) {
                                if (locationName[1].trim().equalsIgnoreCase(parseStateCityListData.get(i).getState_english())) {
                                    for (int j = 0; j < parseStateCityListData.get(i).getCityList().size(); j++) {
                                        if (locationName[0].trim().equalsIgnoreCase(parseStateCityListData.get(i).getCityList().get(j).getCity_english())) {
                                            tvUserLocation.setText(parseStateCityListData.get(i).getCityList().get(j).getCity_hindi()
                                                    + ", " + parseStateCityListData.get(i).getState_hindi());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (postData.getIs_community_visible().equalsIgnoreCase("0")) {
                    cardViewCommunityOpen.setVisibility(View.VISIBLE);
                }
                if (postData.getPost_type().equalsIgnoreCase("3")) {
                    llUserDetail.setVisibility(View.GONE);
                    tvUserName.setText(getResources().getString(R.string.str_rongo_expert));
                } else {
                    tvUserName.setText("" + postData.getPosted_user_name());
                    llUserDetail.setVisibility(View.VISIBLE);
                }
                if (postData.getIs_expert().equalsIgnoreCase("1")) {
                    ivIsExpert.setVisibility(View.VISIBLE);
                } else {
                    ivIsExpert.setVisibility(View.GONE);
                }

                if (postData.getIs_viewd().equalsIgnoreCase("0")) {
                    if (postData.getIs_applied() != null && postData.getIs_applied().equalsIgnoreCase("0")) {
                        cardViewFeedBack.setVisibility(View.VISIBLE);
                    } else {
                        cardViewFeedBack.setVisibility(View.GONE);
                    }
                }

                if (!TextUtils.isEmpty(postData.getVote_type())) {
                    if (postData.getVote_type().equalsIgnoreCase("1")) {
                        tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.like, 0, 0, 0);
                    } else {
                        tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.unlike, 0, 0, 0);
                    }
                } else {
                    tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.unlike, 0, 0, 0);
                }

                if (appDatabase.offlineLikeDao().getLikeByPostId(post_id).size() > 0) {
                    postData.setVote_type("1");
                    tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.like, 0, 0, 0);
                }

                if (!TextUtils.isEmpty(postData.getLikes())) {
                    tvLike.setText(postData.getLikes() + " " + getResources().getString(R.string.str_like_txt));
                } else {
                    tvLike.setText("0 " + getResources().getString(R.string.str_like_txt));
                }

                if (!TextUtils.isEmpty(postData.getViews())) {
                    tvView.setText(postData.getViews() + " " + getResources().getString(R.string.str_view_txt));
                } else {
                    tvView.setText("0 " + getResources().getString(R.string.str_view_txt));
                }

                if (!TextUtils.isEmpty(postData.getComments())) {
                    JSONArray jsonArray = new JSONArray(postData.getComments());
                    if (jsonArray != null && jsonArray.length() > 0) {
                        tvCommentCount.setText(jsonArray.length() + " " + getResources().getString(R.string.str_comments_txt));
                        tvCommentCount1.setText(jsonArray.length() + " " + getResources().getString(R.string.str_comments_txt));
                        tvCommentCount.setVisibility(View.VISIBLE);
                    } else {
                        tvCommentCount.setVisibility(View.GONE);
                    }
                } else {
                    tvCommentCount.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(postData.getDescription())) {
                    txtDescription.setVisibility(View.VISIBLE);
                    txtDescription.setText(HtmlCompat.fromHtml(postData.getDescription(), HtmlCompat.FROM_HTML_MODE_LEGACY));
                } else {
                    txtDescription.setVisibility(View.GONE);
                }

                audio_Path = "";
                llAudioView.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(postData.getRecording_key()) && !postData.getRecording_key().equalsIgnoreCase("null")) {
                    if (InternetConnection.checkConnectionForFasl(CommunityDetailsActivity.this)) {
                        JSONArray jArray = new JSONArray(postData.getRecording_key().toString());
                        if (jArray != null && jArray.length() > 0) {
                            audio_Path = sharedPreferences.getString(Constants.SharedPreferences_AUDIO_PATH, Constants.AUDIO_PATH_LINK) + jArray.opt(0).toString();
                            llAudioView.setVisibility(View.VISIBLE);
                            Log.v("audio_Path: yes-", "if- " + audio_Path);
                        }
                    }
                } else {
                    if (!postData.getRecording_local_path().equalsIgnoreCase("null") && !TextUtils.isEmpty(postData.getRecording_local_path())) {
                        audio_Path = postData.getRecording_local_path();
                        File file = new File(Functions.replaceAudioLinkString(audio_Path));
                        if (file.exists()) {
                            llAudioView.setVisibility(View.VISIBLE);
                            Log.v("audio_Path: yes-", "else- " + audio_Path);
                        }
                    }
                }

                if (!InternetConnection.checkConnectionForFasl(CommunityDetailsActivity.this)) {
                    /*
                     * TODO Manage Salah view Event
                     * */
                    if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreference_Salah_PostId, ""))) {
                        ArrayList<String> stringList = new ArrayList<String>(Arrays.asList(sharedPreferences.getString(Constants.SharedPreference_Salah_PostId, "").split(",")));
                        List<String> ids = new ArrayList<String>();
                        if (stringList != null && stringList.size() > 0) {
                            for (int i = 0; i < stringList.size(); i++) {
                                ids.add(stringList.get(i));
                            }
                        }
                        ids.add(post_id);

                        String csv = android.text.TextUtils.join(",", ids);
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Salah_PostId, csv);
                        stringList.clear();
                    } else {
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Salah_PostId, post_id);
                    }

                    /*
                     * TODO manage Salah Badge Counter value
                     * */
                    int salahCount = Integer.parseInt(sharedPreferences.getString(Constants.SharedPreference_Salah_Counter, "0"));
                    if (salahCount > 0) {
                        PostData postData = appDatabase.postDao().getPostByPostId(post_id);
                        if (postData.getIs_viewd().equalsIgnoreCase("0")) {
                            salahCount = salahCount - 1;
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Salah_Counter, "" + salahCount);
                        }
                    }
                }

                /*
                 * Update view event in database
                 * */
                appDatabase.postDao().update("1", post_id);

                if (!TextUtils.isEmpty(postData.getKeyword())) {
                    int k_id = Integer.parseInt(postData.getKeyword());
                    if (k_id > 0) {
                        txtQuestionRelated.setText(postData.getDisplay_name() + " " + getString(R.string.str_samndhi));
                    } else {
                        List<ProblemRelatedData> problemRelatedData = SharedPreferencesUtils.getProblemRelated(this, Constants.SharedPreferences_OPTIONS);
                        if (problemRelatedData != null && problemRelatedData.size() > 0) {
                            for (int i = 0; i < problemRelatedData.size(); i++) {
                                if (postData.getProblem_related().equalsIgnoreCase(problemRelatedData.get(i).getKey())) {
                                    txtQuestionRelated.setText(problemRelatedData.get(i).getValue() + " " + getString(R.string.str_samndhi));
                                    break;
                                }
                            }
                        }
                    }
                }

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                Calendar cal = Calendar.getInstance();
                cal.setTime(sdf.parse(postData.getCreated_on()));
//                txtDate.setText(Functions.setTimeAgoMyTopic(QuestionDetailsActivity.this, cal.getTimeInMillis()));
                tvCropWeekName.setText(Functions.setTimeAgoMyTopic(CommunityDetailsActivity.this, cal.getTimeInMillis()));

                saveCommentDataOffline(false, true);

                pictures = new ArrayList<>();
                String pictureKey = postData.getPicture_key();
                if (pictureKey.contains("storage")) {
                    pictureKey = pictureKey.replace("[", "").replace("]", "");
                    pictureKey = pictureKey.replace("\"", "");
                    pictures = new ArrayList<String>(Arrays.asList(pictureKey.split(",")));
                } else {
                    JSONArray jsonArray = new JSONArray(pictureKey);
                    if (jsonArray != null && jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            pictures.add(jsonArray.opt(i).toString());
                        }
                    }
                }

                if (pictures.size() == 0) {
                    frameLayout.setVisibility(View.GONE);
                } else {
                    frameLayout.setVisibility(View.VISIBLE);
                    if (pictures.size() == 1) {
                        imgNext.setVisibility(View.GONE);
                        imgPrevios.setVisibility(View.GONE);
                    } else {
                        imgNext.setVisibility(View.VISIBLE);
                        imgPrevios.setVisibility(View.VISIBLE);
                    }
                }

                pager.setAdapter(new ImagesSavdhaniAdapter(CommunityDetailsActivity.this, pictures, imagePath, "0"));
                pager.setClipToPadding(false);
                pager.setOffscreenPageLimit(pictures.size());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void saveCommentDataOffline(boolean isInsert, boolean isDisplay) {
        try {
            if (isInsert) {
                final List<OfflineCommentData> offlineCommentData = new ArrayList<OfflineCommentData>();
                offlineCommentData.add(new OfflineCommentData(new Random().nextInt(), SharedPreferencesUtils.getUserId(CommunityDetailsActivity.this),
                        post_id, edtComment.getText().toString().trim(), "", filePath, Functions.getCurrentDateTime()));
                appDatabase.offlineCommentDao().insertAll(offlineCommentData);
            }

            if (isDisplay) {
                expertResponceModels = new ArrayList<ExpertResponceModel>();
                if (InternetConnection.checkConnectionForFasl(CommunityDetailsActivity.this)) {
                    if (!TextUtils.isEmpty(edtComment.getText().toString().trim())) {
                        expertResponceModels.add(new ExpertResponceModel(RongoApp.getUserFullName(), "0", post_id, "1", SharedPreferencesUtils.getUserId(CommunityDetailsActivity.this),
                                edtComment.getText().toString().trim(), "", "", "", "", "",
                                Functions.getCurrentDateTime(), "0", "", "", filePath, "", ""));
                    }
                }

                if (!TextUtils.isEmpty(postData.getComments())) {
                    JSONArray jsonArray = new JSONArray(postData.getComments());
                    if (jsonArray != null && jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            ExpertResponceModel expertResponceModel = new ExpertResponceModel();
                            expertResponceModel.setPosted_user_name(jsonArray.optJSONObject(i).optString("posted_user_name"));
                            expertResponceModel.setComment_id(jsonArray.optJSONObject(i).optString("comment_id"));
                            expertResponceModel.setPost_id(jsonArray.optJSONObject(i).optString("post_id"));
                            expertResponceModel.setType(jsonArray.optJSONObject(i).optString("type"));
                            expertResponceModel.setUser_id(jsonArray.optJSONObject(i).optString("user_id"));
                            expertResponceModel.setComment_text(jsonArray.optJSONObject(i).optString("comment_text"));
                            expertResponceModel.setIs_action_required(jsonArray.optJSONObject(i).optString("is_action_required"));
                            expertResponceModel.setIs_applied(jsonArray.optJSONObject(i).optString("is_applied"));
                            expertResponceModel.setNot_applied_reason(jsonArray.optJSONObject(i).optString("not_applied_reason"));
                            expertResponceModel.setIs_useful(jsonArray.optJSONObject(i).optString("is_useful"));
                            expertResponceModel.setNot_useful_reason(jsonArray.optJSONObject(i).optString("not_useful_reason"));
                            expertResponceModel.setCreated_on(jsonArray.optJSONObject(i).optString("created_on"));
                            expertResponceModel.setVote_type(jsonArray.optJSONObject(i).optString("vote_type"));
                            expertResponceModel.setLikes(jsonArray.optJSONObject(i).optString("likes"));
                            expertResponceModel.setKeyword_advisory(jsonArray.optJSONObject(i).optString("keyword_advisory"));
                            expertResponceModel.setIs_expert(jsonArray.optJSONObject(i).optString("is_expert"));
                            expertResponceModel.setRecording_local_path(jsonArray.optJSONObject(i).optString("recording_key"));
                            expertResponceModel.setRecording_key(jsonArray.optJSONObject(i).optString("recording_local_path"));
                            expertResponceModels.add(expertResponceModel);
                        }
                    }
                }

                List<OfflineCommentData> offlineCommentData = appDatabase.offlineCommentDao().getAllComment();
                if (offlineCommentData.size() > 0) {
                    for (int i = 0; i < offlineCommentData.size(); i++) {
                        ExpertResponceModel expertResponceModel = new ExpertResponceModel();
                        expertResponceModel.setPost_id(offlineCommentData.get(i).getPost_id());
                        expertResponceModel.setUser_id(SharedPreferencesUtils.getUserId(CommunityDetailsActivity.this));
                        expertResponceModel.setComment_text(offlineCommentData.get(i).getComment_text());
                        expertResponceModel.setCreated_on(offlineCommentData.get(i).getDatetime());
                        expertResponceModel.setType("1");
                        expertResponceModel.setPosted_user_name(RongoApp.getUserFullName());
                        expertResponceModel.setComment_id("");
                        expertResponceModel.setIs_action_required("");
                        expertResponceModel.setIs_applied("");
                        expertResponceModel.setNot_applied_reason("");
                        expertResponceModel.setIs_useful("");
                        expertResponceModel.setNot_useful_reason("");
                        expertResponceModel.setVote_type("0");
                        expertResponceModel.setIs_expert("");
                        expertResponceModel.setRecording_local_path(offlineCommentData.get(i).getView());
                        expertResponceModel.setRecording_key("");
                        expertResponceModels.add(expertResponceModel);
                    }
                }

                if (expertResponceModels.size() > 0) {
                    tvNoComment.setVisibility(View.GONE);
                } else {
                    tvNoComment.setVisibility(View.VISIBLE);
                }
                salahQuestionDetailsAdapter = new CommunityQuesDetailsAdapter(CommunityDetailsActivity.this, audio_Path, expertResponceModels);
                rvDetails.setAdapter(salahQuestionDetailsAdapter);
                salahQuestionDetailsAdapter.notifyDataSetChanged();
                edtComment.setText("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void updateCommunityVisibility(String YesNo) {
        RetrofitClient.getInstance().getApi().updateCommunityVisibility(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                SharedPreferencesUtils.getUserId(this), post_id, YesNo).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (YesNo.equalsIgnoreCase("1")) {
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_community_interest, "1");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

}