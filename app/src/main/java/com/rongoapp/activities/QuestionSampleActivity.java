package com.rongoapp.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.card.MaterialCardView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.makeramen.roundedimageview.RoundedImageView;
import com.rongoapp.R;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.FetchJsonFromAssetsFile;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.DailyQuizStatusData;
import com.rongoapp.data.OfflineQuizData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuestionSampleActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    ArrayList<String> answersList = new ArrayList<>();
    String isAnswer, type = "", keyword_id = "", image = "", QuizId;
    boolean isAnswerValid = false;

    RoundedImageView ivQuestion, ivPuzzle;
    ImageView ivBack, ivOne, ivTwo, ivThree, ivFour, ivFive, ivSix, ivSeven, ivEight, ivNine;
    TextView tvTitle, txtQuestion, tvOne, tvTwo, tvThree, tvFour, txtQuestion1, tvAdviceTxt;
    LinearLayout llAnswerView, llPart1, llPart2, llCropSelector, llSeeAdvisory;
    Button btnNext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_sample);

        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(this);
        answersList = new ArrayList<>();

        setInitView();
        setListner();

        tvAdviceTxt = (TextView) findViewById(R.id.tvAdviceTxt);

        int cropWeek = RongoApp.getCropWeekDaysFunction();
        if (cropWeek > 0 && cropWeek < Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"))) {
            List<DailyQuizStatusData> dailyQuizStatus = appDatabase.dailyQuizStatusDao().getDailyQuizStatus(
                    sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0"),
                    sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, "0"),
                    sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, "0"));
            if (dailyQuizStatus != null && dailyQuizStatus.size() >= 1) {
                btnNext.setAlpha(0.2f);
                btnNext.setEnabled(false);
                tvAdviceTxt.setText(R.string.str_quiz_redirect_msg);
            } else {
                btnNext.setAlpha(1f);
                btnNext.setEnabled(true);
                tvAdviceTxt.setText(R.string.str_ques_answer_is_salah_me_he);
            }
        } else {
            finish();
        }
    }

    void setInitView() {
        ivBack = (ImageView) findViewById(R.id.ivBack);
        ivQuestion = (RoundedImageView) findViewById(R.id.ivQuestion);
        ivPuzzle = (RoundedImageView) findViewById(R.id.ivPuzzle);

        ivOne = (ImageView) findViewById(R.id.ivOne);
        ivTwo = (ImageView) findViewById(R.id.ivTwo);
        ivThree = (ImageView) findViewById(R.id.ivThree);
        ivFour = (ImageView) findViewById(R.id.ivFour);
        ivFive = (ImageView) findViewById(R.id.ivFive);
        ivSix = (ImageView) findViewById(R.id.ivSix);
        ivSeven = (ImageView) findViewById(R.id.ivSeven);
        ivEight = (ImageView) findViewById(R.id.ivEight);
        ivNine = (ImageView) findViewById(R.id.ivNine);

        tvTitle = (TextView) findViewById(R.id.tvTitle);

        llAnswerView = (LinearLayout) findViewById(R.id.llAnswerView);
        llPart1 = (LinearLayout) findViewById(R.id.llPart1);
        llPart2 = (LinearLayout) findViewById(R.id.llPart2);

        llCropSelector = (LinearLayout) findViewById(R.id.llCropSelector);
        llCropSelector.setVisibility(View.GONE);

        llSeeAdvisory = (LinearLayout) findViewById(R.id.llSeeAdvisory);

        txtQuestion = (TextView) findViewById(R.id.txtQuestion);
        txtQuestion1 = (TextView) findViewById(R.id.txtQuestion1);
        tvOne = (TextView) findViewById(R.id.tvOne);
        tvTwo = (TextView) findViewById(R.id.tvTwo);
        tvThree = (TextView) findViewById(R.id.tvThree);
        tvFour = (TextView) findViewById(R.id.tvFour);

        btnNext = (Button) findViewById(R.id.btnNext);

        llCropSelector.setVisibility(View.GONE);
        llAnswerView.setVisibility(View.VISIBLE);
        llPart1.setVisibility(View.VISIBLE);
        llPart2.setVisibility(View.GONE);
        ivQuestion.setVisibility(View.VISIBLE);
        setQuizData();
    }

    void setListner() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        llSeeAdvisory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tvOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answersList = new ArrayList<>();
                answersList.add("0");
                setAnswerSelection("1");
            }
        });

        tvTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answersList = new ArrayList<>();
                answersList.add("1");
                setAnswerSelection("2");
            }
        });

        tvThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answersList = new ArrayList<>();
                answersList.add("2");
                setAnswerSelection("3");
            }
        });

        tvFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answersList = new ArrayList<>();
                answersList.add("3");
                setAnswerSelection("4");
            }
        });

        ivOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(1);
            }
        });

        ivTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(2);
            }
        });

        ivThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(3);
            }
        });

        ivFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(4);
            }
        });

        ivFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(5);
            }
        });

        ivSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(6);
            }
        });

        ivSeven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(7);
            }
        });

        ivEight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(8);
            }
        });

        ivNine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(9);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAnswerCheck(answersList);
                showDialog();
            }
        });
    }

    void setSelection(int answer) {
        if (answersList.size() > 0) {
            if (answersList.contains(String.valueOf(answer))) {
                answersList.remove(String.valueOf(answer));
            } else {
                answersList.add(String.valueOf(answer));
            }
        } else {
            answersList = new ArrayList<>();
            answersList.add(String.valueOf(answer));
        }

        ivOne.setImageResource(0);
        ivTwo.setImageResource(0);
        ivThree.setImageResource(0);
        ivFour.setImageResource(0);
        ivFive.setImageResource(0);
        ivSix.setImageResource(0);
        ivSeven.setImageResource(0);
        ivEight.setImageResource(0);
        ivNine.setImageResource(0);
        ivOne.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivTwo.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivThree.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivFour.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivFive.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivSix.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivSeven.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivEight.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivNine.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));

        for (int i = 0; i < answersList.size(); i++) {
            if (answersList.get(i).equalsIgnoreCase("1")) {
                ivOne.setImageResource(R.mipmap.q_tick);
                ivOne.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("2")) {
                ivTwo.setImageResource(R.mipmap.q_tick);
                ivTwo.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("3")) {
                ivThree.setImageResource(R.mipmap.q_tick);
                ivThree.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("4")) {
                ivFour.setImageResource(R.mipmap.q_tick);
                ivFour.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("5")) {
                ivFive.setImageResource(R.mipmap.q_tick);
                ivFive.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("6")) {
                ivSix.setImageResource(R.mipmap.q_tick);
                ivSix.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("7")) {
                ivSeven.setImageResource(R.mipmap.q_tick);
                ivSeven.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("8")) {
                ivEight.setImageResource(R.mipmap.q_tick);
                ivEight.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("9")) {
                ivNine.setImageResource(R.mipmap.q_tick);
                ivNine.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
        }
    }

    void setAnswerSelection(String pos) {
        answersList = new ArrayList<>();
        answersList.add(String.valueOf(pos));
        if (pos.equalsIgnoreCase("1")) {
//            tvOne.setBackground(getResources().getDrawable(R.drawable.rounded_box_orange_border));
            tvOne.setBackground(getResources().getDrawable(R.drawable.rounded_box_select));
            tvTwo.setBackground(getResources().getDrawable(R.drawable.rounded_box));
            tvThree.setBackground(getResources().getDrawable(R.drawable.rounded_box));
            tvFour.setBackground(getResources().getDrawable(R.drawable.rounded_box));
        } else if (pos.equalsIgnoreCase("2")) {
            tvOne.setBackground(getResources().getDrawable(R.drawable.rounded_box));
            tvTwo.setBackground(getResources().getDrawable(R.drawable.rounded_box_select));
            tvThree.setBackground(getResources().getDrawable(R.drawable.rounded_box));
            tvFour.setBackground(getResources().getDrawable(R.drawable.rounded_box));
        } else if (pos.equalsIgnoreCase("3")) {
            tvOne.setBackground(getResources().getDrawable(R.drawable.rounded_box));
            tvTwo.setBackground(getResources().getDrawable(R.drawable.rounded_box));
            tvThree.setBackground(getResources().getDrawable(R.drawable.rounded_box_select));
            tvFour.setBackground(getResources().getDrawable(R.drawable.rounded_box));
        } else if (pos.equalsIgnoreCase("4")) {
            tvOne.setBackground(getResources().getDrawable(R.drawable.rounded_box));
            tvTwo.setBackground(getResources().getDrawable(R.drawable.rounded_box));
            tvThree.setBackground(getResources().getDrawable(R.drawable.rounded_box));
            tvFour.setBackground(getResources().getDrawable(R.drawable.rounded_box_select));
        }
    }

    boolean isAnswerCheck(ArrayList<String> answer) {
        isAnswerValid = false;
        String[] allIdsArray = TextUtils.split(isAnswer, ",");
        ArrayList<String> idsList = new ArrayList<String>(Arrays.asList(allIdsArray));
        for (String next : idsList) {
            Log.e("Result", next + " - " + answer);
            if (answer.contains(next)) {
                isAnswerValid = true;
                break;
            }
        }
        return isAnswerValid;
    }


    void setQuizData() {
        try {
            JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.getDailyQuizDataJSONFromAsset(QuestionSampleActivity.this));
            if (jsonObject != null && jsonObject.length() > 0) {
                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                    JSONObject data = jsonObject.optJSONObject("data");
                    if (data != null && data.length() > 0) {
                        JSONObject jsonObj = data.optJSONObject("result");
                        if (jsonObj != null && jsonObj.length() > 0) {
                            QuizId = jsonObj.getString("id");
                            JSONObject json_data = jsonObj.getJSONObject("json_data");
//                            Log.e("quiz==", "" + json_data);
                            if (json_data != null && json_data.length() > 0) {
                                Iterator<String> keys = json_data.keys();
                                while (keys.hasNext()) {
                                    String key = keys.next();
                                    String value = json_data.optString(key);
                                    if (key.equalsIgnoreCase(RongoApp.getSelectedCrop())) {
                                        JSONObject jsonLang = new JSONObject(value);
                                        if (jsonLang != null && jsonLang.length() > 0) {
                                            Iterator<String> keyLang = jsonLang.keys();
                                            while (keyLang.hasNext()) {
                                                String key1 = keyLang.next();
                                                String value1 = jsonLang.optString(key1);
                                                if (key1.equalsIgnoreCase(RongoApp.getSeasonName())) {
                                                    JSONObject jsonLang1 = new JSONObject(value1);
                                                    if (jsonLang1 != null && jsonLang1.length() > 0) {
                                                        Iterator<String> keyLang1 = jsonLang1.keys();
                                                        while (keyLang1.hasNext()) {
                                                            String key2 = keyLang1.next();
                                                            String value2 = jsonLang1.optString(key2);
                                                            if (key2.equalsIgnoreCase(RongoApp.getDefaultLanguage())) {
                                                                JSONArray jsonArray = new JSONArray(value2);
                                                                JSONObject resultData = jsonArray.getJSONObject(Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "2")) - 1);
                                                                JSONObject jsonObjects = resultData.getJSONObject(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, "0"));
//                                                                JSONObject resultData = jsonArray.getJSONObject(4);
//                                                                JSONObject jsonObjects = resultData.getJSONObject("4");

                                                                type = jsonObjects.optString("type");
                                                                keyword_id = jsonObjects.optString("keyword_id");
                                                                isAnswer = jsonObjects.optString("answer");
                                                                String question = jsonObjects.optString("question");
                                                                txtQuestion.setText("" + question);
                                                                txtQuestion1.setText("" + question);
                                                                image = jsonObjects.optString("image");

                                                                if (!type.equalsIgnoreCase("puzzle")) {
                                                                    llCropSelector.setVisibility(View.GONE);
                                                                    llAnswerView.setVisibility(View.VISIBLE);
                                                                    ivQuestion.setVisibility(View.VISIBLE);
                                                                    txtQuestion.setVisibility(View.VISIBLE);

                                                                    JSONArray array = jsonObjects.optJSONArray("options");
                                                                    if (array != null && array.length() > 0) {
                                                                        List<String> list = new Gson().fromJson(array.toString(), new TypeToken<List<String>>() {
                                                                        }.getType());
                                                                        llPart2.setVisibility(View.GONE);
                                                                        tvOne.setText(" (A)  " + list.get(0));
                                                                        tvTwo.setText(" (B)  " + list.get(1));
                                                                        if (list.size() > 2) {
                                                                            if (!TextUtils.isEmpty(list.get(2))) {
                                                                                llPart2.setVisibility(View.VISIBLE);
                                                                                tvThree.setText(" (C)  " + list.get(2));
                                                                                tvFour.setText(" (D)  " + list.get(3));
                                                                            }
                                                                        }
                                                                    }
                                                                } else {
                                                                    llCropSelector.setVisibility(View.VISIBLE);
                                                                    llAnswerView.setVisibility(View.GONE);
                                                                    ivQuestion.setVisibility(View.GONE);
                                                                    txtQuestion.setVisibility(View.GONE);
                                                                }

                                                                if (!TextUtils.isEmpty(type)) {
                                                                    if (!type.equalsIgnoreCase("puzzle")) {
                                                                        ivQuestion.setVisibility(View.VISIBLE);
                                                                        ivPuzzle.setVisibility(View.GONE);
                                                                    } else {
                                                                        ivPuzzle.setVisibility(View.VISIBLE);
                                                                        ivQuestion.setVisibility(View.GONE);
                                                                    }

                                                                    if (!TextUtils.isEmpty(keyword_id)) {
                                                                        ivQuestion.setImageDrawable(Functions.getAssetsCropImage(QuestionSampleActivity.this,
                                                                                Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + image));

                                                                        ivPuzzle.setImageDrawable(Functions.getAssetsCropImage(QuestionSampleActivity.this,
                                                                                Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + image));
                                                                    } else {
                                                                        ivQuestion.setImageDrawable(Functions.getAssetsCropImage(QuestionSampleActivity.this,
                                                                                Constants.ASSETS_ADVISORY_CROP_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + image));

                                                                        ivPuzzle.setImageDrawable(Functions.getAssetsCropImage(QuestionSampleActivity.this,
                                                                                Constants.ASSETS_ADVISORY_CROP_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + image));
                                                                    }
                                                                } else {
                                                                    ivQuestion.setVisibility(View.GONE);
                                                                    ivPuzzle.setVisibility(View.GONE);
                                                                    llCropSelector.setVisibility(View.GONE);
                                                                }
                                                                Log.e("isAnswer==", "" + isAnswer);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void showDialog() {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        View sheetView = this.getLayoutInflater().inflate(R.layout.bottom_quiz_thanks_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        AppCompatTextView txtOfflineSMS = sheetView.findViewById(R.id.txtOfflineSMS);
        txtOfflineSMS.setVisibility(View.GONE);
        if (InternetConnection.checkConnectionForFasl(QuestionSampleActivity.this)) {
            txtOfflineSMS.setVisibility(View.GONE);
        } else {
            txtOfflineSMS.setVisibility(View.VISIBLE);
        }

        ImageView ivTagImage = (ImageView) sheetView.findViewById(R.id.ivTagImage);
        TextView tvTagText = (TextView) sheetView.findViewById(R.id.tvTagText);
        TextView tvTagDecs = (TextView) sheetView.findViewById(R.id.tvTagDecs);

        LinearLayout llRewardView = (LinearLayout) sheetView.findViewById(R.id.llRewardView);

        MaterialCardView cardKeyword = (MaterialCardView) sheetView.findViewById(R.id.cardKeyword);
        cardKeyword.setVisibility(View.GONE);
        RelativeLayout rlShareView = (RelativeLayout) sheetView.findViewById(R.id.rlShareView);
        rlShareView.setVisibility(View.GONE);

        TextView tvRewardNew = (TextView) sheetView.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));

        RoundedImageView ivKeyword = (RoundedImageView) sheetView.findViewById(R.id.ivKeyword);
        TextView tvKeywordText = (TextView) sheetView.findViewById(R.id.tvKeywordText);

        if (!TextUtils.isEmpty(keyword_id)) {
            rlShareView.setVisibility(View.GONE);
            cardKeyword.setVisibility(View.VISIBLE);

            ivKeyword.setImageDrawable(Functions.getAssetsCropImage(QuestionSampleActivity.this,
                    Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + image));
            try {
                JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.
                        loadAdvisoryKeywordJSONFromAsset(QuestionSampleActivity.this, keyword_id));
                if (jsonObject != null && jsonObject.length() > 0) {
                    String name = jsonObject.optString("name");

                    Spannable wordtoSpan = new SpannableString(name + getString(R.string.str_more_info_text));
                    wordtoSpan.setSpan(new ForegroundColorSpan(Color.rgb(255, 69, 0)), 0, name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tvKeywordText.setText(wordtoSpan);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlShareView.setVisibility(View.VISIBLE);
            cardKeyword.setVisibility(View.GONE);
        }

        cardKeyword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(QuestionSampleActivity.this, SavdhaniDetailsActivity.class);
                i.putExtra("keyword_id", keyword_id);
                startActivity(i);
                finish();
            }
        });

        if (isAnswerValid) {
            llRewardView.setVisibility(View.VISIBLE);
            ivTagImage.setImageResource(R.mipmap.quiz_yes);
            tvTagText.setText(R.string.str_aapka_utar_sahi_he);
            tvTagText.setTextColor(getResources().getColor(R.color.chat_text));
            tvTagDecs.setVisibility(View.GONE);
            tvTagDecs.setText(R.string.str_aaj_10_reward_jite_he);

            updateQuizResult("1");
        } else {
            llRewardView.setVisibility(View.GONE);
            ivTagImage.setImageResource(R.mipmap.quiz_no);
            tvTagText.setText(R.string.str_utar_galt_he);
            tvTagText.setTextColor(getResources().getColor(R.color.bunai_yes));
            tvTagDecs.setVisibility(View.VISIBLE);
            tvTagDecs.setText(R.string.str_jankari_ke_liye_salah_pade);

            updateQuizResult("0");
        }

        AppCompatTextView tvShareWhatapp = sheetView.findViewById(R.id.tvShareWhatapp);
        tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.shareWhatAppIntent(QuestionSampleActivity.this);
            }
        });

        AppCompatTextView tvShareCopy = sheetView.findViewById(R.id.tvShareCopy);
        tvShareCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.shareSMSIntent(QuestionSampleActivity.this, RongoApp.getReferralText(QuestionSampleActivity.this));
            }
        });

        AppCompatTextView tvSharefacebook = sheetView.findViewById(R.id.tvSharefacebook);
        tvSharefacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.shareFacebookIntent(QuestionSampleActivity.this);
            }
        });

        AppCompatTextView tvShareMore = sheetView.findViewById(R.id.tvShareMore);
        tvShareMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.performShare(QuestionSampleActivity.this, appDatabase);
            }
        });

        ImageView ivClose = (ImageView) sheetView.findViewById(R.id.ivClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
                finish();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        ((View) sheetView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();
    }


    void updateQuizResult(final String status) {
        try {
            Functions.setFirebaseLogEventTrack(QuestionSampleActivity.this, Constants.Click_Event, "Daily Quiz", status, "updateQuizResult");

            String answer = answersList.toString().replace("[", "").replace("]", "");

            if (InternetConnection.checkConnectionForFasl(QuestionSampleActivity.this)) {
                RetrofitClient.getInstance().getApi().updateDailyQuizResponse(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                        RongoApp.getAppVersion(), SharedPreferencesUtils.getUserId(QuestionSampleActivity.this),
                        Functions.getDeviceid(QuestionSampleActivity.this), QuizId, RongoApp.getSelectedCrop(),
                        RongoApp.getSelectedPCrop(), sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0"),
                        sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, "0"),
                        answer, status, getDailyQuizRewardPoint(status)).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response != null && response.isSuccessful()) {
                            setDailyQuizStatus(status);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
            } else {
                setDailyQuizStatus(status);

                ArrayList<OfflineQuizData> offlineQuizData = new ArrayList<OfflineQuizData>();
                OfflineQuizData offlineQuiz = new OfflineQuizData();
                offlineQuiz.set_id(0);
                offlineQuiz.setQuizID(QuizId);
                offlineQuiz.setCropId(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""));
                offlineQuiz.setPcropId(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""));
                offlineQuiz.setCropWeek(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""));
                offlineQuiz.setCropDay(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, ""));
                offlineQuiz.setAnsValue(answer);
                offlineQuiz.setIs_true(status);
                offlineQuiz.setMetaData(getDailyQuizRewardPoint(status));
                offlineQuizData.add(offlineQuiz);
                appDatabase.offlineQuizDao().insertAll(offlineQuizData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setDailyQuizStatus(String status) {
        try {
            String answer = answersList.toString().replace("[", "").replace("]", "");
            ArrayList<DailyQuizStatusData> dailyQuizStatusData = new ArrayList<DailyQuizStatusData>();
            DailyQuizStatusData dailyQuizStatus = new DailyQuizStatusData();
            dailyQuizStatus.set_id(0);
            dailyQuizStatus.setQuizID(QuizId);
            dailyQuizStatus.setCropId(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""));
            dailyQuizStatus.setPcropId(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""));
            dailyQuizStatus.setCropWeek(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""));
            dailyQuizStatus.setCropDay(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, ""));
            dailyQuizStatus.setAnsValue(answer);
            dailyQuizStatus.setMetaData(getDailyQuizRewardPoint(status));
            dailyQuizStatusData.add(dailyQuizStatus);
            appDatabase.dailyQuizStatusDao().insertAll(dailyQuizStatusData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String getDailyQuizRewardPoint(String status) {
        String rewardPoint = "";
        try {
            if (status.equalsIgnoreCase("1")) {
                JSONArray jsonArray = new JSONArray();
                JSONObject obj = new JSONObject();
                obj.put("type", "point");
                obj.put("value", RewardPoints.getDailyQuizRewardPoint(sharedPreferences));
                jsonArray.put(obj);
                rewardPoint = jsonArray.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rewardPoint;
    }

}