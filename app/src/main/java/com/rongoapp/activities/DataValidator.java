package com.rongoapp.activities;

import android.app.Activity;
import android.text.TextUtils;

import com.rongoapp.controller.FetchJsonFromAssetsFile;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.views.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataValidator {

    public static void parseCropDataFormat(Activity activity) {
        try {
            RetrofitClient.getInstance().getApi().getMasterCropData(RongoApp.getSeasonName(),
                    RongoApp.getDefaultLanguage(), RongoApp.getAppVersion()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        try {
                            String r_str = response.body().string();
                            if (!TextUtils.isEmpty(r_str)) {
                                JSONObject jb = new JSONObject(r_str);
                                if (jb != null && jb.length() > 0) {
                                    if (jb.optString("status_code").equalsIgnoreCase("0")) {
                                        JSONObject jsonObject = jb.optJSONObject("data");
                                        if (jsonObject != null && jsonObject.length() > 0) {
                                            JSONArray crop_matrix_array = jsonObject.optJSONArray("crop_matrix");
                                            Log.e("crop_matrix_array: ", "" + crop_matrix_array);

                                            for (int i = 0; i < crop_matrix_array.length(); i++) {
                                                JSONObject matrix = crop_matrix_array.optJSONObject(i);
//                                                Log.e("matrix: ", "" + matrix);
                                                String cropId = matrix.getString("crop_id");
//                                                Log.e("cropId: ", "" + cropId);
                                                JSONArray season = matrix.getJSONArray("season");
                                                String seasonName = season.toString().replace("[", "").replace("]", "").replace(", ", ",");
                                                String[] arrayStr = seasonName.split(",");
                                                for (String s : arrayStr) {
                                                    String seasonId = s.replace("\"", "");
//                                                    Log.e("sessionname:", "" + s);
                                                    JSONArray language = matrix.getJSONArray("language");
//                                                    Log.e("language: ", "" + language);
                                                    String languageName = language.toString().replace("[", "").replace("]", "").replace(", ", ",");
                                                    String[] arrayStr1 = languageName.split(",");
                                                    for (String lan : arrayStr1) {
                                                        String languageId = lan.replace("\"", "");
//                                                        Log.e("language:", "" + lan);

                                                        Log.e("DailyQuiz-" + cropId + " :-", "" + checkDailyQuiz(activity, cropId, seasonId, languageId));
                                                        Log.e("WeeklyCropQuestion-" + cropId + " :-", "" + checkWeeklyCropQuestion(activity, cropId, seasonId, languageId));
                                                        Log.e("checkProfileData-" + cropId + " :-", "" + checkProfileData(activity, cropId, seasonId, languageId));
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static boolean checkDailyQuiz(Activity activity, String cropId, String season, String language) {
        boolean isDailyQuiz = false;
        try {
            Log.e("checkDailyQuiz+", "" + cropId + "-" + season + "-" + language);
            JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.getDailyQuizDataJSONFromAsset(activity));
            if (jsonObject != null && jsonObject.length() > 0) {
                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                    JSONObject data = jsonObject.optJSONObject("data");
                    if (data != null && data.length() > 0) {
                        JSONObject result = data.optJSONObject("result");
                        if (result != null && result.length() > 0) {
                            JSONObject QuizData = new JSONObject("" + result);
                            JSONObject json_data = QuizData.getJSONObject("json_data");
                            if (json_data != null && json_data.length() > 0) {
                                Iterator<String> keys = json_data.keys();
                                while (keys.hasNext()) {
                                    String key = keys.next();
                                    String value = json_data.optString(key);
//                                    Log.e("Jsonparsing+", "key= " + key + "\n Value=" + value);
                                    if (key.equalsIgnoreCase(cropId)) {
                                        JSONObject jsonLang = new JSONObject(value);
                                        if (jsonLang != null && jsonLang.length() > 0) {
                                            Iterator<String> keyLang = jsonLang.keys();
                                            while (keyLang.hasNext()) {
                                                String key1 = keyLang.next();
                                                String value1 = jsonLang.optString(key1);
//                                                Log.e("Jsonparsing++", "key= " + key1 + "\n Value=" + value1);
                                                if (key1.equalsIgnoreCase(season)) {
                                                    JSONObject jsonLang1 = new JSONObject(value1);
                                                    if (jsonLang1 != null && jsonLang1.length() > 0) {
                                                        Iterator<String> keyLang1 = jsonLang1.keys();
                                                        while (keyLang1.hasNext()) {
                                                            String key2 = keyLang1.next();
                                                            String value2 = jsonLang1.optString(key2);
//                                                            Log.e("Jsonparsing+++", "key= " + key2 + "\n Value=" + value2);
                                                            if (key2.equalsIgnoreCase(language)) {
                                                                JSONArray jsonArray = new JSONArray(value2);
//                                                                Log.e("parseQuizData", "" + jsonArray);
                                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                                    JSONObject resultData = jsonArray.getJSONObject(i);
//                                                                    Log.e("resultData", i + " - ");

                                                                    JSONObject jbq = resultData.getJSONObject("1");
                                                                    if (TextUtils.isEmpty(jbq.getString("answer"))) {
                                                                        isDailyQuiz = false;
                                                                    }
                                                                    if (TextUtils.isEmpty(jbq.getString("question"))) {
                                                                        isDailyQuiz = false;
                                                                    }
                                                                    JSONArray array = jbq.optJSONArray("options");
                                                                    if (TextUtils.isEmpty(array.toString())) {
                                                                        isDailyQuiz = false;
                                                                    }

                                                                    isDailyQuiz = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isDailyQuiz;
    }

    public static boolean checkWeeklyCropQuestion(Activity activity, String cropId, String season, String language) {
        boolean isWeeklyCropQuestion = false;
        try {
            Log.e("checkWeeklyCropQuestion+", "" + cropId + "-" + season + "-" + language);
            JSONObject jb = new JSONObject(FetchJsonFromAssetsFile.getAllCropWeeklyQuestionJSONFromAsset(activity));
            if (jb != null && jb.length() > 0) {
                if (jb.optString("status_code").equalsIgnoreCase("0")) {
                    JSONObject data = jb.optJSONObject("data");
                    if (data != null && data.length() > 0) {
                        JSONObject resultData = data.optJSONObject("result");
                        if (!TextUtils.isEmpty(resultData.toString())) {
                            JSONObject quesCrop = resultData.optJSONObject(cropId);
                            JSONObject quesCrop1 = quesCrop.optJSONObject(season);
                            JSONArray jsonObject = quesCrop1.optJSONArray(language);
                            JSONObject object = jsonObject.optJSONObject(0);
                            JSONArray cropWeek = object.optJSONArray("crop_week");
                            JSONArray jd = cropWeek.optJSONArray(0);
                            String weekCropDays = jd.toString().replace("[", "").replace("]", "").replace(", ", ",");
                            if (!TextUtils.isEmpty(weekCropDays)) {
                                String[] arrayStr = weekCropDays.split(",");
                                for (String s : arrayStr) {
                                    String weekCropDay = s.replace("\"", "");
                                    JSONObject questions = object.optJSONObject("questions");
                                    JSONObject quesCropWeek = Objects.requireNonNull(questions).optJSONObject(weekCropDay);
                                    if (!TextUtils.isEmpty(quesCropWeek.toString())) {
//                                        Log.v("+++@@444+++: ", weekCropDay + " - " + quesCropWeek);
                                        isWeeklyCropQuestion = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isWeeklyCropQuestion;
    }

    public static boolean checkProfileData(Activity activity, String cropId, String season, String language) {
        boolean isProfile = false;
        try {
            Log.e("checkProfileData+", "" + cropId + "-" + season + "-" + language);
            JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.getProfileDataJSONFromAsset(activity));
            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                JSONObject jData = jsonObject.optJSONObject("data");
                if (jData != null && jData.length() > 0) {
                    JSONObject jresult = jData.optJSONObject("result");
                    if (jresult != null && jresult.length() > 0) {
                        JSONObject jseason = jresult.optJSONObject("season");
                        if (jseason != null && jseason.length() > 0) {
                            JSONArray jcrops = jseason.optJSONArray("crops");
                            if (jcrops != null && jcrops.length() > 0) {
                                for (int i = 0; i < jcrops.length(); i++) {
                                    JSONObject jbdata = jcrops.optJSONObject(i);
                                    if (cropId.equalsIgnoreCase(jbdata.optString("crop_id"))) {
                                        if (language.equalsIgnoreCase(jbdata.optString("app_language"))) {
                                            if (season.equalsIgnoreCase(jbdata.optString("season_name"))) {
                                                JSONArray crop_week_detail = jbdata.optJSONArray("crop_week_detail");
                                                if (crop_week_detail != null && crop_week_detail.length() > 0) {
                                                    for (int k = 0; k < crop_week_detail.length(); k++) {
                                                        JSONObject crop_week = crop_week_detail.optJSONObject(i);
                                                        if (!TextUtils.isEmpty(crop_week.optString("stage_id"))) {
                                                            if (!TextUtils.isEmpty(crop_week.optString("stage_name"))) {
                                                                if (!TextUtils.isEmpty(crop_week.optString("display_name"))) {
                                                                    isProfile = true;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isProfile;
    }


    public static boolean checkKeywordData(Activity activity, String keyword_id) {
        boolean isKeyword = false;
        try {
            JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.loadAdvisoryKeywordJSONFromAsset(activity, keyword_id));
            if (jsonObject != null && jsonObject.length() > 0) {
                String name = jsonObject.optString("name");
                String image_path = jsonObject.optString("image_path");

                JSONArray chemical_info = jsonObject.optJSONArray("chemical_info");
                if (chemical_info != null && chemical_info.length() > 0) {

                }
                JSONArray laxan = jsonObject.optJSONArray("symptoms");
                if (laxan != null && laxan.length() > 0) {

                }

                JSONArray management = jsonObject.optJSONArray("management");
                if (laxan != null && management.length() > 0) {

                }

                JSONArray jsonArray = jsonObject.optJSONArray("advisory");
                if (jsonArray != null && jsonArray.length() > 0) {

                }

                JSONObject images = jsonObject.optJSONObject("images");
                if (images != null && images.length() > 0) {
                    JSONObject stage_1 = images.optJSONObject("stage_1");
                    if (stage_1 != null && stage_1.length() > 0) {
                        JSONArray image = stage_1.optJSONArray("image");
                        if (image != null && image.length() > 0) {

                        }
                    }
                }

                isKeyword = true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return isKeyword;
    }

    public static boolean checkAllCropsData(Activity activity, String cropId, String season, String language) {
        boolean isAllCrops = false;
        try {
            Log.e("checkAllCropsData+", "" + cropId + "-" + season + "-" + language);
            JSONArray allCropsData = new JSONArray(FetchJsonFromAssetsFile.loadAllCropsJSONFromAsset(activity, cropId, season, language));
            if (allCropsData != null && allCropsData.length() > 0) {
                for (int i = 0; i < allCropsData.length(); i++) {
                    JSONObject resultData = allCropsData.getJSONObject(i);
                    JSONObject result = resultData.optJSONObject("result");
                    if (result != null && result.length() > 0) {
                        JSONObject keyword_advisory = result.optJSONObject("keyword_advisory");
                        if (keyword_advisory != null && keyword_advisory.length() > 0) {

                        }

                        JSONObject weekly_advisory = result.optJSONObject("weekly_advisory");
                        if (weekly_advisory != null && weekly_advisory.length() > 0) {
                            String stage_name = weekly_advisory.optString("stage");
                            String image_path = weekly_advisory.optString("image_path");

                            JSONArray jsonArray = weekly_advisory.optJSONArray("general_advice");
                            if (jsonArray != null && jsonArray.length() > 0) {

                            }

                            JSONArray advisory_image = weekly_advisory.optJSONArray("advisory_image");
                            if (advisory_image != null && advisory_image.length() > 0) {

                            }

                            JSONArray chemical_info = weekly_advisory.optJSONArray("chemical_info");
                            if (chemical_info != null && chemical_info.length() > 0) {

                            }

                            JSONArray chemical = weekly_advisory.optJSONArray("chemical");
                            if (chemical != null && chemical.length() > 0) {

                            }

                            JSONArray disease = weekly_advisory.optJSONArray("disease");
                            if (disease != null && disease.length() > 0) {

                            }

                            JSONArray pest = weekly_advisory.optJSONArray("pest");
                            if (pest != null && pest.length() > 0) {

                            }

                            JSONArray nutrition = weekly_advisory.optJSONArray("nutrition");
                            if (nutrition != null && nutrition.length() > 0) {

                            }
                        }

                        isAllCrops = true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isAllCrops;
    }

}
