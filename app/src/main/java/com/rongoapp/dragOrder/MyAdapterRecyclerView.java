package com.rongoapp.dragOrder;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.rongoapp.R;
import com.rongoapp.controller.GlideApp;

import java.util.List;

public class MyAdapterRecyclerView extends RecyclerView.Adapter<MyAdapterRecyclerView.MyViewHolder> {

    List<Drawable> mList;

    public MyAdapterRecyclerView(List<Drawable> mList) {
        this.mList = mList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reoder_item_list, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
//        GlideApp.with(holder.itemView.getContext()).load(mList.get(position))
//                .placeholder(R.drawable.placeholderbg).into(holder.ivUser);
        holder.ivUser.setImageDrawable(mList.get(position));
    }

    @Override
    public int getItemCount() {
        if (mList == null) {
            return 0;
        } else {
            return mList.size();
        }
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        RoundedImageView ivUser;

        public MyViewHolder(View itemView) {
            super(itemView);
            ivUser = (RoundedImageView) itemView.findViewById(R.id.ivUser);
        }
    }

}
