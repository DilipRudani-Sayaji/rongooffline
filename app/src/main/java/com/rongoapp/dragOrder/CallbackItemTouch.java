package com.rongoapp.dragOrder;

public interface CallbackItemTouch {
    void itemTouchOnMove(int oldPosition, int newPosition);
}
