package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.AddCropStatusData;

import java.util.List;

@Dao
public interface AddCropStatusDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(AddCropStatusData addCropStatusData);

    @Query("Select * from  AddCropStatusData where  status = :status")
    public List<AddCropStatusData> getAllDataByStatus(String status);

    @Query("Select * from  AddCropStatusData")
    public List<AddCropStatusData> getAllData();

    @Query("Select * from  AddCropStatusData where  cropId = :cropId")
    public List<AddCropStatusData> getAllData(String cropId);

    @Query("DELETE FROM AddCropStatusData")
    public void nukeTable();

    @Query("DELETE FROM AddCropStatusData where cropId=:cropId")
    public void deleteByCropId(String cropId);

    @Query("DELETE FROM AddCropStatusData where cropId=:cropId AND pcropIds = :pCropId")
    public void deleteByCropData(String cropId, String pCropId);
}
