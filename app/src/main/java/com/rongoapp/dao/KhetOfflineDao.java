package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.KhetOfflineData;

import java.util.List;

@Dao
public interface KhetOfflineDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(KhetOfflineData... khetOfflineData);

    @Query("SELECT * FROM KhetOfflineData")
    List<KhetOfflineData> getAll();

    @Query("DELETE FROM KhetOfflineData")
    void nukeTable();

    @Delete
    void delete(KhetOfflineData khetOfflineData);

    @Query("DELETE FROM KhetOfflineData WHERE uid = :id")
    void delete(int id);

}