package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.WeeklyAdvisoryData;

import java.util.List;

@Dao
public interface WeeklyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<WeeklyAdvisoryData> weeklyAdvisoryData);

    @Query("Select * from  WeeklyAdvisoryData where cropWeek = :cropWeek AND cropId = :cropId")
    public WeeklyAdvisoryData getWeeklyAdvisoryByCropWeek(int cropWeek, String cropId);

    @Query("Select * from  WeeklyAdvisoryData")
    public WeeklyAdvisoryData getWeeklyAdvisoryByCropWeek();

    @Query("DELETE FROM WeeklyAdvisoryData")
    public void nukeTable();

}
