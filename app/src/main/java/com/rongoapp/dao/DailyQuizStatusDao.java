package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.DailyQuizStatusData;

import java.util.List;

@Dao
public interface DailyQuizStatusDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<DailyQuizStatusData> dailyQuizStatusData);

    @Query("Select * from  DailyQuizStatusData")
    public List<DailyQuizStatusData> getAllQuizData();

    @Query("Select * from  DailyQuizStatusData WHERE cropWeek =:cropWeek AND cropDay = :cropDay AND pcropId = :pCropID")
    public List<DailyQuizStatusData> getDailyQuizStatus(String cropWeek, String cropDay, String pCropID);

    @Query("Select * from  DailyQuizStatusData WHERE cropWeek =:cropWeek AND cropDay = :cropDay")
    public List<DailyQuizStatusData> getDailyQuizStatus(String cropWeek, String cropDay);

    @Query("DELETE FROM DailyQuizStatusData")
    public void nukeTable();

}
