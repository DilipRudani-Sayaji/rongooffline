package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.SurveyQuestions;

import java.util.List;

@Dao
public interface SurveyQuestionsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<SurveyQuestions> incomeData);

    @Query("Select * from  SurveyQuestions where keyValue=:key")
    public List<SurveyQuestions> getAllQuestions(String key);

    @Query("Select * from  SurveyQuestions order by keyValue")
    public List<SurveyQuestions> getAllQuestions();

    @Query("Select * from  SurveyQuestions where keyValue>:key order by keyValue limit 0,1")
    public List<SurveyQuestions> getNextQuestion(int key);

    @Query("DELETE FROM SurveyQuestions")
    public void nukeTable();

}
