package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.ProfileOfflineData;

import java.util.List;

@Dao
public interface ProfileOfflineDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(ProfileOfflineData... users);

    @Query("SELECT * FROM ProfileOfflineData")
    List<ProfileOfflineData> getAll();

    @Query("Select * from  ProfileOfflineData where crop_id=:crop_id AND p_crop_id = :p_crop_id")
    public List<ProfileOfflineData> getOfflineProfile(String crop_id, String p_crop_id);

    @Query("Select * from  ProfileOfflineData where crop_id=:crop_id")
    public List<ProfileOfflineData> getOfflineProfileCropId(String crop_id);

    @Query("UPDATE ProfileOfflineData SET sowing_date=:sowingDate, expected_sowing_date=:expectedSowingDate, is_sowing_done=:isDone WHERE crop_id = :crop_id")
    void update(String sowingDate, String expectedSowingDate, String isDone, String crop_id);

    @Query("UPDATE ProfileOfflineData SET sowing_date=:sowingDate, expected_sowing_date=:expectedSowingDate, is_sowing_done=:isDone, city=:city, state=:state WHERE crop_id = :crop_id")
    void update(String sowingDate, String expectedSowingDate, String isDone, String city, String state, String crop_id);

    @Query("DELETE FROM ProfileOfflineData")
    void nukeTable();

    @Query("DELETE FROM ProfileOfflineData WHERE uid = :id")
    void delete(int id);

}