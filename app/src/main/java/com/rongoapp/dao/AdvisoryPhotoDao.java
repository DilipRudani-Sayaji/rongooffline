package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.AdvisoryPhotoData;

import java.util.List;

@Dao
public interface AdvisoryPhotoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<AdvisoryPhotoData> advisoryPhotoData);

    @Query("Select * from  AdvisoryPhotoData where cropWeek=:cropWeek AND pcropId = :pCropId")
    public List<AdvisoryPhotoData> getAllData(String cropWeek, String pCropId);

    @Query("DELETE FROM AdvisoryPhotoData")
    public void nukeTable();

}
