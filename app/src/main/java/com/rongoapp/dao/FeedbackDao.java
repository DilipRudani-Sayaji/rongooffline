package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.FeedbackData;

import java.util.List;

@Dao
public interface FeedbackDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<FeedbackData> incomeData);

    @Query("Select * from  FeedbackData")
    public List<FeedbackData> getAllData();

    @Query("DELETE FROM FeedbackData")
    public void nukeTable();
}
