package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.OfflineLikeData;

import java.util.List;

@Dao
public interface OfflineLikeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<OfflineLikeData> offlineLikeData);

    @Query("Select * from  OfflineLikeData")
    public List<OfflineLikeData> getAllLike();

    @Query("Select * from  OfflineLikeData where post_id=:post_id")
    public List<OfflineLikeData> getLikeByPostId(String post_id);

    @Query("DELETE FROM OfflineLikeData")
    public void nukeTable();

}
