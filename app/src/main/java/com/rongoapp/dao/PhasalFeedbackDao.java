package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.PhsalFeedback;
import com.rongoapp.data.PostData;

import java.util.List;

@Dao
public interface PhasalFeedbackDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(PhsalFeedback phsalFeedback);

    @Query("Select * from  PhsalFeedback where cropWeek = :cropWeek AND givenFeedback = :givenFeedback")
    public PhsalFeedback getFeedback(int cropWeek, String givenFeedback);

//    @Query("DELETE FROM PhsalFeedback where cropWeek = :cropWeek")
//    public void deleteByUserId(int cropWeek);

    @Query("DELETE FROM PhsalFeedback")
    public void nukeTable();

}
