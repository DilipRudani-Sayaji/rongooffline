package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.UserInvite;

import java.util.List;

@Dao
public interface UserInviteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    @Insert
    void insertAll(UserInvite... users);

    @Query("SELECT * FROM UserInvite")
    List<UserInvite> getAll();

    @Query("SELECT * FROM UserInvite WHERE uid IN (:userIds)")
    List<UserInvite> loadAllByIds(int[] userIds);

//    @Query("SELECT * FROM user WHERE first_name LIKE :first AND " + "last_name LIKE :last LIMIT 1")
//    User findByName(String first, String last);

    @Delete
    void delete(UserInvite user);

    @Query("DELETE FROM UserInvite WHERE uid = :id")
    void delete(int id);

}