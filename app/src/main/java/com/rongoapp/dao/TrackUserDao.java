package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.TrackUserData;

import java.util.List;

@Dao
public interface TrackUserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(TrackUserData... trackUserData);

    @Query("SELECT * FROM TrackUserData ORDER BY date_time ASC")
    List<TrackUserData> getAll();

    @Query("DELETE FROM TrackUserData")
    public void nukeTable();

    @Query("DELETE FROM TrackUserData WHERE uid = :id")
    void delete(int id);

}