package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.LuckyDrawData;

import java.util.List;

@Dao
public interface LuckyDrawOfflineDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<LuckyDrawData> incomeData);

    @Query("Select * from  LuckyDrawData")
    public List<LuckyDrawData> getAllData();

    @Query("Select * from  LuckyDrawData where crop_week=:cropWeek AND pcropId = :pCropId")
    public List<LuckyDrawData> getAllData(String cropWeek, String pCropId);

    @Query("DELETE FROM LuckyDrawData")
    public void nukeTable();
}
