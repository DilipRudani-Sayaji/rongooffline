package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.OfflineQuizData;
import com.rongoapp.data.OfflineRewardData;

import java.util.List;

@Dao
public interface OfflineQuizDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<OfflineQuizData> offlineRewardData);

    @Query("Select * from  OfflineQuizData")
    public List<OfflineQuizData> getAllQuizData();

    @Query("Select * from  OfflineQuizData limit :limits")
    public List<OfflineQuizData> getQuizList(int limits);

    @Query("DELETE FROM OfflineQuizData")
    public void nukeTable();

}
