package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.OfflineRewardData;

import java.util.List;

@Dao
public interface OfflineRewardDataDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<OfflineRewardData> offlineRewardData);

    @Query("Select * from  OfflineRewardData")
    public List<OfflineRewardData> getAllReward();

    @Query("Select * from  OfflineRewardData limit :limits")
    public List<OfflineRewardData> getRewardList(int limits);

    @Query("DELETE FROM OfflineRewardData")
    public void nukeTable();

}
