package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.OfflineWeeklyCropInfoData;

import java.util.List;

@Dao
public interface OfflineWeeklyCropInfoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<OfflineWeeklyCropInfoData> weeklyCropInfo);

    @Query("Select * from  OfflineWeeklyCropInfoData")
    public List<OfflineWeeklyCropInfoData> getWeeklyInfo();

    @Query("Select * from  OfflineWeeklyCropInfoData where crop_week=:cropWeek AND p_crop_id = :pCropId")
    public List<OfflineWeeklyCropInfoData> getAllData(String cropWeek, String pCropId);

    @Query("DELETE FROM OfflineWeeklyCropInfoData")
    public void nukeTable();

}
