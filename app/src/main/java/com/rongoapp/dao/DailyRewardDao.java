package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.DailyRewardData;

import java.util.List;

@Dao
public interface DailyRewardDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(DailyRewardData... dailyRewardData);

    @Query("SELECT * FROM DailyRewardData")
    List<DailyRewardData> getAll();

    @Query("DELETE FROM DailyRewardData")
    public void nukeTable();

    @Query("DELETE FROM DailyRewardData WHERE uid = :id")
    void delete(int id);

}