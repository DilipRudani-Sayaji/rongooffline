package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.OfflineData;

import java.util.List;

@Dao
public interface OfflineDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<OfflineData> incomeData);

    @Query("Select * from  OfflineData")
    public List<OfflineData> getAllData();

    @Query("DELETE FROM OfflineData")
    public void nukeTable();
}
