package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.OfflineData;
import com.rongoapp.data.OfflineGrowthData;

import java.util.List;

@Dao
public interface OfflineGrowthDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<OfflineGrowthData> incomeData);

    @Query("Select * from  OfflineGrowthData")
    public List<OfflineGrowthData> getAllData();

    @Query("DELETE FROM OfflineGrowthData")
    public void nukeTable();
}
