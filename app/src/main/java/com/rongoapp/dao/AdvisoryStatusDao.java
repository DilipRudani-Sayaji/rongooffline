package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.AdvisoryStatusData;
import com.rongoapp.data.FeedbackData;

import java.util.List;

@Dao
public interface AdvisoryStatusDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<AdvisoryStatusData> advisoryStatusData);

    @Query("Select * from  AdvisoryStatusData where cropWeek=:cropWeek AND pcropId = :pCropId")
    public List<AdvisoryStatusData> getAllData(String cropWeek, String pCropId);

    @Query("DELETE FROM AdvisoryStatusData")
    public void nukeTable();

    @Query("DELETE FROM ADVISORYSTATUSDATA where cropWeek=:cropWeek AND pcropId = :pCropId")
    public void deleteByCropWeek(String cropWeek, String pCropId);
}
