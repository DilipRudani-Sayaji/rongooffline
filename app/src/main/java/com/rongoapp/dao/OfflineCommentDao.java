package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.OfflineCommentData;

import java.util.List;

@Dao
public interface OfflineCommentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<OfflineCommentData> offlineCommentData);

    @Query("Select * from  OfflineCommentData")
    public List<OfflineCommentData> getAllComment();


    @Query("DELETE FROM OfflineCommentData")
    public void nukeTable();

}
