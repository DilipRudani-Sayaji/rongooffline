package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.JankariFeedBackData;
import com.rongoapp.data.OfflineData;

import java.util.List;

@Dao
public interface JankariFeedbackDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<JankariFeedBackData> incomeData);

    @Query("Select * from  JankariFeedBackData")
    public List<JankariFeedBackData> getAllData();

    @Query("DELETE FROM JankariFeedBackData")
    public void nukeTable();
}
