package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.PostData;

import java.util.List;

@Dao
public interface PostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<PostData> postDataList);

    @Query("Select * from  PostData ORDER BY created_on DESC")
    public List<PostData> getPosts();

    @Query("Select * from  PostData")
    public List<PostData> getAllPosts();

    @Query("Select * from  PostData where post_id = :post_id")
    public PostData getPostByPostId(String post_id);

    @Query("DELETE FROM PostData where user_id = :user_id")
    public void deleteByUserId(String user_id);

    /**
     * Updating only isView
     * By Post id
     */
    @Query("UPDATE PostData SET is_viewd = :is_viewd WHERE post_id =:post_id")
    void update(String is_viewd, String post_id);

    @Query("DELETE FROM PostData")
    public void nukeTable();

}
