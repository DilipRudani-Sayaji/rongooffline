package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.TrackUserOfflineData;

import java.util.List;

@Dao
public interface TrackUserOfflineDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(TrackUserOfflineData... trackUserOfflineData);

    @Query("SELECT * FROM TrackUserOfflineData")
    List<TrackUserOfflineData> getAll();

    @Query("DELETE FROM TrackUserOfflineData")
    public void nukeTable();

    @Query("DELETE FROM TrackUserOfflineData WHERE uid = :id")
    void delete(int id);

}