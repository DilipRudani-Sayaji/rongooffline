package com.rongoapp.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.rongoapp.data.WeeklyCropInfo;

import java.util.List;

@Dao
public interface WeeklyCropInfoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<WeeklyCropInfo> weeklyCropInfo);

    //    @Query("Select * from  WeeklyCropInfo where cropWeek=:cropWeek - 1 AND status = :status")
    @Query("Select * from  WeeklyCropInfo where cropWeek=:cropWeek  AND cropDay = :cropDay AND status = :status AND pcropId = :pCropId")
    public List<WeeklyCropInfo> getWeeklyCropInfo(String cropWeek, String cropDay, String status, String pCropId);

    @Query("Select * from  WeeklyCropInfo where cropWeek=:cropWeek AND status = :status")
    public List<WeeklyCropInfo> getWeeklyCropInfo(String cropWeek, String status);

    @Query("Select * from  WeeklyCropInfo where cropWeek=:cropWeek AND pcropId = :pCropId AND infoTypo = :infoType")
    public List<WeeklyCropInfo> getCropInfoByWeek(String cropWeek, String pCropId, String infoType);

    @Query("Select * from  WeeklyCropInfo where cropWeek=:cropWeek")
    public List<WeeklyCropInfo> getCropInfoByWeek(String cropWeek);

    @Query("Select * from  WeeklyCropInfo")
    public List<WeeklyCropInfo> getWeeklyInfo();

    @Query("DELETE FROM WeeklyCropInfo")
    public void nukeTable();

    //    @Query("UPDATE WeeklyCropInfo SET status = :status WHERE cropWeek =:cropWeek + 1 AND cropDay = :cropDay")
    @Query("UPDATE WeeklyCropInfo SET status = :status WHERE cropWeek =:cropWeek AND cropDay = :cropDay AND pcropId = :pCropId")
    void updateWeeklyCropStatus(String status, String cropWeek, String cropDay, String pCropId);

}
