package com.rongoapp.database;

import android.content.Context;

import androidx.annotation.VisibleForTesting;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.rongoapp.controller.DataConverter;
import com.rongoapp.controller.ListConverters;
import com.rongoapp.dao.AddCropStatusDao;
import com.rongoapp.dao.AdvisoryPhotoDao;
import com.rongoapp.dao.AdvisoryStatusDao;
import com.rongoapp.dao.DailyQuizStatusDao;
import com.rongoapp.dao.DailyRewardDao;
import com.rongoapp.dao.FeedbackDao;
import com.rongoapp.dao.JankariFeedbackDao;
import com.rongoapp.dao.KhetOfflineDao;
import com.rongoapp.dao.LuckyDrawOfflineDao;
import com.rongoapp.dao.OfflineCommentDao;
import com.rongoapp.dao.OfflineDao;
import com.rongoapp.dao.OfflineGrowthDao;
import com.rongoapp.dao.OfflineLikeDao;
import com.rongoapp.dao.OfflineQuizDao;
import com.rongoapp.dao.OfflineRewardDataDao;
import com.rongoapp.dao.OfflineWeeklyCropInfoDao;
import com.rongoapp.dao.PhasalFeedbackDao;
import com.rongoapp.dao.PostDao;
import com.rongoapp.dao.ProfileOfflineDao;
import com.rongoapp.dao.SurveyQuestionsDao;
import com.rongoapp.dao.TrackUserDao;
import com.rongoapp.dao.TrackUserOfflineDao;
import com.rongoapp.dao.UserInviteDao;
import com.rongoapp.dao.WeeklyCropInfoDao;
import com.rongoapp.dao.WeeklyDao;
import com.rongoapp.data.AddCropStatusData;
import com.rongoapp.data.AdvisoryPhotoData;
import com.rongoapp.data.AdvisoryStatusData;
import com.rongoapp.data.DailyQuizStatusData;
import com.rongoapp.data.DailyRewardData;
import com.rongoapp.data.FeedbackData;
import com.rongoapp.data.JankariFeedBackData;
import com.rongoapp.data.KhetOfflineData;
import com.rongoapp.data.LuckyDrawData;
import com.rongoapp.data.OfflineCommentData;
import com.rongoapp.data.OfflineData;
import com.rongoapp.data.OfflineGrowthData;
import com.rongoapp.data.OfflineLikeData;
import com.rongoapp.data.OfflineQuizData;
import com.rongoapp.data.OfflineRewardData;
import com.rongoapp.data.OfflineWeeklyCropInfoData;
import com.rongoapp.data.PhsalFeedback;
import com.rongoapp.data.PostData;
import com.rongoapp.data.ProfileOfflineData;
import com.rongoapp.data.SurveyQuestions;
import com.rongoapp.data.TrackUserData;
import com.rongoapp.data.TrackUserOfflineData;
import com.rongoapp.data.UserInvite;
import com.rongoapp.data.WeeklyAdvisoryData;
import com.rongoapp.data.WeeklyCropInfo;

@Database(entities = {
        DailyQuizStatusData.class, OfflineWeeklyCropInfoData.class, WeeklyCropInfo.class, OfflineQuizData.class,
        OfflineRewardData.class, DailyRewardData.class, KhetOfflineData.class, AdvisoryPhotoData.class,
        TrackUserOfflineData.class, TrackUserData.class, ProfileOfflineData.class, UserInvite.class,
        SurveyQuestions.class, AdvisoryStatusData.class, LuckyDrawData.class, WeeklyAdvisoryData.class,
        PostData.class, PhsalFeedback.class, OfflineData.class, OfflineGrowthData.class, FeedbackData.class,
        JankariFeedBackData.class, OfflineCommentData.class, OfflineLikeData.class, AddCropStatusData.class
}, version = 10, exportSchema = false)

@TypeConverters({ListConverters.class, DataConverter.class})

public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    @VisibleForTesting
    public static final String DATABASE_NAME = "rongo-db";


    public abstract AddCropStatusDao addCropStatusDao();

    public abstract OfflineLikeDao offlineLikeDao();

    public abstract OfflineCommentDao offlineCommentDao();

    public abstract DailyQuizStatusDao dailyQuizStatusDao();

    public abstract OfflineWeeklyCropInfoDao offlineWeeklyCropInfoDao();

    public abstract WeeklyCropInfoDao weeklyCropInfoDao();

    public abstract OfflineQuizDao offlineQuizDao();

    public abstract OfflineRewardDataDao offlineRewardDataDao();

    public abstract DailyRewardDao dailyRewardDao();

    public abstract KhetOfflineDao khetOfflineDao();

    public abstract AdvisoryPhotoDao advisoryPhotoDao();

    public abstract TrackUserOfflineDao trackUserOfflineDao();

    public abstract TrackUserDao trackUserDao();

    public abstract ProfileOfflineDao ProfileOfflineDao();

    public abstract UserInviteDao userInviteDao();

    public abstract WeeklyDao weeklyAdvisoryDao();

    public abstract PostDao postDao();

    public abstract AdvisoryStatusDao advisoryStatusDao();

    public abstract PhasalFeedbackDao phasalFeedbackDao();

    public abstract OfflineDao offlineDao();

    public abstract SurveyQuestionsDao surveyQuestionsDao();

    public abstract OfflineGrowthDao offlineGrowthDao();

    public abstract FeedbackDao feedbackDao();

    public abstract JankariFeedbackDao jankariFeedbackDao();

    public abstract LuckyDrawOfflineDao luckyDrawOfflineDao();


    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .addMigrations(MIGRATION_1_2)
                    .addMigrations(MIGRATION_1_3)
                    .addMigrations(MIGRATION_1_4)
                    .addMigrations(MIGRATION_1_5)
                    .addMigrations(MIGRATION_1_6)
                    .addMigrations(MIGRATION_1_7)
                    .addMigrations(MIGRATION_1_8)
                    .addMigrations(MIGRATION_1_9)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;
    }

    public static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            // Create a new table
            database.execSQL("CREATE TABLE ContactsData (contactId TEXT, name TEXT, mobileNumber TEXT, _id INTEGER PRIMARY KEY NOT NULL)");
            database.execSQL("CREATE TABLE OfflineWeeklyCropInfoData (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, p_crop_id TEXT, season_id TEXT, crop_week TEXT, crop_id TEXT, field_id TEXT, info_type TEXT, info_text TEXT, info_value TEXT, info_metadata TEXT, user_id TEXT)");
            database.execSQL("CREATE TABLE WeeklyCropInfo (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, cropWeek TEXT, cropDay TEXT, isDone TEXT, status TEXT)");
            database.execSQL("CREATE TABLE OfflineQuizData (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, quizID TEXT, cropWeek TEXT, cropDay TEXT, ansValue TEXT, is_true TEXT, metaData TEXT)");
            database.execSQL("CREATE TABLE OfflineRewardData (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, contestID TEXT, rewardID TEXT, type TEXT, value TEXT, text TEXT, is_true TEXT)");
            database.execSQL("CREATE TABLE DailyRewardData (uid INTEGER NOT NULL, reward_points TEXT, metadata TEXT, created_on TEXT, PRIMARY KEY(uid))");
            database.execSQL("CREATE TABLE KhetOfflineData (uid INTEGER NOT NULL, field_id TEXT, season_id TEXT, p_crop_id TEXT, latitude TEXT, longitude TEXT, khet_address TEXT, PRIMARY KEY(uid))");
            database.execSQL("CREATE TABLE DailyQuizStatusData (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, quizID TEXT, cropWeek TEXT, cropDay TEXT, ansValue TEXTō, metaData TEXT)");

            // Copy the data
//            database.execSQL("INSERT INTO `user_new` (id, name, mobile_no) SELECT id, name, mobile_no FROM user");

            // Change the table name to the correct one & Add New Column
//            database.execSQL("ALTER TABLE user_new RENAME TO user");
            database.execSQL("ALTER TABLE PostData ADD COLUMN recording_key TEXT");
            database.execSQL("ALTER TABLE PostData ADD COLUMN recording_local_path TEXT");
            database.execSQL("ALTER TABLE OfflineData ADD COLUMN recording_local_path TEXT");
            database.execSQL("ALTER TABLE FeedbackData ADD COLUMN recording_local_path TEXT");
            database.execSQL("ALTER TABLE AdvisoryStatusData ADD COLUMN isHealth TEXT");

            // Remove the old table
//            database.execSQL("DROP TABLE user");
        }
    };

    public static final Migration MIGRATION_1_3 = new Migration(1, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE WeeklyCropInfo ADD COLUMN infoTypo TEXT");
        }
    };

    public static final Migration MIGRATION_1_4 = new Migration(1, 4) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE PostData ADD COLUMN likes TEXT");
            database.execSQL("ALTER TABLE PostData ADD COLUMN views TEXT");
            database.execSQL("ALTER TABLE PostData ADD COLUMN vote_type TEXT");
            database.execSQL("ALTER TABLE PostData ADD COLUMN is_commented TEXT");
            database.execSQL("ALTER TABLE PostData ADD COLUMN posted_user_name TEXT");
            database.execSQL("ALTER TABLE PostData ADD COLUMN profile_picture TEXT");
            database.execSQL("ALTER TABLE PostData ADD COLUMN posted_user_location TEXT");
            database.execSQL("ALTER TABLE PostData ADD COLUMN is_expert TEXT");

            database.execSQL("ALTER TABLE OfflineData ADD COLUMN season_id TEXT");
            database.execSQL("ALTER TABLE OfflineData ADD COLUMN crop_id TEXT");
            database.execSQL("ALTER TABLE OfflineData ADD COLUMN p_crop_id TEXT");

            database.execSQL("ALTER TABLE LuckyDrawData ADD COLUMN pcropId TEXT");

            database.execSQL("ALTER TABLE OfflineQuizData ADD COLUMN cropId TEXT");
            database.execSQL("ALTER TABLE OfflineQuizData ADD COLUMN pcropId TEXT");

            database.execSQL("ALTER TABLE DailyQuizStatusData ADD COLUMN cropId TEXT");
            database.execSQL("ALTER TABLE DailyQuizStatusData ADD COLUMN pcropId TEXT");

            database.execSQL("ALTER TABLE WeeklyCropInfo ADD COLUMN cropId TEXT");
            database.execSQL("ALTER TABLE WeeklyCropInfo ADD COLUMN pcropId TEXT");

            database.execSQL("ALTER TABLE AdvisoryStatusData ADD COLUMN cropId TEXT");
            database.execSQL("ALTER TABLE AdvisoryStatusData ADD COLUMN pcropId TEXT");

            database.execSQL("ALTER TABLE AdvisoryPhotoData ADD COLUMN cropId TEXT");
            database.execSQL("ALTER TABLE AdvisoryPhotoData ADD COLUMN pcropId TEXT");

            database.execSQL("ALTER TABLE WeeklyAdvisoryData ADD COLUMN cropId TEXT");
            database.execSQL("ALTER TABLE WeeklyAdvisoryData ADD COLUMN pcropId TEXT");

            database.execSQL("ALTER TABLE ProfileOfflineData ADD COLUMN crop_id TEXT");
            database.execSQL("ALTER TABLE ProfileOfflineData ADD COLUMN area_unit TEXT");
            database.execSQL("ALTER TABLE ProfileOfflineData ADD COLUMN crop_status TEXT");
            database.execSQL("ALTER TABLE ProfileOfflineData ADD COLUMN season_feedback TEXT");
            database.execSQL("ALTER TABLE ProfileOfflineData ADD COLUMN season_end_crop_week TEXT");

            database.execSQL("CREATE TABLE OfflineCommentData (user_id TEXT, post_id TEXT, comment_text TEXT, datetime TEXT, _id INTEGER PRIMARY KEY NOT NULL)");
            database.execSQL("CREATE TABLE OfflineLikeData (post_id TEXT, type TEXT, comment_id TEXT, ōdatetime TEXT, _id INTEGER PRIMARY KEY NOT NULL)");

        }
    };

    /*
     * TODO 01-10-2021, Version:6
     * */
    public static final Migration MIGRATION_1_5 = new Migration(1, 5) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE PostData ADD COLUMN is_community_visible TEXT");

            database.execSQL("DROP TABLE IncomeData");
            database.execSQL("DROP TABLE ContactsData");
        }
    };

    /*
     * TODO 01-12-2021, Version:7 for Offline Data Manage
     * */
    public static final Migration MIGRATION_1_6 = new Migration(1, 6) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE AddCropStatusData (ids INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, cropId TEXT, pcropIds TEXT, sessionId TEXT, fieldId TEXT, sowingDate TEXT, sowingDone TEXT, sowingExpDate TEXT, sessionEnd TEXT, sessionEndTotal TEXT, status TEXT)");
        }
    };

    public static final Migration MIGRATION_1_7 = new Migration(1, 7) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE JankariFeedBackData ADD COLUMN is_applied TEXT");

            database.execSQL("ALTER TABLE PostData ADD COLUMN is_applied TEXT");
        }
    };

    public static final Migration MIGRATION_1_8 = new Migration(1, 8) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE FeedbackData ADD COLUMN rate TEXT");
        }
    };

    public static final Migration MIGRATION_1_9 = new Migration(1, 9) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE ProfileOfflineData ADD COLUMN app_language TEXT");
        }
    };

//      TODO Sample for Database Upgrade.  https://medium.com/androiddevelopers/understanding-migrations-with-room-f01e04b07929
//    https://developer.android.com/training/data-storage/room/migrating-db-versions.html
//    https://levelup.gitconnected.com/things-to-keep-in-mind-for-room-db-migrations-7edef257ea3

}
