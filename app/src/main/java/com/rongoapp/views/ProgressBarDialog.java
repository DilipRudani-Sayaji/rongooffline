package com.rongoapp.views;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Handler;
import android.view.WindowManager;

import com.rongoapp.R;
import com.rongoapp.controller.Constants;
import com.rongoapp.utilities.InternetConnection;

public class ProgressBarDialog extends ProgressDialog {

    public static ProgressDialog mProgressBarDialog;
    public static Activity activity;
    public static int progressStatus = 0;
    public static Handler handler = new Handler();

    public ProgressBarDialog(Activity _activity) {
        super(_activity);
        activity = _activity;
        mProgressBarDialog = new ProgressDialog(_activity);
    }

    // Method to show Progress bar
    public void showProgressDialogWithTitle(String substring) {
        Log.e("showProgressD ", substring.toString());
        mProgressBarDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressBarDialog.setCancelable(false);
        if (InternetConnection.getNetworkClass(activity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
            mProgressBarDialog.setTitle(activity.getResources().getString(R.string.str_load_title_msg));
            mProgressBarDialog.setMessage(activity.getResources().getString(R.string.str_slow_intermet_load_msg));
        } else {
            mProgressBarDialog.setMessage(activity.getResources().getString(R.string.str_load_title_msg));
        }
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!((Activity) activity).isFinishing()) {
                    try {
                        mProgressBarDialog.show();
                    } catch (WindowManager.BadTokenException e) {
                        Log.e("WindowManagerBad ", e.toString());
                    }
                }
            }
        });
    }

    public void showProgressDialogWithTitle(String title, String substring) {
        mProgressBarDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressBarDialog.setCancelable(false);
        mProgressBarDialog.setTitle(title);
        mProgressBarDialog.setMessage(substring);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!((Activity) activity).isFinishing()) {
                    try {
                        mProgressBarDialog.show();
                    } catch (WindowManager.BadTokenException e) {
                        Log.e("WindowManagerBad ", e.toString());
                    }
                }
            }
        });
    }

    // Method to show Progress bar
    public void showProgressDialogWithTitleCount(String title, String substring) {
        mProgressBarDialog.setTitle(title);
        mProgressBarDialog.setMessage(substring);
        mProgressBarDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressBarDialog.setCancelable(false);
        mProgressBarDialog.setMax(100);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!((Activity) activity).isFinishing()) {
                    try {
                        mProgressBarDialog.show();
                    } catch (WindowManager.BadTokenException e) {
                        Log.e("WindowManagerBad ", e.toString());
                    }
                }
            }
        });

        // Start Process Operation in a background thread
        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < 100) {
                    try {
                        // This is mock thread using sleep to show progress
                        Thread.sleep(200);
                        progressStatus += 5;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    // Change percentage in the progress bar
                    handler.post(new Runnable() {
                        public void run() {
                            mProgressBarDialog.setProgress(progressStatus);
                        }
                    });
                }
                //hide Progressbar after finishing process
                hideProgressDialogWithTitle();
            }
        }).start();
    }

    // Method to hide/ dismiss Progress bar
    public void hideProgressDialogWithTitle() {
        mProgressBarDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressBarDialog.dismiss();
//        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

}
