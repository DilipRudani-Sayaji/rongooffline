package com.rongoapp.controller;

import com.google.firebase.inappmessaging.FirebaseInAppMessagingClickListener;
import com.google.firebase.inappmessaging.model.Action;
import com.google.firebase.inappmessaging.model.CampaignMetadata;
import com.google.firebase.inappmessaging.model.InAppMessage;
import com.rongoapp.views.Log;

//        Trigger in-app messages programmatically
//        FirebaseInAppMessaging.getInstance().triggerEvent("exampleTrigger");

//        Temporarily disable in-app messages
//        FirebaseInAppMessaging.getInstance().setMessagesSuppressed(true);

// Only needed if firebase_inapp_messaging_auto_data_collection_enabled is set to
// false in AndroidManifest.xml
//FirebaseInAppMessaging.getInstance().setAutomaticDataCollectionEnabled(true);MainActivity.java

public class InAppMSGClickListener implements FirebaseInAppMessagingClickListener {

    private static final String TAG = "FirebaseInAppMessagingClickListener";

    @Override
    public void messageClicked(InAppMessage inAppMessage, Action action) {
        // Determine which URL the user clicked
        String url = action.getActionUrl();
        Log.e(TAG, "" + url);

        // Get general information about the campaign
        CampaignMetadata metadata = inAppMessage.getCampaignMetadata();
        Log.e(TAG, "" + metadata);
    }
}