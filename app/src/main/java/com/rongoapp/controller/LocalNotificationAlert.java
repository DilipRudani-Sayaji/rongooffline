package com.rongoapp.controller;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.rongoapp.R;
import com.rongoapp.activities.MainActivity;
import com.rongoapp.utilities.ServiceUtility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class LocalNotificationAlert extends BroadcastReceiver {

    public static final String NOTIFICATION_CHANNEL_ID = "10001";
    public static String NOTIFICATION_ID = "notification-id";
    public static String NOTIFICATION_TYPE = "notification-type";
    public static String NOTIFICATION = "notification";
    String dataType, notificationText = "Rongo";

    public void onReceive(Context context, Intent intent) {
//        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        Notification notification = intent.getParcelableExtra(NOTIFICATION);
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//            int importance = NotificationManager.IMPORTANCE_HIGH;
//            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
//            assert notificationManager != null;
//            notificationManager.createNotificationChannel(notificationChannel);
//        }
//        int id = intent.getIntExtra(NOTIFICATION_ID, 0);
//        assert notificationManager != null;
//        notificationManager.notify(id, notification);

        dataType = intent.getExtras().getString(NOTIFICATION_TYPE);
        Log.e("msg: ", "" + dataType);
        if (dataType.equalsIgnoreCase("login")) {
            notificationText = "मौसम लगातार बदल रहा है। ऐसे में अपने ऐप को लॉगइन करना न भूलें। खेती से जुड़ी नवीनतम जानकारियां प्राप्त करें और कृषि वैज्ञानिकों से सीधे बात करें।";
        } else if (dataType.equalsIgnoreCase("register")) {
            notificationText = "हमें खेद है कि कोशिश के बावजूद आपका रजिस्ट्रेशन सफल नहीं हुआ। कृपया दोबारा कोशिश करें या मदद के लिए हमें 079-480-61880 पर कॉल करें।";
        } else if (dataType.equalsIgnoreCase("weather_alert")) {
            notificationText = "बात चाहे बाढ़ की हो, सूखे की हो या तेज हवाओं की! रोगों ऐप आपको आने वाली हर मुसीबतों से सही समय पर आपको सावधान करता है! जाने कैसे बचाएं खराब मौसम से अपनी फसलों को !";
        } else if (dataType.equalsIgnoreCase("no_sowing_date")) {
            notificationText = "रोंगो ऐप का फायदा पाने और 20 प्वाइंटस कमाने के लिए फसल और बुवाई की तारीख दर्ज करें।";
        } else if (dataType.equalsIgnoreCase("expected_sowing")) {
            notificationText = "एप्लिकेशन का पूरा लाभ पाने और अंक अर्जित करने के लिए तारीख और बीज का प्रकार सही-सही दर्ज करें।";
        } else if (dataType.equalsIgnoreCase("weekly_alert1")) {
            notificationText = "" + getCropWeekName(context) + " चरण के लिए सही जानकारी अब आपके लिए उपलब्ध है। मौसम, बीमारी और कीटों के बारे में पता करने के लिए आज ही रोंगो ऐप पर लॉगिन करें।";
        } else if (dataType.equalsIgnoreCase("weekly_alert4")) {
            notificationText = "रोंगो ऐप पर अपनी फसल संबंधी विविध परेशानियों का हल हमारे अनुभवी कृषि वैज्ञानिकों और कृषि समुदाय से पूछें।";
        } else if (dataType.equalsIgnoreCase("weekly_alert5")) {
            notificationText = "कैसी है आपकी फसल? रोंगो ऐप पर प्रतियोगिता में भाग लेने के लिए हमें अपनी फसल की फोटो भेजना न भूलें।";
        } else if (dataType.equalsIgnoreCase("weekly_alert7")) {
            notificationText = "हर हफ्ते अंक कमाने के लिए हम आपके लिए रोंगो ऐप पर एक नई प्रतियोगिता लाते हैं। रोजाना लॉगइन करें और आसान से सवालों का जवाब लेकर प्रतियोगिता में भाग लें।";
        } else if (dataType.equalsIgnoreCase("internet")) {
            notificationText = "धीमे इंटरनेट के कारण आपका प्रश्न रोंगो एक्सपर्ट तक नहीं पहुंचा है। तुरंत भेजने के लिए कृपया बेहतर इंटरनेट से कनेक्ट करें, या तो हमारी हेल्पलाइन 079-480-61880 पर एक मिस्ड कॉल करे।";
        } else if (dataType.equalsIgnoreCase("invite")) {
            notificationText = "अन्य किसानों के साथ रोंगो ऐप साझा करना न भूलें और प्वाइंट्स जीतें!";
        } else if (dataType.equalsIgnoreCase("invite_1")) {
            notificationText = "बधाई हो! आपके आमंत्रित मित्र के रोंगो में जुड़ने से आपने %s पॉइंट प्राप्त कर लिए है।";
        } else if (dataType.equalsIgnoreCase("ask_question")) {
            notificationText = "समस्या के समाधान को लेकर परेशां न हो, अपने सवाल हमारे विशेषज्ञ से पूछें। फोटो खीचें और अपने प्रश्न यहां पोस्ट करें";
        } else if (dataType.equalsIgnoreCase("community")) {
            notificationText = "मौसम, बीमारी और कीट की स्थिति लगातार बदल रही है। ऐसे में रोंगो ऐप को लॉगइन करना न भूलें। खेती की नई जानकारियां प्राप्त करें और सीधे हमारे कृषि वैज्ञानिकों व कृषि समुदाय से जुड़ें।";
        } else if (dataType.equalsIgnoreCase("quiz")) {
            notificationText = "आज के प्रश्न को देखने और पॉइंट्स जीतने के लिए लॉगिन करें!";
        } else if (dataType.equalsIgnoreCase("setting")) {
            notificationText = "20 पॉइंट्स पाने के लिए, रोंगो ऐप में लॉगिन करें और अपने बीज के प्रकार को प्रगति / सेटिंग पेज में दर्ज करें!";
        } else if (dataType.equalsIgnoreCase("reward")) {
            notificationText = "दिलचस्प पुरस्कार के साथ आप के लिए विशेष प्रतियोगिता - अब जीतने के लिए पहिया स्पिन करें सिर्फ रोंगो एप पर!";
        } else if (dataType.equalsIgnoreCase("fertilizer")) {
            notificationText = "उर्वरक कौनसा डालें, इस पर समय बर्बाद न करें, बस रोंगो ऍप में लॉगिन करें और पूरी जानकारी प्राप्त करें";
        } else if (dataType.equalsIgnoreCase("user_action")) {
            notificationText = "हमारे द्वारा दी गयी जानकारी पर अपनी राय दें और पॉइंट्स जीतें। इससे हम ऐप को आपके लिए अधिक आसान बना सकेंगे।";
        } else if (dataType.equalsIgnoreCase("no_action")) {
            notificationText = "आपके प्रश्न का उत्तर अब रोंगो ऐप पर उपलब्ध है। देखने के लिए लॉगिन करें!";
        } else if (dataType.equalsIgnoreCase("expert")) {
            notificationText = "आपका पौधा कीट व रोग से सुरक्षित है? अगर नहीं तो कॉल 079-480-61880 या हमारे कृषि समुदाय या रोंगो  ऐप में अपना प्रश्न पूछें।";
        } else if (dataType.equalsIgnoreCase("expert_1")) {
            notificationText = "रोग हो या कीट..हमारे विशेषज्ञों के पास आपके हर सवाल का जवाब है।";
        }

        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        notificationIntent.setAction(Long.toString(System.currentTimeMillis()));

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(notificationIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(notificationText))
                .setAutoCancel(true)
                .setChannelId(NOTIFICATION_CHANNEL_ID)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setSound(defaultSoundUri)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setSmallIcon(getNotificationIcon())
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION, NotificationManager.IMPORTANCE_HIGH);
            mChannel.setDescription(NOTIFICATION);
            mChannel.enableLights(true);
            mChannel.setShowBadge(true);
            mChannel.setLightColor(Color.RED);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(mChannel);
        }
        notificationManager.notify(ServiceUtility.randInt(0, 99999), notificationBuilder.build());
    }

    int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_notif_trans : R.mipmap.ic_launcher;
    }

    String getCropWeekName(Context context) {
        String stageName = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
            Date c = Calendar.getInstance().getTime();

            String SowingDate = context.getSharedPreferences(Constants.MediaPrefs, 0).getString(Constants.SharedPreference_SowingDate, "");
            if (!TextUtils.isEmpty(SowingDate)) {
                Date date1 = dateFormat.parse(SowingDate);
                Date date2 = dateFormat.parse(dateFormat.format(c.getTime()));

                long crop_in_days = date2.getTime() - date1.getTime();
                long crop_in_weeks = crop_in_days / 7;

                long diffInDay = TimeUnit.DAYS.convert(crop_in_weeks, TimeUnit.MILLISECONDS);
                long diffInDays = round(diffInDay);

                JSONArray data = new JSONArray(FetchJsonFromAssetsFile.loadAdvisoryJSONFromAsset(context));
                if (data != null && data.length() > 0) {
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject resultData = data.getJSONObject(i);
                        JSONObject result = resultData.optJSONObject("result");
                        if (i == diffInDays) {
                            JSONObject weekly_advisory = result.optJSONObject("weekly_advisory");
                            if (weekly_advisory != null && weekly_advisory.length() > 0) {
                                stageName = weekly_advisory.optString("stage");
                            }
                        }
                    }
                }
                Log.e("AlarmReceiver App", "" + stageName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stageName;
    }

    int round(double d) {
        double dAbs = Math.abs(d);
        int i = (int) dAbs;
        double result = dAbs - (double) i;
        if (result < 0.0) {
            return d < 0 ? -i : i;
        } else {
            return d < 0 ? -(i + 1) : i + 1;
        }
    }


    public static void scheduleNotification(Context context, String type, int delay) {
        Intent notificationIntent = new Intent(context, LocalNotificationAlert.class);
        notificationIntent.putExtra(LocalNotificationAlert.NOTIFICATION_TYPE, type);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        assert alarmManager != null;
        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }
}

/*
 * For how to use notification
 * */
//    scheduleNotification(getNotification("30 second delay"), 30000);

//    private void scheduleNotification(Context context, Notification notification, int delay) {
//        Intent notificationIntent = new Intent(context, LocalNotificationAlert.class);
//        notificationIntent.putExtra(LocalNotificationAlert.NOTIFICATION_ID, 1);
//        notificationIntent.putExtra(LocalNotificationAlert.NOTIFICATION, notification);
//        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//        long futureInMillis = SystemClock.elapsedRealtime() + delay;
//        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
//        assert alarmManager != null;
//        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
//    }

//    private Notification getNotification(String content) {
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "default");
//        builder.setContentTitle("Scheduled Notification");
//        builder.setContentText(content);
//        builder.setSmallIcon(R.mipmap.ic_launcher);
//        builder.setAutoCancel(true);
//        builder.setChannelId(LocalNotificationAlert.NOTIFICATION_CHANNEL_ID);
//        return builder.build();
//    }