package com.rongoapp.controller;

public interface CropSwitchCallback<status> {
    void onComplete(String result);
}