package com.rongoapp.controller;

public interface AudioRecorderCallback {

    void onDone(String status);

}