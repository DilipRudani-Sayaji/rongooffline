package com.rongoapp.controller;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rongoapp.data.SurveyQuestionItemData;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

public class DataConverter implements Serializable {

    @TypeConverter // note this annotation
    public String fromOptionValuesList(List<SurveyQuestionItemData> optionValues) {
        if (optionValues == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<SurveyQuestionItemData>>() {
        }.getType();
        String json = gson.toJson(optionValues, type);
        return json;
    }

    @TypeConverter // note this annotation
    public List<SurveyQuestionItemData> toOptionValuesList(String optionValuesString) {
        if (optionValuesString == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<SurveyQuestionItemData>>() {
        }.getType();
        List<SurveyQuestionItemData> productCategoriesList = gson.fromJson(optionValuesString, type);
        return productCategoriesList;
    }
}
