package com.rongoapp.controller;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.makeramen.roundedimageview.RoundedImageView;
import com.rongoapp.R;
import com.rongoapp.utilities.TouchImageView;

public class ImageZoomDialog {

    public static void openImages(Activity context, String imagePath) {
        Dialog dialog = new Dialog(context, R.style.NewDialog);
        dialog.setContentView(R.layout.bottom_show_images);
        dialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);

        RoundedImageView imgClose = dialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        TouchImageView ivTouch = dialog.findViewById(R.id.ivTouch);
//        ivTouch.setScaleType(ImageView.ScaleType.FIT_XY);
        GlideApp.with(ivTouch.getContext()).load(imagePath).into(ivTouch);
    }

}
