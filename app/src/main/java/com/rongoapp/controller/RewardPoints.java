package com.rongoapp.controller;

import android.content.SharedPreferences;
import android.text.TextUtils;

import org.json.JSONObject;

public class RewardPoints {

    private static final String dailyLogin = "daily_login";
    private static final String postQuestion = "post_question";
    private static final String peopleInvite = "people_invite";
    private static final String dailyQuiz = "daily_quiz";
    private static final String uploadPicture = "upload_picture";
    private static final String sowingInfo = "sowing_info";
    private static final String weeklyCropInfo = "weekly_crop_info";
    private static final String weeklyCropHealth = "weekly_crop_health";
    private static final String advisoryFeedback = "advisory_feedback";
    private static final String advisoryShare = "advisory_share";
    private static final String survey = "survey";
    private static final String referral = "referral";


    public static String getDailyLoginPoint(SharedPreferences sharedPreferences) {
        String points = "0";
        try {
            String rewardData = sharedPreferences.getString(Constants.SharedPreference_Reward, "");
            if (!TextUtils.isEmpty(rewardData)) {
                JSONObject result = new JSONObject(rewardData);
                if (result != null && result.length() > 0) {
                    points = result.getString(dailyLogin);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return points;
    }

    public static String getPostQuestionRewardPoint(SharedPreferences sharedPreferences) {
        String points = "0";
        try {
            String rewardData = sharedPreferences.getString(Constants.SharedPreference_Reward, "");
            if (!TextUtils.isEmpty(rewardData)) {
                JSONObject result = new JSONObject(rewardData);
                if (result != null && result.length() > 0) {
                    points = result.getString(postQuestion);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return points;
    }

    public static String getInviteRewardPoint(SharedPreferences sharedPreferences) {
        String points = "0";
        try {
            String rewardData = sharedPreferences.getString(Constants.SharedPreference_Reward, "");
            if (!TextUtils.isEmpty(rewardData)) {
                JSONObject result = new JSONObject(rewardData);
                if (result != null && result.length() > 0) {
                    points = result.getString(peopleInvite);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return points;
    }

    public static String getDailyQuizRewardPoint(SharedPreferences sharedPreferences) {
        String points = "0";
        try {
            String rewardData = sharedPreferences.getString(Constants.SharedPreference_Reward, "");
            if (!TextUtils.isEmpty(rewardData)) {
                JSONObject result = new JSONObject(rewardData);
                if (result != null && result.length() > 0) {
                    points = result.getString(dailyQuiz);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return points;
    }

    public static String getUploadPictureRewardPoint(SharedPreferences sharedPreferences) {
        String points = "0";
        try {
            String rewardData = sharedPreferences.getString(Constants.SharedPreference_Reward, "");
            if (!TextUtils.isEmpty(rewardData)) {
                JSONObject result = new JSONObject(rewardData);
                if (result != null && result.length() > 0) {
                    points = result.getString(uploadPicture);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return points;
    }

    public static String getSowingDateRewardPoint(SharedPreferences sharedPreferences) {
        String points = "0";
        try {
            String rewardData = sharedPreferences.getString(Constants.SharedPreference_Reward, "");
            if (!TextUtils.isEmpty(rewardData)) {
                JSONObject result = new JSONObject(rewardData);
                if (result != null && result.length() > 0) {
                    points = result.getString(sowingInfo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return points;
    }

    public static String getAdvisoryFeedbackRewardPoint(SharedPreferences sharedPreferences) {
        String points = "0";
        try {
            String rewardData = sharedPreferences.getString(Constants.SharedPreference_Reward, "");
            if (!TextUtils.isEmpty(rewardData)) {
                JSONObject result = new JSONObject(rewardData);
                if (result != null && result.length() > 0) {
                    points = result.getString(advisoryFeedback);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return points;
    }

    public static String getAdvisoryShareRewardPoint(SharedPreferences sharedPreferences) {
        String points = "0";
        try {
            String rewardData = sharedPreferences.getString(Constants.SharedPreference_Reward, "");
            if (!TextUtils.isEmpty(rewardData)) {
                JSONObject result = new JSONObject(rewardData);
                if (result != null && result.length() > 0) {
                    points = result.getString(advisoryShare);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return points;
    }

    public static String getWeeklyCropInfoRewardPoint(SharedPreferences sharedPreferences) {
        String points = "0";
        try {
            String rewardData = sharedPreferences.getString(Constants.SharedPreference_Reward, "");
            if (!TextUtils.isEmpty(rewardData)) {
                JSONObject result = new JSONObject(rewardData);
                if (result != null && result.length() > 0) {
                    points = result.getString(weeklyCropInfo);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return points;
    }

    public static String getCropHealthRewardPoint(SharedPreferences sharedPreferences) {
        String points = "0";
        try {
            String rewardData = sharedPreferences.getString(Constants.SharedPreference_Reward, "");
            if (!TextUtils.isEmpty(rewardData)) {
                JSONObject result = new JSONObject(rewardData);
                if (result != null && result.length() > 0) {
                    points = result.getString(weeklyCropHealth);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return points;
    }

    public static String getSurveyRewardPoint(SharedPreferences sharedPreferences) {
        String points = "0";
        try {
            String rewardData = sharedPreferences.getString(Constants.SharedPreference_Reward, "");
            if (!TextUtils.isEmpty(rewardData)) {
                JSONObject result = new JSONObject(rewardData);
                if (result != null && result.length() > 0) {
                    points = result.getString(survey);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return points;
    }

    public static String getReferralRewardPoint(SharedPreferences sharedPreferences) {
        String points = "0";
        try {
            String rewardData = sharedPreferences.getString(Constants.SharedPreference_Reward, "");
            if (!TextUtils.isEmpty(rewardData)) {
                JSONObject result = new JSONObject(rewardData);
                if (result != null && result.length() > 0) {
                    points = result.getString(referral);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return points;
    }

}
