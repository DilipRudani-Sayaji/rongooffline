package com.rongoapp.controller;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class FetchJsonFromAssetsFile {

    public static String getJsonFromAssets(Activity context, String fileName) {
        String jsonString;
        try {
            InputStream is = context.getAssets().open(fileName);

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            jsonString = new String(buffer, Constants.FILE_ENCODING_TYPE);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return jsonString;
    }


    public static String loadAdvisoryJSONFromAsset(Context activity) {
        String json = null;
        try {
            String seasonName = RongoApp.getSeasonName();
            if (TextUtils.isEmpty(seasonName)) {
                seasonName = Functions.getSeasonName(RongoApp.getSelectedCrop());
            }

//            InputStream is = activity.getAssets().open("advisory_json/crop_1_advisory_kharif_hi.json");
            InputStream is = activity.getAssets().open(Constants.ASSETS_ADVISORY_JSON_PATH + RongoApp.getSelectedCrop()
                    + "_advisory_" + seasonName + "_" + RongoApp.getDefaultLanguage() + ".json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, Constants.FILE_ENCODING_TYPE);
        } catch (Exception e) {
            e.printStackTrace();
            SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                    Constants.SharedPreferences_SEASON_NAME, "");
            return loadAdvisoryJSONFromAsset(activity);
        }
        return json;
    }

    public static String loadAllCropsJSONFromAsset(Activity activity, String cropId, String season, String language) {
        String json = null;
        try {
//            InputStream is = activity.getAssets().open("advisory_json/crop_1_advisory_kharif_hi.json");
            InputStream is = activity.getAssets().open(Constants.ASSETS_ADVISORY_JSON_PATH + cropId
                    + "_advisory_" + season + "_" + language + ".json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, Constants.FILE_ENCODING_TYPE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

    public static String loadAdvisoryKeywordJSONFromAsset(Activity activity, String keywordId) {
        String json = null;
        try {
            InputStream is = activity.getAssets().open(Constants.ASSETS_ADVISORY_KEYWORD_PATH
                    + RongoApp.getDefaultLanguage() + "/" + keywordId + ".json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, Constants.FILE_ENCODING_TYPE);
        } catch (FileNotFoundException e) {
            return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
        return json;
    }

    public static String loadHighKeywordJSONFromAsset(Context activity) {
        String json = null;
        try {
            InputStream is = activity.getAssets().open(Constants.ASSETS_ADVISORY_KEYWORD_PRIORITY_PATH
                    + RongoApp.getSelectedCrop() + "_keyword_priority.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, Constants.FILE_ENCODING_TYPE);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String getCityStateListJSONFromAsset(Context activity) {
        String json = null;
        try {
            InputStream is = activity.getAssets().open("cityStateList.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, Constants.FILE_ENCODING_TYPE);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String getAllCropListJSONFromAsset(Context activity) {
        String json = null;
        try {
            InputStream is = activity.getAssets().open("allCropList.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, Constants.FILE_ENCODING_TYPE);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String getAllRewardListJSONFromAsset(Context activity) {
        String json = null;
        try {
            InputStream is = activity.getAssets().open("allRewardPointsList.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, Constants.FILE_ENCODING_TYPE);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String getAllCropWeeklyQuestionJSONFromAsset(Context activity) {
        String json = null;
        try {
            InputStream is = activity.getAssets().open("weeklyCropQuestion.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, Constants.FILE_ENCODING_TYPE);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String getFertilizerDataJSONFromAsset(Context activity) {
        String json = null;
        try {
            InputStream is = activity.getAssets().open("fertilizerList.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, Constants.FILE_ENCODING_TYPE);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String getMasterDropDownDataJSONFromAsset(Context activity) {
        String json = null;
        try {
            InputStream is = activity.getAssets().open("masterDropdownList.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, Constants.FILE_ENCODING_TYPE);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String getSessionEndDataJSONFromAsset(Context activity) {
        String json = null;
        try {
            InputStream is = activity.getAssets().open("sessionEndData.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, Constants.FILE_ENCODING_TYPE);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String getProfileDataJSONFromAsset(Context activity) {
        String json = null;
        try {
            InputStream is = activity.getAssets().open("profileData.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, Constants.FILE_ENCODING_TYPE);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    public static String getPragatiDataJSONFromAsset(Context activity) {
        String json = null;
        try {
            InputStream is = activity.getAssets().open("pragatiData.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, Constants.FILE_ENCODING_TYPE);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String getDailyQuizDataJSONFromAsset(Context activity) {
        String json = null;
        try {
            InputStream is = activity.getAssets().open("dailyQuizData.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, Constants.FILE_ENCODING_TYPE);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public static String getTestDataJSONFromAsset(Context activity) {
        String json = null;
        try {
            InputStream is = activity.getAssets().open("test.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, Constants.FILE_ENCODING_TYPE);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
