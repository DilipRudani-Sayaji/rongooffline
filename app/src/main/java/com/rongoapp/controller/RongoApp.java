package com.rongoapp.controller;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.SystemClock;
import android.provider.Settings;
import android.speech.tts.TextToSpeech;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.facebook.FacebookSdk;
import com.facebook.LoggingBehavior;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rongoapp.R;
import com.rongoapp.activities.MainActivity;
import com.rongoapp.audioPlayerTTS.TTS;
import com.rongoapp.data.AddCropStatusData;
import com.rongoapp.data.AppOpenData;
import com.rongoapp.data.CropAreaFocusedData;
import com.rongoapp.data.CropsData;
import com.rongoapp.data.InsertHomeFeedData;
import com.rongoapp.data.MasterCropData;
import com.rongoapp.data.PostData;
import com.rongoapp.data.ProblemRelatedData;
import com.rongoapp.data.SeedTypeData;
import com.rongoapp.data.SurveyQuestionItemData;
import com.rongoapp.data.SurveyQuestions;
import com.rongoapp.data.TrackUserOfflineData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.network.DroidListener;
import com.rongoapp.network.DroidNet;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.ExceptionHandler;
import com.rongoapp.utilities.InternetConnection;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RongoApp extends Application implements DroidListener {

    public static RongoApp rongoApp;
    public static Context mContext;
    public static AppDatabase appDatabase;
    public static SharedPreferences sharedPreferences;
    public static FirebaseAnalytics mFirebaseAnalytics;
    public static AppOpenData mAppOpenData = new AppOpenData();
    public static TTS tts;

    public DroidNet mDroidNet;
    public static long mLastLongClick = 0;
    public static boolean howToEarn = true, isConnected, isTrackAPI = true;

    public static synchronized RongoApp getInstance() {
        return rongoApp;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            appDatabase = AppDatabase.getDatabase(this);
            sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
            mContext = getApplicationContext();
            rongoApp = this;
            DroidNet.init(this);
            mDroidNet = DroidNet.getInstance();
            mDroidNet.addInternetConnectivityListener(this);

            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this));

            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

//            InAppMSGClickListener listener = new InAppMSGClickListener();
//            FirebaseInAppMessaging.getInstance().addClickListener(listener);

            FacebookSdk.sdkInitialize(this);
            FacebookSdk.setAutoLogAppEventsEnabled(true);
            FacebookSdk.setAutoInitEnabled(true);
            FacebookSdk.fullyInitialize();
            FacebookSdk.setAdvertiserIDCollectionEnabled(true);
            AppEventsLogger.activateApp(this);
//            FacebookSdk.setIsDebugEnabled(true);
            FacebookSdk.addLoggingBehavior(LoggingBehavior.APP_EVENTS);

//          Smartlook.setupAndStartRecording("41bd64d822bfa6c95740c00e9756c1f048612030");

//            if (InternetConnection.checkConnectionForFasl(this)) {
//                Log.v("++getRequestProtocol++", "" + getRequestProtocol());
//            }

            setAppOpenData();
            getCropWeekDaysFunction();

            TextToSpeech textToSpeech = new TextToSpeech(this, null, Constants.GOOGLE_TTS_ENGINE);
            if (getDefaultLanguage().equalsIgnoreCase(LocaleManager.GUJARATI)) {
                tts = new TTS(mContext, new Locale(Constants.Gujarati_Language_code));
                textToSpeech.setLanguage(new Locale(Constants.Gujarati_Language_code));
            } else if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.MARATHI)) {
                tts = new TTS(mContext, new Locale(Constants.Marathi_Language_code));
                textToSpeech.setLanguage(new Locale(Constants.Marathi_Language_code));
            } else {
                tts = new TTS(mContext, new Locale(Constants.Hindi_Language_code));
                textToSpeech.setLanguage(new Locale(Constants.Hindi_Language_code));
            }

            if (InternetConnection.checkConnectionForFasl(this)) {
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_NetworkType, "" + InternetConnection.getNetworkClass(mContext));
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_NetworkSpeed, "" + InternetConnection.getNetworkSpeed(mContext));
                logActivatedAppEvent(Functions.getCurrentDateTime());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getRequestProtocol() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        HttpURLConnection connection = null;
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("app_language", RongoApp.getDefaultLanguage());
            jsonObject.put("app_version", RongoApp.getAppVersion());
            jsonObject.put("device_id", Functions.getDeviceid(this));
            String urlParameters = jsonObject.toString();

            URL url = new URL("http://rongoapp.in/openapi_v2/getRequestProtocol");
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");
            connection.setRequestProperty("Authorization", "PRIVATE-TOKEN dGVzdA==");
            connection.setUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);

            // Send request
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();

            // Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();

            String r_str = response.toString();
            if (!TextUtils.isEmpty(r_str)) {
                JSONObject jb = new JSONObject(r_str);
                if (jb != null && jb.length() > 0) {
                    if (jb.optString("status_code").equalsIgnoreCase("0")) {
                        Log.e("Domain: ", "" + jb.optString("data"));
                        if (!jb.optString("data").equalsIgnoreCase("null") && !TextUtils.isEmpty(jb.optString("data"))) {
//                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Protocol,
//                                    "" + jb.optString("data"));

                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Protocol,
                                    "https");
                        }
                    }
                }
            }

            return response.toString();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return null;
    }

    public static void hideSystemUI(Activity activity) {
        activity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_IMMERSIVE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }

    public void setAppOpenData() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            mAppOpenData.mPackageInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            mAppOpenData.setCurrentAppVersion(mAppOpenData.getmPackageInfo().versionName);
            mAppOpenData.setAndroidId("" + Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID));
            mAppOpenData.setmBrand(Build.BRAND);
            mAppOpenData.setmModel(Build.MODEL);
            mAppOpenData.setmDeviceType("Android");
            mAppOpenData.setmOsVersion(Build.VERSION.RELEASE);
            mAppOpenData.setmInterNetConnection(InternetConnection.getNetworkClass(this));
//            mAppOpenData.setmDeviceId("" + telephonyManager.getDeviceId());
            mAppOpenData.setmDeviceId("");
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public static List<MasterCropData> getMasterCropList() {
        List<MasterCropData> masterCropList = new ArrayList<>();
        String json = sharedPreferences.getString(Constants.SharedPreferences_Master_Crop_List, "");
        if (!TextUtils.isEmpty(json)) {
            Type type = new TypeToken<List<MasterCropData>>() {
            }.getType();
            masterCropList = new Gson().fromJson(json, type);
        }
        return masterCropList;
    }

    /*
     * TODO App Language Change Function
     * */
    public static void setNewLocale(Activity mContext, @LocaleManager.LocaleDef String language) {
        if (TextUtils.isEmpty(language)) {
            language = LocaleManager.HINDI;
        }

        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Default_Language, language);
        LocaleManager.setNewLocale(mContext, language);
        Intent intent = new Intent(mContext, MainActivity.class);
        mContext.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        mContext.finish();
    }

    public static void setCropSwitch(Activity mContext, @LocaleManager.LocaleDef String language) {
        if (TextUtils.isEmpty(language)) {
            language = LocaleManager.HINDI;
        }
        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Default_Language, language);
        LocaleManager.setNewLocale(mContext, language);
        Intent intent = mContext.getIntent();
        mContext.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        mContext.finish();
    }

    public static String getProtocol() {
        return sharedPreferences.getString(Constants.SharedPreference_Protocol, mContext.getResources().getString(R.string.domain));
    }

    public static String getSessionEndCropId() {
        return sharedPreferences.getString(Constants.SharedPreferences_SessionEndCropID, "");
    }

    public static String getFCMToken() {
        return sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, "");
    }

    public static String getSelectedCrop() {
        return sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, "1");
    }

    public static String getSelectedState() {
        return sharedPreferences.getString(Constants.SharedPreference_User_State, "");
    }

    public static String getSelectedPCrop() {
        return sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, RongoApp.getSelectedCrop());
    }

    public static String getWeeklyCropQuestion() {
        return sharedPreferences.getString(Constants.SharedPreferences_Weekly_Crop_Question, "");
    }

    public static String getUserId() {
        return sharedPreferences.getString(Constants.SharedPreference_UserId, "");
    }

    public static String getUserFullName() {
        return sharedPreferences.getString(Constants.SharedPreference_First_Name, "") + " " + sharedPreferences.getString(Constants.SharedPreference_Last_Name, "");
    }

    public static String getUserFirstName() {
        return sharedPreferences.getString(Constants.SharedPreference_UserId, "");
    }

    public static String getUserNumber() {
        return sharedPreferences.getString(Constants.SharedPreferences_MOBILE_NO, "");
    }

    public static String getDefaultLanguage() {
        return sharedPreferences.getString(Constants.SharedPreference_Default_Language, LocaleManager.HINDI);
    }

    public static String getSeasonName() {
        return sharedPreferences.getString(Constants.SharedPreferences_SEASON_NAME, Constants.KHARIF_SEASON);
    }

    public static String getReferralImage(Context activity) {
        return sharedPreferences.getString(Constants.SharedPreference_Referral_Image, "");
    }

    public static String getReferralText(Context activity) {
        return sharedPreferences.getString(Constants.SharedPreference_Referral_Link, "");
    }

    public static int getSessionEndWeek() {
        return Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"));
    }

    public static String getCurrentCropWeek() {
        return sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0");
    }

    public static String getWhatsAppNotificationStatus() {
        return sharedPreferences.getString(Constants.SharedPreference_WhatsApp_Notif, "");
    }

    public static String getAppVersion() {
        String appVersion = "0";
        try {
            PackageInfo pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            appVersion = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return appVersion;
        }
        return appVersion;
    }

    /**
     * This function assumes logger is an instance of AppEventsLogger and has been
     * created using AppEventsLogger.newLogger() call.
     */
    public void logActivatedAppEvent(String datetime) {
        Bundle params = new Bundle();
        params.putString("datetime", datetime);
        AppEventsLogger.newLogger(mContext).logEvent(Constants.FB_EVENT_NAME_ACTIVATED_APP, params);
    }

    public static void logVisitedPageTabEvent(String screen) {
        Bundle params = new Bundle();
        params.putString(Constants.EVENT_PARAM_DATE_TIME, Functions.getCurrentDateTime());
        params.putString(Constants.EVENT_PARAM_USER_ID, getUserId());
        params.putString(Constants.FB_EVENT_PARAM_CONTENT_TYPE, screen);
        AppEventsLogger.newLogger(mContext).logEvent(Constants.FB_EVENT_NAME_VIEWED_CONTENT, params);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleManager.setLocale(base));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleManager.setLocale(this);
    }

    public static void getCityStateData(boolean isOnline) {
//        if (InternetConnection.checkConnectionForFasl(mContext) && isOnline) {
//            RetrofitClient.getInstance().getApi().getMasterCityData(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
//                    RongoApp.getAppVersion(), Functions.getDeviceid(mContext)).enqueue(new Callback<ResponseBody>() {
//                @Override
//                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                    if (response != null && response.isSuccessful()) {
//                        try {
//                            String r = response.body().string();
//                            if (!TextUtils.isEmpty(r)) {
//                                JSONObject jsonObject = new JSONObject(r);
//                                if (jsonObject != null && jsonObject.length() > 0) {
//                                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
//                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences,
//                                                Constants.SharedPreferences_StateCityData, jsonObject.toString().toString());
//                                    }
//                                }
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ResponseBody> call, Throwable t) {
//                }
//            });
//        } else {
            try {
                JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.getCityStateListJSONFromAsset(mContext));
                if (jsonObject != null && jsonObject.length() > 0) {
                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                        if (jsonObject != null && jsonObject.length() > 0) {
                            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                        Constants.SharedPreferences_StateCityData, jsonObject.toString().toString());
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
//        }
    }

    public static void getMasterCropData(boolean isOnline) {
        if (InternetConnection.checkConnectionForFasl(mContext) && isOnline) {
            RetrofitClient.getInstance().getApi().getMasterCropData(RongoApp.getSeasonName(),
                    RongoApp.getDefaultLanguage(), RongoApp.getAppVersion()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        try {
                            String r_str = response.body().string();
                            if (!TextUtils.isEmpty(r_str)) {
                                JSONObject jb = new JSONObject(r_str);
                                if (jb != null && jb.length() > 0) {
                                    if (jb.optString("status_code").equalsIgnoreCase("0")) {
                                        JSONObject jsonObject = jb.optJSONObject("data");
                                        if (jsonObject != null && jsonObject.length() > 0) {
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Master_Crop_List,
                                                    "" + jsonObject.optJSONArray("crops"));
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        } else {
            try {
                JSONObject jb = new JSONObject(FetchJsonFromAssetsFile.getAllCropListJSONFromAsset(mContext));
                if (jb != null && jb.length() > 0) {
                    if (jb.optString("status_code").equalsIgnoreCase("0")) {
                        JSONObject jsonObject = jb.optJSONObject("data");
                        if (jsonObject != null && jsonObject.length() > 0) {
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Master_Crop_List,
                                    "" + jsonObject.optJSONArray("crops"));
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void getMasterRewardPoints(boolean isOnline) {
        try {
            if (InternetConnection.checkConnectionForFasl(mContext) && isOnline) {
                RetrofitClient.getInstance().getApi().getMasterRewardPoints(RongoApp.getSeasonName(),
                        RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                        SharedPreferencesUtils.getUserId(mContext)).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response != null && response.isSuccessful()) {
                            try {
                                String r = response.body().string();
                                if (!TextUtils.isEmpty(r)) {
                                    JSONObject jsonObject = new JSONObject(r);
                                    if (jsonObject != null && jsonObject.length() > 0) {
                                        if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                            JSONObject data = jsonObject.optJSONObject("data");
                                            if (data != null && data.length() > 0) {
                                                JSONObject result = data.optJSONObject("result");
                                                if (result != null && result.length() > 0) {
                                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Reward, "" + result);
                                                }
                                            }
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
            } else {
                JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.getAllRewardListJSONFromAsset(mContext));
                if (jsonObject != null && jsonObject.length() > 0) {
                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                        JSONObject data = jsonObject.optJSONObject("data");
                        if (data != null && data.length() > 0) {
                            JSONObject result = data.optJSONObject("result");
                            if (result != null && result.length() > 0) {
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Reward, "" + result);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getAllCropWeeklyQuestion(boolean isOnline) {
        try {
            if (InternetConnection.checkConnectionForFasl(mContext) && isOnline) {
                RetrofitClient.getInstance().getApi().getWeeklyCropInfoQuestion(RongoApp.getSeasonName(),
                        RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), SharedPreferencesUtils.getUserId(mContext),
                        RongoApp.getSelectedPCrop()).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response != null && response.isSuccessful()) {
                            try {
                                String r = response.body().string();
                                if (!TextUtils.isEmpty(r)) {
                                    JSONObject jsonObj = new JSONObject(r);
                                    if (jsonObj != null && jsonObj.length() > 0) {
                                        if (jsonObj.optString("status_code").equalsIgnoreCase("0")) {
                                            JSONObject data = jsonObj.optJSONObject("data");
                                            if (data != null && data.length() > 0) {
                                                JSONObject resultData = data.optJSONObject("result");
                                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Weekly_Crop_Question, "" + resultData);
                                            }
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
            } else {
                JSONObject jb = new JSONObject(FetchJsonFromAssetsFile.getAllCropWeeklyQuestionJSONFromAsset(mContext));
                if (jb != null && jb.length() > 0) {
                    if (jb.optString("status_code").equalsIgnoreCase("0")) {
                        JSONObject data = jb.optJSONObject("data");
                        if (data != null && data.length() > 0) {
                            JSONObject resultData = data.optJSONObject("result");
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                    Constants.SharedPreferences_Weekly_Crop_Question, "" + resultData);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getFertilizerInfo(boolean isOnline) {
        try {
            if (InternetConnection.checkConnectionForFasl(mContext) && isOnline) {
                RetrofitClient.getInstance().getApi().getFertilizerInfo(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                        Functions.getDeviceid(mContext), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                        SharedPreferencesUtils.getUserId(mContext), RongoApp.getSelectedCrop()).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            if (response != null && response.isSuccessful()) {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                if (jsonObject != null && jsonObject.length() > 0) {
                                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                        JSONObject data = jsonObject.optJSONObject("data");
                                        if (data != null && data.length() > 0) {
                                            JSONObject jsonArray = data.optJSONObject("result");
                                            if (jsonArray != null && jsonArray.length() > 0) {
                                                SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                                        Constants.SharedPreferences_FertilizerInfo, "" + jsonArray);
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
            } else {
                JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.getFertilizerDataJSONFromAsset(mContext));
                if (jsonObject != null && jsonObject.length() > 0) {
                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                        JSONObject data = jsonObject.optJSONObject("data");
                        if (data != null && data.length() > 0) {
                            JSONObject jsonArray = data.optJSONObject("result");
                            if (jsonArray != null && jsonArray.length() > 0) {
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                        Constants.SharedPreferences_FertilizerInfo, "" + jsonArray);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getMasterDropDownData(boolean isOnline) {
        try {
            if (InternetConnection.checkConnectionForFasl(mContext) && isOnline) {
                RetrofitClient.getInstance().getApi().getMasterDropdownData(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                        Functions.getDeviceid(mContext), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                        SharedPreferencesUtils.getUserId(mContext)).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response != null && response.isSuccessful()) {
                            try {
                                String r_str = response.body().string();
                                if (!TextUtils.isEmpty(r_str)) {
                                    JSONObject jb = new JSONObject(r_str);
                                    if (jb != null && jb.length() > 0) {
                                        if (jb.optString("status_code").equalsIgnoreCase("0")) {

                                            JSONObject data = jb.optJSONObject("data");
                                            if (data != null && data.length() > 0) {
                                                JSONObject result = data.optJSONObject("result");
                                                if (result != null && result.length() > 0) {

                                                    JSONArray jsonArray = result.optJSONArray("rewards");
                                                    if (jsonArray != null && jsonArray.length() > 0) {
                                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                                                Constants.SharedPreferences_REWARD_LIST, "" + jsonArray);
                                                    }

                                                    JSONArray report_post_reason = result.optJSONArray("report_post_reason");
                                                    if (report_post_reason != null && report_post_reason.length() > 0) {
                                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                                                Constants.SharedPreferences_ReportPost, "" + report_post_reason);
                                                    }

                                                    JSONArray report_comment_reason = result.optJSONArray("report_comment_reason");
                                                    if (report_comment_reason != null && report_comment_reason.length() > 0) {
                                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                                                Constants.SharedPreferences_ReportComment, "" + report_comment_reason);
                                                    }

                                                    JSONObject seed_type = result.optJSONObject("seed_type");
                                                    List<SeedTypeData> seedTypeData = new ArrayList<>();
                                                    if (seed_type != null && seed_type.length() > 0) {
                                                        Iterator<String> keys = seed_type.keys();
                                                        while (keys.hasNext()) {
                                                            String key = keys.next();
                                                            String value = seed_type.optString(key);
                                                            seedTypeData.add(new SeedTypeData(key, value));
                                                        }
                                                        SharedPreferencesUtils.saveSeedTypes(mContext, seedTypeData, Constants.SharedPreferences_SEED_TYPES);
                                                    }

                                                    JSONObject problem_related = result.optJSONObject("problem_related");
                                                    List<ProblemRelatedData> optionsList = new ArrayList<>();
                                                    if (problem_related != null && problem_related.length() > 0) {
                                                        Iterator<String> keys = problem_related.keys();
                                                        while (keys.hasNext()) {
                                                            String key = keys.next();
                                                            String value = problem_related.optString(key);
                                                            optionsList.add(new ProblemRelatedData(key, value));
                                                        }
                                                        SharedPreferencesUtils.saveProblemRelated(mContext, optionsList, Constants.SharedPreferences_OPTIONS);
                                                    }

                                                    JSONObject crop_area_focused = result.optJSONObject("crop_area_focused");
                                                    final List<CropAreaFocusedData> cropAreaFocusedData = new ArrayList<>();
                                                    if (crop_area_focused != null && crop_area_focused.length() > 0) {
                                                        Iterator keys = crop_area_focused.keys();
                                                        while (keys.hasNext()) {
                                                            final String key = (String) keys.next();
                                                            JSONObject jsonObject = crop_area_focused.optJSONObject(key);

                                                            String text = jsonObject.optString("text");
                                                            String description = jsonObject.optString("description");
                                                            String image = jsonObject.optString("image");
                                                            cropAreaFocusedData.add(new CropAreaFocusedData(key, text, description, image));
                                                        }
                                                        SharedPreferencesUtils.saveCropArea(mContext, cropAreaFocusedData, Constants.SharedPreferences_CROP_FOCUSED_AREA);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
            } else {
                JSONObject jb = new JSONObject(FetchJsonFromAssetsFile.getMasterDropDownDataJSONFromAsset(mContext));
                if (jb != null && jb.length() > 0) {
                    if (jb.optString("status_code").equalsIgnoreCase("0")) {

                        JSONObject data = jb.optJSONObject("data");
                        if (data != null && data.length() > 0) {
                            JSONObject result1 = data.optJSONObject("result");
//                            Log.e("+++1++", "" + result1.optString(RongoApp.getDefaultLanguage()));

                            JSONObject result = new JSONObject(result1.optString(RongoApp.getDefaultLanguage()));
                            if (result != null && result.length() > 0) {

                                JSONArray jsonArray = result.optJSONArray("rewards");
                                if (jsonArray != null && jsonArray.length() > 0) {
                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                            Constants.SharedPreferences_REWARD_LIST, "" + jsonArray);
                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                            Constants.SharedPreferences_Pending_Reward, "" + jsonArray);
                                }

                                JSONObject seed_type = result.optJSONObject("seed_type");
                                List<SeedTypeData> seedTypeData = new ArrayList<>();
                                if (seed_type != null && seed_type.length() > 0) {
                                    Iterator<String> keys = seed_type.keys();
                                    while (keys.hasNext()) {
                                        String key = keys.next();
                                        String value = seed_type.optString(key);
                                        seedTypeData.add(new SeedTypeData(key, value));
                                    }
                                    SharedPreferencesUtils.saveSeedTypes(mContext, seedTypeData, Constants.SharedPreferences_SEED_TYPES);
                                }

                                JSONObject problem_related = result.optJSONObject("problem_related");
                                List<ProblemRelatedData> optionsList = new ArrayList<>();
                                if (problem_related != null && problem_related.length() > 0) {
                                    Iterator<String> keys = problem_related.keys();
                                    while (keys.hasNext()) {
                                        String key = keys.next();
                                        String value = problem_related.optString(key);
                                        optionsList.add(new ProblemRelatedData(key, value));
                                    }
                                    SharedPreferencesUtils.saveProblemRelated(mContext, optionsList, Constants.SharedPreferences_OPTIONS);
                                }

                                JSONObject crop_area_focused = result.optJSONObject("crop_area_focused");
                                final List<CropAreaFocusedData> cropAreaFocusedData = new ArrayList<>();
                                if (crop_area_focused != null && crop_area_focused.length() > 0) {
                                    Iterator keys = crop_area_focused.keys();
                                    while (keys.hasNext()) {
                                        final String key = (String) keys.next();
                                        JSONObject jsonObject = crop_area_focused.optJSONObject(key);

                                        final String text = jsonObject.optString("text");
                                        final String description = jsonObject.optString("description");
                                        final String image = jsonObject.optString("image");
                                        cropAreaFocusedData.add(new CropAreaFocusedData(key, text, description, image));
                                    }
                                    SharedPreferencesUtils.saveCropArea(mContext, cropAreaFocusedData, Constants.SharedPreferences_CROP_FOCUSED_AREA);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getSessionEndData() {
        try {
            if (InternetConnection.checkConnectionForFasl(mContext)) {
                RetrofitClient.getInstance().getApi().getSeasonEndSummary(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                        SharedPreferencesUtils.getUserId(mContext), sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                        sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""), sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                        sharedPreferences.getString(Constants.SharedPreferences_Field_ID, "")).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response != null && response.isSuccessful()) {
                            try {
                                String r = response.body().string();
                                if (!TextUtils.isEmpty(r)) {
                                    JSONObject jsonObject = new JSONObject(r);
                                    if (jsonObject != null && jsonObject.length() > 0) {
                                        if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                            JSONObject data = jsonObject.optJSONObject("data");
                                            if (data != null && data.length() > 0) {
                                                JSONObject resultData = data.optJSONObject("result");
                                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Session_End_Data, "" + resultData);
                                            }
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
            } else {
                JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.getSessionEndDataJSONFromAsset(mContext));
                if (jsonObject != null && jsonObject.length() > 0) {
                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                        JSONObject data = jsonObject.optJSONObject("data");
                        if (data != null && data.length() > 0) {
                            JSONObject resultData = data.optJSONObject("result");
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Session_End_Data, "" + resultData);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getPragatiData() {
        try {
            if (InternetConnection.checkConnectionForFasl(mContext)) {
                RetrofitClient.getInstance().getApi().getCropPragatiTimeline(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                        SharedPreferencesUtils.getUserId(mContext), sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                        Functions.getCommaSepPCropId(sharedPreferences)).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response != null && response.isSuccessful()) {
                            try {
                                String r = response.body().string();
                                if (!TextUtils.isEmpty(r)) {
                                    JSONObject jsonObject = new JSONObject(r);
                                    if (jsonObject != null && jsonObject.length() > 0) {
                                        if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                            JSONObject data = jsonObject.optJSONObject("data");
                                            if (data != null && data.length() > 0) {
                                                JSONObject resultData = data.optJSONObject("result");
                                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Pragati, "" + resultData);
                                            }
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
            } else {
                JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.getPragatiDataJSONFromAsset(mContext));
                if (jsonObject != null && jsonObject.length() > 0) {
                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                        JSONObject data = jsonObject.optJSONObject("data");
                        if (data != null && data.length() > 0) {
                            JSONObject resultData = data.optJSONObject("result");
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Pragati, "" + resultData);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getDailyQuizData(boolean isOnline) {
        try {
            if (InternetConnection.checkConnectionForFasl(mContext) && isOnline) {
                RetrofitClient.getInstance().getApi().getAllDailyQuiz(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                        SharedPreferencesUtils.getUserId(mContext)).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response != null && response.isSuccessful()) {
                            try {
                                String r = response.body().string();
                                if (!TextUtils.isEmpty(r)) {
                                    JSONObject jsonObject = new JSONObject(r);
                                    if (jsonObject != null && jsonObject.length() > 0) {
                                        if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                            JSONObject data = jsonObject.optJSONObject("data");
                                            if (data != null && data.length() > 0) {
                                                JSONObject jsonArray = data.optJSONObject("result");
                                                if (jsonArray != null && jsonArray.length() > 0) {
                                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Quiz, "" + jsonArray);
                                                }
                                            }
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
            } else {
                JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.getDailyQuizDataJSONFromAsset(mContext));
                if (jsonObject != null && jsonObject.length() > 0) {
                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                        JSONObject data = jsonObject.optJSONObject("data");
                        if (data != null && data.length() > 0) {
                            JSONObject jsonArray = data.optJSONObject("result");
                            if (jsonArray != null && jsonArray.length() > 0) {
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Quiz, "" + jsonArray);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getWeeklyWeather() {
        if (InternetConnection.checkConnectionForFasl(mContext)) {
            RetrofitClient.getInstance().getApi().getWeeklyWeatherData(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    SharedPreferencesUtils.getUserId(mContext), sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                    sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0")).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        try {
                            String r = response.body().string();
                            if (!TextUtils.isEmpty(r)) {
                                JSONObject jsonObject = new JSONObject(r);
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                        Constants.SharedPreferences_WeeklyWeatherData, jsonObject.toString().toString());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        }
    }

    public static void getSurveyQuestions() {
        if (InternetConnection.checkConnectionForFasl(mContext)) {
            List<SurveyQuestions> surveyQuestions = new ArrayList<>();
            RetrofitClient.getInstance().getApi().getSurveyQuestions(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    Functions.getDeviceid(mContext), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                    SharedPreferencesUtils.getUserId(mContext)).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response != null && response.isSuccessful()) {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject data = jsonObject.optJSONObject("data");
                                    if (data != null && data.length() > 0) {
                                        JSONObject result = data.optJSONObject("result");
                                        surveyQuestions.clear();
                                        if (result != null && result.length() > 0) {
                                            Iterator<String> keys = result.keys();
                                            while (keys.hasNext()) {
                                                String key = keys.next();
                                                JSONObject value = result.optJSONObject(key);

                                                String id = "0";
                                                id = value.optString("id");
                                                int keyValue = Integer.parseInt((id.equalsIgnoreCase("")) ? "0" : id);
                                                String question = value.optString("question");
                                                String field_type = value.optString("field_type");

                                                JSONArray jsonArray = value.optJSONArray("options");
                                                List<SurveyQuestionItemData> surveyQuestionItemData = new ArrayList<>();
                                                List<String> optionsLis = new ArrayList<>();
                                                if (jsonArray != null && jsonArray.length() > 0) {
                                                    for (int i = 0; i < jsonArray.length(); i++) {
                                                        optionsLis.add(jsonArray.optString(i));
                                                    }
                                                }
                                                surveyQuestionItemData.add(new SurveyQuestionItemData(0, id, question, field_type, optionsLis));
                                                surveyQuestions.add(new SurveyQuestions(0, keyValue, surveyQuestionItemData));
                                            }
                                        }

                                        appDatabase.surveyQuestionsDao().nukeTable();
                                        appDatabase.surveyQuestionsDao().insertAll(surveyQuestions);
                                        SharedPreferencesUtils.saveSurveyQuestions(mContext, surveyQuestions, "survey");
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        }
    }

    public static void getWeeklyAdvisory() {
        if (InternetConnection.checkConnectionForFasl(mContext)) {
            RetrofitClient.getInstance().getApi().getWeeklyAdvisory(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    Functions.getDeviceid(mContext), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                    SharedPreferencesUtils.getUserId(mContext), sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                    sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0")).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        try {
                            String r = response.body().string();
                            if (!TextUtils.isEmpty(r)) {
                                JSONObject jsonObject = new JSONObject(r);
                                if (jsonObject.length() > 0) {
                                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                        JSONObject data = jsonObject.optJSONObject("data");
                                        if (data != null && data.length() > 0) {
                                            JSONObject result = data.optJSONObject("result");
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                                    Constants.SharedPreferences_Weekly_Advisory, "" + result);
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        }
    }

    public static void getPosts() {
        if (InternetConnection.checkConnectionForFasl(mContext)) {
            RetrofitClient.getInstance().getApi().getPosts(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                    RongoApp.getAppVersion(), Functions.getDeviceid(mContext),
                    sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                    SharedPreferencesUtils.getUserId(mContext)).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        try {
                            String str = response.body().string();
                            if (!TextUtils.isEmpty(str)) {
                                JSONObject jsonObject = new JSONObject(str);
                                if (jsonObject != null && jsonObject.length() > 0) {
                                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                        JSONObject data = jsonObject.optJSONObject("data");
                                        if (data != null && data.length() > 0) {
                                            JSONObject result = data.optJSONObject("result");
                                            if (result != null && result.length() > 0) {
                                                try {
                                                    String banner_image = result.optString("banner_image");
                                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, "bannerImage", banner_image);
                                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_AUDIO_PATH, jsonObject.optString("audio_path"));

                                                    JSONArray jsonArray = result.optJSONArray("posts");
                                                    if (jsonArray != null && jsonArray.length() > 0) {
                                                        List<PostData> postData = Functions.getPostData(mContext, jsonArray);
                                                        appDatabase.postDao().nukeTable();
                                                        appDatabase.postDao().insertAll(postData);
                                                    }
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        }
    }

    public static void getContentData() {
        if (InternetConnection.checkConnectionForFasl(mContext)) {
            RetrofitClient.getInstance().getApi().getContentData(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                    RongoApp.getAppVersion(), Functions.getDeviceid(mContext),
                    SharedPreferencesUtils.getUserId(mContext)).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        String str = response.body().string();
                        if (!TextUtils.isEmpty(str)) {
                            JSONObject jsonObject = new JSONObject(str);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                JSONObject data = jsonObject.optJSONObject("data");
                                if (data != null && data.length() > 0) {
                                    JSONObject result = data.optJSONObject("result");
                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_internet_radio, "" + result.optString("internet_radio"));
                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Referral_Image, "" + result.optString("referral_image"));
                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Community_Banner, "" + result.optString("community_banner"));
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        }
    }

    public static void getContestData() {
        if (InternetConnection.checkConnectionForFasl(mContext)) {
            RetrofitClient.getInstance().getApi().getContestData(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    SharedPreferencesUtils.getUserId(mContext), Functions.getDeviceid(mContext), sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                    sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0")).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        try {
                            String r = response.body().string();
                            if (!TextUtils.isEmpty(r)) {
                                JSONObject jsonObject = new JSONObject(r);
                                if (jsonObject != null && jsonObject.length() > 0) {
                                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                        JSONObject data = jsonObject.optJSONObject("data");
                                        if (data != null && data.length() > 0) {
                                            JSONArray jsonArray = data.optJSONArray("result");
                                            if (jsonArray != null && jsonArray.length() > 0) {
                                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_CONTEST, "" + jsonArray);
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        }
    }

    public static void getUserStories() {
        try {
            if (InternetConnection.checkConnectionForFasl(mContext)) {
                RetrofitClient.getInstance().getApi().getUserStories(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                        RongoApp.getAppVersion(), RongoApp.getUserId(), "all").enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            if (response != null && response.isSuccessful()) {
                                String r = response.body().string();
                                if (!TextUtils.isEmpty(r)) {
                                    JSONObject jsonObject = new JSONObject(r);
                                    if (jsonObject != null && jsonObject.length() > 0) {
                                        if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                            JSONObject dataObject = jsonObject.optJSONObject("data");
                                            if (dataObject != null && dataObject.length() > 0) {
                                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_story_data, "" + dataObject);
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getProfile() {
        if (InternetConnection.checkConnectionForFasl(mContext)) {
            RetrofitClient.getInstance().getApi().getProfile(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    Functions.getDeviceid(mContext), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                    SharedPreferencesUtils.getUserId(mContext)).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        try {
                            String r_str = response.body().string();
                            if (!TextUtils.isEmpty(r_str)) {
                                JSONObject jb = new JSONObject(r_str);
                                if (jb != null && jb.length() > 0) {
                                    JSONObject jData = jb.optJSONObject("data");
                                    if (jData != null && jData.length() > 0) {
                                        JSONObject jresult = jData.optJSONObject("result");
                                        if (jresult != null && jresult.length() > 0) {
                                            JSONObject jprofile = jresult.optJSONObject("profile");
                                            if (jprofile != null) {
                                                new InsertHomeFeedData(jb).insertData();
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        }
    }

    public static void updateWeeklyCropInfo(EditText editTextSowingDate) {
        String infoType = "sowing", infoText = mContext.getResources().getString(R.string.str_buvay), infoValue = "", metaData = mContext.getResources().getString(R.string.str_ha);
        long dTime = Functions.covertToMili("dd-MM-yyyy", editTextSowingDate.getText().toString().trim());
        String date = Functions.converOneDateFormatToOther("dd-MM-yyyy", "yyyy-MM-dd hh:mm:ss", editTextSowingDate.getText().toString().trim());
        if (System.currentTimeMillis() > dTime) {
            metaData = mContext.getResources().getString(R.string.str_ha);
        } else {
            metaData = mContext.getResources().getString(R.string.str_na);
        }

        RetrofitClient.getInstance().getApi().updateWeeklyCropInfo(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                RongoApp.getAppVersion(), Functions.getDeviceid(mContext), SharedPreferencesUtils.getUserId(mContext),
                sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_Field_ID, ""),
                infoType, infoText, date, metaData).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    public static void engagementToolActivity(String action) {
        if (InternetConnection.checkConnectionForFasl(mContext)) {
            RetrofitClient.getInstance().getApi().engagementToolActivity(getSeasonName(), getDefaultLanguage(),
                    getAppVersion(), getUserId(), action).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        }
    }

    public static void reportUserContent(String option, String type, String action) {
        if (InternetConnection.checkConnectionForFasl(mContext)) {
            RetrofitClient.getInstance().getApi().reportUserContent(getSeasonName(), getDefaultLanguage(),
                    getAppVersion(), getUserId(), option, type, action).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        }
    }


    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        this.isConnected = isConnected;
        Log.e("onInternetChanged", "" + isConnected);
        if (isConnected) {
            if (!TextUtils.isEmpty(RongoApp.getUserId())) {
                isTrackAPI = true;
                setTrackOffline();
                if (SystemClock.elapsedRealtime() < 0) {
                    return;
                }
            }
        }
    }

    public void setTrackOffline() {
        try {
            if (isTrackAPI) {
                List<TrackUserOfflineData> trackUserOfflineData = new ArrayList<TrackUserOfflineData>();
                trackUserOfflineData = appDatabase.trackUserOfflineDao().getAll();
                if (trackUserOfflineData != null && trackUserOfflineData.size() > 0) {
                    Log.e("App trackUser Size", "" + trackUserOfflineData.size());
                    offlineTrackUpdateAPI(trackUserOfflineData);
                    isTrackAPI = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void offlineTrackUpdateAPI(final List<TrackUserOfflineData> trackUserOfflineData) {
        isTrackAPI = false;
        final int[] i = {0};

        RetrofitClient.getInstance().getApi().updateAppActivity(RongoApp.getSeasonName(), getDefaultLanguage(), getAppVersion(), trackUserOfflineData.get(i[0]).getUserId(),
                trackUserOfflineData.get(i[0]).getLastLoginTime(), trackUserOfflineData.get(i[0]).getAppTimeSpent(),
                "OFFLINE", "0", trackUserOfflineData.get(i[0]).isAction(),
                trackUserOfflineData.get(i[0]).getUserTrackEvent(), "").enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
//                        Log.e("App delete Track id", "" + trackUserOfflineData.get(i[0]).getUid());
                        appDatabase.trackUserOfflineDao().delete(trackUserOfflineData.get(i[0]).getUid());

                        trackUserOfflineData.remove(i[0]);
                        if (trackUserOfflineData.size() > 0) {
                            offlineTrackUpdateAPI(trackUserOfflineData);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }


    public static int getCropWeekDaysFunction() {

        if (Functions.doubleTapCheck(mLastLongClick)) {
            return Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0"));
        }
        mLastLongClick = SystemClock.elapsedRealtime();

        int cropWeek = 0;
        String cropWeekDay = "0";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
            Date c = Calendar.getInstance().getTime();

            List<CropsData> cropsData = Functions.getCropData(appDatabase, sharedPreferences);
            if (cropsData != null && cropsData.size() > 0) {
                String SowingDate = cropsData.get(0).getDate_of_sowing();
                if (!TextUtils.isEmpty(SowingDate) && cropsData.get(0).getIs_sowing_done().equalsIgnoreCase("yes")) {
                    if (SowingDate.equalsIgnoreCase("0000-00-00 00:00:00")) {
                        if (TextUtils.isEmpty(cropsData.get(0).getCrop_in_weeks())) {
                            cropWeek = 0;
                        } else {
                            if (!cropsData.get(0).getCrop_in_weeks().equalsIgnoreCase("null")) {
                                cropWeek = Integer.parseInt(cropsData.get(0).getCrop_in_weeks());
                            } else {
                                cropWeek = 0;
                            }
                        }
                        Log.d("App Profile0", "crop_in_weeks: " + cropWeek);
                    } else {
                        Log.d("App", "SowingDate: " + SowingDate);
                        Date date1 = dateFormat.parse(SowingDate);
                        Date date2 = dateFormat.parse(dateFormat.format(c.getTime()));

                        long crop_in_days = date2.getTime() - date1.getTime();
//                        Log.d("App", "crop_in_days: " + crop_in_days);

                        float daysBetween = (crop_in_days / (1000 * 60 * 60 * 24));
                        cropWeek = (int) Math.ceil(daysBetween / 7);
//                        Log.e("App cropWeek", "" + cropWeek);

                        long daysDiff = (crop_in_days / (60 * 60 * 24 * 1000));
//                        Log.d("App daysDiff", "" + daysDiff);

                        cropWeekDay = String.valueOf((daysDiff % 7) + 1);
                        Log.e("App0 ====", "" + cropWeekDay);
                    }
                } else {
                    cropWeek = 0;
                }
            } else {
                List<AddCropStatusData> addCropStatus = appDatabase.addCropStatusDao().getAllData(RongoApp.getSelectedCrop());
                if (addCropStatus.size() > 0) {
                    String SowingDate = addCropStatus.get(0).getSowingDate();
                    if (!TextUtils.isEmpty(SowingDate) && addCropStatus.get(0).getSowingDone().equalsIgnoreCase("yes")) {
                        Log.d("App", "SowingDate: " + SowingDate);
                        Date date1 = dateFormat.parse(SowingDate);
                        Date date2 = dateFormat.parse(dateFormat.format(c.getTime()));

                        long crop_in_days = date2.getTime() - date1.getTime();
//                        Log.d("App", "crop_in_days: " + crop_in_days);

                        float daysBetween = (crop_in_days / (1000 * 60 * 60 * 24));
                        cropWeek = (int) Math.ceil(daysBetween / 7);
//                        Log.e("App cropWeek", "" + cropWeek);

                        long daysDiff = (crop_in_days / (60 * 60 * 24 * 1000));
//                        Log.d("App daysDiff", "" + daysDiff);

                        cropWeekDay = String.valueOf((daysDiff % 7) + 1);
                        Log.e("App0 ====", "" + cropWeekDay);
                    }
                }
            }

//            Log.e("App end0 cropWeek", "---" + sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, ""));
            if (cropWeek >= Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"))) {
                cropWeek = Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"));
            }

//            Log.e("App final0 cropWeek", "" + cropWeek);
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_CROP_WEEK_DAY, cropWeekDay + "");
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_CROP_WEEK, cropWeek + "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cropWeek;
    }

    public static int getCropWeeksFunction() {
        int cropWeek = 0;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
            Date c = Calendar.getInstance().getTime();

            List<CropsData> cropsData = Functions.getCropData(appDatabase, sharedPreferences);
            if (cropsData != null && cropsData.size() > 0) {
                String SowingDate = cropsData.get(0).getDate_of_sowing();
                if (!TextUtils.isEmpty(SowingDate) && cropsData.get(0).getIs_sowing_done().equalsIgnoreCase("yes")) {
                    if (SowingDate.equalsIgnoreCase("0000-00-00 00:00:00")) {
                        if (TextUtils.isEmpty(cropsData.get(0).getCrop_in_weeks())) {
                            cropWeek = 0;
                        } else {
                            if (!cropsData.get(0).getCrop_in_weeks().equalsIgnoreCase("null")) {
                                cropWeek = Integer.parseInt(cropsData.get(0).getCrop_in_weeks());
                            } else {
                                cropWeek = 0;
                            }
                        }
                        Log.d("App Profile", "crop_in_weeks: " + cropWeek);
                    } else {
                        Log.d("App", "SowingDate: " + SowingDate);
                        Date date1 = dateFormat.parse(SowingDate);
                        Date date2 = dateFormat.parse(dateFormat.format(c.getTime()));

                        long crop_in_days = date2.getTime() - date1.getTime();
//                        Log.d("App", "crop_in_days: " + crop_in_days);

                        float daysBetween = (crop_in_days / (1000 * 60 * 60 * 24));
                        cropWeek = (int) Math.ceil(daysBetween / 7);
                        Log.e("App cropWeek", "" + cropWeek);
                    }
                } else {
                    cropWeek = 0;
                }
            } else {
                List<AddCropStatusData> addCropStatus = appDatabase.addCropStatusDao().getAllData(RongoApp.getSelectedCrop());
                if (addCropStatus.size() > 0) {
                    if (!TextUtils.isEmpty(addCropStatus.get(0).getSowingDate()) && addCropStatus.get(0).getSowingDone().equalsIgnoreCase("yes")) {
                        Log.d("App", "SowingDate: " + addCropStatus.get(0).getSowingDate());
                        Date date1 = dateFormat.parse(addCropStatus.get(0).getSowingDate());
                        Date date2 = dateFormat.parse(dateFormat.format(c.getTime()));
                        long crop_in_days = date2.getTime() - date1.getTime();
//                        Log.d("App", "crop_in_days: " + crop_in_days);
                        float daysBetween = (crop_in_days / (1000 * 60 * 60 * 24));
                        cropWeek = (int) Math.ceil(daysBetween / 7);
                        Log.e("App cropWeek", "" + cropWeek);
                    }
                }
            }

//            Log.e("App end1 cropWeek", "" + sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, ""));
            if (cropWeek >= Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"))) {
                cropWeek = Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"));
            }
//            Log.e("App final1 cropWeek", "" + cropWeek);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cropWeek;
    }

    public static int getOriginalCropWeeksFunction() {
        int cropWeek = 0;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
            Date c = Calendar.getInstance().getTime();

            List<CropsData> cropsData = Functions.getCropData(appDatabase, sharedPreferences);
            if (cropsData != null && cropsData.size() > 0) {
                String SowingDate = cropsData.get(0).getDate_of_sowing();
                if (!TextUtils.isEmpty(SowingDate) && cropsData.get(0).getIs_sowing_done().equalsIgnoreCase("yes")) {
                    if (SowingDate.equalsIgnoreCase("0000-00-00 00:00:00")) {
                        if (TextUtils.isEmpty(cropsData.get(0).getCrop_in_weeks())) {
                            cropWeek = 0;
                        } else {
                            if (!cropsData.get(0).getCrop_in_weeks().equalsIgnoreCase("null")) {
                                cropWeek = Integer.parseInt(cropsData.get(0).getCrop_in_weeks());
                            } else {
                                cropWeek = 0;
                            }
                        }
                        Log.d("App Profile", "crop_in_weeks: " + cropWeek);
                    } else {
                        Log.d("App", "SowingDate: " + SowingDate);
                        Date date1 = dateFormat.parse(SowingDate);
                        Date date2 = dateFormat.parse(dateFormat.format(c.getTime()));

                        long crop_in_days = date2.getTime() - date1.getTime();
//                        Log.d("App", "crop_in_days: " + crop_in_days);

                        float daysBetween = (crop_in_days / (1000 * 60 * 60 * 24));
                        cropWeek = (int) Math.ceil(daysBetween / 7);
                        Log.e("App cropWeek", "" + cropWeek);
                    }
                } else {
                    cropWeek = 0;
                }
            } else {
                List<AddCropStatusData> addCropStatus = appDatabase.addCropStatusDao().getAllData(RongoApp.getSelectedCrop());
                if (addCropStatus.size() > 0) {
                    String SowingDate = addCropStatus.get(0).getSowingDate();
                    if (!TextUtils.isEmpty(SowingDate) && addCropStatus.get(0).getSowingDone().equalsIgnoreCase("yes")) {
                        Log.d("App", "SowingDate: " + SowingDate);
                        Date date1 = dateFormat.parse(SowingDate);
                        Date date2 = dateFormat.parse(dateFormat.format(c.getTime()));
                        long crop_in_days = date2.getTime() - date1.getTime();
                        Log.d("App", "crop_in_days: " + crop_in_days);
                        float daysBetween = (crop_in_days / (1000 * 60 * 60 * 24));
                        cropWeek = (int) Math.ceil(daysBetween / 7);
                        Log.e("App cropWeek", "" + cropWeek);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cropWeek;
    }


    public static void getSeasonNameFunction() {
        try {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            int dayOfMonth = calendar.get(Calendar.MONTH);
            Log.i("####00", "" + dayOfMonth);

            String seasonName = Constants.KHARIF_SEASON;
            List<CropsData> cropsData = Functions.getCropData(appDatabase, sharedPreferences);
            if (cropsData != null && cropsData.size() > 0) {
//                Log.d("####0", "" + cropsData.size() + " - " + cropsData.get(0).getCrop_id());

                seasonName = Functions.getSeasonName(cropsData.get(0).getCrop_id());

                String SowingDate = cropsData.get(0).getDate_of_sowing();
                if (!TextUtils.isEmpty(SowingDate) && cropsData.get(0).getIs_sowing_done().equalsIgnoreCase("yes")) {
                    if (SowingDate.equalsIgnoreCase("0000-00-00 00:00:00")) {
                        Log.d("####1", "" + SowingDate);
                    } else {
                        Log.d("####2", "" + SowingDate);
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
                        Date date1 = dateFormat.parse(SowingDate);
                        Log.d("####3", "" + date1);
                        Calendar c = Calendar.getInstance();
                        c.setTime(date1);
                        dayOfMonth = c.get(Calendar.MONTH);
                        Log.d("####4", "" + dayOfMonth);

                        List<String> kharif = new ArrayList<String>(Arrays.asList(mContext.getResources().getStringArray(R.array.Kharif)));
                        List<String> rabi = new ArrayList<String>(Arrays.asList(mContext.getResources().getStringArray(R.array.Rabi)));
                        if (kharif.contains(String.valueOf(dayOfMonth))) {
                            seasonName = Constants.KHARIF_SEASON;
                        }
                        if (rabi.contains(String.valueOf(dayOfMonth))) {
                            seasonName = Constants.RABI_SEASON;
                        }

                    }
                }
            } else {
                List<AddCropStatusData> addCropStatus = appDatabase.addCropStatusDao().getAllData(RongoApp.getSelectedCrop());
                if (addCropStatus.size() > 0) {
                    seasonName = Functions.getSeasonName(addCropStatus.get(0).getCropId());

                    String SowingDate = addCropStatus.get(0).getSowingDate();
                    if (!TextUtils.isEmpty(SowingDate) && addCropStatus.get(0).getSowingDone().equalsIgnoreCase("yes")) {
                        if (SowingDate.equalsIgnoreCase("0000-00-00 00:00:00")) {
                            Log.d("####1", "" + SowingDate);
                        } else {
                            Log.d("####2", "" + SowingDate);
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
                            Date date1 = dateFormat.parse(SowingDate);
                            Log.d("####3", "" + date1);
                            Calendar c = Calendar.getInstance();
                            c.setTime(date1);
                            dayOfMonth = c.get(Calendar.MONTH);
                            Log.d("####4", "" + dayOfMonth);

                            List<String> kharif = new ArrayList<String>(Arrays.asList(mContext.getResources().getStringArray(R.array.Kharif)));
                            List<String> rabi = new ArrayList<String>(Arrays.asList(mContext.getResources().getStringArray(R.array.Rabi)));
                            if (kharif.contains(String.valueOf(dayOfMonth))) {
                                seasonName = Constants.KHARIF_SEASON;
                            }
                            if (rabi.contains(String.valueOf(dayOfMonth))) {
                                seasonName = Constants.RABI_SEASON;
                            }

                        }
                    }
                }
            }
            Log.i("####5", "" + seasonName);
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, seasonName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
