package com.rongoapp.controller;

import android.app.Activity;

/*
*  new BackgroundTask(MainActivity.this) {
        @Override
        public void doInBackground() {

            //put you background code
            //same like doingBackground
            //Background Thread
        }

        @Override
        public void onPostExecute() {

            //hear is result part same
            //same like post execute
            //UI Thread(update your UI widget)
        }
    }.execute();
* */

public abstract class BackgroundTask {

    private Activity activity;

    public BackgroundTask(Activity activity) {
        this.activity = activity;
    }

    private void startBackground() {
        new Thread(new Runnable() {
            public void run() {
                doInBackground();
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        onPostExecute();
                    }
                });
            }
        }).start();
    }

    public void execute() {
        startBackground();
    }

    public abstract void doInBackground();

    public abstract void onPostExecute();

}