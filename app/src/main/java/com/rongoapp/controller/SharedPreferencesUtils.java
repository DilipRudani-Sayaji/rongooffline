package com.rongoapp.controller;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rongoapp.data.CropAreaFocusedData;
import com.rongoapp.data.ProblemRelatedData;
import com.rongoapp.data.SeedTypeData;
import com.rongoapp.data.StageData;
import com.rongoapp.data.SurveyQuestions;

import java.lang.reflect.Type;
import java.util.List;

public class SharedPreferencesUtils {

    public static void storeStateOfString(SharedPreferences mediaPrefs, String prefsKeys, String prefsValue) {
        SharedPreferences.Editor prefEditor = mediaPrefs.edit();
        prefEditor.putString(prefsKeys, prefsValue);
        prefEditor.commit();
    }

    public static void storeState(SharedPreferences mediaPrefs, String prefsKeys, int prefsValue) {
        SharedPreferences.Editor prefEditor = mediaPrefs.edit();
        prefEditor.putInt(prefsKeys, prefsValue);
        prefEditor.commit();
    }

    public static void storeStateLong(SharedPreferences mediaPrefs, String prefsKeys, long prefsValue) {
        SharedPreferences.Editor prefEditor = mediaPrefs.edit();
        prefEditor.putLong(prefsKeys, prefsValue);
        prefEditor.commit();
    }


    public static void logout(SharedPreferences mediaPrefs) {
        SharedPreferences.Editor prefEditor = mediaPrefs.edit();
        prefEditor.putString(Constants.SharedPreference_UserId, "");
        prefEditor.commit();
    }

    public static String getUserId(Context activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(Constants.MediaPrefs, 0);
        return sharedPreferences.getString(Constants.SharedPreference_UserId, "");
    }

    public static void saveList(Context c, List<String> list, String key) {
        SharedPreferences prefs = c.getSharedPreferences(Constants.MediaPrefs, 0);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.commit();
    }

    public static List<String> getList(Context c, String key) {
        SharedPreferences prefs = c.getSharedPreferences(Constants.MediaPrefs, 0);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<List<String>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


    public static void saveArrayList(Context c, List<String> list, String key) {
        SharedPreferences prefs = c.getSharedPreferences(Constants.MediaPrefs, 0);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.commit();
    }

    public static List<String> getArrayList(Context c, String key) {
        SharedPreferences prefs = c.getSharedPreferences(Constants.MediaPrefs, 0);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<List<String>>() {}.getType();
        return gson.fromJson(json, type);
    }

    public static void saveSurveyQuestions(Context c, List<SurveyQuestions> list, String key) {
        SharedPreferences prefs = c.getSharedPreferences(Constants.MediaPrefs, 0);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.commit();
    }

    public static List<SurveyQuestions> getSurveeyQuestions(Context c, String key) {
        SharedPreferences prefs = c.getSharedPreferences(Constants.MediaPrefs, 0);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<List<SurveyQuestions>>() {
        }.getType();
        return gson.fromJson(json, type);
    }


    public static void saveCropArea(Context c, List<CropAreaFocusedData> list, String key) {
        SharedPreferences prefs = c.getSharedPreferences(Constants.MediaPrefs, 0);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.commit();
    }

    public static List<CropAreaFocusedData> getCropArea(Context c, String key) {
        SharedPreferences prefs = c.getSharedPreferences(Constants.MediaPrefs, 0);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<List<CropAreaFocusedData>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static void saveSeedTypes(Context c, List<SeedTypeData> list, String key) {
        SharedPreferences prefs = c.getSharedPreferences(Constants.MediaPrefs, 0);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.commit();
    }

    public static List<SeedTypeData> getSeedTypes(Context c, String key) {
        SharedPreferences prefs = c.getSharedPreferences(Constants.MediaPrefs, 0);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<List<SeedTypeData>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static void saveProblemRelated(Context c, List<ProblemRelatedData> list, String key) {
        SharedPreferences prefs = c.getSharedPreferences(Constants.MediaPrefs, 0);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.commit();
    }

    public static List<ProblemRelatedData> getProblemRelated(Context c, String key) {
        SharedPreferences prefs = c.getSharedPreferences(Constants.MediaPrefs, 0);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<List<ProblemRelatedData>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public static void saveStageData(Context c, List<StageData> list, String key) {
        SharedPreferences prefs = c.getSharedPreferences(Constants.MediaPrefs, 0);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.commit();
    }

    public static List<StageData> getStageData(Context c, String key) {
        SharedPreferences prefs = c.getSharedPreferences(Constants.MediaPrefs, 0);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<List<StageData>>() {
        }.getType();
        return gson.fromJson(json, type);
    }
}
