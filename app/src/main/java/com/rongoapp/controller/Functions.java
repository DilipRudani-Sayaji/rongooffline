package com.rongoapp.controller;

import static com.rongoapp.controller.RongoApp.mAppOpenData;
import static com.rongoapp.controller.RongoApp.mContext;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.os.StrictMode;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.provider.Settings;
import android.provider.Telephony;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.text.HtmlCompat;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rongoapp.R;
import com.rongoapp.activities.HomeActivity;
import com.rongoapp.data.CropWeekDetails;
import com.rongoapp.data.CropsData;
import com.rongoapp.data.DailyRewardData;
import com.rongoapp.data.MasterCropData;
import com.rongoapp.data.PostData;
import com.rongoapp.data.ProfileOfflineData;
import com.rongoapp.data.ProfileResultData;
import com.rongoapp.data.RewardData;
import com.rongoapp.data.SeasonData;
import com.rongoapp.data.StateCityDataModel;
import com.rongoapp.data.TrackUserData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.utilities.AlarmReceiver;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.CustomSnackbar;
import com.rongoapp.views.CustomSnackbarGreen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class Functions {

//    Object json = new JSONTokener(weeklyWeather.toString()).nextValue();
//                if (json instanceof JSONObject) {
//        Log.e("json", "JSONObject");
//    } else if (json instanceof JSONArray) {
//        Log.e("json", "JSONArray");
//    }

    // To check if server is reachable
    public static boolean isServerReachable() {
        try {
            InetAddress.getByName("rongoapp.in").isReachable(3000);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static RequestBody getRequestFile(File file) {
        RequestBody fbody = RequestBody.create(file, MediaType.parse("image/*"));
        return fbody;
    }

    public static File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        String mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public static String replaceAudioLinkString(String string) {
        string = string.replace("[", "").replace("]", "");
        string = string.replace("\\/", "/");
        return string;
    }

    public static String getKeyName(SharedPreferences sharedPreferences, int position, String infoType) {
        String keyName = "";
        try {
            JSONObject WeeklyCropInfo = new JSONObject(RongoApp.getWeeklyCropQuestion());
            if (!TextUtils.isEmpty(WeeklyCropInfo.toString())) {
                JSONObject quesCrop = WeeklyCropInfo.optJSONObject(RongoApp.getSelectedCrop());
                JSONObject quesSeason = quesCrop.optJSONObject(RongoApp.getSeasonName());
                JSONArray jsonObjct = quesSeason.optJSONArray(RongoApp.getDefaultLanguage());
                JSONObject object = jsonObjct.optJSONObject(0);
//                Log.e("+1+", "" + object);
                JSONArray cropWeek = object.optJSONArray("crop_week");
                JSONArray jd = cropWeek.optJSONArray(position);
//                Log.e("+2+", "" + jd);
                String weekCropDays = jd.toString().replace("[", "").replace("]", "").replace(", ", ",");
//                Log.e("+3+", "" + weekCropDays);
                if (!TextUtils.isEmpty(weekCropDays)) {
                    String[] arrayStr = weekCropDays.split(",");
                    for (String s : arrayStr) {
//                        Log.e("+4+", "" + s);
                        String weekCropDay = s.replace("\"", "");
//                        Log.e("+5+", "" + weekCropDay);
                        JSONObject quesCropWeek = Objects.requireNonNull(Functions.getWeeklyCropQuestion(sharedPreferences)).optJSONObject(weekCropDay);
                        if (!TextUtils.isEmpty(quesCropWeek.toString())) {
//                            Log.e("+6+", "" + quesCropWeek);
//                            Log.e("+7+", "" + infoType);
                            if (infoType.contains(quesCropWeek.getString("info_type"))) {
                                keyName = quesCropWeek.getString("info_text");
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (TextUtils.isEmpty(keyName)) {
            keyName = getKey(infoType);
        }
        return keyName;
    }

    public static String getKey(String infoType) {
        String keyName = "";
        try {
            JSONObject WeeklyCropInfo = new JSONObject(RongoApp.getWeeklyCropQuestion());
            if (!TextUtils.isEmpty(WeeklyCropInfo.toString())) {
                JSONObject quesCrop = WeeklyCropInfo.optJSONObject(RongoApp.getSelectedCrop());
                JSONObject quesSeason = quesCrop.optJSONObject(RongoApp.getSeasonName());
                JSONArray jsonObject = quesSeason.optJSONArray(RongoApp.getDefaultLanguage());
                JSONObject object = jsonObject.optJSONObject(0);
                JSONObject questions = object.optJSONObject("questions");
                for (int i = 1; i <= questions.length(); i++) {
                    JSONObject quesCropWeek = questions.optJSONObject(String.valueOf(i));
                    if (!TextUtils.isEmpty(quesCropWeek.toString())) {
//                        Log.e("+" + i + "+", "" + quesCropWeek);
                        if (infoType.equalsIgnoreCase(quesCropWeek.getString("info_type"))) {
                            keyName = quesCropWeek.getString("info_text");
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return keyName;
    }

    public static String getValue(String infoType, String infoValue) {
        String valueName = infoValue;
        try {
            JSONObject WeeklyCropInfo = new JSONObject(RongoApp.getWeeklyCropQuestion());
            if (!TextUtils.isEmpty(WeeklyCropInfo.toString())) {
                JSONObject quesCrop = WeeklyCropInfo.optJSONObject(RongoApp.getSelectedCrop());
                JSONArray jsonObject = quesCrop.optJSONArray(LocaleManager.HINDI);
                JSONObject object = jsonObject.optJSONObject(0);
                JSONObject questions = object.optJSONObject("questions");
                for (int i = 1; i <= questions.length(); i++) {
                    JSONObject quesCropWeek = questions.optJSONObject(String.valueOf(i));
                    if (!TextUtils.isEmpty(quesCropWeek.toString())) {
//                        Log.e("+" + i + "+", "" + quesCropWeek);
                        if (infoType.equalsIgnoreCase(quesCropWeek.optString("info_type"))) {
                            if (quesCropWeek.optString("input_type").equalsIgnoreCase("button")) {
                                if (infoValue.equalsIgnoreCase("हाँ")) {
                                    valueName = RongoApp.getInstance().getString(R.string.str_ha);
                                } else {
                                    valueName = RongoApp.getInstance().getString(R.string.str_na);
                                }
                            } else if (quesCropWeek.optString("input_type").equalsIgnoreCase("multi_dropdown")) {
                                JSONArray cropWeek = quesCropWeek.optJSONArray("options");
                                if (cropWeek != null && cropWeek.length() > 0) {
                                    for (int j = 0; j < cropWeek.length(); j++) {
                                        if (infoValue.equalsIgnoreCase(cropWeek.optString(j))) {
                                            valueName = cropWeek.optString(j);
                                        }
                                    }
                                }
                            } else if (quesCropWeek.optString("input_type").equalsIgnoreCase("text")) {

                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return valueName;
    }

    public static String getMobileNumber(String num) {
        if (TextUtils.isEmpty(num))
            return num;

        String finalNumber = null;
        String Trimmed = num.toString().trim();
        if (Trimmed.length() > 10) {
            char[] number = Trimmed.toCharArray();
            int extra;
            int dif = Trimmed.length() - 10;
            for (int i = dif; i < Trimmed.length(); i++) {
                extra = i - dif;
                number[extra] = number[i];
            }
            for (int i = 10; i < Trimmed.length(); i++) {
                number[i] = ' ';
            }
            finalNumber = String.valueOf(number);
        }
        if (!TextUtils.isEmpty(finalNumber)) {
            return finalNumber.trim();
        } else {
            return num;
        }
    }

    public static void setCustomBlackToastBar(Activity context, String text) {
        View layout = context.getLayoutInflater().inflate(R.layout.custom_toast_black,
                (ViewGroup) context.findViewById(R.id.custom_toast_layout));
        Toast toast = new Toast(context);
        TextView textView = (TextView) layout.findViewById(R.id.tvTitle);
        textView.setText(text);
        toast.setGravity(Gravity.BOTTOM | Gravity.CENTER, 0, 82);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public static GradientDrawable getBorderWithColor(String radius, String color) {
        GradientDrawable shape = new GradientDrawable();
        shape.setCornerRadius(Float.parseFloat(radius));
        shape.setColor(Color.parseColor(color));
        return shape;
    }

    public static Spanned getConvertHtml(String text) {
        text = text
                .replace("<br>", "")
                .replace("<li>", "<br>&#8226; ")
                .replace("</li>", "");
        return HtmlCompat.fromHtml(text, HtmlCompat.FROM_HTML_MODE_LEGACY);
    }

    public static Drawable getDrawable(Activity context, int draw) {
        Drawable drawable = null;
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            drawable = ContextCompat.getDrawable(context, draw);
        } else {
            drawable = ContextCompat.getDrawable(context, draw);
        }
        return drawable;
    }

    public static RequestOptions getPlaceholder(String cropId) {
        RequestOptions requestOptions = new RequestOptions();
        if (cropId.equalsIgnoreCase("1")) {
            requestOptions.placeholder(R.mipmap.maize);
            requestOptions.error(R.mipmap.maize);
        } else if (cropId.equalsIgnoreCase("2")) {
            requestOptions.placeholder(R.mipmap.rice);
            requestOptions.error(R.mipmap.rice);
        } else if (cropId.equalsIgnoreCase("3")) {
            requestOptions.placeholder(R.mipmap.cotton);
            requestOptions.error(R.mipmap.cotton);
        } else if (cropId.equalsIgnoreCase("4")) {
            requestOptions.placeholder(R.mipmap.wheat);
            requestOptions.error(R.mipmap.wheat);
        } else if (cropId.equalsIgnoreCase("5")) {
            requestOptions.placeholder(R.mipmap.okra);
            requestOptions.error(R.mipmap.okra);
        } else if (cropId.equalsIgnoreCase("6")) {
            requestOptions.placeholder(R.mipmap.pearmillet);
            requestOptions.error(R.mipmap.pearmillet);
        } else if (cropId.equalsIgnoreCase("7")) {
            requestOptions.placeholder(R.mipmap.corinder);
            requestOptions.error(R.mipmap.corinder);
        } else if (cropId.equalsIgnoreCase("8")) {
            requestOptions.placeholder(R.mipmap.bittergourd);
            requestOptions.error(R.mipmap.bittergourd);
        } else if (cropId.equalsIgnoreCase("9")) {
            requestOptions.placeholder(R.mipmap.bottlegourd);
            requestOptions.error(R.mipmap.bottlegourd);
        } else if (cropId.equalsIgnoreCase("10")) {
            requestOptions.placeholder(R.mipmap.watermelon);
            requestOptions.error(R.mipmap.watermelon);
        } else if (cropId.equalsIgnoreCase("11")) {
            requestOptions.placeholder(R.mipmap.groundnut);
            requestOptions.error(R.mipmap.groundnut);
        } else if (cropId.equalsIgnoreCase("12")) {
            requestOptions.placeholder(R.mipmap.soyabean);
            requestOptions.error(R.mipmap.soyabean);
        }
        return requestOptions;
    }

    public static RequestOptions getCropAreaPlaceholder(int position) {
        RequestOptions requestOptions = new RequestOptions();
        if (position == 0) {
            requestOptions.placeholder(R.drawable.crop);
            requestOptions.error(R.drawable.crop);
        } else if (position == 1) {
            requestOptions.placeholder(R.drawable.root);
            requestOptions.error(R.drawable.root);
        } else if (position == 2) {
            requestOptions.placeholder(R.drawable.stem);
            requestOptions.error(R.drawable.stem);
        } else if (position == 3) {
            requestOptions.placeholder(R.drawable.leaf);
            requestOptions.error(R.drawable.leaf);
        } else if (position == 4) {
            requestOptions.placeholder(R.drawable.flower);
            requestOptions.error(R.drawable.flower);
        } else if (position == 5) {
            requestOptions.placeholder(R.drawable.fruit);
            requestOptions.error(R.drawable.fruit);
        }
        return requestOptions;
    }

    public static Drawable getCropAreaPlaceholderDrawble(int position, Activity activity) {
        Drawable drawable = null;
        if (position == 0) {
            drawable = activity.getResources().getDrawable(R.drawable.crop);
        } else if (position == 1) {
            drawable = activity.getResources().getDrawable(R.drawable.root);
        } else if (position == 2) {
            drawable = activity.getResources().getDrawable(R.drawable.stem);
        } else if (position == 3) {
            drawable = activity.getResources().getDrawable(R.drawable.leaf);
        } else if (position == 4) {
            drawable = activity.getResources().getDrawable(R.drawable.flower);
        } else if (position == 5) {
            drawable = activity.getResources().getDrawable(R.drawable.fruit);
        }
        return drawable;
    }

    public static Locale getLocaleLanguage() {
        Locale locale = new Locale(Constants.Hindi_Language_code);
        if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.GUJARATI)) {
            locale = new Locale(Constants.Gujarati_Language_code);
        } else if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.MARATHI)) {
            locale = new Locale(Constants.Marathi_Language_code);
        } else {
            locale = new Locale(Constants.Hindi_Language_code);
        }
        return locale;
    }

    public static RequestOptions getDrawablePlaceholder(Drawable drawable) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(drawable);
        requestOptions.error(drawable);
        return requestOptions;
    }

    public static int getQuestionIsAvailable(SharedPreferences sharedPreferences) {
        try {
            String quizData = sharedPreferences.getString(Constants.SharedPreferences_Quiz, "");
            if (!TextUtils.isEmpty(quizData)) {
                JSONObject QuizData = new JSONObject(quizData);
                if (QuizData != null && QuizData.length() > 0) {
                    JSONObject json_data = QuizData.getJSONObject("json_data");
                    if (json_data != null && json_data.length() > 0) {
                        Iterator<String> keys = json_data.keys();
                        while (keys.hasNext()) {
                            String key = keys.next();
                            String value = json_data.optString(key);
//                            Log.e("Jsonparsing", "key= " + key + "\n Value=" + value);
                            if (key.equalsIgnoreCase(RongoApp.getSelectedCrop())) {
                                JSONObject jsonLang = new JSONObject(value);
                                if (jsonLang != null && jsonLang.length() > 0) {
                                    Iterator<String> keyLang = jsonLang.keys();
                                    while (keyLang.hasNext()) {
                                        String key1 = keyLang.next();
                                        String value1 = jsonLang.optString(key1);
//                                        Log.e("Jsonparsing++", "key= " + key1 + "\n Value=" + value1);
                                        if (key1.equalsIgnoreCase(RongoApp.getSeasonName())) {
                                            JSONObject jsonLang1 = new JSONObject(value1);
                                            if (jsonLang1 != null && jsonLang1.length() > 0) {
                                                Iterator<String> keyLang1 = jsonLang1.keys();
                                                while (keyLang1.hasNext()) {
                                                    String key2 = keyLang1.next();
                                                    String value2 = jsonLang1.optString(key2);
//                                                    Log.e("Jsonparsing+1+", "key= " + key2 + "\n Value=" + value2);
                                                    if (key2.equalsIgnoreCase(RongoApp.getDefaultLanguage())) {
                                                        JSONArray jsonArray = new JSONArray(value2);
//                                                        Log.e("parseQuizData1", "" + jsonArray);
                                                        return jsonArray.length();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String replaceString(String string) {
        return string.replaceAll("[;\\/:*?\"<>|&']", "");
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static float pxToDpFloat(float px) {
        return (float) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static String getString(JSONArray array) {
        String result = "";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < array.length(); i++) {
            try {
//                Log.e("++lll+++", i + " - " + array.length());
                sb.append(array.getString(i)).append(", ");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        result = sb.deleteCharAt(sb.length() - 2).toString();
        return result;
    }

    public static int getDaysDifference(Date fromDate, Date toDate) {
        if (fromDate == null || toDate == null)
            return 0;

        return (int) ((toDate.getTime() - fromDate.getTime()) / (1000 * 60 * 60 * 24));
    }


    public static long covertToMili(String format, String givenDateString) {
        long timeInMilliseconds = 0;
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.US);
        try {
            Date mDate = sdf.parse(givenDateString);
            timeInMilliseconds = mDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds;
    }

    public static boolean doubleTapCheck(long mLastLongClick) {
        if (SystemClock.elapsedRealtime() - mLastLongClick < 1000) {
            return true;
        } else {
            return false;
        }
    }

    public static SpannableString spannableStr(String text, int start, int end) {
        SpannableString spannableString = new SpannableString(text);
        ForegroundColorSpan foregroundSpan = new ForegroundColorSpan(Color.rgb(241, 106, 48));
//        BackgroundColorSpan backgroundSpan = new BackgroundColorSpan(Color.YELLOW);
        spannableString.setSpan(foregroundSpan, start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        spannableString.setSpan(backgroundSpan, 3, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new RelativeSizeSpan(1.2f), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    public static void showSnackBar(View view, String message) {
        CustomSnackbar customSnackbar = CustomSnackbar.make((ViewGroup) view, CustomSnackbar.LENGTH_LONG);

        View view1 = customSnackbar.getView();
        customSnackbar.getView().setPadding(0, 0, 0, 0);
        AppCompatTextView appCompatTextView = view1.findViewById(R.id.snackbar_text);
        appCompatTextView.setText(message);

        customSnackbar.show();
    }

    public static void showSnackBarGreen(View view, String message) {
        CustomSnackbarGreen customSnackbar = CustomSnackbarGreen.make((ViewGroup) view, CustomSnackbar.LENGTH_LONG);

        View view1 = customSnackbar.getView();
        customSnackbar.getView().setPadding(0, 0, 0, 0);
        AppCompatTextView appCompatTextView = view1.findViewById(R.id.snackbar_text_green);
        appCompatTextView.setText(message);

        customSnackbar.show();
    }

    public static String getDeviceid(Context con) {
        String deviceId = "";
        deviceId = Settings.Secure.getString(con.getContentResolver(), Settings.Secure.ANDROID_ID);
        return deviceId;
    }


    public static String milliSecondsToDateFormat(long millis, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat, Locale.US);
        String dateString = formatter.format(new Date(millis));
        return dateString;
    }

    public static boolean isValidEmail(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        InputMethodManager methodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (view != null && methodManager != null) {
            methodManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    public static String getGooglePhotoImagePath(Bitmap bitmap, Activity contex) {
        String mCurrentPhotoPath = "";
        String fileName = "google_image" + ".JPEG";
        ContentValues values = new ContentValues();

        values.put(MediaStore.Images.Media.TITLE, fileName);
        values.put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");

        File folder = null;
        if (Build.VERSION.SDK_INT >= 29) {
            folder = new File(contex.getExternalFilesDir(null) + File.separator + ".GooglePhoto");
        } else {
            if (Build.VERSION.SDK_INT >= 26) {
                folder = new File(Environment.getExternalStorageDirectory(), ".GooglePhoto");
            } else {
                folder = new File(Environment.getExternalStorageDirectory() + File.separator + ".GooglePhoto");
            }
        }
        folder.mkdir();
        File file = new File(folder, fileName);
        if (file.exists()) {
            file.delete();
        }
        mCurrentPhotoPath = file.getAbsolutePath();
        // get a Uri for the location to save the file
        Uri uri = contex.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        try {
            FileOutputStream outStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
            outStream.flush(); // empty the buffer
            outStream.close(); // close the stream
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                File f = new File("file://" + Environment.getExternalStorageDirectory());
                Uri contentUri = Uri.fromFile(f);
                mediaScanIntent.setData(contentUri);
                contex.sendBroadcast(mediaScanIntent);
                MediaScannerConnection.scanFile(contex, new String[]{file.getPath()}, new String[]{"image/jpeg"}, null);
            } else {
                contex.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, uri));
                MediaScannerConnection.scanFile(contex, new String[]{file.getPath()}, new String[]{"image/jpeg"}, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return mCurrentPhotoPath;
    }

    public static Bitmap getBitmapFromUri(Uri uri, Activity contex) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = contex.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    public static Drawable getAssetsCropImage(Activity activity, String path) {
        Drawable drawable = null;
        InputStream inputStream = null;
        try {
            inputStream = activity.getAssets().open(path);
            drawable = Drawable.createFromStream(inputStream, null);
        } catch (Exception e) {
            e.printStackTrace();
            drawable = null;
        }

        if (drawable == null) {
            drawable = ContextCompat.getDrawable(activity, R.drawable.placeholderbg);
        }

        return drawable;
    }

    public static Drawable getAssetsImage(Activity activity, String path) {
        Drawable drawable = null;
        InputStream inputStream = null;
        try {
            inputStream = activity.getAssets().open(path);
            drawable = Drawable.createFromStream(inputStream, null);
        } catch (Exception e) {
            e.printStackTrace();
            drawable = null;
        }
        return drawable;
    }

    public static Drawable getAssetsKeywordImage(Activity activity, String path) {
        Drawable drawable = null;
        InputStream inputStream = null;
        try {
            inputStream = activity.getAssets().open(path);
            drawable = Drawable.createFromStream(inputStream, null);
        } catch (Exception e) {
            e.printStackTrace();
            drawable = null;
        }

        if (drawable == null) {
            drawable = ContextCompat.getDrawable(activity, R.drawable.placeholderbg);
        }

        return drawable;
    }

    public static Drawable getAssetsWeatherImage(Activity activity, String path) {
        Drawable drawable = null;
        InputStream inputStream = null;
        try {
            inputStream = activity.getAssets().open(path);
            drawable = Drawable.createFromStream(inputStream, null);
        } catch (Exception e) {
            e.printStackTrace();
            drawable = null;
        }

        if (drawable == null) {
            drawable = ContextCompat.getDrawable(activity, R.drawable.placeholderbg);
        }

        return drawable;
    }


    public static void displayImage(String url, ImageView imageView) {
        GlideApp.with(imageView.getContext()).load(url).into(imageView);
    }

    public static void displayImageWithPlaceHolder(String url, ImageView imageView) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.placeholderbg);
        requestOptions.error(R.drawable.placeholderbg);

        GlideApp.with(imageView.getContext()).load(url).apply(requestOptions).into(imageView);
    }

    public static void displayImageLocal(String url, ImageView imageView) {
        if (url == null) {
            url = "";
        }
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.fitCenter();
        requestOptions.placeholder(R.drawable.placeholderbg);
        requestOptions.error(R.drawable.placeholderbg);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.ALL);
        GlideApp.with(imageView.getContext()).load(new File(url)).apply(requestOptions).into(imageView);
    }

    public static Spanned fromHtml(String source) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(source);
        }
    }

    public static String converOneDateFormatToOther(String source, String destination, String dateString) {
        DateFormat originalFormat = new SimpleDateFormat(source, Locale.US);
        DateFormat targetFormat = new SimpleDateFormat(destination);
        Date date = new Date();
        try {
            date = originalFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = targetFormat.format(date);
        return formattedDate;
    }

    public static void showProgress(Activity activity, ProgressBar progressBar) {
        progressBar.setVisibility(View.VISIBLE);
//        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
//                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    public static void hideProgress(Activity activity, ProgressBar progressBar) {
        progressBar.setVisibility(View.GONE);
//        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

//     int getRandomIndex() {
//        Random rand = new Random();
//        return rand.nextInt(data.size() - 1) + 0;
//    }

    public static int getRandomColor(int pos) {
        int color = 0xffFFF3E0;
        try {
            if (pos == 0) {
                color = Color.argb(255, 126, 215, 255);
            } else if (pos == 1) {
                color = Color.argb(255, 143, 97, 222);
            } else if (pos == 2) {
                color = Color.argb(255, 242, 206, 105);
            } else if (pos == 3) {
                color = Color.argb(255, 255, 161, 154);
            } else if (pos == 4) {
                color = Color.argb(255, 143, 97, 222);
            } else if (pos == 5) {
                color = Color.argb(255, 185, 194, 131);
            } else if (pos == 6) {
                color = Color.argb(255, 196, 162, 252);
            } else {
                color = Color.argb(255, 196, 162, 252);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return color;
    }

    public static int getFertilizerWeeks(SharedPreferences sharedPreferences) {
        int week = 0;
        try {
            JSONObject fertilizerInfo = new JSONObject(sharedPreferences.getString(Constants.SharedPreferences_FertilizerInfo, ""));
            if (fertilizerInfo != null && fertilizerInfo.length() > 0) {
                Iterator<String> keys = fertilizerInfo.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    String value = fertilizerInfo.optString(key);
                    if (key.equalsIgnoreCase(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, "1"))) {
                        JSONObject jsonLang = new JSONObject(value);
                        if (jsonLang != null && jsonLang.length() > 0) {
                            Iterator<String> keyLang = jsonLang.keys();
                            while (keyLang.hasNext()) {
                                String key1 = keyLang.next();
                                String value1 = jsonLang.optString(key1);
                                if (key1.equalsIgnoreCase(RongoApp.getDefaultLanguage())) {
                                    JSONObject jsonFert = new JSONObject(value1);
                                    week = Integer.parseInt(jsonFert.optString("last_week"));
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.v("+++++Ferti++++++", "" + week);
        return week;
    }

    public static String getName(int index) throws JSONException {
        String name = "", str = "";
        if (RongoApp.getDefaultLanguage().equalsIgnoreCase("gu")) {
            str = "[શૂન્ય, \"પ્રથમ\", \"બીજું\", \"ત્રીજું\", \"ચોથું\", \"પાંચમું\", \"છઠ્ઠું\", \"સાતમું\", \"આઠમું\", \"નવમું\", \"દસમું\", \"અગિયારમું\", \"બારમું\", \"તેરમું\", \"ચૌદમું\", \"પંદરમું\", \"સોળમું\", \"સત્તરમું\", \"અઢારમું\", \"ઓગણીસમું\", \"વીસમું\", \"એકવીસમું\", \"બાવીશમું\", \"તેવીશમું\", \"ચોવીસમું\", \"પચ્ચીસમું\", \"છવ્વીસમું\", \"સત્તાવીસમું\", \"અઠ્ઠાવીસમું\", \"ઓગણત્રીસમું\", \"ત્રિસમું\", \"એકત્રીસમું\", \"બત્રીસમું\", \"તેત્રીસમું\", \"ચોત્રીસમું\", \"પાંત્રીસમું\", \"છત્રીસમું\", \"સાડત્રીસમું\", \"આડત્રીસમું\", \"ઓગણસાલીસમું\", \"ચાલીસમું\", \"એક્તાલીસમું\", \"બેતાલીસમું\", \"ત્રેતાલીસમું\", \"ચુમાંલીસમું\", \"પીસ્તાલીસમું\", \"છેતાલીસમું\", \"સુડતાલીસમું\", \"અડતાલીસમું\", \"ઓગણપચાસમુંં\", \"પચાસમુંં\", \"એકાવનમુંં\", \"બાવનમુંં\", \"ત્રેપનમુંં\", \"ચોપનમુંં\", \"પંચાવનમુંં\", \"છપ્પનમુંં\", \"સત્તાવનમુંં\", \"અઠ્ઠાવનમુંં\", \"ઓગણસાઠમુંં\", \"સાઈઠમોં\"]";
        } else if (RongoApp.getDefaultLanguage().equalsIgnoreCase("mr")) {
            str = "[शून्य, \"पहिला\", \"दूसरा\", \"तीसरा\", \"चौथा\", \"पाचवा\", \"सहवा\", \"सातवा\", \"आठवा\", \"नववा\", \"दहावा\", \"अकरावा \", \"बारावा\", \"तेरावा\", \"चौदावा\", \"पंधरावा\", \"सोळावा\", \"सतरावा \", \"अठरावा\", \"एकोणिसावा \",\"विसावा\", \"एकवीसवा\", \"बावीसवा\", \"तेवीसवा\", \"चोविसावा\", \"पंचवीसवा\", \"सविसावा \", \"सत्तावीसवा \", \"अठ्ठावीसवा \", \"एकोणतिसवा  \", \"तिसावा\", \"एकतिसवा \", \"बत्तीसवा \", \"तेतीसवा \", \"चोतीसवा \", \"पस्तीसवा\", \"छत्तीसवा\", \"सदोतीसवा \", \"आडोतीसवा\",\"एकोनचाळीसवा\", \"चाळीसवा\", \"एकेचाळीसवा\", \"बेचाळीसवा\", \"त्रेचाळिसवा \", \"चोरेचाळिसवा\", \"पंचेचाळीसवा\", \"सेहचाळीसवा\", \"सत्तेचाळीसवा\", \"अठ्ठेचाळीसवा\", \"एकोनपन्नासवा\", \"पन्नासवा \", \"एकावन्नावा \", \"बावन्नावा\", \"त्रेपन्नावा\", \"चोपन्नावा\", \"पंचावन्नवा\", \"छपवन्नवा\", \"सत्तावनवा \", \"अठ्ठावनवा\", \"एकोनसाठ्ठवा\", \"साठ्ठवा\"]";
        } else {
            str = "[शून्य, \"पहला\", \"दूसरा\", \"तीसरा\", \"चौथा\", \"पांचवाँ\", \"छठा\", \"सातवाँ\", \"आठवाँ\", \"नौवाँ\", \"दसवाँ\", \"ग्यारहवाँ\", \"बारहवाँ\", \"तेरहवाँ\", \"चौदहवाँ\", \"पंद्रहवाँ\", \"सोलहवाँ\", \"सत्तरवां\", \"अठारहवाँ\", \"उन्नीसवाँ\", \"बीसवाँ\", \"इक्कीसवाँ\", \"बाईसवाँ\", \"तेईसवाँ\", \"चौबीसवाँ\", \"पच्चीसवाँ\", \"छब्बीसवाँ\", \"सत्ताइसवाँ\", \"अट्ठाइसवाँ\", \"उनतीसवाँ\", \"तीसवाँ\", \"इकतीसवाँ\", \"बत्तीसवाँ\", \"तैंतीसवाँ\", \"चौंतीसवां\", \"पैंतीसवाँ\", \"छत्तीसवाँ\", \"सैंतीसवाँ\", \"अड़तीसवाँ\", \"उनतालीसवाँ\", \"चालीसवाँ\", \"इकतालीसवां\", \"बयालीसवाँ\", \"तैंतालीसवाँ\", \"चौतालीसवाँ\", \"पैंतालीसवां\", \"छियालीसवाँ\", \"सैंतालीसवाँ\", \"अड़तालीसवाँ\", \"उनचासवाँ\", \"पचासवाँ\", \"इक्यावनवां\", \"बावनवाँ\", \"तिरेपानवाँ\", \"चौवनवाँ\", \"पचपनवाँ\", \"छप्पनवाँ\", \"सतावनवाँ\", \"अट्ठावनवाँ\", \"उनसठवाँ\", \"साठवाँ\"]";
        }
        JSONArray jsonArray = new JSONArray(str);
        name = jsonArray.opt(index).toString();
        Log.v("Week Name", name + " : " + index);
        return name;
    }

    public static ProfileResultData getData() {
        SharedPreferences sharedPreferences = RongoApp.rongoApp.getApplicationContext().getSharedPreferences(Constants.MediaPrefs, 0);
        Gson gson = new Gson();
        String json = sharedPreferences.getString(Constants.SharedPreference_profileResultData, "");
        ProfileResultData obj = gson.fromJson(json, ProfileResultData.class);
        return obj;
    }

    public static String setTimeAgoMyTopic(Context _context, long strdate) {
        String return_str = "";
        long millis = strdate;

        Calendar cal = Calendar.getInstance();
        Date o_date = new Date(cal.getTimeInMillis());

        long curTime = o_date.getTime();
        long diff_second = (curTime / 1000) - (millis / 1000);
        long year = diff_second / (86400 * 30 * 12);
        long month = diff_second / (86400 * 30);
        long week = diff_second / (86400 * 7);
        long day = diff_second / 86400;
        long hour = diff_second / 3600;
        long minutes = diff_second / 60;

        if (year > 0) {
            if (year > 1)
                return_str = year + " " + _context.getResources().getString(R.string.years_ago);
            else
                return_str = year + " " + _context.getResources().getString(R.string.years_ago);

        } else if (month > 0) {
            if (month > 1)
                return_str = month + " " + _context.getResources().getString(R.string.months_ago);
            else
                return_str = month + " " + _context.getResources().getString(R.string.months_ago);

        } else if (week >= 1) {
            if (week > 1)
                return_str = week + " " + _context.getResources().getString(R.string.weeks_ago);
            else
                return_str = week + " " + _context.getResources().getString(R.string.weeks_ago);

        } else if (day >= 1) {
            if (day > 1)
                return_str = day + " " + _context.getResources().getString(R.string.days_ago);
            else {
                return_str = "कल";
            }
        } else if (hour >= 1) {
            if (hour > 1)
                return_str = hour + " " + _context.getResources().getString(R.string.hours_ago);
            else
                return_str = hour + " " + _context.getResources().getString(R.string.hours_ago);
        } else if (minutes >= 1) {
            if (minutes > 1)
                return_str = minutes + " " + _context.getResources().getString(R.string.mins_ago);
            else
                return_str = minutes + " " + _context.getResources().getString(R.string.mins_ago);
        } else {
            return_str = _context.getResources().getString(R.string.few_sec_ago);
        }

        return return_str;
    }


    public static File getOutputMediaFile() {

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + RongoApp.rongoApp.getApplicationContext().getPackageName()
                + "/Files");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        File mediaFile;
        String mImageName = "_localImage.JPEG";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public static String storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d("ERROR_LOAD", "Error creating media file, check storage permissions: ");// e.getMessage());
            return "";
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("FILELEE", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("FILELEE", "Error accessing file: " + e.getMessage());
        }

        return pictureFile.getAbsolutePath();
    }

    public static File getFile() {

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + RongoApp.rongoApp.getApplicationContext().getPackageName()
                + "/Salah");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        File mediaFile;
        String mImageName = "_banner.JPEG";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public static String storeImageSalahBanner(Bitmap image) {
        File pictureFile = getFile();
        if (pictureFile == null) {
            Log.d("ERROR_LOAD", "Error creating media file, check storage permissions: ");// e.getMessage());
            return "";
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("FILELEE", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("FILELEE", "Error accessing file: " + e.getMessage());
        }

        return pictureFile.getAbsolutePath();
    }

    public static String preLoadImage(Bitmap image, String id) {
        File pictureFile = getPreloadImages(id);
        if (pictureFile == null) {
            Log.d("ERROR_LOAD", "Error creating media file, check storage permissions: ");// e.getMessage());
            return "";
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("FILELEE", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("FILELEE", "Error accessing file: " + e.getMessage());
        }

        return pictureFile.getAbsolutePath();
    }


    public static List<String> getFromSdcard() {
        List<String> images = new ArrayList<>();
        File[] listFile;
        File file = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + RongoApp.rongoApp.getApplicationContext().getPackageName()
                + "/PreloadImages");
        if (file.isDirectory()) {
            listFile = file.listFiles();
            if (listFile != null && listFile.length > 0) {
                for (int i = 0; i < listFile.length; i++) {

                    images.add(listFile[i].getAbsolutePath());
                }
            }

        }
        return images;
    }

    public static File getPreloadImages(String id) {

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + RongoApp.rongoApp.getApplicationContext().getPackageName()
                + "/PreloadImages");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        File mediaFile;
        String mImageName = "localImage" + id + ".JPEG";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }


    public static String advisoryImages(Bitmap image) {
        File pictureFile = getAdvisoryImages();
        if (pictureFile == null) {
            Log.d("ERROR_LOAD", "Error creating media file, check storage permissions: ");// e.getMessage());
            return "";
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("FILELEE", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("FILELEE", "Error accessing file: " + e.getMessage());
        }

        return pictureFile.getAbsolutePath();
    }

//    public static void clearDukanDarInfo(SharedPreferences sharedPreferences) {
//        SharedPreferencesUtils.storeStateOfString(sharedPreferences, "location", "");
//        SharedPreferencesUtils.storeStateOfString(sharedPreferences, "shopKeeperName", "");
//        SharedPreferencesUtils.storeStateOfString(sharedPreferences, "hybrid_number", "");
//        SharedPreferencesUtils.storeStateOfString(sharedPreferences, "shopKeeperPhone", "");
//    }

    public static List<String> getFromSdcardAdvisory() {
        List<String> images = new ArrayList<>();
        File[] listFile;
        File file = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + RongoApp.rongoApp.getApplicationContext().getPackageName()
                + "/Advisory");
        if (file.isDirectory()) {
            listFile = file.listFiles();
            if (listFile != null && listFile.length > 0) {
                for (int i = 0; i < listFile.length; i++) {
                    images.add(listFile[i].getAbsolutePath());
                }
            }
        }
        return images;
    }

    public static File getAdvisoryImages() {

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + RongoApp.rongoApp.getApplicationContext().getPackageName()
                + "/Advisory");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        File mediaFile;
        String mImageName = "localImage" + System.currentTimeMillis() + ".JPEG";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public static void setLastAppOpenTime(SharedPreferences sharedPreferences) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            Date dateformat = new Date(System.currentTimeMillis());
            String date = dateFormat.format(dateformat);
            Log.d("App Open Time: ", date);
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_LastLoginTime, "" + date);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getDayFromDateString(String stringDate) {
        String day = "";
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date = null;
        try {
            date = inFormat.parse(stringDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat outFormat = new SimpleDateFormat("EEE", Locale.getDefault());
        day = outFormat.format(date);
        return day;
    }


    public static String getTodayDate() {
        String currentdate = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Calendar currentCal = Calendar.getInstance();
        currentdate = dateFormat.format(currentCal.getTime());
        return currentdate;
    }

    public static String getCustomDate(int limit) {
        String toDate = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Calendar currentCal = Calendar.getInstance();
        String currentdate = dateFormat.format(currentCal.getTime());
        currentCal.add(Calendar.DATE, limit);
        toDate = dateFormat.format(currentCal.getTime());
        Log.e("getCustomDate", currentdate + " - " + toDate);
        return toDate;
    }

    public static ArrayList<String> getStartEndCustomizeDate() {
        ArrayList<String> dateList = new ArrayList<String>();
        for (int i = 0; i < 5; i++) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Calendar currentCal = Calendar.getInstance();
            currentCal.add(Calendar.DATE, i);
            String toDate = dateFormat.format(currentCal.getTime());
            dateList.add(toDate);
        }
        return dateList;
    }

    public static boolean getValidateCurrentAfterDate(int limit) {
        boolean toDate = false;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Calendar toDayCalendar = Calendar.getInstance(Locale.US);
        Date date1 = toDayCalendar.getTime();
        Calendar tomorrowCalendar = Calendar.getInstance(Locale.US);
        tomorrowCalendar.add(Calendar.DAY_OF_MONTH, limit);
        Date date2 = tomorrowCalendar.getTime();
        // date1 is a present date and date2 is tomorrow date
        if (date1.compareTo(date2) < 0) {
            //  0 comes when two date are same,
            //  1 comes when date1 is higher then date2
            // -1 comes when date1 is lower then date2
            Log.e("getdateX", "" + sdf.format(date2));
            toDate = true;
        } else if (date1.compareTo(date2) == 0) {
            Log.e("getdateX1", "" + sdf.format(date2));
            toDate = true;
        }
        return toDate;
    }

    public static void openPostQuestionSuccessDialog(Activity activity, AppDatabase appDatabase, String QuesPoint, String InvitePoint) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(activity);
        View view = LayoutInflater.from(activity).inflate(R.layout.bottom_success_post_ques_dialog, null);
        bottomSheetDialog.setContentView(view);

        AppCompatTextView txtOfflineSMS = view.findViewById(R.id.txtOfflineSMS);
        if (InternetConnection.checkConnectionForFasl(activity)) {
            if (InternetConnection.getNetworkClass(activity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                txtOfflineSMS.setVisibility(View.VISIBLE);
                txtOfflineSMS.setText(activity.getResources().getText(R.string.str_internet_msg_post_ques_2g));
            } else {
                txtOfflineSMS.setVisibility(View.GONE);
            }
        } else {
            txtOfflineSMS.setVisibility(View.VISIBLE);
            txtOfflineSMS.setText(activity.getResources().getText(R.string.str_internet_coming_to_upload_question));
        }

        AppCompatTextView tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText(QuesPoint);

        AppCompatTextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(InvitePoint);

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();

                if (!TextUtils.isEmpty(RongoApp.getUserId()) && RongoApp.getWhatsAppNotificationStatus().equalsIgnoreCase("")) {
                    if (InternetConnection.checkConnectionForFasl(activity)) {
                        HomeActivity.setWhatsAppNotification();
                    }
                }
            }
        });

        AppCompatTextView tvShareWhatapp = view.findViewById(R.id.tvShareWhatapp);
        tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    bottomSheetDialog.dismiss();
                    setUserScreenTrackEvent(appDatabase, "Invite", "WhatsApp");
                    shareWhatAppIntent(activity);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(activity, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        AppCompatTextView tvShareCopy = view.findViewById(R.id.tvShareCopy);
        tvShareCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    bottomSheetDialog.dismiss();
                    setUserScreenTrackEvent(appDatabase, "Invite", "SMS");
                    shareSMSIntent(activity, RongoApp.getReferralText(activity));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AppCompatTextView tvSharefacebook = view.findViewById(R.id.tvSharefacebook);
        tvSharefacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                setUserScreenTrackEvent(appDatabase, "Invite", "Facebook");
                shareFacebookIntent(activity);
            }
        });

        AppCompatTextView tvShareMore = view.findViewById(R.id.tvShareMore);
        tvShareMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                performShare(activity, appDatabase);
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(activity.getResources().getColor(android.R.color.transparent));
        if (!bottomSheetDialog.isShowing()) {
            bottomSheetDialog.show();
        }
    }


    public static void setDailyReward(AppDatabase appDatabase, String points) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            Date dateformat = new Date(System.currentTimeMillis());
            String datetime = dateFormat.format(dateformat);
//            Log.v("Function Class1", "" + datetime);

            DailyRewardData user = new DailyRewardData();
            user.setUid(new Random().nextInt());
            user.setReward_points(points);
            user.setMetadata("daily_login");
            user.setCreated_on(datetime);
//            user.setCreated_on(getRewardDate());
            appDatabase.dailyRewardDao().insertAll(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getRewardDate() {
        String date = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
            Date dateformat = new Date(System.currentTimeMillis());
            date = dateFormat.format(dateformat);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static void setUserScreenTrackEvent(AppDatabase appDatabase, String screenName, String id) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            Date dateformat = new Date(System.currentTimeMillis());
            String datetime = dateFormat.format(dateformat);
//            Log.d("Function Class2", "" + datetime);

            TrackUserData user = new TrackUserData();
            user.setUid(new Random().nextInt());
            user.setScreenName(screenName);
            user.setActivityId(id);
            user.setDateTime(datetime);
            appDatabase.trackUserDao().insertAll(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setFirebaseLogEventTrack(Activity activity, String eventName, String screenName, String id, String action) {
        try {
            Bundle params = new Bundle();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            Date date = new Date(System.currentTimeMillis());
            params.putString("action_time", dateFormat.format(date));

            params.putString("androidId", mAppOpenData.getAndroidId());
            params.putString("mDeviceId", mAppOpenData.getmDeviceId());
            params.putString("mBrand", mAppOpenData.getmBrand());
            params.putString("mModel", mAppOpenData.getmModel());
            params.putString("mDeviceType", mAppOpenData.getmDeviceType());
            params.putString("mOsVersion", mAppOpenData.getmOsVersion());
            params.putString("strCurrentVersion", mAppOpenData.getCurrentAppVersion());
            if (InternetConnection.checkConnectionForFasl(activity)) {
                params.putString("mInterNetAvailable", "" + InternetConnection.checkConnectionForFasl(activity));
                params.putString("networkType", InternetConnection.getNetworkClass(activity));
                params.putString("networkSpeed", InternetConnection.getNetworkSpeed(activity));
            }
            params.putString("mLatitude", "");
            params.putString("mLongitude", "");

            params.putString("screenName", screenName);
            params.putString("id", id);
            params.putString("action", action);
            RongoApp.getInstance().mFirebaseAnalytics.logEvent(eventName, params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setFirebaseErrorLog(String errorLog) {
        try {
            Bundle params = new Bundle();
            params.putString("mError_time", getCurrentDateTime());
            params.putString("mAndroidId", mAppOpenData.getAndroidId());
            params.putString("mDeviceId", mAppOpenData.getmDeviceId());
            params.putString("mBrand", mAppOpenData.getmBrand());
            params.putString("mModel", mAppOpenData.getmModel());
            params.putString("mDeviceType", mAppOpenData.getmDeviceType());
            params.putString("mOsVersion", mAppOpenData.getmOsVersion());
            params.putString("mAppVersion", mAppOpenData.getCurrentAppVersion());
            params.putString("mErrorLog", errorLog);
            RongoApp.getInstance().mFirebaseAnalytics.logEvent(Constants.EVENT_NAME_ERROR_LOG, params);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getAfterWeekDate() {
        String date = "";
        try {
            Date dateformat = new Date(System.currentTimeMillis());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateformat);
            calendar.add(Calendar.DAY_OF_YEAR, -7);
            Date newDate = calendar.getTime();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
            date = dateFormat.format(newDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getAfterThreeDaysDate() {
        String date = "";
        try {
            Date dateformat = new Date(System.currentTimeMillis());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dateformat);
            calendar.add(Calendar.DAY_OF_YEAR, -3);
            Date newDate = calendar.getTime();
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
            date = dateFormat.format(newDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String getCurrentDate() {
        String date = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
            Date dateformat = new Date(System.currentTimeMillis());
            date = dateFormat.format(dateformat);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static List<StateCityDataModel> parseStateCityListData(SharedPreferences sharedPreferences) {
        List<StateCityDataModel> stateCityDataModels = new ArrayList<StateCityDataModel>();
        try {
            String stateCityData = sharedPreferences.getString(Constants.SharedPreferences_StateCityData, "");
            if (!TextUtils.isEmpty(stateCityData)) {
//                Log.e("stateCityData:", "" + stateCityData);
                JSONObject jsonObject = new JSONObject(stateCityData);
                JSONObject data = jsonObject.optJSONObject("data");
                if (data != null && data.length() > 0) {
                    JSONArray result1 = data.optJSONArray("result");
                    JSONArray result = new JSONArray(result1.optJSONObject(0).optString(RongoApp.getDefaultLanguage()));
                    if (result != null && result.length() > 0) {
                        for (int i = 0; i < result.length(); i++) {
                            JSONObject object = result.optJSONObject(i);

                            JSONObject state = object.optJSONObject("state");
                            if (state != null && state.length() > 0) {
                                parseJsonState(i, state, stateCityDataModels);
                            }

                            JSONObject district = object.optJSONObject("district");
                            if (district != null && district.length() > 0) {
                                parseJsonCity(i, district, stateCityDataModels);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stateCityDataModels;
    }

    public static void parseJsonState(int id, JSONObject data, List<StateCityDataModel> stateCityDataModels) {
        StateCityDataModel stateCityDataModel = new StateCityDataModel();
        if (data != null) {
            Iterator<String> it = data.keys();
            while (it.hasNext()) {
                String key = it.next();
                try {
                    if (data.get(key) instanceof JSONArray) {
                        JSONArray arry = data.getJSONArray(key);
                        int size = arry.length();
                        for (int i = 0; i < size; i++) {
                            parseJsonState(i, arry.getJSONObject(i), stateCityDataModels);
                        }
                    } else if (data.get(key) instanceof JSONObject) {
                        parseJsonState(0, data.getJSONObject(key), stateCityDataModels);
                    } else {
//                        System.out.println("" + key + "-" + id + " : " + data.optString(key));
                        stateCityDataModel.setId(String.valueOf(id));
                        stateCityDataModel.setState_english(key);
                        stateCityDataModel.setState_hindi(data.optString(key));
                        stateCityDataModels.add(stateCityDataModel);
                    }
                } catch (Throwable e) {
//                    System.out.println("" + key + " : " + data.optString(key));
                    e.printStackTrace();
                }
            }
        }
    }

    public static void parseJsonCity(int id, JSONObject data, List<StateCityDataModel> stateCityDataModels) {
        ArrayList<StateCityDataModel.CityListModel> cityListModels = new ArrayList<>();
        if (data != null) {
            Iterator<String> it = data.keys();
            while (it.hasNext()) {
                String key = it.next();
                try {
                    if (data.get(key) instanceof JSONArray) {
                        JSONArray arry = data.getJSONArray(key);
                        int size = arry.length();
                        for (int i = 0; i < size; i++) {
                            parseJsonState(i, arry.getJSONObject(i), stateCityDataModels);
                        }
                    } else if (data.get(key) instanceof JSONObject) {
                        parseJsonState(0, data.getJSONObject(key), stateCityDataModels);
                    } else {
//                        System.out.println("" + key + "-" + id + " : " + data.optString(key));
                        StateCityDataModel.CityListModel cityListModel = new StateCityDataModel.CityListModel();
                        cityListModel.setId(String.valueOf(id));
                        cityListModel.setCity_english(key);
                        cityListModel.setCity_hindi(data.optString(key));
                        cityListModels.add(cityListModel);
                    }
                } catch (Throwable e) {
//                    System.out.println("" + key + " : " + data.optString(key));
                    e.printStackTrace();
                }
            }
            stateCityDataModels.get(id).setCityList(cityListModels);
        }
    }

    public static String getContestAvailable(SharedPreferences sharedPreferences) {
        String isAvailable = "0";
        try {
            String contestsData = sharedPreferences.getString(Constants.SharedPreferences_CONTEST, "");
//            Log.e("contestsData: ", "" + contestsData);
            if (!TextUtils.isEmpty(contestsData)) {
                JSONArray jsonArray = new JSONArray(contestsData);
                if (jsonArray != null && jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject resultData = jsonArray.getJSONObject(i);
                        if (resultData.optString("type").contains("spin")) {
//                            contestID = resultData.optString("id");
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SPIN_RESULT,
                                    "" + resultData.optString("profile_contest_result"));
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_CONTEST_ID,
                                    "" + resultData.optString("id"));

                            String profile_contest_is_available = resultData.optString("profile_contest_is_available");

                            String jackPotStatus = resultData.optString("profile_contest_status");
                            if (jackPotStatus.equalsIgnoreCase("0")) {
                                isAvailable = "1";
                            } else {
                                if (!TextUtils.isEmpty(resultData.optString("profile_contest_metadata"))) {
                                    JSONObject metadata = new JSONObject(resultData.optString("profile_contest_metadata"));
                                    if (metadata != null && metadata.length() > 0) {
                                        String end_date = metadata.optString("end_date");
                                        if (metadata.getString("is_true").equalsIgnoreCase("0")) {

                                            if (profile_contest_is_available.equalsIgnoreCase("1")) {
                                                isAvailable = "1";
                                            } else {
                                                isAvailable = "2";
                                            }

//                                            String playDateTime = sharedPreferences.getString(Constants.SharedPreference_Spin_Date, "");
//                                            if (!TextUtils.isEmpty(playDateTime)) {
//                                                long ex_time = Functions.covertToMili("dd/MM/yyyy", playDateTime);
//                                                Date d_ = new Date();
//                                                d_.setTime(System.currentTimeMillis());
//
//                                                Date ex = new Date();
//                                                ex.setTime(ex_time);
//                                                Log.e("Spin DateTime", Functions.getDaysDifference(d_, ex) + " - " + playDateTime);
//                                                int days = Functions.getDaysDifference(d_, ex);
//                                                if (days >= 7) {
//                                                    isAvailable = true;
//                                                }
//                                            }

                                        } else {
                                            isAvailable = "0";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            Log.e("isAvailable: ", "" + isAvailable);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isAvailable;
    }

    public static String getCurrentDDMM() {
        String date = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("ddMM", Locale.US);
            Date dateformat = new Date(System.currentTimeMillis());
            date = dateFormat.format(dateformat);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("DDMM", "" + date);
        return date;
    }

    public static String getCurrentTime() {
        String time = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss", Locale.US);
            Date dateformat = new Date(System.currentTimeMillis());
            time = dateFormat.format(dateformat);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return time;
    }

    public static String getCurrentDateTime() {
        String dateTime = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            Date dateformat = new Date(System.currentTimeMillis());
            dateTime = dateFormat.format(dateformat);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dateTime;
    }

    public static String compressImage(Activity context, String imageUri) {
        long kilobyte = 1024;
        long megabyte = kilobyte * 1024;

        File file = new File(imageUri);
        if (file.length() > 0) {
            Log.v("image Size: ", "" + file.length());
            long size = file.length() / megabyte;
            Log.v("image Size: ", "" + size);
            if (size < 2) {
                return imageUri;
            }
        }

        String filePath = getRealPathFromURI(context, imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612
//        float maxHeight = 1500.0f;
//        float maxWidth = 1300.0f;
        float maxHeight = actualWidth;
        float maxWidth = actualHeight;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;
            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            com.rongoapp.views.Log.v("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                com.rongoapp.views.Log.v("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                com.rongoapp.views.Log.v("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                com.rongoapp.views.Log.v("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename(context);
        try {
            out = new FileOutputStream(filename);
//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        com.rongoapp.views.Log.v("Compress Image:", "" + filename);
        return filename;
    }

    public static String getFilename(Activity activity) {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "Rongo/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".JPEG");
        return uriSting;
    }

    public static String getRealPathFromURI(Activity context, String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = context.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        return inSampleSize;
    }


    private int getAppInstallTimeHours(Activity mContext) {
        int hours = 0;
        try {
            PackageManager pm = mContext.getPackageManager();
            PackageInfo packageInfo = pm.getPackageInfo("com.rongoapp", PackageManager.GET_PERMISSIONS);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

            String installTime = dateFormat.format(new Date(packageInfo.firstInstallTime));
            Log.d("Application Class", "Installed: " + installTime);

            String updateTime = dateFormat.format(new Date(packageInfo.lastUpdateTime));
            Log.d("Application Class", "Updated: " + updateTime);

            Date date1 = dateFormat.parse(installTime);
            Date date2 = new Date(System.currentTimeMillis());

            long mills = date2.getTime() - date1.getTime();
            hours = (int) (mills / (1000 * 60 * 60));
            Log.d("Application Class", "hours: " + hours);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hours;
    }

    private String getCurrentDay() {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        String day = new SimpleDateFormat("EEEE", Locale.US).format(date.getTime());
        System.out.println(day);
        return day;
    }

    public static void setRongoLogo(ImageView ivLogo) {
        if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.GUJARATI)) {
            ivLogo.setImageResource(R.mipmap.rongo_logo_gu);
        } else {
            ivLogo.setImageResource(R.mipmap.rongo_logo);
        }
    }

    public static String getSeasonName(Activity activity, String month) {
        List<String> kharif = new ArrayList<String>(Arrays.asList(activity.getResources().getStringArray(R.array.Kharif)));
        List<String> rabi = new ArrayList<String>(Arrays.asList(activity.getResources().getStringArray(R.array.Rabi)));
        String season = "";
        if (kharif.contains(month)) {
            season = Constants.KHARIF_SEASON;
        }
        if (rabi.contains(month)) {
            season = Constants.RABI_SEASON;
        }
        return season;
    }

    public static boolean showWeatherView(String sDate) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
            if (!TextUtils.isEmpty(sDate)) {
                Date date1 = dateFormat.parse(sDate);
                Date date2 = dateFormat.parse(getCurrentDate());
//                Log.d("showWeatherView", "" + date1 + " - " + date2);
                if (date1.before(date2)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private String getExpectedDays(String exSowingDate) {
        String day = "0";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.US);
            Date c = Calendar.getInstance().getTime();
            if (!TextUtils.isEmpty(exSowingDate)) {
                Log.d("Application Class", "exSowingDate: " + exSowingDate);
                Date date1 = dateFormat.parse(exSowingDate);
                Date date2 = dateFormat.parse(dateFormat.format(c.getTime()));
                Log.d("Application Class", "date2 day: " + date2);

                day = String.valueOf(printDifference(date2, date1));
                Log.d("getExpectedDays", "day: " + day);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return day;
    }

    public long printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;
        System.out.println("elapsedDays : " + elapsedDays);

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;
        long elapsedMinutes = different / minutesInMilli;
        return elapsedDays;
    }

    /*
     * Alarm Notification Trigger set Repeting Notification.
     * */
    private void setAlarmNotificationRepeting(Activity activity) {
        /* Set the alarm to start at 10:30 AM */
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, 18);
        calendar.set(Calendar.MINUTE, 58);
        /* Repeating on every 20 minutes interval */
        int interval = 1000 * 60 * 20;

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE));
        long Interval = 1000 * 60;

        AlarmManager alarmManager = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);
        Intent notificationIntent = new Intent(activity, AlarmReceiver.class);
        PendingIntent broadcast = PendingIntent.getBroadcast(activity, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        assert alarmManager != null;
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), Interval, broadcast);
    }

    /*
     *  Alarm Notification Trigger set Once Notification.
     * */
    private void setAlarmNotificationOnce(Activity activity) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 0);

        AlarmManager alarmManager = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);
        Intent notificationIntent = new Intent(activity, AlarmReceiver.class);
        notificationIntent.putExtra("class", "register");
        PendingIntent broadcast = PendingIntent.getBroadcast(activity, 100, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//        alarmManager.set(AlarmManager.RTC, cal.getTimeInMillis(), broadcast);
        alarmManager.set(AlarmManager.RTC, System.currentTimeMillis() * 1000, broadcast);
    }

    public static String encodeToString(String text) throws UnsupportedEncodingException {
        byte[] encrpt = text.getBytes(Constants.FILE_ENCODING_TYPE);
        return Base64.encodeToString(encrpt, Base64.DEFAULT);
    }

    public static String decodeToString(String text) throws UnsupportedEncodingException {
        byte[] decrypt = Base64.decode(text, Base64.DEFAULT);
        return new String(decrypt, Constants.FILE_ENCODING_TYPE);
    }

    public static boolean getQuizIntent(Activity activity, SharedPreferences sharedPreferences) {
        boolean isNew = false;
        try {
            if (RongoApp.getSelectedCrop().equalsIgnoreCase("11") || RongoApp.getSelectedCrop().equalsIgnoreCase("12")) {
                return true;
            }

            if (RongoApp.getSelectedCrop().equalsIgnoreCase("1")) {
                if (RongoApp.getSeasonName().equalsIgnoreCase(Constants.KHARIF_SEASON)) {
                    return true;
                } else {
                    return false;
                }
            }

            JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.getDailyQuizDataJSONFromAsset(activity));
            if (jsonObject != null && jsonObject.length() > 0) {
                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                    JSONObject data = jsonObject.optJSONObject("data");
                    if (data != null && data.length() > 0) {
                        JSONObject jsonObj = data.optJSONObject("result");
                        if (jsonObj != null && jsonObj.length() > 0) {
                            JSONObject json_data = jsonObj.getJSONObject("json_data");
                            if (json_data != null && json_data.length() > 0) {
                                Iterator<String> keys = json_data.keys();
                                while (keys.hasNext()) {
                                    String key = keys.next();
                                    String value = json_data.optString(key);
                                    if (key.equalsIgnoreCase(RongoApp.getSelectedCrop())) {
                                        JSONObject jsonLang = new JSONObject(value);
                                        if (jsonLang != null && jsonLang.length() > 0) {
                                            Iterator<String> keyLang = jsonLang.keys();
                                            while (keyLang.hasNext()) {
                                                String key1 = keyLang.next();
                                                String value1 = jsonLang.optString(key1);
                                                if (key1.equalsIgnoreCase(RongoApp.getSeasonName())) {
                                                    JSONObject jsonLang1 = new JSONObject(value1);
                                                    if (jsonLang1 != null && jsonLang1.length() > 0) {
                                                        Iterator<String> keyLang1 = jsonLang1.keys();
                                                        while (keyLang1.hasNext()) {
                                                            String key2 = keyLang1.next();
                                                            String value2 = jsonLang1.optString(key2);
                                                            if (key2.equalsIgnoreCase(RongoApp.getDefaultLanguage())) {
                                                                JSONArray jsonArray = new JSONArray(value2);
                                                                JSONObject resultData = jsonArray.getJSONObject(Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "2")) - 1);
                                                                JSONObject jsonObjects = resultData.getJSONObject(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, "0"));

                                                                String type = jsonObjects.optString("type");
                                                                if (!TextUtils.isEmpty(type)) {
//                                                                    intent = new Intent(homeActivity, QuizActivity.class);
                                                                    isNew = true;
                                                                } else {
                                                                    isNew = false;
//                                                                    intent = new Intent(homeActivity, QuestionSampleActivity.class);
                                                                }

                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return isNew;
    }

    public static boolean isHealthAvailable(SharedPreferences sharedPreferences, AppDatabase appDatabase) {
        boolean isHealth = false;
        try {
            if (sharedPreferences.getString(Constants.SharedPreference_Sowing_Done, "").equalsIgnoreCase("yes")) {
                String cropWeeks = sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, "0");
                int cropWeek = RongoApp.getCropWeekDaysFunction();
                if (cropWeek == Integer.parseInt(cropWeeks)) {
                    JSONObject WeeklyCropInfo = new JSONObject(RongoApp.getWeeklyCropQuestion());
                    if (!TextUtils.isEmpty(WeeklyCropInfo.toString())) {
                        String weekCropDays = Functions.getWeeklyCropDay(sharedPreferences);
                        if (!TextUtils.isEmpty(weekCropDays)) {
                            JSONObject quesCropWeek = null;
                            String[] arrayStr = weekCropDays.split(",");
                            for (String s : arrayStr) {
                                String weekCropDay = s.replace("\"", "");
                                quesCropWeek = Objects.requireNonNull(Functions.getWeeklyCropQuestion(sharedPreferences)).optJSONObject(weekCropDay);
                                if (!TextUtils.isEmpty(quesCropWeek.toString())) {
                                    List<com.rongoapp.data.WeeklyCropInfo> weeklyCropInfoCheck = appDatabase.weeklyCropInfoDao().getCropInfoByWeek(
                                            cropWeeks, RongoApp.getSelectedPCrop(), quesCropWeek.getString("info_type"));
                                    if (weeklyCropInfoCheck.size() > 0) {
//                                        bunaiStatusCard.setVisibility(View.GONE);
                                        isHealth = false;
                                    } else {
//                                        bunaiStatusCard.setVisibility(View.VISIBLE);
                                        isHealth = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isHealth;
    }

    public static String getAssetFileForWeeklyAdvisory(Activity activity, SharedPreferences sharedPreferences) {
        String weeklyAdvisoryData = "";
        try {
//            Log.e("User-Crop-Week:", "" + sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, "0"));
            JSONArray cropData = new JSONArray(FetchJsonFromAssetsFile.loadAdvisoryJSONFromAsset(activity));
            if (cropData != null && cropData.length() > 0) {
                for (int i = 0; i < cropData.length(); i++) {
                    JSONObject resultData = cropData.getJSONObject(i);
                    JSONObject result = resultData.optJSONObject("result");
                    if (i == Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, "0"))) {
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Weekly_Advisory, "" + result);
                        weeklyAdvisoryData = "" + result;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return weeklyAdvisoryData;
    }

    public static void performShare(Activity activity, AppDatabase appDatabase) {
        try {
            setUserScreenTrackEvent(appDatabase, "Invite", "More");
            String shareTxt = "" + HtmlCompat.fromHtml(RongoApp.getReferralText(activity), HtmlCompat.FROM_HTML_MODE_LEGACY);
            if (InternetConnection.checkConnectionForFasl(activity)) {
                shareCommunity(activity, shareTxt, "");
            } else {
                Functions.shareDefaultInviteIntent(activity);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Functions.shareDefaultInviteIntent(activity);
        }
    }

    public static void shareCommunity(Activity activity, String text, String url) {
        try {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    try {
                        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                        StrictMode.setVmPolicy(builder.build());

                        Intent share = new Intent(Intent.ACTION_SEND);
                        share.setType("image/jpeg");
                        share.putExtra(Intent.EXTRA_TEXT, text);
                        if (!TextUtils.isEmpty(url)) {
                            share.putExtra(Intent.EXTRA_STREAM, getImageUri(activity, getBitmapFromURL(activity, url)));
                        }
                        activity.startActivityForResult(Intent.createChooser(share, "Share Image"), Constants.MORE_SHARE_CODE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Functions.shareDefaultInviteIntent(activity);
        }
    }

    public static void shareSMSIntent(Activity activity, String text) {
        try {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());

            if (getDefaultSmsAppPackageName(activity) != null) {
                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("address", "");
                smsIntent.putExtra("sms_body", "" + HtmlCompat.fromHtml(text, 0));
                smsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivityForResult(smsIntent, Constants.SMS_SHARE_CODE);
            } else {
                Functions.shareDefaultInviteIntent(activity);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Functions.shareDefaultInviteIntent(activity);
        }
    }

    public static void shareWhatAppUrlIntent(Activity activity, String url) {
        try {
            Log.v("whatsApp-url", "" + url);
            if (InternetConnection.checkConnectionForFasl(activity)) {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());

                Intent share = new Intent(Intent.ACTION_SEND);
                share.setPackage("com.whatsapp");
                share.setType("image/*");
                if (!TextUtils.isEmpty(url)) {
                    share.putExtra(Intent.EXTRA_STREAM, getImageUri(activity, getBitmapFromURL(activity, url)));
                }
                share.putExtra(Intent.EXTRA_TEXT, "" + HtmlCompat.fromHtml(RongoApp.getReferralText(activity), HtmlCompat.FROM_HTML_MODE_LEGACY));
                share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                activity.startActivityForResult(share, Constants.WHATS_APP_SHARE_CODE); //1 is request code
            } else {
                shareDefaultInviteIntent(activity);
            }
        } catch (Exception e) {
            e.printStackTrace();
            shareDefaultInviteIntent(activity);
        }
    }

    public static void shareWhatAppIntent(Activity activity) {
        try {
            if (InternetConnection.checkConnectionForFasl(activity)) {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("image/*");
                share.setPackage("com.whatsapp");
                share.putExtra(Intent.EXTRA_STREAM, getImageUri(activity, getBitmapFromURL(activity, RongoApp.getReferralImage(activity))));
                share.putExtra(Intent.EXTRA_TEXT, "" + HtmlCompat.fromHtml(RongoApp.getReferralText(activity), HtmlCompat.FROM_HTML_MODE_LEGACY));
                share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivityForResult(share, Constants.WHATS_APP_SHARE_CODE); //1 is request code
            } else {
                shareDefaultInviteIntent(activity);
            }
        } catch (Exception e) {
            e.printStackTrace();
            shareDefaultInviteIntent(activity);
        }
    }

    public static void shareFacebookIntent(Activity activity) {
        try {
            if (InternetConnection.checkConnectionForFasl(activity)) {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());

                ShareLinkContent linkContent = new ShareLinkContent.Builder()
                        .setContentUrl(Uri.parse("https://rongoapp.in/app"))
                        .setShareHashtag(new ShareHashtag.Builder()
                                .setHashtag("" + HtmlCompat.fromHtml(RongoApp.getReferralText(activity), HtmlCompat.FROM_HTML_MODE_LEGACY))
                                .build())
                        .setQuote(activity.getResources().getString(R.string.str_download_msg))
                        .build();
                ShareDialog.show(activity, linkContent);
            } else {
                shareDefaultInviteIntent(activity);
            }
//            ShareDialog dialog = new ShareDialog(activity);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(activity, "facebook have not been installed.", Toast.LENGTH_SHORT).show();
            shareDefaultInviteIntent(activity);
        } catch (Exception e) {
            e.printStackTrace();
            shareDefaultInviteIntent(activity);
        }
    }

    public static void shareDefaultInviteIntent(Activity activity) {
        try {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    try {
                        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                        StrictMode.setVmPolicy(builder.build());

                        Intent share = new Intent(Intent.ACTION_SEND);
                        share.setType("image/*");
                        share.putExtra(Intent.EXTRA_TEXT, "" + HtmlCompat.fromHtml(RongoApp.getReferralText(activity), HtmlCompat.FROM_HTML_MODE_LEGACY));
                        share.putExtra(Intent.EXTRA_STREAM, getImageUri(activity, null));
                        activity.startActivityForResult(Intent.createChooser(share, "Share Image"), Constants.MORE_SHARE_CODE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static Bitmap getBitmapFromURL(Activity activity, String src) {
        try {
            if (TextUtils.isEmpty(src)) {
                src = RongoApp.getReferralImage(activity);
            }
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }

    public static Bitmap getBitmapFromURLNew(Activity activity, String src) {
        try {
            if (TextUtils.isEmpty(src)) {
                return BitmapFactory.decodeResource(activity.getResources(), R.drawable.placeholderbg);
            }
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return BitmapFactory.decodeResource(activity.getResources(), R.drawable.placeholderbg);
        }
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        try {
            if (inImage == null) {
                inImage = BitmapFactory.decodeResource(inContext.getResources(), R.drawable.app_share_image);
            }
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
            String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Rongo_Invite", null);
            return Uri.parse(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDefaultSmsAppPackageName(@NonNull Context context) {
        String defaultSmsPackageName;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(context);
            return defaultSmsPackageName;
        } else {
            Intent intent = new Intent(Intent.ACTION_VIEW)
                    .addCategory(Intent.CATEGORY_DEFAULT).setType("vnd.android-dir/mms-sms");
            final List<ResolveInfo> resolveInfos = context.getPackageManager().queryIntentActivities(intent, 0);
            if (resolveInfos != null && !resolveInfos.isEmpty())
                return resolveInfos.get(0).activityInfo.packageName;

        }
        return null;
    }

    public static String getFirebaseDynamicLink(Activity activity) {
        final String[] shareLink = new String[1];

        DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(Uri.parse("https://rongoapp.in/app/i/" + RongoApp.getUserId()))
                .setDomainUriPrefix("https://rongo.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().setMinimumVersion(1).build())
                .buildDynamicLink();  // Or buildShortDynamicLink()

        Uri dynamicLinkUri = dynamicLink.getUri();
        Log.e("++ dynamicLinkUri ++", "" + dynamicLinkUri);

        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(dynamicLinkUri)
                .setDomainUriPrefix("https://rongo.page.link")
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().setMinimumVersion(1).build())
                .buildShortDynamicLink()
                .addOnCompleteListener(activity, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            Uri shortLink = task.getResult().getShortLink();
                            Log.e("++ shortLink ++", "" + shortLink);
                            shareLink[0] = shortLink.toString();
                        }
                    }
                });

        return shareLink[0];
    }

    public static String getSeasonName(String cropId) {
        String seasonName = Constants.KHARIF_SEASON;
        if (cropId.equalsIgnoreCase("1")) {
            seasonName = Constants.KHARIF_SEASON;
        } else if (cropId.equalsIgnoreCase("2")) {
            seasonName = Constants.KHARIF_SEASON;
        } else if (cropId.equalsIgnoreCase("3")) {
            seasonName = Constants.KHARIF_SEASON;
        } else if (cropId.equalsIgnoreCase("4")) {
            seasonName = Constants.RABI_SEASON;
        } else if (cropId.equalsIgnoreCase("5")) {
            seasonName = Constants.KHARIF_SEASON;
        } else if (cropId.equalsIgnoreCase("6")) {
            seasonName = Constants.KHARIF_SEASON;
        } else if (cropId.equalsIgnoreCase("7")) {
            seasonName = Constants.RABI_SEASON;
        } else if (cropId.equalsIgnoreCase("8")) {
            seasonName = Constants.RABI_SEASON;
        } else if (cropId.equalsIgnoreCase("9")) {
            seasonName = Constants.RABI_SEASON;
        } else if (cropId.equalsIgnoreCase("10")) {
            seasonName = Constants.RABI_SEASON;
        } else if (cropId.equalsIgnoreCase("11")) {
            seasonName = Constants.KHARIF_SEASON;
        } else if (cropId.equalsIgnoreCase("12")) {
            seasonName = Constants.KHARIF_SEASON;
        } else if (cropId.equalsIgnoreCase("13")) {
            seasonName = Constants.KHARIF_SEASON;
        }
        return seasonName;
    }

    public static void disableEditText(EditText editText) {
        editText.setFocusable(false);
        editText.setEnabled(false);
        editText.setCursorVisible(false);
        editText.setKeyListener(null);
//        editText.setBackgroundColor(Color.TRANSPARENT);
    }


    public static String updateRewardPoints(SharedPreferences sharedPreferences, String points) {
        String rewardPoints = "0";
        try {
            rewardPoints = sharedPreferences.getString(Constants.SharedPreferences_REWARD_POINT, "0");
            rewardPoints = String.valueOf(Integer.parseInt(rewardPoints) + Integer.parseInt(points));
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_REWARD_POINT, "" + rewardPoints);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rewardPoints;
    }

    public static String deductRewardPoints(SharedPreferences sharedPreferences, String points) {
        String rewardPoints = "0";
        try {
            rewardPoints = sharedPreferences.getString(Constants.SharedPreferences_REWARD_POINT, "0");
            rewardPoints = String.valueOf(Integer.parseInt(rewardPoints) - Integer.parseInt(points));
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_REWARD_POINT, "" + rewardPoints);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rewardPoints;
    }

    public static List<RewardData> getAllRewardList(SharedPreferences sharedPreferences) {
        List<RewardData> RewardListData = new ArrayList<RewardData>();
        try {
            String RewardData = sharedPreferences.getString(Constants.SharedPreferences_REWARD_LIST, "");
            if (!TextUtils.isEmpty(RewardData)) {
                JSONArray jsonArray = new JSONArray(RewardData);
                if (jsonArray != null && jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jb = jsonArray.optJSONObject(i);
                        RewardData rewardData = new RewardData();
                        rewardData.set_id(i);
                        rewardData.setRewardId(jb.optString("id"));
                        rewardData.setRewardTitle(jb.optString("title"));
                        rewardData.setDescription(jb.optString("description"));
                        rewardData.setMetadata(jb.optString("reward"));
                        rewardData.setType(jb.optString("type"));
                        rewardData.setStatus(jb.optString("required_points"));
                        RewardListData.add(rewardData);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return RewardListData;
    }

    public static String getP_Crop_Id(SharedPreferences sharedPreferences) {
        String p_crop_id = "";
        try {
            ProfileResultData profileResultData = Functions.getData();
            if (profileResultData != null) {
                SeasonData seasonData = profileResultData.getSeasonData();
                if (seasonData != null) {
                    if (seasonData.getCropsDataList() != null && seasonData.getCropsDataList().size() > 0) {
                        List<CropsData> cropsData = seasonData.getCropsDataList();
                        if (cropsData != null && cropsData.size() > 0) {
                            for (int i = 0; i < cropsData.size(); i++) {
                                if (sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, "1").equalsIgnoreCase(cropsData.get(i).getCrop_id())) {
                                    p_crop_id = cropsData.get(i).getP_crop_id();
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return p_crop_id;
    }

    public static String getCommaSepPCropId(SharedPreferences sharedPreferences) {
        String p_crop_id = "";
        try {
            ProfileResultData profileResultData = Functions.getData();
            if (profileResultData != null) {
                SeasonData seasonData = profileResultData.getSeasonData();
                if (seasonData != null) {
                    if (seasonData.getCropsDataList() != null && seasonData.getCropsDataList().size() > 0) {
                        List<CropsData> cropsData = seasonData.getCropsDataList();
                        if (cropsData != null && cropsData.size() > 0) {
                            for (int i = 0; i < cropsData.size(); i++) {
                                if (p_crop_id.equalsIgnoreCase("")) {
                                    p_crop_id = cropsData.get(i).getP_crop_id();
                                } else {
                                    p_crop_id = p_crop_id + "," + cropsData.get(i).getP_crop_id();
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (p_crop_id.equalsIgnoreCase("")) {
            p_crop_id = sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, "");
        }
        return p_crop_id;
    }

    public static String getWeeklyCropDay(SharedPreferences sharedPreferences) {
        String cropDays = "";
        try {
            JSONObject WeeklyCropInfo = new JSONObject(RongoApp.getWeeklyCropQuestion());
            if (!TextUtils.isEmpty(WeeklyCropInfo.toString())) {
                JSONObject quesCrop = WeeklyCropInfo.optJSONObject(RongoApp.getSelectedCrop());
                if (!TextUtils.isEmpty(quesCrop.toString())) {
                    JSONObject quesCrop1 = quesCrop.optJSONObject(RongoApp.getSeasonName());
                    if (!TextUtils.isEmpty(quesCrop1.toString())) {
                        JSONArray jsonObject = quesCrop1.optJSONArray(RongoApp.getDefaultLanguage());
                        if (!TextUtils.isEmpty(jsonObject.toString())) {

                            JSONObject object = jsonObject.optJSONObject(0);
                            JSONArray cropWeek = object.optJSONArray("crop_week");
                            if (cropWeek != null) {
                                JSONArray jd = cropWeek.optJSONArray(Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, "0")));
                                if (jd != null) {
                                    cropDays = jd.toString().replace("[", "").replace("]", "").replace(", ", ",");
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cropDays;
    }

    public static JSONObject getWeeklyCropQuestion(SharedPreferences sharedPreferences) {
        JSONObject questionInfo = null;
        try {
            JSONObject WeeklyCropInfo = new JSONObject(RongoApp.getWeeklyCropQuestion());
            if (!TextUtils.isEmpty(WeeklyCropInfo.toString())) {
                JSONObject quesCrop = WeeklyCropInfo.optJSONObject(RongoApp.getSelectedCrop());
                JSONObject quesCrop1 = quesCrop.optJSONObject(RongoApp.getSeasonName());
                JSONArray jsonObject = quesCrop1.optJSONArray(RongoApp.getDefaultLanguage());
                JSONObject object = jsonObject.optJSONObject(0);
                questionInfo = object.optJSONObject("questions");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return questionInfo;
    }

    public static List<CropsData> getCropData(AppDatabase appDatabase, SharedPreferences sharedPreferences) {
        List<CropsData> cropData = new ArrayList<>();
        try {
            List<ProfileOfflineData> profileOfflineData = appDatabase.ProfileOfflineDao()
                    .getOfflineProfile(RongoApp.getSelectedCrop(), RongoApp.getSelectedPCrop());
            if (profileOfflineData != null && profileOfflineData.size() > 0) {
                CropsData cropsData1 = new CropsData();
                cropsData1.setField_id(profileOfflineData.get(0).getFieldId());
                cropsData1.setField_size(profileOfflineData.get(0).getFieldSize());
                cropsData1.setP_crop_id(profileOfflineData.get(0).getP_crop_id());
                cropsData1.setIs_sowing_done(profileOfflineData.get(0).getIs_sowing_done());
                cropsData1.setExpected_date_of_sowing(profileOfflineData.get(0).getExpectedSowingDate());
                cropsData1.setDate_of_sowing(profileOfflineData.get(0).getSowingDate());
                cropsData1.setSeed_type(profileOfflineData.get(0).getSeedType());
                cropsData1.setCrop_id(profileOfflineData.get(0).getCrop_id());
                cropsData1.setCrop_status(profileOfflineData.get(0).getCrop_status());
                cropsData1.setSeason_feedback(profileOfflineData.get(0).getSeason_feedback());
                cropsData1.setSeason_end_crop_week(profileOfflineData.get(0).getSeason_end_crop_week());
                cropsData1.setArea_unit(profileOfflineData.get(0).getArea_unit());
                cropsData1.setCrop_in_days("");
                cropsData1.setCrop_in_weeks("");
                cropsData1.setSeed_metadata("");

                ProfileResultData profileResultData = Functions.getData();
                if (profileResultData != null) {
                    SeasonData seasonData = profileResultData.getSeasonData();
                    if (seasonData != null) {
                        if (seasonData.getCropsDataList() != null && seasonData.getCropsDataList().size() > 0) {
                            List<CropsData> cropsData = seasonData.getCropsDataList();
                            if (cropsData != null && cropsData.size() > 0) {
                                for (int i = 0; i < cropsData.size(); i++) {
//                                    Log.e("+++lang++",RongoApp.getDefaultLanguage()+"-"+cropsData.get(i).getApp_language());
                                    if (RongoApp.getSelectedCrop().equalsIgnoreCase(cropsData.get(i).getCrop_id())) {
                                        if (RongoApp.getDefaultLanguage().contains(cropsData.get(i).getApp_language())) {
//                                            Log.e("+++lang1++",""+cropsData.get(i).getApp_language());
                                            cropsData1.setCropWeekDetailsLis(cropsData.get(i).getCropWeekDetailsLis());
                                            cropsData1.setGeo_latitude(cropsData.get(i).getGeo_latitude());
                                            cropsData1.setGeo_longitude(cropsData.get(i).getGeo_longitude());
                                            cropsData1.setGeo_metadata(cropsData.get(i).getGeo_metadata());
                                        } else {
                                            JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.getProfileDataJSONFromAsset(mContext));
                                            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                                JSONObject jData = jsonObject.optJSONObject("data");
                                                JSONObject jresult = jData.optJSONObject("result");
                                                JSONObject jseason = jresult.optJSONObject("season");
                                                JSONArray jsonArray = jseason.optJSONArray("crops");
                                                for (int k = 0; k < jsonArray.length(); k++) {
                                                    JSONObject jb_season = jsonArray.optJSONObject(k);
                                                    if (RongoApp.getSelectedCrop().equalsIgnoreCase(jb_season.optString("crop_id"))) {
                                                        if (RongoApp.getDefaultLanguage().equalsIgnoreCase(jb_season.optString("app_language"))) {
                                                            if (RongoApp.getSeasonName().equalsIgnoreCase(jb_season.optString("season_name"))) {

                                                                if (jresult != null && jresult.length() > 0) {
                                                                    JSONObject jseason1 = jresult.optJSONObject("season");
                                                                    if (jseason1 != null && jseason1.length() > 0) {
                                                                        JSONArray jsonArray1 = jseason1.optJSONArray("crops");
                                                                        if (jsonArray1 != null && jsonArray1.length() > 0) {
                                                                            for (int z = 0; z < jsonArray1.length(); z++) {
                                                                                JSONObject jb_season1 = jsonArray1.optJSONObject(z);
                                                                                if (RongoApp.getSelectedCrop().equalsIgnoreCase(jb_season1.optString("crop_id"))) {
                                                                                    String geo_latitude = jb_season1.optString("geo_latitude");
                                                                                    String geo_longitude = jb_season1.optString("geo_longitude");
                                                                                    String geo_metadata = jb_season1.optString("geo_metadata");

                                                                                    List<CropWeekDetails> cropWeekDetailsList = new ArrayList<>();
                                                                                    JSONArray cropWeekDetails = jb_season.getJSONArray("crop_week_detail");
                                                                                    if (cropWeekDetails != null && cropWeekDetails.length() > 0) {
                                                                                        for (int j = 0; j < cropWeekDetails.length(); j++) {
                                                                                            JSONObject jb = cropWeekDetails.optJSONObject(j);
                                                                                            int crop_week = jb.optInt("crop_week");
                                                                                            String stage_id = jb.optString("stage_id");
                                                                                            String stage_name = jb.optString("stage_name");
                                                                                            String display_name = jb.optString("display_name");
                                                                                            String stage_start_week = jb.optString("stage_start_week");
                                                                                            String stage_end_week = jb.optString("stage_end_week");
                                                                                            int is_new_stage = jb.optInt("is_new_stage");
                                                                                            cropWeekDetailsList.add(new CropWeekDetails(crop_week, stage_id, stage_name, display_name, stage_start_week, stage_end_week, is_new_stage));
                                                                                        }
                                                                                    }

                                                                                    cropsData1.setCropWeekDetailsLis(cropWeekDetailsList);
                                                                                    cropsData1.setGeo_latitude(geo_latitude);
                                                                                    cropsData1.setGeo_longitude(geo_longitude);
                                                                                    cropsData1.setGeo_metadata(geo_metadata);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                cropData.add(cropsData1);
            } else {
                ProfileResultData profileResultData = Functions.getData();
                if (profileResultData != null) {
                    SeasonData seasonData = profileResultData.getSeasonData();
                    if (seasonData != null) {
                        if (seasonData.getCropsDataList() != null && seasonData.getCropsDataList().size() > 0) {
                            List<CropsData> cropsData = seasonData.getCropsDataList();
                            if (cropsData != null && cropsData.size() > 0) {
                                for (int i = 0; i < cropsData.size(); i++) {
//                                    Log.e("+++lang2+++", RongoApp.getDefaultLanguage() + "-" + cropsData.get(i).getApp_language());
                                    if (RongoApp.getSelectedCrop().equalsIgnoreCase(cropsData.get(i).getCrop_id())) {
                                        if (RongoApp.getDefaultLanguage().equals(cropsData.get(i).getApp_language())) {
//                                            Log.e("+++lang3++", "" + cropsData.get(i).getApp_language());
                                            CropsData cropsData1 = new CropsData();
                                            cropsData1.setField_id(cropsData.get(i).getField_id());
                                            cropsData1.setArea_unit(cropsData.get(i).getArea_unit());
                                            cropsData1.setGeo_latitude(cropsData.get(i).getGeo_latitude());
                                            cropsData1.setGeo_longitude(cropsData.get(i).getGeo_longitude());
                                            cropsData1.setGeo_metadata(cropsData.get(i).getGeo_metadata());
                                            cropsData1.setField_size(cropsData.get(i).getField_size());
                                            cropsData1.setP_crop_id(cropsData.get(i).getP_crop_id());
                                            cropsData1.setIs_sowing_done(cropsData.get(i).getIs_sowing_done());
                                            cropsData1.setExpected_date_of_sowing(cropsData.get(i).getExpected_date_of_sowing());
                                            cropsData1.setDate_of_sowing(cropsData.get(i).getDate_of_sowing());
                                            cropsData1.setSeed_type(cropsData.get(i).getSeed_type());
                                            cropsData1.setCrop_in_days(cropsData.get(i).getCrop_in_days());
                                            cropsData1.setCrop_in_weeks(cropsData.get(i).getCrop_in_weeks());
                                            cropsData1.setCrop_id(cropsData.get(i).getCrop_id());
                                            cropsData1.setCropWeekDetailsLis(cropsData.get(i).getCropWeekDetailsLis());
                                            cropsData1.setSeed_metadata(cropsData.get(i).getSeed_metadata());
                                            cropsData1.setCrop_status(cropsData.get(i).getCrop_status());
                                            cropsData1.setSeason_feedback(cropsData.get(i).getSeason_feedback());
                                            cropsData1.setSeason_end_crop_week(cropsData.get(i).getSeason_end_crop_week());
                                            cropData.add(cropsData1);

                                        } else {
                                            JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.getProfileDataJSONFromAsset(mContext));
                                            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                                JSONObject jData = jsonObject.optJSONObject("data");
                                                JSONObject jresult = jData.optJSONObject("result");
                                                JSONObject jseason = jresult.optJSONObject("season");
                                                JSONArray jsonArray = jseason.optJSONArray("crops");
                                                for (int k = 0; k < jsonArray.length(); k++) {
                                                    JSONObject jb_season = jsonArray.optJSONObject(k);
                                                    if (RongoApp.getSelectedCrop().equalsIgnoreCase(jb_season.optString("crop_id"))) {
                                                        if (RongoApp.getDefaultLanguage().equalsIgnoreCase(jb_season.optString("app_language"))) {
                                                            if (RongoApp.getSeasonName().equalsIgnoreCase(jb_season.optString("season_name"))) {

                                                                if (jresult != null && jresult.length() > 0) {
                                                                    JSONObject jseason1 = jresult.optJSONObject("season");
                                                                    if (jseason1 != null && jseason1.length() > 0) {
                                                                        JSONArray jsonArray1 = jseason1.optJSONArray("crops");
                                                                        if (jsonArray1 != null && jsonArray1.length() > 0) {
                                                                            for (int z = 0; z < jsonArray1.length(); z++) {
                                                                                JSONObject jb_season1 = jsonArray1.optJSONObject(z);
                                                                                if (RongoApp.getSelectedCrop().equalsIgnoreCase(jb_season1.optString("crop_id"))) {
                                                                                    String geo_latitude = jb_season1.optString("geo_latitude");
                                                                                    String geo_longitude = jb_season1.optString("geo_longitude");
                                                                                    String geo_metadata = jb_season1.optString("geo_metadata");
                                                                                    String field_id = jb_season1.optString("field_id");
                                                                                    String area_unit = jb_season1.optString("area_unit");
                                                                                    String field_size = jb_season1.optString("field_size");
                                                                                    String seed_type = jb_season1.optString("seed_type");
                                                                                    String app_language = jb_season1.optString("app_language");

                                                                                    List<CropWeekDetails> cropWeekDetailsList = new ArrayList<>();
                                                                                    JSONArray cropWeekDetails = jb_season.getJSONArray("crop_week_detail");
                                                                                    if (cropWeekDetails != null && cropWeekDetails.length() > 0) {
                                                                                        for (int j = 0; j < cropWeekDetails.length(); j++) {
                                                                                            JSONObject jb = cropWeekDetails.optJSONObject(j);
                                                                                            int crop_week = jb.optInt("crop_week");
                                                                                            String stage_id = jb.optString("stage_id");
                                                                                            String stage_name = jb.optString("stage_name");
                                                                                            String display_name = jb.optString("display_name");
                                                                                            String stage_start_week = jb.optString("stage_start_week");
                                                                                            String stage_end_week = jb.optString("stage_end_week");
                                                                                            int is_new_stage = jb.optInt("is_new_stage");
                                                                                            cropWeekDetailsList.add(new CropWeekDetails(crop_week, stage_id, stage_name, display_name, stage_start_week, stage_end_week, is_new_stage));
                                                                                        }
                                                                                    }

                                                                                    CropsData cropsData1 = new CropsData();
                                                                                    cropsData1.setField_id(field_id);
                                                                                    cropsData1.setArea_unit(area_unit);
                                                                                    cropsData1.setGeo_latitude(geo_latitude);
                                                                                    cropsData1.setGeo_longitude(geo_longitude);
                                                                                    cropsData1.setGeo_metadata(geo_metadata);
                                                                                    cropsData1.setField_size(field_size);
                                                                                    cropsData1.setSeed_type(seed_type);
                                                                                    cropsData1.setApp_language(app_language);
                                                                                    cropsData1.setCropWeekDetailsLis(cropWeekDetailsList);

                                                                                    cropsData1.setP_crop_id(cropsData.get(i).getP_crop_id());
                                                                                    cropsData1.setIs_sowing_done(cropsData.get(i).getIs_sowing_done());
                                                                                    cropsData1.setExpected_date_of_sowing(cropsData.get(i).getExpected_date_of_sowing());
                                                                                    cropsData1.setDate_of_sowing(cropsData.get(i).getDate_of_sowing());
                                                                                    cropsData1.setSeed_type(cropsData.get(i).getSeed_type());
                                                                                    cropsData1.setCrop_in_days(cropsData.get(i).getCrop_in_days());
                                                                                    cropsData1.setCrop_in_weeks(cropsData.get(i).getCrop_in_weeks());
                                                                                    cropsData1.setCrop_id(cropsData.get(i).getCrop_id());
                                                                                    cropsData1.setSeed_metadata(cropsData.get(i).getSeed_metadata());
                                                                                    cropsData1.setCrop_status(cropsData.get(i).getCrop_status());
                                                                                    cropsData1.setSeason_feedback(cropsData.get(i).getSeason_feedback());
                                                                                    cropsData1.setSeason_end_crop_week(cropsData.get(i).getSeason_end_crop_week());
                                                                                    cropData.add(cropsData1);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }

                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cropData;
    }

    public static String getSessionEndCropWeek(String crop_id) {
        String sessionEndCropWeek = "0";
        try {
            ProfileResultData profileResultData = Functions.getData();
            if (profileResultData != null) {
                SeasonData seasonData = profileResultData.getSeasonData();
                if (seasonData != null) {
                    if (seasonData.getCropsDataList() != null && seasonData.getCropsDataList().size() > 0) {
                        List<CropsData> cropsData = seasonData.getCropsDataList();
                        if (cropsData != null && cropsData.size() > 0) {
                            for (int i = 0; i < cropsData.size(); i++) {
                                if (crop_id.equalsIgnoreCase(cropsData.get(i).getCrop_id())) {
                                    sessionEndCropWeek = cropsData.get(i).getSeason_end_crop_week();
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sessionEndCropWeek;
    }

    public static List<CropsData> getCropDataByID(String crop_id) {
        List<CropsData> cropData = new ArrayList<>();
        try {
            ProfileResultData profileResultData = Functions.getData();
            if (profileResultData != null) {
                SeasonData seasonData = profileResultData.getSeasonData();
                if (seasonData != null) {
                    if (seasonData.getCropsDataList() != null && seasonData.getCropsDataList().size() > 0) {
                        List<CropsData> cropsData = seasonData.getCropsDataList();
                        if (cropsData != null && cropsData.size() > 0) {
                            for (int i = 0; i < cropsData.size(); i++) {
                                if (crop_id.equalsIgnoreCase(cropsData.get(i).getCrop_id())) {
                                    CropsData cropsData1 = new CropsData();
                                    cropsData1.setField_id(cropsData.get(i).getField_id());
                                    cropsData1.setArea_unit(cropsData.get(i).getArea_unit());
                                    cropsData1.setGeo_latitude(cropsData.get(i).getGeo_latitude());
                                    cropsData1.setGeo_longitude(cropsData.get(i).getGeo_longitude());
                                    cropsData1.setGeo_metadata(cropsData.get(i).getGeo_metadata());
                                    cropsData1.setField_size(cropsData.get(i).getField_size());
                                    cropsData1.setP_crop_id(cropsData.get(i).getP_crop_id());
                                    cropsData1.setIs_sowing_done(cropsData.get(i).getIs_sowing_done());
                                    cropsData1.setExpected_date_of_sowing(cropsData.get(i).getExpected_date_of_sowing());
                                    cropsData1.setDate_of_sowing(cropsData.get(i).getDate_of_sowing());
                                    cropsData1.setSeed_type(cropsData.get(i).getSeed_type());
                                    cropsData1.setCrop_in_days(cropsData.get(i).getCrop_in_days());
                                    cropsData1.setCrop_in_weeks(cropsData.get(i).getCrop_in_weeks());
                                    cropsData1.setCrop_id(cropsData.get(i).getCrop_id());
                                    cropsData1.setCropWeekDetailsLis(cropsData.get(i).getCropWeekDetailsLis());
                                    cropsData1.setSeed_metadata(cropsData.get(i).getSeed_metadata());
                                    cropsData1.setCrop_status(cropsData.get(i).getCrop_status());
                                    cropsData1.setSeason_feedback(cropsData.get(i).getSeason_feedback());
                                    cropsData1.setSeason_end_crop_week(cropsData.get(i).getSeason_end_crop_week());
                                    cropData.add(cropsData1);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cropData;
    }

    public static List<CropsData> getProfileCropData() {
        List<CropsData> cropData = new ArrayList<>();
        try {
            ProfileResultData profileResultData = Functions.getData();
            if (profileResultData != null) {
                SeasonData seasonData = profileResultData.getSeasonData();
                if (seasonData != null) {
                    if (seasonData.getCropsDataList() != null && seasonData.getCropsDataList().size() > 0) {
                        List<CropsData> cropsData = seasonData.getCropsDataList();
                        if (cropsData != null && cropsData.size() > 0) {
                            cropData.addAll(cropsData);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cropData;
    }

    public static MasterCropData getMasterCropData(SharedPreferences sharedPreferences) {
        MasterCropData masterCropData = new MasterCropData();
        String json = sharedPreferences.getString(Constants.SharedPreferences_Master_Crop_List, "");
        if (!TextUtils.isEmpty(json)) {
            masterCropData = new Gson().fromJson(json, MasterCropData.class);
        }
        return masterCropData;
    }

    public static List<MasterCropData> getMasterCropList(SharedPreferences sharedPreferences) {
        List<MasterCropData> masterCropList = new ArrayList<>();
        String json = sharedPreferences.getString(Constants.SharedPreferences_Master_Crop_List, "");
        if (!TextUtils.isEmpty(json)) {
            Type type = new TypeToken<List<MasterCropData>>() {
            }.getType();
            masterCropList = new Gson().fromJson(json, type);
        }
        return masterCropList;
    }

    public static List<MasterCropData> getSelectedCropList(SharedPreferences sharedPreferences) {
        List<MasterCropData> profileCropList = new ArrayList<>();
        List<MasterCropData> masterCropList = Functions.getMasterCropList(sharedPreferences);
        List<CropsData> profileCropsData = Functions.getProfileCropData();
        for (int i = 0; i < masterCropList.size(); i++) {
            for (int j = 0; j < profileCropsData.size(); j++) {
                if (masterCropList.get(i).getCrop_id().equalsIgnoreCase(profileCropsData.get(j).getCrop_id())) {
                    profileCropList.add(new MasterCropData(masterCropList.get(i).getCrop_name(), masterCropList.get(i).getCrop_id(),
                            masterCropList.get(i).getCrop_image(), masterCropList.get(i).getDisplay_name(), "1"));
                }
            }
        }
        return profileCropList;
    }

    public static List<String> getSelectedCropIdList(SharedPreferences sharedPreferences) {
        List<String> profileCropList = new ArrayList<>();
        List<MasterCropData> masterCropList = Functions.getMasterCropList(sharedPreferences);
        List<CropsData> profileCropsData = Functions.getProfileCropData();
        for (int i = 0; i < masterCropList.size(); i++) {
            for (int j = 0; j < profileCropsData.size(); j++) {
                if (masterCropList.get(i).getCrop_id().equalsIgnoreCase(profileCropsData.get(j).getCrop_id())) {
                    profileCropList.add(masterCropList.get(i).getCrop_id());
                }
            }
        }
        return profileCropList;
    }


    public static void savePostData(HomeActivity homeActivity, SharedPreferences sharedPreferences, JSONArray jsonArray, int page) {
        try {
            List<PostData> postData = new ArrayList<PostData>();
//            if (page == 1) {
//            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Salah_Community, "");
//            }

            if (jsonArray != null && jsonArray.length() > 0) {
//                String oldCommunityList = sharedPreferences.getString(Constants.SharedPreferences_Salah_Community, "");
//                if (!TextUtils.isEmpty(oldCommunityList)) {
//                    postData = new ArrayList<PostData>();
//                    JSONArray oldArray = new JSONArray(oldCommunityList);
//                    postData = Functions.getPostData(homeActivity, oldArray);
//                }

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jb = jsonArray.optJSONObject(i);

                    String posted_user_name = jb.optString("posted_user_name");
                    String profile_picture = jb.optString("profile_picture");
                    String posted_user_location = jb.optString("posted_user_location");

                    String post_id = jb.optString("post_id");
                    String likes = jb.optString("likes");
                    String views = jb.optString("views");
                    String keyword = jb.optString("keyword");
                    String problem_related = jb.optString("problem_related");
                    String picture_key = jb.optString("picture_key");
                    String is_commented = jb.optString("is_commented");
                    String recording_key = jb.optString("recording_key");
                    String recording_local_path = jb.optString("recording_local_path");
                    String description = jb.optString("description");
                    String name = jb.optString("name");
                    String display_name = jb.optString("display_name");
                    String crop_age = jb.optString("crop_age");
                    String created_on = jb.optString("created_on");
                    String is_viewd = jb.optString("is_viewd");
                    String vote_type = jb.optString("vote_type");
                    String post_type = jb.optString("post_type");
                    String share_link = jb.optString("share_link");
                    String is_expert = jb.optString("is_expert");
                    String is_applied = jb.optString("is_applied");
                    String is_community_visible = jb.optString("is_community_visible");
                    String comments = "";
                    if (jb.has("comments")) {
                        JSONArray commentsArray = jb.optJSONArray("comments");
                        if (commentsArray != null && commentsArray.length() > 0) {
                            comments = commentsArray.toString();
                        }
                    }

                    postData.add(new PostData(new Random().nextInt(), post_id, keyword, problem_related, picture_key, description, name, likes, views, vote_type,
                            is_commented, display_name, crop_age, created_on, is_viewd, comments, SharedPreferencesUtils.getUserId(homeActivity),
                            recording_key, recording_local_path, posted_user_name, profile_picture, posted_user_location, post_type, share_link,
                            is_expert, is_community_visible, is_applied));
                }

                if (postData.size() > 0) {
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Salah_Community, "");
                    JSONArray community = new JSONArray(new Gson().toJson(postData, new TypeToken<ArrayList<PostData>>() {
                    }.getType()));
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Salah_Community, "" + community);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<PostData> getPostData(Context homeActivity, JSONArray jsonArray) {
        List<PostData> postData = new ArrayList<PostData>();
        try {
            if (jsonArray != null && jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jb = jsonArray.optJSONObject(i);

                    String posted_user_name = jb.optString("posted_user_name");
                    String profile_picture = jb.optString("profile_picture");
                    String posted_user_location = jb.optString("posted_user_location");

                    String post_id = jb.optString("post_id");
                    String likes = jb.optString("likes");
                    String views = jb.optString("views");
                    String keyword = jb.optString("keyword");
                    String problem_related = jb.optString("problem_related");
                    String picture_key = jb.optString("picture_key");
                    String is_commented = jb.optString("is_commented");
                    String recording_key = jb.optString("recording_key");
                    String recording_local_path = jb.optString("recording_local_path");
                    String description = jb.optString("description");
                    String name = jb.optString("name");
                    String display_name = jb.optString("display_name");
                    String crop_age = jb.optString("crop_age");
                    String created_on = jb.optString("created_on");
                    String is_viewd = jb.optString("is_viewd");
                    String vote_type = jb.optString("vote_type");
                    String post_type = jb.optString("post_type");
                    String share_link = jb.optString("share_link");
                    String is_expert = jb.optString("is_expert");
                    String is_applied = jb.optString("is_applied");
                    String is_community_visible = jb.optString("is_community_visible");
                    String comments = "";
                    if (jb.has("comments")) {
                        JSONArray commentsArray = jb.optJSONArray("comments");
                        if (commentsArray != null && commentsArray.length() > 0) {
                            comments = commentsArray.toString();
                        }
                    }

                    postData.add(new PostData((i + 1), post_id, keyword, problem_related, picture_key, description, name, likes, views, vote_type,
                            is_commented, display_name, crop_age, created_on, is_viewd, comments, SharedPreferencesUtils.getUserId(homeActivity),
                            recording_key, recording_local_path, posted_user_name, profile_picture, posted_user_location, post_type, share_link,
                            is_expert, is_community_visible, is_applied));
                }
            }
//            Log.e("+++postData+++", "" + postData.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return postData;
    }


    public static PostData getPostDataByPostId(Activity homeActivity, JSONArray jsonArray, String postId) {
        PostData postData = new PostData();
        try {
            if (jsonArray != null && jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jb = jsonArray.optJSONObject(i);
                    if (jb.optString("post_id").equalsIgnoreCase(postId)) {
                        String posted_user_name = jb.optString("posted_user_name");
                        String profile_picture = jb.optString("profile_picture");
                        String posted_user_location = jb.optString("posted_user_location");

                        String post_id = jb.optString("post_id");
                        String likes = jb.optString("likes");
                        String views = jb.optString("views");
                        String keyword = jb.optString("keyword");
                        String problem_related = jb.optString("problem_related");
                        String picture_key = jb.optString("picture_key");
                        String is_commented = jb.optString("is_commented");
                        String recording_key = jb.optString("recording_key");
                        String recording_local_path = jb.optString("recording_local_path");
                        String description = jb.optString("description");
                        String name = jb.optString("name");
                        String display_name = jb.optString("display_name");
                        String crop_age = jb.optString("crop_age");
                        String created_on = jb.optString("created_on");
                        String is_viewd = jb.optString("is_viewd");
                        String vote_type = jb.optString("vote_type");
                        String post_type = jb.optString("post_type");
                        String share_link = jb.optString("share_link");
                        String is_expert = jb.optString("is_expert");
                        String is_applied = jb.optString("is_applied");
                        String is_community_visible = jb.optString("is_community_visible");
                        String comments = "";
                        if (jb.has("comments")) {
                            JSONArray commentsArray = jb.optJSONArray("comments");
                            if (commentsArray != null && commentsArray.length() > 0) {
                                comments = commentsArray.toString();
                            }
                        }

                        postData = new PostData(i, post_id, keyword, problem_related, picture_key, description, name, likes, views, vote_type,
                                is_commented, display_name, crop_age, created_on, is_viewd, comments, SharedPreferencesUtils.getUserId(homeActivity),
                                recording_key, recording_local_path, posted_user_name, profile_picture, posted_user_location, post_type, share_link,
                                is_expert, is_community_visible, is_applied);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return postData;
    }


    public static void openCommenShareDialog(Activity homeActivity, String points, SharedPreferences sharedPreferences, AppDatabase appDatabase) {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_commen_share_dialog, null);
        bottomSheetDialog.setContentView(view);

        AppCompatTextView tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText(points);

        AppCompatTextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));

        AppCompatTextView tvShareWhatapp = view.findViewById(R.id.tvShareWhatapp);
        tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "WhatsApp");
                    Functions.shareWhatAppIntent(homeActivity);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(homeActivity, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        AppCompatTextView tvShareCopy = view.findViewById(R.id.tvShareCopy);
        tvShareCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "SMS");
                    Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                    smsIntent.setType("vnd.android-dir/mms-sms");
                    smsIntent.putExtra("address", "");
                    smsIntent.putExtra("sms_body", "" + HtmlCompat.fromHtml(sharedPreferences.getString(Constants.SharedPreference_Referral_Link, ""), 0));
                    smsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    homeActivity.startActivity(smsIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AppCompatTextView tvSharefacebook = view.findViewById(R.id.tvSharefacebook);
        tvSharefacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.setUserScreenTrackEvent(appDatabase, "Invite", "Facebook");
                Functions.shareFacebookIntent(homeActivity);
            }
        });

        AppCompatTextView tvShareMore = view.findViewById(R.id.tvShareMore);
        tvShareMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.performShare(homeActivity, appDatabase);
            }
        });

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(homeActivity.getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

//    public static String TextConvert(String inputText) {
////        https://www.zoftino.com/android-translate-text-example
//        final String[] translateText = {""};
//        FirebaseTranslatorOptions options = new FirebaseTranslatorOptions.Builder()
//                .setSourceLanguage(FirebaseTranslateLanguage.HI)
//                .setTargetLanguage(FirebaseTranslateLanguage.GU)
//                .build();
//        FirebaseTranslator translator = FirebaseNaturalLanguage.getInstance().getTranslator(options);
//        translator.translate(inputText).addOnSuccessListener(
//                new OnSuccessListener<String>() {
//                    @Override
//                    public void onSuccess(@NonNull String translatedText) {
//                        translateText[0] = translatedText;
//                    }
//                })
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        translateText[0] = inputText;
//                    }
//                });
//        return translateText[0];
//    }

}
