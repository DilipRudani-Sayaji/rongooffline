package com.rongoapp.controller;

import android.content.SharedPreferences;

import org.json.JSONArray;
import org.json.JSONObject;

public class MetaDataGenerate {

    public static String getDailyLoginMetaData(SharedPreferences sharedPreferences) {
        String userdata = "";
        try {
            JSONArray jsonArray = new JSONArray();
            JSONObject obj = new JSONObject();
            obj.put("reward_points", RewardPoints.getDailyLoginPoint(sharedPreferences));
            obj.put("metadata", "daily_login");
            obj.put("created_on", Functions.getCurrentDate());
            jsonArray.put(obj);
            userdata = jsonArray.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userdata;
    }

}
