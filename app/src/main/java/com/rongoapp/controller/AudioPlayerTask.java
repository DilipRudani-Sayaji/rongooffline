package com.rongoapp.controller;

import android.app.Activity;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.rongoapp.R;

public class AudioPlayerTask {

    public Activity activity;
    public MediaPlayer mMediaPlayer;
    public boolean isPlaying = false;
    public ImageView mPlayButton;

    public AudioPlayerTask(Activity _activity, ImageView _mPlayButton) {
        activity = _activity;
        mPlayButton = _mPlayButton;
        mMediaPlayer = new MediaPlayer();
    }

    public void onPlayMedia(String filePath) {
        try {
            mMediaPlayer = new MediaPlayer();
//            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mMediaPlayer.setAudioAttributes(new AudioAttributes.Builder().setContentType(AudioAttributes.CONTENT_TYPE_MUSIC).build());
            mMediaPlayer.setDataSource(filePath);
            mMediaPlayer.prepare();
            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    stopPlaying();
                }
            });

            mPlayButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onPlay(isPlaying, filePath);
                    isPlaying = !isPlaying;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Play start/stop
    public void onPlay(boolean isPlaying, String filePath) {
        if (!isPlaying) {
            //currently MediaPlayer is not playing audio
            if (mMediaPlayer == null) {
                startPlaying(filePath); //start from beginning
            } else {
                //resume the currently paused MediaPlayer
                mPlayButton.setImageResource(R.mipmap.pause);
                mMediaPlayer.start();
            }
        } else {
            //pause the MediaPlayer
            mPlayButton.setImageResource(R.mipmap.play);
            mMediaPlayer.pause();
        }
    }

    public void startPlaying(String filePath) {
        try {
            mPlayButton.setImageResource(R.mipmap.pause);
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(filePath);
            mMediaPlayer.prepare();
            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mMediaPlayer.start();
                }
            });
            mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    stopPlaying();
                }
            });
            //keep screen on while playing audio
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopPlaying() {
        try {
            if (mPlayButton !=null) {
                mPlayButton.setImageResource(R.mipmap.play);
            }
            if (mMediaPlayer !=null) {
                mMediaPlayer.stop();
                mMediaPlayer.reset();
            }
            isPlaying = !isPlaying;
            activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
