package com.rongoapp.controller;

import android.app.Activity;
import android.content.SharedPreferences;

import com.rongoapp.database.AppDatabase;

public class InternetSwitchTask extends AsyncTaskExecutor<String> {

    Activity mContext;
    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;

    public InternetSwitchTask(Activity context) {
        mContext = context;
        appDatabase = AppDatabase.getDatabase(mContext);
        sharedPreferences = mContext.getSharedPreferences(Constants.MediaPrefs, 0);
    }

    @Override
    protected Void doInBackground(String... params) {
        // Your long running task
        return null;
    }

    @Override
    protected void onPostExecute() {
        // update UI on task completed
    }

    @Override
    protected void onCancelled() {
        // update UI on task cancelled
    }

}