package com.rongoapp.controller;

public class Constants {

    public static final String MediaPrefs = "RongoApp";

    public static final String FILE_ENCODING_TYPE = "UTF-8";
    public static final String GOOGLE_TTS_ENGINE = "com.google.android.tts";

    public static final String GUJARAT_STATE = "Gujarat";
    public static final String MAHARASHTRA_STATE = "Maharashtra";

    public static final int POST_QUESTION_CODE = 111;
    public static final int WHATS_APP_SHARE_CODE = 10101;
    public static final int FACEBOOK_SHARE_CODE = 20202;
    public static final int MORE_SHARE_CODE = 30303;
    public static final int SMS_SHARE_CODE = 40404;

    public static final String English_Language_code = "en_US";
    public static final String Hindi_Language_code = "hi_IN";
    public static final String Gujarati_Language_code = "gu_IN";
    public static final String Marathi_Language_code = "mr_IN";

    public static final String English_Language = "english";
    public static final String Hindi_Language = "हिंदी";
    public static final String Gujarati_Language = "ગુજરાતી";
    public static final String Marathi_Language = "मराठी";

    public static final String Gujarat = "Gujarat";
    public static final String Maharashtra = "Maharashtra";

    public static final String MAIZE = "1";
    public static final String RICE = "2";
    public static final String COTTON = "3";
    public static final String WHEAT = "4";
    public static final String OKRA = "5";
    public static final String PEARMILLET = "6";
    public static final String CORIANDER = "7";
    public static final String BITTERGOURD = "8";
    public static final String BOTTLEGOURD = "9";
    public static final String WATERMELON = "10";
    public static final String GROUNDNUT = "11";
    public static final String SOYABEAN = "12";
    public static final String Castor = "13";

    public static final String ET_STORY_TRACK = "et_story_track";
    public static final String ET_INVITE = "et_invite";
    public static final String ET_RONGO = "et_rongo";
    public static final String ET_WEATHER = "et_weather";
    public static final String ET_EXPERT = "et_expert";
    public static final String ET_QUIZ = "et_quiz";
    public static final String ET_KARYA = "et_karya";
    public static final String ET_CROP_HEALTH = "et_crop_health";
    public static final String ET_SAVDHANI = "et_savdhani";

    public static final String KHARIF_SEASON = "kharif";
    public static final String RABI_SEASON = "rabi";

    public static final String ASSETS_QUIZ_IMAGE_PATH = "advisory_image/quiz";
    public static final String ASSETS_ADVISORY_JSON_PATH = "advisory_json/crop_";
    public static final String ASSETS_ADVISORY_KEYWORD_PATH = "advisory_keyword/";
    public static final String ASSETS_ADVISORY_CROP_IMAGE_PATH = "advisory_image/crop/";
    public static final String ASSETS_ADVISORY_KEYWORD_IMAGE_PATH = "advisory_image/keyword/";
    public static final String ASSETS_ADVISORY_WEATHER_IMAGE_PATH = "advisory_image/weather/";
    public static final String ASSETS_ADVISORY_KEYWORD_PRIORITY_PATH = "advisory_keyword_priority/crop_";

    public static final String NETWORK_TYPE = "2G";
    public static final String NETWORK_OFFLINE = "Offline";

    public static final String saveAdvisoryFeedback = "saveAdvisoryFeedback";
    public static final String saveExpertResponseFeedback = "saveExpertResponseFeedback";

    public static final String WEEKLY_CROP_INFO_QUE = "my.own.weeklyCropInfo";
    public static final String BROADCAST_QUE = "my.own.broadcast";
    public static final String PRAGATI_QUE = "my.own.broadcast.pragati";
    public static final String FEEDBACK_QUE = "my.own.broadcast.feedback";
    public static final String LUCKY_DRAW_QUE = "my.own.broadcast.LuckyDraw";
    public static final String JANKARI_QUE = "my.own.broadcast.jankari";
    public static final String QUIZ_QUE = "my.own.broadcast.quiz";
    public static final String COMMENT_QUE = "my.own.broadcast.comment";
    public static final String LIKE_QUE = "my.own.broadcast.like";

    public static final String OFFLINE_DATA = "offlineData";
    public static final String OFFLINE_GROWTH_DATA = "OfflineGrowthData";
    public static final String WEEKLY_CROP_INFO_DATA = "weeklyCropInfoData";
    public static final String FEEDBACK_DATA = "feedBackData";
    public static final String LUCKY_DRAW_DATA = "luckyDrawData";
    public static final String JANKARI_FEEDBACK_DATA = "jankariFeedBackData";
    public static final String OFFLINE_QUIZ_DATA = "offlineQuizData";
    public static final String OFFLINE_COMMENT_DATA = "offlineCommentData";
    public static final String OFFLINE_LIKE_DATA = "offlineLikeData";

    public static final String QUE_FEEDBACK_COUNT = "que_feedback_count";
    public static final String QUE_PRAGATI_COUNT = "que_pragati_count";
    public static final String QUE_LUCKY_DRAW_COUNT = "que_pragati_count";
    public static final String QUE_OFFLINE_COUNT = "que_offline_count";
    public static final String QUE_JANKARI_COUNT = "que_jankari_count";
    public static final String QUE_COMMENT_COUNT = "que_comment_count";
    public static final String QUE_LIKE_COUNT = "que_like_count";
    public static final String QUE_QUIZ_COUNT = "que_quiz_count";
    public static final String QUE_WEEKLY_COUNT = "que_weekly_count";

    //    private $global_image_path=BASE_PATH."assets/image/";
//    private $keyword_image_path=BASE_PATH."assets/image/advisory/keyword/";
//    private $advisory_image_path=BASE_PATH."assets/image/advisory/crop/";
//    private $upload_image_path=BASE_PATH."uploads/picture_taken/";
//    private $upload_audio_path=BASE_PATH."uploads/voice_recorded/";
    public static final String KEYWORD_IMAGE_PATH = "https://rongoapp.in/assets/image/advisory/keyword/";
    public static final String AUDIO_PATH_LINK = "https://rongoapp.in/uploads/voice_recorded/";
    public static final String IMAGE_PATH_LINK = "https://rongoapp.in//uploads//picture_taken/";

    public static final String Screen_Track = "App_Screen_Track";
    public static final String Click_Event = "App_Click_Event";

    public static final String SharedPreference_Protocol = "SharedPreference_Protocol";
    public static final String SharedPreference_Error_Log = "SharedPreference_Error_Log";

    public static final String SharedPreference_Default_Language = "SharedPreference_Default_Language";

    public static final String SharedPreference_Multi_Language_Info_status = "SharedPreference_Multi_Language_Info_status";
    public static final String SharedPreference_Multi_Crop_Info_status = "SharedPreference_Multi_Crop_Info_status";

    public static final String SharedPreference_First_Name = "SharedPreference_First_Name";
    public static final String SharedPreference_Last_Name = "SharedPreference_Last_Name";

    public static final String SharedPreference_Invite_Msg = "SharedPreference_Invite_Msg";

    public static final String SharedPreferences_story_data = "SharedPreferences_story_data";
    public static final String SharedPreferences_story_savdhani_count = "SharedPreferences_story_savdhani_count";
    public static final String SharedPreferences_story_count = "SharedPreferences_story_count";

    public static final String SharedPreference_profileResultData = "SharedPreference_profileResultData";

    public static final String SharedPreference_Community_Banner = "SharedPreference_Community_Banner";
    public static final String SharedPreference_internet_radio = "SharedPreference_internet_radio";

    public static final String SharedPreference_WhatsApp_Notif = "SharedPreference_WhatsApp_Notif";

    public static final String SharedPreference_Referral_by = "SharedPreference_Referral_by";
    public static final String SharedPreference_Referral_code = "SharedPreference_Referral_code";
    public static final String SharedPreference_Referral_Link = "SharedPreference_Referral_Link";
    public static final String SharedPreference_Referral_Image = "SharedPreference_Referral_Image";

    public static final String SharedPreference_App_Version = "SharedPreference_App_Version";
    public static final String SharedPreference_App_Update = "SharedPreference_App_Update";

    public static final String SharedPreference_Reward_Info_Dialog = "SharedPreference_Reward_Info_Dialog";
    public static final String SharedPreference_Reward = "SharedPreference_Reward";

    public static final String SharedPreference_RongoHelpLineNumber = "SharedPreference_RongoHelpLineNumber";
    public static final String SharedPreference_Salah_Counter = "SharedPreference_Salah_Counter";

    public static final String SharedPreference_Story_PostId = "SharedPreference_Story_PostId";

    public static final String SharedPreference_Salah_PostId = "SharedPreference_Salah_PostId";

    public static final String SharedPreference_IsAction = "SharedPreference_IsAction";
    public static final String SharedPreference_Sowing_Done = "SharedPreference_Sowing_Done";
    public static final String SharedPreference_SowingDate = "SharedPreference_sowingDate";
    public static final String SharedPreference_Expected_SowingDate = "SharedPreference_Expected_SowingDate";
    public static final String SharedPreference_WeeklyUploadPhoto = "SharedPreference_WeeklyUploadPhoto";

    public static final String SharedPreference_DynamicLink = "SharedPreference_DynamicLink";

    public static final String SharedPreference_LastLoginTime = "SharedPreference_LastLoginTime";

    public static final String SharedPreference_NetworkSpeed = "SharedPreference_NetworkSpeed";
    public static final String SharedPreference_NetworkType = "SharedPreference_NetworkType";

    public static final String SharedPreference_New_State = "SharedPreference_New_State";
    public static final String SharedPreference_New_City = "SharedPreference_New_City";
    public static final String SharedPreference_State = "SharedPreference_State";
    public static final String SharedPreference_City = "SharedPreference_City";
    public static final String SharedPreference_Country = "SharedPreference_Country";
    public static final String SharedPreference_Lat = "SharedPreference_Lat";
    public static final String SharedPreference_Long = "SharedPreference_Long";

    public static final String SharedPreferences_REWARD_POINT = "SharedPreferences_REWARD_POINT";
    public static final String SharedPreferences_Pending_Reward = "SharedPreferences_Pending_Reward";
    public static final String SharedPreferences_Earned_Reward = "SharedPreferences_Earned_Reward";
    public static final String SharedPreferences_How_Earn_Reward = "SharedPreferences_How_Earn_Reward";

    public static final String SharedPreference_DailyLogin_Date = "SharedPreference_DailyLogin_Date";
    public static final String SharedPreference_ASK_Helpful_Date = "SharedPreference_ASK_Helpful_Date";
    public static final String SharedPreference_Spin_Date = "SharedPreference_Spin_Date";

    public static final String SharedPreferences_CONTEST_ID = "SharedPreferences_CONTEST_ID";
    public static final String SharedPreferences_CONTEST = "SharedPreferences_CONTEST";
    public static final String SharedPreferences_SPIN_RESULT = "SharedPreferences_SPIN_RESULT";

    public static final String SharedPreferences_Pragati = "SharedPreferences_Pragati";
    public static final String SharedPreferences_REWARD_LIST = "SharedPreferences_REWARD_LIST";
    public static final String SharedPreferences_Quiz = "SharedPreferences_Quiz";

    public static final String SharedPreferences_ReportPost = "SharedPreferences_ReportPost";
    public static final String SharedPreferences_ReportComment = "SharedPreferences_ReportComment";

    public static final String SharedPreferences_AUDIO_PATH = "SharedPreferences_AUDIO_PATH";
    public static final String SharedPreferences_IMAGE_PATH = "SharedPreferences_IMAGE_PATH";

    public static final String SharedPreferences_CROP_WEEK_DAY = "SharedPreferences_CROP_WEEK_DAY";

    public static final String SharedPreferences_Salah_Tab = "SharedPreferences_Salah_Tab";
    public static final String SharedPreferences_Salah_Community = "SharedPreferences_Salah_Community";
    public static final String SharedPreferences_is_survey_visible = "SharedPreferences_is_survey_visible";
    public static final String SharedPreferences_survey_post_position = "SharedPreferences_survey_post_position";

    public static final String SharedPreferences_SessionEndCropID = "SharedPreferences_SessionEndCropID";

    public static final String SharedPreferences_Session_End_Data = "SharedPreferences_Session_End_Data";
    public static final String SharedPreferences_crop_status = "SharedPreferences_crop_status";
    public static final String SharedPreferences_season_feedback = "SharedPreferences_season_feedback";
    public static final String SharedPreferences_Submit_SessionEnd_DATA = "SharedPreferences_Submit_SessionEnd_DATA";
    public static final String SharedPreferences_season_end_crop_week = "SharedPreferences_season_end_crop_week";

    public static final String SharedPreferences_Question_DATE = "SharedPreferences_Question_DATE";
    public static final String SharedPreference_UserId = "SharedPreference_UserId";
    public static final String SharedPreference_community_interest = "SharedPreference_community_interest";
    public static final String SharedPreference_User_State = "SharedPreference_User_State";
    public static final String SharedPreference_Lucky_Draw = "SharedPreference_Lucky_Draw";
    public static final String SharedPreference_Time_FOR_API_CALL = "SharedPreference_Time_FOR_API_CALL";
    public static final String SharedPreference_Time_FOR_API_CALL_PROFILE = "SharedPreference_Time_FOR_API_CALL_PROFILE";
    public static final String SharedPreferences_FCM_TOKEN = "SharedPreferences_FCM_TOKEN";
    public static final String SharedPreferences_VERIFY_OTP = "SharedPreferences_VERIFY_OTP";
    public static final String SharedPreferences_MOBILE_NO = "SharedPreferences_MOBILE_NO";
    public static final String SharedPreferences_CROP_ID = "SharedPreferences_CROP_ID";
    public static final String SharedPreferences_Field_ID = "SharedPreferences_Field_ID";
    public static final String SharedPreferences_P_CROP_ID = "SharedPreferences_P_CROP_ID";
    public static final String SharedPreferences_SEASON_ID = "SharedPreferences_SEASON_ID";
    public static final String SharedPreferences_SEASON_NAME = "SharedPreferences_SEASON_NAME";
    public static final String SharedPreferences_CROP_WEEK = "SharedPreferences_CROP_WEEK";
    public static final String SharedPreferences_USER_CROP_WEEK = "SharedPreferences_USER_CROP_WEEK";
    public static final String SharedPreferences_JSON_KEYWORD = "SharedPreferences_JSON_KEYWORD";
    public static final String SharedPreferences_OPTIONS = "SharedPreferences_OPTIONS";
    public static final String SharedPreferences_SEED_TYPES = "SharedPreferences_SEED_TYPES";
    public static final String SharedPreferences_CROP_FOCUSED_AREA = "SharedPreferences_CROP_FOCUSED_AREA";
    public static final String SharedPreferences_LAST_SURVEY_ID = "SharedPreferences_LAST_SURVEY_ID";
    public static final String SharedPreferences_WEATHER_ALERT = "SharedPreferences_WEATHER_ALERT";
    public static final String SharedPreferences_WEATHER_ADVISORY = "SharedPreferences_WEATHER_ADVISORY";

    public static final String SharedPreferences_Number_Change = "SharedPreferences_Number_Change";
    public static final String SharedPreferences_Number_Update_Dialog = "SharedPreferences_Number_Update_Dialog";

    public static final String SharedPreferences_Weekly_Crop_Question = "SharedPreferences_Weekly_Crop_Question";
    public static final String SharedPreferences_Weekly_Advisory = "SharedPreferences_Weekly_Advisory";
    public static final String SharedPreferences_FertilizerInfo = "SharedPreferences_FertilizerInfo";
    public static final String SharedPreferences_WeeklyWeatherData = "SharedPreferences_WeeklyWeatherData";

    public static final String SharedPreferences_Master_Crop_List = "SharedPreferences_Master_Crop_List";
    public static final String SharedPreferences_StateCityData = "SharedPreferences_StateCityData";

    public static final String SharedPreferences_Weather_Show_DATE = "SharedPreferences_Weather_Show_DATE";

    public static final String ADVISORY_FEEDBACK_KIDS = "ADVISORY_FEEDBACK_KIDS";

    public static final int CAMERA_REQUEST_CODE = 100;
    public static final int GALLERY_REQUEST_CODE = 101;
    public static final int PERMISSION_READ_STORAGE = 2001;
    public static final int PERMISSION_CAMERA = 2002;
    public static final int PERMISSION_WRITE_STORAGE = 2003;
    public static final int PERMISSION_COARSE_LOCATION = 2004;
    public static final int PERMISSION_FINE_LOCATION = 2005;

    public static final String providerAuthority = "com.rongoapp.fileprovider";

    public static final String COMMENT = "comment";
    public static final String POST = "post";
    public static final String KEYWORD = "keyword";
    public static final String WEATHER = "weather";
    public static final String PAGE = "page";
    public static final String ADHIK = "adhik";
    public static final String SALAH = "salah";
    public static final String PRAGATI = "pragati";

    public static final String STORY_NAME = "STORY_NAME";
    public static final String STORY = "STORY";
    public static final String STORY_WEATHER = "STORY_WEATHER";
    public static final String STORY_COMMUNITY = "STORY_COMMUNITY";
    public static final String STORY_WEEKLY_QUIZ = "STORY_WEEKLY_QUIZ";
    public static final String STORY_QUIZ = "STORY_QUIZ";
    public static final String STORY_SAVDHANI = "STORY_SAVDHANI";
    public static final String STORY_CROP_HEALTH = "STORY_CROP_HEALTH";
    public static final String STORY_INVITE = "STORY_INVITE";
    public static final String STORY_TEST = "STORY_TEST";

    /*
     * Facebook Events Name & Param.
     * */
    public static final String FB_EVENT_NAME_REGISTRATION_METHOD = "Complete Registration";
    public static final String FB_EVENT_NAME_ACTIVATED_APP = "Activate App";
    public static final String FB_EVENT_NAME_SPENT_CREDITS = "Spent Credits";
    public static final String FB_EVENT_NAME_CONTACT = "Contact";
    public static final String FB_EVENT_NAME_FIND_LOCATION = "Find Location";
    public static final String FB_EVENT_NAME_RATED = "Rate";
    public static final String FB_EVENT_NAME_VIEWED_CONTENT = "View content";
    public static final String FB_EVENT_NAME_SCHEDULE = "EVENT_NAME_SCHEDULE";
    public static final String FB_EVENT_NAME_SEARCHED = "EVENT_NAME_SEARCHED";
    public static final String FB_EVENT_NAME_SUBSCRIBE = "EVENT_NAME_SUBSCRIBE";
    public static final String FB_EVENT_NAME_ACHIEVED_LEVEL = "EVENT_NAME_ACHIEVED_LEVEL";
    public static final String FB_EVENT_NAME_EVENT_NAME_AD_CLICK = "EVENT_NAME_AD_CLICK";
    public static final String FB_EVENT_NAME_ADDED_PAYMENT_INFO = "EVENT_NAME_ADDED_PAYMENT_INFO";
    public static final String FB_EVENT_NAME_ADDED_TO_WISHLIST = "EVENT_NAME_ADDED_TO_WISHLIST";
    public static final String FB_EVENT_NAME_CUSTOMIZE_PRODUCT = "EVENT_NAME_CUSTOMIZE_PRODUCT";
    public static final String FB_EVENT_NAME_SUBMIT_APPLICATION = "EVENT_NAME_SUBMIT_APPLICATION";
    public static final String FB_EVENT_NAME_COMPLETED_TUTORIAL = "EVENT_NAME_COMPLETED_TUTORIAL";
    public static final String FB_EVENT_NAME_INITIATED_CHECKOUT = "EVENT_NAME_INITIATED_CHECKOUT";
    public static final String FB_EVENT_NAME_UNLOCKED_ACHIEVEMENT = "EVENT_NAME_UNLOCKED_ACHIEVEMENT";

    public static final String FB_EVENT_PARAM_REGISTRATION_METHOD = "Registration Method";
    public static final String FB_EVENT_PARAM_MAX_RATING_VALUE = "Max. Rating Value";
    public static final String FB_EVENT_PARAM_CONTENT_TYPE = "Content Type";
    public static final String FB_EVENT_PARAM_CONTENT = "Content";
    public static final String FB_EVENT_PARAM_LEVEL = "EVENT_PARAM_LEVEL";
    public static final String FB_EVENT_PARAM_NUMBER = "EVENT_PARAM_NUMBER";
    public static final String FB_EVENT_PARAM_AD_TYPE = "EVENT_PARAM_AD_TYPE";
    public static final String FB_EVENT_PARAM_SUCCESS = "EVENT_PARAM_SUCCESS";
    public static final String FB_EVENT_PARAM_CONTENT_ID = "EVENT_PARAM_CONTENT_ID";
    public static final String FB_EVENT_PARAM_NUM_ITEMS = "EVENT_PARAM_NUM_ITEMS";
    public static final String FB_EVENT_PARAM_SEARCH_STRING = "EVENT_PARAM_SEARCH_STRING";
    public static final String FB_EVENT_PARAM_SUBSCRIPTION_ID = "EVENT_PARAM_SUBSCRIPTION_ID ";
    public static final String FB_EVENT_PARAM_DESCRIPTION = "EVENT_PARAM_DESCRIPTION";
    public static final String FB_EVENT_PARAM_ORDER_ID = "EVENT_PARAM_ORDER_ID";

    public static final String EVENT_NAME_PLAYED_SPIN_WHEEL = "PLAYED_SPIN_WHEEL";
    public static final String EVENT_NAME_SOWING_DETAIL = "SOWING_DETAIL";
    public static final String EVENT_NAME_ERROR_LOG = "ERROR_LOG";
    public static final String EVENT_NAME_ASK_TO_EXPERT = "ASK_TO_EXPERT";

    public static final String EVENT_PARAM_USER_ID = "USER_ID";
    public static final String EVENT_PARAM_DATE_TIME = "DATE_TIME";
    public static final String EVENT_PARAM_REWARD = "REWARD";
    public static final String EVENT_PARAM_CROP = "CROP";
    public static final String EVENT_PARAM_KEYWORD = "KEYWORD";
    public static final String EVENT_PARAM_SOWING_DATE = "SOWING_DATE";

    public static class BROADCAST_RECEIVER {
        public final static String NOTIFICATION_COUNT = "NOTIFICATION_COUNT";
        public final static String UPDATE_CROP_INFO = "UPDATE_CROP_INFO";
        public final static String AUDIO_RECORD_RECEIVER = "AUDIO_RECORD_RECEIVER";
        public final static String UPDATE_COMMUNITY_RECEIVER = "UPDATE_COMMUNITY_RECEIVER";
        public final static String UPDATE_TOOLTIP_VIEW = "UPDATE_TOOLTIP_VIEW";
        public final static String UPDATE_STORY = "UPDATE_STORY";
    }

}
