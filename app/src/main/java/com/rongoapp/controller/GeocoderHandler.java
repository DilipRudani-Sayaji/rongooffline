package com.rongoapp.controller;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class GeocoderHandler extends Handler {
    @Override
    public void handleMessage(Message message) {
        switch (message.what) {
            case 1:
                Bundle bundle = message.getData();
                String address = bundle.getString("address");
                Log.v("locationAddress", address);
                break;
            default:
                address = "";
        }
    }
}