package com.rongoapp.controller;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.rongoapp.R;
import com.rongoapp.utilities.RecordingService;

import java.io.File;
import java.util.List;

public class AudioRecorderDialog extends BottomSheetDialogFragment {

    public AudioRecorderCallback audiorecorderCallback;

    public AudioRecorderDialog(AudioRecorderCallback recorderCallback) {
        audiorecorderCallback = recorderCallback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setStyle(STYLE_NO_FRAME, R.style.SheetDialog);
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View sheetView = inflater.inflate(R.layout.voice_record_dialog, container, false);

        Chronometer mChronometer = (Chronometer) sheetView.findViewById(R.id.chronometer);

        Dexter.withContext(getActivity())
                .withPermissions(Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            File folder = new File(Environment.getExternalStorageDirectory() + "/SoundRecorder");
                            if (!folder.exists()) {
                                folder.mkdir();
                            }

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    onRecord(true, mChronometer);
                                }
                            }, 200);
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            dismiss();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();

        TextView tvStop = (TextView) sheetView.findViewById(R.id.tvStop);
        tvStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecord(false, mChronometer);
                audiorecorderCallback.onDone("1");
                dismiss();
            }
        });

        ImageView imgClose = (ImageView) sheetView.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audiorecorderCallback.onDone("0");
                dismiss();
            }
        });

        return sheetView;
    }

    //TODO: recording pause
    public void onRecord(boolean start, Chronometer mChronometer) {
        try {
            Intent intent = new Intent(getActivity(), RecordingService.class);
            if (start) {
                mChronometer.setBase(SystemClock.elapsedRealtime());
                mChronometer.start();
                getActivity().startService(intent);
                getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            } else {
                //stop recording
                mChronometer.stop();
                mChronometer.setBase(SystemClock.elapsedRealtime());
                getActivity().stopService(intent);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
