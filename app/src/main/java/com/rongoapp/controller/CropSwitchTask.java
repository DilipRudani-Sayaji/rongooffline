package com.rongoapp.controller;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.rongoapp.activities.LoginOptionsActivity;
import com.rongoapp.data.InsertHomeFeedData;
import com.rongoapp.data.PostData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.services.RetrofitClient;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CropSwitchTask extends AsyncTaskExecutor<String> {

    Activity mContext;
    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    CropSwitchCallback<String> callBack;
    String cropId;

    public CropSwitchTask(Activity context, String cropid, CropSwitchCallback<String> callback) {
        mContext = context;
        cropId = cropid;
        callBack = callback;
        appDatabase = AppDatabase.getDatabase(mContext);
        sharedPreferences = mContext.getSharedPreferences(Constants.MediaPrefs, 0);
    }

    @Override
    protected Void doInBackground(String... params) {
        // Your long running task
        RongoApp.getMasterCropData(false);
        RongoApp.getCityStateData(false);
        RongoApp.getMasterDropDownData(true);
        RongoApp.getFertilizerInfo(false);
        RongoApp.getDailyQuizData(false);
        RongoApp.getAllCropWeeklyQuestion(false);
        RongoApp.getMasterRewardPoints(false);
        RongoApp.getSessionEndData();
        RongoApp.getWeeklyWeather();
        RongoApp.getSurveyQuestions();
        RongoApp.getWeeklyAdvisory();
        RongoApp.getContestData();
        RongoApp.getPragatiData();
        RongoApp.getPosts();
        RongoApp.getContentData();
        getProfile();
        return null;
    }

    @Override
    protected void onPostExecute() {
        // update UI on task completed
        callBack.onComplete("1");
    }

    @Override
    protected void onCancelled() {
        // update UI on task cancelled
    }

    void getProfile() {
        RetrofitClient.getInstance().getApi().getProfile(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                Functions.getDeviceid(mContext), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                SharedPreferencesUtils.getUserId(mContext)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String r_str = response.body().string();
                        if (!TextUtils.isEmpty(r_str)) {
                            JSONObject jb = new JSONObject(r_str);
                            if (jb != null && jb.length() > 0) {
                                JSONObject jData = jb.optJSONObject("data");
                                if (jData != null && jData.length() > 0) {
                                    JSONObject jresult = jData.optJSONObject("result");
                                    if (jresult != null && jresult.length() > 0) {
                                        JSONObject jprofile = jresult.optJSONObject("profile");
                                        if (jprofile == null) {
                                            SharedPreferencesUtils.logout(sharedPreferences);
                                            Intent i = new Intent(mContext, LoginOptionsActivity.class);
                                            mContext.startActivity(i);
                                            mContext.finish();
                                        } else {
                                            new InsertHomeFeedData(jb).insertData();
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

}