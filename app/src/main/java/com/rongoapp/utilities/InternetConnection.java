package com.rongoapp.utilities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.rongoapp.R;
import com.rongoapp.controller.Functions;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class InternetConnection {

    public static boolean checkConnection(Activity context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connMgr != null) {
            NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();
            if (activeNetworkInfo != null) { // connected to the internet
                // connected to the mobile provider's data plan
                if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                    // connected to wifi
                    return true;
                } else return activeNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE;
            } else {
                Functions.showSnackBar(context.findViewById(android.R.id.content), context.getString(R.string.no_connection));
                return false;
            }
        }
        return false;
    }

    public static boolean checkConnectionForFasl(Activity context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connMgr != null) {
            NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();
            if (activeNetworkInfo != null) { // connected to the internet
                // connected to the mobile provider's data plan
                if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                    // connected to wifi
                    return true;
                } else return activeNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE;
            } else {
//                Functions.showSnackBar(context.findViewById(android.R.id.content),context.getString(R.string.no_connection));
                return false;
            }
        }
        return false;
    }

    public static boolean checkConnectionForFasl(Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connMgr != null) {
            NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();
            if (activeNetworkInfo != null) { // connected to the internet
                // connected to the mobile provider's data plan
                if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                    // connected to wifi
                    return true;
                } else return activeNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE;
            } else {
//                Functions.showSnackBar(context.findViewById(android.R.id.content),context.getString(R.string.no_connection));
                return false;
            }
        }
        return false;
    }

    /*
     * Internet Status & Info
     * */
    public static String getNetworkClass(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null || !info.isConnected())
            return "-"; // not connected
        if (info.getType() == ConnectivityManager.TYPE_WIFI)
            return "WIFI";
        if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
            int networkType = info.getSubtype();
            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN:     // api< 8: replace by 11
                case TelephonyManager.NETWORK_TYPE_GSM:      // api<25: replace by 16
                    return "2G";
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B:   // api< 9: replace by 12
                case TelephonyManager.NETWORK_TYPE_EHRPD:    // api<11: replace by 14
                case TelephonyManager.NETWORK_TYPE_HSPAP:    // api<13: replace by 15
                case TelephonyManager.NETWORK_TYPE_TD_SCDMA: // api<25: replace by 17
                    return "3G";
                case TelephonyManager.NETWORK_TYPE_LTE:      // api<11: replace by 13
                case TelephonyManager.NETWORK_TYPE_IWLAN:    // api<25: replace by 18
                case 19: // LTE_CA
                    return "4G";
//                case TelephonyManager.NETWORK_TYPE_NR:       // api<29: replace by 20
//                    return "5G";
                default:
                    return "?";
            }
        }
        return "?";
    }

    public String ConnectionQuality(Context context) {
        NetworkInfo info = getInfo(context);
        if (info == null || !info.isConnected()) {
            return "UNKNOWN";
        }

        if (info.getType() == ConnectivityManager.TYPE_WIFI) {
            WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            int numberOfLevels = 5;
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            int level = WifiManager.calculateSignalLevel(wifiInfo.getRssi(), numberOfLevels);
            if (level == 2)
                return "POOR";
            else if (level == 3)
                return "MODERATE";
            else if (level == 4)
                return "GOOD";
            else if (level == 5)
                return "EXCELLENT";
            else
                return "UNKNOWN";
        } else if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
            int networkClass = getNetworkClass(getNetworkType(context));
            if (networkClass == 1)
                return "POOR";
            else if (networkClass == 2)
                return "GOOD";
            else if (networkClass == 3)
                return "EXCELLENT";
            else
                return "UNKNOWN";
        } else
            return "UNKNOWN";
    }

    public static String getNetworkSpeed(Context context) {
        String speed = "0 kbps";
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo info = cm.getActiveNetworkInfo();
            if (info.getType() == ConnectivityManager.TYPE_WIFI) {
                WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                int numberOfLevels = 5;
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                int level = WifiManager.calculateSignalLevel(wifiInfo.getRssi(), numberOfLevels);
                if (level == 2) {
//                return "POOR";
                    speed = "0-150 kbps";
                } else if (level == 3) {
//                return "MODERATE";
                    speed = "150-550 kbps";
                } else if (level == 4) {
//                return "GOOD";
                    speed = "550-2000 kbps";
                } else if (level == 5) {
//                return "EXCELLENT";
                    speed = "2000-5000 kbps";
                } else {
//                return "UNKNOWN";
                    speed = "0 kbps";
                }
            } else if (info.getType() == ConnectivityManager.TYPE_MOBILE) {
                // check NetworkInfo subtype
                if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_GPRS) {
                    // Bandwidth between 100 kbps and below
                    speed = "0-100 Kbps";
                } else if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_EDGE) {
                    // Bandwidth between 50-100 kbps
                    speed = "50-100 kbps";
                } else if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_EVDO_0) {
                    // Bandwidth between 400-1000 kbps
                    speed = "400-1000 kbps";
                } else if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_EVDO_A) {
                    // Bandwidth between 600-1400 kbps
                    speed = "600-1400 kbps";
                } else if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_1xRTT) {
                    // Bandwidth between 50-100 kbps
                    speed = "50-100 kbps";
                } else if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_CDMA) {
                    // Bandwidth between 14-64 kbps
                    speed = "14-64 kbps";
                } else if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_HSDPA) {
                    // Bandwidth between 2-14 Mbps
                    speed = "2-14 Mbps";
                } else if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_HSPA) {
                    // Bandwidth between 700-1700 kbps
                    speed = "700-1700 kbps";
                } else if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_HSUPA) {
                    // Bandwidth between 1-23 Mbps
                    speed = "1-23 Mbps";
                } else if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_UMTS) {
                    // Bandwidth between 400-7000 kbps
                    speed = "400-7000 kbps";
                } else if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_LTE) {
                    // Bandwidth between 400-7000 kbps
                    speed = "400-7000 kbps";
                } else if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_NR) {
                    // Bandwidth between 400-7000 kbps
                    speed = "400-7000 kbps";
                } else if (info.getSubtype() == TelephonyManager.NETWORK_TYPE_UNKNOWN) {
                    // Bandwidth between Unknown
                    speed = "0 kbps";
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            speed = "0 kbps";
        }
//        NetworkCapabilities nc = cm.getNetworkCapabilities(cm.getActiveNetwork());
//        int downSpeed = nc.getLinkDownstreamBandwidthKbps();
//        int upSpeed = nc.getLinkUpstreamBandwidthKbps();
//        Log.e("downSpeed", "==" + downSpeed + "---" + "upSpeed" + "==" + upSpeed);
//        Log.e("Wifi Level: ", "" + getWifiLevel(context));
        Log.d("speed: ", "" + speed);
        return speed;
    }

    public static int getWifiLevel(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        int linkSpeed = wifiManager.getConnectionInfo().getRssi();
        int level = WifiManager.calculateSignalLevel(linkSpeed, 5);
        return level;
    }

    public NetworkInfo getInfo(Context context) {
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
    }

    public int getNetworkClass(int networkType) {
        try {
            return getNetworkClassReflect(networkType);
        } catch (Exception ignored) {
        }

        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_GPRS:
            case 16: // TelephonyManager.NETWORK_TYPE_GSM:
            case TelephonyManager.NETWORK_TYPE_EDGE:
            case TelephonyManager.NETWORK_TYPE_CDMA:
            case TelephonyManager.NETWORK_TYPE_1xRTT:
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return 1;
            case TelephonyManager.NETWORK_TYPE_UMTS:
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
            case TelephonyManager.NETWORK_TYPE_HSDPA:
            case TelephonyManager.NETWORK_TYPE_HSUPA:
            case TelephonyManager.NETWORK_TYPE_HSPA:
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
            case TelephonyManager.NETWORK_TYPE_EHRPD:
            case TelephonyManager.NETWORK_TYPE_HSPAP:
            case 17: // TelephonyManager.NETWORK_TYPE_TD_SCDMA:
                return 2;
            case TelephonyManager.NETWORK_TYPE_LTE:
            case 18: // TelephonyManager.NETWORK_TYPE_IWLAN:
                return 3;
            default:
                return 0;
        }
    }

    private int getNetworkClassReflect(int networkType) throws
            NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method getNetworkClass = TelephonyManager.class.getDeclaredMethod("getNetworkClass", int.class);
        if (!getNetworkClass.isAccessible()) {
            getNetworkClass.setAccessible(true);
        }
        return (Integer) getNetworkClass.invoke(null, networkType);
    }

    public static int getNetworkType(Context context) {
        return ((TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE)).getNetworkType();
    }

}
