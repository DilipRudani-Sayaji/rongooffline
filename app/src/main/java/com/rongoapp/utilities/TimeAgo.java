package com.rongoapp.utilities;

import android.content.Context;

import com.rongoapp.R;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TimeAgo {

    Context context;
    List<Long> times;
    List<String> timesString;

    public TimeAgo(Context context) {
        this.context = context;
        times = Arrays.asList(
                TimeUnit.DAYS.toMillis(365),
                TimeUnit.DAYS.toMillis(30),
                TimeUnit.DAYS.toMillis(1),
                TimeUnit.HOURS.toMillis(1),
                TimeUnit.MINUTES.toMillis(1),
                TimeUnit.SECONDS.toMillis(1));

        timesString = Arrays.asList(context.getString(R.string.strYear), context.getString(R.string.strMonth), context.getString(R.string.strDay), context.getString(R.string.strHour), context.getString(R.string.strMinute), context.getString(R.string.strSecond));
    }

    public String toDuration(long millisec) {
        long todayMilli = Calendar.getInstance().getTimeInMillis();
        long duration = todayMilli - millisec;
        StringBuffer res = new StringBuffer();
        for (int i = 0; i < times.size(); i++) {
            Long current = times.get(i);
            long temp = duration / current;
            if (temp > 0) {
                res.append(temp)./*append(" ").*/append(timesString.get(i) /*)*//*.append(temp > 1 ? context.getString(R.string.strS) : "")
                        .append(" " + context.getString(R.string.strAgo)*/);
                break;
            }
        }
        if ("".equals(res.toString()))
            return context.getString(R.string.strJustNow);
        else
            return res.toString();
    }

    public static void main(String args[]) {
    }

    /**
     * Function to convert millisecond in human readable date format.
     *
     * @param millisec contains millisecond value.
     * @return human readable format date.
     */
    public String toGetDuration(long millisec) {
        millisec = millisec * 1000;
        long todayMilli = Calendar.getInstance().getTimeInMillis();
        long duration = todayMilli - millisec;
        StringBuffer res = new StringBuffer();
        for (int i = 0; i < times.size(); i++) {
            Long current = times.get(i);
            long temp = duration / current;
            if (temp > 0) {
                res.append(temp).append(" ").append(timesString.get(i)).append(temp > 1 ? context.getString(R.string.strS) : "").append(" " + context.getString(R.string.strAgo));
                break;
            }
        }
        if ("".equals(res.toString()))
            return context.getString(R.string.strJustNow);
        else
            return res.toString();
    }

}
