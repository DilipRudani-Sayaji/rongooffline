package com.rongoapp.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.util.Log;

import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.SharedPreferencesUtils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Calendar;

public class ExceptionHandler implements Thread.UncaughtExceptionHandler {

    private final Context mContext;
    private final String LINE_SEPARATOR = "\n";

    SharedPreferences sharedPreferences;

    public ExceptionHandler(Context context) {
        mContext = context;
        sharedPreferences = context.getSharedPreferences(Constants.MediaPrefs, 0);
    }

    public String getAppVersion() {
        String version = "";
        try {
            PackageInfo pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return version;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable exception) {
        try {
            StringWriter stackTrace = new StringWriter();
            exception.printStackTrace(new PrintWriter(stackTrace));
            Calendar cal = Calendar.getInstance();

            StringBuilder errorReport = new StringBuilder();
            errorReport.append("***** LOCAL CAUSE OF ERROR (TSH) Version: " + getAppVersion() + " Date: " + cal.getTime() + " *****\n\n");
            errorReport.append("Localized Error Message: ");
            errorReport.append(exception.getLocalizedMessage());
            errorReport.append("Error Message: ");
            errorReport.append(exception.getMessage());
            errorReport.append("StackTrace");
            errorReport.append(stackTrace.toString());

            errorReport.append("\n************ DEVICE INFORMATION ***********\n");
            errorReport.append("Brand: ");
            errorReport.append(Build.BRAND);
            errorReport.append(LINE_SEPARATOR);
            errorReport.append("Device: ");
            errorReport.append(Build.DEVICE);
            errorReport.append(LINE_SEPARATOR);
            errorReport.append("Model: ");
            errorReport.append(Build.MODEL);
            errorReport.append(LINE_SEPARATOR);
            errorReport.append("Id: ");
            errorReport.append(Build.ID);
            errorReport.append(LINE_SEPARATOR);
            errorReport.append("Product: ");
            errorReport.append(Build.PRODUCT);
            errorReport.append(LINE_SEPARATOR);

            errorReport.append("\n************ FIRMWARE ************\n");
            errorReport.append("SDK: ");
            errorReport.append(Build.VERSION.SDK);
            errorReport.append(LINE_SEPARATOR);
            errorReport.append("Release: ");
            errorReport.append(Build.VERSION.RELEASE);
            errorReport.append(LINE_SEPARATOR);
            errorReport.append("Incremental: ");
            errorReport.append(Build.VERSION.INCREMENTAL);
            errorReport.append(LINE_SEPARATOR);
            Log.i("ExceptionHandler", "" + errorReport.toString());

            SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                    Constants.SharedPreference_Error_Log, "" + errorReport.toString());
            Functions.setFirebaseErrorLog(errorReport.toString());

            Log.e("System Exit", "System Exit 10");
//            Debug.trace("Exception fire");
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(10);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}