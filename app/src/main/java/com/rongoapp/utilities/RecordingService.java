package com.rongoapp.utilities;

import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;

import com.rongoapp.controller.Constants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.TimerTask;

public class RecordingService extends Service {

    private static final String LOG_TAG = "RecordingService";

    MediaRecorder mRecorder = null;
    TimerTask mIncrementTimerTask = null;
    String mFileName = null, mFilePath = null;
    Uri audioUri;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startRecording();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (mRecorder != null) {
            stopRecording();
        }
        super.onDestroy();
    }

    public void startRecording() {
        try {
            mFileName = "Recording" + "_" + System.currentTimeMillis() + ".mp3";
            ContentValues values = new ContentValues();
            values.put(MediaStore.Audio.Media.DISPLAY_NAME, mFileName);
            values.put(MediaStore.Audio.Media.TITLE, "Recording_Audio");
            values.put(MediaStore.Audio.Media.DATE_ADDED, (int) (System.currentTimeMillis() / 1000));
            values.put(MediaStore.Audio.Media.MIME_TYPE, "audio/mp3");
            values.put(MediaStore.Audio.Media.RELATIVE_PATH, "Music/Recordings/");
            audioUri = getContentResolver().insert(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, values);
            ParcelFileDescriptor file = getContentResolver().openFileDescriptor(audioUri, "w");
            mRecorder = new MediaRecorder();
            mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mRecorder.setOutputFile(file.getFileDescriptor());
            mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mRecorder.setAudioChannels(1);
            mRecorder.prepare();
            mRecorder.start();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(LOG_TAG, "prepare() failed");
        }
    }

    public void stopRecording() {
        try {
            if (mRecorder != null) {
                mRecorder.stop();
                mRecorder.reset();

                //remove notification
                if (mIncrementTimerTask != null) {
                    mIncrementTimerTask.cancel();
                    mIncrementTimerTask = null;
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    File testFile = new File(Environment.getExternalStorageDirectory().getPath(), "Music/Recordings/");
                    mFilePath = testFile.getAbsolutePath() + "/" + mFileName;
                } else {
                    File file = getFile(getApplicationContext(), audioUri);
                    mFilePath = file.getAbsolutePath();
                }

                Intent intent = new Intent();
                intent.setAction(Constants.BROADCAST_RECEIVER.AUDIO_RECORD_RECEIVER);
                intent.putExtra("mFileName", mFileName);
                intent.putExtra("mFilePath", mFilePath);
                getApplicationContext().sendBroadcast(intent);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(LOG_TAG, "exception", e);
        }
    }

    public static File getFile(Context context, Uri uri) throws IOException {
        File destinationFilename = new File(context.getFilesDir().getPath() + File.separatorChar + queryName(context, uri));
        try (InputStream ins = context.getContentResolver().openInputStream(uri)) {
            createFileFromStream(ins, destinationFilename);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return destinationFilename;
    }

    public static void createFileFromStream(InputStream ins, File destination) {
        try (OutputStream os = new FileOutputStream(destination)) {
            byte[] buffer = new byte[4096];
            int length;
            while ((length = ins.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
            os.flush();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static String queryName(Context context, Uri uri) {
        Cursor returnCursor = context.getContentResolver().query(uri, null, null, null, null);
        assert returnCursor != null;
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        returnCursor.moveToFirst();
        String name = returnCursor.getString(nameIndex);
        returnCursor.close();
        return name;
    }

}
