package com.rongoapp.utilities;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Debug {

    private static final String TAG = "TAG";
    public static boolean isDebug = true;
    public static boolean isPersistant = false;

    public static void trace(final String msg) {
        if (isDebug) {
            Log.i(TAG, msg);
            if (isPersistant) {
                appendLog(msg);
            }
        }
    }

    public static void trace(final String tag, final String msg) {
        if (isDebug) {
            Log.i(tag, msg);
        }
    }

    public static void appendLog(final String text) {
        final File logFile = new File(Environment.getExternalStorageDirectory() + "/tsh.txt");
        if (!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch (final IOException e) {
                e.printStackTrace();
            }
        }
        try {
            // BufferedWriter for performance, true to set append to file flag
            final BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(text);
            buf.newLine();
            buf.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
}
