package com.rongoapp.utilities;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.rongoapp.R;
import com.rongoapp.activities.MainActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class SaveDocumentToSD {

    /*
* TODO:  Receipt Name Format --> for DOWNLOAD IMAGES
        Document & Image : FILENAME_USERID_TIMESTAMP.pdf
* */
    public static void saveImage(Activity activity, String fileName, Bitmap image) {
        try {
            String savedImagePath = "";
            String imageFileName = fileName + "_" + 1 + "_" + System.currentTimeMillis() + ".JPEG";
            File storageDir = new File(Environment.getExternalStorageDirectory() + File.separator + "Rongo" + File.separator + "Document");
            boolean success = true;
            if (!storageDir.exists()) {
                success = storageDir.mkdirs();
            }
            if (success) {
                File imageFile = new File(storageDir, imageFileName);
                savedImagePath = imageFile.getAbsolutePath();
                try {
                    OutputStream fOut = new FileOutputStream(imageFile);
                    image.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                    fOut.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // Add the image to the system gallery
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                File f = new File(savedImagePath);
                Uri contentUri = Uri.fromFile(f);
                mediaScanIntent.setData(contentUri);
                activity.sendBroadcast(mediaScanIntent);

                sendNotification(activity, storageDir, imageFileName);

                Toast.makeText(activity, "Your " + fileName + " Save Successfully.", Toast.LENGTH_SHORT).show();
//                Crouton.makeText(activity, "Your " + fileName + " Save Successfully.", Style.CONFIRM).show();
            }
//            return savedImagePath;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static GetFilePathAndStatus getFileFromBase64AndSaveInSDCard(String base64, String filename, String extension) {
        GetFilePathAndStatus getFilePathAndStatus = new GetFilePathAndStatus();
        try {
            byte[] pdfAsBytes = Base64.decode(base64, 0);
            FileOutputStream os;
            os = new FileOutputStream(getReportPath(filename, extension), false);
            os.write(pdfAsBytes);
            os.flush();
            os.close();
            getFilePathAndStatus.filStatus = true;
            getFilePathAndStatus.filePath = getReportPath(filename, extension);
            return getFilePathAndStatus;
        } catch (IOException e) {
            e.printStackTrace();
            getFilePathAndStatus.filStatus = false;
            getFilePathAndStatus.filePath = getReportPath(filename, extension);
            return getFilePathAndStatus;
        }
    }

    public static String getReportPath(String filename, String extension) {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "TSH/Document");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + filename + "." + extension);
        Log.e("x-x-x-->", "" + uriSting);
        return uriSting;

    }

    public static class GetFilePathAndStatus {
        public boolean filStatus;
        public String filePath;
    }


    public static void sendNotification(Activity mActivity, File path, String message) {
        try {
            File file = new File(path, message);
//            Log.e("file path...", "" + file.getAbsolutePath());

            Intent intent = new Intent(mActivity, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.setAction(file.getAbsolutePath());
//            intent.setAction(Long.toString(System.currentTimeMillis()));

            PendingIntent pendingIntent = PendingIntent.getActivity(mActivity, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mActivity, "27")
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(BitmapFactory.decodeResource(mActivity.getResources(), R.mipmap.ic_launcher))
                    .setContentTitle(message)
                    .setContentText("")
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setAutoCancel(true)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) mActivity.getSystemService(Context.NOTIFICATION_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel mChannel = new NotificationChannel("27", "All Notification", NotificationManager.IMPORTANCE_HIGH);
                mChannel.setDescription("");
                mChannel.enableLights(true);
                mChannel.setShowBadge(true);
                mChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
                notificationManager.createNotificationChannel(mChannel);
            }

            notificationManager.notify(ServiceUtility.randInt(0, 9999999), notificationBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
