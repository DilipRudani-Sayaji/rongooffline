package com.rongoapp.utilities;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.rongoapp.R;
import com.rongoapp.activities.MainActivity;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.FetchJsonFromAssetsFile;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class AlarmReceiver extends BroadcastReceiver {

    private static final String CHANNEL_ID = "com.rongoapp.utilities.channelId";
    private String Class_Name = "", text = "";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("AlarmReceiver: ", "AlarmReceiver");
        if (!InternetConnection.checkConnectionForFasl(context)) {
            Class_Name = intent.getExtras().getString("class");
            Log.e("msg: ", "" + Class_Name);

            if (Class_Name.equalsIgnoreCase("register")) {
                text = "हमें खेद है कि कोशिश के बावजूद आपका रजिस्ट्रेशन विफळ गया। कृपया दोबारा कोशिश करें, या मदद के लिए हमें  ##########  नंबर पर कॉल करें।";
            } else if (Class_Name.equalsIgnoreCase("no_login")) {
                text = "मौसम लगातार बदल रहा है। ऐसे में अपने ऐप को लॉगइन करना न भूलें। खेती से जुड़ी नवीनतम जानकारियां प्राप्त करें और कृषि वैज्ञानिकों से सीधे बात करें।";
            } else if (Class_Name.equalsIgnoreCase("no_action")) {
                text = "हमारे द्वारा दिए गए परामर्श पर अपने विचार व्यक्त करें और प्वाइंट्स अर्जित करें। इससे हम ऐप को आपके लिए अधिक सुविधानजक बना सकेंगे।";
            } else if (Class_Name.equalsIgnoreCase("weather_alert")) {
                text = "बात चाहे बाढ़ की हो, सूखे की हो या तेज हवाओं की! रोगों ऐप आपको आने वाली हर मुसीबतों से सही समय पर आपको सावधान करता है! जाने कैसे बचाएं खराब मौसम से अपनी फसलों को !";
            } else if (Class_Name.equalsIgnoreCase("no_sowing_date")) {
                text = "एप्लिकेशन का पूरा लाभ पाने और अंक अर्जित करने के लिए तारीख और बीज का प्रकार सही-सही दर्ज करें।";
//            } else if (Class_Name.equalsIgnoreCase("expected_sowing")) {
//                text = "एप्लिकेशन का पूरा लाभ पाने और अंक अर्जित करने के लिए तारीख और बीज का प्रकार सही-सही दर्ज करें।";
            } else if (Class_Name.equalsIgnoreCase("expected_sowing")) {
                text = "क्या आपने बुवाई की है?";
            } else if (Class_Name.equalsIgnoreCase("weekly_alert1")) {
                text = "" + getCropWeekName(context) + "चरण के लिए मार्गदर्शन अब आपके लिए उपलब्ध है";
            } else if (Class_Name.equalsIgnoreCase("weekly_alert4")) {
                text = "हमारे कृषि विशेषज्ञों के साथ अपनी चिंताओं को साझा करना, न भूलें";
            } else if (Class_Name.equalsIgnoreCase("weekly_alert5")) {
                text = "क्या आपकी फसल स्वस्थ है? प्रतियोगिता में भाग लेने के लिए हमें फसल की फोटो भेजना न भूलें";
            } else if (Class_Name.equalsIgnoreCase("weekly_alert7")) {
                text = "रिवार्ड पॉइंट पाने के लिए प्रश्नों का उत्तर हर दिन दें। रिवार्ड पॉइंट पाना चाहते हैं, तो सवाल का जवाब हर दिन दें।";
            }

            Intent notificationIntent = new Intent(context, MainActivity.class);
            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            notificationIntent.setAction(Long.toString(System.currentTimeMillis()));

            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(MainActivity.class);
            stackBuilder.addNextIntent(notificationIntent);

            PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
//                .setContentTitle(title)
//                .setContentText(text)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                    .setAutoCancel(true)
//                .setWhen(getTimeMilliSec(mNotificaitonHelper.getTimestamp()))
                    .setChannelId(CHANNEL_ID)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                    .setSound(defaultSoundUri)
                    .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                    .setSmallIcon(getNotificationIcon())
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                String description = "Notifications regarding our products";
                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, "Product", NotificationManager.IMPORTANCE_HIGH);
                mChannel.setDescription(description);
                mChannel.enableLights(true);
                mChannel.setShowBadge(true);
                mChannel.setLightColor(Color.RED);
                assert notificationManager != null;
                notificationManager.createNotificationChannel(mChannel);
            }
            notificationManager.notify(ServiceUtility.randInt(0, 9999999), notificationBuilder.build());
        }
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_notif_trans : R.mipmap.ic_launcher;
    }

    private long getTimeMilliSec(String timeStamp) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        try {
            Date date = format.parse(timeStamp);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private String getCropWeekName(Context context) {
        String stageName = "";
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",Locale.US);
            Date c = Calendar.getInstance().getTime();

            String SowingDate = context.getSharedPreferences(Constants.MediaPrefs, 0).getString(Constants.SharedPreference_SowingDate, "");
            if (!TextUtils.isEmpty(SowingDate)) {

                Date date1 = dateFormat.parse(SowingDate);
                Date date2 = dateFormat.parse(dateFormat.format(c.getTime()));

                long crop_in_days = date2.getTime() - date1.getTime();
                long crop_in_weeks = crop_in_days / 7;

                long diffInDay = TimeUnit.DAYS.convert(crop_in_weeks, TimeUnit.MILLISECONDS);
                Log.e("App diffIn", "" + round(diffInDay));
                long diffInDays = round(diffInDay);

//                long diffInDays = (TimeUnit.MILLISECONDS.toDays(crop_in_weeks) + 1);
//                Log.e("AlarmReceiver App", "" + (diffInDays));

//                JSONArray data = new JSONArray(FetchJsonFromAssetsFile.loadAdvisoryJSONFromAsset(context));
//                if (data != null && data.length() > 0) {
//                    for (int i = 0; i < data.length(); i++) {
//                        JSONObject resultData = data.getJSONObject(i);
//                        JSONObject result = resultData.optJSONObject("result");
//                        if (i == diffInDays) {
//                            JSONObject weekly_advisory = result.optJSONObject("weekly_advisory");
//                            if (weekly_advisory != null && weekly_advisory.length() > 0) {
//                                stageName = weekly_advisory.optString("stage");
//                            }
//                        }
//                    }
//                }
                Log.e("AlarmReceiver App", "" + stageName);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stageName;
    }

    public static int round(double d) {
        double dAbs = Math.abs(d);
        int i = (int) dAbs;
        double result = dAbs - (double) i;
        if (result < 0.0) {
            return d < 0 ? -i : i;
        } else {
            return d < 0 ? -(i + 1) : i + 1;
        }
    }

}