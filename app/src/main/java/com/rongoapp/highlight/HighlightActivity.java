package com.rongoapp.highlight;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.rongoapp.R;

public class HighlightActivity extends AppCompatActivity {

    HighlightManager highlightManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highlight);

        /*
         * Create an instance of the HighlightManager. It is important to do
         * this _after_ the call to Activity.setContentView().
         */
        highlightManager = new HighlightManager(this);

        /*
         * Calling HighlightManager.reshowAllHighlights() will make sure that
         * highlights are shown with each launch of the app. Otherwise the
         * highlights will only be shown the first time. This is useful for
         * debugging purposes but should not be used for a production release.
         */
        highlightManager.reshowAllHighlights();

        /*
         * In the following three different highlights are defined via
         * HighlightManager.addView(). The id refers to the android:id parameter
         * of the respective view to be highlighted.
         */
        highlightManager.addView(R.id.text1).setTitle(R.string.app_name)
                .setDescriptionId(R.string.app_name);
        highlightManager.addView(R.id.button).setTitle(R.string.app_name)
                .setDescriptionId(R.string.app_name);
        highlightManager.addView(R.id.text2).setTitle(R.string.app_name)
                .setDescriptionId(R.string.app_name);
    }

}
