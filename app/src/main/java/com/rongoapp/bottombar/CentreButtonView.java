package com.rongoapp.bottombar;

import android.content.Context;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rongoapp.R;
import com.rongoapp.controller.GlideApp;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.data.MasterCropData;
import com.rongoapp.controller.Functions;
import com.rongoapp.utilities.InternetConnection;

import java.util.List;

//https://stackoverflow.com/questions/41945677/adding-views-to-linearlayout-programmatically-not-setting-text-on-all-but-one/41945756

public class CentreButtonView extends LinearLayout {

    private int contentWidth = (int) getResources().getDimension(R.dimen.space_centre_button_default_size);
    private int mainContentHeight = (int) getResources().getDimension(R.dimen.space_centre_button_default_size);
    private int spaceItemTextSize = (int) getResources().getDimension(R.dimen.space_item_text_default_size);

    ImageView spaceItemIcon;
    TextView spaceItemText;
    String cropId = "";

    public CentreButtonView(Context context) {
        super(context);
        setOrientation(VERTICAL);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RelativeLayout.LayoutParams textAndIconContainerParams = new RelativeLayout.LayoutParams(contentWidth, mainContentHeight);
        RelativeLayout textAndIconContainer = (RelativeLayout) inflater.inflate(R.layout.space_center_item_view, this, false);
        textAndIconContainer.setLayoutParams(textAndIconContainerParams);

        spaceItemIcon = (ImageView) textAndIconContainer.findViewById(R.id.space_icon);
        spaceItemText = (TextView) textAndIconContainer.findViewById(R.id.space_text);
        spaceItemText.setTextSize(TypedValue.COMPLEX_UNIT_PX, spaceItemTextSize);

        List<MasterCropData> masterCropList = RongoApp.getMasterCropList();
        if (masterCropList.size() > 0) {
            if (!TextUtils.isEmpty(cropId)) {
                for (int j = 0; j < masterCropList.size(); j++) {
                    if (masterCropList.get(j).getCrop_id().equalsIgnoreCase(cropId)) {
                        spaceItemText.setText(masterCropList.get(j).getDisplay_name());
                        if (!TextUtils.isEmpty(masterCropList.get(j).getCrop_image()) && InternetConnection.checkConnectionForFasl(context)) {
//                            Functions.displayImageWithPlaceHolder(masterCropList.get(j).getCrop_image(), spaceItemIcon);

                            GlideApp.with(context).load(masterCropList.get(j).getCrop_image())
                                    .apply(Functions.getPlaceholder(masterCropList.get(j).getCrop_id())).into(spaceItemIcon);
                        } else {
                            if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("1")) {
                                spaceItemIcon.setImageResource(R.mipmap.maize);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("2")) {
                                spaceItemIcon.setImageResource(R.mipmap.rice);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("3")) {
                                spaceItemIcon.setImageResource(R.mipmap.cotton);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("4")) {
                                spaceItemIcon.setImageResource(R.mipmap.wheat);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("5")) {
                                spaceItemIcon.setImageResource(R.mipmap.okra);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("6")) {
                                spaceItemIcon.setImageResource(R.mipmap.pearmillet);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("7")) {
                                spaceItemIcon.setImageResource(R.mipmap.corinder);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("8")) {
                                spaceItemIcon.setImageResource(R.mipmap.bittergourd);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("9")) {
                                spaceItemIcon.setImageResource(R.mipmap.bottlegourd);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("10")) {
                                spaceItemIcon.setImageResource(R.mipmap.watermelon);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("11")) {
                                spaceItemIcon.setImageResource(R.mipmap.groundnut);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("12")) {
                                spaceItemIcon.setImageResource(R.mipmap.soyabean);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("13")) {
                                spaceItemIcon.setImageResource(R.mipmap.caster);
                            }
                        }
                    }
                }
            }
        } else {
            if (cropId.equalsIgnoreCase("1")) {
                spaceItemIcon.setImageResource(R.mipmap.maize);
                spaceItemText.setText(context.getResources().getText(R.string.str_maize));
            } else if (cropId.equalsIgnoreCase("2")) {
                spaceItemIcon.setImageResource(R.mipmap.rice);
                spaceItemText.setText(context.getResources().getText(R.string.str_rice));
            } else if (cropId.equalsIgnoreCase("3")) {
                spaceItemIcon.setImageResource(R.mipmap.cotton);
                spaceItemText.setText(context.getResources().getText(R.string.str_cotton));
            } else if (cropId.equalsIgnoreCase("4")) {
                spaceItemIcon.setImageResource(R.mipmap.wheat);
                spaceItemText.setText(context.getResources().getText(R.string.str_wheat));
            } else if (cropId.equalsIgnoreCase("5")) {
                spaceItemIcon.setImageResource(R.mipmap.okra);
                spaceItemText.setText(context.getResources().getText(R.string.str_okra));
            } else if (cropId.equalsIgnoreCase("6")) {
                spaceItemIcon.setImageResource(R.mipmap.pearmillet);
                spaceItemText.setText(context.getResources().getText(R.string.str_pearlmillet));
            } else if (cropId.equalsIgnoreCase("7")) {
                spaceItemIcon.setImageResource(R.mipmap.corinder);
                spaceItemText.setText(context.getResources().getText(R.string.str_coriander));
            } else if (cropId.equalsIgnoreCase("8")) {
                spaceItemIcon.setImageResource(R.mipmap.bittergourd);
                spaceItemText.setText(context.getResources().getText(R.string.str_bittergourd));
            } else if (cropId.equalsIgnoreCase("9")) {
                spaceItemIcon.setImageResource(R.mipmap.bottlegourd);
                spaceItemText.setText(context.getResources().getText(R.string.str_bottlegourd));
            } else if (cropId.equalsIgnoreCase("10")) {
                spaceItemIcon.setImageResource(R.mipmap.watermelon);
                spaceItemText.setText(context.getResources().getText(R.string.str_watermelon));
            } else if (cropId.equalsIgnoreCase("11")) {
                spaceItemIcon.setImageResource(R.mipmap.groundnut);
                spaceItemText.setText(context.getResources().getText(R.string.str_groundnut));
            } else if (cropId.equalsIgnoreCase("12")) {
                spaceItemIcon.setImageResource(R.mipmap.soyabean);
                spaceItemText.setText(context.getResources().getText(R.string.str_soyabean));
            } else if (cropId.equalsIgnoreCase("13")) {
                spaceItemIcon.setImageResource(R.mipmap.caster);
                spaceItemText.setText(context.getResources().getText(R.string.str_castor));
            }
        }

        addView(textAndIconContainer);
    }

    public void setCropId(String crop_id, Context context) {
        cropId = crop_id;

        List<MasterCropData> masterCropList = RongoApp.getMasterCropList();
        if (masterCropList.size() > 0) {
            if (!TextUtils.isEmpty(cropId)) {
                for (int j = 0; j < masterCropList.size(); j++) {
                    if (masterCropList.get(j).getCrop_id().equalsIgnoreCase(cropId)) {
                        spaceItemText.setText(masterCropList.get(j).getDisplay_name());
                        if (!TextUtils.isEmpty(masterCropList.get(j).getCrop_image()) && InternetConnection.checkConnectionForFasl(context)) {

                            GlideApp.with(context).load(masterCropList.get(j).getCrop_image())
                                    .apply(Functions.getPlaceholder(masterCropList.get(j).getCrop_id())).into(spaceItemIcon);
                        } else {
                            if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("1")) {
                                spaceItemIcon.setImageResource(R.mipmap.maize);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("2")) {
                                spaceItemIcon.setImageResource(R.mipmap.rice);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("3")) {
                                spaceItemIcon.setImageResource(R.mipmap.cotton);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("4")) {
                                spaceItemIcon.setImageResource(R.mipmap.wheat);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("5")) {
                                spaceItemIcon.setImageResource(R.mipmap.okra);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("6")) {
                                spaceItemIcon.setImageResource(R.mipmap.pearmillet);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("7")) {
                                spaceItemIcon.setImageResource(R.mipmap.corinder);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("8")) {
                                spaceItemIcon.setImageResource(R.mipmap.bittergourd);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("9")) {
                                spaceItemIcon.setImageResource(R.mipmap.bottlegourd);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("10")) {
                                spaceItemIcon.setImageResource(R.mipmap.watermelon);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("11")) {
                                spaceItemIcon.setImageResource(R.mipmap.groundnut);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("12")) {
                                spaceItemIcon.setImageResource(R.mipmap.soyabean);
                            } else if (masterCropList.get(j).getCrop_id().equalsIgnoreCase("13")) {
                                spaceItemIcon.setImageResource(R.mipmap.caster);
                            }
                        }
                    }
                }
            }
        } else {
            if (cropId.equalsIgnoreCase("1")) {
                spaceItemIcon.setImageResource(R.mipmap.maize);
                spaceItemText.setText(context.getResources().getText(R.string.str_maize));
            } else if (cropId.equalsIgnoreCase("2")) {
                spaceItemIcon.setImageResource(R.mipmap.rice);
                spaceItemText.setText(context.getResources().getText(R.string.str_rice));
            } else if (cropId.equalsIgnoreCase("3")) {
                spaceItemIcon.setImageResource(R.mipmap.cotton);
                spaceItemText.setText(context.getResources().getText(R.string.str_cotton));
            } else if (cropId.equalsIgnoreCase("4")) {
                spaceItemIcon.setImageResource(R.mipmap.wheat);
                spaceItemText.setText(context.getResources().getText(R.string.str_wheat));
            } else if (cropId.equalsIgnoreCase("5")) {
                spaceItemIcon.setImageResource(R.mipmap.okra);
                spaceItemText.setText(context.getResources().getText(R.string.str_okra));
            } else if (cropId.equalsIgnoreCase("6")) {
                spaceItemIcon.setImageResource(R.mipmap.pearmillet);
                spaceItemText.setText(context.getResources().getText(R.string.str_pearlmillet));
            } else if (cropId.equalsIgnoreCase("7")) {
                spaceItemIcon.setImageResource(R.mipmap.corinder);
                spaceItemText.setText(context.getResources().getText(R.string.str_coriander));
            } else if (cropId.equalsIgnoreCase("8")) {
                spaceItemIcon.setImageResource(R.mipmap.bittergourd);
                spaceItemText.setText(context.getResources().getText(R.string.str_bittergourd));
            } else if (cropId.equalsIgnoreCase("9")) {
                spaceItemIcon.setImageResource(R.mipmap.bottlegourd);
                spaceItemText.setText(context.getResources().getText(R.string.str_bottlegourd));
            } else if (cropId.equalsIgnoreCase("10")) {
                spaceItemIcon.setImageResource(R.mipmap.watermelon);
                spaceItemText.setText(context.getResources().getText(R.string.str_watermelon));
            } else if (cropId.equalsIgnoreCase("11")) {
                spaceItemIcon.setImageResource(R.mipmap.groundnut);
                spaceItemText.setText(context.getResources().getText(R.string.str_groundnut));
            } else if (cropId.equalsIgnoreCase("12")) {
                spaceItemIcon.setImageResource(R.mipmap.soyabean);
                spaceItemText.setText(context.getResources().getText(R.string.str_soyabean));
            } else if (cropId.equalsIgnoreCase("13")) {
                spaceItemIcon.setImageResource(R.mipmap.caster);
                spaceItemText.setText(context.getResources().getText(R.string.str_castor));
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        boolean result = super.onTouchEvent(ev);
        if (!result) {
            if (ev.getAction() == MotionEvent.ACTION_UP) {
                cancelLongPress();
            }
            setPressed(false);
        }
        return result;
    }

}
