package com.rongoapp.bottombar;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.core.content.ContextCompat;

import com.rongoapp.R;
import com.rongoapp.controller.RongoApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SpaceNavigationView extends RelativeLayout {

    private static final String TAG = "SpaceNavigationView";
    private Context context;
    private Typeface customFont;
    private Bundle savedInstanceState;
    private static final int NOT_DEFINED = -777; //random number, not - 1 because it is Color.WHITE

    private static final String CURRENT_SELECTED_ITEM_BUNDLE_KEY = "currentItem";
    private static final String BADGES_ITEM_BUNDLE_KEY = "badgeItem";
    private static final String CHANGED_ICON_AND_TEXT_BUNDLE_KEY = "changedIconAndText";
    private static final String CENTRE_BUTTON_ICON_KEY = "centreButtonIconKey";
    private static final String SPACE_BACKGROUND_COLOR_KEY = "backgroundColorKey";
    private static final String BADGE_FULL_TEXT_KEY = "badgeFullTextKey";
    private static final String VISIBILITY = "visibilty";

    private static final int MAX_SPACE_ITEM_SIZE = 4, MIN_SPACE_ITEM_SIZE = 2;
    private final int spaceNavigationHeight = (int) getResources().getDimension(R.dimen.space_navigation_height);
    private final int mainContentHeight = (int) getResources().getDimension(R.dimen.main_content_height);
    private final int centreContentWight = (int) getResources().getDimension(R.dimen.centre_content_width);
    private final int centreButtonSize = (int) getResources().getDimension(R.dimen.space_centre_button_default_size);

    private List<SpaceItem> spaceItems = new ArrayList<>();
    private List<View> spaceItemList = new ArrayList<>();
    private List<RelativeLayout> badgeList = new ArrayList<>();

    private HashMap<Integer, Object> badgeSaveInstanceHashMap = new HashMap<>();
    private HashMap<Integer, SpaceItem> changedItemAndIconHashMap = new HashMap<>();
    private SpaceOnClickListener spaceOnClickListener;

    private RelativeLayout centreBackgroundView;
    private LinearLayout leftContent, rightContent;
    private BezierView centreContent;

    CentreButtonView centreButtonView;

    private int spaceItemIconSize = NOT_DEFINED;
    private int spaceItemIconOnlySize = NOT_DEFINED;
    private int spaceNavigationWidth;
    private int spaceItemTextSize = NOT_DEFINED;

    private int spaceBackgroundColor = NOT_DEFINED;
    private int centreButtonIcon = R.mipmap.mazeicon;
    private int activeSpaceItemColor = NOT_DEFINED;
    private int inActiveSpaceItemColor = NOT_DEFINED;

    public int currentSelectedItem = 0;
    public int lastSelectedItem = 0;

    private int contentWidth;

    private boolean isCentreButtonSelectable = false;
    private boolean isCentrePartLinear = false;
    private boolean isTextOnlyMode = false;
    private boolean isIconOnlyMode = false;
    private boolean isCustomFont = false;
    private boolean shouldShowBadgeWithNinePlus = true;

    private NavigationViewShadow navigationViewBorderLine;

    /**
     * Constructors
     */
    public SpaceNavigationView(Context context) {
        this(context, null);
    }

    public SpaceNavigationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SpaceNavigationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init(attrs);
    }

    /**
     * Init custom attributes
     *
     * @param attrs attributes
     */
    private void init(AttributeSet attrs) {
        if (attrs != null) {
            Resources resources = getResources();
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SpaceNavigationView);
            spaceItemIconSize = typedArray.getDimensionPixelSize(R.styleable.SpaceNavigationView_space_item_icon_size, resources.getDimensionPixelSize(R.dimen.space_item_icon_only_size));
            spaceItemIconOnlySize = typedArray.getDimensionPixelSize(R.styleable.SpaceNavigationView_space_item_icon_only_size, resources.getDimensionPixelSize(R.dimen.space_item_icon_only_size));
            spaceItemTextSize = typedArray.getDimensionPixelSize(R.styleable.SpaceNavigationView_space_item_text_size, resources.getDimensionPixelSize(R.dimen.space_item_text_default_size));
            spaceBackgroundColor = typedArray.getColor(R.styleable.SpaceNavigationView_space_background_color, resources.getColor(R.color.space_default_color));
            activeSpaceItemColor = typedArray.getColor(R.styleable.SpaceNavigationView_active_item_color, resources.getColor(R.color.space_white));
            inActiveSpaceItemColor = typedArray.getColor(R.styleable.SpaceNavigationView_inactive_item_color, resources.getColor(R.color.default_inactive_item_color));
            centreButtonIcon = typedArray.getResourceId(R.styleable.SpaceNavigationView_centre_button_icon, R.mipmap.maize);
            isCentrePartLinear = typedArray.getBoolean(R.styleable.SpaceNavigationView_centre_part_linear, false);
            typedArray.recycle();
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        /**
         * Set default colors and sizes
         */
        if (spaceBackgroundColor == NOT_DEFINED)
            spaceBackgroundColor = ContextCompat.getColor(context, R.color.space_default_color);

//        if (centreButtonColor == NOT_DEFINED)
//            centreButtonColor = ContextCompat.getColor(context, android.R.color.transparent);

        if (centreButtonIcon == NOT_DEFINED)
            centreButtonIcon = R.mipmap.mazeicon;

        if (activeSpaceItemColor == NOT_DEFINED)
            activeSpaceItemColor = ContextCompat.getColor(context, R.color.space_white);

        if (inActiveSpaceItemColor == NOT_DEFINED)
            inActiveSpaceItemColor = ContextCompat.getColor(context, R.color.default_inactive_item_color);

        if (spaceItemTextSize == NOT_DEFINED)
            spaceItemTextSize = (int) getResources().getDimension(R.dimen.space_item_text_default_size);

        if (spaceItemIconSize == NOT_DEFINED)
//            spaceItemIconSize = (int) getResources().getDimension(R.dimen.space_item_icon_default_size);
            spaceItemIconSize = (int) getResources().getDimension(R.dimen.space_item_icon_only_size);

        if (spaceItemIconOnlySize == NOT_DEFINED)
            spaceItemIconOnlySize = (int) getResources().getDimension(R.dimen.space_item_icon_only_size);

        /**
         * Set main layout size and color
         */
        ViewGroup.LayoutParams params = getLayoutParams();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = spaceNavigationHeight + 8;
        setBackgroundColor(ContextCompat.getColor(context, R.color.space_transparent));
        setLayoutParams(params);
    }

    @Override
    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        super.onSizeChanged(width, height, oldWidth, oldHeight);
        spaceNavigationWidth = width;

        /**
         * Restore current item index from savedInstance
         */
        restoreCurrentItem();

        /**
         * Trow exceptions if items size is greater than 4 or lesser than 2
         */
        if (spaceItems.size() < MIN_SPACE_ITEM_SIZE && !isInEditMode()) {
            throw new NullPointerException("Your space item count must be greater than 1 ," +
                    " your current items count isa : " + spaceItems.size());
        }

        if (spaceItems.size() > MAX_SPACE_ITEM_SIZE && !isInEditMode()) {
            throw new IndexOutOfBoundsException("Your items count maximum can be 4," +
                    " your current items count is : " + spaceItems.size());
        }

        /**
         * Get left or right content width
         */
        contentWidth = (width - spaceNavigationHeight) / 2;

        /**
         * Removing all view for not being duplicated
         */
        removeAllViews();

        /**
         * Views initializations and customizing
         */
        initAndAddViewsToMainView();

        /**
         * Redraw main view to make subviews visible
         */
        postRequestLayout();

        /**
         * Retore Translation height
         */

        restoreTranslation();
    }

    /**
     * Views initializations and customizing
     */
    private void initAndAddViewsToMainView() {
        RelativeLayout mainContent = new RelativeLayout(context);
        centreBackgroundView = new RelativeLayout(context);

        leftContent = new LinearLayout(context);
        rightContent = new LinearLayout(context);

        centreContent = buildBezierView();
        navigationViewBorderLine = buildBezierBorderline();

        centreButtonView = new CentreButtonView(context);
        setCenterView(RongoApp.getSelectedCrop());

        centreButtonView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (spaceOnClickListener != null)
                    spaceOnClickListener.onCentreButtonClick();
                if (isCentreButtonSelectable)
                    updateSpaceItems(-1);
            }
        });

        /**
         * Set fab layout params
         */
        LayoutParams fabParams = new LayoutParams(centreButtonSize, centreButtonSize);
        fabParams.addRule(RelativeLayout.CENTER_IN_PARENT);

        /**
         * Main content size
         */
        LayoutParams mainContentParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, mainContentHeight);
        mainContentParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        /**
         * Centre content size
         */
        LayoutParams centreContentParams = new LayoutParams(centreContentWight, spaceNavigationHeight);
        centreContentParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        centreContentParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        /**
         * Centre Background View content size and position
         */
        LayoutParams centreBackgroundViewParams = new LayoutParams(centreContentWight, mainContentHeight);
        centreBackgroundViewParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        centreBackgroundViewParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        /**
         * Left content size
         */
        LayoutParams leftContentParams = new LayoutParams(contentWidth, mainContentHeight);
        leftContentParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        leftContentParams.addRule(LinearLayout.HORIZONTAL);
        leftContentParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        /**
         * Right content size
         */
        LayoutParams rightContentParams = new LayoutParams(contentWidth, mainContentHeight);
        rightContentParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        rightContentParams.addRule(LinearLayout.HORIZONTAL);
        rightContentParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        /**
         * Adding views background colors
         */
        setBackgroundColors();

        /**
         * Borderline params
         */
        LayoutParams borderLineParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, spaceNavigationHeight);
        borderLineParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        borderLineParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);

        addView(navigationViewBorderLine, borderLineParams);

        /**
         * Adding view to centreContent
         */
//        centreContent.addView(centreButton, fabParams);
        centreContent.addView(centreButtonView, fabParams);

        /**
         * Adding views to mainContent
         */
        addView(leftContent, leftContentParams);
        addView(rightContent, rightContentParams);

        /**
         * Adding views to mainView
         */
        addView(centreBackgroundView, centreBackgroundViewParams);
        addView(centreContent, centreContentParams);
        addView(mainContent, mainContentParams);

        /**
         * Restore changed icons and texts from savedInstance
         */
        restoreChangedIconsAndTexts();

        /**
         * Adding current space items to left and right content
         */
        addSpaceItems(leftContent, rightContent);
    }

    public void setCenterView(String cropId) {
//        Log.e("+++++++", "" + cropId);
        if (centreButtonView == null) {
            centreButtonView = new CentreButtonView(context);
            setCenterView(RongoApp.getSelectedCrop());
        }

        if (centreButtonView != null) {
            centreButtonView.setCropId(cropId, context);
            centreButtonView.setOrientation(LinearLayout.VERTICAL);
            centreButtonView.invalidate();
        }
    }

    /**
     * Adding given space items to content
     *
     * @param leftContent  to left content
     * @param rightContent and right content
     */
    private void addSpaceItems(LinearLayout leftContent, LinearLayout rightContent) {
        /**
         * Removing all views for not being duplicated
         */
        if (leftContent.getChildCount() > 0 || rightContent.getChildCount() > 0) {
            leftContent.removeAllViews();
            rightContent.removeAllViews();
        }

        /**
         * Clear spaceItemList and badgeList for not being duplicated
         */
//        spaceItemList.clear();
        spaceItemList = new ArrayList<>();
//        badgeList.clear();
        badgeList = new ArrayList<>();

        /**
         * Getting LayoutInflater to inflate space item view from XML
         */
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < spaceItems.size(); i++) {
            final int index = i;
            int targetWidth;

            if (spaceItems.size() > MIN_SPACE_ITEM_SIZE) {
                targetWidth = contentWidth / 2;
            } else {
                targetWidth = contentWidth;
            }

            RelativeLayout.LayoutParams textAndIconContainerParams = new RelativeLayout.LayoutParams(targetWidth, mainContentHeight);
            RelativeLayout textAndIconContainer = (RelativeLayout) inflater.inflate(R.layout.space_item_view, this, false);
            textAndIconContainer.setLayoutParams(textAndIconContainerParams);

            ImageView spaceItemIcon = (ImageView) textAndIconContainer.findViewById(R.id.space_icon);
            TextView spaceItemText = (TextView) textAndIconContainer.findViewById(R.id.space_text);
            RelativeLayout badgeContainer = (RelativeLayout) textAndIconContainer.findViewById(R.id.badge_container);
            spaceItemIcon.setImageResource(spaceItems.get(i).getItemIcon());
            spaceItemText.setText(spaceItems.get(i).getItemName());
            spaceItemText.setTextSize(TypedValue.COMPLEX_UNIT_PX, spaceItemTextSize);

            /**
             * Set custom font to space item textView
             */
            if (isCustomFont)
                spaceItemText.setTypeface(customFont);

            /**
             * Hide item icon and show only text
             */
            if (isTextOnlyMode)
                changeViewVisibilityGone(spaceItemIcon);

            /**
             * Hide item text and change icon size
             */
            ViewGroup.LayoutParams iconParams = spaceItemIcon.getLayoutParams();
            if (isIconOnlyMode) {
                iconParams.height = spaceItemIconOnlySize;
                iconParams.width = spaceItemIconOnlySize;
                spaceItemIcon.setLayoutParams(iconParams);
                changeViewVisibilityGone(spaceItemText);
            } else {
                iconParams.height = spaceItemIconSize;
                iconParams.width = spaceItemIconSize;
                spaceItemIcon.setLayoutParams(iconParams);
            }

            /**
             * Adding space items to item list for future
             */
            spaceItemList.add(textAndIconContainer);

            /**
             * Adding badge items to badge list for future
             */
            badgeList.add(badgeContainer);

            /**
             * Adding sub views to left and right sides
             */
            if (spaceItems.size() == MIN_SPACE_ITEM_SIZE && leftContent.getChildCount() == 1) {
                rightContent.addView(textAndIconContainer, textAndIconContainerParams);
            } else if (spaceItems.size() > MIN_SPACE_ITEM_SIZE && leftContent.getChildCount() == 2) {
                rightContent.addView(textAndIconContainer, textAndIconContainerParams);
            } else {
                leftContent.addView(textAndIconContainer, textAndIconContainerParams);
            }

            /**
             * Changing current selected item tint
             */
            if (i == currentSelectedItem) {
                Log.v("currentSelectedItem", currentSelectedItem + "");
                spaceItemText.setTextColor(activeSpaceItemColor);
                spaceItemIcon.setImageResource(R.mipmap.plant_selected);
                // Utils.changeImageViewTint(spaceItemIcon, activeSpaceItemColor);
            } else {
                spaceItemText.setTextColor(inActiveSpaceItemColor);
                //Utils.changeImageViewTint(spaceItemIcon, inActiveSpaceItemColor);
            }

            textAndIconContainer.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    updateSpaceItems(index);
                }
            });

        }

        /**
         * Restore available badges from saveInstance
         */
        restoreBadges();
    }

    static void changeViewVisibilityGone(View view) {
        if (view != null && view.getVisibility() == View.VISIBLE)
            view.setVisibility(View.GONE);
    }

    /**
     * Update selected item and change it's and non selected item tint
     *
     * @param selectedIndex item in index
     */
    public void updateSpaceItems(final int selectedIndex) {
        try {
            /**
             * return if item already selected
             */
            if (currentSelectedItem == selectedIndex) {
                if (spaceOnClickListener != null && selectedIndex >= 0)
                    spaceOnClickListener.onItemReselected(selectedIndex, spaceItems.get(selectedIndex).getItemName());

                return;
            }

            /*
             * @TODO Change BY Dilip for Center button Click highlight
             */
            for (int i = 0; i < spaceItemList.size(); i++) {
                if (i == selectedIndex) {
                    RelativeLayout textAndIconContainer = (RelativeLayout) spaceItemList.get(selectedIndex);
                    ImageView spaceItemIcon = (ImageView) textAndIconContainer.findViewById(R.id.space_icon);
                    TextView spaceItemText = (TextView) textAndIconContainer.findViewById(R.id.space_text);
                    spaceItemText.setTextColor(activeSpaceItemColor);
                    Log.v("currentSelectedItem", currentSelectedItem + " : " + selectedIndex);

                    changeImageViewTint(spaceItemIcon, ContextCompat.getColor(context, android.R.color.transparent));

                    if (selectedIndex == 0) {
                        spaceItemIcon.setImageResource(R.mipmap.plant_selected);
                    }

                    if (selectedIndex == 1) {
                        spaceItemIcon.setImageResource(R.mipmap.growth_selected);
                    }

                    if (selectedIndex == 2) {
                        spaceItemIcon.setImageResource(R.mipmap.cis);
                    }

                    if (selectedIndex == 3) {
                        spaceItemIcon.setImageResource(R.mipmap.more_selected);
                    }

                } else if (i == currentSelectedItem) {
                    RelativeLayout textAndIconContainer = (RelativeLayout) spaceItemList.get(i);
                    ImageView spaceItemIcon = (ImageView) textAndIconContainer.findViewById(R.id.space_icon);
                    TextView spaceItemText = (TextView) textAndIconContainer.findViewById(R.id.space_text);
                    spaceItemText.setTextColor(inActiveSpaceItemColor);
                    changeImageViewTint(spaceItemIcon, ContextCompat.getColor(context, android.R.color.black));
                    Log.v("currentSelectedItem", currentSelectedItem + " : " + selectedIndex + " :Y");
                }
            }

            /**
             * Set a listener that gets fired when the selected item changes
             *
             * @param listener a listener for monitoring changes in item selection
             */
            if (spaceOnClickListener != null)
                if (selectedIndex >= 0) {
                    spaceOnClickListener.onItemClick(selectedIndex, spaceItems.get(selectedIndex).getItemName());
                } else {
                    spaceOnClickListener.onItemClick(selectedIndex, "");
                }

//        if (spaceOnClickListener != null && selectedIndex >= 0)
//            spaceOnClickListener.onItemClick(selectedIndex, spaceItems.get(selectedIndex).getItemName());

            /**
             * Change current selected item index
             */
            lastSelectedItem = currentSelectedItem;
            currentSelectedItem = selectedIndex;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void changeImageViewTint(ImageView imageView, int color) {
        imageView.setColorFilter(color);
    }

    /**
     * Set views background colors
     */
    private void setBackgroundColors() {
        rightContent.setBackgroundColor(spaceBackgroundColor);
        centreBackgroundView.setBackgroundColor(spaceBackgroundColor);
        leftContent.setBackgroundColor(spaceBackgroundColor);
    }

    /**
     * Indicate event queue that we have changed the View hierarchy during a layout pass
     */
    private void postRequestLayout() {
        SpaceNavigationView.this.getHandler().post(new Runnable() {
            @Override
            public void run() {
                SpaceNavigationView.this.requestLayout();
            }
        });
    }

    /**
     * Restore current item index from savedInstance
     */
    private void restoreCurrentItem() {
        Bundle restoredBundle = savedInstanceState;
        if (restoredBundle != null) {
            if (restoredBundle.containsKey(CURRENT_SELECTED_ITEM_BUNDLE_KEY))
                currentSelectedItem = restoredBundle.getInt(CURRENT_SELECTED_ITEM_BUNDLE_KEY, 0);
        }
    }

    /**
     * Restore available badges from saveInstance
     */
    @SuppressWarnings("unchecked")
    private void restoreBadges() {
        Bundle restoredBundle = savedInstanceState;
        if (restoredBundle != null) {
            if (restoredBundle.containsKey(BADGE_FULL_TEXT_KEY)) {
                shouldShowBadgeWithNinePlus = restoredBundle.getBoolean(BADGE_FULL_TEXT_KEY);
            }

            if (restoredBundle.containsKey(BADGES_ITEM_BUNDLE_KEY)) {
                badgeSaveInstanceHashMap = (HashMap<Integer, Object>) savedInstanceState.getSerializable(BADGES_ITEM_BUNDLE_KEY);
                if (badgeSaveInstanceHashMap != null) {
                    for (Integer integer : badgeSaveInstanceHashMap.keySet()) {
                        BadgeHelper.forceShowBadge(badgeList.get(integer), (BadgeItem) badgeSaveInstanceHashMap.get(integer), shouldShowBadgeWithNinePlus);
                    }
                }
            }
        }
    }

    /**
     * Restore changed icons,colors and texts from saveInstance
     */
    @SuppressWarnings("unchecked")
    private void restoreChangedIconsAndTexts() {
        Bundle restoredBundle = savedInstanceState;
        if (restoredBundle != null) {
            if (restoredBundle.containsKey(CHANGED_ICON_AND_TEXT_BUNDLE_KEY)) {
                changedItemAndIconHashMap = (HashMap<Integer, SpaceItem>) restoredBundle.getSerializable(CHANGED_ICON_AND_TEXT_BUNDLE_KEY);
                if (changedItemAndIconHashMap != null) {
                    SpaceItem spaceItem;
                    for (int i = 0; i < changedItemAndIconHashMap.size(); i++) {
                        spaceItem = changedItemAndIconHashMap.get(i);
                        spaceItems.get(i).setItemIcon(spaceItem.getItemIcon());
                        spaceItems.get(i).setItemName(spaceItem.getItemName());
                    }
                }
            }

            if (restoredBundle.containsKey(CENTRE_BUTTON_ICON_KEY)) {
                centreButtonIcon = restoredBundle.getInt(CENTRE_BUTTON_ICON_KEY);
//                centreButton.setImageResource(centreButtonIcon);
            }

            if (restoredBundle.containsKey(SPACE_BACKGROUND_COLOR_KEY)) {
                int backgroundColor = restoredBundle.getInt(SPACE_BACKGROUND_COLOR_KEY);
                changeSpaceBackgroundColor(backgroundColor);
            }
        }
    }

    /**
     * Creating bezier view with params
     *
     * @return created bezier view
     */
    private BezierView buildBezierView() {
        BezierView bezierView = new BezierView(context, spaceBackgroundColor);
        bezierView.build(centreContentWight, spaceNavigationHeight - mainContentHeight, isCentrePartLinear);
        return bezierView;
    }

    private NavigationViewShadow buildBezierBorderline() {
        NavigationViewShadow borderline = new NavigationViewShadow(context);
        borderline.build(spaceNavigationWidth, centreContentWight, spaceNavigationHeight - mainContentHeight, isCentrePartLinear);
        return borderline;
    }

    /**
     * Throw Array Index Out Of Bounds Exception
     *
     * @param itemIndex item index to show on logs
     */
    private void throwArrayIndexOutOfBoundsException(int itemIndex) {
        throw new ArrayIndexOutOfBoundsException("Your item index can't be 0 or greater than space item size," +
                " your items size is " + spaceItems.size() + ", your current index is :" + itemIndex);
    }

    /**
     * Initialization with savedInstanceState to save current selected
     * position and current badges
     *
     * @param savedInstanceState bundle to saveInstance
     */
    public void initWithSaveInstanceState(Bundle savedInstanceState) {
        this.savedInstanceState = savedInstanceState;
    }

    /**
     * Save badges and current position
     *
     * @param outState bundle to saveInstance
     */
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(CURRENT_SELECTED_ITEM_BUNDLE_KEY, currentSelectedItem);
        outState.putInt(CENTRE_BUTTON_ICON_KEY, centreButtonIcon);
        outState.putInt(SPACE_BACKGROUND_COLOR_KEY, spaceBackgroundColor);
        outState.putBoolean(BADGE_FULL_TEXT_KEY, shouldShowBadgeWithNinePlus);
        outState.putFloat(VISIBILITY, this.getTranslationY());

        if (badgeSaveInstanceHashMap.size() > 0)
            outState.putSerializable(BADGES_ITEM_BUNDLE_KEY, badgeSaveInstanceHashMap);
        if (changedItemAndIconHashMap.size() > 0)
            outState.putSerializable(CHANGED_ICON_AND_TEXT_BUNDLE_KEY, changedItemAndIconHashMap);
    }

    /**
     * Set centre circle button background color
     *
     * @param centreButtonColor target color
     */
    public void setCentreButtonColor(@ColorInt int centreButtonColor) {
        // this.centreButtonColor = centreButtonColor;
    }

    /**
     * Set main background color
     *
     * @param spaceBackgroundColor target color
     */
    public void setSpaceBackgroundColor(@ColorInt int spaceBackgroundColor) {
        this.spaceBackgroundColor = spaceBackgroundColor;
    }

    /**
     * Set centre button icon
     *
     * @param centreButtonIcon target icon
     */
    public void setCentreButtonIcon(int centreButtonIcon) {
        this.centreButtonIcon = centreButtonIcon;
    }

    /**
     * Set active centre button color
     *
     * @param activeCentreButtonBackgroundColor color to change
     */
    public void setActiveCentreButtonBackgroundColor(@ColorInt int activeCentreButtonBackgroundColor) {
        //this.activeCentreButtonBackgroundColor = activeCentreButtonBackgroundColor;
    }

    /**
     * Set active item text color
     *
     * @param activeSpaceItemColor color to change
     */
    public void setActiveSpaceItemColor(@ColorInt int activeSpaceItemColor) {
        this.activeSpaceItemColor = activeSpaceItemColor;
    }

    /**
     * Set inactive item text color
     *
     * @param inActiveSpaceItemColor color to change
     */
    public void setInActiveSpaceItemColor(@ColorInt int inActiveSpaceItemColor) {
        this.inActiveSpaceItemColor = inActiveSpaceItemColor;
    }

    /**
     * Set item icon size
     *
     * @param spaceItemIconSize target size
     */
    public void setSpaceItemIconSize(int spaceItemIconSize) {
        this.spaceItemIconSize = spaceItemIconSize;
    }

    /**
     * Set item icon size if showIconOnly() called
     *
     * @param spaceItemIconOnlySize target size
     */
    public void setSpaceItemIconSizeInOnlyIconMode(int spaceItemIconOnlySize) {
        this.spaceItemIconOnlySize = spaceItemIconOnlySize;
    }

    /**
     * Set item text size
     *
     * @param spaceItemTextSize target size
     */
    public void setSpaceItemTextSize(int spaceItemTextSize) {
        this.spaceItemTextSize = spaceItemTextSize;
    }

    /**
     * Show only text in item
     */
    public void showTextOnly() {
        isTextOnlyMode = true;
    }

    /**
     * Show only icon in item
     */
    public void showIconOnly() {
        isIconOnlyMode = true;
    }

    /**
     * Makes centre button selectable
     */
    public void setCentreButtonSelectable(boolean isSelectable) {
        this.isCentreButtonSelectable = isSelectable;
    }

    /**
     * Add space item to navigation
     *
     * @param spaceItem item to add
     */
    public void addSpaceItem(SpaceItem spaceItem) {
        spaceItems.add(spaceItem);
    }

    /**
     * Change current selected item to centre button
     */
    public void setCentreButtonSelected() {
        if (!isCentreButtonSelectable)
            throw new ArrayIndexOutOfBoundsException("Please be more careful, you must set the centre button selectable");
        else
            updateSpaceItems(-1);
    }

    /**
     * Set space item and centre click
     *
     * @param spaceOnClickListener space click listener
     */
    public void setSpaceOnClickListener(SpaceOnClickListener spaceOnClickListener) {
        this.spaceOnClickListener = spaceOnClickListener;
    }

    /**
     * Change current selected item to given index
     * Note: -1 represents the centre button
     *
     * @param indexToChange given index
     */
    public void changeCurrentItem(int indexToChange) {
        if (indexToChange < -1 || indexToChange > spaceItems.size())
            throw new ArrayIndexOutOfBoundsException("Please be more careful, we do't have such item : " + indexToChange);
        else {
            updateSpaceItems(indexToChange);
        }
    }

    /**
     * Show badge at index
     *
     * @param itemIndex index
     * @param badgeText badge count text
     */
    public void showBadgeAtIndex(int itemIndex, int badgeText, @ColorInt int badgeColor) {
        if (itemIndex < 0 || itemIndex > spaceItems.size()) {
            throwArrayIndexOutOfBoundsException(itemIndex);
        } else {
            RelativeLayout badgeView = badgeList.get(itemIndex);

            /**
             * Set circle background to badge view
             */
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                badgeView.setBackground(BadgeHelper.makeShapeDrawable(badgeColor));
            } else {
                badgeView.setBackgroundDrawable(BadgeHelper.makeShapeDrawable(badgeColor));
            }

            BadgeItem badgeItem = new BadgeItem(itemIndex, badgeText, badgeColor);
            BadgeHelper.showBadge(badgeView, badgeItem, shouldShowBadgeWithNinePlus);
            badgeSaveInstanceHashMap.put(itemIndex, badgeItem);
        }
    }

    /**
     * Restore translation height from saveInstance
     */
    @SuppressWarnings("unchecked")
    private void restoreTranslation() {
        Bundle restoredBundle = savedInstanceState;
        if (restoredBundle != null) {
            if (restoredBundle.containsKey(VISIBILITY)) {
                this.setTranslationY(restoredBundle.getFloat(VISIBILITY));
            }
        }
    }

    /**
     * Hide badge at index
     *
     * @param index badge index
     * @deprecated Use {@link #hideBadgeAtIndex(int index)} instead.
     */
    @Deprecated
    public void hideBudgeAtIndex(final int index) {
        if (badgeList.get(index).getVisibility() == GONE) {
            Log.d(TAG, "Badge at index: " + index + " already hidden");
        } else {
            BadgeHelper.hideBadge(badgeList.get(index));
            badgeSaveInstanceHashMap.remove(index);
        }
    }

    /**
     * Hide badge at index
     *
     * @param index badge index
     */
    public void hideBadgeAtIndex(final int index) {
        if (badgeList.get(index).getVisibility() == GONE) {
            Log.d(TAG, "Badge at index: " + index + " already hidden");
        } else {
            BadgeHelper.hideBadge(badgeList.get(index));
            badgeSaveInstanceHashMap.remove(index);
        }
    }

    /**
     * Hiding all available badges
     *
     * @deprecated Use {@link #hideAllBadges()} instead.
     */
    @Deprecated
    public void hideAllBudges() {
        for (RelativeLayout badge : badgeList) {
            if (badge.getVisibility() == VISIBLE)
                BadgeHelper.hideBadge(badge);
        }
//        badgeSaveInstanceHashMap.clear();
        badgeSaveInstanceHashMap = new HashMap<>();
    }

    /**
     * Hiding all available badges
     */
    public void hideAllBadges() {
        for (RelativeLayout badge : badgeList) {
            if (badge.getVisibility() == VISIBLE)
                BadgeHelper.hideBadge(badge);
        }
//        badgeSaveInstanceHashMap.clear();
        badgeSaveInstanceHashMap = new HashMap<>();
    }

    /**
     * Change badge text at index
     *
     * @param badgeIndex target index
     * @param badgeText  badge count text to change
     */
    public void changeBadgeTextAtIndex(int badgeIndex, int badgeText) {
        if (badgeSaveInstanceHashMap.get(badgeIndex) != null && (((BadgeItem) badgeSaveInstanceHashMap.get(badgeIndex)).getIntBadgeText() != badgeText)) {
            BadgeItem currentBadgeItem = (BadgeItem) badgeSaveInstanceHashMap.get(badgeIndex);
            BadgeItem badgeItemForSave = new BadgeItem(badgeIndex, badgeText, currentBadgeItem.getBadgeColor());
            BadgeHelper.forceShowBadge(badgeList.get(badgeIndex), badgeItemForSave, shouldShowBadgeWithNinePlus);
            badgeSaveInstanceHashMap.put(badgeIndex, badgeItemForSave);
        }
    }

    /**
     * Set custom font for space item textView
     *
     * @param customFont custom font
     */
    public void setFont(Typeface customFont) {
        isCustomFont = true;
        this.customFont = customFont;
    }

    /**
     * Change centre button icon if space navigation already set up
     *
     * @param icon Target icon to change
     */
    public void changeCenterButtonIcon(int icon) {
//        if (centreButton == null) {
//            Log.e(TAG, "You should call setCentreButtonIcon() instead, " +
//                    "changeCenterButtonIcon works if space navigation already set up");
//        } else {
//            centreButton.setImageResource(icon);
//            centreButtonIcon = icon;
//        }
    }

    /**
     * Change item icon if space navigation already set up
     *
     * @param itemIndex Target position
     * @param newIcon   Icon to change
     */
    public void changeItemIconAtPosition(int itemIndex, int newIcon) {
        if (itemIndex < 0 || itemIndex > spaceItems.size()) {
            throwArrayIndexOutOfBoundsException(itemIndex);
        } else {
            SpaceItem spaceItem = spaceItems.get(itemIndex);
            RelativeLayout textAndIconContainer = (RelativeLayout) spaceItemList.get(itemIndex);
            ImageView spaceItemIcon = (ImageView) textAndIconContainer.findViewById(R.id.space_icon);
            spaceItemIcon.setImageResource(newIcon);
            spaceItem.setItemIcon(newIcon);
            changedItemAndIconHashMap.put(itemIndex, spaceItem);
        }
    }

    /**
     * Change item text if space navigation already set up
     *
     * @param itemIndex Target position
     * @param newText   Text to change
     */
    public void changeItemTextAtPosition(int itemIndex, String newText) {
        if (itemIndex < 0 || itemIndex > spaceItems.size()) {
            throwArrayIndexOutOfBoundsException(itemIndex);
        } else {
            SpaceItem spaceItem = spaceItems.get(itemIndex);
            RelativeLayout textAndIconContainer = (RelativeLayout) spaceItemList.get(itemIndex);
            TextView spaceItemIcon = (TextView) textAndIconContainer.findViewById(R.id.space_text);
            spaceItemIcon.setText(newText);
            spaceItem.setItemName(newText);
            changedItemAndIconHashMap.put(itemIndex, spaceItem);
        }
    }

    /**
     * Change space background color if space view already set up
     *
     * @param color Target color to change
     */
    public void changeSpaceBackgroundColor(@ColorInt int color) {
        if (color == spaceBackgroundColor) {
            Log.d(TAG, "changeSpaceBackgroundColor: color already changed");
            return;
        }

        spaceBackgroundColor = color;
        setBackgroundColors();
        centreContent.changeBackgroundColor(color);
    }


    /**
     * If you want to show full badge text or show 9+
     *
     * @param shouldShowBadgeWithNinePlus false for full text
     */
    public void shouldShowFullBadgeText(boolean shouldShowBadgeWithNinePlus) {
        this.shouldShowBadgeWithNinePlus = shouldShowBadgeWithNinePlus;
    }

    /**
     * set active centre button color
     *
     * @param color target color
     */
    public void setActiveCentreButtonIconColor(@ColorInt int color) {
        //   activeCentreButtonIconColor = color;
    }

    /**
     * set inactive centre button color
     *
     * @param color target color
     */
    public void setInActiveCentreButtonIconColor(@ColorInt int color) {
        //  inActiveCentreButtonIconColor = color;
    }
}
