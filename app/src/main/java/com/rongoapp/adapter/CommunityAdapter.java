package com.rongoapp.adapter;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.text.HtmlCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.rongoapp.R;
import com.rongoapp.activities.CommunityDetailsActivity;
import com.rongoapp.activities.HomeActivity;
import com.rongoapp.controller.AudioPlayerTask;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.PostData;
import com.rongoapp.data.ProblemRelatedData;
import com.rongoapp.data.StateCityDataModel;
import com.rongoapp.data.SurveyQuestions;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.fragments.CommunityFragment;
import com.rongoapp.utilities.InternetConnection;

import org.json.JSONArray;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class CommunityAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    CommunityFragment communityFragment;
    HomeActivity homeActivity;
    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    AudioPlayerTask audioPlayerTask;

    long mLastLongClick = 0;
    protected int mLastPosition = -1;

    int LIST_AD_DELTA = 3;
    private static final int CONTENT_TYPE = 0;
    private static final int AD_TYPE = 1;
    private static final int LOAD_TYPE = 2;

    List<PostData> postData;
    OnLoadMoreListener onLoadMoreListener;
    boolean isMoreLoading = true, isSurvey = false;
    String imagePath, audioPath;

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public CommunityAdapter(CommunityFragment communityFragment, OnLoadMoreListener onLoadMoreListener, String imagePath, String audioPath, HomeActivity homeActivity) {
        this.homeActivity = homeActivity;
        this.communityFragment = communityFragment;
        this.imagePath = imagePath;
        this.audioPath = audioPath;
        this.onLoadMoreListener = onLoadMoreListener;
        sharedPreferences = homeActivity.getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(homeActivity);
        LIST_AD_DELTA = Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_survey_post_position, "3"));

        String surveyId = "1";
        if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_LAST_SURVEY_ID, "1"))) {
            surveyId = sharedPreferences.getString(Constants.SharedPreferences_LAST_SURVEY_ID, "1");
        }
        if (surveyId.equalsIgnoreCase("null") || surveyId == null) {
            surveyId = "1";
        }

        List<SurveyQuestions> surveyQuestions = appDatabase.surveyQuestionsDao().getNextQuestion(Integer.parseInt(surveyId));
        if (!surveyQuestions.isEmpty()) {
            if (sharedPreferences.getString(Constants.SharedPreferences_is_survey_visible, "1").equalsIgnoreCase("1")) {
                isSurvey = true;
            }
        }
    }

    public void showLoading() {
        if (isMoreLoading && postData != null && onLoadMoreListener != null) {
            isMoreLoading = false;
            new Handler().post(new Runnable() {
                @Override
                public void run() {
                    postData.add(null);
                    notifyItemInserted(postData.size() - 1);
                    onLoadMoreListener.onLoadMore();
                }
            });
        }
    }

    public void setMore(boolean isMore) {
        this.isMoreLoading = isMore;
    }

    public void dismissLoading() {
        if (postData != null && postData.size() > 0) {
            postData.remove(postData.size() - 1);
            notifyItemRemoved(postData.size());
        }
    }

    public void addAll(List<PostData> lst) {
        postData = new ArrayList<>();
        postData.addAll(lst);
        notifyDataSetChanged();
    }

    public void addItemMore(List<PostData> lst) {
        int sizeInit = postData.size();
        postData.addAll(lst);
        notifyItemRangeChanged(sizeInit, postData.size());
        notifyDataSetChanged();
    }


    public static class LoadingViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    void showLoadingView(LoadingViewHolder viewHolder, int position) {
        viewHolder.progressBar.setIndeterminate(true);
        viewHolder.progressBar.setVisibility(View.VISIBLE);
    }


    public static class SurveyViewHolder extends RecyclerView.ViewHolder {
        MaterialCardView surveyCardView;
        TextView tvRewardNew;

        public SurveyViewHolder(@NonNull View itemView) {
            super(itemView);
            surveyCardView = itemView.findViewById(R.id.surveyCardView);
            tvRewardNew = itemView.findViewById(R.id.tvRewardNew);
        }
    }

    void showSurveyView(SurveyViewHolder holder, int position) {
        holder.tvRewardNew.setText(RewardPoints.getSurveyRewardPoint(sharedPreferences));
        holder.surveyCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                communityFragment.setSurveyDialog();
                isSurvey = false;
            }
        });
    }

    protected void setAnimation(View viewToAnimate, int position) {
        if (position > mLastPosition) {
            ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(505);
            viewToAnimate.startAnimation(anim);
            mLastPosition = position;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == AD_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.community_survey_item, parent, false);
            return new SurveyViewHolder(view);
        } else if (viewType == LOAD_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_loading, parent, false);
            return new LoadingViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_community, parent, false);
            return new ItemViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        setAnimation(viewHolder.itemView, position);

        if (viewHolder instanceof ItemViewHolder) {
            populateItemRows((ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof LoadingViewHolder) {
            showLoadingView((LoadingViewHolder) viewHolder, position);
        } else if (viewHolder instanceof SurveyViewHolder) {
            showSurveyView((SurveyViewHolder) viewHolder, position);
        }
    }

    @Override
    public int getItemCount() {
        return postData == null ? 0 : postData.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (isSurvey) {
            if (position == LIST_AD_DELTA)
                return AD_TYPE;
        }
        return postData.get(position) == null ? LOAD_TYPE : CONTENT_TYPE;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        public AppCompatTextView tvCommentCount, txtNew, txtQuestionRelated, txtDate, tvExpertSujav, tvView, tvLike, tvShare, tvRog, tvUserLocation, tvUserName;
        public ImageView ivIcon, ivPlay, ivIsExpert;
        public ProgressBar progress, progressBar;
        public LinearLayout llAudioView, llUserDetail;

        public ItemViewHolder(View itemView) {
            super(itemView);
            this.llUserDetail = itemView.findViewById(R.id.llUserDetail);
            this.ivIsExpert = itemView.findViewById(R.id.ivIsExpert);
            this.progressBar = itemView.findViewById(R.id.pb);
            this.progressBar.setVisibility(View.GONE);
            this.txtQuestionRelated = itemView.findViewById(R.id.txtQuestionRelated);
            this.txtDate = itemView.findViewById(R.id.txtDate);
            this.txtNew = itemView.findViewById(R.id.txtNew);
            txtNew.setVisibility(View.GONE);
            this.tvCommentCount = itemView.findViewById(R.id.tvCommentCount);
            this.tvView = itemView.findViewById(R.id.tvView);
            this.tvLike = itemView.findViewById(R.id.tvLike);
            this.tvShare = itemView.findViewById(R.id.tvShare);
            this.tvRog = itemView.findViewById(R.id.tvRog);
            this.tvExpertSujav = itemView.findViewById(R.id.tvExpertSujav);
            this.tvUserLocation = itemView.findViewById(R.id.tvUserLocation);
            this.tvUserName = itemView.findViewById(R.id.tvUserName);

            this.ivIcon = itemView.findViewById(R.id.ivIcon);
            this.ivPlay = itemView.findViewById(R.id.ivPlay);

            this.progress = itemView.findViewById(R.id.progress);
            this.progress.setVisibility(View.GONE);
            this.llAudioView = itemView.findViewById(R.id.llAudioView);
        }
    }

    void populateItemRows(ItemViewHolder holder, int position) {
        try {
            holder.tvUserLocation.setText("" + postData.get(position).getPosted_user_location());

            List<StateCityDataModel> parseStateCityListData = Functions.parseStateCityListData(sharedPreferences);
            for (int i = 0; i < parseStateCityListData.size(); i++) {
                if (postData.get(position).getPosted_user_location().equalsIgnoreCase(parseStateCityListData.get(i).getState_english())) {
                    holder.tvUserLocation.setText("" + parseStateCityListData.get(i).getState_hindi());
                }
            }

            if (postData.get(position).getPost_type().equalsIgnoreCase("3")) {
                holder.tvUserName.setText(homeActivity.getResources().getString(R.string.str_rongo_expert));
                holder.llUserDetail.setVisibility(View.GONE);
            } else {
                holder.llUserDetail.setVisibility(View.VISIBLE);
                holder.tvUserName.setText("" + postData.get(position).getPosted_user_name());
            }

            if (postData.get(position).getIs_expert().equalsIgnoreCase("1")) {
                holder.ivIsExpert.setVisibility(View.VISIBLE);
            } else {
                holder.ivIsExpert.setVisibility(View.GONE);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    Intent intent = new Intent(holder.itemView.getContext(), CommunityDetailsActivity.class);
                    intent.putExtra("postData", postData.get(position));
                    intent.putExtra("imagePath", imagePath);
                    intent.putExtra("type", "1");
                    intent.putExtra("post_id", postData.get(position).getPost_id());
                    holder.itemView.getContext().startActivity(intent);
                }
            });

            JSONArray imageArray = new JSONArray(postData.get(position).getPicture_key());
            if (imageArray != null && imageArray.length() > 0) {
                holder.ivIcon.setVisibility(View.VISIBLE);
                if (TextUtils.isEmpty(imagePath)) {
                    imagePath = Constants.IMAGE_PATH_LINK;
                }
                Functions.displayImage(imagePath + imageArray.opt(0).toString(), holder.ivIcon);
            } else {
                holder.ivIcon.setVisibility(View.GONE);
            }

            holder.tvShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (Functions.doubleTapCheck(mLastLongClick)) {
                            return;
                        }
                        mLastLongClick = SystemClock.elapsedRealtime();

                        Functions.showProgress(homeActivity, holder.progressBar);
                        String test = homeActivity.getResources().getString(R.string.str_rongo_community) + postData.get(position).getDescription() + "</br> <br>" + postData.get(position).getShare_link() + "</br>";
                        String shareTxt = "" + HtmlCompat.fromHtml(test, HtmlCompat.FROM_HTML_MODE_LEGACY);

                        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                            if (holder.ivIcon.getVisibility() == View.VISIBLE) {
                                JSONArray jsonArray = new JSONArray(postData.get(position).getPicture_key());
                                if (jsonArray != null && jsonArray.length() > 0) {
                                    if (TextUtils.isEmpty(imagePath)) {
                                        imagePath = Constants.IMAGE_PATH_LINK;
                                    }

                                    Functions.hideProgress(homeActivity, holder.progressBar);
                                    Functions.shareCommunity(homeActivity, shareTxt, imagePath + jsonArray.opt(0).toString());

                                } else {
                                    Functions.hideProgress(homeActivity, holder.progressBar);
                                    Functions.shareCommunity(homeActivity, shareTxt, "");
                                }
                            } else {
                                Functions.hideProgress(homeActivity, holder.progressBar);
                                Functions.shareCommunity(homeActivity, shareTxt, "");
                            }
                        } else {
                            Functions.hideProgress(homeActivity, holder.progressBar);
                            Functions.shareCommunity(homeActivity, shareTxt, "");
                        }
                        Functions.hideProgress(homeActivity, holder.progressBar);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Functions.hideProgress(homeActivity, holder.progressBar);
                    }
                }
            });

            if (!TextUtils.isEmpty(postData.get(position).getLikes())) {
                holder.tvLike.setText(postData.get(position).getLikes() + " " + holder.itemView.getContext().getResources().getString(R.string.str_like_txt));
            } else {
                holder.tvLike.setText("0 " + holder.itemView.getContext().getResources().getString(R.string.str_like_txt));
            }

            if (!TextUtils.isEmpty(postData.get(position).getViews())) {
                holder.tvView.setText(postData.get(position).getViews() + " " + holder.itemView.getContext().getResources().getString(R.string.str_view_txt));
            } else {
                holder.tvView.setText("0 " + holder.itemView.getContext().getResources().getString(R.string.str_view_txt));
            }

            if (!TextUtils.isEmpty(postData.get(position).getVote_type())) {
                if (postData.get(position).getVote_type().equalsIgnoreCase("1")) {
                    holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.like, 0, 0, 0);
                } else {
                    holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.unlike, 0, 0, 0);
                }
            } else {
                holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.unlike, 0, 0, 0);
            }

            holder.tvLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    if (!TextUtils.isEmpty(postData.get(position).getVote_type())) {
                        if (postData.get(position).getVote_type().equalsIgnoreCase("1")) {
                            holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.like, 0, 0, 0);
                        } else {
                            holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.unlike, 0, 0, 0);
                            communityFragment.likeCommunityPost(position, postData.get(position).getPost_id());
                        }
                    } else {
                        holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.unlike, 0, 0, 0);
                        communityFragment.likeCommunityPost(position, postData.get(position).getPost_id());
                    }
                }
            });

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            Calendar cal = Calendar.getInstance();
            cal.setTime(sdf.parse(postData.get(position).getCreated_on()));
            holder.txtDate.setText(Functions.setTimeAgoMyTopic(holder.itemView.getContext(), cal.getTimeInMillis()));

            if (!TextUtils.isEmpty(postData.get(position).getDescription())) {
                holder.txtQuestionRelated.setVisibility(View.VISIBLE);
                holder.txtQuestionRelated.setText(HtmlCompat.fromHtml(postData.get(position).getDescription(), HtmlCompat.FROM_HTML_MODE_LEGACY));
            } else {
                holder.txtQuestionRelated.setVisibility(View.GONE);
            }

            String type = homeActivity.getResources().getString(R.string.str_other);
            List<ProblemRelatedData> problemRelatedData = SharedPreferencesUtils.getProblemRelated(holder.itemView.getContext(), Constants.SharedPreferences_OPTIONS);
            if (problemRelatedData != null && problemRelatedData.size() > 0) {
                for (int i = 0; i < problemRelatedData.size(); i++) {
                    if (postData.get(position).getProblem_related().equalsIgnoreCase(problemRelatedData.get(i).getKey())) {
                        if (postData.get(position).getProblem_related().equalsIgnoreCase("5") ||
                                postData.get(position).getProblem_related().equalsIgnoreCase("0")) {
                            type = problemRelatedData.get(i).getValue();
                        } else {
                            type = problemRelatedData.get(i).getValue() + " " + homeActivity.getResources().getString(R.string.str_samndhi);
                        }
                        break;
                    }
                }
            }
            holder.tvRog.setText(HtmlCompat.fromHtml("<B>" + type + "</B>", HtmlCompat.FROM_HTML_MODE_LEGACY));

//            holder.tvCommentCount.setPaintFlags(holder.tvCommentCount.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            holder.tvCommentCount.setText("0 " + homeActivity.getResources().getString(R.string.str_comments_txt));
            holder.tvExpertSujav.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(postData.get(position).getComments())) {
                JSONArray jsonArray = new JSONArray(postData.get(position).getComments());
                if (jsonArray != null && jsonArray.length() > 0) {
                    holder.tvCommentCount.setText(jsonArray.length() + " " + homeActivity.getResources().getString(R.string.str_comments_txt));
                    for (int i = 0; i < jsonArray.length(); i++) {
                        if (jsonArray.optJSONObject(i).optString("type").equalsIgnoreCase("0")) {
                            holder.tvExpertSujav.setVisibility(View.VISIBLE);
                            break;
                        } else {
                            holder.tvExpertSujav.setVisibility(View.GONE);
                        }
                    }
                }
            }

            holder.tvCommentCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    Dexter.withContext(homeActivity).withPermissions(Manifest.permission.RECORD_AUDIO, WRITE_EXTERNAL_STORAGE, READ_EXTERNAL_STORAGE)
                            .withListener(new MultiplePermissionsListener() {
                                @Override
                                public void onPermissionsChecked(MultiplePermissionsReport report) {
                                    if (report.areAllPermissionsGranted()) {
                                        communityFragment.openCommentDialog(position, postData.get(position).getPost_id(), postData.get(position).getComments());
                                    }
                                }

                                @Override
                                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                    token.continuePermissionRequest();
                                }
                            }).check();

                }
            });

            if (postData.get(position).getIs_commented().equalsIgnoreCase("1")) {
                holder.txtNew.setVisibility(View.VISIBLE);
            } else {
                holder.txtNew.setVisibility(View.GONE);
            }

            holder.ivPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    audioPlayerTask = new AudioPlayerTask(homeActivity, holder.ivPlay);
                    audioPlayerTask.onPlayMedia(getAudioLink(position));
                }
            });

            holder.llAudioView.setVisibility(View.GONE);
            if (postData.get(position).getRecording_key() != null && !TextUtils.isEmpty(postData.get(position).getRecording_key()) && !postData.get(position).getRecording_key().equalsIgnoreCase("null")) {
                if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                    JSONArray jArray = new JSONArray(postData.get(position).getRecording_key().toString());
                    if (jArray != null && jArray.length() > 0) {
                        holder.llAudioView.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                if (!postData.get(position).getRecording_local_path().equalsIgnoreCase("null") && !TextUtils.isEmpty(postData.get(position).getRecording_local_path())) {
                    String audio_Path = postData.get(position).getRecording_local_path();
                    File file = new File(Functions.replaceAudioLinkString(audio_Path));
                    if (file.exists()) {
                        holder.llAudioView.setVisibility(View.VISIBLE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(homeActivity, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
            Functions.hideProgress(homeActivity, holder.progressBar);
        }
    }

    public String getAudioLink(int position) {
        String audio_Path = "";
        try {
            if (!TextUtils.isEmpty(postData.get(position).getRecording_key()) && !postData.get(position).getRecording_key().equalsIgnoreCase("null")) {
                JSONArray jArray = new JSONArray(postData.get(position).getRecording_key().toString());
                if (jArray != null && jArray.length() > 0) {
                    audio_Path = audioPath + jArray.opt(0).toString();
                }
            } else {
                if (!postData.get(position).getRecording_local_path().equalsIgnoreCase("null") && !TextUtils.isEmpty(postData.get(position).getRecording_local_path())) {
                    audio_Path = postData.get(position).getRecording_local_path();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return audio_Path;
    }

}
