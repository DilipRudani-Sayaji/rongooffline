package com.rongoapp.adapter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.rongoapp.R;
import com.rongoapp.activities.PostQuestionActivity;
import com.rongoapp.activities.QuestionSampleActivity;
import com.rongoapp.activities.QuizActivity;
import com.rongoapp.activities.RewardActivity;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.OfflineRewardData;
import com.rongoapp.data.RewardData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.spinWheel.LuckyItem;
import com.rongoapp.spinWheel.LuckyWheelView;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PendingRewardAdapter extends RecyclerView.Adapter<PendingRewardAdapter.ViewHolder> {

    RewardActivity rewardActivity;
    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    List<LuckyItem> data = new ArrayList<>();
    String contestID = "", rewardId = "";
    List<RewardData> listdata;
    long mLastLongClick = 0;

    public PendingRewardAdapter(RewardActivity rewardActivity, List<RewardData> listdata, String contest_id) {
        this.rewardActivity = rewardActivity;
        this.listdata = listdata;
        sharedPreferences = rewardActivity.getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(rewardActivity);
        contestID = contest_id;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.pending_reward_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try {
            holder.tvTitle.setText(listdata.get(position).getRewardTitle());

            if (listdata.get(position).getType().equalsIgnoreCase("jackpot")) {
                holder.ivRewardIcon.setImageResource(R.mipmap.spin_icon_big);
            } else if (listdata.get(position).getType().equalsIgnoreCase("recharge_coupon")) {
                holder.ivRewardIcon.setImageResource(R.mipmap.recharge_big);
            } else if (listdata.get(position).getType().equalsIgnoreCase("expert_call")) {
                holder.ivRewardIcon.setImageResource(R.mipmap.e_call_big);
            } else if (listdata.get(position).getType().equalsIgnoreCase("combo")) {
                holder.ivRewardIcon.setImageResource(R.mipmap.combo_big);
            }

            if (Integer.parseInt(Functions.updateRewardPoints(sharedPreferences, "0")) <= Integer.parseInt(listdata.get(position).getStatus())) {
                holder.tvRewardPoint.setText(listdata.get(position).getStatus() + " " + rewardActivity.getResources().getString(R.string.str_points) + " " + rewardActivity.getResources().getString(R.string.str_require));
                holder.tvRewardPoint.setVisibility(View.VISIBLE);
                holder.llView.setAlpha(0.2f);
            } else {
                holder.llView.setAlpha(1f);
                holder.tvRewardPoint.setVisibility(View.GONE);
                holder.llView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Functions.doubleTapCheck(mLastLongClick)) {
                            return;
                        }
                        mLastLongClick = SystemClock.elapsedRealtime();

                        if (listdata.get(position).getType().equalsIgnoreCase("jackpot")) {
                            if (Functions.getContestAvailable(sharedPreferences).equalsIgnoreCase("2")) {
                                showSpinRewardDialog(listdata.get(position).getRewardId(), listdata.get(position).getRewardTitle(),
                                        "0", listdata.get(position).getDescription(), false);
//                                Log.e("@@@@@", "1");
                            } else {
                                openSpinWheelDialog(position, listdata.get(position).getRewardId(), listdata.get(position).getStatus(),
                                        listdata.get(position).getRewardTitle());
//                                Log.e("@@@@@", "2");
                            }
                        } else {
//                            Log.e("@@@@@", "3");
                            showDialog(listdata.get(position).getRewardId(), listdata.get(position).getType(), listdata.get(position).getRewardTitle(),
                                    listdata.get(position).getDescription(), listdata.get(position).getStatus(), listdata.get(position).getMetadata());
                        }
                    }
                });
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvRewardPoint;
        ImageView ivRewardIcon;
        LinearLayout llView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.llView = (LinearLayout) itemView.findViewById(R.id.llView);
            this.tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            this.tvRewardPoint = (TextView) itemView.findViewById(R.id.tvRewardPoint);
            this.ivRewardIcon = (ImageView) itemView.findViewById(R.id.ivRewardIcon);
        }
    }


    void showDialog(final String id, String type, String title, String decs, final String status, final String metadata) {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(rewardActivity);
        View sheetView = LayoutInflater.from(rewardActivity).inflate(R.layout.pending_reward_screen_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        TextView txtTitle = (TextView) sheetView.findViewById(R.id.txtTitle);
        txtTitle.setText(title);
        TextView txtDesc = (TextView) sheetView.findViewById(R.id.txtDesc);
//        txtDesc.setText(decs);

        Bundle param = new Bundle();
        param.putString(Constants.FB_EVENT_PARAM_CONTENT_TYPE, type);
        param.putString(Constants.EVENT_PARAM_USER_ID, RongoApp.getUserId());
        AppEventsLogger.newLogger(rewardActivity).logEvent(Constants.FB_EVENT_NAME_SPENT_CREDITS, param);

        ImageView ivRewardIcon = (ImageView) sheetView.findViewById(R.id.ivRewardIcon);
        if (type.equalsIgnoreCase("jackpot")) {
            ivRewardIcon.setImageResource(R.mipmap.spin_icon_big);
        } else if (type.equalsIgnoreCase("recharge_coupon")) {
            ivRewardIcon.setImageResource(R.mipmap.recharge_big);
        } else if (type.equalsIgnoreCase("expert_call")) {
            ivRewardIcon.setImageResource(R.mipmap.e_call_big);
        } else if (type.equalsIgnoreCase("combo")) {
            ivRewardIcon.setImageResource(R.mipmap.combo_big);
        }

        TextView tvRewardPoint = (TextView) sheetView.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText(status + " " + rewardActivity.getResources().getString(R.string.str_points));

        ImageView imgClose = (ImageView) sheetView.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });

        final TextView tvOffline = (TextView) sheetView.findViewById(R.id.tvOffline);
        tvOffline.setVisibility(View.GONE);

        final TextView tvRedMsg = (TextView) sheetView.findViewById(R.id.tvRedMsg);
        tvRedMsg.setVisibility(View.GONE);

        final TextView tvRewardMsg = (TextView) sheetView.findViewById(R.id.tvRewardMsg);
        tvRewardMsg.setVisibility(View.VISIBLE);

        TextView tvRechargeTxt = (TextView) sheetView.findViewById(R.id.tvRechargeTxt);
        tvRechargeTxt.setVisibility(View.GONE);

        if (type.equalsIgnoreCase("recharge_coupon")) {
            tvRechargeTxt.setVisibility(View.VISIBLE);
        } else {
            tvRechargeTxt.setVisibility(View.GONE);
        }

        if (InternetConnection.checkConnectionForFasl(rewardActivity)) {
            tvRewardMsg.setAlpha(1.0f);
            tvOffline.setVisibility(View.GONE);
        } else {
            tvRewardMsg.setAlpha(0.5f);
            tvOffline.setVisibility(View.VISIBLE);
        }

        tvRewardMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(Functions.updateRewardPoints(sharedPreferences, "0")) >= Integer.parseInt(status)) {
                    tvRedMsg.setVisibility(View.GONE);
                    if (InternetConnection.checkConnectionForFasl(rewardActivity)) {
                        claimRewardUsingPoints(status, getMetaData(id, metadata));
                        tvRewardMsg.setAlpha(1.0f);
                        tvOffline.setVisibility(View.GONE);
                        mBottomSheetDialog.dismiss();
                    } else {
                        tvRewardMsg.setAlpha(0.5f);
                        tvOffline.setVisibility(View.VISIBLE);
                    }
                } else {
                    tvRedMsg.setVisibility(View.VISIBLE);
//                    Functions.showSnackBar(rewardActivity.findViewById(android.R.id.content), "आपके पास पर्याप्त पॉइंट्स नहीं हैं।");
                }
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        ((View) sheetView.getParent()).setBackgroundColor(rewardActivity.getResources().getColor(android.R.color.transparent));
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();
    }

    String getMetaData(String id, String json) {
        String metaData = "";
        try {
            JSONArray jsonArray = new JSONArray(json);
            if (jsonArray != null && jsonArray.length() > 0) {
                JSONObject jb = jsonArray.optJSONObject(0);
                JSONObject obj = new JSONObject();
                obj.put("is_true", jb.optString("is_true"));
                obj.put("value", jb.optString("value"));
                obj.put("type", jb.optString("type"));
                obj.put("text", jb.optString("text"));
                obj.put("reward_id", id);
                obj.put("status", "1");
                metaData = obj.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return metaData;
    }

    void claimRewardUsingPoints(final String points, String metadata) {
        try {
            RetrofitClient.getInstance().getApi().claimRewardUsingPoints(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    SharedPreferencesUtils.getUserId(rewardActivity), "0", "", points, metadata).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        Functions.deductRewardPoints(sharedPreferences, points);
                        rewardActivity.getRewardUpdate();
                        openThankYouDialog("1");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void openThankYouDialog(String str) {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(rewardActivity);
        View view = LayoutInflater.from(rewardActivity).inflate(R.layout.reward_thank_you_dialog, null);
        bottomSheetDialog.setContentView(view);

        AppCompatTextView txtTitle = view.findViewById(R.id.txtTitle);
        ImageView ivBanner = view.findViewById(R.id.ivBanner);
        AppCompatTextView tvDecs = view.findViewById(R.id.tvDecs);

        if (str.equalsIgnoreCase("1")) {
            ivBanner.setImageResource(R.mipmap.expert_speak);
            tvDecs.setText(R.string.str_help_to_rongo_exp);
        } else {
            ivBanner.setImageResource(R.mipmap.open_box);
            tvDecs.setText(R.string.str_you_get_reward);
        }

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        AppCompatTextView txtEnter = view.findViewById(R.id.txtEnter);
        txtEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(rewardActivity.getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    void openSpinWheelDialog(int pos, String id, String point, String title) {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(rewardActivity);
        View sheetView = LayoutInflater.from(rewardActivity).inflate(R.layout.spin_wheel_contest_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        TextView tvRewardPoint = (TextView) sheetView.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText(point + " " + rewardActivity.getResources().getString(R.string.str_points));
        TextView tvRedMsg = (TextView) sheetView.findViewById(R.id.tvRedMsg);
        tvRedMsg.setVisibility(View.GONE);
        Button btnSpinPlay = (Button) sheetView.findViewById(R.id.btnSpinPlay);
        TextView tvOffline = (TextView) sheetView.findViewById(R.id.tvOffline);
        tvOffline.setVisibility(View.GONE);

        if (InternetConnection.checkConnectionForFasl(rewardActivity)) {
            btnSpinPlay.setAlpha(1.0f);
            tvOffline.setVisibility(View.GONE);
        } else {
            btnSpinPlay.setAlpha(0.5f);
            tvOffline.setVisibility(View.VISIBLE);
        }

        LuckyWheelView luckyWheelView = (LuckyWheelView) sheetView.findViewById(R.id.luckyWheel);
        setSpinnerWheelView();
        luckyWheelView.setData(data);
        luckyWheelView.setRound(3);
        luckyWheelView.setTouchEnabled(false);

        btnSpinPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (InternetConnection.checkConnection(rewardActivity)) {
                    if (Integer.parseInt(Functions.updateRewardPoints(sharedPreferences, "0")) >= Integer.parseInt(point)) {
                        tvRedMsg.setVisibility(View.GONE);
//                        luckyWheelView.startLuckyWheelWithTargetIndex(1);
                        luckyWheelView.startLuckyWheelWithTargetIndex(Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_SPIN_RESULT, "0")));

                        claimRewardUsingPoints(listdata.get(pos).getStatus(), getMetaData(listdata.get(pos).getRewardId(), listdata.get(pos).getMetadata()));
                    } else {
                        tvRedMsg.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        luckyWheelView.setLuckyRoundItemSelectedListener(new LuckyWheelView.LuckyRoundItemSelectedListener() {
            @Override
            public void LuckyRoundItemSelected(int index) {
                if (InternetConnection.checkConnectionForFasl(rewardActivity)) {
                    showSpinRewardDialog(id, "" + rewardActivity.getResources().getString(R.string.str_jackpot), "" + data.get(index).getSecondaryText(), "" + data.get(index).getType(), true);
                    btnSpinPlay.setAlpha(1.0f);
                    tvOffline.setVisibility(View.GONE);
                } else {
                    btnSpinPlay.setAlpha(0.5f);
                    tvOffline.setVisibility(View.VISIBLE);
                }
                mBottomSheetDialog.dismiss();
            }
        });

        ImageView imgClose = (ImageView) sheetView.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        ((View) sheetView.getParent()).setBackgroundColor(rewardActivity.getResources().getColor(android.R.color.transparent));
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();
    }

    void setSpinnerWheelView() {
        try {
            data = new ArrayList<>();
            String contestsData = sharedPreferences.getString(Constants.SharedPreferences_CONTEST, "");
            if (!TextUtils.isEmpty(contestsData)) {
//                Log.e("+++++++++", "" + contestsData);
                JSONArray jsonArray = new JSONArray(contestsData);
                if (jsonArray != null && jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject resultData = jsonArray.getJSONObject(i);
                        if (resultData.optString("type").equalsIgnoreCase("spin")) {
                            JSONArray spinArray = resultData.getJSONArray("json_data");
                            if (spinArray != null && spinArray.length() > 0) {
                                for (int j = 0; j < spinArray.length(); j++) {
                                    JSONObject jsonData = spinArray.getJSONObject(j);
                                    JSONArray spinDataArray = new JSONArray(jsonData.getString("spin"));
                                    if (spinDataArray != null && spinDataArray.length() > 0) {
                                        for (int k = 0; k < spinDataArray.length(); k++) {
                                            JSONObject spinData = spinDataArray.getJSONObject(k);

                                            LuckyItem luckyItem1 = new LuckyItem();
                                            if (k == 0) {
//                                                luckyItem1.setIcon(R.mipmap.thumb_1);
                                                luckyItem1.setSecondaryText("");
                                            } else if (k == 1) {
                                                luckyItem1.setIcon(R.mipmap.gift_r);
                                                luckyItem1.setSecondaryText(rewardActivity.getResources().getString(R.string.str_jackpot));
                                            } else if (k == 2) {
                                                luckyItem1.setSecondaryText("");
//                                                luckyItem1.setIcon(R.mipmap.thumb_2);
                                            } else if (k == 3) {
                                                luckyItem1.setSecondaryText("");
//                                                luckyItem1.setIcon(R.mipmap.thumb_3);
                                            } else if (k == 4) {
                                                luckyItem1.setIcon(R.mipmap.gift_s);
                                                luckyItem1.setSecondaryText(rewardActivity.getResources().getString(R.string.str_jackpot));
                                            } else if (k == 5) {
                                                luckyItem1.setSecondaryText("");
//                                                luckyItem1.setIcon(R.mipmap.thumb_4);
                                            }

                                            luckyItem1.setType(spinData.getString("type"));
                                            luckyItem1.setTopText(spinData.getString("text"));
                                            luckyItem1.setId(spinData.getString("id"));
                                            luckyItem1.setColor(Functions.getRandomColor(k));
                                            data.add(luckyItem1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void showSpinRewardDialog(String id, final String text, String value, String type, boolean isSpin) {
        String textPass = rewardActivity.getResources().getString(R.string.str_try_again);
        if (TextUtils.isEmpty(value)) {
            value = String.valueOf(0);
        }
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(rewardActivity);
        View sheetView = LayoutInflater.from(rewardActivity).inflate(R.layout.spin_wheel_result_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Spin_Date, Functions.getCurrentDate());

        ImageView ivGift = (ImageView) sheetView.findViewById(R.id.ivGift);
        TextView tvRewardMsg = (TextView) sheetView.findViewById(R.id.tvRewardMsg);
        TextView tvDecs = (TextView) sheetView.findViewById(R.id.tvDecs);
        TextView textView = (TextView) sheetView.findViewById(R.id.text);
        TextView tvFailText = (TextView) sheetView.findViewById(R.id.tvFailText);
        tvFailText.setText(R.string.str_try_again_new);

        TextView tvShareReward = (TextView) sheetView.findViewById(R.id.tvShareReward);
        tvShareReward.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));
        TextView tvExpertReward = (TextView) sheetView.findViewById(R.id.tvExpertReward);
        tvExpertReward.setText(RewardPoints.getPostQuestionRewardPoint(sharedPreferences));
        TextView tvPhotoReward = (TextView) sheetView.findViewById(R.id.tvPhotoReward);
        tvPhotoReward.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences));
        TextView tvQuizReward = (TextView) sheetView.findViewById(R.id.tvQuizReward);
        tvQuizReward.setText(RewardPoints.getDailyQuizRewardPoint(sharedPreferences));

        TextView tvOffline = (TextView) sheetView.findViewById(R.id.tvOffline);
        tvOffline.setVisibility(View.GONE);

        LinearLayout llSuccessView = (LinearLayout) sheetView.findViewById(R.id.llSuccessView);
        LinearLayout llFailView = (LinearLayout) sheetView.findViewById(R.id.llFailView);

        LinearLayout llJackPotTryAgain = (LinearLayout) sheetView.findViewById(R.id.llJackPotTryAgain);
        llJackPotTryAgain.setVisibility(View.GONE);

        RelativeLayout rlDailyQuiz = (RelativeLayout) sheetView.findViewById(R.id.rlDailyQuiz);
        rlDailyQuiz.setVisibility(View.GONE);
        RelativeLayout rlPhotoUpload = (RelativeLayout) sheetView.findViewById(R.id.rlPhotoUpload);
        RelativeLayout rlExpertCall = (RelativeLayout) sheetView.findViewById(R.id.rlExpertCall);
        RelativeLayout rlShare = (RelativeLayout) sheetView.findViewById(R.id.rlShare);

        int cropWeek = RongoApp.getCropWeekDaysFunction();
        if (cropWeek > 1 && cropWeek < Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"))) {
//            if (cropWeek > 1 && cropWeek < Functions.getQuestionIsAvailable(sharedPreferences)) {
//                rlDailyQuiz.setVisibility(View.VISIBLE);
//            } else {
//                rlDailyQuiz.setVisibility(View.GONE);
//            }
            rlDailyQuiz.setVisibility(View.VISIBLE);
        } else {
            rlDailyQuiz.setVisibility(View.GONE);
        }

        TextView tvRechargeTxt = (TextView) sheetView.findViewById(R.id.tvRechargeTxt);
        tvRechargeTxt.setVisibility(View.GONE);
        if (type.equalsIgnoreCase("recharge_coupon")) {
            tvRechargeTxt.setVisibility(View.VISIBLE);
        }

        if (value.equalsIgnoreCase("0")) {
            llFailView.setVisibility(View.VISIBLE);
            llJackPotTryAgain.setVisibility(View.VISIBLE);
            llSuccessView.setVisibility(View.GONE);
            ivGift.setVisibility(View.GONE);
            textPass = rewardActivity.getResources().getString(R.string.str_try_again);
        } else {
            textPass = text;
            ivGift.setVisibility(View.VISIBLE);
            llSuccessView.setVisibility(View.VISIBLE);
            llFailView.setVisibility(View.GONE);
            textView.setText(text);
            ivGift.setVisibility(View.VISIBLE);
            ivGift.setImageResource(R.mipmap.spin_icon);
        }

        rlDailyQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
//                Intent i = new Intent(rewardActivity, QuestionActivity.class);
//                rewardActivity.startActivity(i);

                Intent intent = null;
                try {
                    if (RongoApp.getSelectedCrop().equalsIgnoreCase("11")) {
                        intent = new Intent(rewardActivity, QuestionSampleActivity.class);
                    } else {
                        if (Functions.getQuizIntent(rewardActivity, sharedPreferences)) {
                            intent = new Intent(rewardActivity, QuizActivity.class);
                        } else {
                            intent = new Intent(rewardActivity, QuestionSampleActivity.class);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                rewardActivity.startActivity(intent);
            }
        });

        rlPhotoUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                rewardActivity.openCrops();
            }
        });

        rlExpertCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
                Intent i = new Intent(rewardActivity, PostQuestionActivity.class);
                i.putExtra("keyword_id", "0");
                rewardActivity.startActivityForResult(i, Constants.POST_QUESTION_CODE);
            }
        });

        rlShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
                Functions.performShare(rewardActivity, appDatabase);
            }
        });

        String finalValue = value;
        tvRewardMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (InternetConnection.checkConnectionForFasl(rewardActivity)) {
                    tvRewardMsg.setAlpha(1.0f);
                    tvOffline.setVisibility(View.GONE);
                    mBottomSheetDialog.dismiss();
                } else {
                    tvRewardMsg.setAlpha(0.5f);
                    tvOffline.setVisibility(View.VISIBLE);
                }

                showSpinDateTimeDialog(id, finalValue);
//                updateRewardStatus();
            }
        });

        ImageView ivClose = (ImageView) sheetView.findViewById(R.id.imgClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });

        if (isSpin) {
            if (InternetConnection.checkConnectionForFasl(rewardActivity)) {
                tvRewardMsg.setAlpha(1.0f);
                tvOffline.setVisibility(View.GONE);
                updateContestResult(id, textPass, value, type);
            } else {
                tvRewardMsg.setAlpha(0.5f);
                tvOffline.setVisibility(View.VISIBLE);
                ArrayList<OfflineRewardData> offlineRewardData = new ArrayList<OfflineRewardData>();
                OfflineRewardData offlineReward = new OfflineRewardData();
                offlineReward.set_id(0);
                offlineReward.setRewardID(id);
                offlineReward.setContestID(contestID);
                offlineReward.setText(textPass);
                offlineReward.setType(type);
                offlineReward.setValue(value);
                if (value.equalsIgnoreCase("0")) {
                    offlineReward.setIs_true("0");
                } else {
                    offlineReward.setIs_true("1");
                }
                offlineRewardData.add(offlineReward);
                appDatabase.offlineRewardDataDao().insertAll(offlineRewardData);
            }

            rewardActivity.getRewardUpdate();
        }

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        ((View) sheetView.getParent()).setBackgroundColor(rewardActivity.getResources().getColor(android.R.color.transparent));
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.show();
    }

    String getSpinWheelMetaData(String id, String text, String status, String type) {
        String metaData = "";
        try {
            JSONObject obj = new JSONObject();
            if (status.equalsIgnoreCase("0")) {
                obj.put("is_true", "0");
            } else {
                obj.put("is_true", "1");
            }
            obj.put("value", status);
            obj.put("type", type);
            obj.put("text", text);
            obj.put("reward_id", id);
            obj.put("status", "0");
            metaData = obj.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return metaData;
    }

    void updateContestResult(String id, String text, String status, String type) {
        try {
            RetrofitClient.getInstance().getApi().updateContestResponse(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    SharedPreferencesUtils.getUserId(rewardActivity), contestID, "", getSpinWheelMetaData(id, text, status, type)).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        try {
                            String r = response.body().string();
                            if (!TextUtils.isEmpty(r)) {
                                JSONObject jsonObject = new JSONObject(r);
                                if (jsonObject != null && jsonObject.length() > 0) {
                                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                        JSONObject data = jsonObject.optJSONObject("data");
                                        if (data != null && data.length() > 0) {
                                            JSONObject resultData = data.optJSONObject("result");
                                            rewardId = resultData.optString("reward_id");
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void showSpinDateTimeDialog(String id, final String text) {
        final String[] sDay = {""}, sTime = {""};
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(rewardActivity);
        View sheetView = LayoutInflater.from(rewardActivity).inflate(R.layout.spin_wheel_date_time_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        ImageView ivClose = (ImageView) sheetView.findViewById(R.id.imgClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });

        TextView tvTitleMsg = (TextView) sheetView.findViewById(R.id.tvTitleMsg);
        TextView tvDarjKare = (TextView) sheetView.findViewById(R.id.tvDarjKare);

        List<String> timeList = new ArrayList<String>(Arrays.asList(rewardActivity.getResources().getStringArray(R.array.times)));
        Spinner spinnerTime = (Spinner) sheetView.findViewById(R.id.spinnerTime);
        SpinnerHintArrayAdapter spinnerArrayAdapter = new SpinnerHintArrayAdapter(rewardActivity, R.layout.spinner_item_list, timeList);
        spinnerArrayAdapter.add(rewardActivity.getResources().getString(R.string.str_time_select));
        spinnerTime.setAdapter(spinnerArrayAdapter);
        spinnerTime.setSelection(spinnerArrayAdapter.getCount());
        spinnerTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                if (!item.equalsIgnoreCase(rewardActivity.getResources().getString(R.string.str_time_select))) {
                    sTime[0] = item;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        List<String> dayList = new ArrayList<String>(Arrays.asList(rewardActivity.getResources().getStringArray(R.array.days)));
        Spinner spinnerDay = (Spinner) sheetView.findViewById(R.id.spinnerDay);
        SpinnerHintArrayAdapter spinnerHintArrayAdapter = new SpinnerHintArrayAdapter(rewardActivity, R.layout.spinner_item_list, dayList);
        spinnerHintArrayAdapter.add(rewardActivity.getResources().getString(R.string.str_day_select));
        spinnerDay.setAdapter(spinnerHintArrayAdapter);
        spinnerDay.setSelection(spinnerHintArrayAdapter.getCount());
        spinnerDay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                if (!item.equalsIgnoreCase(rewardActivity.getResources().getString(R.string.str_day_select))) {
                    sDay[0] = item;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        tvDarjKare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(sDay[0])) {
                    if (!TextUtils.isEmpty(sTime[0])) {
                        updateRewardStatus(id, sTime[0], sDay[0]);
                        mBottomSheetDialog.dismiss();
                    } else {
                        Toast.makeText(rewardActivity, rewardActivity.getResources().getString(R.string.str_please_select_time), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(rewardActivity, rewardActivity.getResources().getString(R.string.str_please_select_day), Toast.LENGTH_SHORT).show();
                }
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        ((View) sheetView.getParent()).setBackgroundColor(rewardActivity.getResources().getColor(android.R.color.transparent));
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();
    }

    String getJackpotData(String time, String weekday) {
        String metaData = "";
        try {
            JSONObject obj = new JSONObject();
            obj.put("timeslot", time);
            obj.put("weekday", weekday);
            metaData = obj.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return metaData;
    }

    void updateRewardStatus(String id, String time, String weekday) {
        try {
            RetrofitClient.getInstance().getApi().updateRewardStatus(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    SharedPreferencesUtils.getUserId(rewardActivity), rewardId, "1", getJackpotData(time, weekday)).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        openThankYou();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void openThankYou() {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(rewardActivity);
        View view = LayoutInflater.from(rewardActivity).inflate(R.layout.reward_thank_you_dialog, null);
        bottomSheetDialog.setContentView(view);

        rewardActivity.getRewardUpdate();

        ImageView ivBanner = view.findViewById(R.id.ivBanner);
        ivBanner.setImageResource(R.mipmap.expert_speak);

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        AppCompatTextView txtTitle = view.findViewById(R.id.txtTitle);
        AppCompatTextView txtEnter = view.findViewById(R.id.txtEnter);
        txtEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(rewardActivity.getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

}
