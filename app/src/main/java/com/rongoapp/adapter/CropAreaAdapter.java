package com.rongoapp.adapter;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.rongoapp.R;
import com.rongoapp.activities.PostQuestionActivity;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.GlideApp;
import com.rongoapp.data.CropAreaFocusedData;

import java.util.List;

public class CropAreaAdapter extends RecyclerView.Adapter<CropAreaAdapter.ViewHolder> {

    List<CropAreaFocusedData> listdata;
    PostQuestionActivity postQuestionActivity;
    CropAreaFocusedData selectedCropData;
    List<String> fromSdcard;
    boolean isSet = false;

    public CropAreaAdapter(PostQuestionActivity postQuestionActivity, List<CropAreaFocusedData> listdata, CropAreaFocusedData selectedCropData, List<String> fromSdcard) {
        this.listdata = listdata;
        this.postQuestionActivity = postQuestionActivity;
        this.selectedCropData = selectedCropData;
        this.fromSdcard = fromSdcard;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_fasal_types, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.txtName.setText(listdata.get(position).getText());

        GlideApp.with(holder.itemView.getContext()).load(listdata.get(position).getImage())
                .apply(Functions.getCropAreaPlaceholder(position))
                .error(Functions.getCropAreaPlaceholderDrawble(position, postQuestionActivity)).into(holder.imgFasal);

        if (selectedCropData != null) {
            if (listdata.get(position).equals(selectedCropData)) {
                holder.txtName.setTextColor(ContextCompat.getColor(postQuestionActivity, R.color.bunai_yes));
                holder.imgFasal.setBorderColor(ContextCompat.getColor(postQuestionActivity, R.color.bunai_yes));
            } else {
                holder.txtName.setTextColor(ContextCompat.getColor(postQuestionActivity, R.color.ekad));
                holder.imgFasal.setBorderColor(ContextCompat.getColor(postQuestionActivity, R.color.grey_300));
            }
        } else {
            holder.txtName.setTextColor(ContextCompat.getColor(postQuestionActivity, R.color.ekad));
            holder.imgFasal.setBorderColor(ContextCompat.getColor(postQuestionActivity, R.color.grey_300));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelectedData(listdata.get(position), position, true);
            }
        });

        if (!isSet) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Write whatever to want to do after delay specified (1 sec)
                    if (position == (listdata.size() - 1)) {
                        setSelectedData(listdata.get(0), 0, false);
                        isSet = true;
                    }
                }
            }, 500);
        }
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtName;
        RoundedImageView imgFasal;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txtName = (TextView) itemView.findViewById(R.id.txtName);
            this.imgFasal = itemView.findViewById(R.id.imgFasal);
        }
    }

    public void setSelectedData(CropAreaFocusedData selectedData, int pos, boolean isOpen) {
        selectedCropData = selectedData;
        postQuestionActivity.getSelectedArea(selectedCropData, pos, isOpen);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
