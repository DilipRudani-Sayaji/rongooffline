package com.rongoapp.adapter;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.rongoapp.R;
import com.rongoapp.activities.HomeActivity;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.fragments.PhasalFragment;

import java.util.List;

public class WelcomeAdapter extends RecyclerView.Adapter<WelcomeAdapter.ViewHolder> {

    HomeActivity homeActivity;
    PhasalFragment phasalFragment;
    List<String> listdata;
    RecyclerView recyclerView;
    Dialog dialog;
    SharedPreferences sharedPreferences;

    public WelcomeAdapter(HomeActivity homeActivity, List<String> listdata, RecyclerView recyclerView, Dialog dialog, PhasalFragment phasalFragment) {
        this.listdata = listdata;
        this.recyclerView = recyclerView;
        this.dialog = dialog;
        this.phasalFragment = phasalFragment;
        this.homeActivity = homeActivity;
        sharedPreferences = homeActivity.getSharedPreferences(Constants.MediaPrefs, 0);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.welcome_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
//        holder.textView.setText(Functions.getConvertHtml(listdata.get(position)));
        if (position == 0) {
            holder.llFirstView.setVisibility(View.VISIBLE);
            holder.llSecondView.setVisibility(View.GONE);

            holder.tvSelect2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    recyclerView.scrollToPosition(1);
                }
            });

            holder.tvNextBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    recyclerView.scrollToPosition(1);
                }
            });
        } else if (position == 1) {
            holder.llFirstView.setVisibility(View.GONE);
            holder.llSecondView.setVisibility(View.VISIBLE);
            holder.tvSelect2.performLongClick();

            holder.tvShareReward.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));
            holder.tvExpertReward.setText(RewardPoints.getPostQuestionRewardPoint(sharedPreferences));
            holder.tvPhotoReward.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences));
            holder.tvQuizReward.setText(RewardPoints.getDailyQuizRewardPoint(sharedPreferences));

            holder.tvSelect3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    recyclerView.scrollToPosition(0);
                }
            });

            holder.tvStartBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        dialog.dismiss();

                        Intent intent = new Intent();
                        intent.setAction(Constants.BROADCAST_RECEIVER.UPDATE_TOOLTIP_VIEW);
                        RongoApp.getInstance().sendBroadcast(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            recyclerView.scrollToPosition(0);
        }
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvSelect1, tvSelect2, tvSelect3, tvSelect4, tvStartBtn, tvNextBtn, tvShareReward, tvExpertReward, tvPhotoReward, tvQuizReward;
        LinearLayout llFirstView, llSecondView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.llFirstView = (LinearLayout) itemView.findViewById(R.id.llFirstView);
            this.llSecondView = (LinearLayout) itemView.findViewById(R.id.llSecondView);

            this.tvSelect1 = (TextView) itemView.findViewById(R.id.tvSelect1);
            this.tvSelect2 = (TextView) itemView.findViewById(R.id.tvSelect2);
            this.tvSelect3 = (TextView) itemView.findViewById(R.id.tvSelect3);
            this.tvSelect4 = (TextView) itemView.findViewById(R.id.tvSelect4);

            this.tvShareReward = (TextView) itemView.findViewById(R.id.tvShareReward);
            this.tvExpertReward = (TextView) itemView.findViewById(R.id.tvExpertReward);
            this.tvPhotoReward = (TextView) itemView.findViewById(R.id.tvPhotoReward);
            this.tvQuizReward = (TextView) itemView.findViewById(R.id.tvQuizReward);

            this.tvNextBtn = (TextView) itemView.findViewById(R.id.tvNextBtn);
            this.tvStartBtn = (TextView) itemView.findViewById(R.id.tvStartBtn);
        }
    }

}
