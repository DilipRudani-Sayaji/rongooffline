package com.rongoapp.adapter;

import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.text.HtmlCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.rongoapp.R;
import com.rongoapp.activities.HomeActivity;
import com.rongoapp.audioPlayerTTS.TTS;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.GlideApp;
import com.rongoapp.controller.ImageZoomDialog;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.utilities.SeeMoreTextView;

import java.util.List;

public class MyPagerAdapter extends RecyclerView.Adapter<MyPagerAdapter.ViewHolder> {

    HomeActivity homeActivity;
    List<String> generalAdvisory, advisoryImages;
    String image_path = "";
    long mLastLongClick = 0;
    TTS.AudioPlayerListener audioPlayerListener;

    public MyPagerAdapter(HomeActivity context, List<String> generalAdvisory, List<String> advisoryImages, String image_path,
                          TTS.AudioPlayerListener audioPlayerListener) {
        this.homeActivity = context;
        this.generalAdvisory = generalAdvisory;
        this.advisoryImages = advisoryImages;
        this.image_path = image_path;
        this.audioPlayerListener = audioPlayerListener;
    }

    @Override
    public MyPagerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_general_salah, parent, false);
        MyPagerAdapter.ViewHolder viewHolder = new MyPagerAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyPagerAdapter.ViewHolder holder, int position) {
//        holder.textView.setText(Functions.getConvertHtml(generalAdvisory.get(position)));
        try {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            homeActivity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int width = displayMetrics.widthPixels;
            if (generalAdvisory.size() > 1) {
                holder.rootItem.setLayoutParams(new FrameLayout.LayoutParams((width - 280), FrameLayout.LayoutParams.WRAP_CONTENT));
            }

            holder.txtDesc.setContent(homeActivity, "" + HtmlCompat.fromHtml(generalAdvisory.get(position), HtmlCompat.FROM_HTML_MODE_LEGACY));

            if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                    holder.imageCover.setImageDrawable(Functions.getAssetsCropImage(homeActivity, Constants.ASSETS_ADVISORY_CROP_IMAGE_PATH +
                            RongoApp.getSelectedCrop() + "/" + advisoryImages.get(position)));
                } else {
                    Drawable drawable = Functions.getAssetsCropImage(homeActivity, Constants.ASSETS_ADVISORY_CROP_IMAGE_PATH +
                            RongoApp.getSelectedCrop() + "/" + advisoryImages.get(position));

                    GlideApp.with(homeActivity).load(image_path + "/" + advisoryImages.get(position))
                            .apply(Functions.getDrawablePlaceholder(drawable)).into(holder.imageCover);
                }
            } else {
                holder.imageCover.setImageDrawable(Functions.getAssetsCropImage(homeActivity, Constants.ASSETS_ADVISORY_CROP_IMAGE_PATH +
                        RongoApp.getSelectedCrop() + "/" + advisoryImages.get(position)));
            }

            holder.imageCover.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                        ImageZoomDialog.openImages(homeActivity, image_path + "/" + advisoryImages.get(position));
                    }
                }
            });

            holder.imgPlaySalah.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    if (homeActivity.tts != null) {
                        homeActivity.tts.speak(Functions.fromHtml(generalAdvisory.get(position)).toString(),
                                holder.imgPlaySalah, holder.progressBar, audioPlayerListener);
                    } else {
                        HomeActivity.setTextToSpeechInit();
                        homeActivity.tts.speak(Functions.fromHtml(generalAdvisory.get(position)).toString(),
                                holder.imgPlaySalah, holder.progressBar, audioPlayerListener);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            holder.imageCover.setImageResource(R.drawable.placeholderbg);
            holder.txtDesc.setContent(homeActivity, "" + HtmlCompat.fromHtml(generalAdvisory.get(position), HtmlCompat.FROM_HTML_MODE_LEGACY));
        }
    }

    @Override
    public int getItemCount() {
        return generalAdvisory.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        SeeMoreTextView txtDesc;
        AppCompatImageView imageCover;
        ImageView imgPlaySalah;
        ProgressBar progressBar;
        FrameLayout rootItem;

        public ViewHolder(View itemView) {
            super(itemView);
            this.rootItem = (FrameLayout) itemView.findViewById(R.id.rootItem);
            this.txtDesc = itemView.findViewById(R.id.txtDesc);
            this.imageCover = itemView.findViewById(R.id.image);
            this.imgPlaySalah = itemView.findViewById(R.id.imgPlay);
            this.progressBar = itemView.findViewById(R.id.myPb);
        }
    }

}
