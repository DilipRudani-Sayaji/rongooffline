package com.rongoapp.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.rongoapp.R;
import com.rongoapp.activities.SavdhaniDetailsActivity;
import com.rongoapp.data.PestData;

import java.util.List;

public class PestAdapter extends RecyclerView.Adapter<PestAdapter.ViewHolder> {

    List<PestData> pestData;
    BottomSheetDialog bottomSheetDialog;

    public PestAdapter(List<PestData> listdata, BottomSheetDialog bottomSheetDialog) {
        this.pestData = listdata;
        this.bottomSheetDialog = bottomSheetDialog;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_diases, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.textView.setText(pestData.get(position).getName());

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bottomSheetDialog != null) {
                    bottomSheetDialog.dismiss();
                }
                Intent i = new Intent(holder.itemView.getContext(), SavdhaniDetailsActivity.class);
                i.putExtra("pestdata", pestData.get(position));
                i.putExtra("keyword_id", pestData.get(position).getKeyword_id());
                holder.itemView.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pestData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textView = (TextView) itemView.findViewById(R.id.txtNames);
        }
    }

}
