package com.rongoapp.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.rongoapp.R;
import com.rongoapp.activities.PostQuestionActivity;
import com.rongoapp.controller.Constants;
import com.rongoapp.data.ProblemRelatedData;

import java.util.List;

public class CropProblemPostQuesAdapter extends RecyclerView.Adapter<CropProblemPostQuesAdapter.ViewHolder> {

    Activity homeActivity;
    List<ProblemRelatedData> listdata;
    BottomSheetDialog bottomSheetDialog;

    public CropProblemPostQuesAdapter(Activity homeActivity, List<ProblemRelatedData> listdata, BottomSheetDialog bottomSheetDialog) {
        this.listdata = listdata;
        this.homeActivity = homeActivity;
        this.bottomSheetDialog = bottomSheetDialog;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_fasal_problem_post_ques, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (position == 0) {
            holder.txtName.setBackground(homeActivity.getResources().getDrawable(R.drawable.rounded_box_orange_white));
        }
        holder.txtName.setText(listdata.get(position).getValue());
        holder.txtName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                Intent i = new Intent(homeActivity, PostQuestionActivity.class);
                i.putExtra("keyword_id", "0");
                i.putExtra("type", "2");
                i.putExtra("problem_related", listdata.get(position).getKey());
                homeActivity.startActivityForResult(i, Constants.POST_QUESTION_CODE);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtName;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txtName = (TextView) itemView.findViewById(R.id.txtName);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
