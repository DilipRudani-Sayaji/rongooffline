package com.rongoapp.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.viewpager.widget.PagerAdapter;

import com.rongoapp.R;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.GlideApp;
import com.rongoapp.controller.ImageZoomDialog;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.utilities.InternetConnection;

import java.io.File;
import java.util.List;

public class ImagesSavdhaniAdapter extends PagerAdapter {

    Activity context;
    List<String> generalAdvisory;
    String image_path = "", screenName = "0";
    long mLastLongClick = 0;

    public ImagesSavdhaniAdapter(Activity context, List<String> generalAdvisory, String image_path, String screenName) {
        this.context = context;
        this.generalAdvisory = generalAdvisory;
        this.image_path = image_path;
        this.screenName = screenName;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_item_image, null);
        try {
            AppCompatImageView imageCover = view.findViewById(R.id.image);

            String path = generalAdvisory.get(position);
            if (path.contains("storage")) {
                File imgFile = new File(path);
                if (imgFile.exists()) {
                    Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                    imageCover.setImageBitmap(myBitmap);
                }
            } else {
                if (TextUtils.isEmpty(image_path)) {
                    image_path = Constants.KEYWORD_IMAGE_PATH + RongoApp.getSelectedCrop() + "/";
                }

//                Log.v("Keyword-image", image_path + "" + generalAdvisory.get(position));
                if (screenName.equalsIgnoreCase("0")) {
                    GlideApp.with(context).load(image_path + "/" + generalAdvisory.get(position))
                            .error(R.drawable.placeholderbg).placeholder(R.drawable.placeholderbg).into(imageCover);
                } else {
                    Drawable drawable = Functions.getAssetsKeywordImage(context, Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH +
                            RongoApp.getSelectedCrop() + "/" + generalAdvisory.get(0));
                    GlideApp.with(context).load(image_path + "/" + generalAdvisory.get(position))
                            .apply(Functions.getDrawablePlaceholder(drawable)).into(imageCover);
                }
            }

            imageCover.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    if (InternetConnection.checkConnectionForFasl(context)) {
                        ImageZoomDialog.openImages(context, image_path + "/" + generalAdvisory.get(position));
                    }
                }
            });

            container.addView(view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return generalAdvisory.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

}
