package com.rongoapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.rongoapp.R;

import java.util.ArrayList;
import java.util.List;

public class SpinnerHintArrayAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final int mResource;
    private List<String> items;

    public SpinnerHintArrayAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List objects) {
        super(context, resource, 0, objects);
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = new ArrayList<String>();
        items = objects;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent) {
        View view = mInflater.inflate(mResource, parent, false);

        TextView tvExpLevel = (TextView) view.findViewById(R.id.tvExpLevel);
        tvExpLevel.setText(items.get(position));

        return view;
    }

    @Override
    public int getCount() {
        int count = super.getCount();
        return count > 0 ? count - 1 : count;
    }

}