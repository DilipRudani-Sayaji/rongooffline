package com.rongoapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.rongoapp.R;
import com.rongoapp.fragments.PhasalFragment;

import java.util.ArrayList;
import java.util.List;

public class CropQuesOptionsAdapter extends RecyclerView.Adapter<CropQuesOptionsAdapter.ViewHolder> {

    List<String> listdata;
    PhasalFragment phasalFragment;
    String field_type = "", selectedStr = "";
    List<String> selectedStringsList = new ArrayList<>();
    List<String> selectedStringsListForNames;

    public CropQuesOptionsAdapter(List<String> listdata, PhasalFragment phasalFragment, String fieldType, String selectedStr, List<String> selectedStringsList, List<String> selectedStringsListForNames) {
        this.listdata = listdata;
        this.phasalFragment = phasalFragment;
        this.field_type = fieldType;
        this.selectedStringsList = selectedStringsList;
        this.selectedStr = selectedStr;
        this.selectedStringsListForNames = selectedStringsListForNames;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_cropques_selection, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.textView.setText(listdata.get(position));

        if (field_type.equalsIgnoreCase("select")) {
            if (listdata.get(position).equalsIgnoreCase(selectedStr)) {
                holder.imgSelection.setImageResource(R.mipmap.uria_selected);
            } else {
                holder.imgSelection.setImageResource(R.mipmap.uria_unselected);
            }

        } else if (field_type.equalsIgnoreCase("select_multiple")) {
            if (selectedStringsList.contains(position + "")) {
                holder.imgSelection.setImageResource(R.mipmap.uria_selected);
            } else {
                holder.imgSelection.setImageResource(R.mipmap.uria_unselected);
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (field_type.equalsIgnoreCase("select")) {
                    singleSelection(listdata.get(position));
                } else if (field_type.equalsIgnoreCase("select_multiple")) {
                    multipleSelection(listdata.get(position), position + "");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public AppCompatTextView textView;
        ImageView imgSelection;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textView = itemView.findViewById(R.id.checkbox);
            this.imgSelection = itemView.findViewById(R.id.imgSelection);
        }
    }

    public void singleSelection(String myStr) {
        selectedStr = myStr;
//        phasalFragment.selectedSingle(myStr);
        notifyDataSetChanged();
    }

    public void multipleSelection(String myStr, String position) {
        selectedStr = position;
        if (selectedStringsList.contains(selectedStr)) {
            selectedStringsList.remove(selectedStr);
        } else {
            selectedStringsList.add(selectedStr);
        }

        if (selectedStringsListForNames.contains(myStr)) {
            selectedStringsListForNames.remove(myStr);
        } else {
            selectedStringsListForNames.add(myStr);
        }

        phasalFragment.selectedMultiple(selectedStringsList, selectedStringsListForNames);
        notifyDataSetChanged();
    }

}
