package com.rongoapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.rongoapp.R;
import com.rongoapp.data.SeedTypeData;
import com.rongoapp.fragments.AdhikFragment;

import java.util.List;

public class SeedTypesAdapter extends RecyclerView.Adapter<SeedTypesAdapter.ViewHolder> {

    List<SeedTypeData> listdata;
    AdhikFragment phasalFragment;
    String seedType = "";

    public SeedTypesAdapter(List<SeedTypeData> listdata, AdhikFragment phasalFragment, String seedType) {
        this.listdata = listdata;
        this.phasalFragment = phasalFragment;
        this.seedType = seedType;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_seed_types, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.textView.setText(listdata.get(position).getValue());

        if (listdata.get(position).getKey().equalsIgnoreCase(seedType)) {
            holder.imgSelection.setImageResource(R.mipmap.uria_selected);
        } else {
            holder.imgSelection.setImageResource(R.mipmap.uria_unselected);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelection(listdata.get(position).getKey());
            }
        });
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        ImageView imgSelection;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textView = itemView.findViewById(R.id.txtSeedName);
            this.imgSelection = itemView.findViewById(R.id.imgSelection);
        }
    }

    public void setSelection(String s) {
        this.seedType = s;
        phasalFragment.seedType(seedType);
        notifyDataSetChanged();
    }

}
