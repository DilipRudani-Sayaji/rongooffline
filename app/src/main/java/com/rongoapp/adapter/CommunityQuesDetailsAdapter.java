package com.rongoapp.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.SystemClock;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.rongoapp.R;
import com.rongoapp.activities.CommunityDetailsActivity;
import com.rongoapp.activities.SavdhaniDetailsActivity;
import com.rongoapp.controller.AudioPlayerTask;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.FetchJsonFromAssetsFile;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.ExpertResponceModel;
import com.rongoapp.data.OfflineLikeData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommunityQuesDetailsAdapter extends RecyclerView.Adapter<CommunityQuesDetailsAdapter.ViewHolder> {

    Activity homeActivity;
    AppDatabase appDatabase;
    SharedPreferences sharedPreferences;
    long mLastLongClick = 0;
    List<ExpertResponceModel> expertResponceModels;
    AudioPlayerTask audioPlayerTask;
    String audioPath;
    int optionType;

    public CommunityQuesDetailsAdapter(Activity homeActivity, String audioPath, List<ExpertResponceModel> expertResponceModels) {
        this.expertResponceModels = expertResponceModels;
        this.homeActivity = homeActivity;
        this.audioPath = audioPath;
        sharedPreferences = homeActivity.getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(homeActivity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            holder.ivIsReport.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openReport(expertResponceModels.get(position).getComment_id());
                }
            });

            if (!TextUtils.isEmpty(expertResponceModels.get(position).getVote_type())) {
                if (expertResponceModels.get(position).getVote_type().equalsIgnoreCase("1")) {
                    holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.like, 0, 0, 0);
                } else {
                    holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.unlike, 0, 0, 0);
                }
            } else {
                holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.unlike, 0, 0, 0);
            }

            holder.tvCommentTxt.setText(expertResponceModels.get(position).getComment_text());
            holder.tvLike.setText(expertResponceModels.get(position).getLikes() + " " + holder.itemView.getContext().getResources().getString(R.string.str_like_txt));

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            Calendar cal = Calendar.getInstance();
            cal.setTime(sdf.parse(expertResponceModels.get(position).getCreated_on()));
            holder.txtDate.setText(Functions.setTimeAgoMyTopic(holder.itemView.getContext(), cal.getTimeInMillis()));

            if (expertResponceModels.get(position).getType().equalsIgnoreCase("0")) {
                holder.tvPostedName.setText(holder.itemView.getContext().getResources().getString(R.string.str_expert_sujav));
                holder.tvPostedName.setTextColor(holder.itemView.getContext().getResources().getColor(R.color.chat_text));
                holder.tvPostedName.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.ee, 0, 0, 0);
                holder.llChatBox.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.left_corner_select));
            } else {
                holder.tvPostedName.setText(expertResponceModels.get(position).getPosted_user_name());
                holder.tvPostedName.setTextColor(holder.itemView.getContext().getResources().getColor(R.color.ekad));
                holder.tvPostedName.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.c_user, 0, 0, 0);
                holder.llChatBox.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.left_corner_unselect));
            }

            if (expertResponceModels.get(position).getIs_expert().equalsIgnoreCase("1")) {
                holder.ivIsExpert.setVisibility(View.VISIBLE);
            } else {
                holder.ivIsExpert.setVisibility(View.GONE);
            }

            holder.ivVoiceRecorder.setVisibility(View.GONE);
            if (expertResponceModels.get(position).getRecording_key() != null && !TextUtils.isEmpty(expertResponceModels.get(position).getRecording_key()) && !expertResponceModels.get(position).getRecording_key().equalsIgnoreCase("null")) {
                if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                    JSONArray jArray = new JSONArray(expertResponceModels.get(position).getRecording_key().toString());
                    if (jArray != null && jArray.length() > 0) {
                        holder.ivVoiceRecorder.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                if (!expertResponceModels.get(position).getRecording_local_path().equalsIgnoreCase("null") && !TextUtils.isEmpty(expertResponceModels.get(position).getRecording_local_path())) {
                    String audio_Path = expertResponceModels.get(position).getRecording_local_path();
                    File file = new File(Functions.replaceAudioLinkString(audio_Path));
                    if (file.exists()) {
                        holder.ivVoiceRecorder.setVisibility(View.VISIBLE);
                    }
                }
            }

            audioPlayerTask = new AudioPlayerTask(homeActivity, holder.ivVoiceRecorder);
            holder.ivVoiceRecorder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    audioPlayerTask.onPlayMedia(getAudioLink(position));
                }
            });

            holder.tvLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    if (!TextUtils.isEmpty(expertResponceModels.get(position).getVote_type())) {
                        if (expertResponceModels.get(position).getVote_type().equalsIgnoreCase("1")) {
                            holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.like, 0, 0, 0);
                        } else {
                            holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.unlike, 0, 0, 0);
                            likeCommunityPost(position);
                        }
                    } else {
                        holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.unlike, 0, 0, 0);
                        likeCommunityPost(position);
                    }
                }
            });

            List<String> keywordList = new ArrayList<String>(Arrays.asList(expertResponceModels.get(position).getKeyword_advisory().split(",")));
            Log.e("++keywords++", "" + keywordList);
            for (int i = 0; i < keywordList.size(); i++) {
                String keyWord = FetchJsonFromAssetsFile.loadAdvisoryKeywordJSONFromAsset(homeActivity, keywordList.get(i));
                if (!TextUtils.isEmpty(keyWord)) {
                    holder.tvInfo.setVisibility(View.VISIBLE);
                    JSONObject jsonObject = new JSONObject(keyWord);
                    if (jsonObject != null && jsonObject.length() > 0) {
                        String name = jsonObject.optString("name");

                        LinearLayout linearLayout = new LinearLayout(homeActivity);
                        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                        linearLayout.setOrientation(LinearLayout.HORIZONTAL);

                        TextView view = new TextView(homeActivity);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        params.setMargins(10, 2, 20, 4);
                        view.setLayoutParams(params);
                        view.setTag(keywordList.get(i));
                        view.setText(name);

                        String originalText = (String) view.getText();
                        int startPosition = 0, endPosition = name.length();
                        SpannableString spannableStr = new SpannableString(originalText);
                        UnderlineSpan underlineSpan = new UnderlineSpan();
                        spannableStr.setSpan(underlineSpan, startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        ForegroundColorSpan backgroundColorSpan = new ForegroundColorSpan(Color.rgb(255, 69, 0));
                        spannableStr.setSpan(backgroundColorSpan, startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                        spannableStr.setSpan(new RelativeSizeSpan(1f), startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

                        view.setText(spannableStr);
                        view.setOnClickListener(clickInLinearLayout());

                        ImageView imageView = new ImageView(homeActivity);
                        LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        params1.setMargins(10, 10, 10, 0);
                        imageView.setImageResource(R.mipmap.o_bact);
                        imageView.setLayoutParams(params1);

                        linearLayout.addView(imageView);
                        linearLayout.addView(view);
                        holder.llKeyWordView.addView(linearLayout);
                    }
                } else {
                    holder.tvInfo.setVisibility(View.GONE);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private View.OnClickListener clickInLinearLayout() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.e("pos-", "" + v.getTag().toString());
                Intent i = new Intent(homeActivity, SavdhaniDetailsActivity.class);
                i.putExtra("keyword_id", "" + v.getTag().toString());
                homeActivity.startActivity(i);
            }
        };
    }

    @Override
    public int getItemCount() {
        return expertResponceModels.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvCommentTxt, tvPostedName, txtDate, tvLike, tvInfo;
        public LinearLayout llChatBox, llKeyWordView;
        public View view;
        public ImageView ivIsExpert, ivVoiceRecorder, ivIsReport;

        public ViewHolder(View itemView) {
            super(itemView);
            this.ivIsReport = (ImageView) itemView.findViewById(R.id.ivIsReport);
            this.ivVoiceRecorder = (ImageView) itemView.findViewById(R.id.ivVoiceRecorder);
            this.ivIsExpert = (ImageView) itemView.findViewById(R.id.ivIsExpert);
            this.tvCommentTxt = (TextView) itemView.findViewById(R.id.tvCommentTxt);
            this.tvInfo = (TextView) itemView.findViewById(R.id.tvInfo);
            this.tvPostedName = (TextView) itemView.findViewById(R.id.tvPostedName);
            this.txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            this.tvLike = (TextView) itemView.findViewById(R.id.tvLike);
            this.llKeyWordView = (LinearLayout) itemView.findViewById(R.id.llKeyWordView);
            this.llChatBox = (LinearLayout) itemView.findViewById(R.id.llChatBox);
        }
    }

    void likeCommunityPost(int pos) {
        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
            RetrofitClient.getInstance().getApi().likeCommunityPost(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    SharedPreferencesUtils.getUserId(homeActivity), expertResponceModels.get(pos).getPost_id(),
                    expertResponceModels.get(pos).getComment_id(), "1").enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        setLikeEvent(false, pos);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        } else {
            setLikeEvent(true, pos);
        }
    }

    void setLikeEvent(boolean isInsert, int pos) {
        try {
            int count = Integer.parseInt(expertResponceModels.get(pos).getLikes());
            expertResponceModels.get(pos).setLikes(String.valueOf(count + 1));
            expertResponceModels.get(pos).setVote_type("1");
            notifyDataSetChanged();

            if (isInsert) {
                final List<OfflineLikeData> offlineLikeData = new ArrayList<OfflineLikeData>();
                offlineLikeData.add(new OfflineLikeData(new Random().nextInt(), expertResponceModels.get(pos).getPost_id(), "1",
                        expertResponceModels.get(pos).getComment_id(), Functions.getCurrentDateTime()));
                appDatabase.offlineLikeDao().insertAll(offlineLikeData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String getAudioLink(int position) {
        String audio_Path = "";
        try {
            if (!TextUtils.isEmpty(expertResponceModels.get(position).getRecording_key()) && !expertResponceModels.get(position).getRecording_key().equalsIgnoreCase("null")) {
                JSONArray jArray = new JSONArray(expertResponceModels.get(position).getRecording_key().toString());
                if (jArray != null && jArray.length() > 0) {
                    audio_Path = audioPath + jArray.opt(0).toString();
                }
            } else {
                if (!expertResponceModels.get(position).getRecording_local_path().equalsIgnoreCase("null") && !TextUtils.isEmpty(expertResponceModels.get(position).getRecording_local_path())) {
                    audio_Path = expertResponceModels.get(position).getRecording_local_path();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return audio_Path;
    }


    void openReport(String id) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_report_option, null);
        bottomSheetDialog.setContentView(view);

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        RelativeLayout rlReportView = view.findViewById(R.id.rlReportView);
        rlReportView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
                reportUpdateDialog(id);
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(homeActivity.getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    void reportUpdateDialog(String id) {
        try {
            Dialog dialog = new Dialog(homeActivity, R.style.NewDialog);
            dialog.setContentView(R.layout.report_update_dialog);
            dialog.show();

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);

            ImageView imgClose = dialog.findViewById(R.id.imgClose);
            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            RecyclerView rcvReport = dialog.findViewById(R.id.rcvReport);
            ArrayList<String> typeArray = new ArrayList<>();
            try {
                JSONArray jsonArray = new JSONArray(sharedPreferences.getString(Constants.SharedPreferences_ReportComment, null));
                if (jsonArray != null && jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        typeArray.add("" + jsonArray.get(i));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            rcvReport.setLayoutManager(new LinearLayoutManager(homeActivity, RecyclerView.VERTICAL, false));
            rcvReport.setAdapter(new TypesAdapter(typeArray));

            AppCompatTextView txtDarjKare = dialog.findViewById(R.id.txtDarjKare);
            txtDarjKare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!TextUtils.isEmpty(String.valueOf(optionType))) {
                        dialog.dismiss();
                        Functions.setCustomBlackToastBar(homeActivity, homeActivity.getResources().getString(R.string.str_report_darj));
                        RongoApp.reportUserContent(String.valueOf(optionType), Constants.POST, id);
                    } else {
                        Functions.setCustomBlackToastBar(homeActivity, homeActivity.getResources().getString(R.string.str_option_select));
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    class TypesAdapter extends RecyclerView.Adapter<TypesAdapter.ViewHolder> {
        List<String> listdata;

        public TypesAdapter(List<String> listdata) {
            this.listdata = listdata;
        }

        @Override
        public TypesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_option_type, parent, false);
            TypesAdapter.ViewHolder viewHolder = new TypesAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(TypesAdapter.ViewHolder holder, int position) {
            holder.textView.setText(listdata.get(position));

            if (position == optionType) {
                holder.imgSelection.setImageResource(R.mipmap.uria_selected);
            } else {
                holder.imgSelection.setImageResource(R.mipmap.uria_unselected);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    optionType = position;
                    notifyDataSetChanged();
                }
            });
        }

        @Override
        public int getItemCount() {
            return listdata.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView textView;
            ImageView imgSelection;

            public ViewHolder(View itemView) {
                super(itemView);
                this.textView = itemView.findViewById(R.id.txtSeedName);
                this.imgSelection = itemView.findViewById(R.id.imgSelection);
            }
        }
    }

}
