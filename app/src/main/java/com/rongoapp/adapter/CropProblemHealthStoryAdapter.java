package com.rongoapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.rongoapp.R;
import com.rongoapp.data.ProblemRelatedData;
import com.rongoapp.fragments.CropHealthFragment;

import java.util.List;

public class CropProblemHealthStoryAdapter extends RecyclerView.Adapter<CropProblemHealthStoryAdapter.ViewHolder> {

    List<ProblemRelatedData> listdata;
    CropHealthFragment cropHealthFragment;
    String selectedCrop;

    public CropProblemHealthStoryAdapter(CropHealthFragment cropHealthFragment, List<ProblemRelatedData> listdata, String selectedCrop) {
        this.listdata = listdata;
        this.cropHealthFragment = cropHealthFragment;
        this.selectedCrop = selectedCrop;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_fasal_problem, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.txtName.setText(listdata.get(position).getValue());
        holder.llProblem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelectedData(listdata.get(position).getKey());
            }
        });

        if (selectedCrop != null) {
            if (listdata.get(position).getKey().equals(selectedCrop)) {
                holder.llProblem.setBackground(ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.rect_option));
                holder.txtName.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.bunai_yes));
            } else {
                holder.llProblem.setBackground(ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.rounded_layout_unselect));
                holder.txtName.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.ekad));
            }
        } else {
            holder.llProblem.setBackground(ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.rounded_layout_unselect));
            holder.txtName.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.ekad));
        }

        if (listdata.get(position).getKey().equalsIgnoreCase("1")) {
            holder.ivPic.setImageResource(R.mipmap.p_rog);
        } else if (listdata.get(position).getKey().equalsIgnoreCase("2")) {
            holder.ivPic.setImageResource(R.mipmap.p_leaf);
        } else if (listdata.get(position).getKey().equalsIgnoreCase("3")) {
            holder.ivPic.setImageResource(R.mipmap.ladybug);
        } else if (listdata.get(position).getKey().equalsIgnoreCase("4")) {
            holder.ivPic.setImageResource(R.mipmap.p_weather);
        } else if (listdata.get(position).getKey().equalsIgnoreCase("5")) {
            holder.ivPic.setImageResource(R.mipmap.anya);
        }

    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtName;
        ImageView ivPic;
        LinearLayout llProblem;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txtName = (TextView) itemView.findViewById(R.id.txtName);
            this.ivPic = (ImageView) itemView.findViewById(R.id.ivPic);
            this.llProblem = (LinearLayout) itemView.findViewById(R.id.llProblem);
        }
    }

    public void setSelectedData(String selectedData) {
        selectedCrop = selectedData;
        cropHealthFragment.getSelectedProblem(selectedData);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
