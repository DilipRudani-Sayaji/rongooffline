package com.rongoapp.adapter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.text.HtmlCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.card.MaterialCardView;
import com.rongoapp.R;
import com.rongoapp.activities.CommunityDetailsActivity;
import com.rongoapp.activities.HomeActivity;
import com.rongoapp.controller.AudioPlayerTask;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.PostData;
import com.rongoapp.data.ProblemRelatedData;
import com.rongoapp.data.StateCityDataModel;
import com.rongoapp.data.SurveyQuestions;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.fragments.CommunityFragment;
import com.rongoapp.utilities.InternetConnection;

import org.json.JSONArray;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class CommunityQuestionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    CommunityFragment communityFragment;
    AppDatabase appDatabase;
    SharedPreferences sharedPreferences;
    HomeActivity homeActivity;

    long mLastLongClick = 0;
    int LIST_AD_DELTA = 3;
    private static final int CONTENT_TYPE = 0;
    private static final int AD_TYPE = 1;

    List<PostData> postData;
    String imagePath, audioPath;
    boolean isSurvey = false;

    AudioPlayerTask audioPlayerTask;

    public CommunityQuestionsAdapter(CommunityFragment communityFragment, HomeActivity homeActivity, List<PostData> postData, String imagePath, String audioPath) {
        this.homeActivity = homeActivity;
        this.imagePath = imagePath;
        this.audioPath = audioPath;
        this.postData = postData;
        this.communityFragment = communityFragment;
        appDatabase = AppDatabase.getDatabase(homeActivity);
        sharedPreferences = homeActivity.getSharedPreferences(Constants.MediaPrefs, 0);
        LIST_AD_DELTA = Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_survey_post_position, "3"));

        String surveyId = "1";
        if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_LAST_SURVEY_ID, "1"))) {
            surveyId = sharedPreferences.getString(Constants.SharedPreferences_LAST_SURVEY_ID, "1");
        }
        if (surveyId.equalsIgnoreCase("null") || surveyId == null) {
            surveyId = "1";
        }
        List<SurveyQuestions> surveyQuestions = appDatabase.surveyQuestionsDao().getNextQuestion(Integer.parseInt(surveyId));
        if (!surveyQuestions.isEmpty()) {
            if (sharedPreferences.getString(Constants.SharedPreferences_is_survey_visible, "1").equalsIgnoreCase("1")) {
                isSurvey = true;
            }
        }
    }

    public static class SurveyViewHolder extends RecyclerView.ViewHolder {
        MaterialCardView surveyCardView;
        TextView tvRewardNew;

        public SurveyViewHolder(@NonNull View itemView) {
            super(itemView);
            surveyCardView = itemView.findViewById(R.id.surveyCardView);
            tvRewardNew = itemView.findViewById(R.id.tvRewardNew);
        }
    }

    void showSurveyView(SurveyViewHolder holder, int position) {
        holder.tvRewardNew.setText(RewardPoints.getSurveyRewardPoint(sharedPreferences));
        holder.surveyCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                communityFragment.setSurveyDialog();
                isSurvey = false;
            }
        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == AD_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.community_survey_item, parent, false);
            return new SurveyViewHolder(view);
        } else {
            View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_salah, parent, false);
            return new ItemViewHolder(listItem);
        }
    }

    @Override
    public int getItemCount() {
//        int additionalContent = 0;
//        if (postData.size() > 0 && LIST_AD_DELTA > 0 && postData.size() > LIST_AD_DELTA) {
//            additionalContent = postData.size() / LIST_AD_DELTA;
//        }
//        return postData.size() + additionalContent;
        return postData.size();
    }

    @Override
    public int getItemViewType(int position) {
//        if (isSurvey) {
//            if (position == LIST_AD_DELTA)
//                return AD_TYPE;
//        }
        return CONTENT_TYPE;
    }

    protected int mLastPosition = -1;

    protected void setAnimation(View viewToAnimate, int position) {
        if (position > mLastPosition) {
            ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(505);
            viewToAnimate.startAnimation(anim);
            mLastPosition = position;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        setAnimation(viewHolder.itemView, position);
        if (viewHolder instanceof ItemViewHolder) {
            populateItemRows((ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof SurveyViewHolder) {
            showSurveyView((SurveyViewHolder) viewHolder, position);
        }
    }

    public void populateItemRows(ItemViewHolder holder, int position) {
        try {
            holder.tvUserLocation.setText("" + postData.get(position).getPosted_user_location());

            List<StateCityDataModel> parseStateCityListData = Functions.parseStateCityListData(sharedPreferences);
            for (int i = 0; i < parseStateCityListData.size(); i++) {
                if (postData.get(position).getPosted_user_location().equalsIgnoreCase(parseStateCityListData.get(i).getState_english())) {
                    holder.tvUserLocation.setText("" + parseStateCityListData.get(i).getState_hindi());
                }
            }

            if (postData.get(position).getPost_type().equalsIgnoreCase("3")) {
                holder.llUserDetail.setVisibility(View.GONE);
            } else {
                holder.llUserDetail.setVisibility(View.VISIBLE);
                holder.tvUserName.setText("" + postData.get(position).getPosted_user_name());
            }

            if (postData.get(position).getIs_expert().equalsIgnoreCase("1")) {
                holder.ivIsExpert.setVisibility(View.VISIBLE);
            } else {
                holder.ivIsExpert.setVisibility(View.GONE);
            }

            holder.tvShare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (Functions.doubleTapCheck(mLastLongClick)) {
                            return;
                        }
                        mLastLongClick = SystemClock.elapsedRealtime();

                        Functions.showProgress(homeActivity, holder.progressBar);
                        String test = homeActivity.getResources().getString(R.string.str_rongo_community) + postData.get(position).getDescription() + "</br> <br>" + postData.get(position).getShare_link() + "</br>";
                        String shareTxt = "" + HtmlCompat.fromHtml(test, HtmlCompat.FROM_HTML_MODE_LEGACY);

                        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                            if (holder.ivIcon.getVisibility() == View.VISIBLE) {
                                JSONArray jsonArray = new JSONArray(postData.get(position).getPicture_key());
                                if (jsonArray != null && jsonArray.length() > 0) {
                                    if (TextUtils.isEmpty(imagePath)) {
                                        imagePath = Constants.IMAGE_PATH_LINK;
                                    }

                                    Functions.hideProgress(homeActivity, holder.progressBar);
                                    Functions.shareCommunity(homeActivity, shareTxt, imagePath + jsonArray.opt(0).toString());
                                } else {
                                    Functions.hideProgress(homeActivity, holder.progressBar);
                                    Functions.shareCommunity(homeActivity, shareTxt, "");
                                }
                            } else {
                                Functions.hideProgress(homeActivity, holder.progressBar);
                                Functions.shareCommunity(homeActivity, shareTxt, "");
                            }
                        } else {
                            Functions.hideProgress(homeActivity, holder.progressBar);
                            Functions.shareCommunity(homeActivity, shareTxt, "");
                        }
                        Functions.hideProgress(homeActivity, holder.progressBar);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(homeActivity, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                        Functions.hideProgress(homeActivity, holder.progressBar);
                    }
                }
            });

            holder.tvLike.setText(postData.get(position).getLikes() + " " + holder.itemView.getContext().getResources().getString(R.string.str_like_txt));
            holder.tvView.setText(postData.get(position).getViews() + " " + holder.itemView.getContext().getResources().getString(R.string.str_view_txt));

            if (!TextUtils.isEmpty(postData.get(position).getVote_type())) {
                if (postData.get(position).getVote_type().equalsIgnoreCase("1")) {
                    holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.like, 0, 0, 0);
                } else {
                    holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.unlike, 0, 0, 0);
                }
            } else {
                holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.unlike, 0, 0, 0);
            }

            holder.tvLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    if (!TextUtils.isEmpty(postData.get(position).getVote_type())) {
                        if (postData.get(position).getVote_type().equalsIgnoreCase("1")) {
                            holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.like, 0, 0, 0);
                        } else {
                            holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.unlike, 0, 0, 0);
                            communityFragment.likeCommunityPost(position, postData.get(position).getPost_id());
                        }
                    } else {
                        holder.tvLike.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.unlike, 0, 0, 0);
                        communityFragment.likeCommunityPost(position, postData.get(position).getPost_id());
                    }
                }
            });


            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            Calendar cal = Calendar.getInstance();
            cal.setTime(sdf.parse(postData.get(position).getCreated_on()));
            holder.txtDate.setText(Functions.setTimeAgoMyTopic(holder.itemView.getContext(), cal.getTimeInMillis()));

            if (!TextUtils.isEmpty(postData.get(position).getDescription())) {
                holder.txtQuestionRelated.setVisibility(View.VISIBLE);
                holder.txtQuestionRelated.setText(HtmlCompat.fromHtml(postData.get(position).getDescription(), HtmlCompat.FROM_HTML_MODE_LEGACY));
            } else {
                holder.txtQuestionRelated.setVisibility(View.GONE);
            }

            String type = homeActivity.getResources().getString(R.string.str_other);
            List<ProblemRelatedData> problemRelatedData = SharedPreferencesUtils.getProblemRelated(holder.itemView.getContext(), Constants.SharedPreferences_OPTIONS);
            if (problemRelatedData != null && problemRelatedData.size() > 0) {
                for (int i = 0; i < problemRelatedData.size(); i++) {
                    if (postData.get(position).getProblem_related().equalsIgnoreCase(problemRelatedData.get(i).getKey())) {
                        if (postData.get(position).getProblem_related().equalsIgnoreCase("5") ||
                                postData.get(position).getProblem_related().equalsIgnoreCase("0")) {
                            type = problemRelatedData.get(i).getValue();
                        } else {
                            type = problemRelatedData.get(i).getValue() + " " + homeActivity.getResources().getString(R.string.str_samndhi);
                        }
                        break;
                    }
                }
            }
            holder.tvRog.setText(HtmlCompat.fromHtml("<B>" + type + "</B>", HtmlCompat.FROM_HTML_MODE_LEGACY));

            holder.tvCommentCount.setText("0 " + homeActivity.getResources().getString(R.string.str_comments_txt));
            holder.tvExpertSujav.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(postData.get(position).getComments())) {
                JSONArray jsonArray = new JSONArray(postData.get(position).getComments());
                if (jsonArray != null && jsonArray.length() > 0) {
                    holder.tvCommentCount.setText(jsonArray.length() + " " + homeActivity.getResources().getString(R.string.str_comments_txt));
                    for (int i = 0; i < jsonArray.length(); i++) {
                        if (jsonArray.optJSONObject(i).optString("type").equalsIgnoreCase("0")) {
                            holder.tvExpertSujav.setVisibility(View.VISIBLE);
                            break;
                        } else {
                            holder.tvExpertSujav.setVisibility(View.GONE);
                        }
                    }
                }
            }

            holder.tvCommentCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    communityFragment.openCommentDialog(position, postData.get(position).getPost_id(), postData.get(position).getComments());
                }
            });

            if (postData.get(position).getIs_commented().equalsIgnoreCase("1")) {
                holder.txtNew.setVisibility(View.VISIBLE);
            } else {
                holder.txtNew.setVisibility(View.GONE);
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    Intent intent = new Intent(holder.itemView.getContext(), CommunityDetailsActivity.class);
                    intent.putExtra("postData", postData.get(position));
                    intent.putExtra("imagePath", imagePath);
                    intent.putExtra("type", "0");
                    intent.putExtra("post_id", postData.get(position).getPost_id());
                    holder.itemView.getContext().startActivity(intent);
                }
            });

            holder.ivPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    audioPlayerTask = new AudioPlayerTask(homeActivity, holder.ivPlay);
                    audioPlayerTask.onPlayMedia(getAudioLink(position));
                }
            });

            holder.llAudioView.setVisibility(View.GONE);
            if (postData.get(position).getRecording_key() != null && !TextUtils.isEmpty(postData.get(position).getRecording_key()) && !postData.get(position).getRecording_key().equalsIgnoreCase("null")) {
                if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                    JSONArray jArray = new JSONArray(postData.get(position).getRecording_key().toString());
                    if (jArray != null && jArray.length() > 0) {
                        holder.llAudioView.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                if (postData.get(position).getRecording_local_path() != null && !postData.get(position).getRecording_local_path().equalsIgnoreCase("null") && !TextUtils.isEmpty(postData.get(position).getRecording_local_path())) {
                    String audio_Path = postData.get(position).getRecording_local_path();
                    File file = new File(Functions.replaceAudioLinkString(audio_Path));
                    if (file.exists()) {
                        holder.llAudioView.setVisibility(View.VISIBLE);
                    }
                }
            }

            try {
                String pictureFile = postData.get(position).getPicture_key();
                if (pictureFile.contains("storage")) {
                    List<String> myList = new ArrayList<String>(Arrays.asList(pictureFile.replace("[", "").replace("]", "").split(",")));
                    File imgFile = new File(myList.get(0));
                    if (imgFile.exists()) {
                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                        holder.ivIcon.setImageBitmap(myBitmap);
                    }
                } else {
                    JSONArray imageArray = new JSONArray(pictureFile);
                    if (imageArray != null && imageArray.length() > 0) {
                        holder.ivIcon.setVisibility(View.VISIBLE);
                        if (TextUtils.isEmpty(imagePath)) {
                            imagePath = Constants.IMAGE_PATH_LINK;
                        }
                        Functions.displayImage(imagePath + imageArray.opt(0).toString(), holder.ivIcon);
                    } else {
                        holder.ivIcon.setVisibility(View.GONE);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                holder.ivIcon.setImageResource(R.drawable.placeholderbg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder {
        public AppCompatTextView tvCommentCount, txtNew, txtQuestionRelated, txtDate, tvExpertSujav, tvView, tvLike, tvShare, tvRog, tvUserLocation, tvUserName;
        public ImageView ivIcon, ivPlay, ivIsExpert;
        public ProgressBar progress, progressBar;
        public LinearLayout llAudioView, llUserDetail;

        public ItemViewHolder(View itemView) {
            super(itemView);
            this.llUserDetail = itemView.findViewById(R.id.llUserDetail);
            this.ivIsExpert = itemView.findViewById(R.id.ivIsExpert);
            this.progressBar = itemView.findViewById(R.id.pb);
            this.progressBar.setVisibility(View.GONE);
            this.txtQuestionRelated = itemView.findViewById(R.id.txtQuestionRelated);
            this.txtDate = itemView.findViewById(R.id.txtDate);
            this.txtNew = itemView.findViewById(R.id.txtNew);
            this.tvCommentCount = itemView.findViewById(R.id.tvCommentCount);
            this.tvView = itemView.findViewById(R.id.tvView);
            this.tvLike = itemView.findViewById(R.id.tvLike);
            this.tvShare = itemView.findViewById(R.id.tvShare);
            this.tvRog = itemView.findViewById(R.id.tvRog);
            this.tvExpertSujav = itemView.findViewById(R.id.tvExpertSujav);
            this.tvUserLocation = itemView.findViewById(R.id.tvUserLocation);
            this.tvUserName = itemView.findViewById(R.id.tvUserName);

            this.ivIcon = itemView.findViewById(R.id.ivIcon);
            this.ivPlay = itemView.findViewById(R.id.ivPlay);

            this.progress = itemView.findViewById(R.id.progress);
            this.progress.setVisibility(View.GONE);
            this.llAudioView = itemView.findViewById(R.id.llAudioView);
        }
    }

    public String getAudioLink(int position) {
        String audio_Path = "";
        try {
            if (postData.get(position).getRecording_key() != null && !TextUtils.isEmpty(postData.get(position).getRecording_key()) &&
                    !postData.get(position).getRecording_key().equalsIgnoreCase("null")) {
                JSONArray jArray = new JSONArray(postData.get(position).getRecording_key().toString());
                if (jArray != null && jArray.length() > 0) {
                    audio_Path = audioPath + jArray.opt(0).toString();
                }
            } else {
                if (!postData.get(position).getRecording_local_path().equalsIgnoreCase("null") &&
                        !TextUtils.isEmpty(postData.get(position).getRecording_local_path())) {
                    audio_Path = postData.get(position).getRecording_local_path();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return audio_Path;
    }

}
