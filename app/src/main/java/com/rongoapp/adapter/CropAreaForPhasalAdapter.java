package com.rongoapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.rongoapp.R;
import com.rongoapp.controller.GlideApp;
import com.rongoapp.data.CropAreaFocusedData;
import com.rongoapp.fragments.PhasalFragment;
import com.rongoapp.controller.Functions;

import java.util.List;

public class CropAreaForPhasalAdapter extends RecyclerView.Adapter<CropAreaForPhasalAdapter.ViewHolder> {

    List<CropAreaFocusedData> listdata;
    PhasalFragment phasalFragment;
    CropAreaFocusedData selectedCropData;

    public CropAreaForPhasalAdapter(PhasalFragment phasalFragment, List<CropAreaFocusedData> listdata, CropAreaFocusedData selectedCropData) {
        this.listdata = listdata;
        this.phasalFragment = phasalFragment;
        this.selectedCropData = selectedCropData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_crop_pragati, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.txtName.setText(listdata.get(position).getText());

        GlideApp.with(holder.itemView.getContext()).load(listdata.get(position).getImage())
                .apply(Functions.getCropAreaPlaceholder(position)).into(holder.imgFasal);

        if (selectedCropData != null) {
            if (listdata.get(position).equals(selectedCropData)) {
                holder.txtName.setBackgroundColor(ContextCompat.getColor(phasalFragment.getContext(), R.color.seventy_black_opacity));
                holder.txtName.setTextColor(ContextCompat.getColor(phasalFragment.getContext(), R.color.white));
                holder.imgSelectedFasal.setVisibility(View.VISIBLE);
            } else {
                holder.txtName.setBackgroundColor(ContextCompat.getColor(phasalFragment.getContext(), R.color.upload_photo));
                holder.txtName.setTextColor(ContextCompat.getColor(phasalFragment.getContext(), android.R.color.black));
                holder.imgSelectedFasal.setVisibility(View.GONE);
            }
        } else {
            holder.txtName.setBackgroundColor(ContextCompat.getColor(phasalFragment.getContext(), R.color.upload_photo));
            holder.txtName.setTextColor(ContextCompat.getColor(phasalFragment.getContext(), android.R.color.black));
            holder.imgSelectedFasal.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSelectedData(listdata.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtName;
        ImageView imgSelectedFasal;
        RoundedImageView imgFasal;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txtName = (TextView) itemView.findViewById(R.id.txtName);
            this.imgSelectedFasal = itemView.findViewById(R.id.imgSelectedFasal);
            this.imgFasal = itemView.findViewById(R.id.imgFasal);
        }
    }

    public void setSelectedData(CropAreaFocusedData selectedData) {
        selectedCropData = selectedData;
        phasalFragment.getSelectedArea(selectedCropData);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
