package com.rongoapp.adapter;

import android.content.SharedPreferences;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.rongoapp.R;
import com.rongoapp.activities.RewardActivity;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.RewardData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.ProgressBarDialog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EarnedRewardAdapter extends RecyclerView.Adapter<EarnedRewardAdapter.ViewHolder> {

    RewardActivity homeActivity;
    List<RewardData> listdata;
    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    List<RewardData> pendingRewardArray = new ArrayList<>();

    public EarnedRewardAdapter(RewardActivity homeActivity, List<RewardData> listdata) {
        this.homeActivity = homeActivity;
        this.listdata = listdata;
        sharedPreferences = homeActivity.getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(homeActivity);
        pendingRewardArray = Functions.getAllRewardList(sharedPreferences);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.earned_reward_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.tvRewardTitle.setText(listdata.get(position).getRewardTitle());

        for (int i = 0; i < pendingRewardArray.size(); i++) {
            if (pendingRewardArray.get(i).getRewardId().equalsIgnoreCase(listdata.get(position).getRewardId())) {
                holder.tvRewardTitle.setText("" + pendingRewardArray.get(i).getRewardTitle());
            }
        }

        if (listdata.get(position).getType().equalsIgnoreCase("jackpot")) {
            holder.ivRewardIcon.setImageResource(R.mipmap.spin_icon_small);
        } else if (listdata.get(position).getType().equalsIgnoreCase("recharge_coupon")) {
            holder.ivRewardIcon.setImageResource(R.mipmap.recharge_small);
        } else if (listdata.get(position).getType().equalsIgnoreCase("expert_call")) {
            holder.ivRewardIcon.setImageResource(R.mipmap.e_call_small);
        } else if (listdata.get(position).getType().equalsIgnoreCase("combo")) {
            holder.ivRewardIcon.setImageResource(R.mipmap.combo_small);
        } else {
            holder.ivRewardIcon.setImageResource(R.mipmap.big_box);
        }

        holder.llRewardView.setAlpha(1f);
        if (listdata.get(position).getStatus().equalsIgnoreCase("0")) {
            holder.ivRewardStatus.setImageResource(R.mipmap.alert);
            holder.tvRewardStatus.setText(R.string.str_reward_prapt_kare);
            holder.tvRewardStatus.setTextColor(homeActivity.getResources().getColor(R.color.bunai_yes));
            holder.tvRewardStatus.setPaintFlags(holder.tvRewardStatus.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            holder.llRewardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listdata.get(position).getType().equalsIgnoreCase("jackpot")) {
                        showSpinDateTimeDialog(listdata.get(position).getRewardId(), listdata.get(position).getRewardTitle());
                    } else {
                        showDialog(listdata.get(position).getType(), listdata.get(position).getRewardTitle(), listdata.get(position).getRewardId());
                    }
                }
            });

        } else if (listdata.get(position).getStatus().equalsIgnoreCase("1")) {
            holder.ivRewardStatus.setVisibility(View.VISIBLE);
            holder.ivRewardStatus.setImageResource(R.mipmap.greyy);
            holder.tvRewardStatus.setTextColor(homeActivity.getResources().getColor(R.color.GREEN));
            holder.tvRewardStatus.setText(R.string.str_reward_sending);
            holder.llRewardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openThankYouDialog("1", false);
                }
            });

        } else if (listdata.get(position).getStatus().equalsIgnoreCase("2")) {
            holder.ivRewardStatus.setVisibility(View.VISIBLE);
            holder.ivRewardStatus.setImageResource(R.mipmap.greeny);
            holder.tvRewardStatus.setTextColor(homeActivity.getResources().getColor(R.color.general_salah));
            holder.tvRewardStatus.setText(R.string.str_you_get_reward);
            holder.tvRewardStatus.setBackground(null);
            holder.llRewardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openThankYouDialog("2", false);
                }
            });

        } else if (listdata.get(position).getStatus().equalsIgnoreCase("3")) {
            holder.ivRewardStatus.setVisibility(View.GONE);
            holder.ivRewardStatus.setImageResource(R.mipmap.redw);
            holder.tvRewardStatus.setTextColor(homeActivity.getResources().getColor(R.color.general_salah));
            holder.llRewardView.setAlpha(0.2f);
            holder.tvRewardStatus.setText(R.string.str_expire);
            holder.tvRewardStatus.setBackground(null);
        }

    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout llRewardView;
        TextView tvRewardTitle, tvRewardStatus;
        ImageView ivRewardStatus, ivRewardIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            this.llRewardView = (LinearLayout) itemView.findViewById(R.id.llRewardView);
            this.tvRewardTitle = (TextView) itemView.findViewById(R.id.tvRewardTitle);
            this.tvRewardStatus = (TextView) itemView.findViewById(R.id.tvRewardStatus);
            this.ivRewardStatus = (ImageView) itemView.findViewById(R.id.ivRewardStatus);
            this.ivRewardIcon = (ImageView) itemView.findViewById(R.id.ivReward);
        }
    }


    void showDialog(String type, final String value, final String contastId) {
        Functions.setFirebaseLogEventTrack(homeActivity, Constants.Click_Event, "EarnReward Adapter", contastId, "Spin Wheel");

        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(homeActivity);
        View sheetView = LayoutInflater.from(homeActivity).inflate(R.layout.spin_wheel_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        ImageView ivRewardIcon = (ImageView) sheetView.findViewById(R.id.ivGift);
        if (type.equalsIgnoreCase("jackpot")) {
            ivRewardIcon.setImageResource(R.mipmap.spin_icon_big);
        } else if (type.equalsIgnoreCase("recharge_coupon")) {
            ivRewardIcon.setImageResource(R.mipmap.recharge_big);
        } else if (type.equalsIgnoreCase("expert_call")) {
            ivRewardIcon.setImageResource(R.mipmap.e_call_big);
        } else if (type.equalsIgnoreCase("combo")) {
            ivRewardIcon.setImageResource(R.mipmap.combo_big);
        }

        TextView tvRewardMsg = (TextView) sheetView.findViewById(R.id.tvRewardMsg);
        tvRewardMsg.setVisibility(View.VISIBLE);

        TextView tvOffline = (TextView) sheetView.findViewById(R.id.tvOffline);
        tvOffline.setVisibility(View.GONE);

        LinearLayout llSuccessView = (LinearLayout) sheetView.findViewById(R.id.llSuccessView);
        llSuccessView.setVisibility(View.VISIBLE);

        LinearLayout llFailView = (LinearLayout) sheetView.findViewById(R.id.llFailView);
        llFailView.setVisibility(View.GONE);

        TextView tvRechargeTxt = (TextView) sheetView.findViewById(R.id.tvRechargeTxt);
        tvRechargeTxt.setVisibility(View.GONE);

        if (type.equalsIgnoreCase("recharge_coupon")) {
            tvRechargeTxt.setVisibility(View.VISIBLE);
        } else {
            tvRechargeTxt.setVisibility(View.GONE);
        }

        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
            tvRewardMsg.setAlpha(1.0f);
            tvOffline.setVisibility(View.GONE);
        } else {
            tvRewardMsg.setAlpha(0.5f);
            tvOffline.setVisibility(View.VISIBLE);
        }

        tvRewardMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                    updateRewardStatus(contastId);
                    tvRewardMsg.setAlpha(1.0f);
                    tvOffline.setVisibility(View.GONE);
                    mBottomSheetDialog.dismiss();
                } else {
                    tvRewardMsg.setAlpha(0.5f);
                    tvOffline.setVisibility(View.VISIBLE);
                }
            }
        });

        TextView textView = (TextView) sheetView.findViewById(R.id.text);
        textView.setText(value);

        ImageView ivClose = (ImageView) sheetView.findViewById(R.id.imgClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        ((View) sheetView.getParent()).setBackgroundColor(homeActivity.getResources().getColor(android.R.color.transparent));

        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();
    }

    void updateRewardStatus(String contestID) {
        try {
            ProgressBarDialog progressBarDialog = new ProgressBarDialog(homeActivity);
            progressBarDialog.showProgressDialogWithTitle(homeActivity.getResources().getString(R.string.app_name));

            RetrofitClient.getInstance().getApi().updateRewardStatus(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    SharedPreferencesUtils.getUserId(homeActivity), contestID, "1", "").enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        progressBarDialog.hideProgressDialogWithTitle();
                        openThankYouDialog("1", true);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    progressBarDialog.hideProgressDialogWithTitle();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void openThankYouDialog(String str, Boolean isLoad) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.reward_thank_you_dialog, null);
        bottomSheetDialog.setContentView(view);

        AppCompatTextView txtTitle = view.findViewById(R.id.txtTitle);
        if (isLoad) {
            homeActivity.getRewardUpdate();
        }
        ImageView ivBanner = view.findViewById(R.id.ivBanner);
        AppCompatTextView tvDecs = view.findViewById(R.id.tvDecs);

        if (str.equalsIgnoreCase("1")) {
            ivBanner.setImageResource(R.mipmap.expert_speak);
            tvDecs.setText(R.string.str_help_to_rongo_exp);
        } else {
            ivBanner.setImageResource(R.mipmap.open_box);
            tvDecs.setText(R.string.str_you_get_reward);
        }

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        AppCompatTextView txtEnter = view.findViewById(R.id.txtEnter);
        txtEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(homeActivity.getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }


    void showSpinDateTimeDialog(String id, final String text) {
        final String[] sDay = {""}, sTime = {""};
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(homeActivity);
        View sheetView = LayoutInflater.from(homeActivity).inflate(R.layout.spin_wheel_date_time_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        ImageView ivClose = (ImageView) sheetView.findViewById(R.id.imgClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });

        TextView tvTitleMsg = (TextView) sheetView.findViewById(R.id.tvTitleMsg);
        TextView tvDarjKare = (TextView) sheetView.findViewById(R.id.tvDarjKare);

        List<String> timeList = new ArrayList<String>(Arrays.asList(homeActivity.getResources().getStringArray(R.array.times)));
        Spinner spinnerTime = (Spinner) sheetView.findViewById(R.id.spinnerTime);
        SpinnerHintArrayAdapter spinnerArrayAdapter = new SpinnerHintArrayAdapter(homeActivity, R.layout.spinner_item_list, timeList);
        spinnerArrayAdapter.add(homeActivity.getResources().getString(R.string.str_time_select));
        spinnerTime.setAdapter(spinnerArrayAdapter);
        spinnerTime.setSelection(spinnerArrayAdapter.getCount());
        spinnerTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                if (!item.equalsIgnoreCase(homeActivity.getResources().getString(R.string.str_time_select))) {
                    sTime[0] = item;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        List<String> dayList = new ArrayList<String>(Arrays.asList(homeActivity.getResources().getStringArray(R.array.days)));
        Spinner spinnerDay = (Spinner) sheetView.findViewById(R.id.spinnerDay);
        SpinnerHintArrayAdapter spinnerHintArrayAdapter = new SpinnerHintArrayAdapter(homeActivity, R.layout.spinner_item_list, dayList);
        spinnerHintArrayAdapter.add(homeActivity.getResources().getString(R.string.str_day_select));
        spinnerDay.setAdapter(spinnerHintArrayAdapter);
        spinnerDay.setSelection(spinnerHintArrayAdapter.getCount());
        spinnerDay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                if (!item.equalsIgnoreCase(homeActivity.getResources().getString(R.string.str_day_select))) {
                    sDay[0] = item;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        tvDarjKare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(sDay[0])) {
                    if (!TextUtils.isEmpty(sTime[0])) {
                        updateRewardStatus(id, sTime[0], sDay[0]);
                        mBottomSheetDialog.dismiss();
                    } else {
                        Toast.makeText(homeActivity, homeActivity.getResources().getString(R.string.str_please_select_time), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(homeActivity, homeActivity.getResources().getString(R.string.str_please_select_day), Toast.LENGTH_SHORT).show();
                }
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        ((View) sheetView.getParent()).setBackgroundColor(homeActivity.getResources().getColor(android.R.color.transparent));
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();
    }

    String getJackpotData(String time, String weekday) {
        String metaData = "";
        try {
            JSONObject obj = new JSONObject();
            obj.put("timeslot", time);
            obj.put("weekday", weekday);
            metaData = obj.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return metaData;
    }

    void updateRewardStatus(String rewardId, String time, String weekday) {
        try {
            RetrofitClient.getInstance().getApi().updateRewardStatus(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    SharedPreferencesUtils.getUserId(homeActivity), rewardId, "1", getJackpotData(time, weekday)).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        openThankYou();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void openThankYou() {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.reward_thank_you_dialog, null);
        bottomSheetDialog.setContentView(view);

        homeActivity.getRewardUpdate();

        AppCompatTextView txtTitle = view.findViewById(R.id.txtTitle);
        ImageView ivBanner = view.findViewById(R.id.ivBanner);
        ivBanner.setImageResource(R.mipmap.expert_speak);

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        AppCompatTextView txtEnter = view.findViewById(R.id.txtEnter);
        txtEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(homeActivity.getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

}
