package com.rongoapp.adapter;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rongoapp.R;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.GlideApp;
import com.rongoapp.data.MasterCropData;

import java.util.List;

public class CustomSpinnerAdapter extends BaseAdapter {

    Activity context;
    List<MasterCropData> masterCropList;

    public CustomSpinnerAdapter(Activity applicationContext, List<MasterCropData> masterCropList) {
        this.context = applicationContext;
        this.masterCropList = masterCropList;
    }

    @Override
    public int getCount() {
        return masterCropList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = (LayoutInflater.from(context)).inflate(R.layout.custom_spinner_items, null);

        TextView names = (TextView) view.findViewById(R.id.textView);
        names.setText(masterCropList.get(i).getDisplay_name());

        ImageView icon = (ImageView) view.findViewById(R.id.imageView);
        if (!TextUtils.isEmpty(masterCropList.get(i).getCrop_image())) {
            GlideApp.with(context).load(masterCropList.get(i).getCrop_image())
                    .apply(Functions.getPlaceholder(masterCropList.get(i).getCrop_id())).into(icon);
        } else {
            if (masterCropList.get(i).getCrop_id().equalsIgnoreCase("1")) {
                icon.setImageResource(R.mipmap.maize);
            } else if (masterCropList.get(i).getCrop_id().equalsIgnoreCase("2")) {
                icon.setImageResource(R.mipmap.rice);
            } else if (masterCropList.get(i).getCrop_id().equalsIgnoreCase("3")) {
                icon.setImageResource(R.mipmap.cotton);
            } else if (masterCropList.get(i).getCrop_id().equalsIgnoreCase("4")) {
                icon.setImageResource(R.mipmap.wheat);
            } else if (masterCropList.get(i).getCrop_id().equalsIgnoreCase("5")) {
                icon.setImageResource(R.mipmap.okra);
            } else if (masterCropList.get(i).getCrop_id().equalsIgnoreCase("6")) {
                icon.setImageResource(R.mipmap.pearmillet);
            } else if (masterCropList.get(i).getCrop_id().equalsIgnoreCase("7")) {
                icon.setImageResource(R.mipmap.corinder);
            } else if (masterCropList.get(i).getCrop_id().equalsIgnoreCase("8")) {
                icon.setImageResource(R.mipmap.bittergourd);
            } else if (masterCropList.get(i).getCrop_id().equalsIgnoreCase("9")) {
                icon.setImageResource(R.mipmap.bottlegourd);
            } else if (masterCropList.get(i).getCrop_id().equalsIgnoreCase("10")) {
                icon.setImageResource(R.mipmap.watermelon);
            } else if (masterCropList.get(i).getCrop_id().equalsIgnoreCase("11")) {
                icon.setImageResource(R.mipmap.groundnut);
            } else if (masterCropList.get(i).getCrop_id().equalsIgnoreCase("12")) {
                icon.setImageResource(R.mipmap.soyabean);
            } else if (masterCropList.get(i).getCrop_id().equalsIgnoreCase("13")) {
                icon.setImageResource(R.mipmap.caster);
            }
        }

        return view;
    }
}
