package com.rongoapp.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.viewpager.widget.PagerAdapter;

import com.rongoapp.R;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.data.CropWeekDetails;
import com.rongoapp.fragments.PhasalFragment;
import com.rongoapp.utilities.AutoResizeTextView;

import java.util.List;

public class StagesAdapter extends PagerAdapter {

    Context context;
    PhasalFragment phasalFragment;
    SharedPreferences sharedPreferences;
    List<CropWeekDetails> cropWeekDetails;
    String seasonName;

    public StagesAdapter(Context context, PhasalFragment phasalFragment, List<CropWeekDetails> cropWeekDetails) {
        this.context = context;
        this.phasalFragment = phasalFragment;
        this.cropWeekDetails = cropWeekDetails;
        sharedPreferences = context.getSharedPreferences(Constants.MediaPrefs, 0);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_item_top_cards, null);

        try {
            AutoResizeTextView txtStageName = view.findViewById(R.id.txtStageName);
            txtStageName.setText(cropWeekDetails.get(position).getDisplay_name());

            AppCompatTextView txtLastWeek = view.findViewById(R.id.txtLastWeek);
            if (position == 0) {
                txtLastWeek.setVisibility(View.GONE);
            } else {
                txtLastWeek.setVisibility(View.VISIBLE);
            }

            AppCompatTextView txtWeek = view.findViewById(R.id.txtWeek);
            if (cropWeekDetails.get(position).getCrop_week() != 0) {

                if (RongoApp.getSeasonName().equalsIgnoreCase(Constants.KHARIF_SEASON)) {
                    seasonName = context.getString(R.string.str_kharif_season);
                } else if (RongoApp.getSeasonName().equalsIgnoreCase(Constants.RABI_SEASON)) {
                    seasonName = context.getString(R.string.str_rabi_season);
                } else {
                    if (RongoApp.getSelectedCrop().equalsIgnoreCase("1")) {
                        seasonName = context.getString(R.string.str_kharif_season);
                    } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("2")) {
                        seasonName = context.getString(R.string.str_kharif_season);
                    } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("3")) {
                        seasonName = context.getString(R.string.str_kharif_season);
                    } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("4")) {
                        seasonName = context.getString(R.string.str_rabi_season);
                    } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("5")) {
                        seasonName = context.getString(R.string.str_kharif_season);
                    } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("6")) {
                        seasonName = context.getString(R.string.str_kharif_season);
                    } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("7")) {
                        seasonName = context.getString(R.string.str_rabi_season);
                    } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("8")) {
                        seasonName = context.getString(R.string.str_rabi_season);
                    } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("9")) {
                        seasonName = context.getString(R.string.str_rabi_season);
                    } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("10")) {
                        seasonName = context.getString(R.string.str_rabi_season);
                    } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("11")) {
                        seasonName = context.getString(R.string.str_kharif_season);
                    } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("12")) {
                        seasonName = context.getString(R.string.str_kharif_season);
                    } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("13")) {
                        seasonName = context.getString(R.string.str_kharif_season);
                    }
                }

                txtWeek.setText("(" + seasonName + ") " + Functions.getName(cropWeekDetails.get(position).getCrop_week()) + " " + context.getResources().getString(R.string.saptah));

                if (cropWeekDetails.get(position).getCrop_week() >= Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"))) {
                    txtWeek.setText("");
                }
            }

            txtLastWeek.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    phasalFragment.lastWeek(position);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return cropWeekDetails.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

}
