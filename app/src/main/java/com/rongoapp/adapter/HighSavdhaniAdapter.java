package com.rongoapp.adapter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.rongoapp.R;
import com.rongoapp.activities.HomeActivity;
import com.rongoapp.activities.SavdhaniDetailsActivity;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.FetchJsonFromAssetsFile;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.data.DiasesData;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class HighSavdhaniAdapter extends RecyclerView.Adapter<HighSavdhaniAdapter.ViewHolder> {

    HomeActivity homeActivity;
    SharedPreferences sharedPreferences;
    List<DiasesData> listdata;
    BottomSheetDialog bottomSheetDialog;

    public HighSavdhaniAdapter(HomeActivity homeActivity, List<DiasesData> listdata, BottomSheetDialog bottomSheetDialog) {
        this.homeActivity = homeActivity;
        this.listdata = listdata;
        this.bottomSheetDialog = bottomSheetDialog;
        sharedPreferences = homeActivity.getSharedPreferences(Constants.MediaPrefs, 0);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.high_savdhani_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try {
            holder.tvTitle.setText(listdata.get(position).getName());

            holder.tvTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (bottomSheetDialog != null) {
                        bottomSheetDialog.dismiss();
                    }

                    Intent i = new Intent(holder.itemView.getContext(), SavdhaniDetailsActivity.class);
                    i.putExtra("risk", "1");
                    i.putExtra("keyword_id", listdata.get(position).getKeyword_id());
                    holder.itemView.getContext().startActivity(i);
                }
            });

            holder.ivIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (bottomSheetDialog != null) {
                        bottomSheetDialog.dismiss();
                    }

                    Intent i = new Intent(holder.itemView.getContext(), SavdhaniDetailsActivity.class);
                    i.putExtra("risk", "1");
                    i.putExtra("keyword_id", listdata.get(position).getKeyword_id());
                    holder.itemView.getContext().startActivity(i);
                }
            });

            JSONObject jsonObject = new JSONObject("" + FetchJsonFromAssetsFile.
                    loadAdvisoryKeywordJSONFromAsset(homeActivity, listdata.get(position).getKeyword_id()));
            if (jsonObject != null && jsonObject.length() > 0) {
                JSONArray advisory_image = jsonObject.optJSONArray("advisory_image");
                if (advisory_image != null && advisory_image.length() > 0) {
                    for (int i = 0; i < advisory_image.length(); i++) {
//                        Log.e("@@@path", "" + advisory_image.optString(i));

                        Drawable drawable = Functions.getAssetsKeywordImage(homeActivity,
                                Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH +
                                        RongoApp.getSelectedCrop() + "/" + advisory_image.optString(i));
                        holder.ivIcon.setImageDrawable(drawable);
                    }
                }
            }

//            Drawable drawable = Functions.getAssetsKeywordImage(homeActivity, Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH +
//                    RongoApp.getSelectedCrop() + "/" + listdata.get(position).getKeyword_id());
//            if (!InternetConnection.checkConnectionForFasl(homeActivity)) {
//                holder.ivIcon.setImageDrawable(drawable);
//            } else {
//                GlideApp.with(homeActivity).load(image_path + "/" + listdata.get(position).getKeyword_id())
//                        .apply(Functions.getDrawablePlaceholder(drawable)).into(holder.ivIcon);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        ImageView ivIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            this.tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            this.ivIcon = (ImageView) itemView.findViewById(R.id.ivIcon);
        }
    }

}