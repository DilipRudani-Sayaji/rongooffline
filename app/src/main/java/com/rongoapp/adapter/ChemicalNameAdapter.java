package com.rongoapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.rongoapp.R;
import com.rongoapp.data.ChemicalNameModel;

import java.util.List;

public class ChemicalNameAdapter extends RecyclerView.Adapter<ChemicalNameAdapter.ViewHolder> {

    List<ChemicalNameModel> chemicalNameModels;

    public ChemicalNameAdapter(List<ChemicalNameModel> listdata) {
        this.chemicalNameModels = listdata;
    }

    @Override
    public ChemicalNameAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.chemical_name_list_item, parent, false);
        ChemicalNameAdapter.ViewHolder viewHolder = new ChemicalNameAdapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ChemicalNameAdapter.ViewHolder holder, final int position) {
        holder.tvTechnicalName.setText(chemicalNameModels.get(position).getTechnical());
        holder.tvName.setText(chemicalNameModels.get(position).getNormal());
    }

    @Override
    public int getItemCount() {
        return chemicalNameModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvTechnicalName;

        public ViewHolder(View itemView) {
            super(itemView);
            this.tvName = (TextView) itemView.findViewById(R.id.tvName);
            this.tvTechnicalName = (TextView) itemView.findViewById(R.id.tvTechnicalName);
        }
    }

}