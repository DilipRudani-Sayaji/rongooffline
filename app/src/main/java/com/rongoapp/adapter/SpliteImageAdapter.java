package com.rongoapp.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.rongoapp.R;

import java.util.ArrayList;

public class SpliteImageAdapter extends BaseAdapter {

    Context mContext;
    ArrayList<Bitmap> imageChunks;

    public SpliteImageAdapter(Context c, ArrayList<Bitmap> images) {
        mContext = c;
        imageChunks = images;
    }

    @Override
    public int getCount() {
        return imageChunks.size();
    }

    @Override
    public Object getItem(int position) {
        return imageChunks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = (LayoutInflater.from(mContext)).inflate(R.layout.split_image, null);
        ImageView image = (ImageView) view.findViewById(R.id.imageView);
        ImageView ivTick = (ImageView) view.findViewById(R.id.ivTick);
        image.setImageBitmap(imageChunks.get(position));
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ivTick.getVisibility() == View.VISIBLE) {
                    ivTick.setVisibility(View.GONE);
                } else {
                    ivTick.setVisibility(View.VISIBLE);
                }
            }
        });

        return view;
    }
}
