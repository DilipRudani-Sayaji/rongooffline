package com.rongoapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.rongoapp.R;
import com.rongoapp.controller.Functions;

import java.util.List;

public class ThumbnailsAdapter extends RecyclerView.Adapter<ThumbnailsAdapter.ViewHolder> {

    List<String> thumbData;

    public ThumbnailsAdapter(List<String> listdata) {
        this.thumbData = listdata;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.row_item_gallery_photo, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        String path = thumbData.get(position);
        if (path.contains("/root")) {
            path = path.replace("/root", "");
        }

        Functions.displayImageLocal(path, holder.imgthumbnail);

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                thumbData.remove(thumbData.get(position));
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return thumbData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        RoundedImageView imgthumbnail;
        ImageView imgDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imgthumbnail = itemView.findViewById(R.id.imgthumbnail);
            this.imgDelete = itemView.findViewById(R.id.imgDelete);
        }
    }

}
