package com.rongoapp.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.rongoapp.R;
import com.rongoapp.controller.Functions;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.Arrays;
import java.util.List;

public class ManagementAdapter extends RecyclerView.Adapter<ManagementAdapter.ViewHolder> {

    List<String> listdata;
    Activity activity;

    public ManagementAdapter(Activity activity, List<String> listdata) {
        this.activity = activity;
        this.listdata = listdata;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.manage_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
//            holder.tvManage.setText(Functions.getConvertHtml(listdata.get(position)));

            Object json = new JSONTokener(listdata.get(position)).nextValue();
            if (json instanceof JSONObject) {
                JSONObject jsonObject = new JSONObject(listdata.get(position));
                if (jsonObject != null && jsonObject.length() > 0) {
                    for (int i = 0; i < jsonObject.length(); i++) {

                        if (!TextUtils.isEmpty(jsonObject.getString("advisory"))) {
                            holder.ivbullet.setVisibility(View.VISIBLE);
                            holder.tvManage.setVisibility(View.VISIBLE);
                            holder.tvManage.setText(Functions.getConvertHtml(jsonObject.getString("advisory")));
                        } else {
                            holder.ivbullet.setVisibility(View.INVISIBLE);
                            holder.tvManage.setVisibility(View.GONE);
                        }

                        if (!TextUtils.isEmpty(jsonObject.getString("note"))) {
                            holder.tvSubManage.setVisibility(View.VISIBLE);
                            holder.tvSubManage.setText(Functions.getConvertHtml(jsonObject.getString("note")) + ":");
                        } else {
                            holder.tvSubManage.setVisibility(View.GONE);
                        }

                        String note = jsonObject.getString("chemical_usage").replace("[", "")
                                .replace("]", "").replace("\"", "");
                        if (!TextUtils.isEmpty(note)) {
                            holder.llNoteView.removeAllViews();
                            holder.llNoteView.invalidate();
                            List<String> items = Arrays.asList(note.split("\\s*,\\s*"));
                            if (items != null && items.size() > 0) {
                                for (int j = 0; j < items.size(); j++) {
                                    LinearLayout childLayout = new LinearLayout(activity);
                                    childLayout.setOrientation(LinearLayout.HORIZONTAL);
                                    childLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

                                    TextView mType = new TextView(activity);
                                    mType.setLayoutParams(new TableLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                            LinearLayout.LayoutParams.WRAP_CONTENT));
                                    mType.setTextSize(20);
                                    mType.setPadding(20, 2, 0, 0);
                                    mType.setTypeface(Typeface.DEFAULT_BOLD);
                                    mType.setGravity(Gravity.LEFT | Gravity.CENTER);
                                    mType.setText(Html.fromHtml("&#8226;", Html.FROM_HTML_MODE_LEGACY));

                                    TextView mValue = new TextView(activity);
                                    mValue.setLayoutParams(new TableLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                                            LinearLayout.LayoutParams.WRAP_CONTENT));
                                    mValue.setTextSize(15);
                                    mValue.setPadding(10, 0, 0, 4);
                                    mValue.setTypeface(null, Typeface.NORMAL);
                                    mValue.setGravity(Gravity.LEFT | Gravity.CENTER);
                                    mValue.setText(items.get(j));

                                    childLayout.addView(mValue, 0);
                                    childLayout.addView(mType, 0);

                                    holder.llNoteView.addView(childLayout);
                                }
                            }
                        }
                    }
                }
            } else {
                holder.tvManage.setText(Functions.getConvertHtml(listdata.get(position)));
            }
        } catch (Exception e) {
            e.printStackTrace();
            holder.tvManage.setText(Functions.getConvertHtml(listdata.get(position)));
        }
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvManage, tvSubManage, tvSubManage1;
        public ImageView ivbullet;
        public LinearLayout llNoteView;

        public ViewHolder(View itemView) {
            super(itemView);
            ivbullet = (ImageView) itemView.findViewById(R.id.ivbullet);
            this.tvManage = (TextView) itemView.findViewById(R.id.tvManage);
            this.tvSubManage = (TextView) itemView.findViewById(R.id.tvSubManage);
            tvSubManage.setVisibility(View.GONE);
            this.tvSubManage1 = (TextView) itemView.findViewById(R.id.tvSubManage1);
            tvSubManage1.setVisibility(View.GONE);
            this.llNoteView = (LinearLayout) itemView.findViewById(R.id.llNoteView);
        }
    }

}
