package com.rongoapp.adapter;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.rongoapp.R;
import com.rongoapp.activities.HomeActivity;
import com.rongoapp.activities.SavdhaniDetailsActivity;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.FetchJsonFromAssetsFile;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.data.SavdhaniDataList;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class HighRiskSavdhaniAdapter extends RecyclerView.Adapter<HighRiskSavdhaniAdapter.ViewHolder> {

    HomeActivity homeActivity;
    List<SavdhaniDataList> listdata;

    public HighRiskSavdhaniAdapter(HomeActivity homeActivity, List<SavdhaniDataList> listdata) {
        this.homeActivity = homeActivity;
        this.listdata = listdata;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.high_risk_savdhani_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try {
            holder.tvTitle.setText(listdata.get(position).getName());

            if (listdata.get(position).getStatus().equalsIgnoreCase("1")) {
                holder.tvTitle.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.diea, 0, 0, 0);
            } else if (listdata.get(position).getStatus().equalsIgnoreCase("2")) {
                holder.tvTitle.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.pes, 0, 0, 0);
            } else if (listdata.get(position).getStatus().equalsIgnoreCase("3")) {
                holder.tvTitle.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.nutr, 0, 0, 0);
            }

            if (listdata.get(position).getType().equalsIgnoreCase("1")) {
                holder.tvType.setText(holder.itemView.getContext().getString(R.string.str_high_risk));
                holder.tvType.setCompoundDrawablesWithIntrinsicBounds(holder.itemView.getContext().getDrawable(R.mipmap.alert), null, null, null);
            } else {
                holder.tvType.setText(holder.itemView.getContext().getString(R.string.str_savdhani_text));
                holder.tvType.setCompoundDrawablesWithIntrinsicBounds(holder.itemView.getContext().getDrawable(R.mipmap.yellow_warning), null, null, null);
            }

            holder.tvTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(holder.itemView.getContext(), SavdhaniDetailsActivity.class);
                    i.putExtra("risk", listdata.get(position).getType());
                    i.putExtra("keyword_id", listdata.get(position).getKeyword_id());
                    holder.itemView.getContext().startActivity(i);
                }
            });

            holder.ivIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(holder.itemView.getContext(), SavdhaniDetailsActivity.class);
                    i.putExtra("risk", listdata.get(position).getType());
                    i.putExtra("keyword_id", listdata.get(position).getKeyword_id());
                    holder.itemView.getContext().startActivity(i);
                }
            });

            JSONObject jsonObject = new JSONObject("" + FetchJsonFromAssetsFile.loadAdvisoryKeywordJSONFromAsset(
                    homeActivity, listdata.get(position).getKeyword_id()));
            if (jsonObject != null && jsonObject.length() > 0) {
                JSONArray advisory_image = jsonObject.optJSONArray("advisory_image");
                if (advisory_image != null && advisory_image.length() > 0) {

                    String image = advisory_image.optString(0);
                    Drawable drawable = Functions.getAssetsKeywordImage(homeActivity, Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH +
                            RongoApp.getSelectedCrop() + "/" + image);
                    holder.ivIcon.setImageDrawable(drawable);

//                    for (int i = 0; i < advisory_image.length(); i++) {
//                        Drawable drawable = Functions.getAssetsKeywordImage(homeActivity, Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH +
//                                RongoApp.getSelectedCrop() + "/" + advisory_image.optString(0));
//                        holder.ivIcon.setImageDrawable(drawable);
//                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvType;
        ImageView ivIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            this.tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            this.tvType = (TextView) itemView.findViewById(R.id.tvType);
            this.ivIcon = (ImageView) itemView.findViewById(R.id.ivIcon);
        }
    }

}