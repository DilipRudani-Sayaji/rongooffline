package com.rongoapp.spinWheel;

public class LuckyItem {

    public String id;
    public String topText;
    public String type;
    public String value;
    public String secondaryText;
    public int secondaryTextOrientation;
    public int icon;
    public int color;

    public LuckyItem() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTopText() {
        return topText;
    }

    public void setTopText(String topText) {
        this.topText = topText;
    }

    public String getSecondaryText() {
        return secondaryText;
    }

    public void setSecondaryText(String secondaryText) {
        this.secondaryText = secondaryText;
    }

    public int getSecondaryTextOrientation() {
        return secondaryTextOrientation;
    }

    public void setSecondaryTextOrientation(int secondaryTextOrientation) {
        this.secondaryTextOrientation = secondaryTextOrientation;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
