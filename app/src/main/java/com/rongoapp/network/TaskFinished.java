package com.rongoapp.network;

interface TaskFinished<T> {
    void onTaskFinished(T data);
}
