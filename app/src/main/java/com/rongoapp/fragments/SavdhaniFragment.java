package com.rongoapp.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;
import com.rongoapp.R;
import com.rongoapp.activities.SavdhaniDetailsActivity;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.FetchJsonFromAssetsFile;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.GlideApp;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.DiasesData;
import com.rongoapp.data.NutritionData;
import com.rongoapp.data.PestData;
import com.rongoapp.data.SavdhaniCardList;
import com.rongoapp.data.SavdhaniDataList;
import com.rongoapp.data.WeeklyAdvisoryData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.storyProgressView.StoriesProgressView;
import com.rongoapp.views.GridSpacingItemDecoration;
import com.rongoapp.views.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class SavdhaniFragment extends DialogFragment implements StoriesProgressView.StoriesListener {

    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    View view;
    String storyType = "1", image_path = "https://rongoapp.in/assets/image/advisory/keyword/";

    List<SavdhaniCardList> savdhaniCardLists = new ArrayList<>();
    List<SavdhaniDataList> highRiskData = new ArrayList<SavdhaniDataList>();
    List<SavdhaniCardList> tempList = new ArrayList<>();

    RelativeLayout rlMainView;
    StoriesProgressView storiesProgressView;
    RoundedImageView ivClose;
    LinearLayout llMoreView, llTopBannerView;
    TextView tvNoDataAvailable, tvMoreText;

    int counter = 0, storyCounter = 0;
    long pressTime = 0L;
    long limit = 500L;
    View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    pressTime = System.currentTimeMillis();
                    storiesProgressView.pause();
                    return false;
                case MotionEvent.ACTION_UP:
                    long now = System.currentTimeMillis();
                    storiesProgressView.resume();
                    return limit < now - pressTime;
            }
            return false;
        }
    };

    @Override
    public int getTheme() {
        return R.style.FullScreenDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.FullScreenDialog);

        sharedPreferences = getActivity().getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(getActivity());

        RongoApp.engagementToolActivity(Constants.ET_SAVDHANI);
        RongoApp.logVisitedPageTabEvent("Savdhani Story");

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public static SavdhaniFragment newInstance() {
        SavdhaniFragment fragment = new SavdhaniFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_savdhani_dialog, container, false);
        setSavdhaniData();
        return view;
    }

    void setSavdhaniData() {
        try {
            tvNoDataAvailable = (TextView) view.findViewById(R.id.tvNoDataAvailable);
            tvMoreText = (TextView) view.findViewById(R.id.tvMoreText);
            llMoreView = (LinearLayout) view.findViewById(R.id.llMoreView);

            ivClose = (RoundedImageView) view.findViewById(R.id.ivClose);
            ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            llTopBannerView = (LinearLayout) view.findViewById(R.id.llTopBannerView);
            int cropWeek = RongoApp.getCropWeekDaysFunction();
            if (cropWeek > 0 && cropWeek < Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"))) {
                List<WeeklyAdvisoryData> weeklyAdvisoryData = weeklyAdvisoryData();
                if (weeklyAdvisoryData.size() > 0) {
                    try {
                        storyCounter = 0;
                        String storyCounterData = sharedPreferences.getString(Constants.SharedPreferences_story_savdhani_count, "0");
                        highRiskData = new ArrayList<SavdhaniDataList>();
                        savdhaniCardLists = new ArrayList<>();
                        List<PestData> mPestData = weeklyAdvisoryData.get(0).getPestDataList();
                        List<DiasesData> mDiaseData = weeklyAdvisoryData.get(0).getDiasesDataList();
                        List<NutritionData> mNutritionData = weeklyAdvisoryData.get(0).getNutretionList();

                        JSONObject resultData = new JSONObject(Objects.requireNonNull(FetchJsonFromAssetsFile.loadHighKeywordJSONFromAsset(getActivity())));
                        if (resultData != null && resultData.length() > 0) {
                            Iterator<String> it = resultData.keys();
                            while (it.hasNext()) {
                                String key = it.next();
                                if (key.equalsIgnoreCase(RongoApp.getSelectedState().toLowerCase())) {
                                    JSONObject result = resultData.optJSONObject(key);
                                    JSONArray keywords = new JSONArray(result.optString("high"));
                                    if (keywords != null && keywords.length() > 0) {
                                        for (int i = 0; i < keywords.length(); i++) {

                                            savdhaniCardLists = new ArrayList<>();
                                            SavdhaniCardList savdhaniCardList = new SavdhaniCardList();
                                            String status = "";
                                            String keywordID = keywords.optString(i);
                                            JSONObject jsonObject = new JSONObject("" + FetchJsonFromAssetsFile.loadAdvisoryKeywordJSONFromAsset(getActivity(), keywordID));
//                                        image_path = jsonObject.optString("image_path");
                                            String name = jsonObject.optString("name");
                                            JSONArray advisory_image = jsonObject.optJSONArray("advisory_image");
                                            JSONObject images = jsonObject.optJSONObject("images");
                                            JSONObject stage_1 = images.optJSONObject("stage_1");
                                            JSONArray image = stage_1.optJSONArray("image");
                                            JSONArray laxan = jsonObject.optJSONArray("symptoms");


                                            if (i == 0) {
                                                savdhaniCardList = new SavdhaniCardList();
                                                savdhaniCardList.setKeyword_id(keywordID);
                                                savdhaniCardList.setName(name);
                                                savdhaniCardList.setStatus("1");
                                                savdhaniCardList.setType("1");
                                                savdhaniCardList.setImage(advisory_image.optString(0));
                                                savdhaniCardList.setText("");
                                                savdhaniCardLists.add(savdhaniCardList);

                                                savdhaniCardList = new SavdhaniCardList();
                                                savdhaniCardList.setKeyword_id(keywordID);
                                                savdhaniCardList.setName(name);
                                                savdhaniCardList.setStatus("1");
                                                savdhaniCardList.setType("1");
                                                savdhaniCardList.setImage(advisory_image.optString(0));
                                                savdhaniCardList.setText("");
                                                savdhaniCardLists.add(savdhaniCardList);
                                            } else {
                                                savdhaniCardList = new SavdhaniCardList();
                                                savdhaniCardList.setKeyword_id(keywordID);
                                                savdhaniCardList.setName(name);
                                                savdhaniCardList.setStatus("1");
                                                savdhaniCardList.setType("1");
                                                savdhaniCardList.setImage(advisory_image.optString(0));
                                                savdhaniCardList.setText("");
                                                savdhaniCardLists.add(savdhaniCardList);
                                            }

                                            for (int j = 0; j < mPestData.size(); j++) {
                                                if (mPestData.get(j).getKeyword_id().equalsIgnoreCase(keywordID)) {
                                                    status = "1";
//                                                for (int k = 0; k < image.length(); k++) {
                                                    for (int k = 0; k < 4; k++) {
                                                        savdhaniCardList = new SavdhaniCardList();
                                                        savdhaniCardList.setKeyword_id(keywordID);
                                                        savdhaniCardList.setName(name);
                                                        savdhaniCardList.setStatus("1");
                                                        savdhaniCardList.setType("1");
                                                        savdhaniCardList.setImage(image.optString(k));
                                                        if (!TextUtils.isEmpty(laxan.optString(k))) {
                                                            savdhaniCardList.setText(laxan.optString(k));
                                                            savdhaniCardLists.add(savdhaniCardList);
                                                        } else {
                                                            if (savdhaniCardLists.size() < 4) {
                                                                savdhaniCardList.setText("");
                                                                savdhaniCardLists.add(savdhaniCardList);
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            for (int j = 0; j < mDiaseData.size(); j++) {
                                                if (mDiaseData.get(j).getKeyword_id().equalsIgnoreCase(keywordID)) {
                                                    status = "2";
                                                    for (int k = 0; k < 4; k++) {
                                                        savdhaniCardList = new SavdhaniCardList();
                                                        savdhaniCardList.setName(name);
                                                        savdhaniCardList.setKeyword_id(keywordID);
                                                        savdhaniCardList.setStatus("2");
                                                        savdhaniCardList.setType("2");
                                                        savdhaniCardList.setImage(image.optString(k));
                                                        if (!TextUtils.isEmpty(laxan.optString(k))) {
                                                            savdhaniCardList.setText(laxan.optString(k));
                                                            savdhaniCardLists.add(savdhaniCardList);
                                                        } else {
                                                            if (savdhaniCardLists.size() < 4) {
                                                                savdhaniCardList.setText("");
                                                                savdhaniCardLists.add(savdhaniCardList);
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            for (int j = 0; j < mNutritionData.size(); j++) {
                                                if (mNutritionData.get(j).getKeyword_id().equalsIgnoreCase(keywordID)) {
                                                    status = "3";
                                                    for (int k = 0; k < 4; k++) {
                                                        savdhaniCardList = new SavdhaniCardList();
                                                        savdhaniCardList.setKeyword_id(keywordID);
                                                        savdhaniCardList.setStatus("3");
                                                        savdhaniCardList.setName(name);
                                                        savdhaniCardList.setType("3");
                                                        savdhaniCardList.setImage(image.optString(k));
                                                        if (!TextUtils.isEmpty(laxan.optString(k))) {
                                                            savdhaniCardList.setText(laxan.optString(k));
                                                            savdhaniCardLists.add(savdhaniCardList);
                                                        } else {
                                                            if (savdhaniCardLists.size() < 4) {
                                                                savdhaniCardList.setText("");
                                                                savdhaniCardLists.add(savdhaniCardList);
                                                            }
                                                        }
                                                    }
                                                }
                                            }


                                            SavdhaniDataList savdhaniDataList = new SavdhaniDataList();
                                            savdhaniDataList.setType("1");
                                            savdhaniDataList.setStatus(status);
                                            savdhaniDataList.setName(name);
//                                        savdhaniDataList.setBitmap(getDrawable(advisory_image.optString(0)));
                                            savdhaniDataList.setBitmap(advisory_image.optString(0));
                                            savdhaniDataList.setKeyword_id(keywordID);
                                            savdhaniDataList.setSavdhaniCardLists(savdhaniCardLists);
                                            Log.e("savdhaniCardLists: ", laxan.length() + "-" + keywordID + "-" + savdhaniCardLists.size());
                                            if (savdhaniCardLists.size() > 2) {
                                                highRiskData.add(savdhaniDataList);
                                            }

                                        }
                                    }
                                }
                            }
                        }

                        if (highRiskData.size() > 0) {
                            tvNoDataAvailable.setVisibility(View.GONE);

                            if (highRiskData.size() > 6) {
                                llMoreView.setVisibility(View.VISIBLE);
                                tvMoreText.setText("+" + (highRiskData.size() - 6));
                            }

                            if (!TextUtils.isEmpty(storyCounterData)) {
                                if (highRiskData.size() > Integer.parseInt(storyCounterData)) {
                                    storyCounter = Integer.parseInt(storyCounterData);
                                }
                            }
                            setStoryCardView();
                        } else {
                            tvNoDataAvailable.setVisibility(View.VISIBLE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    tvNoDataAvailable.setVisibility(View.VISIBLE);
                }
            } else {
                tvNoDataAvailable.setVisibility(View.VISIBLE);
                tvNoDataAvailable.setText(getResources().getString(R.string.str_no_data_available));
                llTopBannerView.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setStoryCardView() {
        try {
            counter = 0;
            tempList = new ArrayList<>();
            tempList.addAll(highRiskData.get(storyCounter).getSavdhaniCardLists());
            Log.e("size: ", storyCounter + "-" + highRiskData.size() + "--" + tempList.size());

            storiesProgressView = (StoriesProgressView) view.findViewById(R.id.stories);
            storiesProgressView.setStoriesCount(tempList.size());
            storiesProgressView.setStoryDuration(4000L);
            storiesProgressView.setStoriesListener(this);
            storiesProgressView.startStories(counter);

            rlMainView = (RelativeLayout) view.findViewById(R.id.rlMainView);
//            rlMainView.setBackgroundColor(getDominantColor(highRiskData.get(counter).getBitmap()));

            TextView tvSubTitle = view.findViewById(R.id.tvSubTitle);
            tvSubTitle.setText(tempList.get(counter).getName());

            TextView tvBack = view.findViewById(R.id.tvBack);
            tvBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ((counter - 1) < 0) {
                        if ((storyCounter - 1) < 0) {
                            Intent intent = new Intent();
                            intent.setAction(Constants.BROADCAST_RECEIVER.UPDATE_STORY);
                            intent.putExtra(Constants.STORY_NAME, Constants.STORY_QUIZ);
                            getActivity().sendBroadcast(intent);
                            dismiss();
                        } else {
                            storyCounter = --storyCounter;
                            setStoryCardView();
                        }
                    } else {
                        storiesProgressView.reverse();
                    }
                }
            });
            TextView tvNext = view.findViewById(R.id.tvNext);
            tvNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tempList.size() == (counter + 1)) {
                        if (highRiskData.size() == (storyCounter + 1)) {
                            Intent intent = new Intent();
                            intent.setAction(Constants.BROADCAST_RECEIVER.UPDATE_STORY);
                            intent.putExtra(Constants.STORY_NAME, Constants.STORY_CROP_HEALTH);
                            getActivity().sendBroadcast(intent);
                            dismiss();

                            SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                    Constants.SharedPreferences_story_savdhani_count, "0");
                        } else {
                            storyCounter = ++storyCounter;
                            setStoryCardView();
                        }
                    } else {
                        storiesProgressView.skip();
                    }
                }
            });

            setUpdatedCardView();

            View reverse = view.findViewById(R.id.reverse);
            reverse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    storiesProgressView.reverse();
                }
            });
            reverse.setOnTouchListener(onTouchListener);

            View skip = view.findViewById(R.id.skip);
            skip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    storiesProgressView.skip();
                }
            });
            skip.setOnTouchListener(onTouchListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setUpdatedCardView() {
        try {
            Functions.setFirebaseLogEventTrack(getActivity(), Constants.ET_STORY_TRACK, Constants.ET_SAVDHANI,
                    "" + RongoApp.getUserId(), "Savdhani_" + tempList.get(counter).getKeyword_id() + "_" + counter);
            Functions.setUserScreenTrackEvent(appDatabase, Constants.ET_STORY_TRACK, Constants.ET_SAVDHANI + "_" + RongoApp.getUserId());

            SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                    Constants.SharedPreferences_story_savdhani_count, "" + storyCounter);

            RelativeLayout rlMain = (RelativeLayout) view.findViewById(R.id.rlMain);
            RelativeLayout rlOne = (RelativeLayout) view.findViewById(R.id.rlOne);
            RelativeLayout rlSecond = (RelativeLayout) view.findViewById(R.id.rlSecond);
            RelativeLayout rlThree = (RelativeLayout) view.findViewById(R.id.rlThree);
            RelativeLayout rlFour = (RelativeLayout) view.findViewById(R.id.rlFour);
            RelativeLayout rlFive = (RelativeLayout) view.findViewById(R.id.rlFive);

            ImageView ivSavdhani = (ImageView) view.findViewById(R.id.ivSavdhani);
            ivSavdhani.setVisibility(View.VISIBLE);
            ImageView ivSavdhaniOne1 = (ImageView) view.findViewById(R.id.ivSavdhaniOne1);

            TextView tvOne = (AppCompatTextView) view.findViewById(R.id.tvOne);
            TextView tvTitleTwo = (AppCompatTextView) view.findViewById(R.id.tvTitleTwo);
            TextView tvDecsTwo = (AppCompatTextView) view.findViewById(R.id.tvDecsTwo);
            TextView tvTitleThree = (AppCompatTextView) view.findViewById(R.id.tvTitleThree);
            TextView tvDecsFour = (AppCompatTextView) view.findViewById(R.id.tvDecsFour);
            TextView tvHa = (AppCompatTextView) view.findViewById(R.id.tvHa);
            TextView tvNa = (AppCompatTextView) view.findViewById(R.id.tvNa);

            tvHa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e("Click:", tempList.get(counter).getKeyword_id());
                    Intent i = new Intent(getActivity(), SavdhaniDetailsActivity.class);
                    i.putExtra("risk", tempList.get(counter).getType());
                    i.putExtra("keyword_id", tempList.get(counter).getKeyword_id());
                    getActivity().startActivity(i);
                    dismiss();
                }
            });
            tvNa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (highRiskData.size() == (storyCounter + 1)) {
                        storyCounter = 0;
                        setStoryCardView();
                    } else {
                        storyCounter = ++storyCounter;
                        setStoryCardView();

                        rlMain.setVisibility(View.GONE);
                        rlOne.setVisibility(View.VISIBLE);
                        rlSecond.setVisibility(View.GONE);
                        rlThree.setVisibility(View.GONE);
                        rlFour.setVisibility(View.GONE);
                        rlFive.setVisibility(View.GONE);
                    }
                }
            });

            if (tempList.get(counter).getStatus().equalsIgnoreCase("1")) {
                ivSavdhaniOne1.setImageResource(R.mipmap.diea);
            } else if (tempList.get(counter).getStatus().equalsIgnoreCase("2")) {
                ivSavdhaniOne1.setImageResource(R.mipmap.pes);
            } else if (tempList.get(counter).getStatus().equalsIgnoreCase("3")) {
                ivSavdhaniOne1.setImageResource(R.mipmap.nutr);
            }

            Log.e("Data:", storyCounter + "-" + counter + "-" + tempList.size());
            if (!TextUtils.isEmpty(tempList.get(counter).getImage())) {
                GlideApp.with(getActivity()).load(image_path +
                        RongoApp.getSelectedCrop() + "/" + tempList.get(counter).getImage())
                        .error(R.drawable.story_placeholder)
                        .placeholder(R.drawable.story_placeholder).into(ivSavdhani);

                tvOne.setText(tempList.get(counter).getName());
                tvDecsTwo.setText(tempList.get(counter).getText());
                tvTitleThree.setText(tempList.get(counter).getText());
                tvDecsFour.setText(tempList.get(counter).getText());
            } else {
                GlideApp.with(getActivity()).load(R.drawable.story_placeholder)
                        .placeholder(R.drawable.story_placeholder)
                        .error(R.drawable.story_placeholder).into(ivSavdhani);
            }

            if (storyCounter == 0) {
                ivSavdhani.setVisibility(View.GONE);
                rlMain.setVisibility(View.VISIBLE);
                rlOne.setVisibility(View.GONE);
                rlSecond.setVisibility(View.GONE);
                rlThree.setVisibility(View.GONE);
                rlFour.setVisibility(View.GONE);
                rlFive.setVisibility(View.GONE);

                RecyclerView rcvProblem = (RecyclerView) view.findViewById(R.id.rcvSavdhani);
                rcvProblem.addItemDecoration(new GridSpacingItemDecoration(2, (int) Functions.convertDpToPixel(2, getActivity()), false));
                rcvProblem.setNestedScrollingEnabled(false);
                rcvProblem.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                rcvProblem.setAdapter(new HighRiskSavdhaniAdapter(highRiskData));

                if (counter > 0) {
                    ivSavdhani.setVisibility(View.VISIBLE);
                }
                if (counter == 1) {
                    rlMain.setVisibility(View.GONE);
                    rlOne.setVisibility(View.VISIBLE);
                    rlSecond.setVisibility(View.GONE);
                    rlThree.setVisibility(View.GONE);
                    rlFour.setVisibility(View.GONE);
                    rlFive.setVisibility(View.GONE);
                }
                if (counter == 2) {
                    rlMain.setVisibility(View.GONE);
                    rlOne.setVisibility(View.GONE);
                    rlSecond.setVisibility(View.GONE);
                    rlThree.setVisibility(View.VISIBLE);
                    rlFour.setVisibility(View.GONE);
                    rlFive.setVisibility(View.GONE);
                }
                if (counter == 3) {
                    rlMain.setVisibility(View.GONE);
                    rlOne.setVisibility(View.GONE);
                    rlSecond.setVisibility(View.GONE);
                    rlThree.setVisibility(View.GONE);
                    rlFour.setVisibility(View.VISIBLE);
                    rlFive.setVisibility(View.GONE);
                }
                if (counter == 4) {
                    rlMain.setVisibility(View.GONE);
                    rlOne.setVisibility(View.GONE);
                    rlSecond.setVisibility(View.GONE);
                    rlThree.setVisibility(View.GONE);
                    rlFour.setVisibility(View.GONE);
                    rlFive.setVisibility(View.VISIBLE);
                }

                if (tempList.size() == 3) {
                    if (counter == 2) {
                        rlMain.setVisibility(View.GONE);
                        rlOne.setVisibility(View.GONE);
                        rlSecond.setVisibility(View.GONE);
                        rlThree.setVisibility(View.GONE);
                        rlFour.setVisibility(View.GONE);
                        rlFive.setVisibility(View.VISIBLE);
                    }
                }

                if (tempList.size() == 4) {
                    if (counter == 3) {
                        rlMain.setVisibility(View.GONE);
                        rlOne.setVisibility(View.GONE);
                        rlSecond.setVisibility(View.GONE);
                        rlThree.setVisibility(View.GONE);
                        rlFour.setVisibility(View.GONE);
                        rlFive.setVisibility(View.VISIBLE);
                    }
                }

                if (tempList.size() == 5) {
                    if (counter == 4) {
                        rlMain.setVisibility(View.GONE);
                        rlOne.setVisibility(View.GONE);
                        rlSecond.setVisibility(View.GONE);
                        rlThree.setVisibility(View.GONE);
                        rlFour.setVisibility(View.GONE);
                        rlFive.setVisibility(View.VISIBLE);
                    }
                }

            }

            if (storyCounter > 0) {
                ivSavdhani.setVisibility(View.VISIBLE);

                if (counter == 0) {
                    rlMain.setVisibility(View.GONE);
                    rlOne.setVisibility(View.VISIBLE);
                    rlSecond.setVisibility(View.GONE);
                    rlThree.setVisibility(View.GONE);
                    rlFour.setVisibility(View.GONE);
                    rlFive.setVisibility(View.GONE);
                }
                if (counter == 1) {
                    rlMain.setVisibility(View.GONE);
                    rlOne.setVisibility(View.GONE);
                    rlSecond.setVisibility(View.VISIBLE);
                    rlThree.setVisibility(View.GONE);
                    rlFour.setVisibility(View.GONE);
                    rlFive.setVisibility(View.GONE);
                }

                if (tempList.size() == 3) {
                    if (counter == 2) {
                        rlMain.setVisibility(View.GONE);
                        rlOne.setVisibility(View.GONE);
                        rlSecond.setVisibility(View.GONE);
                        rlThree.setVisibility(View.GONE);
                        rlFour.setVisibility(View.GONE);
                        rlFive.setVisibility(View.VISIBLE);
                    }
                }

                if (tempList.size() == 4) {
                    if (counter == 2) {
                        rlMain.setVisibility(View.GONE);
                        rlOne.setVisibility(View.GONE);
                        rlSecond.setVisibility(View.GONE);
                        rlThree.setVisibility(View.VISIBLE);
                        rlFour.setVisibility(View.GONE);
                        rlFive.setVisibility(View.GONE);
                    }
                    if (counter == 3) {
                        rlMain.setVisibility(View.GONE);
                        rlOne.setVisibility(View.GONE);
                        rlSecond.setVisibility(View.GONE);
                        rlThree.setVisibility(View.GONE);
                        rlFour.setVisibility(View.GONE);
                        rlFive.setVisibility(View.VISIBLE);
                    }
                }

                if (tempList.size() == 5) {
                    if (counter == 2) {
                        rlMain.setVisibility(View.GONE);
                        rlOne.setVisibility(View.GONE);
                        rlSecond.setVisibility(View.GONE);
                        rlThree.setVisibility(View.VISIBLE);
                        rlFour.setVisibility(View.GONE);
                        rlFive.setVisibility(View.GONE);
                    }
                    if (counter == 3) {
                        rlMain.setVisibility(View.GONE);
                        rlOne.setVisibility(View.GONE);
                        rlSecond.setVisibility(View.GONE);
                        rlThree.setVisibility(View.GONE);
                        rlFour.setVisibility(View.VISIBLE);
                        rlFive.setVisibility(View.GONE);
                    }
                    if (counter == 4) {
                        rlMain.setVisibility(View.GONE);
                        rlOne.setVisibility(View.GONE);
                        rlSecond.setVisibility(View.GONE);
                        rlThree.setVisibility(View.GONE);
                        rlFour.setVisibility(View.GONE);
                        rlFive.setVisibility(View.VISIBLE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Drawable getDrawable(String image) {
        return Functions.getAssetsKeywordImage(getActivity(),
                Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + image);
    }

    public static int getDominantColor(Bitmap bitmap) {
        Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, 1, 1, true);
        final int color = newBitmap.getPixel(0, 0);
        newBitmap.recycle();
        return color;
    }

    @Override
    public void onNext() {
        counter = ++counter;
        setUpdatedCardView();
    }

    @Override
    public void onPrev() {
        if ((counter - 1) < 0) return;
        counter = --counter;
        setUpdatedCardView();
    }

    @Override
    public void onComplete() {
//        dismiss();
//        storyCounter = ++storyCounter;
//        setStoryCardView();
        Log.e("onComplete=", "=" + storyType);
    }

    @Override
    public void onDestroy() {
        if (storiesProgressView != null)
            storiesProgressView.destroy();
        super.onDestroy();
    }

    public List<WeeklyAdvisoryData> weeklyAdvisoryData() {
        List<String> generalAdvisory = new ArrayList<String>();
        List<String> advisoryImages = new ArrayList<String>();
        List<WeeklyAdvisoryData> weeklyAdvisoryData = new ArrayList<WeeklyAdvisoryData>();
        String weeklyAdvisory = "";
        try {
            weeklyAdvisory = sharedPreferences.getString(Constants.SharedPreferences_Weekly_Advisory, null);
            if (TextUtils.isEmpty(weeklyAdvisory)) {
                weeklyAdvisory = Functions.getAssetFileForWeeklyAdvisory(getActivity(), sharedPreferences);
            }
            if (!TextUtils.isEmpty(weeklyAdvisory)) {
                JSONObject jsonObject = new JSONObject(weeklyAdvisory);
                if (jsonObject != null && jsonObject.length() > 0) {
                    JSONObject weekly_advisory = jsonObject.optJSONObject("weekly_advisory");
                    if (weekly_advisory != null && weekly_advisory.length() > 0) {
                        String stage_name = weekly_advisory.optString("stage");
//                        image_path = weekly_advisory.optString("image_path");

                        JSONArray jsonArray = weekly_advisory.optJSONArray("general_advice");
                        String g_str = "";
                        if (jsonArray != null && jsonArray.length() > 0) {
                            g_str = jsonArray.toString();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                if (!TextUtils.isEmpty(jsonArray.getString(i))) {
                                    generalAdvisory.add(jsonArray.getString(i));
                                }
                            }
                        }

                        JSONArray advisory_image = weekly_advisory.optJSONArray("advisory_image");
                        String ad_img = "";
                        if (advisory_image != null && advisory_image.length() > 0) {
                            ad_img = advisory_image.toString();
                            for (int i = 0; i < advisory_image.length(); i++) {
                                advisoryImages.add(advisory_image.optString(i));
                            }
                        }

                        JSONArray disease = weekly_advisory.optJSONArray("disease");
                        String dis = "";
                        List<DiasesData> diasesData = new ArrayList<>();
                        if (disease != null && disease.length() > 0) {
                            dis = disease.toString();
                            for (int i = 0; i < disease.length(); i++) {
                                JSONObject jd = disease.optJSONObject(i);
                                String name = jd.optString("name");
                                String keyword_id = jd.optString("keyword_id");

                                diasesData.add(new DiasesData(name, keyword_id));
                            }
                        }

                        JSONArray pest = weekly_advisory.optJSONArray("pest");
                        List<PestData> pestData = new ArrayList<>();
                        String pst = "";
                        if (pest != null && pest.length() > 0) {
                            pst = pest.toString();
                            for (int i = 0; i < pest.length(); i++) {
                                JSONObject jd = pest.optJSONObject(i);
                                String name = jd.optString("name");
                                String keyword_id = jd.optString("keyword_id");

                                pestData.add(new PestData(name, keyword_id));
                            }
                        }

                        JSONArray nutrition = weekly_advisory.optJSONArray("nutrition");
                        List<NutritionData> nutritionData = new ArrayList<>();
                        String nut = "";
                        if (nutrition != null && nutrition.length() > 0) {
                            nut = nutrition.toString();
                            for (int i = 0; i < nutrition.length(); i++) {
                                JSONObject jd = nutrition.optJSONObject(i);
                                String name = jd.optString("name");
                                String keyword_id = jd.optString("keyword_id");
                                nutritionData.add(new NutritionData(name, keyword_id));
                            }
                        }

                        weeklyAdvisoryData.add(new WeeklyAdvisoryData(0, RongoApp.getCropWeeksFunction(), RongoApp.getSelectedCrop(),
                                RongoApp.getSelectedPCrop(), stage_name, image_path, g_str, ad_img, "", dis, pst, nut, ""));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return weeklyAdvisoryData;
    }


    public class HighRiskSavdhaniAdapter extends RecyclerView.Adapter<HighRiskSavdhaniAdapter.ViewHolder> {

        List<SavdhaniDataList> listdata;

        public HighRiskSavdhaniAdapter(List<SavdhaniDataList> listdata) {
            this.listdata = listdata;
        }

        @Override
        public HighRiskSavdhaniAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.high_risk_savdhani_story_list_item, parent, false);
            HighRiskSavdhaniAdapter.ViewHolder viewHolder = new HighRiskSavdhaniAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final HighRiskSavdhaniAdapter.ViewHolder holder, final int position) {
            try {
                holder.tvTitle.setText(listdata.get(position).getName());

                if (listdata.get(position).getStatus().equalsIgnoreCase("1")) {
                    holder.tvTitle.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.diea, 0, 0, 0);
                } else if (listdata.get(position).getStatus().equalsIgnoreCase("2")) {
                    holder.tvTitle.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.pes, 0, 0, 0);
                } else if (listdata.get(position).getStatus().equalsIgnoreCase("3")) {
                    holder.tvTitle.setCompoundDrawablesWithIntrinsicBounds(R.mipmap.nutr, 0, 0, 0);
                }

                holder.tvType.setText(holder.itemView.getContext().getString(R.string.str_high_risk));
                holder.tvType.setCompoundDrawablesWithIntrinsicBounds(holder.itemView.getContext().getDrawable(R.mipmap.alert), null, null, null);

                Drawable drawable = Functions.getAssetsKeywordImage(getActivity(),
                        Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH +
                                RongoApp.getSelectedCrop() + "/" + listdata.get(position).getBitmap());

                GlideApp.with(getActivity()).load(image_path +
                        RongoApp.getSelectedCrop() + "/" + listdata.get(position).getBitmap())
                        .error(drawable).placeholder(drawable).into(holder.ivIcon);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            if (listdata.size() > 5) {
                return 6;
            } else {
                return listdata.size();
            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvTitle, tvType;
            ImageView ivIcon;
            RelativeLayout rlMainView;

            public ViewHolder(View itemView) {
                super(itemView);
                this.rlMainView = (RelativeLayout) itemView.findViewById(R.id.rlMainView);
                this.tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
                this.tvType = (TextView) itemView.findViewById(R.id.tvType);
                this.ivIcon = (ImageView) itemView.findViewById(R.id.ivIcon);
            }
        }

    }

}