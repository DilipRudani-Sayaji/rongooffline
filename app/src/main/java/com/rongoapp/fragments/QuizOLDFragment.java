package com.rongoapp.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.card.MaterialCardView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.makeramen.roundedimageview.RoundedImageView;
import com.rongoapp.R;
import com.rongoapp.activities.QuizActivity;
import com.rongoapp.activities.RewardActivity;
import com.rongoapp.activities.SavdhaniDetailsActivity;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.FetchJsonFromAssetsFile;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.DailyQuizStatusData;
import com.rongoapp.data.OfflineQuizData;
import com.rongoapp.data.QuizAnswerListModel;
import com.rongoapp.data.StateCityDataModel;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.Log;
import com.rongoapp.views.ProgressBarDialog;
import com.rongoapp.views.SuccessGifView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuizOLDFragment extends DialogFragment {

    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    View view;
    Activity activity;

    ArrayList<String> answersList = new ArrayList<>();
    String isAnswer, type = "", keyword_id = "", image = "", QuizId = "", totalAns = "";
    boolean isAnswerValid = false;

    NestedScrollView nestedScroll;
    RoundedImageView ivQuestion, ivPuzzle;
    ImageView ivBack, ivOne, ivTwo, ivThree, ivFour, ivFive, ivSix, ivSeven, ivEight, ivNine;
    TextView tvTitle, txtQuestion, tvOne, tvTwo, tvThree, tvFour, txtQuestion1, tvNoDataAvailable;
    LinearLayout llAnswerView, llPart1, llPart2, llCropSelector, llSeeAdvisory;
    RelativeLayout rlLeaderBoard;
    Button btnNext;
    ArrayList<StateCityDataModel> stateCityDataModels = new ArrayList<StateCityDataModel>();
    ArrayList<QuizAnswerListModel> quizAnswerListModels = new ArrayList<>();

    @Override
    public int getTheme() {
        return R.style.FullScreenDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.FullScreenDialog);
        activity = getActivity();
        sharedPreferences = getActivity().getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(getActivity());
        RongoApp.engagementToolActivity(Constants.ET_QUIZ);
        RongoApp.logVisitedPageTabEvent("Old Quiz");
        Functions.setFirebaseLogEventTrack(getActivity(), Constants.ET_STORY_TRACK, Constants.ET_QUIZ,
                "" + RongoApp.getUserId(), Constants.ET_QUIZ + "_" + RongoApp.getUserId());
        Functions.setUserScreenTrackEvent(appDatabase, Constants.ET_STORY_TRACK, Constants.ET_QUIZ + "_" + RongoApp.getUserId());
        Log.e("Quiz", "Old");
    }

    public static QuizOLDFragment newInstance() {
        QuizOLDFragment fragment = new QuizOLDFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_quiz_old_dialog, container, false);

        answersList = new ArrayList<>();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        TextView tvBack = view.findViewById(R.id.tvBack);
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(Constants.STORY_NAME, Constants.STORY);
                intent.setAction(Constants.BROADCAST_RECEIVER.UPDATE_STORY);
                getActivity().sendBroadcast(intent);
                dismiss();
            }
        });
        TextView tvNext = view.findViewById(R.id.tvNext);
        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(Constants.STORY_NAME, Constants.STORY_SAVDHANI);
                intent.setAction(Constants.BROADCAST_RECEIVER.UPDATE_STORY);
                getActivity().sendBroadcast(intent);
                dismiss();
            }
        });

        setInitView();
        setListner();
        return view;
    }

    void setInitView() {
        ivBack = (ImageView) view.findViewById(R.id.ivClose);
        ivQuestion = (RoundedImageView) view.findViewById(R.id.ivQuestion);
        ivPuzzle = (RoundedImageView) view.findViewById(R.id.ivPuzzle);

        ivOne = (ImageView) view.findViewById(R.id.ivOne);
        ivTwo = (ImageView) view.findViewById(R.id.ivTwo);
        ivThree = (ImageView) view.findViewById(R.id.ivThree);
        ivFour = (ImageView) view.findViewById(R.id.ivFour);
        ivFive = (ImageView) view.findViewById(R.id.ivFive);
        ivSix = (ImageView) view.findViewById(R.id.ivSix);
        ivSeven = (ImageView) view.findViewById(R.id.ivSeven);
        ivEight = (ImageView) view.findViewById(R.id.ivEight);
        ivNine = (ImageView) view.findViewById(R.id.ivNine);

        tvTitle = (TextView) view.findViewById(R.id.tvTitle);

        llAnswerView = (LinearLayout) view.findViewById(R.id.llAnswerView);
        llPart1 = (LinearLayout) view.findViewById(R.id.llPart1);
        llPart2 = (LinearLayout) view.findViewById(R.id.llPart2);

        llCropSelector = (LinearLayout) view.findViewById(R.id.llCropSelector);
        llCropSelector.setVisibility(View.GONE);

        llSeeAdvisory = (LinearLayout) view.findViewById(R.id.llSeeAdvisory);

        txtQuestion = (TextView) view.findViewById(R.id.txtQuestion);
        txtQuestion1 = (TextView) view.findViewById(R.id.txtQuestion1);
        tvOne = (TextView) view.findViewById(R.id.tvOne);
        tvTwo = (TextView) view.findViewById(R.id.tvTwo);
        tvThree = (TextView) view.findViewById(R.id.tvThree);
        tvFour = (TextView) view.findViewById(R.id.tvFour);

        btnNext = (Button) view.findViewById(R.id.btnNext);

        nestedScroll = (NestedScrollView) view.findViewById(R.id.nestedScroll);

        rlLeaderBoard = view.findViewById(R.id.rlLeaderBoard);
        rlLeaderBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getQuizLeaderboard();
            }
        });

        tvNoDataAvailable = (TextView) view.findViewById(R.id.tvNoDataAvailable);
        int cropWeek = RongoApp.getCropWeekDaysFunction();
        if (cropWeek > 0 && cropWeek < Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"))) {
            List<DailyQuizStatusData> dailyQuizStatus = appDatabase.dailyQuizStatusDao().getDailyQuizStatus(
                    sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0"),
                    sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, "0"),
                    sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, "0"));
            if (dailyQuizStatus != null && dailyQuizStatus.size() >= 1) {
                btnNext.setAlpha(0.2f);
                btnNext.setEnabled(false);
//                tvAdviceTxt.setText(R.string.str_quiz_redirect_msg);
                tvNoDataAvailable.setVisibility(View.VISIBLE);
                rlLeaderBoard.setVisibility(View.VISIBLE);
                nestedScroll.setVisibility(View.GONE);
            } else {
                btnNext.setAlpha(1f);
                btnNext.setEnabled(true);
//                tvAdviceTxt.setText(R.string.str_ques_answer_is_salah_me_he);
                tvNoDataAvailable.setVisibility(View.GONE);
                rlLeaderBoard.setVisibility(View.GONE);
                nestedScroll.setVisibility(View.VISIBLE);
            }
        } else {
            btnNext.setVisibility(View.GONE);
            tvNoDataAvailable.setVisibility(View.VISIBLE);
            rlLeaderBoard.setVisibility(View.VISIBLE);
        }

        llCropSelector.setVisibility(View.GONE);
        llAnswerView.setVisibility(View.VISIBLE);
        llPart1.setVisibility(View.VISIBLE);
        llPart2.setVisibility(View.GONE);
        ivQuestion.setVisibility(View.VISIBLE);
        setQuizData();
        parseStateCityListData();
    }

    void setListner() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        llSeeAdvisory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        tvOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answersList = new ArrayList<>();
                answersList.add("0");
                setAnswerSelection("1");
            }
        });

        tvTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answersList = new ArrayList<>();
                answersList.add("1");
                setAnswerSelection("2");
            }
        });

        tvThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answersList = new ArrayList<>();
                answersList.add("2");
                setAnswerSelection("3");
            }
        });

        tvFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answersList = new ArrayList<>();
                answersList.add("3");
                setAnswerSelection("4");
            }
        });

        ivOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(1);
            }
        });

        ivTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(2);
            }
        });

        ivThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(3);
            }
        });

        ivFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(4);
            }
        });

        ivFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(5);
            }
        });

        ivSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(6);
            }
        });

        ivSeven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(7);
            }
        });

        ivEight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(8);
            }
        });

        ivNine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSelection(9);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAnswerCheck(answersList);
//                showDialog();
                showSuccessDialog();
            }
        });
    }

    void setSelection(int answer) {
        if (answersList.size() > 0) {
            if (answersList.contains(String.valueOf(answer))) {
                answersList.remove(String.valueOf(answer));
            } else {
                answersList.add(String.valueOf(answer));
            }
        } else {
            answersList = new ArrayList<>();
            answersList.add(String.valueOf(answer));
        }

        ivOne.setImageResource(0);
        ivTwo.setImageResource(0);
        ivThree.setImageResource(0);
        ivFour.setImageResource(0);
        ivFive.setImageResource(0);
        ivSix.setImageResource(0);
        ivSeven.setImageResource(0);
        ivEight.setImageResource(0);
        ivNine.setImageResource(0);
        ivOne.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivTwo.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivThree.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivFour.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivFive.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivSix.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivSeven.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivEight.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivNine.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));

        for (int i = 0; i < answersList.size(); i++) {
            if (answersList.get(i).equalsIgnoreCase("1")) {
                ivOne.setImageResource(R.mipmap.q_tick);
                ivOne.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("2")) {
                ivTwo.setImageResource(R.mipmap.q_tick);
                ivTwo.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("3")) {
                ivThree.setImageResource(R.mipmap.q_tick);
                ivThree.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("4")) {
                ivFour.setImageResource(R.mipmap.q_tick);
                ivFour.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("5")) {
                ivFive.setImageResource(R.mipmap.q_tick);
                ivFive.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("6")) {
                ivSix.setImageResource(R.mipmap.q_tick);
                ivSix.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("7")) {
                ivSeven.setImageResource(R.mipmap.q_tick);
                ivSeven.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("8")) {
                ivEight.setImageResource(R.mipmap.q_tick);
                ivEight.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("9")) {
                ivNine.setImageResource(R.mipmap.q_tick);
                ivNine.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
        }
    }

    void setAnswerSelection(String pos) {
        answersList = new ArrayList<>();
        answersList.add(String.valueOf(pos));
        if (pos.equalsIgnoreCase("1")) {
//            tvOne.setBackground(getResources().getDrawable(R.drawable.rounded_box_orange_border));
            tvOne.setBackground(getResources().getDrawable(R.drawable.rounded_box_select));
            tvTwo.setBackground(getResources().getDrawable(R.drawable.rounded_box));
            tvThree.setBackground(getResources().getDrawable(R.drawable.rounded_box));
            tvFour.setBackground(getResources().getDrawable(R.drawable.rounded_box));
        } else if (pos.equalsIgnoreCase("2")) {
            tvOne.setBackground(getResources().getDrawable(R.drawable.rounded_box));
            tvTwo.setBackground(getResources().getDrawable(R.drawable.rounded_box_select));
            tvThree.setBackground(getResources().getDrawable(R.drawable.rounded_box));
            tvFour.setBackground(getResources().getDrawable(R.drawable.rounded_box));
        } else if (pos.equalsIgnoreCase("3")) {
            tvOne.setBackground(getResources().getDrawable(R.drawable.rounded_box));
            tvTwo.setBackground(getResources().getDrawable(R.drawable.rounded_box));
            tvThree.setBackground(getResources().getDrawable(R.drawable.rounded_box_select));
            tvFour.setBackground(getResources().getDrawable(R.drawable.rounded_box));
        } else if (pos.equalsIgnoreCase("4")) {
            tvOne.setBackground(getResources().getDrawable(R.drawable.rounded_box));
            tvTwo.setBackground(getResources().getDrawable(R.drawable.rounded_box));
            tvThree.setBackground(getResources().getDrawable(R.drawable.rounded_box));
            tvFour.setBackground(getResources().getDrawable(R.drawable.rounded_box_select));
        }
    }

    boolean isAnswerCheck(ArrayList<String> answer) {
        isAnswerValid = false;
        String[] allIdsArray = TextUtils.split(isAnswer, ",");
        ArrayList<String> idsList = new ArrayList<String>(Arrays.asList(allIdsArray));
        for (String next : idsList) {
            Log.e("Result", next + " - " + answer);
            if (answer.contains(next)) {
                isAnswerValid = true;
                break;
            }
        }
        return isAnswerValid;
    }


    void setQuizData() {
        try {
            JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.getDailyQuizDataJSONFromAsset(activity));
            if (jsonObject != null && jsonObject.length() > 0) {
                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                    JSONObject data = jsonObject.optJSONObject("data");
                    if (data != null && data.length() > 0) {
                        JSONObject jsonObj = data.optJSONObject("result");
                        if (jsonObj != null && jsonObj.length() > 0) {
                            QuizId = jsonObj.getString("id");
                            JSONObject json_data = jsonObj.getJSONObject("json_data");
//                            Log.e("quiz==", "" + json_data);
                            if (json_data != null && json_data.length() > 0) {
                                Iterator<String> keys = json_data.keys();
                                while (keys.hasNext()) {
                                    String key = keys.next();
                                    String value = json_data.optString(key);
                                    if (key.equalsIgnoreCase(RongoApp.getSelectedCrop())) {
                                        JSONObject jsonLang = new JSONObject(value);
                                        if (jsonLang != null && jsonLang.length() > 0) {
                                            Iterator<String> keyLang = jsonLang.keys();
                                            while (keyLang.hasNext()) {
                                                String key1 = keyLang.next();
                                                String value1 = jsonLang.optString(key1);
                                                if (key1.equalsIgnoreCase(RongoApp.getSeasonName())) {
                                                    JSONObject jsonLang1 = new JSONObject(value1);
                                                    if (jsonLang1 != null && jsonLang1.length() > 0) {
                                                        Iterator<String> keyLang1 = jsonLang1.keys();
                                                        while (keyLang1.hasNext()) {
                                                            String key2 = keyLang1.next();
                                                            String value2 = jsonLang1.optString(key2);
                                                            if (key2.equalsIgnoreCase(RongoApp.getDefaultLanguage())) {
                                                                JSONArray jsonArray = new JSONArray(value2);
                                                                JSONObject resultData = jsonArray.getJSONObject(Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "2")) - 1);
                                                                JSONObject jsonObjects = resultData.getJSONObject(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, "0"));
//                                                                JSONObject resultData = jsonArray.getJSONObject(4);
//                                                                JSONObject jsonObjects = resultData.getJSONObject("4");

                                                                type = jsonObjects.optString("type");
                                                                keyword_id = jsonObjects.optString("keyword_id");
                                                                isAnswer = jsonObjects.optString("answer");
                                                                String question = jsonObjects.optString("question");
                                                                txtQuestion.setText("" + question);
                                                                txtQuestion1.setText("" + question);
                                                                image = jsonObjects.optString("image");

                                                                if (!type.equalsIgnoreCase("puzzle")) {
                                                                    llCropSelector.setVisibility(View.GONE);
                                                                    llAnswerView.setVisibility(View.VISIBLE);
                                                                    ivQuestion.setVisibility(View.VISIBLE);
                                                                    txtQuestion.setVisibility(View.VISIBLE);

                                                                    JSONArray array = jsonObjects.optJSONArray("options");
                                                                    if (array != null && array.length() > 0) {
                                                                        List<String> list = new Gson().fromJson(array.toString(), new TypeToken<List<String>>() {
                                                                        }.getType());
                                                                        llPart2.setVisibility(View.GONE);
                                                                        tvOne.setText(" (A)  " + list.get(0));
                                                                        tvTwo.setText(" (B)  " + list.get(1));
                                                                        if (list.size() > 2) {
                                                                            if (!TextUtils.isEmpty(list.get(2))) {
                                                                                llPart2.setVisibility(View.VISIBLE);
                                                                                tvThree.setText(" (C)  " + list.get(2));
                                                                                tvFour.setText(" (D)  " + list.get(3));
                                                                            }
                                                                        }
                                                                    }
                                                                } else {
                                                                    llCropSelector.setVisibility(View.VISIBLE);
                                                                    llAnswerView.setVisibility(View.GONE);
                                                                    ivQuestion.setVisibility(View.GONE);
                                                                    txtQuestion.setVisibility(View.GONE);
                                                                }

                                                                if (!TextUtils.isEmpty(type)) {
                                                                    if (!type.equalsIgnoreCase("puzzle")) {
                                                                        ivQuestion.setVisibility(View.VISIBLE);
                                                                        ivPuzzle.setVisibility(View.GONE);
                                                                    } else {
                                                                        ivPuzzle.setVisibility(View.VISIBLE);
                                                                        ivQuestion.setVisibility(View.GONE);
                                                                    }

                                                                    if (!TextUtils.isEmpty(keyword_id)) {
                                                                        ivQuestion.setImageDrawable(Functions.getAssetsCropImage(activity,
                                                                                Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + image));

                                                                        ivPuzzle.setImageDrawable(Functions.getAssetsCropImage(activity,
                                                                                Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + image));
                                                                    } else {
                                                                        ivQuestion.setImageDrawable(Functions.getAssetsCropImage(activity,
                                                                                Constants.ASSETS_ADVISORY_CROP_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + image));

                                                                        ivPuzzle.setImageDrawable(Functions.getAssetsCropImage(activity,
                                                                                Constants.ASSETS_ADVISORY_CROP_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + image));
                                                                    }
                                                                } else {
                                                                    ivQuestion.setVisibility(View.GONE);
                                                                    ivPuzzle.setVisibility(View.GONE);
                                                                    llCropSelector.setVisibility(View.GONE);
                                                                }
                                                                Log.e("isAnswer==", "" + isAnswer);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void showDialog() {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(activity);
        View sheetView = this.getLayoutInflater().inflate(R.layout.bottom_quiz_thanks_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        AppCompatTextView txtOfflineSMS = sheetView.findViewById(R.id.txtOfflineSMS);
        txtOfflineSMS.setVisibility(View.GONE);
        if (InternetConnection.checkConnectionForFasl(activity)) {
            txtOfflineSMS.setVisibility(View.GONE);
        } else {
            txtOfflineSMS.setVisibility(View.VISIBLE);
        }

        ImageView ivTagImage = (ImageView) sheetView.findViewById(R.id.ivTagImage);
        TextView tvTagText = (TextView) sheetView.findViewById(R.id.tvTagText);
        TextView tvTagDecs = (TextView) sheetView.findViewById(R.id.tvTagDecs);

        LinearLayout llRewardView = (LinearLayout) sheetView.findViewById(R.id.llRewardView);

        MaterialCardView cardKeyword = (MaterialCardView) sheetView.findViewById(R.id.cardKeyword);
        cardKeyword.setVisibility(View.GONE);
        RelativeLayout rlShareView = (RelativeLayout) sheetView.findViewById(R.id.rlShareView);
        rlShareView.setVisibility(View.GONE);

        TextView tvRewardNew = (TextView) sheetView.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));

        RoundedImageView ivKeyword = (RoundedImageView) sheetView.findViewById(R.id.ivKeyword);
        TextView tvKeywordText = (TextView) sheetView.findViewById(R.id.tvKeywordText);

        if (!TextUtils.isEmpty(keyword_id)) {
            rlShareView.setVisibility(View.GONE);
            cardKeyword.setVisibility(View.VISIBLE);

            ivKeyword.setImageDrawable(Functions.getAssetsCropImage(activity,
                    Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + image));
            try {
                JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.
                        loadAdvisoryKeywordJSONFromAsset(activity, keyword_id));
                if (jsonObject != null && jsonObject.length() > 0) {
                    String name = jsonObject.optString("name");

                    Spannable wordtoSpan = new SpannableString(name + getString(R.string.str_more_info_text));
                    wordtoSpan.setSpan(new ForegroundColorSpan(Color.rgb(255, 69, 0)), 0, name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tvKeywordText.setText(wordtoSpan);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlShareView.setVisibility(View.VISIBLE);
            cardKeyword.setVisibility(View.GONE);
        }

        cardKeyword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity, SavdhaniDetailsActivity.class);
                i.putExtra("keyword_id", keyword_id);
                startActivity(i);
                dismiss();
            }
        });

        if (isAnswerValid) {
            llRewardView.setVisibility(View.VISIBLE);
            ivTagImage.setImageResource(R.mipmap.quiz_yes);
            tvTagText.setText(R.string.str_aapka_utar_sahi_he);
            tvTagText.setTextColor(getResources().getColor(R.color.chat_text));
            tvTagDecs.setVisibility(View.GONE);
            tvTagDecs.setText(R.string.str_aaj_10_reward_jite_he);

            updateQuizResult("1");
        } else {
            llRewardView.setVisibility(View.GONE);
            ivTagImage.setImageResource(R.mipmap.quiz_no);
            tvTagText.setText(R.string.str_utar_galt_he);
            tvTagText.setTextColor(getResources().getColor(R.color.bunai_yes));
            tvTagDecs.setVisibility(View.VISIBLE);
            tvTagDecs.setText(R.string.str_jankari_ke_liye_salah_pade);

            updateQuizResult("0");
        }

        AppCompatTextView tvShareWhatapp = sheetView.findViewById(R.id.tvShareWhatapp);
        tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.shareWhatAppIntent(activity);
            }
        });

        AppCompatTextView tvShareCopy = sheetView.findViewById(R.id.tvShareCopy);
        tvShareCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.shareSMSIntent(activity, RongoApp.getReferralText(activity));
            }
        });

        AppCompatTextView tvSharefacebook = sheetView.findViewById(R.id.tvSharefacebook);
        tvSharefacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.shareFacebookIntent(activity);
            }
        });

        AppCompatTextView tvShareMore = sheetView.findViewById(R.id.tvShareMore);
        tvShareMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.performShare(activity, appDatabase);
            }
        });

        ImageView ivClose = (ImageView) sheetView.findViewById(R.id.ivClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
                dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        ((View) sheetView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();
    }

    void showSuccessDialog() {
        Dialog sheetView = new Dialog(getActivity(), R.style.NewDialog);
        sheetView.setContentView(R.layout.bottom_quiz_thanks_new_dialog);
        sheetView.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = sheetView.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);

        ImageView ivClose = (ImageView) sheetView.findViewById(R.id.ivClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                sheetView.dismiss();
            }
        });

        AppCompatTextView txtOfflineSMS = sheetView.findViewById(R.id.txtOfflineSMS);
        txtOfflineSMS.setVisibility(View.GONE);
        if (InternetConnection.checkConnectionForFasl(getActivity())) {
            txtOfflineSMS.setVisibility(View.GONE);
        } else {
            txtOfflineSMS.setVisibility(View.VISIBLE);
        }

        SuccessGifView ivSuccessGif = (SuccessGifView) sheetView.findViewById(R.id.ivSuccessGif);
        ImageView ivTagImage = (ImageView) sheetView.findViewById(R.id.ivTagImage);
        TextView tvTagText = (TextView) sheetView.findViewById(R.id.tvTagText);
        TextView tvTagDecs = (TextView) sheetView.findViewById(R.id.tvTagDecs);

        LinearLayout llRewardView = (LinearLayout) sheetView.findViewById(R.id.llRewardView);

        MaterialCardView cardKeyword = (MaterialCardView) sheetView.findViewById(R.id.cardKeyword);
        cardKeyword.setVisibility(View.GONE);
        cardKeyword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), SavdhaniDetailsActivity.class);
                i.putExtra("keyword_id", keyword_id);
                startActivity(i);
                dismiss();
            }
        });

        LinearLayout cardSavdhani = (LinearLayout) sheetView.findViewById(R.id.cardSavdhani);
        cardSavdhani.setVisibility(View.GONE);
        cardSavdhani.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), RewardActivity.class);
                startActivity(intent);
                sheetView.dismiss();
                dismiss();
            }
        });

        RelativeLayout rlShareView = (RelativeLayout) sheetView.findViewById(R.id.rlShareView);
        rlShareView.setVisibility(View.GONE);

        LinearLayout llSeeAdvisory = (LinearLayout) sheetView.findViewById(R.id.llSeeAdvisory);
        llSeeAdvisory.setVisibility(View.GONE);
        llSeeAdvisory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sheetView.dismiss();
                dismiss();
            }
        });

        RoundedImageView ivKeyword = (RoundedImageView) sheetView.findViewById(R.id.ivKeyword);
        TextView tvKeywordText = (TextView) sheetView.findViewById(R.id.tvKeywordText);

        if (isAnswerValid) {
            cardSavdhani.setVisibility(View.VISIBLE);
            rlShareView.setVisibility(View.GONE);
            cardKeyword.setVisibility(View.GONE);
            llRewardView.setVisibility(View.VISIBLE);
            ivSuccessGif.setVisibility(View.VISIBLE);
            ivTagImage.setVisibility(View.GONE);
            tvTagText.setText(R.string.str_aapka_utar_sahi_he);
//            tvTagText.setTextColor(getResources().getColor(R.color.chat_text));
            tvTagDecs.setVisibility(View.GONE);
            tvTagDecs.setText(R.string.str_aaj_10_reward_jite_he);

            updateQuizResult("1");
        } else {
            cardSavdhani.setVisibility(View.GONE);
            llRewardView.setVisibility(View.GONE);
            ivSuccessGif.setVisibility(View.GONE);
            ivTagImage.setVisibility(View.VISIBLE);
            ivTagImage.setImageResource(R.mipmap.quiz_no);
            tvTagText.setText(R.string.str_utar_galt_he);
//            tvTagText.setTextColor(getResources().getColor(R.color.bunai_yes));
            tvTagDecs.setVisibility(View.VISIBLE);
            tvTagDecs.setText(R.string.str_get_more_reward);

            updateQuizResult("0");

            if (!TextUtils.isEmpty(keyword_id)) {
                llSeeAdvisory.setVisibility(View.GONE);
                cardKeyword.setVisibility(View.VISIBLE);
                try {
                    ivKeyword.setImageDrawable(Functions.getAssetsCropImage(getActivity(),
                            Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + image));

                    JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.
                            loadAdvisoryKeywordJSONFromAsset(getActivity(), keyword_id));
                    if (jsonObject != null && jsonObject.length() > 0) {
                        String name = jsonObject.optString("name");
                        Spannable wordtoSpan = new SpannableString(name + getString(R.string.str_bare_text));
                        wordtoSpan.setSpan(new ForegroundColorSpan(Color.rgb(255, 69, 0)), 0, name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tvKeywordText.setText(wordtoSpan);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                llSeeAdvisory.setVisibility(View.VISIBLE);
                cardKeyword.setVisibility(View.GONE);
            }
        }
    }


    void updateQuizResult(final String status) {
        try {
            Functions.setFirebaseLogEventTrack(activity, Constants.Click_Event, "Daily Quiz", status, "updateQuizResult");

            String answer = answersList.toString().replace("[", "").replace("]", "");

            if (InternetConnection.checkConnectionForFasl(activity)) {
                RetrofitClient.getInstance().getApi().updateDailyQuizResponse(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                        RongoApp.getAppVersion(), SharedPreferencesUtils.getUserId(activity),
                        Functions.getDeviceid(activity), QuizId, RongoApp.getSelectedCrop(),
                        RongoApp.getSelectedPCrop(), sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0"),
                        sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, "0"),
                        answer, status, getDailyQuizRewardPoint(status)).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response != null && response.isSuccessful()) {
                            setDailyQuizStatus(status);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
            } else {
                setDailyQuizStatus(status);

                ArrayList<OfflineQuizData> offlineQuizData = new ArrayList<OfflineQuizData>();
                OfflineQuizData offlineQuiz = new OfflineQuizData();
                offlineQuiz.set_id(0);
                offlineQuiz.setQuizID(QuizId);
                offlineQuiz.setCropId(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""));
                offlineQuiz.setPcropId(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""));
                offlineQuiz.setCropWeek(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""));
                offlineQuiz.setCropDay(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, ""));
                offlineQuiz.setAnsValue(answer);
                offlineQuiz.setIs_true(status);
                offlineQuiz.setMetaData(getDailyQuizRewardPoint(status));
                offlineQuizData.add(offlineQuiz);
                appDatabase.offlineQuizDao().insertAll(offlineQuizData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setDailyQuizStatus(String status) {
        try {
            String answer = answersList.toString().replace("[", "").replace("]", "");
            ArrayList<DailyQuizStatusData> dailyQuizStatusData = new ArrayList<DailyQuizStatusData>();
            DailyQuizStatusData dailyQuizStatus = new DailyQuizStatusData();
            dailyQuizStatus.set_id(0);
            dailyQuizStatus.setQuizID(QuizId);
            dailyQuizStatus.setCropId(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""));
            dailyQuizStatus.setPcropId(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""));
            dailyQuizStatus.setCropWeek(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""));
            dailyQuizStatus.setCropDay(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, ""));
            dailyQuizStatus.setAnsValue(answer);
            dailyQuizStatus.setMetaData(getDailyQuizRewardPoint(status));
            dailyQuizStatusData.add(dailyQuizStatus);
            appDatabase.dailyQuizStatusDao().insertAll(dailyQuizStatusData);

            Intent intent = new Intent();
            intent.putExtra(Constants.STORY_NAME, Constants.STORY_TEST);
            intent.setAction(Constants.BROADCAST_RECEIVER.UPDATE_STORY);
            getActivity().sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String getDailyQuizRewardPoint(String status) {
        String rewardPoint = "";
        try {
            if (status.equalsIgnoreCase("1")) {
                JSONArray jsonArray = new JSONArray();
                JSONObject obj = new JSONObject();
                obj.put("type", "point");
                obj.put("value", RewardPoints.getDailyQuizRewardPoint(sharedPreferences));
                jsonArray.put(obj);
                rewardPoint = jsonArray.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rewardPoint;
    }

    void getQuizLeaderboard() {
        try {
            if (InternetConnection.checkConnection(getActivity())) {

                ProgressBarDialog progressBarDialog = new ProgressBarDialog(getActivity());
                progressBarDialog.showProgressDialogWithTitle(getResources().getString(R.string.app_name));

                RetrofitClient.getInstance().getApi().getQuizLeaderboard(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                        SharedPreferencesUtils.getUserId(getActivity())).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response != null) {
                            try {
                                quizAnswerListModels = new ArrayList<QuizAnswerListModel>();
                                String responseStrig = response.body().string();
                                if (!TextUtils.isEmpty(responseStrig)) {
                                    JSONObject jb = new JSONObject(responseStrig);
                                    if (jb != null && jb.length() > 0) {
                                        if (jb.optString("status_code").equalsIgnoreCase("0")) {
                                            JSONObject jData = jb.optJSONObject("data");
                                            if (jData != null && jData.length() > 0) {
                                                totalAns = jData.optString("user");
                                                JSONArray jallResult = jData.optJSONArray("all");
                                                if (jallResult != null && jallResult.length() > 0) {
                                                    for (int i = 0; i < jallResult.length(); i++) {
                                                        JSONObject jBall = jallResult.optJSONObject(i);
                                                        String user_id = jBall.optString("user_id");
                                                        String first_name = jBall.optString("first_name");
                                                        String location_city = jBall.optString("location_city");
                                                        String location_state = jBall.optString("location_state");
                                                        String total = jBall.optString("total");
                                                        if (!TextUtils.isEmpty(first_name) && !first_name.equalsIgnoreCase("null")) {
                                                            quizAnswerListModels.add(new QuizAnswerListModel(user_id,
                                                                    first_name, location_state, location_city, total));
                                                        }
                                                    }
                                                }

                                                progressBarDialog.hideProgressDialogWithTitle();
                                                LeaderBoardDialog();
                                            }
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                progressBarDialog.hideProgressDialogWithTitle();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        progressBarDialog.hideProgressDialogWithTitle();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void LeaderBoardDialog() {
        try {
            Dialog dialog = new Dialog(getActivity(), R.style.NewDialog);
            dialog.setContentView(R.layout.leader_board_dialog);
            dialog.show();

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(lp);

            ImageView ivBack = dialog.findViewById(R.id.ivBack);
            ivBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            ImageView iv100 = dialog.findViewById(R.id.iv100);
            ImageView iv75 = dialog.findViewById(R.id.iv75);
            ImageView iv50 = dialog.findViewById(R.id.iv50);
            ImageView iv25 = dialog.findViewById(R.id.iv25);

            TextView tvTotalPoints = (TextView) dialog.findViewById(R.id.tvTotalPoints);
            TextView tvTotalAns = (TextView) dialog.findViewById(R.id.tvTotalAns);
            tvTotalAns.setText("" + totalAns);
            tvTotalPoints.setText("" + (Integer.parseInt(totalAns) * 10));

            if (Integer.parseInt(totalAns) >= 25) {
                iv25.setImageResource(R.mipmap.r1);
            }
            if (Integer.parseInt(totalAns) >= 50) {
                iv50.setImageResource(R.mipmap.r2);
            }
            if (Integer.parseInt(totalAns) >= 75) {
                iv75.setImageResource(R.mipmap.r3);
            }
            if (Integer.parseInt(totalAns) >= 100) {
                iv100.setImageResource(R.mipmap.r4);
            }

            TextView tvName = (TextView) dialog.findViewById(R.id.tvName);
            TextView tvLocation = (TextView) dialog.findViewById(R.id.tvLocation);
            TextView tvTotal = (TextView) dialog.findViewById(R.id.tvTotal);
            tvTotal.setText(quizAnswerListModels.get(0).getTotal() + " " + getString(R.string.str_sahi_uttar));
            tvName.setText(quizAnswerListModels.get(0).getFirst_name());
            tvLocation.setText(stateEnglishToHindWord(quizAnswerListModels.get(0).getLocation_state()) + ", " +
                    cityEnglishToHindWord(quizAnswerListModels.get(0).getLocation_state(), quizAnswerListModels.get(0).getLocation_city()));

            TextView tvName1 = (TextView) dialog.findViewById(R.id.tvName1);
            TextView tvLocation1 = (TextView) dialog.findViewById(R.id.tvLocation1);
            TextView tvTotal1 = (TextView) dialog.findViewById(R.id.tvTotal1);
            tvTotal1.setText(quizAnswerListModels.get(1).getTotal() + " " + getString(R.string.str_sahi_uttar));
            tvName1.setText(quizAnswerListModels.get(1).getFirst_name());
            tvLocation1.setText(stateEnglishToHindWord(quizAnswerListModels.get(1).getLocation_state()) + ", " +
                    cityEnglishToHindWord(quizAnswerListModels.get(1).getLocation_state(), quizAnswerListModels.get(1).getLocation_city()));

            TextView tvName2 = (TextView) dialog.findViewById(R.id.tvName2);
            TextView tvLocation2 = (TextView) dialog.findViewById(R.id.tvLocation2);
            TextView tvTotal2 = (TextView) dialog.findViewById(R.id.tvTotal2);
            tvTotal2.setText(quizAnswerListModels.get(2).getTotal() + " " + getString(R.string.str_sahi_uttar));
            tvName2.setText(quizAnswerListModels.get(2).getFirst_name());
            tvLocation2.setText(stateEnglishToHindWord(quizAnswerListModels.get(2).getLocation_state()) + ", " +
                    cityEnglishToHindWord(quizAnswerListModels.get(2).getLocation_state(), quizAnswerListModels.get(2).getLocation_city()));

            RecyclerView rcvList = dialog.findViewById(R.id.rcvList);
            rcvList.setLayoutManager(new LinearLayoutManager(getActivity()));
            rcvList.setAdapter(new QuizAnsListAdapter(quizAnswerListModels));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class QuizAnsListAdapter extends RecyclerView.Adapter<QuizAnsListAdapter.ViewHolder> {
        List<QuizAnswerListModel> listdata;

        public QuizAnsListAdapter(List<QuizAnswerListModel> listdata) {
            this.listdata = listdata;
        }

        @NotNull
        @Override
        public QuizAnsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.quiz_ans_list_item, parent, false);
            QuizAnsListAdapter.ViewHolder viewHolder = new QuizAnsListAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final QuizAnsListAdapter.ViewHolder holder, final int position) {
            try {
//                holder.tvTotal.setPaintFlags(holder.tvTotal.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                holder.tvTotal.setText(listdata.get(position).getTotal() + " सही उत्तर");
                holder.tvName.setText(listdata.get(position).getFirst_name());
                holder.tvLocation.setText("पटना, बिहार");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return listdata.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvTotal, tvName, tvLocation;

            public ViewHolder(View itemView) {
                super(itemView);
                this.tvName = (TextView) itemView.findViewById(R.id.tvName);
                this.tvLocation = (TextView) itemView.findViewById(R.id.tvLocation);
                this.tvTotal = (TextView) itemView.findViewById(R.id.tvTotal);
            }
        }
    }

    void parseStateCityListData() {
        try {
            String stateCityData = sharedPreferences.getString(Constants.SharedPreferences_StateCityData, "");
            if (!TextUtils.isEmpty(stateCityData)) {
                JSONObject jsonObject = new JSONObject(stateCityData);
                if (jsonObject != null & jsonObject.length() > 0) {
                    JSONObject data = jsonObject.optJSONObject("data");
                    if (data != null && data.length() > 0) {
                        stateCityDataModels = new ArrayList<StateCityDataModel>();
                        JSONArray result1 = data.optJSONArray("result");
                        if (result1 != null && result1.length() > 0) {
                            JSONArray result = new JSONArray(result1.optJSONObject(0).optString(RongoApp.getDefaultLanguage()));
                            for (int i = 0; i < result.length(); i++) {
                                JSONObject object = result.optJSONObject(i);

                                JSONObject state = object.optJSONObject("state");
                                if (state != null && state.length() > 0) {
                                    parseJsonState(i, state);
                                }

                                JSONObject district = object.optJSONObject("district");
                                if (district != null && district.length() > 0) {
                                    parseJsonCity(i, district);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void parseJsonState(int id, JSONObject data) {
        StateCityDataModel stateCityDataModel = new StateCityDataModel();
        if (data != null) {
            Iterator<String> it = data.keys();
            while (it.hasNext()) {
                String key = it.next();
                try {
                    if (data.get(key) instanceof JSONArray) {
                        JSONArray arry = data.getJSONArray(key);
                        int size = arry.length();
                        for (int i = 0; i < size; i++) {
                            parseJsonState(i, arry.getJSONObject(i));
                        }
                    } else if (data.get(key) instanceof JSONObject) {
                        parseJsonState(0, data.getJSONObject(key));
                    } else {
//                        System.out.println("" + key + "-" + id + " : " + data.optString(key));
                        stateCityDataModel.setId(String.valueOf(id));
                        stateCityDataModel.setState_english(key);
                        stateCityDataModel.setState_hindi(data.optString(key));
                        stateCityDataModels.add(stateCityDataModel);
                    }
                } catch (Throwable e) {
//                    System.out.println("" + key + " : " + data.optString(key));
                    e.printStackTrace();
                }
            }
        }
    }

    void parseJsonCity(int id, JSONObject data) {
        ArrayList<StateCityDataModel.CityListModel> cityListModels = new ArrayList<>();
        if (data != null) {
            Iterator<String> it = data.keys();
            while (it.hasNext()) {
                String key = it.next();
                try {
                    if (data.get(key) instanceof JSONArray) {
                        JSONArray arry = data.getJSONArray(key);
                        int size = arry.length();
                        for (int i = 0; i < size; i++) {
                            parseJsonState(i, arry.getJSONObject(i));
                        }
                    } else if (data.get(key) instanceof JSONObject) {
                        parseJsonState(0, data.getJSONObject(key));
                    } else {
//                        System.out.println("" + key + "-" + id + " : " + data.optString(key));
                        StateCityDataModel.CityListModel cityListModel = new StateCityDataModel.CityListModel();
                        cityListModel.setId(String.valueOf(id));
                        cityListModel.setCity_english(key);
                        cityListModel.setCity_hindi(data.optString(key));
                        cityListModels.add(cityListModel);
                    }
                } catch (Throwable e) {
//                    System.out.println("" + key + " : " + data.optString(key));
                    e.printStackTrace();
                }
            }
            stateCityDataModels.get(id).setCityList(cityListModels);
        }
    }

    String stateEnglishToHindWord(String state) {
        String stateHindi = state;
        try {
            if (!TextUtils.isEmpty(state) && stateCityDataModels.size() > 0) {
                for (int i = 0; i < stateCityDataModels.size(); i++) {
                    if (state.equalsIgnoreCase(stateCityDataModels.get(i).getState_english())) {
                        stateHindi = stateCityDataModels.get(i).getState_hindi();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stateHindi;
    }

    String cityEnglishToHindWord(String state, String city) {
        String cityHindi = city;
        try {
            if (!TextUtils.isEmpty(state) && stateCityDataModels.size() > 0) {
                for (int i = 0; i < stateCityDataModels.size(); i++) {
                    if (state.equalsIgnoreCase(stateCityDataModels.get(i).getState_english())) {
                        for (int j = 0; j < stateCityDataModels.get(i).getCityList().size(); j++) {
                            if (city.equalsIgnoreCase(stateCityDataModels.get(i).getCityList().get(j).getCity_english())) {
                                cityHindi = stateCityDataModels.get(i).getCityList().get(j).getCity_hindi();
                            }
                        }
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cityHindi;
    }

}