package com.rongoapp.fragments;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static com.rongoapp.controller.Constants.CAMERA_REQUEST_CODE;
import static com.rongoapp.controller.Constants.GALLERY_REQUEST_CODE;
import static com.rongoapp.controller.Constants.PERMISSION_READ_STORAGE;
import static com.rongoapp.controller.Constants.providerAuthority;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.text.HtmlCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.card.MaterialCardView;
import com.google.firebase.inappmessaging.FirebaseInAppMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.rongoapp.R;
import com.rongoapp.activities.CommunityDetailsActivity;
import com.rongoapp.activities.HomeActivity;
import com.rongoapp.activities.RewardActivity;
import com.rongoapp.activities.WeatherDetailsActivity;
import com.rongoapp.adapter.ChemicalAdapter;
import com.rongoapp.adapter.ChemicalNameAdapter;
import com.rongoapp.adapter.CropAreaForPhasalAdapter;
import com.rongoapp.adapter.CropProblemPostQuesAdapter;
import com.rongoapp.adapter.CropQuesOptionsAdapter;
import com.rongoapp.adapter.HighRiskSavdhaniAdapter;
import com.rongoapp.adapter.MyPagerAdapter;
import com.rongoapp.adapter.SpinnerArrayAdapter;
import com.rongoapp.adapter.StagesAdapter;
import com.rongoapp.adapter.ThumbnailsAdapter;
import com.rongoapp.adapter.WelcomeAdapter;
import com.rongoapp.adapter.WelcomeOfflineAdapter;
import com.rongoapp.audioPlayerTTS.TTS;
import com.rongoapp.audioPlayerTTS.TTSAudioPlayer;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.FetchJsonFromAssetsFile;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.GlideApp;
import com.rongoapp.controller.InitInterface;
import com.rongoapp.controller.LocaleManager;
import com.rongoapp.controller.PermissionUtils;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.AdvisoryPhotoData;
import com.rongoapp.data.AdvisoryStatusData;
import com.rongoapp.data.ChemicalNameModel;
import com.rongoapp.data.CropAreaFocusedData;
import com.rongoapp.data.CropWeekDetails;
import com.rongoapp.data.CropsData;
import com.rongoapp.data.DailyQuizStatusData;
import com.rongoapp.data.DiasesData;
import com.rongoapp.data.FeedBackSubmitModel;
import com.rongoapp.data.InsertHomeFeedData;
import com.rongoapp.data.KaryaQuestionModel;
import com.rongoapp.data.LuckyDrawData;
import com.rongoapp.data.MasterCropData;
import com.rongoapp.data.NutritionData;
import com.rongoapp.data.OfflineWeeklyCropInfoData;
import com.rongoapp.data.PestData;
import com.rongoapp.data.PostData;
import com.rongoapp.data.ProblemRelatedData;
import com.rongoapp.data.ProfileResultData;
import com.rongoapp.data.SavdhaniDataList;
import com.rongoapp.data.SeasonData;
import com.rongoapp.data.SeedTypeData;
import com.rongoapp.data.StateCityDataModel;
import com.rongoapp.data.SurveyQuestionItemData;
import com.rongoapp.data.SurveyQuestions;
import com.rongoapp.data.WeeklyAdvisoryData;
import com.rongoapp.data.WeeklyCropInfo;
import com.rongoapp.data.WeeklyWeatherData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.imagecompressor.ImageZipper;
import com.rongoapp.services.CropImagesServices;
import com.rongoapp.services.PreloadImagesServices;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.DateUtils;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.CarouselLayoutManager;
import com.rongoapp.views.CustomTypefaceSpan;
import com.rongoapp.views.GridSpacingItemDecoration;
import com.rongoapp.views.ProgressBarDialog;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.Executors;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhasalFragment extends Fragment implements InitInterface, HomeActivity.IOnBackPressed {

    HomeActivity homeActivity;
    PhasalFragment phasalFragment;
    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;

    ShimmerFrameLayout shimmerFrameLayout;

    View view;
    Toolbar toolbar;
    MediaPlayer mediaPlayer;
    TTS.AudioPlayerListener audioPlayerListener;

    long mLastLongClick = 0;
    int mYear = 0, mMonth = 0, mDay = 0, apiCount = 0, surveyCount = 0, cropCurrent = 0, cropWeek = 0;
    double latitude = 0.0, longitude = 0.0;
    boolean isFirstTime = false, userInteraction = true, isCropEnd = true;

    String season_id = "", image_path = "", field_id = "", survey_id = "0", field_type = "", p_crop_id = "", seedType = "";
    String sowingDate = "0000-00-00 00:00:00", expectedSowingDate = "0000-00-00 00:00:00", crop_week = "0", fromDatePicker = "N";
    String options = "", infoText = "", infoType = "", weekCropDay = "", inputType = "", selectedOption = "", s_name = "", post_id = "", storyPostId = "";

    Dialog BuvayDateDialog;
    AppCompatCheckBox rbSSP, rbDAP;
    AutoCompleteTextView editOptions, ediOption1;
    AppCompatEditText editTextPhonenumber, edtProduct;
    ViewPager stageViewPager;
    Spinner spinnerOptions;

    feedbackAdapter feedbackAdapter;
    CropQuesOptionsAdapter questionOptionsAdapter;

    JSONArray chemical_info;
    List<String> generalAdvisory = new ArrayList<String>();
    List<String> advisoryImages = new ArrayList<String>();
    List<String> chemicalNeeds = new ArrayList<String>();
    List<String> selectedStringsList = new ArrayList<>();
    List<String> selectedStringsListForNames = new ArrayList<>();
    List<String> arrayList = new ArrayList<>();
    List<String> stringList = new ArrayList<>();
    List<String> integers = new ArrayList<>();

    ArrayList<StateCityDataModel> stateCityDataModels = new ArrayList<StateCityDataModel>();
    List<SeedTypeData> seedTypeData = new ArrayList<>();
    List<SurveyQuestions> surveyQuestions = new ArrayList<>();
    List<CropWeekDetails> cropWeekDetails = new ArrayList<>();

    ProgressBarDialog progressBarDialog;
    ProgressBar pbChemical, pbJankari, pbRadio;

    AppCompatTextView txtUsername, txtChecmicalName, txtChecmical, txtQuestion, txtTitle, txtInvitation, tvWeatherExpText;
    AppCompatTextView tvShareCode, tvPointsInvite, tvShareWhatapp, tvSharefacebook, tvShareCopy, tvShareMore, tvSendToCommunity, tvWeatherWarningTxt;
    AppCompatTextView txtWeatherAlert, tvRongoHelpLineNumber, txtPratiEkad, tvTodayWeatherDate, tvTodayWeatherMinMax, tvClick;
    AppCompatTextView textRedDot, tvRewardPoint, tvRewardPointBuvayDarj, tvTodayWeatherDistrict, tvAddNewCrop, tvInviteMsg;
    AppCompatTextView txtYes, txtNo, txtHealthy, txtUnhealthy, txtUplaod, tvLegalText, tvTimerView, tvOrDekhe, tvWeatherMsgNew;

    MaterialCardView bunaiStatusCard, cardCommunityView, cardWeatherData, cardHealthStatus, cardLuckYDraw, cardfeedback,
            cardAajKaQuestion, cardNewCropAdd, cardInternetRadio;

    //    MaterialCardView cardViewChemical, cardSavdhani, cardFertilizer, cardGeneralAdvisory;
    LinearLayout llAdvisory, llSavdhani, llChemical, llFertilizer, llWeather, llRokdhamTagView, llDieaseTagView;
    LinearLayout rgJankari, linearNutrition, linearDiases, linearPest, linearGo, llShareReward, llReferralText,
            linearQuestions, llRongoHelpLineNumber, llTodayView, llWeekView, relJalBharav, llButtonView,
            llSelectionView, llEditView, llSessionStart, llSessionEnd, llRewardList, llEndRewardView,
            llWeatherNoInternet, llWeatherOtherDetail, llOrDekhe, llRadioView, llMoreRadioView;

    LinearLayout llWeeklyQuizView, llShareView, llQuizView, llStoryView, llWeatherView, llExpertView, llSavdhaniView;
    TextView storyBadge, weeklyQuizBadge, weatherBadge, expertBadge, quizBadge, inviteBadge, savdhaniBadge;

    RelativeLayout relRewardView, rlHealthView, relativeBuaiCard, rlSamanySalah, relativeArrow, rlCommunityNext, rlWeatherView, rlMoreWeatherData;

    ImageView imgPrevios, imgNext, ivWeatherWarning, ivWeatherExp, imgPlay, imgPlayJankari, ivTag,
            ivWeeklyCropQues, ivCommunity, radioPlay;

    RecyclerView rvChemical, rvOptions, rcvSessionFeedback, rcvWeather, rcvMyPager;

    TextView tvQuestions, tvHa, tvNa, tvDarjKare, tvDarjKare1, tvSessionEndOfflineMsg, tvKisanCount, tvPrashanCount, tvAnswerCount, tvCommentCount,
            txtQuestionRelated, txtDate, tvExpertSujav, tvGoCommunity, tvEndQuiz, tvEndExpert, tvEndReward, tvEndPhotos, tvFeedbackSubmit;

    BroadcastReceiver UPDATE_CROP_INFO = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                    if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                        fetchDataFromAssetFileForWeeklyAdvisory();
                    } else {
//                        getWeeklyAdvisory();
                        fetchDataFromAssetFileForWeeklyAdvisory();
                    }
                } else {
                    fetchDataFromAssetFileForWeeklyAdvisory();
                }
                updateScreenData();
                ProfileResultData profileResultData = Functions.getData();
                displayData(profileResultData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    BroadcastReceiver UPDATE_STORY = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent != null) {
//                  String  StoryName = intent.getStringExtra(Constants.STORY_NAME);
                    if (intent.getStringExtra(Constants.STORY_NAME).equalsIgnoreCase(Constants.STORY_INVITE)) {
                        shareStoryDialog();
                    } else if (intent.getStringExtra(Constants.STORY_NAME).equalsIgnoreCase(Constants.STORY_CROP_HEALTH)) {
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        DialogFragment newFragment = CropHealthFragment.newInstance();
                        newFragment.show(ft, "Crop Health");
                    } else if (intent.getStringExtra(Constants.STORY_NAME).equalsIgnoreCase(Constants.STORY_QUIZ)) {
                        if (!Functions.getQuizIntent(homeActivity, sharedPreferences)) {
                            FragmentTransaction ft = getFragmentManager().beginTransaction();
                            DialogFragment newFragment = QuizOLDFragment.newInstance();
                            newFragment.show(ft, "QuizOLDFragment");
                        } else {
                            FragmentTransaction ft = getFragmentManager().beginTransaction();
                            DialogFragment newFragment = QuizFragment.newInstance();
                            newFragment.show(ft, "dialog");
                        }
                    } else if (intent.getStringExtra(Constants.STORY_NAME).equalsIgnoreCase(Constants.STORY_WEEKLY_QUIZ)) {
                        if (Functions.isHealthAvailable(sharedPreferences, appDatabase)) {
                            pragatiQuizDialog();
                        } else {
                            FragmentTransaction ft1 = getFragmentManager().beginTransaction();
                            DialogFragment newFragment1 = CropHealthFragment.newInstance();
                            newFragment1.show(ft1, "health");
                        }
                    } else if (intent.getStringExtra(Constants.STORY_NAME).equalsIgnoreCase(Constants.STORY_COMMUNITY)) {
                        if (!TextUtils.isEmpty(post_id) && InternetConnection.checkConnectionForFasl(homeActivity)) {
                            expertStoryDialog();
                        } else {
                            if (Functions.isHealthAvailable(sharedPreferences, appDatabase)) {
                                pragatiQuizDialog();
                            } else {
                                FragmentTransaction ft1 = getFragmentManager().beginTransaction();
                                DialogFragment newFragment1 = CropHealthFragment.newInstance();
                                newFragment1.show(ft1, "health");
                            }
                        }
                    } else if (intent.getStringExtra(Constants.STORY_NAME).equalsIgnoreCase(Constants.STORY_WEATHER)) {
                        if (llWeatherView.getVisibility() == View.VISIBLE) {
                            weatherStoryDialog();
                        } else {
                            if (!TextUtils.isEmpty(post_id) && InternetConnection.checkConnectionForFasl(homeActivity)) {
                                expertStoryDialog();
                            } else {
                                if (Functions.isHealthAvailable(sharedPreferences, appDatabase)) {
                                    pragatiQuizDialog();
                                } else {
                                    FragmentTransaction ft1 = getFragmentManager().beginTransaction();
                                    DialogFragment newFragment1 = CropHealthFragment.newInstance();
                                    newFragment1.show(ft1, "health");
                                }
                            }
                        }
                    } else if (intent.getStringExtra(Constants.STORY_NAME).equalsIgnoreCase(Constants.STORY)) {
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        DialogFragment newFragment = StorysFragment.newInstance();
                        newFragment.show(ft, "story");
                    } else if (intent.getStringExtra(Constants.STORY_NAME).equalsIgnoreCase(Constants.STORY_SAVDHANI)) {
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        DialogFragment newFragment = SavdhaniFragment.newInstance();
                        newFragment.show(ft, "story");

                    } else if (intent.getStringExtra(Constants.STORY_NAME).equalsIgnoreCase(Constants.STORY_TEST)) {
                        setDailyQuestion();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public static PhasalFragment getInstance() {
        return new PhasalFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof HomeActivity) {
            homeActivity = (HomeActivity) context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiCount = 0;
        surveyCount = 0;
        RongoApp.logVisitedPageTabEvent("Phasal");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().registerReceiver(UPDATE_CROP_INFO, new IntentFilter(Constants.BROADCAST_RECEIVER.UPDATE_CROP_INFO));
        getActivity().registerReceiver(UPDATE_STORY, new IntentFilter(Constants.BROADCAST_RECEIVER.UPDATE_STORY));
        view = inflater.inflate(R.layout.fragment_phasal, container, false);
        try {
            phasalFragment = this;
            progressBarDialog = new ProgressBarDialog(homeActivity);
            sharedPreferences = homeActivity.getSharedPreferences(Constants.MediaPrefs, 0);
            appDatabase = AppDatabase.getDatabase(homeActivity);
            mediaPlayer = new MediaPlayer();
            Functions.setFirebaseLogEventTrack(homeActivity, Constants.Screen_Track, "Phasal", "", "");

            shimmerFrameLayout = view.findViewById(R.id.shimmerLayout);
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmer();

            setEngementTools();
            HomeActivity.setTextToSpeechInit();
            askStoragePermissions();
            init();
            setListener();
            updateScreenData();
            setNewSavdhaniView();
//            playRadio();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    public static String differenceDatesAndTime(Date mDateStart, Date mDateEnd) {
        long different = mDateEnd.getTime() - mDateStart.getTime();
        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;

        long minutes = elapsedHours * 60 + elapsedMinutes;
        long result = elapsedDays * 24 * 60 + minutes;
        if (0 > result) {
            result = result + 720;  //result is minus then add 12*60 minutes
        }

        return result + "";
    }

    void setShimmerGone() {
        shimmerFrameLayout.stopShimmer();
        shimmerFrameLayout.setVisibility(View.GONE);
    }

    void playRadio() {
        String audio_path = null, url = null;
        try {
            cardInternetRadio = (MaterialCardView) view.findViewById(R.id.cardInternetRadio);

            llRadioView = (LinearLayout) view.findViewById(R.id.llRadioView);
            llMoreRadioView = (LinearLayout) view.findViewById(R.id.llMoreRadioView);

            radioPlay = (ImageView) view.findViewById(R.id.radioPlay);
            pbRadio = (ProgressBar) view.findViewById(R.id.pbRadio);
            llOrDekhe = (LinearLayout) view.findViewById(R.id.llOrDekhe);
            tvOrDekhe = (AppCompatTextView) view.findViewById(R.id.tvOrDekhe);

            tvOrDekhe.setCompoundDrawablesWithIntrinsicBounds(null, null, homeActivity.getResources().getDrawable(R.drawable.arrow_down), null);
            tvOrDekhe.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (llMoreRadioView.getVisibility() == View.VISIBLE) {
                        llMoreRadioView.setVisibility(View.GONE);
                        llRadioView.setVisibility(View.VISIBLE);
                        tvOrDekhe.setCompoundDrawablesWithIntrinsicBounds(null, null, homeActivity.getResources().getDrawable(R.drawable.arrow_down), null);
                    } else {
                        llMoreRadioView.setVisibility(View.VISIBLE);
                        llRadioView.setVisibility(View.VISIBLE);
                        tvOrDekhe.setCompoundDrawablesWithIntrinsicBounds(null, null, homeActivity.getResources().getDrawable(R.drawable.arrow_up), null);
                    }
                }
            });

            String storyData = sharedPreferences.getString(Constants.SharedPreferences_story_data, null);
            if (!TextUtils.isEmpty(storyData)) {
                JSONObject result = new JSONObject(storyData);
                if (result != null && result.length() > 0) {
                    audio_path = result.optString("audio_path");
                    JSONArray internet_radio = new JSONArray(result.optString("internet_radio"));
                    if (internet_radio != null && internet_radio.length() > 0) {
                        cardInternetRadio.setVisibility(View.VISIBLE);
                        JSONObject jsonObject = internet_radio.getJSONObject(0);
                        url = jsonObject.optString("audio_path");
                    } else {
                        cardInternetRadio.setVisibility(View.GONE);
                    }
                }
            } else {
                cardInternetRadio.setVisibility(View.GONE);
            }

            String finalAudio_path = audio_path;
            String finalUrl = url;
            radioPlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Functions.setFirebaseLogEventTrack(homeActivity, Constants.Screen_Track, "et_radio", "" + RongoApp.getUserId(), "Radio Play");
                        Functions.setUserScreenTrackEvent(appDatabase, "et_radio", "" + RongoApp.getUserId());

                        if (mediaPlayer != null && InternetConnection.checkConnectionForFasl(homeActivity)) {
                            if (mediaPlayer.isPlaying()) {
                                mediaPlayer.stop();
                                mediaPlayer.reset();
                                mediaPlayer.release();
                                mediaPlayer = null;
                                radioPlay.setImageResource(R.mipmap.play);
                                pbRadio.setVisibility(View.GONE);
                                mediaPlayer = new MediaPlayer();
                            } else {
                                mediaPlayer = new MediaPlayer();
                                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                pbRadio.setVisibility(View.VISIBLE);
                                try {
                                    mediaPlayer.setDataSource(finalAudio_path + finalUrl);
                                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                        public void onPrepared(final MediaPlayer mediaPlayer) {
                                            pbRadio.setVisibility(View.GONE);
                                            radioPlay.setImageResource(R.mipmap.pause);
                                            mediaPlayer.start();
                                        }
                                    });
                                    mediaPlayer.prepareAsync();
                                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                        @Override
                                        public void onCompletion(MediaPlayer mediaPlayer) {
                                            radioPlay.setImageResource(R.mipmap.play);
                                            pbRadio.setVisibility(View.GONE);
                                        }
                                    });
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), homeActivity.getString(R.string.no_connection));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setEngementTools() {
        try {
            tvTimerView = (AppCompatTextView) view.findViewById(R.id.tvTimerView);
            storyBadge = view.findViewById(R.id.storyBadge);
            weeklyQuizBadge = view.findViewById(R.id.weeklyQuizBadge);
            weatherBadge = view.findViewById(R.id.weatherBadge);
            expertBadge = view.findViewById(R.id.expertBadge);
            quizBadge = view.findViewById(R.id.quizBadge);
            inviteBadge = view.findViewById(R.id.inviteBadge);
            savdhaniBadge = view.findViewById(R.id.savdhaniBadge);

            llStoryView = view.findViewById(R.id.llStoryView);
            llStoryView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    Functions.setFirebaseLogEventTrack(homeActivity, Constants.Screen_Track, "et_Story", "" + RongoApp.getUserId(), "Story");
                    Functions.setUserScreenTrackEvent(appDatabase, "et_Story", "" + RongoApp.getUserId());

                    if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                        String storyData = sharedPreferences.getString(Constants.SharedPreferences_story_data, null);
                        if (!TextUtils.isEmpty(storyData)) {
//                            FragmentTransaction ft = getFragmentManager().beginTransaction();
//                            DialogFragment newFragment = AllStorysFragment.newInstance();
//                            newFragment.show(ft, "dialog");

                            FragmentTransaction ft = getFragmentManager().beginTransaction();
                            DialogFragment newFragment = StorysFragment.newInstance();
                            newFragment.show(ft, "story");
                        } else {
                            getUserStories();
                        }
                    } else {
                        Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), homeActivity.getString(R.string.no_connection));
                    }
                }
            });

            llExpertView = view.findViewById(R.id.llExpertView);
            llExpertView.setVisibility(View.GONE);
            llExpertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    Functions.setFirebaseLogEventTrack(homeActivity, Constants.Screen_Track, "et_ExpertCall", "" + RongoApp.getUserId(), "Expert Call");
                    Functions.setUserScreenTrackEvent(appDatabase, "et_ExpertCall", "" + RongoApp.getUserId());

                    expertStoryDialog();
                }
            });

            llWeatherView = view.findViewById(R.id.llWeatherView);
            llWeatherView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    Functions.setFirebaseLogEventTrack(homeActivity, Constants.Screen_Track, "et_WeatherInfo", "" + RongoApp.getUserId(), "Weather Info");
                    Functions.setUserScreenTrackEvent(appDatabase, "et_WeatherInfo", "" + RongoApp.getUserId());

                    if (TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_WeeklyWeatherData, ""))) {
                        getWeeklyWeather(true);
                    } else {
                        weatherStoryDialog();
                    }
                }
            });

            llQuizView = view.findViewById(R.id.llQuizView);
            llQuizView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    Functions.setFirebaseLogEventTrack(homeActivity, Constants.Screen_Track, "et_Quiz", "" + RongoApp.getUserId(), "Quiz");
                    Functions.setUserScreenTrackEvent(appDatabase, "et_Quiz", "" + RongoApp.getUserId());

                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    if (Functions.getQuizIntent(homeActivity, sharedPreferences)) {
                        DialogFragment newFragment = QuizFragment.newInstance();
                        newFragment.show(ft, "dialog");
                    } else {
                        DialogFragment newFragment = QuizOLDFragment.newInstance();
                        newFragment.show(ft, "QuizOLDFragment");
                    }
                }
            });

            llWeeklyQuizView = view.findViewById(R.id.llWeeklyQuizView);
            llWeeklyQuizView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    Functions.setFirebaseLogEventTrack(homeActivity, Constants.Screen_Track, "et_WeekCropInfo", "" + RongoApp.getUserId(), "WeekCropInfo");
                    Functions.setUserScreenTrackEvent(appDatabase, "et_WeekCropInfo", "" + RongoApp.getUserId());

                    /*
                     * Weekly crop question & healthy card
                     * */
                    if (Functions.isHealthAvailable(sharedPreferences, appDatabase)) {
                        pragatiQuizDialog();
                    } else {
                        FragmentTransaction ft1 = getFragmentManager().beginTransaction();
                        DialogFragment newFragment1 = CropHealthFragment.newInstance();
                        newFragment1.show(ft1, "health");
                    }
                }
            });

            llShareView = view.findViewById(R.id.llShareView);
            llShareView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    Functions.setFirebaseLogEventTrack(homeActivity, Constants.Screen_Track, "et_Share", "" + RongoApp.getUserId(), "Share");
                    Functions.setUserScreenTrackEvent(appDatabase, "et_Share", "" + RongoApp.getUserId());

                    shareStoryDialog();
                }
            });

            llSavdhaniView = view.findViewById(R.id.llSavdhaniView);
            llSavdhaniView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    Functions.setFirebaseLogEventTrack(homeActivity, Constants.Screen_Track, "et_Savdhani", "" + RongoApp.getUserId(), "SavdhaniInfo");
                    Functions.setUserScreenTrackEvent(appDatabase, "et_Savdhani", "" + RongoApp.getUserId());

                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    DialogFragment newFragment = SavdhaniFragment.newInstance();
                    newFragment.show(ft, "savdhani");
                }
            });

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
//                        int salahCount = Integer.parseInt(sharedPreferences.getString(Constants.SharedPreference_Salah_Counter, "0"));
//                        if (salahCount > 0) {
//                            expertBadge.setText("" + salahCount);
//                            expertBadge.setVisibility(View.VISIBLE);
//                            llExpertView.setVisibility(View.VISIBLE);
//                            llExpertView.setEnabled(true);
//                            llExpertView.setAlpha(1f);
//                        } else {
//                            llExpertView.setVisibility(View.GONE);
//                            expertBadge.setVisibility(View.GONE);
//                            llExpertView.setEnabled(false);
//                            llExpertView.setAlpha(0.4f);
//                        }
                        getWeeklyWeather(false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 2000);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void getUserStories() {
        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
            RetrofitClient.getInstance().getApi().getUserStories(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                    RongoApp.getAppVersion(), RongoApp.getUserId(), "all").enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response != null && response.isSuccessful()) {
                            String r = response.body().string();
                            if (!TextUtils.isEmpty(r)) {
                                JSONObject jsonObject = new JSONObject(r);
                                if (jsonObject != null && jsonObject.length() > 0) {
                                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                        JSONObject dataObject = jsonObject.optJSONObject("data");
                                        if (dataObject != null && dataObject.length() > 0) {
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_story_data, "" + dataObject);
                                        }

                                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                                        DialogFragment newFragment = StorysFragment.newInstance();
                                        newFragment.show(ft, "dialog");
                                    } else {
                                        Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), homeActivity.getString(R.string.no_story_available));
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        } else {
            Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), homeActivity.getString(R.string.no_connection));
        }
    }

    void setStoryImagePreload(JSONObject jsonObject) {
        try {
            JSONObject dataObject = jsonObject.optJSONObject("data");
            image_path = dataObject.optString("image_path");
            JSONArray storiesArray = new JSONArray(dataObject.optString("stories"));
            if (storiesArray != null && storiesArray.length() > 0) {
                for (int i = 0; i < storiesArray.length(); i++) {
                    JSONObject jsonObject1 = storiesArray.optJSONObject(i);

                    JSONArray jsonArray = jsonObject1.optJSONArray("cards");
                    for (int j = 0; j < jsonArray.length(); j++) {
                        JSONObject jsonObject2 = jsonArray.optJSONObject(j);
                        String url = image_path + jsonObject2.optString("card_media_path");
                        GlideApp.with(this).load(url).listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                return false;
                            }
                        }).submit();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void expertStoryDialog() {
        try {
            Functions.setFirebaseLogEventTrack(getActivity(), Constants.ET_STORY_TRACK, Constants.ET_EXPERT,
                    "" + RongoApp.getUserId(), Constants.ET_EXPERT + "_" + RongoApp.getUserId());
            Functions.setUserScreenTrackEvent(appDatabase, Constants.ET_STORY_TRACK, Constants.ET_EXPERT + "_" + RongoApp.getUserId());

            Dialog dialog = new Dialog(homeActivity, R.style.NewDialog);
            dialog.setContentView(R.layout.story_expert_dialog);
            dialog.show();

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(lp);

            ImageView ivBack = dialog.findViewById(R.id.ivClose);
            ivBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            TextView tvBack = dialog.findViewById(R.id.tvBack);
            tvBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    DialogFragment newFragment = StorysFragment.newInstance();
                    newFragment.show(ft, "story");
                    dialog.dismiss();
                }
            });

            TextView tvNext = dialog.findViewById(R.id.tvNext);
            tvNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (llWeatherView.getVisibility() == View.VISIBLE) {
                        weatherStoryDialog();
                    } else {
                        if (Functions.isHealthAvailable(sharedPreferences, appDatabase)) {
                            pragatiQuizDialog();
                        } else {
                            FragmentTransaction ft1 = getFragmentManager().beginTransaction();
                            DialogFragment newFragment1 = CropHealthFragment.newInstance();
                            newFragment1.show(ft1, "health");
                        }
                    }
                    dialog.dismiss();
                }
            });

            TextView tvNextBtn = dialog.findViewById(R.id.tvNextBtn);
            tvNextBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!TextUtils.isEmpty(post_id) && InternetConnection.checkConnectionForFasl(homeActivity)) {
                        Intent intent = new Intent(homeActivity, CommunityDetailsActivity.class);
                        intent.putExtra("type", "0");
                        intent.putExtra("post_id", post_id);
                        homeActivity.startActivity(intent);
                    }
                    dialog.dismiss();
                }
            });

            ImageView imgAdvisoryImg = dialog.findViewById(R.id.imgAdvisoryImg);
            TextView tvManage1 = dialog.findViewById(R.id.tvManage1);

            String postIdArray = sharedPreferences.getString(Constants.SharedPreference_Story_PostId, "");
            if (!TextUtils.isEmpty(postIdArray)) {
                JSONArray jsonArray = new JSONArray(postIdArray);
                if (jsonArray != null && jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        String postId = jsonArray.optJSONObject(i).optString("post_id");
                        getSingleCommunityPost(postId, imgAdvisoryImg, tvManage1);
                    }
                }
            }

            RongoApp.engagementToolActivity(Constants.ET_EXPERT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void getSingleCommunityPost(String post_id, ImageView imageView, TextView txtView) {
        progressBarDialog.showProgressDialogWithTitle(getResources().getString(R.string.app_name));
        progressBarDialog.setMessage(getResources().getString(R.string.str_load_title_msg));
        progressBarDialog.show();
        RetrofitClient.getInstance().getApi().getSingleCommunityPost(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                SharedPreferencesUtils.getUserId(homeActivity), post_id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        progressBarDialog.hide();
                        String str = response.body().string();
                        if (!TextUtils.isEmpty(str)) {
                            JSONObject jsonObject = new JSONObject(str);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject data = jsonObject.optJSONObject("data");
                                    if (data != null && data.length() > 0) {
                                        JSONObject result = data.optJSONObject("result");
                                        if (result != null && result.length() > 0) {
                                            JSONArray optJSONArray = result.optJSONArray("posts");
                                            if (optJSONArray != null && optJSONArray.length() > 0) {
                                                String imagePath = result.optString("image_path");
                                                PostData postData = Functions.getPostDataByPostId(homeActivity, optJSONArray, post_id);

                                                storyPostId = postData.getPost_id();
                                                String pictureKey = postData.getPicture_key();
                                                ArrayList<String> pictures = new ArrayList<String>(Arrays.asList(pictureKey.split(",")));
                                                String image = imagePath + "/" + pictures.get(0).replace("[", "").replace("]", "");
                                                Log.e("image:", "" + image);
                                                GlideApp.with(homeActivity)
                                                        .load(image)
                                                        .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                                                        .error(R.drawable.placeholderbg)
                                                        .placeholder(R.drawable.placeholderbg).into(imageView);

                                                if (!TextUtils.isEmpty(postData.getComments())) {
                                                    JSONArray jsonArray = new JSONArray(postData.getComments());
                                                    if (jsonArray != null && jsonArray.length() > 0) {
                                                        for (int i = 0; i < jsonArray.length(); i++) {
                                                            if (jsonArray.optJSONObject(i).optString("type").equalsIgnoreCase("0")) {
//                                                                jsonArray.optJSONObject(i).optString("comment_text")
//                                                                jsonArray.optJSONObject(i).optString("is_expert")
                                                                txtView.setText("" + jsonArray.optJSONObject(i).optString("comment_text"));
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        progressBarDialog.hideProgressDialogWithTitle();
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressBarDialog.hide();
                        progressBarDialog.hideProgressDialogWithTitle();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
                progressBarDialog.hide();
            }
        });
    }

    void weatherStoryDialog() {
        try {
            Functions.setFirebaseLogEventTrack(getActivity(), Constants.ET_STORY_TRACK, Constants.ET_WEATHER,
                    "" + RongoApp.getUserId(), Constants.ET_WEATHER + "_" + RongoApp.getUserId());
            Functions.setUserScreenTrackEvent(appDatabase, Constants.ET_STORY_TRACK, Constants.ET_WEATHER + "_" + RongoApp.getUserId());

            Dialog dialog = new Dialog(homeActivity, R.style.NewDialog);
            dialog.setContentView(R.layout.story_weather_dialog);
            dialog.show();

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(lp);

            ImageView ivBack = dialog.findViewById(R.id.ivClose);
            ivBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            TextView tvBack = dialog.findViewById(R.id.tvBack);
            tvBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int salahCount = Integer.parseInt(sharedPreferences.getString(Constants.SharedPreference_Salah_Counter, "0"));
                    if (salahCount > 0) {
                        if (!TextUtils.isEmpty(post_id) && InternetConnection.checkConnectionForFasl(homeActivity)) {
                            expertStoryDialog();
                        }
                    } else {
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        DialogFragment newFragment = StorysFragment.newInstance();
                        newFragment.show(ft, "story");
                    }
                    dialog.dismiss();
                }
            });
            TextView tvNext = dialog.findViewById(R.id.tvNext);
            tvNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Functions.isHealthAvailable(sharedPreferences, appDatabase)) {
                        pragatiQuizDialog();
                    } else {
                        FragmentTransaction ft1 = getFragmentManager().beginTransaction();
                        DialogFragment newFragment1 = CropHealthFragment.newInstance();
                        newFragment1.show(ft1, "health");
                    }
                    dialog.dismiss();
                }
            });

            TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitles);
            TextView tvTitle1 = (TextView) dialog.findViewById(R.id.tvTitles1);
            TextView tvManage1 = (TextView) dialog.findViewById(R.id.tvManage1);
            ImageView imgAdvisoryImg = dialog.findViewById(R.id.imgAdvisoryImg);

            TextView tvTodayWeatherDate = dialog.findViewById(R.id.tvTodayWeatherDate);
            TextView tvTodayWeatherMinMax = dialog.findViewById(R.id.tvTodayWeatherMinMax);

            TextView tvNextBtn = (TextView) dialog.findViewById(R.id.tvNextBtn);
            tvNextBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(homeActivity, WeatherDetailsActivity.class);
                    intent.putExtra("weather_alert", "");
                    startActivity(intent);
                    dialog.dismiss();
                }
            });

            String weeklyWeather = sharedPreferences.getString(Constants.SharedPreferences_WeeklyWeatherData, "");
            if (!TextUtils.isEmpty(weeklyWeather)) {
                WeeklyWeatherData weeklyWeatherData = new Gson().fromJson(weeklyWeather.toString(), WeeklyWeatherData.class);
                List<WeeklyWeatherData.WeatherDatum> weatherData = weeklyWeatherData.getData().getResult().getWeatherData();

                List<String> weather_alert_list = new ArrayList<>();
                JSONObject jsonObject = new JSONObject(weeklyWeather);
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_WeeklyWeatherData, "" + jsonObject.toString());
                JSONObject data = jsonObject.optJSONObject("data");
                if (data != null && data.length() > 0) {
                    JSONObject result = data.optJSONObject("result");
                    if (result != null && result.length() > 0) {
                        JSONArray weather_alert = result.optJSONArray("weather_alert");
                        if (weather_alert != null && weather_alert.length() > 0) {
                            for (int i = 0; i < weather_alert.length(); i++) {
                                weather_alert_list.add(weather_alert.optString(i));
                            }
                        }

                        JSONObject weather_advisory = result.optJSONObject("weather_advisory");
                        if (weather_advisory != null && weather_advisory.length() > 0) {
                            image_path = weather_advisory.optString("image_path");

                            Iterator<String> keys = weather_advisory.keys();
                            while (keys.hasNext()) {
                                String key = keys.next();
                                JSONObject jb = weather_advisory.optJSONObject(key);
                                for (int i = 0; i < weather_alert_list.size(); i++) {
                                    if (weather_alert_list.get(i).contains(key)) {
                                        String name = jb.optString("display_name");
                                        tvTitle.setText(name);

                                        String advisory_image = jb.optString("advisory_image");
                                        if (!TextUtils.isEmpty(advisory_image)) {
                                            if (!InternetConnection.checkConnectionForFasl(homeActivity)) {
                                                if (RongoApp.getSelectedCrop().equalsIgnoreCase("2") || RongoApp.getSelectedCrop().equalsIgnoreCase("3")) {
                                                    imgAdvisoryImg.setImageDrawable(Functions.getAssetsWeatherImage(homeActivity,
                                                            Constants.ASSETS_ADVISORY_WEATHER_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + advisory_image));
                                                } else {
                                                    imgAdvisoryImg.setImageDrawable(Functions.getAssetsWeatherImage(homeActivity,
                                                            Constants.ASSETS_ADVISORY_WEATHER_IMAGE_PATH + "1" + "/" + advisory_image));
                                                }
                                            } else {
                                                Functions.displayImage(image_path + "/" + advisory_image, imgAdvisoryImg);
                                            }
                                        }

                                        JSONArray management = jb.optJSONArray("management");
                                        String contentSuzav = "";
                                        if (management != null && management.length() > 0) {
                                            for (int k = 0; k < management.length(); k++) {
                                                contentSuzav += Functions.fromHtml(management.optString(k));
                                            }
                                        }
                                        tvManage1.setText(contentSuzav);
                                    }
                                }
                            }
                        }

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                        Date todayDate = Calendar.getInstance().getTime();
                        String todayDateformat = sdf.format(todayDate);

                        Date strDate = sdf.parse(weatherData.get(6).getDate());
                        Calendar expireDateTime = Calendar.getInstance();
                        expireDateTime.setTime(strDate);
                        expireDateTime.set(Calendar.HOUR_OF_DAY, 23);
                        expireDateTime.set(Calendar.MINUTE, 59);
                        expireDateTime.set(Calendar.SECOND, 59);

                        for (int i = 0; i < weatherData.size(); i++) {
                            if (todayDateformat.equalsIgnoreCase(sdf.format(sdf.parse(weatherData.get(i).getDate())))) {
                                tvTodayWeatherDate.setText("" + DateUtils.parseDateHindi(weatherData.get(i).getDate(), "yyyy-MM-dd", "dd MMMM yyyy"));
                                tvTodayWeatherMinMax.setText("" + weatherData.get(i).getMin() + "\u2103 / " + weatherData.get(i).getMax() + "\u2103");
//                                tvWeatherExpText.setText(weatherData.get(i).getRainfallPerc() + getString(R.string.str_baris_aasar));
                            }
                        }

                    }
                }
            }

            RongoApp.engagementToolActivity(Constants.ET_WEATHER);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void pragatiQuizDialog() {
        try {
            Functions.setFirebaseLogEventTrack(getActivity(), Constants.ET_STORY_TRACK, Constants.ET_KARYA,
                    "" + RongoApp.getUserId(), Constants.ET_KARYA + "_" + RongoApp.getUserId());
            Functions.setUserScreenTrackEvent(appDatabase, Constants.ET_STORY_TRACK, Constants.ET_KARYA + "_" + RongoApp.getUserId());

            RongoApp.logVisitedPageTabEvent("Weekly Question Story");

            Dialog dialog = new Dialog(homeActivity, R.style.StoryDialog);
            dialog.setContentView(R.layout.story_weekly_quiz_dialog);
            dialog.show();

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(lp);

            ImageView ivClose = dialog.findViewById(R.id.ivClose);
            ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            TextView tvBack = dialog.findViewById(R.id.tvBack);
            tvBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (llWeatherView.getVisibility() == View.VISIBLE) {
                        weatherStoryDialog();
                    } else {
                        int salahCount = Integer.parseInt(sharedPreferences.getString(Constants.SharedPreference_Salah_Counter, "0"));
                        if (salahCount > 0) {
                            if (!TextUtils.isEmpty(post_id) && InternetConnection.checkConnectionForFasl(homeActivity)) {
                                expertStoryDialog();
                            }
                        } else {
                            FragmentTransaction ft = getFragmentManager().beginTransaction();
                            DialogFragment newFragment = StorysFragment.newInstance();
                            newFragment.show(ft, "dialog");
                        }
                    }
                    dialog.dismiss();
                }
            });
            TextView tvNext = dialog.findViewById(R.id.tvNext);
            tvNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    DialogFragment newFragment = SavdhaniFragment.newInstance();
                    newFragment.show(ft, "dialog");
                    dialog.dismiss();
                }
            });

            TextView tvNoDataAvailable = dialog.findViewById(R.id.tvNoDataAvailable);
            tvNoDataAvailable.setVisibility(View.GONE);

            RecyclerView rcvWeeklyQuiz = dialog.findViewById(R.id.rcvWeeklyQuiz);
            rcvWeeklyQuiz.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
            rcvWeeklyQuiz.setNestedScrollingEnabled(false);
            rcvWeeklyQuiz.setHasFixedSize(true);
            ArrayList<KaryaQuestionModel> karyaQuestionModels = new ArrayList<>();

            if (sharedPreferences.getString(Constants.SharedPreference_Sowing_Done, "").equalsIgnoreCase("yes")) {
                String cropWeeks = sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, "0");
                int cropWeek = RongoApp.getCropWeekDaysFunction();
                if (cropWeek == Integer.parseInt(cropWeeks)) {
                    JSONObject WeeklyCropInfo = new JSONObject(RongoApp.getWeeklyCropQuestion());
                    if (!TextUtils.isEmpty(WeeklyCropInfo.toString())) {
                        String weekCropDays = Functions.getWeeklyCropDay(sharedPreferences);
                        if (!TextUtils.isEmpty(weekCropDays)) {
                            JSONObject quesCropWeek = null;
                            String[] arrayStr = weekCropDays.split(",");
                            for (String s : arrayStr) {
                                weekCropDay = s.replace("\"", "");
                                quesCropWeek = Objects.requireNonNull(Functions.getWeeklyCropQuestion(sharedPreferences)).optJSONObject(weekCropDay);
                                if (!TextUtils.isEmpty(quesCropWeek.toString())) {
                                    List<WeeklyCropInfo> weeklyCropInfoCheck = appDatabase.weeklyCropInfoDao().getCropInfoByWeek(
                                            cropWeeks, RongoApp.getSelectedPCrop(), quesCropWeek.getString("info_type"));
                                    if (weeklyCropInfoCheck.size() > 0) {
//                                        bunaiStatusCard.setVisibility(View.GONE);
                                    } else {
//                                        bunaiStatusCard.setVisibility(View.VISIBLE);
                                        KaryaQuestionModel karyaQuestionModel = new KaryaQuestionModel();
                                        karyaQuestionModel.setQuestion(quesCropWeek.getString("question"));
                                        karyaQuestionModel.setInfoText(quesCropWeek.getString("info_text"));
                                        karyaQuestionModel.setInputType(quesCropWeek.getString("input_type"));
                                        karyaQuestionModel.setInfoType(quesCropWeek.getString("info_type"));
                                        karyaQuestionModel.setOptions(quesCropWeek.getString("options"));
                                        karyaQuestionModels.add(karyaQuestionModel);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (karyaQuestionModels.size() > 0) {
                tvNoDataAvailable.setVisibility(View.GONE);
                rcvWeeklyQuiz.setAdapter(new KaryaAdapter(karyaQuestionModels, dialog));
            } else {
                tvNoDataAvailable.setVisibility(View.VISIBLE);
            }

            RongoApp.engagementToolActivity(Constants.ET_KARYA);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class KaryaAdapter extends RecyclerView.Adapter<KaryaAdapter.ViewHolder> {
        List<KaryaQuestionModel> listData;
        Dialog dialog;

        public KaryaAdapter(List<KaryaQuestionModel> listData, Dialog dialog) {
            this.listData = listData;
            this.dialog = dialog;
        }

        @NotNull
        @Override
        public KaryaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.karya_list_item, parent, false);
            KaryaAdapter.ViewHolder viewHolder = new KaryaAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(KaryaAdapter.ViewHolder holder, int position) {
            try {
                holder.tvKaryaReward.setText(RewardPoints.getWeeklyCropInfoRewardPoint(sharedPreferences));
                holder.tvQuestions.setText(listData.get(position).getQuestion());

                inputType = listData.get(position).getInputType();
                infoText = listData.get(position).getInfoText();
                infoType = listData.get(position).getInfoType();

                holder.ivWeeklyCropQues.setVisibility(View.VISIBLE);

                if (infoType.equalsIgnoreCase("fertilizer")) {
                    holder.ivWeeklyCropQues.setImageResource(R.mipmap.ff);
                } else if (infoType.equalsIgnoreCase("irrigation")) {
                    holder.ivWeeklyCropQues.setImageResource(R.mipmap.ss);
                } else if (infoType.equalsIgnoreCase("harvest")) {
                    ivWeeklyCropQues.setImageResource(R.mipmap.kt);
                } else if (infoType.equalsIgnoreCase("flowering")) {
                } else if (infoType.equalsIgnoreCase("sowing")) {
                } else if (infoType.equalsIgnoreCase("soil_type")) {
                } else if (infoType.equalsIgnoreCase("spacing")) {
                } else if (infoType.equalsIgnoreCase("yield")) {
                } else if (infoType.equalsIgnoreCase("land_preparation")) {
                    holder.edtProduct.setHint(homeActivity.getResources().getString(R.string.str_farm_area));
                } else {
                    holder.ivWeeklyCropQues.setImageResource(R.mipmap.story_redi);
                }

                if (inputType.equalsIgnoreCase("button")) {
                    holder.llButtonView.setVisibility(View.VISIBLE);
                    holder.llSelectionView.setVisibility(View.GONE);
                    holder.llEditView.setVisibility(View.GONE);
                } else if (inputType.equalsIgnoreCase("dropdown")) {
                    holder.llButtonView.setVisibility(View.GONE);
                    holder.llSelectionView.setVisibility(View.VISIBLE);
                    holder.llEditView.setVisibility(View.GONE);
                } else if (inputType.equalsIgnoreCase("multi_dropdown")) {
                    holder.llButtonView.setVisibility(View.GONE);
                    holder.llSelectionView.setVisibility(View.VISIBLE);
                    holder.llEditView.setVisibility(View.GONE);
                } else if (inputType.equalsIgnoreCase("text")) {
                    holder.llButtonView.setVisibility(View.GONE);
                    holder.llSelectionView.setVisibility(View.GONE);
                    holder.llEditView.setVisibility(View.VISIBLE);
                }

                holder.tvDarjKare1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Functions.doubleTapCheck(mLastLongClick)) {
                            return;
                        }
                        mLastLongClick = SystemClock.elapsedRealtime();

                        if (InternetConnection.checkConnection(homeActivity)) {
                            if (TextUtils.isEmpty(edtProduct.getText().toString().trim())) {
                                if (infoType.equalsIgnoreCase("land_preparation")) {
                                    Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_land_for_farming));
                                } else {
                                    Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_enter_production));
                                }
                                return;
                            }

                            if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                setOfflineStoryPragati(weekCropDay, infoType, infoText, edtProduct.getText().toString().trim(), getResources().getString(R.string.str_ha));
                            } else {
                                setOnlineStoryPragati(weekCropDay, infoType, infoText, edtProduct.getText().toString().trim(), getResources().getString(R.string.str_ha));
                            }
                        } else {
                            setOfflineStoryPragati(weekCropDay, infoType, infoText, edtProduct.getText().toString().trim(), getResources().getString(R.string.str_ha));
                        }

                        if (listData.size() >= 2) {
                            listData.remove(position);
                            notifyDataSetChanged();
                        } else {
                            dialog.dismiss();
                            openCropInfoDhanyawadNew();
                        }
                    }
                });

                ArrayList<String> listdata = new ArrayList<String>();
                JSONArray optionsArray = new JSONArray(listData.get(position).getOptions());
                if (optionsArray != null && optionsArray.length() > 0) {
                    for (int i = 0; i < optionsArray.length(); i++) {
                        listdata.add(optionsArray.getString(i));
                    }
                    holder.spinnerOptions.setAdapter(new SpinnerArrayAdapter(homeActivity, R.layout.spinner_item_list, listdata));
                    holder.spinnerOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            options = parent.getItemAtPosition(position).toString();
                            Log.v("Selected Option: ", "" + options);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                }

                holder.tvDarjKare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Functions.doubleTapCheck(mLastLongClick)) {
                            return;
                        }
                        mLastLongClick = SystemClock.elapsedRealtime();

                        if (InternetConnection.checkConnection(homeActivity)) {
                            if (TextUtils.isEmpty(options)) {
                                Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_please_select_option));
                                return;
                            }

                            if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                setOfflineStoryPragati(weekCropDay, infoType, infoText, options, getResources().getString(R.string.str_ha));
                            } else {
                                setOnlineStoryPragati(weekCropDay, infoType, infoText, options, getResources().getString(R.string.str_ha));
                            }
                        } else {
                            setOfflineStoryPragati(weekCropDay, infoType, infoText, options, getResources().getString(R.string.str_ha));
                        }

                        if (listData.size() >= 2) {
                            listData.remove(position);
                            notifyDataSetChanged();
                        } else {
                            dialog.dismiss();
                            openCropInfoDhanyawadNew();
                        }

                    }
                });

                holder.tvHa.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Functions.doubleTapCheck(mLastLongClick)) {
                            return;
                        }
                        mLastLongClick = SystemClock.elapsedRealtime();

                        if (InternetConnection.checkConnection(homeActivity)) {
                            if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                setOfflineStoryPragati(weekCropDay, infoType, infoText, Functions.getCurrentDateTime(), getResources().getString(R.string.str_ha));
                            } else {
                                setOnlineStoryPragati(weekCropDay, infoType, infoText, Functions.getCurrentDateTime(), getResources().getString(R.string.str_ha));
                            }
                        } else {
                            setOfflineStoryPragati(weekCropDay, infoType, infoText, Functions.getCurrentDateTime(), getResources().getString(R.string.str_ha));
                        }

                        if (listData.size() >= 2) {
                            listData.remove(position);
                            notifyDataSetChanged();
                        } else {
                            dialog.dismiss();
                            openCropInfoDhanyawadNew();
                        }
                    }
                });

                holder.tvNa.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Functions.doubleTapCheck(mLastLongClick)) {
                            return;
                        }
                        mLastLongClick = SystemClock.elapsedRealtime();

                        if (InternetConnection.checkConnection(homeActivity)) {
                            if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                setOfflineStoryPragati(weekCropDay, infoType, infoText, "", getResources().getString(R.string.str_na));
                            } else {
                                setOnlineStoryPragati(weekCropDay, infoType, infoText, "", getResources().getString(R.string.str_na));
                            }
                        } else {
                            setOfflineStoryPragati(weekCropDay, infoType, infoText, "", getResources().getString(R.string.str_na));
                        }

                        if (listData.size() >= 2) {
                            listData.remove(position);
                            notifyDataSetChanged();
                        } else {
                            dialog.dismiss();
                            openCropInfoDhanyawadNew();
                        }
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return listData.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            AppCompatTextView tvKaryaReward, tvQuestions, tvHa, tvNa, tvDarjKare, tvDarjKare1;
            AppCompatEditText edtProduct;
            LinearLayout llButtonView, llSelectionView, llEditView, llView;
            ImageView ivWeeklyCropQues;
            Spinner spinnerOptions;

            public ViewHolder(View itemView) {
                super(itemView);
                this.tvKaryaReward = itemView.findViewById(R.id.tvKaryaReward);
                this.tvQuestions = itemView.findViewById(R.id.tvQuestions);
                this.llButtonView = itemView.findViewById(R.id.llButtonView);
                this.tvHa = itemView.findViewById(R.id.tvHa);
                this.tvNa = itemView.findViewById(R.id.tvNa);
                this.llEditView = itemView.findViewById(R.id.llEditView);
                this.llSelectionView = itemView.findViewById(R.id.llSelectionView);
                this.ivWeeklyCropQues = itemView.findViewById(R.id.ivWeeklyCropQues);
                this.spinnerOptions = (Spinner) itemView.findViewById(R.id.spinnerOptions);
                this.tvDarjKare = itemView.findViewById(R.id.tvDarjKare);
                this.tvDarjKare1 = itemView.findViewById(R.id.tvDarjKare1);
                this.edtProduct = itemView.findViewById(R.id.edtProduct);
                this.llView = itemView.findViewById(R.id.llView);
            }
        }
    }

    void setOfflineStoryPragati(final String id, String infoType, String infoText, String infoValue, String metaData) {
        try {
            List<OfflineWeeklyCropInfoData> weeklyCropInfoArray = new ArrayList<OfflineWeeklyCropInfoData>();
            OfflineWeeklyCropInfoData weeklyCropInfo = new OfflineWeeklyCropInfoData();
            weeklyCropInfo.set_id(new Random().nextInt());
            weeklyCropInfo.setUser_id(SharedPreferencesUtils.getUserId(homeActivity));
            weeklyCropInfo.setP_crop_id(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""));
            weeklyCropInfo.setSeason_id(sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""));
            weeklyCropInfo.setCrop_week(sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, ""));
            weeklyCropInfo.setCrop_id(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""));
            weeklyCropInfo.setField_id(sharedPreferences.getString(Constants.SharedPreferences_Field_ID, ""));
            weeklyCropInfo.setInfo_type(infoType);
            weeklyCropInfo.setInfo_text(infoText);
            weeklyCropInfo.setInfo_value(infoValue);
            weeklyCropInfo.setInfo_metadata(metaData);
            weeklyCropInfoArray.add(weeklyCropInfo);
            appDatabase.offlineWeeklyCropInfoDao().insertAll(weeklyCropInfoArray);

            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");

            List<WeeklyCropInfo> infoList = new ArrayList<WeeklyCropInfo>();
            WeeklyCropInfo cropInfo = new WeeklyCropInfo();
            cropInfo.set_id(new Random().nextInt());
            cropInfo.setCropDay(id);
            cropInfo.setCropWeek(sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, ""));
            cropInfo.setCropId(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""));
            cropInfo.setPcropId(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""));
            cropInfo.setIsDone("1");
            cropInfo.setStatus("1");
            cropInfo.setInfoTypo(infoType);
            infoList.add(cropInfo);
            appDatabase.weeklyCropInfoDao().insertAll(infoList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setOnlineStoryPragati(final String id, String infoType, String infoText, String infoValue, String metaData) {
        RetrofitClient.getInstance().getApi().updateWeeklyCropInfo(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                Functions.getDeviceid(homeActivity), SharedPreferencesUtils.getUserId(homeActivity),
                sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, ""),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_Field_ID, ""),
                infoType, infoText, infoValue, metaData).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");

                    List<WeeklyCropInfo> infoList = new ArrayList<WeeklyCropInfo>();
                    WeeklyCropInfo cropInfo = new WeeklyCropInfo();
                    cropInfo.set_id(new Random().nextInt());
                    cropInfo.setCropDay(id);
                    cropInfo.setCropWeek(sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, ""));
                    cropInfo.setCropId(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""));
                    cropInfo.setPcropId(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""));
                    cropInfo.setIsDone("1");
                    cropInfo.setStatus("1");
                    cropInfo.setInfoTypo(infoType);
                    infoList.add(cropInfo);
                    appDatabase.weeklyCropInfoDao().insertAll(infoList);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void openCropInfoDhanyawadNew() {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_crop_info_dhanyawad, null);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setCanceledOnTouchOutside(false);

        AppCompatTextView tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText(RewardPoints.getWeeklyCropInfoRewardPoint(sharedPreferences));

        AppCompatTextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));

        AppCompatTextView txtOfflineSMS = view.findViewById(R.id.txtOfflineSMS);
        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
            txtOfflineSMS.setVisibility(View.GONE);
        } else {
            txtOfflineSMS.setVisibility(View.VISIBLE);
        }

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        AppCompatTextView tvShareWhatapp = view.findViewById(R.id.tvShareWhatapp);
        tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "WhatsApp");
                    Functions.shareWhatAppIntent(homeActivity);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(homeActivity, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        AppCompatTextView tvShareCopy = view.findViewById(R.id.tvShareCopy);
        tvShareCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "SMS");
                    Functions.shareSMSIntent(homeActivity, RongoApp.getReferralText(homeActivity));

//                    Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
//                    smsIntent.setType("vnd.android-dir/mms-sms");
//                    smsIntent.putExtra("address", "");
//                    smsIntent.putExtra("sms_body", "" + HtmlCompat.fromHtml(sharedPreferences.getString(Constants.SharedPreference_Referral_Link, ""), 0));
//                    smsIntent.setFlags(android.content.Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(smsIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AppCompatTextView tvSharefacebook = view.findViewById(R.id.tvSharefacebook);
        tvSharefacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.setUserScreenTrackEvent(appDatabase, "Invite", "Facebook");
                Functions.shareFacebookIntent(homeActivity);
            }
        });

        AppCompatTextView tvShareMore = view.findViewById(R.id.tvShareMore);
        tvShareMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.performShare(homeActivity, appDatabase);
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));

        bottomSheetDialog.show();
    }


    void shareStoryDialog() {
        try {
            Functions.setFirebaseLogEventTrack(getActivity(), Constants.ET_STORY_TRACK, Constants.ET_INVITE,
                    "" + RongoApp.getUserId(), Constants.ET_INVITE + "_" + RongoApp.getUserId());
            Functions.setUserScreenTrackEvent(appDatabase, Constants.ET_STORY_TRACK, Constants.ET_INVITE + "_" + RongoApp.getUserId());

            Dialog dialog = new Dialog(homeActivity, R.style.StoryDialog);
            dialog.setContentView(R.layout.story_share_dialog);
            dialog.show();

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(lp);

            ImageView ivBack = dialog.findViewById(R.id.ivClose);
            ivBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            TextView tvBack = dialog.findViewById(R.id.tvBack);
            tvBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    DialogFragment newFragment = SavdhaniFragment.newInstance();
                    newFragment.show(ft, "dialog");
                    dialog.dismiss();
                }
            });
            TextView tvNext = dialog.findViewById(R.id.tvNext);
            tvNext.setVisibility(View.GONE);

            ImageView ivStoryInvite = dialog.findViewById(R.id.ivStoryInvite);

            AppCompatTextView tvShareWhatapp = dialog.findViewById(R.id.tvShareWhatapp);
            tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    Functions.setFirebaseLogEventTrack(homeActivity, Constants.Screen_Track, "et_Share_whatsApp", "" + RongoApp.getUserId(), "WhatsApp");
                    Functions.setUserScreenTrackEvent(appDatabase, "et_Share_whatsApp", "" + RongoApp.getUserId());

                    Functions.shareWhatAppIntent(homeActivity);
                }
            });

            AppCompatTextView tvShareCopy = dialog.findViewById(R.id.tvShareCopy);
            tvShareCopy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    Functions.setFirebaseLogEventTrack(homeActivity, Constants.Screen_Track, "et_Share_sms", "" + RongoApp.getUserId(), "SMS");
                    Functions.setUserScreenTrackEvent(appDatabase, "et_Share_sms", "" + RongoApp.getUserId());

                    Functions.shareSMSIntent(homeActivity, RongoApp.getReferralText(homeActivity));
                }
            });

            AppCompatTextView tvSharefacebook = dialog.findViewById(R.id.tvSharefacebook);
            tvSharefacebook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    Functions.setFirebaseLogEventTrack(homeActivity, Constants.Screen_Track, "et_Share_facebook", "" + RongoApp.getUserId(), "Facebook");
                    Functions.setUserScreenTrackEvent(appDatabase, "et_Share_facebook", "" + RongoApp.getUserId());

                    Functions.shareFacebookIntent(homeActivity);
                }
            });

            AppCompatTextView tvShareMore = dialog.findViewById(R.id.tvShareMore);
            tvShareMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    Functions.setFirebaseLogEventTrack(homeActivity, Constants.Screen_Track, "et_Share_moreShare", "" + RongoApp.getUserId(), "More Share");
                    Functions.setUserScreenTrackEvent(appDatabase, "et_Share_moreShare", "" + RongoApp.getUserId());

                    Functions.performShare(homeActivity, appDatabase);
                }
            });

            RongoApp.engagementToolActivity(Constants.ET_INVITE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void appUpdateDialog() {
        try {
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_App_Update, "0");
            Dialog dialog = new Dialog(homeActivity, R.style.NewDialog);
            dialog.setContentView(R.layout.app_update_dialog);
            dialog.show();

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);

            ImageView imgClose = dialog.findViewById(R.id.imgClose);
            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            AppCompatTextView tvUpdate = dialog.findViewById(R.id.tvUpdate);
            tvUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    homeActivity.setInAppUpdate();
                    dialog.dismiss();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void updateScreenData() {
        try {
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_USER_CROP_WEEK, String.valueOf(RongoApp.getCropWeeksFunction()));
            if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                    ProfileResultData profileResultData = Functions.getData();
                    if (profileResultData != null) {
                        displayData(profileResultData);
                    }
                    fetchDataFromAssetFileForWeeklyAdvisory();
                    generateOptions();
                } else {
                    ProfileResultData profileResultDataMie = Functions.getData();
                    String expiryContent = "", profileExpire = "";
                    if (profileResultDataMie != null) {
                        expiryContent = profileResultDataMie.getApi_content_expiry();
                        profileExpire = profileResultDataMie.getApi_profile_expiry();
                    }
                    if (!TextUtils.isEmpty(profileExpire)) {
                        long expiryContentLong = Functions.covertToMili("yyyy-MM-dd HH:mm:ss", profileExpire);
                        if (expiryContentLong > sharedPreferences.getLong(Constants.SharedPreference_Time_FOR_API_CALL_PROFILE, 0)) {
                            ProfileResultData profileResultData = Functions.getData();
                            if (profileResultData != null) {
                                displayData(profileResultData);
                            }
                        } else {
                            getProfile();
                        }
                    } else {
                        fetchDataFromAssetFileForWeeklyAdvisory();
                        getProfile();
                    }

                    List<CropAreaFocusedData> cropAreaFocusedData = SharedPreferencesUtils.getCropArea(homeActivity, Constants.SharedPreferences_CROP_FOCUSED_AREA);
                    if (cropAreaFocusedData == null || cropAreaFocusedData.size() == 0) {
                        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                            getMasterDropDownData();
                        }
                    }

                    if (!TextUtils.isEmpty(expiryContent)) {
                        long expiryContentLong = Functions.covertToMili("yyyy-MM-dd HH:mm:ss", expiryContent);
                        if (expiryContentLong > sharedPreferences.getLong(Constants.SharedPreference_Time_FOR_API_CALL, 0)) {
                            isFirstTime = true;
                            fetchDataFromAssetFileForWeeklyAdvisory();
                            generateOptions();
                        } else {
                            getFertilizerInfo("");
                        }
                    } else {
                        getFertilizerInfo("");
                    }
                }

                if (TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreference_Community_Banner, ""))) {
                    getContentData();
                }
                getPragatiData();
                getSessionEndData();
            } else {
                ProfileResultData profileResultData = Functions.getData();
                if (profileResultData != null) {
                    displayData(profileResultData);
                }
                fetchDataFromAssetFileForWeeklyAdvisory();
                generateOptions();
            }

            setCropHealthStatus();
            setCommunityData();

            if (!TextUtils.isEmpty(RongoApp.getWeeklyCropQuestion())) {
                getWeeklyCropInfoQuestionData();
            } else {
                getWeeklyCropInfoQuestion();
            }

            if (TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_FertilizerInfo, ""))) {
                getFertilizerInfo("");
            }

//            String date = sharedPreferences.getString(Constants.SharedPreference_ASK_Helpful_Date, "");
//            if (!TextUtils.isEmpty(date)) {
//                try {
//                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
//                    Date currentDate = sdf.parse(Functions.getAfterWeekDate());
//                    if (Objects.requireNonNull(currentDate).after(sdf.parse(date))) {
//                        askUserForInformation();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            } else {
//                if (sharedPreferences.getString(Constants.SharedPreference_IsAction, "N").equalsIgnoreCase("Y")) {
//                    askUserForInformation();
//                }
//            }

            seedTypeData = SharedPreferencesUtils.getSeedTypes(homeActivity, Constants.SharedPreferences_SEED_TYPES);
            if (seedTypeData == null || seedTypeData.size() == 0) {
                if (InternetConnection.checkConnectionForFasl(homeActivity) && InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                    getMasterDropDownData();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() {
        llSessionStart = view.findViewById(R.id.llSessionStart);
        llSessionEnd = view.findViewById(R.id.llSessionEnd);
        llEndRewardView = view.findViewById(R.id.llEndRewardView);
        bunaiStatusCard = view.findViewById(R.id.bunaiStatusCard);

        llWeather = view.findViewById(R.id.llWeather);

        llRokdhamTagView = view.findViewById(R.id.llRokdhamTagView);
        llDieaseTagView = view.findViewById(R.id.llDieaseTagView);

        llAdvisory = view.findViewById(R.id.llAdvisory);
        llSavdhani = view.findViewById(R.id.llSavdhani);
        llChemical = view.findViewById(R.id.llChemical);
        llFertilizer = view.findViewById(R.id.llFertilizer);
//        cardGeneralAdvisory = view.findViewById(R.id.cardGeneralAdvisory);
//        cardViewChemical = view.findViewById(R.id.cardViewChemical);
//        cardFertilizer = view.findViewById(R.id.cardFertilizer);
//        cardSavdhani = view.findViewById(R.id.cardSavdhani);

        cardfeedback = view.findViewById(R.id.cardfeedback);
        tvSessionEndOfflineMsg = view.findViewById(R.id.tvSessionEndOfflineMsg);
        rcvSessionFeedback = view.findViewById(R.id.rcvSessionFeedback);
        llRewardList = view.findViewById(R.id.llRewardList);
        tvEndQuiz = view.findViewById(R.id.tvEndQuiz);
        tvEndExpert = view.findViewById(R.id.tvEndExpert);
        tvEndReward = view.findViewById(R.id.tvEndReward);
        tvEndPhotos = view.findViewById(R.id.tvEndPhotos);
        tvFeedbackSubmit = view.findViewById(R.id.tvFeedbackSubmit);

        llWeatherOtherDetail = view.findViewById(R.id.llWeatherOtherDetail);
        llWeatherNoInternet = view.findViewById(R.id.llWeatherNoInternet);

        rcvWeather = view.findViewById(R.id.rcvWeather);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(homeActivity, RecyclerView.HORIZONTAL, false);
        rcvWeather.setLayoutManager(linearLayoutManager);
        rcvWeather.addItemDecoration(new com.rongoapp.views.DividerItemDecoration(homeActivity, DividerItemDecoration.HORIZONTAL, false));

        relativeBuaiCard = view.findViewById(R.id.relativeBuaiCard);
        txtYes = view.findViewById(R.id.txtYes);
        txtNo = view.findViewById(R.id.txtNo);
        rlHealthView = view.findViewById(R.id.rlHealthView);

        relRewardView = view.findViewById(R.id.relRewardView);
        tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
        tvRewardPointBuvayDarj = view.findViewById(R.id.tvRewardPointBuvayDarj);
        tvRewardPointBuvayDarj.setText(RewardPoints.getSowingDateRewardPoint(sharedPreferences));
        pbChemical = view.findViewById(R.id.pbChemical);
        pbJankari = view.findViewById(R.id.pbJankari);
        textRedDot = view.findViewById(R.id.textRedDot);
        txtTitle = view.findViewById(R.id.txtTitle);
        toolbar = view.findViewById(R.id.toolbar);
        relativeArrow = view.findViewById(R.id.relativeArrow);
        rbSSP = view.findViewById(R.id.rbSSP);
        rbDAP = view.findViewById(R.id.rbDAP);
        linearQuestions = view.findViewById(R.id.linearQuestions);
        linearQuestions.setVisibility(View.GONE);
        stageViewPager = view.findViewById(R.id.stageViewPager);
        txtPratiEkad = view.findViewById(R.id.txtPratiEkad);
        editOptions = view.findViewById(R.id.ediOption);
        imgPlay = view.findViewById(R.id.imgPlay);
        imgPlayJankari = view.findViewById(R.id.imgPlayJankari);
        relJalBharav = view.findViewById(R.id.relJalBharav);
        txtWeatherAlert = view.findViewById(R.id.txtWeatherAlert);
        rvOptions = view.findViewById(R.id.rvOptions);
        editTextPhonenumber = view.findViewById(R.id.editTextPhonenumber);
        txtInvitation = view.findViewById(R.id.txtInvitation);
        rvOptions.setLayoutManager(new LinearLayoutManager(homeActivity, RecyclerView.VERTICAL, false));

        llShareReward = view.findViewById(R.id.llShareReward);
        tvShareCode = view.findViewById(R.id.tvShareCode);
        tvPointsInvite = view.findViewById(R.id.tvPointsInvite);
        tvShareWhatapp = view.findViewById(R.id.tvShareWhatapp);
        tvSharefacebook = view.findViewById(R.id.tvSharefacebook);
        tvShareCopy = view.findViewById(R.id.tvShareCopy);
        tvShareMore = view.findViewById(R.id.tvShareMore);
        setInviteRewardView();

        rlSamanySalah = view.findViewById(R.id.rlSamanySalah);
        linearGo = view.findViewById(R.id.linearGo);
        txtUsername = view.findViewById(R.id.txtUsername);
        imgNext = view.findViewById(R.id.imgNext);
        imgPrevios = view.findViewById(R.id.imgPrevios);
        rgJankari = view.findViewById(R.id.rgJankari);

        txtChecmical = view.findViewById(R.id.txtChecmical);
        rvChemical = view.findViewById(R.id.rvChemical);
        rvChemical.setLayoutManager(new LinearLayoutManager(homeActivity, RecyclerView.VERTICAL, false));
        rvChemical.setNestedScrollingEnabled(false);

        txtChecmicalName = view.findViewById(R.id.txtChecmicalName);
        txtChecmicalName.setVisibility(View.GONE);
        txtChecmicalName.setPaintFlags(txtChecmicalName.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtChecmicalName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openChemicalNameDialog();
            }
        });

        linearDiases = view.findViewById(R.id.linearDiases);
        linearNutrition = view.findViewById(R.id.linearNutrition);
        linearPest = view.findViewById(R.id.linearPest);
        txtQuestion = view.findViewById(R.id.txtQuestion);

        tvRongoHelpLineNumber = view.findViewById(R.id.tvRongoHelpLineNumber);

        rlWeatherView = view.findViewById(R.id.rlWeatherView);
        cardWeatherData = view.findViewById(R.id.cardWeatherData);
        rlWeatherView.setVisibility(View.VISIBLE);
        cardWeatherData.setVisibility(View.VISIBLE);

        rlMoreWeatherData = view.findViewById(R.id.rlMoreWeatherData);

        llTodayView = view.findViewById(R.id.llTodayView);
        llWeekView = view.findViewById(R.id.llWeekView);

        tvTodayWeatherDistrict = view.findViewById(R.id.tvTodayWeatherDistrict);
        tvClick = view.findViewById(R.id.tvClick);
        tvClick.setCompoundDrawablesWithIntrinsicBounds(null, null, homeActivity.getResources().getDrawable(R.drawable.arrow_down), null);

        llWeekView.setVisibility(View.GONE);
        llTodayView.setVisibility(View.VISIBLE);

        ivTag = view.findViewById(R.id.ivTag);
        if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.GUJARATI)) {
            ivTag.setImageResource(R.mipmap.tag_gu);
        } else {
            ivTag.setImageResource(R.mipmap.tag_hi);
        }

        tvClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.setFirebaseLogEventTrack(homeActivity, Constants.Screen_Track, "Phasal", RongoApp.getUserId(), "Weather");
                if (llWeekView.getVisibility() == View.VISIBLE) {
                    llWeekView.setVisibility(View.GONE);
                    llTodayView.setVisibility(View.VISIBLE);
                    tvClick.setCompoundDrawablesWithIntrinsicBounds(null, null, homeActivity.getResources().getDrawable(R.drawable.arrow_down), null);
                } else {
                    llWeekView.setVisibility(View.VISIBLE);
                    llTodayView.setVisibility(View.VISIBLE);
                    tvClick.setCompoundDrawablesWithIntrinsicBounds(null, null, homeActivity.getResources().getDrawable(R.drawable.arrow_up), null);
                }
            }
        });

        tvTodayWeatherDate = view.findViewById(R.id.tvTodayWeatherDate);
        tvTodayWeatherMinMax = view.findViewById(R.id.tvTodayWeatherMinMax);

        ivWeatherWarning = view.findViewById(R.id.ivWeatherWarning);
        tvWeatherWarningTxt = view.findViewById(R.id.tvWeatherWarningTxt);

        ivWeatherExp = view.findViewById(R.id.ivWeatherExp);
        tvWeatherExpText = view.findViewById(R.id.tvWeatherExpText);
        tvWeatherMsgNew = view.findViewById(R.id.tvWeatherMsgNew);

        llRongoHelpLineNumber = view.findViewById(R.id.llRongoHelpLineNumber);
        llRongoHelpLineNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreference_RongoHelpLineNumber, ""))) {
                    logCalledRongoHelplineEvent(Functions.getCurrentDateTime(), SharedPreferencesUtils.getUserId(homeActivity));
                    Functions.setFirebaseLogEventTrack(homeActivity, Constants.Screen_Track, "Phasal", "", "Help Line number");

                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + sharedPreferences.getString(Constants.SharedPreference_RongoHelpLineNumber, "")));
                    startActivity(intent);
                }
            }
        });

        cardLuckYDraw = view.findViewById(R.id.cardLuckYDraw);
        cardHealthStatus = view.findViewById(R.id.cardHealthStatus);
        cardHealthStatus.setVisibility(View.GONE);
        txtUplaod = view.findViewById(R.id.txtUplaod);
        txtHealthy = view.findViewById(R.id.txtHealthy);
        txtUnhealthy = view.findViewById(R.id.txtUnhealthy);

        AppCompatTextView tvPointsWeeklyCropHealth = view.findViewById(R.id.tvPointsWeeklyCropHealth);
        tvPointsWeeklyCropHealth.setText(RewardPoints.getCropHealthRewardPoint(sharedPreferences));

        AppCompatTextView tvPointsUploadPicture = view.findViewById(R.id.tvPointsUploadPicture);
        tvPointsUploadPicture.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences));

        AppCompatTextView tvPointsSurvey = view.findViewById(R.id.tvPointsSurvey);
        tvPointsSurvey.setText(RewardPoints.getSurveyRewardPoint(sharedPreferences) + " " + homeActivity.getResources().getString(R.string.str_points));

        AppCompatTextView tvRewardPointCropInfoQues = view.findViewById(R.id.tvRewardPointCropInfoQues);
        tvRewardPointCropInfoQues.setText(RewardPoints.getWeeklyCropInfoRewardPoint(sharedPreferences));
        tvQuestions = view.findViewById(R.id.tvQuestions);
        llButtonView = view.findViewById(R.id.llButtonView);
        tvHa = view.findViewById(R.id.tvHa);
        tvNa = view.findViewById(R.id.tvNa);
        llEditView = view.findViewById(R.id.llEditView);
        llSelectionView = view.findViewById(R.id.llSelectionView);
        ivWeeklyCropQues = view.findViewById(R.id.ivWeeklyCropQues);
        spinnerOptions = (Spinner) view.findViewById(R.id.spinnerOptions);
        tvDarjKare = view.findViewById(R.id.tvDarjKare);
        tvDarjKare1 = view.findViewById(R.id.tvDarjKare1);
        edtProduct = view.findViewById(R.id.edtProduct);

        tvLegalText = view.findViewById(R.id.tvLegalText);

        tvInviteMsg = view.findViewById(R.id.tvInviteMsg);
        tvInviteMsg.setText(" " + sharedPreferences.getString(Constants.SharedPreference_Invite_Msg, ""));

//        tvSendToCommunity = view.findViewById(R.id.tvSendToCommunity);
//        tvSendToCommunity.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                HomeActivity.mCircleNavigationView.changeCurrentItem(2);
//            }
//        });

        cardCommunityView = view.findViewById(R.id.cardCommunityView);
        rlCommunityNext = view.findViewById(R.id.rlCommunityNext);
        rlCommunityNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(post_id) && InternetConnection.checkConnectionForFasl(homeActivity)) {
                    Intent intent = new Intent(homeActivity, CommunityDetailsActivity.class);
//                    intent.putExtra("postData", "");
//                    intent.putExtra("imagePath", "");
                    intent.putExtra("type", "0");
                    intent.putExtra("post_id", post_id);
                    homeActivity.startActivity(intent);
                }
            }
        });
        tvGoCommunity = view.findViewById(R.id.tvGoCommunity);
        tvGoCommunity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HomeActivity.mCircleNavigationView.changeCurrentItem(2);
            }
        });

        setSessionEndView();

        rcvMyPager = (RecyclerView) view.findViewById(R.id.rcvMyPager);
        topSticky();
    }

    @Override
    public void setListener() {
        stageViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(final int position) {
                if (!userInteraction) {
                    userInteraction = true;
                    return;
                }
                Log.v("stagePos", "" + position);
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_USER_CROP_WEEK, String.valueOf(position));

                if (position >= Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"))) {
                    llSessionEnd.setVisibility(View.VISIBLE);
                    llSessionStart.setVisibility(View.GONE);
                    setSessionEndData();
                } else {
                    llSessionStart.setVisibility(View.VISIBLE);
                    llSessionEnd.setVisibility(View.GONE);
                    getWeeklyCropInfoQuestion();
                }

                TTSAudioPlayer.getInstance().stopAll();
                setView(position);
                fetchDataFromAssetFileForWeeklyAdvisory();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        String content = rbDAP.getText().toString().trim() + " " + rbSSP.getText().toString().trim();
        imgPlayJankari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (homeActivity.tts != null) {
                    homeActivity.tts.speak(content, imgPlayJankari, pbJankari);
                } else {
                    HomeActivity.setTextToSpeechInit();
                    homeActivity.tts.speak(content, imgPlayJankari, pbJankari);
                }
            }
        });

        relJalBharav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                if (TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_WeeklyWeatherData, ""))) {
                    getWeeklyWeather(true);
                } else {
                    Intent i = new Intent(homeActivity, WeatherDetailsActivity.class);
                    i.putExtra("weather_alert", "");
                    startActivity(i);
                }
            }
        });

        editTextPhonenumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                Functions.performShare(homeActivity, appDatabase);
//                InviteFunction();
            }
        });

        txtInvitation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                Functions.performShare(homeActivity, appDatabase);
            }
        });

        linearGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                if (InternetConnection.checkConnection(homeActivity)) {
                    if (TextUtils.isEmpty(editOptions.getText().toString().trim())) {
                        Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_please_select_option));
                        return;
                    }

                    if (rvOptions.getVisibility() == View.VISIBLE) {
                        rvOptions.setVisibility(View.GONE);
                    }

                    surveyResponse();
                } else {
                    Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), homeActivity.getString(R.string.no_connection));
                }
            }
        });

        editOptions.setCursorVisible(false);
        editOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                if (rvOptions.getVisibility() == View.VISIBLE) {
                    rvOptions.setVisibility(View.GONE);
                } else {
                    rvOptions.setVisibility(View.VISIBLE);
                }
            }
        });

        linearPest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();
//                openSavdhani("Pest");
            }
        });

        linearDiases.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();
//                openSavdhani("Diases");
            }
        });

        linearNutrition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();
//                openSavdhani("Nutrition");
            }
        });

        rbSSP.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (compoundButton.isPressed()) {
                    rbDAP.setButtonDrawable(R.mipmap.uria_unselected);
                    rbSSP.setButtonDrawable(R.mipmap.uria_selected);
                    if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_FertilizerInfo, ""))) {
                        openUria("SSP");
                    } else {
                        getFertilizerInfo("SSP");
                    }
                }
            }
        });

        rbDAP.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (compoundButton.isPressed()) {
                    rbSSP.setButtonDrawable(R.mipmap.uria_unselected);
                    rbDAP.setButtonDrawable(R.mipmap.uria_selected);
                    if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_FertilizerInfo, ""))) {
                        openUria("DAP");
                    } else {
                        getFertilizerInfo("DAP");
                    }
                }
            }
        });

        audioPlayerListener = new TTS.AudioPlayerListener() {
            @Override
            public void audioDone(String status) {
//                int position = getNextPossibleItemIndex(1, viewPager);
//                viewPager.setCurrentItem(position);
                int position = getCurrentItem();
                if (position > 0) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            if (homeActivity.tts != null) {
                                homeActivity.tts.speak(Functions.fromHtml(generalAdvisory.get(position)).toString(), new ImageView(homeActivity), new ProgressBar(homeActivity), audioPlayerListener);
                            }
                        }
                    });
                }
            }
        };

        imgPrevios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                viewPager.setCurrentItem(getNextPossibleItemIndex(-1, viewPager));
                try {
                    if (rcvMyPager.getAdapter() == null)
                        return;

                    int position = getCurrentItem();
                    if (position > 0)
                        setCurrentItem(position - 1, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                viewPager.setCurrentItem(getNextPossibleItemIndex(1, viewPager));
                try {
                    if (rcvMyPager.getAdapter() == null)
                        return;

                    int position = getCurrentItem();
//                    if (position > rcvMyPager.getAdapter().getItemCount())
                    setCurrentItem(position + 1, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        txtYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();
                openSowingDate("Yes");
            }
        });

        txtNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();
                openSowingDate("No");
            }
        });

        tvRewardPoint.setText(Functions.updateRewardPoints(sharedPreferences, "0"));
        relRewardView = view.findViewById(R.id.relRewardView);
        relRewardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                Intent i = new Intent(homeActivity, RewardActivity.class);
                startActivity(i);
            }
        });

        cardAajKaQuestion = view.findViewById(R.id.cardAajKaQuestion);
        cardAajKaQuestion.setVisibility(View.GONE);

        cardNewCropAdd = view.findViewById(R.id.cardNewCropAdd);
        cardNewCropAdd.setVisibility(View.GONE);

        tvAddNewCrop = view.findViewById(R.id.tvAddNewCrop);
        tvAddNewCrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                addCropDialog("1");
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SessionEndCropID,
                        "" + RongoApp.getSelectedCrop());
                homeActivity.openCropSelectionDialog();
            }
        });

        setDailyQuestion();

        cardAajKaQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

//                Intent i = new Intent(homeActivity, QuestionActivity.class);
//                Intent i = new Intent(homeActivity, QuestionSampleActivity.class);

                if (!Functions.getQuizIntent(homeActivity, sharedPreferences)) {
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    DialogFragment newFragment = QuizOLDFragment.newInstance();
                    newFragment.show(ft, "QuizOLDFragment");
                } else {
                    FragmentTransaction ft = getFragmentManager().beginTransaction();
                    DialogFragment newFragment = QuizFragment.newInstance();
                    newFragment.show(ft, "dialog");
                }

//                Intent intent = null;
//                try {
//                    if (RongoApp.getSelectedCrop().equalsIgnoreCase("11")) {
//                        intent = new Intent(homeActivity, QuestionSampleActivity.class);
//                    } else {
//                        if (Functions.getQuizIntent(homeActivity, sharedPreferences)) {
//                            intent = new Intent(homeActivity, QuizActivity.class);
//                        } else {
//                            intent = new Intent(homeActivity, QuestionSampleActivity.class);
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                startActivity(intent);
            }
        });
    }

    void topSticky() {
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) toolbar.getLayoutParams();
        params.height = (int) Functions.convertDpToPixel(116, homeActivity);
        toolbar.setLayoutParams(params);

        RelativeLayout.LayoutParams paramsView = (RelativeLayout.LayoutParams) stageViewPager.getLayoutParams();
        paramsView.height = (int) Functions.convertDpToPixel(114, homeActivity);
        paramsView.setMargins(0, (int) Functions.convertDpToPixel(10, homeActivity), 0, 0);
        stageViewPager.setLayoutParams(paramsView);
    }

    void setNewSavdhaniView() {
        try {
            List<SavdhaniDataList> highRiskData = new ArrayList<SavdhaniDataList>();
            RecyclerView rcvSavdhani = view.findViewById(R.id.rcvSavdhani);
            rcvSavdhani.setLayoutManager(new LinearLayoutManager(homeActivity, RecyclerView.HORIZONTAL, false));
            List<WeeklyAdvisoryData> weeklyAdvisoryData = weeklyAdvisoryData();
            if (weeklyAdvisoryData.size() > 0) {
                try {
                    highRiskData = new ArrayList<SavdhaniDataList>();
                    List<PestData> mPestData = weeklyAdvisoryData.get(0).getPestDataList();
                    List<DiasesData> mDiaseData = weeklyAdvisoryData.get(0).getDiasesDataList();
                    List<NutritionData> mNutritionData = weeklyAdvisoryData.get(0).getNutretionList();

                    JSONObject resultData = new JSONObject(FetchJsonFromAssetsFile.loadHighKeywordJSONFromAsset(homeActivity));
                    if (resultData != null && resultData.length() > 0) {
                        Iterator<String> it = resultData.keys();
                        while (it.hasNext()) {
                            String key = it.next();
                            if (key.equalsIgnoreCase(RongoApp.getSelectedState().toLowerCase())) {
                                JSONObject result = resultData.optJSONObject(key);
                                JSONArray keywords = new JSONArray(result.optString("high"));
                                if (keywords != null && keywords.length() > 0) {

                                    ArrayList<String> HighRiskKeyWords = new ArrayList<String>();
                                    for (int i = 0; i < keywords.length(); i++) {
                                        HighRiskKeyWords.add(keywords.getString(i));
                                    }

                                    for (int j = 0; j < mPestData.size(); j++) {
                                        SavdhaniDataList savdhaniDataList = new SavdhaniDataList();
                                        savdhaniDataList.setName(mPestData.get(j).getName());
                                        savdhaniDataList.setStatus("1");
                                        savdhaniDataList.setKeyword_id(mPestData.get(j).getKeyword_id());
                                        if (HighRiskKeyWords.contains(mPestData.get(j).getKeyword_id())) {
                                            savdhaniDataList.setType("1");
                                        } else {
                                            savdhaniDataList.setType("0");
                                        }
                                        highRiskData.add(savdhaniDataList);
                                    }

                                    for (int j = 0; j < mDiaseData.size(); j++) {
                                        SavdhaniDataList savdhaniDataList = new SavdhaniDataList();
                                        savdhaniDataList.setName(mDiaseData.get(j).getName());
                                        savdhaniDataList.setStatus("2");
                                        savdhaniDataList.setKeyword_id(mDiaseData.get(j).getKeyword_id());
                                        if (HighRiskKeyWords.contains(mDiaseData.get(j).getKeyword_id())) {
                                            savdhaniDataList.setType("1");
                                        } else {
                                            savdhaniDataList.setType("0");
                                        }
                                        highRiskData.add(savdhaniDataList);
                                    }

                                    for (int j = 0; j < mNutritionData.size(); j++) {
                                        SavdhaniDataList savdhaniDataList = new SavdhaniDataList();
                                        savdhaniDataList.setName(mNutritionData.get(j).getName());
                                        savdhaniDataList.setStatus("3");
                                        savdhaniDataList.setKeyword_id(mNutritionData.get(j).getKeyword_id());
                                        if (HighRiskKeyWords.contains(mNutritionData.get(j).getKeyword_id())) {
                                            savdhaniDataList.setType("1");
                                        } else {
                                            savdhaniDataList.setType("0");
                                        }
                                        highRiskData.add(savdhaniDataList);
                                    }
                                } else {
                                    for (int j = 0; j < mPestData.size(); j++) {
                                        SavdhaniDataList savdhaniDataList = new SavdhaniDataList();
                                        savdhaniDataList.setName(mPestData.get(j).getName());
                                        savdhaniDataList.setStatus("1");
                                        savdhaniDataList.setKeyword_id(mPestData.get(j).getKeyword_id());
                                        savdhaniDataList.setType("0");
                                        highRiskData.add(savdhaniDataList);
                                    }

                                    for (int j = 0; j < mDiaseData.size(); j++) {
                                        SavdhaniDataList savdhaniDataList = new SavdhaniDataList();
                                        savdhaniDataList.setName(mDiaseData.get(j).getName());
                                        savdhaniDataList.setStatus("2");
                                        savdhaniDataList.setKeyword_id(mDiaseData.get(j).getKeyword_id());
                                        savdhaniDataList.setType("0");
                                        highRiskData.add(savdhaniDataList);
                                    }

                                    for (int j = 0; j < mNutritionData.size(); j++) {
                                        SavdhaniDataList savdhaniDataList = new SavdhaniDataList();
                                        savdhaniDataList.setName(mNutritionData.get(j).getName());
                                        savdhaniDataList.setStatus("3");
                                        savdhaniDataList.setKeyword_id(mNutritionData.get(j).getKeyword_id());
                                        savdhaniDataList.setType("0");
                                        highRiskData.add(savdhaniDataList);
                                    }
                                }
                            }
                        }
                    }

                    Collections.sort(highRiskData, new Comparator<SavdhaniDataList>() {
                        public int compare(SavdhaniDataList o1, SavdhaniDataList o2) {
                            return o1.getType().compareTo(o2.getType());
                        }
                    });
                    Collections.reverse(highRiskData);

                    if (highRiskData.size() > 0) {
                        llAdvisory.setVisibility(View.VISIBLE);
                        llDieaseTagView.setVisibility(View.VISIBLE);
                        savdhaniBadge.setVisibility(View.VISIBLE);
                    } else {
                        savdhaniBadge.setVisibility(View.GONE);
                        llAdvisory.setVisibility(View.GONE);
                        llDieaseTagView.setVisibility(View.GONE);
                    }

//                    Log.e("highRisk-Size: ", "" + highRiskData.size());
                    rcvSavdhani.setAdapter(new HighRiskSavdhaniAdapter(homeActivity, highRiskData));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                llAdvisory.setVisibility(View.GONE);
                savdhaniBadge.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void openChemicalNameDialog() {
        try {
            BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
            View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_dialog_chemical_name, null);
            bottomSheetDialog.setContentView(view);

            ImageView imgClose = view.findViewById(R.id.imgClose);
            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    bottomSheetDialog.dismiss();
                }
            });

            ArrayList<ChemicalNameModel> chemicalNameModels = new ArrayList<ChemicalNameModel>();
            if (chemical_info.length() > 0) {
                for (int i = 0; i < chemical_info.length(); i++) {
                    JSONObject object = chemical_info.optJSONObject(i);
                    try {
                        chemicalNameModels.add(new ChemicalNameModel(object.optString("technical"),
                                Functions.getString(object.getJSONArray("normal"))));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            RecyclerView rcvChemicalName = view.findViewById(R.id.rcvChemicalName);
            rcvChemicalName.setLayoutManager(new LinearLayoutManager(homeActivity));
            rcvChemicalName.setAdapter(new ChemicalNameAdapter(chemicalNameModels));

            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
            ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
            bottomSheetDialog.show();
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        try {
            TTSAudioPlayer.getInstance().stopAll();
            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        try {
            getActivity().unregisterReceiver(UPDATE_CROP_INFO);

            TTSAudioPlayer.getInstance().stopAll();
            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            Log.e("onResume2", "onResume");
            if (progressBarDialog != null) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_USER_CROP_WEEK,
                    String.valueOf(RongoApp.getCropWeeksFunction()));
            tvRewardPoint.setText(Functions.updateRewardPoints(sharedPreferences, "0"));
            getWeeklyCropInfoQuestion();
            setDailyQuestion();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onBackPressed() {
        if (true) {
            //action not popBackStack
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onDestroy() {
        try {
            TTSAudioPlayer.getInstance().stopAll();
            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    void logCalledRongoHelplineEvent(String datetime, String user_id) {
        Bundle params = new Bundle();
        params.putString(Constants.EVENT_PARAM_DATE_TIME, datetime);
        params.putString(Constants.EVENT_PARAM_USER_ID, user_id);
        AppEventsLogger.newLogger(homeActivity).logEvent(Constants.FB_EVENT_NAME_CONTACT, params);
    }

    void getSessionEndData() {
        if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_Session_End_Data, ""))) {
            return;
        }
        RetrofitClient.getInstance().getApi().getSeasonEndSummary(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), SharedPreferencesUtils.getUserId(homeActivity),
                sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_Field_ID, "")).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String r = response.body().string();
                        if (!TextUtils.isEmpty(r)) {
                            JSONObject jsonObject = new JSONObject(r);
//                            Log.e("getSessionEndData ", "" + jsonObject);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject data = jsonObject.optJSONObject("data");
                                    if (data != null && data.length() > 0) {
                                        JSONObject resultData = data.optJSONObject("result");
                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Session_End_Data, "" + resultData);
                                        if (RongoApp.getOriginalCropWeeksFunction() >= Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"))) {
                                            llSessionEnd.setVisibility(View.VISIBLE);
                                            llSessionStart.setVisibility(View.GONE);
                                            setSessionEndData();
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void checkCropHealthStatus() {
        try {
            int cropWeek = RongoApp.getCropWeekDaysFunction();
            if (cropWeek > 1 && cropWeek < Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"))) {
                rlHealthView.setVisibility(View.VISIBLE);
            } else {
                rlHealthView.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void getWeeklyCropInfoQuestion() {
        try {
            String currentcropWeeks = sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, "0");
            bunaiStatusCard.setVisibility(View.GONE);
            if (sharedPreferences.getString(Constants.SharedPreference_Sowing_Done, "").equalsIgnoreCase("yes")) {
                int cropWeek = RongoApp.getCropWeekDaysFunction();
                if (cropWeek == Integer.parseInt(currentcropWeeks)) {
                    JSONObject WeeklyCropInfo = new JSONObject(RongoApp.getWeeklyCropQuestion());
                    if (!TextUtils.isEmpty(WeeklyCropInfo.toString())) {
                        String weekCropDays = Functions.getWeeklyCropDay(sharedPreferences);
                        if (!TextUtils.isEmpty(weekCropDays)) {
                            JSONObject quesCropWeek = null;
                            String[] arrayStr = weekCropDays.split(",");
                            for (String s : arrayStr) {
                                weekCropDay = s.replace("\"", "");

                                quesCropWeek = Objects.requireNonNull(Functions.getWeeklyCropQuestion(sharedPreferences)).optJSONObject(weekCropDay);
//                            Log.v("+++@@+++: ", currentcropWeeks + "-" + weekCropDay + " - " + quesCropWeek);
                                if (!TextUtils.isEmpty(quesCropWeek.toString())) {
                                    List<WeeklyCropInfo> weeklyCropInfoCheck = appDatabase.weeklyCropInfoDao().getCropInfoByWeek(
                                            currentcropWeeks, RongoApp.getSelectedPCrop(), quesCropWeek.getString("info_type"));
                                    if (weeklyCropInfoCheck.size() > 0) {
                                        bunaiStatusCard.setVisibility(View.GONE);
//                                        weeklyQuizBadge.setVisibility(View.GONE);
//                                        llWeeklyQuizView.setAlpha(0.4f);
//                                        llWeeklyQuizView.setEnabled(false);
//                                    Log.v("+++@@111+++: ", currentcropWeeks + "-" + weekCropDay + " - " + quesCropWeek);
                                    } else {
//                                        llWeeklyQuizView.setEnabled(true);
//                                        llWeeklyQuizView.setAlpha(1f);
//                                        weeklyQuizBadge.setVisibility(View.VISIBLE);
                                        bunaiStatusCard.setVisibility(View.VISIBLE);
//                                    Log.v("+++@@222+++: ", currentcropWeeks + "-" + weekCropDay + " - " + quesCropWeek);

                                        tvQuestions.setText("" + quesCropWeek.getString("question"));

                                        inputType = "" + quesCropWeek.getString("input_type");
                                        infoText = "" + quesCropWeek.getString("info_text");
                                        infoType = "" + quesCropWeek.getString("info_type");

                                        if (infoType.equalsIgnoreCase("fertilizer")) {
                                            ivWeeklyCropQues.setVisibility(View.VISIBLE);
                                            ivWeeklyCropQues.setImageResource(R.mipmap.ff);
//                                    titleText = getString(R.string.str_ques_text1);
                                        } else if (infoType.equalsIgnoreCase("irrigation")) {
                                            ivWeeklyCropQues.setVisibility(View.VISIBLE);
                                            ivWeeklyCropQues.setImageResource(R.mipmap.ss);
//                                    titleText = getString(R.string.str_ques_text2);
                                        } else if (infoType.equalsIgnoreCase("harvest")) {
                                            ivWeeklyCropQues.setVisibility(View.VISIBLE);
                                            ivWeeklyCropQues.setImageResource(R.mipmap.kt);
//                                    titleText = getString(R.string.str_ques_text3);
                                        } else if (infoType.equalsIgnoreCase("flowering")) {
                                            ivWeeklyCropQues.setVisibility(View.GONE);
//                                    titleText = getString(R.string.str_ques_text4);
                                        } else if (infoType.equalsIgnoreCase("sowing")) {
                                            relativeBuaiCard.setVisibility(View.GONE);
                                            ivWeeklyCropQues.setVisibility(View.GONE);
                                        } else if (infoType.equalsIgnoreCase("soil_type")) {
                                            ivWeeklyCropQues.setVisibility(View.GONE);
                                        } else if (infoType.equalsIgnoreCase("spacing")) {
                                            ivWeeklyCropQues.setVisibility(View.GONE);
                                        } else if (infoType.equalsIgnoreCase("yield")) {
                                            ivWeeklyCropQues.setVisibility(View.GONE);
                                        } else if (infoType.equalsIgnoreCase("land_preparation")) {
                                            edtProduct.setHint(homeActivity.getResources().getString(R.string.str_farm_area));
                                        } else {
                                            ivWeeklyCropQues.setVisibility(View.GONE);
                                        }

                                        if (inputType.equalsIgnoreCase("button")) {
                                            if (RongoApp.getSelectedCrop().equalsIgnoreCase("1")) {
                                                ivWeeklyCropQues.setVisibility(View.VISIBLE);
                                            }
                                            llButtonView.setVisibility(View.VISIBLE);
                                            llSelectionView.setVisibility(View.GONE);
                                            llEditView.setVisibility(View.GONE);
                                        } else if (inputType.equalsIgnoreCase("dropdown")) {
                                            llButtonView.setVisibility(View.GONE);
                                            ivWeeklyCropQues.setVisibility(View.GONE);
                                            llSelectionView.setVisibility(View.VISIBLE);
                                            llEditView.setVisibility(View.GONE);
                                        } else if (inputType.equalsIgnoreCase("multi_dropdown")) {
                                            llButtonView.setVisibility(View.GONE);
                                            ivWeeklyCropQues.setVisibility(View.GONE);
                                            llSelectionView.setVisibility(View.VISIBLE);
                                            llEditView.setVisibility(View.GONE);
                                        } else if (inputType.equalsIgnoreCase("text")) {
                                            llButtonView.setVisibility(View.GONE);
                                            ivWeeklyCropQues.setVisibility(View.GONE);
                                            llSelectionView.setVisibility(View.GONE);
                                            llEditView.setVisibility(View.VISIBLE);
                                        }
                                        break;
                                    }
                                }
                            }

                            tvDarjKare1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (Functions.doubleTapCheck(mLastLongClick)) {
                                        return;
                                    }
                                    mLastLongClick = SystemClock.elapsedRealtime();

                                    if (InternetConnection.checkConnection(homeActivity)) {
                                        if (TextUtils.isEmpty(edtProduct.getText().toString().trim())) {
                                            if (infoType.equalsIgnoreCase("land_preparation")) {
                                                Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_land_for_farming));
                                            } else {
                                                Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_enter_production));
                                            }
                                            return;
                                        }

                                        if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                            setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, edtProduct.getText().toString().trim(), getResources().getString(R.string.str_ha));
                                        } else {
                                            updateWeeklyCropQues(weekCropDay, infoType, infoText, edtProduct.getText().toString().trim(), getResources().getString(R.string.str_ha));
                                        }
                                    } else {
//                                Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), homeActivity.getString(R.string.no_connection));
                                        setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, edtProduct.getText().toString().trim(), getResources().getString(R.string.str_ha));
                                    }
                                }
                            });

                            ArrayList<String> listdata = new ArrayList<String>();
                            JSONArray optionsArray = quesCropWeek.optJSONArray("options");
                            if (optionsArray != null && optionsArray.length() > 0) {
                                for (int i = 0; i < optionsArray.length(); i++) {
                                    listdata.add(optionsArray.getString(i));
                                }
                                spinnerOptions.setAdapter(new SpinnerArrayAdapter(homeActivity, R.layout.spinner_item_list, listdata));
                                spinnerOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        options = parent.getItemAtPosition(position).toString();
                                        Log.v("Selected Option: ", "" + options);
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });
                            }

                            tvDarjKare.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (Functions.doubleTapCheck(mLastLongClick)) {
                                        return;
                                    }
                                    mLastLongClick = SystemClock.elapsedRealtime();

                                    if (InternetConnection.checkConnection(homeActivity)) {
                                        if (TextUtils.isEmpty(options)) {
                                            Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_please_select_option));
                                            return;
                                        }

                                        if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                            setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, options, getResources().getString(R.string.str_ha));
                                        } else {
                                            updateWeeklyCropQues(weekCropDay, infoType, infoText, options, getResources().getString(R.string.str_ha));
                                        }
                                    } else {
//                                Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), homeActivity.getString(R.string.no_connection));
                                        setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, options, getResources().getString(R.string.str_ha));
                                    }
                                }
                            });

                            tvHa.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (Functions.doubleTapCheck(mLastLongClick)) {
                                        return;
                                    }
                                    mLastLongClick = SystemClock.elapsedRealtime();

                                    if (InternetConnection.checkConnection(homeActivity)) {
                                        if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                            setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, Functions.getCurrentDateTime(), getResources().getString(R.string.str_ha));
                                        } else {
                                            updateWeeklyCropQues(weekCropDay, infoType, infoText, Functions.getCurrentDateTime(), getResources().getString(R.string.str_ha));
                                        }
                                    } else {
                                        setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, Functions.getCurrentDateTime(), getResources().getString(R.string.str_ha));
                                    }
                                }
                            });

                            tvNa.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (Functions.doubleTapCheck(mLastLongClick)) {
                                        return;
                                    }
                                    mLastLongClick = SystemClock.elapsedRealtime();

                                    if (InternetConnection.checkConnection(homeActivity)) {
                                        if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                            setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, "", getResources().getString(R.string.str_na));
                                        } else {
                                            updateWeeklyCropQues(weekCropDay, infoType, infoText, "", getResources().getString(R.string.str_na));
                                        }
                                    } else {
                                        setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, "", getResources().getString(R.string.str_na));
                                    }
                                }
                            });

                        }
                    } else {
                        getWeeklyCropInfoQuestionData();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setOfflineWeeklyCropInfo(final String id, String infoType, String infoText, String infoValue, String metaData) {
        try {
            List<OfflineWeeklyCropInfoData> weeklyCropInfoArray = new ArrayList<OfflineWeeklyCropInfoData>();
            OfflineWeeklyCropInfoData weeklyCropInfo = new OfflineWeeklyCropInfoData();
            weeklyCropInfo.set_id(new Random().nextInt());
            weeklyCropInfo.setUser_id(SharedPreferencesUtils.getUserId(homeActivity));
            weeklyCropInfo.setP_crop_id(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""));
            weeklyCropInfo.setSeason_id(sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""));
            weeklyCropInfo.setCrop_week(sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, ""));
            weeklyCropInfo.setCrop_id(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""));
            weeklyCropInfo.setField_id(sharedPreferences.getString(Constants.SharedPreferences_Field_ID, ""));
            weeklyCropInfo.setInfo_type(infoType);
            weeklyCropInfo.setInfo_text(infoText);
            weeklyCropInfo.setInfo_value(infoValue);
            weeklyCropInfo.setInfo_metadata(metaData);
            weeklyCropInfoArray.add(weeklyCropInfo);
            appDatabase.offlineWeeklyCropInfoDao().insertAll(weeklyCropInfoArray);

            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");
//            appDatabase.weeklyCropInfoDao().updateCropStatus("1", sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, ""), id, RongoApp.getSelectedPCrop());

            List<WeeklyCropInfo> infoList = new ArrayList<WeeklyCropInfo>();
            WeeklyCropInfo cropInfo = new WeeklyCropInfo();
            cropInfo.set_id(new Random().nextInt());
            cropInfo.setCropDay(id);
            cropInfo.setCropWeek(sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, ""));
            cropInfo.setCropId(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""));
            cropInfo.setPcropId(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""));
            cropInfo.setIsDone("1");
            cropInfo.setStatus("1");
            cropInfo.setInfoTypo(infoType);
            infoList.add(cropInfo);
            appDatabase.weeklyCropInfoDao().insertAll(infoList);

            getWeeklyCropInfoQuestion();
            openCropInfoDhanyawad();
            tvRewardPoint.setText(Functions.updateRewardPoints(sharedPreferences, RewardPoints.getWeeklyCropInfoRewardPoint(sharedPreferences)));

            if (infoType.equalsIgnoreCase("yield")) {
                SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                        Constants.SharedPreferences_crop_status, "ended");
                llSessionEnd.setVisibility(View.GONE);
                llSessionStart.setVisibility(View.GONE);
                cardfeedback.setVisibility(View.GONE);
                tvSessionEndOfflineMsg.setVisibility(View.VISIBLE);

                setSessionEndView();
            }

            if (RongoApp.getOriginalCropWeeksFunction() >= Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"))) {
                ediOption1.getText().clear();
                stringList.clear();
                arrayList.clear();
                selectedOption = "";
                setSessionEndView();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void updateWeeklyCropQues(final String id, String infoType, String infoText, String infoValue, String metaData) {
//        Log.e("Weekly", "Questions " + id + " - " + weekCropDay);
        RetrofitClient.getInstance().getApi().updateWeeklyCropInfo(RongoApp.getSeasonName(),
                RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                Functions.getDeviceid(homeActivity), SharedPreferencesUtils.getUserId(homeActivity),
                sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, ""),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_Field_ID, ""),
                infoType, infoText, infoValue, metaData).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");
//                    appDatabase.weeklyCropInfoDao().updateWeeklyCropStatus("1", sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, ""), id, RongoApp.getSelectedPCrop());

                    List<WeeklyCropInfo> infoList = new ArrayList<WeeklyCropInfo>();
                    WeeklyCropInfo cropInfo = new WeeklyCropInfo();
                    cropInfo.set_id(new Random().nextInt());
                    cropInfo.setCropDay(id);
                    cropInfo.setCropWeek(sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, ""));
                    cropInfo.setCropId(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""));
                    cropInfo.setPcropId(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""));
                    cropInfo.setIsDone("1");
                    cropInfo.setStatus("1");
                    cropInfo.setInfoTypo(infoType);
                    infoList.add(cropInfo);
                    appDatabase.weeklyCropInfoDao().insertAll(infoList);

                    getWeeklyCropInfoQuestion();
                    openCropInfoDhanyawad();
                    tvRewardPoint.setText(Functions.updateRewardPoints(sharedPreferences, RewardPoints.getWeeklyCropInfoRewardPoint(sharedPreferences)));

                    if (RongoApp.getOriginalCropWeeksFunction() >= Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"))) {
                        ediOption1.getText().clear();
                        stringList.clear();
                        arrayList.clear();
                        selectedOption = "";
                        setSessionEndView();
                        getProfile();
                    }

                    if (infoType.equalsIgnoreCase("yield")) {
                        getProfile();
                    }

                    getPragatiData();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void openCropInfoDhanyawad() {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_crop_info_dhanyawad, null);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setCanceledOnTouchOutside(false);

        AppCompatTextView tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText(RewardPoints.getWeeklyCropInfoRewardPoint(sharedPreferences));

        AppCompatTextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));

        AppCompatTextView txtOfflineSMS = view.findViewById(R.id.txtOfflineSMS);
        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
            txtOfflineSMS.setVisibility(View.GONE);
        } else {
            txtOfflineSMS.setVisibility(View.VISIBLE);
        }

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        AppCompatTextView tvShareWhatapp = view.findViewById(R.id.tvShareWhatapp);
        tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "WhatsApp");
                    Functions.shareWhatAppIntent(homeActivity);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(homeActivity, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        AppCompatTextView tvShareCopy = view.findViewById(R.id.tvShareCopy);
        tvShareCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "SMS");
                    Functions.shareSMSIntent(homeActivity, RongoApp.getReferralText(homeActivity));

//                    Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
//                    smsIntent.setType("vnd.android-dir/mms-sms");
//                    smsIntent.putExtra("address", "");
//                    smsIntent.putExtra("sms_body", "" + HtmlCompat.fromHtml(sharedPreferences.getString(Constants.SharedPreference_Referral_Link, ""), 0));
//                    smsIntent.setFlags(android.content.Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(smsIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AppCompatTextView tvSharefacebook = view.findViewById(R.id.tvSharefacebook);
        tvSharefacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.setUserScreenTrackEvent(appDatabase, "Invite", "Facebook");
                Functions.shareFacebookIntent(homeActivity);
            }
        });

        AppCompatTextView tvShareMore = view.findViewById(R.id.tvShareMore);
        tvShareMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.performShare(homeActivity, appDatabase);
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));

        bottomSheetDialog.show();
    }

    void getWeeklyCropInfoQuestionData() {
        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
            RetrofitClient.getInstance().getApi().getWeeklyCropInfoQuestion(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    SharedPreferencesUtils.getUserId(homeActivity), RongoApp.getSelectedPCrop()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        try {
                            String r = response.body().string();
                            if (!TextUtils.isEmpty(r)) {
                                JSONObject jsonObj = new JSONObject(r);
                                if (jsonObj != null && jsonObj.length() > 0) {
                                    if (jsonObj.optString("status_code").equalsIgnoreCase("0")) {
                                        JSONObject data = jsonObj.optJSONObject("data");
                                        if (data != null && data.length() > 0) {
                                            JSONObject resultData = data.optJSONObject("result");
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                                    Constants.SharedPreferences_Weekly_Crop_Question, "" + resultData);
                                            getWeeklyCropInfoQuestion();
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        }
    }

    void getPragatiData() {
        RetrofitClient.getInstance().getApi().getCropPragatiTimeline(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                SharedPreferencesUtils.getUserId(homeActivity), sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                Functions.getCommaSepPCropId(sharedPreferences)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String r = response.body().string();
                        if (!TextUtils.isEmpty(r)) {
                            JSONObject jsonObject = new JSONObject(r);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject data = jsonObject.optJSONObject("data");
                                    if (data != null && data.length() > 0) {
                                        JSONObject resultData = data.optJSONObject("result");
                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Pragati, "" + resultData);
                                        setPragatiData();
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void setPragatiData() {
        try {
            String resultData = sharedPreferences.getString(Constants.SharedPreferences_Pragati, "");
            if (!TextUtils.isEmpty(resultData)) {
                JSONObject result = new JSONObject(resultData);
                if (result != null && result.length() > 0) {
                    JSONObject weekly_info = result.optJSONObject("weekly_info");
                    if (weekly_info != null && weekly_info.length() > 0) {
                        Object weeklyinfojson = new JSONTokener(weekly_info.getString(RongoApp.getSelectedPCrop())).nextValue();
                        if (weeklyinfojson != null) {
                            JSONObject jsonObject = weekly_info.optJSONObject(RongoApp.getSelectedPCrop());
                            if (jsonObject != null && jsonObject.length() > 0) {
                                for (int i = 0; i < RongoApp.getSessionEndWeek(); i++) {
                                    JSONObject jd = jsonObject.optJSONObject(String.valueOf(i));
                                    if (jd != null && jd.length() > 0) {
                                        if (!jd.optString("crop_info").equalsIgnoreCase("null")) {
                                            JSONArray jsonArray = new JSONArray(jd.optString("crop_info"));
                                            if (jsonArray != null && jsonArray.length() > 0) {
                                                for (int k = 0; k < jsonArray.length(); k++) {
                                                    JSONObject object = jsonArray.optJSONObject(k);
                                                    String info_type = object.optString("info_type");
//                                                Log.e("+++info_type", String.valueOf(i) + " - " + info_type);
                                                    List<WeeklyCropInfo> weeklyCropInfoCheck = appDatabase.weeklyCropInfoDao().getCropInfoByWeek(
                                                            String.valueOf(i), RongoApp.getSelectedPCrop(), info_type);
                                                    if (weeklyCropInfoCheck.size() > 0) {

                                                    } else {
                                                        List<WeeklyCropInfo> infoList = new ArrayList<WeeklyCropInfo>();
                                                        WeeklyCropInfo cropInfo = new WeeklyCropInfo();
                                                        cropInfo.set_id(new Random().nextInt());
                                                        cropInfo.setCropDay(String.valueOf(k));
                                                        cropInfo.setCropWeek(String.valueOf(i));
                                                        cropInfo.setCropId(RongoApp.getSelectedCrop());
                                                        cropInfo.setPcropId(RongoApp.getSelectedPCrop());
                                                        cropInfo.setIsDone("1");
                                                        cropInfo.setStatus("1");
                                                        cropInfo.setInfoTypo(info_type);
                                                        infoList.add(cropInfo);
                                                        appDatabase.weeklyCropInfoDao().insertAll(infoList);
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            getWeeklyCropInfoQuestion();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setCropHealthStatus() {
        try {
            txtUnhealthy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    tvRewardPoint.setText(Functions.updateRewardPoints(sharedPreferences, RewardPoints.getCropHealthRewardPoint(sharedPreferences)));

                    if (InternetConnection.checkConnection(homeActivity)) {
                        updateWeeklyAdvisoryStatus("0");
                    } else {
                        setAdvisoryOffline("0");
                    }
                }
            });

            txtHealthy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    tvRewardPoint.setText(Functions.updateRewardPoints(sharedPreferences, RewardPoints.getCropHealthRewardPoint(sharedPreferences)));

                    if (InternetConnection.checkConnection(homeActivity)) {
                        updateWeeklyAdvisoryStatus("1");
                    } else {
                        setAdvisoryOffline("1");
                    }
                }
            });

            txtUplaod.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openCrops("0");
                }
            });

            if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                    cropHealthFromDatabase();
                } else {
                    List<AdvisoryStatusData> advisoryStatusData = appDatabase.advisoryStatusDao().getAllData(
                            sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, "0"), RongoApp.getSelectedPCrop());
                    if (advisoryStatusData.size() == 0) {
                        checkWeeklyAdvisoryStatus();
                    } else {
//                        Log.v("crop_week Adviso Done: ", advisoryStatusData.get(0).getIsDone());
                        if (advisoryStatusData.get(0).getIsDone().equalsIgnoreCase("0")) {
                            cardHealthStatus.setVisibility(View.VISIBLE);
                            cardLuckYDraw.setVisibility(View.GONE);
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_WeeklyUploadPhoto, "N");
                        } else if (advisoryStatusData.get(0).getIsDone().equalsIgnoreCase("1")) {
                            List<AdvisoryPhotoData> advisoryPhotoData = appDatabase.advisoryPhotoDao().
                                    getAllData(sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, "0"), RongoApp.getSelectedPCrop());
                            if (advisoryPhotoData.size() > 0) {
                                if (advisoryPhotoData.get(0).getIsPhoto().equalsIgnoreCase("0")) {
                                    cardHealthStatus.setVisibility(View.GONE);
                                    cardLuckYDraw.setVisibility(View.VISIBLE);
                                } else {
                                    cardHealthStatus.setVisibility(View.GONE);
                                    cardLuckYDraw.setVisibility(View.GONE);
                                }
                            } else {
                                cardHealthStatus.setVisibility(View.GONE);
                                cardLuckYDraw.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                }
            } else {
                cropHealthFromDatabase();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void cropHealthFromDatabase() {
        List<AdvisoryStatusData> advisoryStatusData = appDatabase.advisoryStatusDao().getAllData(
                sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, "0"), RongoApp.getSelectedPCrop());
        if (advisoryStatusData.size() == 0) {
            if (sharedPreferences.getString(Constants.SharedPreference_Lucky_Draw, "N").equalsIgnoreCase("Y")) {
                cardLuckYDraw.setVisibility(View.VISIBLE);
                cardHealthStatus.setVisibility(View.GONE);
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_WeeklyUploadPhoto, "N");
            } else {
                cardLuckYDraw.setVisibility(View.GONE);
                cardHealthStatus.setVisibility(View.VISIBLE);
            }
        } else {
            if (advisoryStatusData.get(0).getIsDone().equalsIgnoreCase("0")) {
                cardHealthStatus.setVisibility(View.VISIBLE);
                cardLuckYDraw.setVisibility(View.GONE);
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_WeeklyUploadPhoto, "N");
            } else {
                cardLuckYDraw.setVisibility(View.VISIBLE);
                cardHealthStatus.setVisibility(View.GONE);
            }
        }
    }

    void checkWeeklyAdvisoryStatus() {
        progressBarDialog.showProgressDialogWithTitle("3");

        RetrofitClient.getInstance().getApi().checkWeeklyAdvisoryStatus(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                Functions.getDeviceid(homeActivity), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                SharedPreferencesUtils.getUserId(homeActivity), season_id, RongoApp.getSelectedPCrop(),
                sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, "0"), RongoApp.getSelectedCrop()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String str = "";
                progressBarDialog.hideProgressDialogWithTitle();
                try {
                    str = response.body().string();
                    if (!TextUtils.isEmpty(str)) {
                        JSONObject jsonObject = new JSONObject(str);
                        if (jsonObject != null && jsonObject.length() > 0) {
                            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {

                                JSONObject data = jsonObject.optJSONObject("data");
                                if (data != null && data.length() > 0) {
                                    JSONObject result = data.optJSONObject("result");
                                    if (result != null && result.length() > 0) {
                                        if (result.optString("is_done").equalsIgnoreCase("0")) {
                                            cardHealthStatus.setVisibility(View.VISIBLE);
                                            cardLuckYDraw.setVisibility(View.GONE);
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_WeeklyUploadPhoto, "N");
                                        } else {
                                            cardLuckYDraw.setVisibility(View.VISIBLE);
                                            cardHealthStatus.setVisibility(View.GONE);
                                        }

                                        if (!TextUtils.isEmpty(result.optString("is_done"))) {
                                            List<AdvisoryStatusData> advisoryStatusData = new ArrayList<>();
                                            advisoryStatusData.add(new AdvisoryStatusData(0, sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0"),
                                                    result.optString("is_done"), RongoApp.getSelectedCrop(), RongoApp.getSelectedPCrop(), "1"));
                                            appDatabase.advisoryStatusDao().insertAll(advisoryStatusData);
                                        }

                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressBarDialog.hideProgressDialogWithTitle();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        });
    }

    void updateWeeklyAdvisoryStatus(final String isHealthy) {
        progressBarDialog.showProgressDialogWithTitle("4");
        RetrofitClient.getInstance().getApi().updateWeeklyAdvisoryStatus(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                Functions.getDeviceid(homeActivity), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                SharedPreferencesUtils.getUserId(homeActivity), season_id, RongoApp.getSelectedPCrop(),
                sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, ""), RongoApp.getSelectedCrop(), isHealthy).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBarDialog.hideProgressDialogWithTitle();
                String str = "";
                try {
                    str = response.body().string();
                    if (!TextUtils.isEmpty(str)) {
                        JSONObject jsonObject = new JSONObject(str);
                        if (jsonObject != null && jsonObject.length() > 0) {
                            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Lucky_Draw, "Y");
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");

                                appDatabase.advisoryStatusDao().deleteByCropWeek(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0"), RongoApp.getSelectedPCrop());

                                List<AdvisoryStatusData> advisoryStatusData = new ArrayList<>();
                                advisoryStatusData.add(new AdvisoryStatusData(0, RongoApp.getCurrentCropWeek(),
                                        "1", RongoApp.getSelectedCrop(), RongoApp.getSelectedPCrop(), isHealthy));
                                appDatabase.advisoryStatusDao().insertAll(advisoryStatusData);

                                if (isHealthy.equalsIgnoreCase("1")) {
                                    openCrops(isHealthy);
                                } else {
                                    openProblem();
                                }
                                cardLuckYDraw.setVisibility(View.VISIBLE);
                                cardHealthStatus.setVisibility(View.GONE);
                            } else {
                                cardLuckYDraw.setVisibility(View.GONE);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressBarDialog.hideProgressDialogWithTitle();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        });
    }

    void setAdvisoryOffline(String isHealthy) {
        try {
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Lucky_Draw, "Y");
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");

            appDatabase.advisoryStatusDao().deleteByCropWeek(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0"), RongoApp.getSelectedPCrop());

            List<AdvisoryStatusData> advisoryStatusData = new ArrayList<>();
            advisoryStatusData.add(new AdvisoryStatusData(0, sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, "0"),
                    "1", RongoApp.getSelectedCrop(), RongoApp.getSelectedPCrop(), isHealthy));
            appDatabase.advisoryStatusDao().insertAll(advisoryStatusData);

            if (isHealthy.equalsIgnoreCase("1")) {
                openCrops("1");
            } else {
                openProblem();
            }
            cardLuckYDraw.setVisibility(View.VISIBLE);
            cardHealthStatus.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    BottomSheetDialog bottomSheetDialogCrop;
    CropAreaFocusedData mSelectedList;

    void openCrops(String status) {
        bottomSheetDialogCrop = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_lucky_draw_crops, null);
        bottomSheetDialogCrop.setContentView(view);
        bottomSheetDialogCrop.setCancelable(false);
        bottomSheetDialogCrop.setCanceledOnTouchOutside(false);

        MaterialCardView cardHealthlly = (MaterialCardView) view.findViewById(R.id.cardHealthlly);
        LinearLayout llUploadView = (LinearLayout) view.findViewById(R.id.llUploadView);
        cardHealthlly.setVisibility(View.VISIBLE);
        llUploadView.setVisibility(View.GONE);

        AppCompatTextView tvPoint = view.findViewById(R.id.tvPoint);
        tvPoint.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences));

        AppCompatTextView tvUpload = view.findViewById(R.id.tvUpload);
        tvUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardHealthlly.setVisibility(View.GONE);
                llUploadView.setVisibility(View.VISIBLE);
            }
        });

        AppCompatTextView tvPointsUploadPicture = view.findViewById(R.id.tvPointsUploadPicture);
        tvPointsUploadPicture.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences) + " " + getString(R.string.str_point_earned));
        RecyclerView rvCrops = view.findViewById(R.id.rvCrops);
        rvCrops.setLayoutManager(new LinearLayoutManager(homeActivity, RecyclerView.HORIZONTAL, false));
        rvCrops.setNestedScrollingEnabled(false);
        rvCrops.setHasFixedSize(true);
        List<CropAreaFocusedData> cropAreaFocusedData = SharedPreferencesUtils.getCropArea(homeActivity, Constants.SharedPreferences_CROP_FOCUSED_AREA);
        if (cropAreaFocusedData != null && cropAreaFocusedData.size() > 0) {
            rvCrops.setAdapter(new CropAreaForPhasalAdapter(PhasalFragment.this, cropAreaFocusedData, mSelectedList));
        }

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogCrop.dismiss();
            }
        });

        ImageView imgNext = view.findViewById(R.id.nextImg);
        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rvCrops.post(new Runnable() {
                    @Override
                    public void run() {
                        if (cropAreaFocusedData != null && cropAreaFocusedData.size() > 0) {
                            rvCrops.scrollToPosition(cropAreaFocusedData.size() - 1);
                        }
                    }
                });
            }
        });

        AppCompatTextView txtUpload = view.findViewById(R.id.txtUpload);
        txtUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogCrop.dismiss();
                openGuidlinesDialog();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialogCrop.show();
    }

    public void getSelectedArea(CropAreaFocusedData mCropAreaFocusedData) {
        this.mSelectedList = mCropAreaFocusedData;
    }

    BottomSheetDialog bottomSheetThumbnail;
    ThumbnailsAdapter thumbnailsAdapter;
    ArrayList<String> imagesPath = new ArrayList<>();
    String mCurrentPhotoPath = "", seedKey = "";

    void openThumbnailDialog() {
        bottomSheetThumbnail = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_upload_more_photos, null);
        bottomSheetThumbnail.setContentView(view);

        AppCompatTextView tvPointsUploadPicture = view.findViewById(R.id.tvPointsUploadPicture);
        tvPointsUploadPicture.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences) + getString(R.string.str_point_earned));

        ImageView imgNext = view.findViewById(R.id.imgNext);
        ProgressBar pb = view.findViewById(R.id.pb);
        pb.setVisibility(View.GONE);
        LinearLayout linearLuckYDraw = view.findViewById(R.id.linearLuckYDraw);
        linearLuckYDraw.setVisibility(View.VISIBLE);
        bottomSheetThumbnail.setCanceledOnTouchOutside(false);
        bottomSheetThumbnail.setCancelable(false);

        if (imagesPath.size() == 1) {
            imgNext.setVisibility(View.GONE);
        } else {
            imgNext.setVisibility(View.VISIBLE);
        }

        RecyclerView rvAllPhotos = view.findViewById(R.id.rvAllPhotos);
        rvAllPhotos.setLayoutManager(new LinearLayoutManager(homeActivity, RecyclerView.HORIZONTAL, false));

        thumbnailsAdapter = new ThumbnailsAdapter(imagesPath);
        rvAllPhotos.setAdapter(thumbnailsAdapter);
        rvAllPhotos.setNestedScrollingEnabled(false);

        AppCompatTextView txtUploadMore = view.findViewById(R.id.txtUploadMore);
        final AppCompatTextView txtGoOn = view.findViewById(R.id.txtGoOn);
        txtGoOn.setText(getResources().getString(R.string.str_darj_kare));

        final ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetThumbnail.dismiss();

                if (bottomSheetDialogCrop != null) {
                    bottomSheetDialogCrop.show();
                }

                if (imagesPath != null && imagesPath.size() > 0) {
                    imagesPath.clear();
                }
            }
        });

        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rvAllPhotos.post(new Runnable() {
                    @Override
                    public void run() {
                        if (imagesPath != null && imagesPath.size() > 0) {
                            rvAllPhotos.scrollToPosition(imagesPath.size() - 1);
                        }
                    }
                });
            }
        });

        txtUploadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetThumbnail.dismiss();
                accessPermission();
            }
        });

        txtGoOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

//                tvRewardPoint.setText(Functions.updateRewardPoints(sharedPreferences, RewardPoints.getUploadPictureRewardPoint(sharedPreferences)));
                if (imagesPath.size() > 0) {
                    List<AdvisoryPhotoData> advisoryPhotoData = new ArrayList<AdvisoryPhotoData>();
                    advisoryPhotoData.add(new AdvisoryPhotoData(0, sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, "0"),
                            RongoApp.getSelectedCrop(), RongoApp.getSelectedPCrop(), "1"));
                    appDatabase.advisoryPhotoDao().insertAll(advisoryPhotoData);
                }

                if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                    if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                        setOfflineData(bottomSheetThumbnail);
                    } else {
                        uploadWeeklyPicture();
                    }
                } else {
                    setOfflineData(bottomSheetThumbnail);
                }
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetThumbnail.show();
    }

    void setOfflineData(BottomSheetDialog bottomSheetThumbnail) {
        try {
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_WeeklyUploadPhoto, "Y");
            String focus_area = "";
            if (mSelectedList != null) {
                focus_area = mSelectedList.getType();
            }

            String finalFocus_area = focus_area;
            Executors.newSingleThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    List<LuckyDrawData> luckyDrawData = new ArrayList<>();
                    luckyDrawData.add(new LuckyDrawData(0, season_id,
                            sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                            sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                            crop_week, finalFocus_area, latitude + "", longitude + "", imagesPath));
                    appDatabase.luckyDrawOfflineDao().insertAll(luckyDrawData);
                }
            });

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mSelectedList = null;
                    mCurrentPhotoPath = "";
                    cardHealthStatus.setVisibility(View.GONE);
                    cardLuckYDraw.setVisibility(View.GONE);
                    imagesPath.clear();
                    if (bottomSheetThumbnail != null && bottomSheetThumbnail.isShowing()) {
                        bottomSheetThumbnail.dismiss();
                    }

                    openSuccessForHealthy("1");
                }
            }, 500);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    BottomSheetDialog bottomSheetDialogGuideLine;

    void openGuidlinesDialog() {
        bottomSheetDialogGuideLine = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_guideline_for_photo_capture, null);
        bottomSheetDialogGuideLine.setContentView(view);
        bottomSheetDialogGuideLine.setCanceledOnTouchOutside(false);
        bottomSheetDialogGuideLine.setCancelable(false);

        AppCompatTextView txtUpload = view.findViewById(R.id.txtUpload);
        AppCompatTextView txtLuckyDraw = view.findViewById(R.id.txtLuckyDraw);
        AppCompatTextView txtDescription = view.findViewById(R.id.txtDescription);
        AppCompatImageView imgGuideline = view.findViewById(R.id.imgGuideline);
        txtLuckyDraw.setVisibility(View.GONE);

        if (mSelectedList != null) {
            Functions.displayImage(mSelectedList.getImage(), imgGuideline);
            txtDescription.setText(mSelectedList.getDescription());
        }

        final ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogGuideLine.dismiss();
                if (bottomSheetDialogCrop != null) {
                    bottomSheetDialogCrop.show();
                }
            }
        });

        txtUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogGuideLine.dismiss();
                accessPermission();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));

        bottomSheetDialogGuideLine.show();
    }

    RequestBody getRequestFile(File file) {
        RequestBody fbody = RequestBody.create(file, MediaType.parse("image/*"));
        return fbody;
    }

    void uploadWeeklyPicture() {
        String focus_area = "";
        if (mSelectedList != null) {
            focus_area = mSelectedList.getType();
        }
        RequestBody requestBodyId = RequestBody.create(SharedPreferencesUtils.getUserId(homeActivity).trim(), MediaType.parse("text/plain"));
        RequestBody seasonIdBody = RequestBody.create(season_id, MediaType.parse("text/plain"));
        RequestBody requestBodyCropId = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyPCropId = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyCropWeek = RequestBody.create(crop_week, MediaType.parse("text/plain"));
        RequestBody requestBodyDeviceId = RequestBody.create(Functions.getDeviceid(homeActivity), MediaType.parse("text/plain"));
        RequestBody requestBodyFCMToken = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyPictureArea = RequestBody.create(focus_area, MediaType.parse("text/plain"));
        RequestBody requestBodylatitude = RequestBody.create(latitude + "", MediaType.parse("text/plain"));
        RequestBody requestBodylongitude = RequestBody.create(longitude + "", MediaType.parse("text/plain"));
        RequestBody requestBodyAppVersion = RequestBody.create(RongoApp.getAppVersion(), MediaType.parse("text/plain"));
        RequestBody requestBodyAppLanguage = RequestBody.create(RongoApp.getDefaultLanguage(), MediaType.parse("text/plain"));
        RequestBody requestBodyAppSeason = RequestBody.create(RongoApp.getSeasonName(), MediaType.parse("text/plain"));

        Map<String, RequestBody> params = new HashMap<>();
        params.put("user_id", requestBodyId);
        params.put("season_id", seasonIdBody);
        params.put("crop_id", requestBodyCropId);
        params.put("p_crop_id", requestBodyPCropId);
        params.put("crop_week", requestBodyCropWeek);
        params.put("picture_area_focused", requestBodyPictureArea);
        params.put("geo_latitude", requestBodylatitude);
        params.put("geo_longitude", requestBodylongitude);
        params.put("device_id", requestBodyDeviceId);
        params.put("fcm_token", requestBodyFCMToken);
        params.put("app_version", requestBodyAppVersion);
        params.put("app_language", requestBodyAppLanguage);
        params.put("season_name", requestBodyAppSeason);

        if (imagesPath != null && imagesPath.size() > 0) {
            RequestBody requestBodyImageUploadedPath = RequestBody.create(imagesPath.toString(), MediaType.parse("text/plain"));
            params.put("picture_key_local", requestBodyImageUploadedPath);

            for (int i = 0; i < imagesPath.size(); i++) {
                File file = new File(imagesPath.get(i).toString().trim());
                try {
                    File compressedImages = new ImageZipper(homeActivity).compressToFile(file);
                    params.put("picture[]" + "\"; filename=\"" + file.getName(), getRequestFile(compressedImages));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        RetrofitClient.getInstance().getApi().uploadWeeklyPicture(params).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String str = response.body().string();
                        if (!TextUtils.isEmpty(str)) {
                            JSONObject jsonObject = new JSONObject(str);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {

                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");
                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_WeeklyUploadPhoto, "Y");
                                    mSelectedList = null;
                                    mCurrentPhotoPath = "";
                                    cardHealthStatus.setVisibility(View.GONE);
                                    cardLuckYDraw.setVisibility(View.GONE);
                                    imagesPath.clear();
                                    if (bottomSheetThumbnail != null && bottomSheetThumbnail.isShowing()) {
                                        bottomSheetThumbnail.dismiss();
                                    }

                                    openSuccessForHealthy("1");
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    BottomSheetDialog bottomSheetDialogLabel;
    AppCompatTextView txtDarjKare;
    String forPhotoLabel = "N";
    ImageView imgPhotoLabel;

    void openPhotoLabelDialog(String path) {
        bottomSheetDialogLabel = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_photo_label_upload, null);
        bottomSheetDialogLabel.setContentView(view);
        bottomSheetDialogLabel.setCancelable(false);
        bottomSheetDialogLabel.setCanceledOnTouchOutside(false);

        AppCompatTextView txtUpload = view.findViewById(R.id.txtUpload);
        txtDarjKare = view.findViewById(R.id.txtDarjKare);
        final LinearLayout linearCapture = view.findViewById(R.id.linearCapture);
        final ProgressBar pb = view.findViewById(R.id.pb);
        final RelativeLayout relativeLabelUpload = view.findViewById(R.id.relativeLabelUpload);
        imgPhotoLabel = view.findViewById(R.id.imgPhotoLabel);
        ImageView imgDelete = view.findViewById(R.id.imgDelete);

        if (forPhotoLabel.equalsIgnoreCase("Y")) {
            linearCapture.setVisibility(View.GONE);
            relativeLabelUpload.setVisibility(View.VISIBLE);
            Functions.displayImage(path, imgPhotoLabel);
        } else {
            linearCapture.setVisibility(View.VISIBLE);
            relativeLabelUpload.setVisibility(View.GONE);
        }

        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forPhotoLabel = "N";

                imgPhotoLabel.setImageResource(0);
                imgPhotoLabel.setImageBitmap(null);

                linearCapture.setVisibility(View.VISIBLE);
                relativeLabelUpload.setVisibility(View.GONE);
            }
        });

        final ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogLabel.dismiss();
                forPhotoLabel = "N";
            }
        });

        txtUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forPhotoLabel = "Y";

                bottomSheetDialogLabel.dismiss();
                accessPermission();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));

        txtDarjKare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forPhotoLabel = "N";
                bottomSheetDialogLabel.dismiss();
            }
        });
        bottomSheetDialogLabel.show();
    }

    void accessPermission() {
        Dexter.withContext(homeActivity)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    void showImagePickerOptions() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(homeActivity);
        View sheetView = getLayoutInflater().inflate(R.layout.bottom_photo_uplaod_options, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);

        LinearLayout linearCamera = sheetView.findViewById(R.id.linearCamera);
        LinearLayout linearGallery = sheetView.findViewById(R.id.linearGallery);
        sheetView.findViewById(R.id.imgClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();

                if (bottomSheetDialogLabel != null) {
                    bottomSheetDialogLabel.show();
                }

                if (bottomSheetDialogGuideLine != null) {
                    bottomSheetDialogGuideLine.show();
                }
            }
        });

        linearCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                if (homeActivity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                    if (PermissionUtils.isCameraGranted(homeActivity)) {
                        if (PermissionUtils.isReadStorageGranted(homeActivity)) {
                            if (PermissionUtils.isWriteStorageGranted(homeActivity)) {
                                dispatchTakePictureIntent();
                            } else {
                                PermissionUtils.checkPermission(homeActivity, WRITE_EXTERNAL_STORAGE, PERMISSION_READ_STORAGE);
                            }
                        } else {
                            PermissionUtils.checkPermission(homeActivity, READ_EXTERNAL_STORAGE, PERMISSION_READ_STORAGE);
                        }
                    } else {
                        PermissionUtils.checkPermission(homeActivity, Manifest.permission.CAMERA, PERMISSION_READ_STORAGE);
                    }
                } else {
                    Toast.makeText(homeActivity, R.string.str_camera_not_support, Toast.LENGTH_LONG).show();
                }
            }
        });

        linearGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.putExtra("seedKey", seedKey);
                startActivityForResult(intent, GALLERY_REQUEST_CODE);
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        ((View) sheetView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        mBottomSheetDialog.show();
    }

    void dispatchTakePictureIntent() {
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File photoFile = createImageFile();
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(homeActivity, providerAuthority, photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                List<ResolveInfo> resolvedIntentActivities = homeActivity.getPackageManager().queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolvedIntentInfo : resolvedIntentActivities) {
                    String packageName = resolvedIntentInfo.activityInfo.packageName;
                    homeActivity.grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
                File f = new File(mCurrentPhotoPath);
                Uri contentUri = Uri.fromFile(f);
                String path = contentUri.getPath();

                if (forPhotoLabel.equalsIgnoreCase("Y")) {
                    openPhotoLabelDialog(path);
                } else {
                    imagesPath.add(path);
                    openThumbnailDialog();
                }
            }

            if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {
                final Uri selectedImage = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = homeActivity.getContentResolver().query(selectedImage, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);
                mCurrentPhotoPath = c.getString(columnIndex);
                c.close();
                if (mCurrentPhotoPath == null) {
                    Bitmap googlepath = Functions.getBitmapFromUri(selectedImage, homeActivity);
                    mCurrentPhotoPath = Functions.getGooglePhotoImagePath(googlepath, homeActivity);
                }

                if (mCurrentPhotoPath.contains("/root")) {
                    mCurrentPhotoPath = mCurrentPhotoPath.replace("/root", "");
                }

                if (forPhotoLabel.equalsIgnoreCase("Y")) {
                    openPhotoLabelDialog(mCurrentPhotoPath);
                } else {
                    imagesPath.add(mCurrentPhotoPath);
                    openThumbnailDialog();
                }
            }

            if (requestCode == 98 && resultCode == Activity.RESULT_OK) {
                if (InternetConnection.checkConnection(homeActivity)) {
                    getProfile();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void seedType(String s) {
        this.seedType = s;
    }

    void openSowingDate(final String txt) {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.sowing_date_dialog, null);
        bottomSheetDialog.setContentView(view);

        final AppCompatEditText editTextSowingDate = view.findViewById(R.id.editTextSowingDate);
        AppCompatTextView txtEnter = view.findViewById(R.id.txtEnter);
        AppCompatTextView txtYourSowingText = view.findViewById(R.id.txtYourSowingText);
        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        if (txt.equalsIgnoreCase("Yes")) {
            txtYourSowingText.setText(R.string.str_apni_buvay_ki_date_darj_kare);
        } else {
            txtYourSowingText.setText(R.string.str_apni_expe_buvay_date_dark_kare);
        }

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));

        editTextSowingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDatePicker(editTextSowingDate, txt);
            }
        });

        txtEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                if (InternetConnection.checkConnection(homeActivity)) {
                    saveSowingInfo(txt, editTextSowingDate.getText().toString().trim());
                    updateWeeklyCropInfo(txt, "sowing", "बुवाई", editTextSowingDate.getText().toString().trim(), getResources().getString(R.string.str_ha));
                }
            }
        });

        bottomSheetDialog.show();
    }

    void openDatePicker(final AppCompatEditText appCompatEditText, final String YesOrNo) {
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(homeActivity, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar c = Calendar.getInstance();
                c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                c.set(Calendar.YEAR, year);
                c.set(Calendar.MONTH, monthOfYear);

                fromDatePicker = "Y";

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                String formattedDate = simpleDateFormat.format(c.getTime());

                if (YesOrNo.equalsIgnoreCase("Yes")) {
                    sowingDate = formattedDate;
                } else {
                    expectedSowingDate = formattedDate;
                }
                appCompatEditText.setText(formattedDate);
            }
        }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }


    void openDhanyawad() {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_dhanyawad, null);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setCanceledOnTouchOutside(false);

        AppCompatTextView txtDesc = view.findViewById(R.id.txtDesc);
        txtDesc.setVisibility(View.VISIBLE);
        txtDesc.setText(R.string.str_thank_for_invite);
        AppCompatTextView txtGoBack = view.findViewById(R.id.txtGoBack);
        txtGoBack.setVisibility(View.VISIBLE);
        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });
        txtGoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    void setDailyQuestion() {
        try {
            if (cardAajKaQuestion != null) {
                cardAajKaQuestion.setVisibility(View.GONE);
                int cropWeek = RongoApp.getCropWeekDaysFunction();
                if (cropWeek > 0 && cropWeek < Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"))) {
                    List<DailyQuizStatusData> dailyQuizStatus = appDatabase.dailyQuizStatusDao().getDailyQuizStatus(
                            sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0"),
                            sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, "0"));
                    if (dailyQuizStatus != null && dailyQuizStatus.size() >= 1) {
                        cardAajKaQuestion.setVisibility(View.GONE);

//                        quizBadge.setVisibility(View.GONE);
//                        llQuizView.setEnabled(false);
//                        llQuizView.setAlpha(0.4f);
                    } else {
//                        quizBadge.setVisibility(View.VISIBLE);
//                        llQuizView.setEnabled(true);
//                        llQuizView.setAlpha(1f);

                        cardAajKaQuestion.setVisibility(View.VISIBLE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void getWeeklyWeather(boolean isStory) {
        if (apiCount > 1) {
            return;
        }

        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
            RetrofitClient.getInstance().getApi().getWeeklyWeatherData(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    SharedPreferencesUtils.getUserId(homeActivity), sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                    sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0")).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        try {
                            apiCount++;
                            String r = response.body().string();
                            if (!TextUtils.isEmpty(r)) {
                                JSONObject jsonObject = new JSONObject(r);
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_WeeklyWeatherData, "" + jsonObject.toString());
                                setWeeklyWeatherData();
                            }

                            if (isStory) {
                                Intent i = new Intent(homeActivity, WeatherDetailsActivity.class);
                                i.putExtra("weather_alert", "");
                                startActivity(i);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        }
    }

    void parseStateCityListData() {
        try {
            JSONObject jsonObject = new JSONObject(sharedPreferences.getString(Constants.SharedPreferences_StateCityData, ""));
            JSONObject data = jsonObject.optJSONObject("data");
            if (data != null && data.length() > 0) {
                stateCityDataModels = new ArrayList<StateCityDataModel>();
                JSONArray result = data.optJSONArray("result");
                if (result != null && result.length() > 0) {
                    for (int i = 0; i < result.length(); i++) {
                        JSONObject object = result.optJSONObject(i);

                        JSONObject state = object.optJSONObject("state");
                        if (state != null && state.length() > 0) {
                            parseJsonState(i, state);
                        }

                        JSONObject district = object.optJSONObject("district");
                        if (district != null && district.length() > 0) {
                            parseJsonCity(i, district);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void parseJsonState(int id, JSONObject data) {
        StateCityDataModel stateCityDataModel = new StateCityDataModel();
        if (data != null) {
            Iterator<String> it = data.keys();
            while (it.hasNext()) {
                String key = it.next();
                try {
                    if (data.get(key) instanceof JSONArray) {
                        JSONArray arry = data.getJSONArray(key);
                        int size = arry.length();
                        for (int i = 0; i < size; i++) {
                            parseJsonState(i, arry.getJSONObject(i));
                        }
                    } else if (data.get(key) instanceof JSONObject) {
                        parseJsonState(0, data.getJSONObject(key));
                    } else {
//                        System.out.println("" + key + "-" + id + " : " + data.optString(key));
                        stateCityDataModel.setId(String.valueOf(id));
                        stateCityDataModel.setState_english(key);
                        stateCityDataModel.setState_hindi(data.optString(key));
                        stateCityDataModels.add(stateCityDataModel);
                    }
                } catch (Throwable e) {
//                    System.out.println("" + key + " : " + data.optString(key));
                    e.printStackTrace();
                }
            }
        }
    }

    void parseJsonCity(int id, JSONObject data) {
        ArrayList<StateCityDataModel.CityListModel> cityListModels = new ArrayList<>();
        if (data != null) {
            Iterator<String> it = data.keys();
            while (it.hasNext()) {
                String key = it.next();
                try {
                    if (data.get(key) instanceof JSONArray) {
                        JSONArray arry = data.getJSONArray(key);
                        int size = arry.length();
                        for (int i = 0; i < size; i++) {
                            parseJsonState(i, arry.getJSONObject(i));
                        }
                    } else if (data.get(key) instanceof JSONObject) {
                        parseJsonState(0, data.getJSONObject(key));
                    } else {
//                        System.out.println("" + key + "-" + id + " : " + data.optString(key));
                        StateCityDataModel.CityListModel cityListModel = new StateCityDataModel.CityListModel();
                        cityListModel.setId(String.valueOf(id));
                        cityListModel.setCity_english(key);
                        cityListModel.setCity_hindi(data.optString(key));
                        cityListModels.add(cityListModel);
                    }
                } catch (Throwable e) {
//                    System.out.println("" + key + " : " + data.optString(key));
                    e.printStackTrace();
                }
            }
            stateCityDataModels.get(id).setCityList(cityListModels);
        }
    }

    String stateEnglishToHindWord(String state) {
        String stateHindi = state;
        try {
            if (!TextUtils.isEmpty(state) && stateCityDataModels.size() > 0) {
                for (int i = 0; i < stateCityDataModels.size(); i++) {
                    if (state.equalsIgnoreCase(stateCityDataModels.get(i).getState_english())) {
                        stateHindi = stateCityDataModels.get(i).getState_hindi();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stateHindi;
    }

    void setWeeklyWeatherData() {
        try {
            parseStateCityListData();
            String weeklyWeather = sharedPreferences.getString(Constants.SharedPreferences_WeeklyWeatherData, "");
            if (TextUtils.isEmpty(weeklyWeather)) {
                if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                    getWeeklyWeather(false);
                } else {
                    tvTodayWeatherDate.setText(DateUtils.parseDateHindi(Functions.getTodayDate(), "yyyy-MM-dd", "dd MMMM yyyy"));
                    tvTodayWeatherDistrict.setText(stateEnglishToHindWord(sharedPreferences.getString(Constants.SharedPreference_New_State, "")));

                    cardWeatherData.setVisibility(View.VISIBLE);
                    rlWeatherView.setVisibility(View.VISIBLE);
                    tvTodayWeatherMinMax.setVisibility(View.GONE);
                    llWeekView.setVisibility(View.GONE);
                    llWeatherNoInternet.setVisibility(View.VISIBLE);
                    llWeatherOtherDetail.setVisibility(View.GONE);
                }
            } else {
                llWeatherNoInternet.setVisibility(View.GONE);
                llWeatherOtherDetail.setVisibility(View.VISIBLE);
//                Log.e("++++ weeklyWeather ++++", "" + weeklyWeather);
                WeeklyWeatherData weeklyWeatherData = new Gson().fromJson(weeklyWeather.toString(), WeeklyWeatherData.class);
                List<WeeklyWeatherData.WeatherDatum> weatherData = weeklyWeatherData.getData().getResult().getWeatherData();

                JSONObject jsonObject = new JSONObject(weeklyWeather);
                JSONObject data = jsonObject.optJSONObject("data");
                if (data != null && data.length() > 0) {
                    parseWeatherData(data);
                }

                if (!TextUtils.isEmpty(weeklyWeatherData.getData().getResult().getLocationCity())) {
                    tvTodayWeatherDistrict.setText("" + weeklyWeatherData.getData().getResult().getLocationCity());
                } else {
                    tvTodayWeatherDistrict.setText(stateEnglishToHindWord(sharedPreferences.getString(Constants.SharedPreference_City, "")));
                }

                tvWeatherWarningTxt.setText("" + weeklyWeatherData.getData().getResult().getSuitableText());
                if (weeklyWeatherData.getData().getResult().getIsSuitable() == 1) {
                    ivWeatherWarning.setImageResource(R.mipmap.weather_right);
                    tvWeatherWarningTxt.setTextColor(getResources().getColor(R.color.GREEN));
                } else {
                    ivWeatherWarning.setImageResource(R.mipmap.weather_warning);
                    tvWeatherWarningTxt.setTextColor(getResources().getColor(R.color.colorAccent));
                }

                tvTodayWeatherDate.setText("" + DateUtils.parseDateHindi(Functions.getCurrentDate(), "dd/MM/yyyy", "dd MMMM yyyy"));

                if (weatherData.size() > 0) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                    Date todayDate = Calendar.getInstance().getTime();
                    String todayDateformat = sdf.format(todayDate);
                    Log.v("today: ", "" + todayDateformat);

                    Date strDate = sdf.parse(weatherData.get(6).getDate());
                    Calendar expireDateTime = Calendar.getInstance();
                    expireDateTime.setTime(strDate);
                    expireDateTime.set(Calendar.HOUR_OF_DAY, 23);
                    expireDateTime.set(Calendar.MINUTE, 59);
                    expireDateTime.set(Calendar.SECOND, 59);

                    if (System.currentTimeMillis() > expireDateTime.getTimeInMillis()) {
                        getWeeklyWeather(false);
                    } else {
                        ArrayList<String> dateLists = Functions.getStartEndCustomizeDate();
                        ArrayList<WeeklyWeatherData.WeatherDatum> weatherData1 = new ArrayList<WeeklyWeatherData.WeatherDatum>();
                        for (int i = 0; i < weatherData.size(); i++) {
                            if (todayDateformat.equalsIgnoreCase(sdf.format(sdf.parse(weatherData.get(i).getDate())))) {
                                tvTodayWeatherDate.setText("" + DateUtils.parseDateHindi(weatherData.get(i).getDate(), "yyyy-MM-dd", "dd MMMM yyyy"));
                                tvTodayWeatherMinMax.setText("" + weatherData.get(i).getMin() + "\u2103 / " + weatherData.get(i).getMax() + "\u2103");
                                tvWeatherExpText.setText(weatherData.get(i).getRainfallPerc() + getString(R.string.str_baris_aasar));
                            }

                            for (int j = 0; j < dateLists.size(); j++) {
                                if (dateLists.get(j).equalsIgnoreCase(sdf.format(sdf.parse(weatherData.get(i).getDate())))) {
                                    if (!todayDateformat.equalsIgnoreCase(sdf.format(sdf.parse(weatherData.get(i).getDate())))) {
                                        WeeklyWeatherData.WeatherDatum weatherDatum = new WeeklyWeatherData.WeatherDatum();
                                        weatherDatum.setDate(weatherData.get(i).getDate());
                                        weatherDatum.setMax(weatherData.get(i).getMax());
                                        weatherDatum.setMin(weatherData.get(i).getMin());
                                        weatherDatum.setRainfall(weatherData.get(i).getRainfall());
                                        weatherDatum.setRainfallPerc(weatherData.get(i).getRainfallPerc());
                                        weatherDatum.setWeatherCondition(weatherData.get(i).getWeatherCondition());
                                        weatherData1.add(weatherDatum);
                                    }
                                }
                            }
                        }

                        rcvWeather.setAdapter(new WeatherDetailAdapter(weatherData1));
                        if (weatherData1.size() > 0) {
                            cardWeatherData.setVisibility(View.VISIBLE);
                            rlWeatherView.setVisibility(View.VISIBLE);
                        } else {
                            cardWeatherData.setVisibility(View.GONE);
                            rlWeatherView.setVisibility(View.GONE);
                        }
                    }
                } else {
                    llWeatherNoInternet.setVisibility(View.VISIBLE);
                    llWeatherOtherDetail.setVisibility(View.GONE);
                    rlMoreWeatherData.setVisibility(View.GONE);
                    tvWeatherMsgNew.setText(homeActivity.getResources().getString(R.string.str_mosam_not_available));
                }
            }
            topSticky();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class WeatherDetailAdapter extends RecyclerView.Adapter<WeatherDetailAdapter.ViewHolder> {
        List<WeeklyWeatherData.WeatherDatum> weatherData;

        public WeatherDetailAdapter(List<WeeklyWeatherData.WeatherDatum> weatherData) {
            this.weatherData = weatherData;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.weather_list_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            try {
                holder.tvDayName.setText(Functions.getDayFromDateString(weatherData.get(position).getDate()));
                holder.tvWeather.setText("" + weatherData.get(position).getMin() + "/" + weatherData.get(position).getMax() + " \u2103");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return weatherData.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView tvDayName, tvWeather;

            public ViewHolder(View itemView) {
                super(itemView);
                this.tvDayName = (TextView) itemView.findViewById(R.id.tvDayName);
                this.tvWeather = (TextView) itemView.findViewById(R.id.tvWeather);
            }
        }
    }


    void getProfile() {
        progressBarDialog.showProgressDialogWithTitle("6");
        RetrofitClient.getInstance().getApi().getProfile(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(homeActivity),
                sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), SharedPreferencesUtils.getUserId(homeActivity)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                progressBarDialog.hideProgressDialogWithTitle();
                if (response != null && response.isSuccessful()) {
                    try {
                        String r_str = response.body().string();
                        if (!TextUtils.isEmpty(r_str)) {
                            JSONObject jb = new JSONObject(r_str);
                            if (jb != null && jb.length() > 0) {
                                ProfileResultData profileResultData = new InsertHomeFeedData(jb).insertData();
                                RongoApp.getCropWeekDaysFunction();
                                displayData(profileResultData);
                            }
                        }
                    } catch (Exception e) {
                        progressBarDialog.hideProgressDialogWithTitle();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        });
    }

    void getMasterDropDownData() {
        RetrofitClient.getInstance().getApi().getMasterDropdownData(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(homeActivity),
                sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                SharedPreferencesUtils.getUserId(homeActivity)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String r_str = response.body().string();
                        if (!TextUtils.isEmpty(r_str)) {
                            JSONObject jb = new JSONObject(r_str);
                            if (jb != null && jb.length() > 0) {
                                if (jb.optString("status_code").equalsIgnoreCase("0")) {

                                    JSONObject data = jb.optJSONObject("data");
                                    if (data != null && data.length() > 0) {
                                        JSONObject result = data.optJSONObject("result");
                                        if (result != null && result.length() > 0) {

                                            JSONArray jsonArray = result.optJSONArray("rewards");
                                            if (jsonArray != null && jsonArray.length() > 0) {
                                                SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                                        Constants.SharedPreferences_REWARD_LIST, "" + jsonArray);
                                            }

                                            JSONArray report_post_reason = result.optJSONArray("report_post_reason");
                                            if (report_post_reason != null && report_post_reason.length() > 0) {
                                                SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                                        Constants.SharedPreferences_ReportPost, "" + report_post_reason);
                                            }

                                            JSONArray report_comment_reason = result.optJSONArray("report_comment_reason");
                                            if (report_comment_reason != null && report_comment_reason.length() > 0) {
                                                SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                                        Constants.SharedPreferences_ReportComment, "" + report_comment_reason);
                                            }

                                            JSONObject seed_type = result.optJSONObject("seed_type");
                                            List<SeedTypeData> seedTypeData = new ArrayList<>();
                                            if (seed_type != null && seed_type.length() > 0) {
                                                Iterator<String> keys = seed_type.keys();
                                                while (keys.hasNext()) {
                                                    String key = keys.next();
                                                    String value = seed_type.optString(key);
                                                    seedTypeData.add(new SeedTypeData(key, value));
                                                }
                                                SharedPreferencesUtils.saveSeedTypes(homeActivity, seedTypeData, Constants.SharedPreferences_SEED_TYPES);
                                            }

                                            JSONObject problem_related = result.optJSONObject("problem_related");
                                            List<ProblemRelatedData> optionsList = new ArrayList<>();
                                            if (problem_related != null && problem_related.length() > 0) {
                                                Iterator<String> keys = problem_related.keys();
                                                while (keys.hasNext()) {
                                                    String key = keys.next();
                                                    String value = problem_related.optString(key);
                                                    optionsList.add(new ProblemRelatedData(key, value));
                                                }
                                                SharedPreferencesUtils.saveProblemRelated(homeActivity, optionsList, Constants.SharedPreferences_OPTIONS);
                                            }

                                            JSONObject crop_area_focused = result.optJSONObject("crop_area_focused");
                                            final List<CropAreaFocusedData> cropAreaFocusedData = new ArrayList<>();
                                            if (crop_area_focused != null && crop_area_focused.length() > 0) {

                                                Iterator keys = crop_area_focused.keys();
                                                while (keys.hasNext()) {
                                                    final String key = (String) keys.next();
                                                    JSONObject jsonObject = crop_area_focused.optJSONObject(key);

                                                    final String text = jsonObject.optString("text");
                                                    final String description = jsonObject.optString("description");
                                                    final String image = jsonObject.optString("image");
                                                    cropAreaFocusedData.add(new CropAreaFocusedData(key, text, description, image));
                                                }

                                                SharedPreferencesUtils.saveCropArea(homeActivity, cropAreaFocusedData, Constants.SharedPreferences_CROP_FOCUSED_AREA);

                                                Intent i = new Intent(homeActivity, CropImagesServices.class);
                                                CropImagesServices.enqueueWork(homeActivity, i);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }


    void getWeeklyAdvisory() {
        if (progressBarDialog != null) {
            progressBarDialog.hideProgressDialogWithTitle();
        }
        RetrofitClient.getInstance().getApi().getWeeklyAdvisory(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                Functions.getDeviceid(homeActivity), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                SharedPreferencesUtils.getUserId(homeActivity), sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, String.valueOf(RongoApp.getCropWeeksFunction()))).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String r = response.body().string();
                        if (!TextUtils.isEmpty(r)) {
                            JSONObject jsonObject = new JSONObject(r);
                            if (jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject data = jsonObject.optJSONObject("data");
                                    if (data != null && data.length() > 0) {
                                        JSONObject result = data.optJSONObject("result");
                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                                Constants.SharedPreferences_Weekly_Advisory, "" + result);

                                        parseWeeklyJson(result);
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void parseWeeklyJson(JSONObject jsonObject) {
//        Log.v("parseWeeklyJson", "" + jsonObject);
        try {
            if (jsonObject != null && jsonObject.length() > 0) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            llSavdhani.setVisibility(View.VISIBLE);
                            JSONObject keyword_advisory = jsonObject.optJSONObject("keyword_advisory");
                            String keyword_js = "";
                            if (keyword_advisory != null && keyword_advisory.length() > 0) {
                                keyword_js = keyword_advisory.toString();
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_JSON_KEYWORD, keyword_js);

                                Intent i = new Intent(homeActivity, PreloadImagesServices.class);
                                PreloadImagesServices.enqueueWork(homeActivity, i);
                            }

                            JSONObject weekly_advisory = jsonObject.optJSONObject("weekly_advisory");
                            if (weekly_advisory != null && weekly_advisory.length() > 0) {
                                String stage_name = weekly_advisory.optString("stage");
                                image_path = weekly_advisory.optString("image_path");

                                JSONArray jsonArray = weekly_advisory.optJSONArray("general_advice");
                                generalAdvisory.clear();
                                if (jsonArray != null && jsonArray.length() > 0) {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        if (!TextUtils.isEmpty(jsonArray.getString(i))) {
                                            generalAdvisory.add(jsonArray.getString(i));
                                        }
                                    }
                                }

                                JSONArray advisory_image = weekly_advisory.optJSONArray("advisory_image");
                                advisoryImages.clear();
                                if (advisory_image != null && advisory_image.length() > 0) {
                                    for (int i = 0; i < advisory_image.length(); i++) {
                                        advisoryImages.add(advisory_image.optString(i));
                                    }
                                }

                                if (generalAdvisory != null && generalAdvisory.size() > 0) {
                                    llSavdhani.setVisibility(View.VISIBLE);
                                    relativeArrow.setVisibility(View.VISIBLE);
                                    rlSamanySalah.setVisibility(View.GONE);
                                    if (generalAdvisory.size() == 1) {
                                        imgNext.setVisibility(View.GONE);
                                        imgPrevios.setVisibility(View.GONE);
                                    } else {
                                        imgNext.setVisibility(View.VISIBLE);
                                        imgPrevios.setVisibility(View.VISIBLE);
                                    }
                                    bindData(generalAdvisory, advisoryImages);
                                } else {
                                    llSavdhani.setVisibility(View.GONE);
                                    rlSamanySalah.setVisibility(View.GONE);
                                }

                                chemical_info = weekly_advisory.optJSONArray("chemical_info");
                                if (chemical_info != null && chemical_info.length() > 0) {
                                    if (!TextUtils.isEmpty(chemical_info.getString(0))) {
                                        txtChecmicalName.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    txtChecmicalName.setVisibility(View.GONE);
                                }

                                JSONArray chemical = weekly_advisory.optJSONArray("chemical");
                                String content = "";
                                chemicalNeeds = new ArrayList<>();
                                if (chemical != null && chemical.length() > 0) {
                                    for (int i = 0; i < chemical.length(); i++) {
                                        if (!TextUtils.isEmpty(chemical.getString(i))) {
                                            chemicalNeeds.add(chemical.getString(i));
                                            content += Functions.fromHtml(chemical.getString(i));
                                        }
                                    }
                                }

                                if (chemicalNeeds != null && chemicalNeeds.size() > 0) {
                                    llChemical.setVisibility(View.VISIBLE);
                                    llRokdhamTagView.setVisibility(View.VISIBLE);
                                    ChemicalAdapter chemicalAdapter = new ChemicalAdapter(homeActivity, chemicalNeeds, PhasalFragment.this);
                                    rvChemical.setAdapter(chemicalAdapter);

                                    String finalContent = content;
                                    imgPlay.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if (homeActivity.tts != null) {
                                                homeActivity.tts.speak(finalContent, imgPlay, pbChemical);
                                            } else {
                                                HomeActivity.setTextToSpeechInit();
                                                homeActivity.tts.speak(finalContent, imgPlay, pbChemical);
                                            }
                                        }
                                    });
//                        imgPlay.setOnClickListener(new TTSAudioPlayerClickListener(homeActivity, content, imgPlay, pbChemical, "Phasal (Chemical)"));
                                } else {
                                    llChemical.setVisibility(View.GONE);
                                    llRokdhamTagView.setVisibility(View.GONE);
                                }

                                int showFertilizer = Functions.getFertilizerWeeks(sharedPreferences);
                                if (showFertilizer >= Integer.parseInt(RongoApp.getCurrentCropWeek())) {
                                    llFertilizer.setVisibility(View.VISIBLE);
                                    llRokdhamTagView.setVisibility(View.VISIBLE);
                                } else {
                                    if (chemicalNeeds != null && chemicalNeeds.size() > 0) {
                                        llRokdhamTagView.setVisibility(View.VISIBLE);
                                    } else {
                                        llFertilizer.setVisibility(View.GONE);
                                    }
                                }


                                JSONArray disease = weekly_advisory.optJSONArray("disease");
                                List<DiasesData> diasesData = new ArrayList<>();
                                if (disease != null && disease.length() > 0) {
                                    for (int i = 0; i < disease.length(); i++) {
                                        JSONObject jd = disease.optJSONObject(i);
                                        String name = jd.optString("name");
                                        String keyword_id = jd.optString("keyword_id");

                                        diasesData.add(new DiasesData(name, keyword_id));
                                    }
                                }

                                JSONArray pest = weekly_advisory.optJSONArray("pest");
                                List<PestData> pestData = new ArrayList<>();
                                if (pest != null && pest.length() > 0) {
                                    for (int i = 0; i < pest.length(); i++) {
                                        JSONObject jd = pest.optJSONObject(i);
                                        String name = jd.optString("name");
                                        String keyword_id = jd.optString("keyword_id");

                                        pestData.add(new PestData(name, keyword_id));
                                    }
                                }

                                JSONArray nutrition = weekly_advisory.optJSONArray("nutrition");
                                List<NutritionData> nutritionData = new ArrayList<>();
                                if (nutrition != null && nutrition.length() > 0) {
                                    for (int i = 0; i < nutrition.length(); i++) {
                                        JSONObject jd = nutrition.optJSONObject(i);
                                        String name = jd.optString("name");
                                        String keyword_id = jd.optString("keyword_id");
                                        nutritionData.add(new NutritionData(name, keyword_id));
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

            setNewSavdhaniView();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchDataFromAssetFileForWeeklyAdvisory() {
        try {
            if (progressBarDialog != null) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
            String cropWeekNew = String.valueOf(RongoApp.getCropWeekDaysFunction());
//            Log.e("fetchAssetAdvisory", "" + cropWeekNew + " - " + RongoApp.getSelectedCrop());
            JSONArray cropData = new JSONArray(FetchJsonFromAssetsFile.loadAdvisoryJSONFromAsset(homeActivity));
            if (cropData != null && cropData.length() > 0) {
                for (int i = 0; i < cropData.length(); i++) {
                    JSONObject resultData = cropData.getJSONObject(i);
                    JSONObject result = resultData.optJSONObject("result");
//                    Log.e("++2++", i+" - " + result);
                    if (i == Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, cropWeekNew))) {
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Weekly_Advisory, "" + result);
                        parseWeeklyJson(result);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void bindData(List<String> generalAdvisory, List<String> advisoryImages) {
        rcvMyPager.setLayoutManager(new CarouselLayoutManager(homeActivity, LinearLayoutManager.HORIZONTAL, false));
        rcvMyPager.setAdapter(new MyPagerAdapter(homeActivity, generalAdvisory, advisoryImages, image_path, audioPlayerListener));
        topSticky();
    }

    int getCurrentItem() {
        return ((LinearLayoutManager) rcvMyPager.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
    }

    void setCurrentItem(int position, boolean smooth) {
//        Toast.makeText(homeActivity, "" + position, Toast.LENGTH_SHORT).show();
        if (smooth)
            rcvMyPager.smoothScrollToPosition(position);
        else
            rcvMyPager.scrollToPosition(position);
    }


    void submitSessionEndData(String metadata) {
        RetrofitClient.getInstance().getApi().submitSeasonEndFeedback(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), SharedPreferencesUtils.getUserId(homeActivity),
                sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""), metadata).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SessionEndCropID, RongoApp.getSelectedCrop());

                    cardfeedback.setVisibility(View.GONE);
//                    cardNewCropAdd.setVisibility(View.VISIBLE);
//                    openThankYou("Y", getWeeklyCropInfoRewardPoint());
                    getProfile();
//                    openThankYouSessionEnd();
                    openSessionEndToAddNewCrop();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void openSessionEndToAddNewCrop() {
        try {
            Dialog view = new Dialog(homeActivity, R.style.NewDialog);
            view.setContentView(R.layout.dialog_add_new_session_crop);
            view.setCanceledOnTouchOutside(false);
            view.setCancelable(false);
            view.show();

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = view.getWindow();
            lp.copyFrom(window.getAttributes());
            lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            window.setAttributes(lp);

            ImageView imgClose = view.findViewById(R.id.imgClose);
            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    view.dismiss();
                }
            });

            RecyclerView rcvCrop = view.findViewById(R.id.rcvCrop);
            rcvCrop.setLayoutManager(new LinearLayoutManager(homeActivity, LinearLayoutManager.HORIZONTAL, false));

            List<MasterCropData> masterCropList = Functions.getMasterCropList(sharedPreferences);
            List<MasterCropData> masterCropData = new ArrayList<MasterCropData>();
            if (masterCropList != null && masterCropList.size() > 0) {
                for (int j = 0; j < masterCropList.size(); j++) {
                    if (masterCropList.get(j).getCrop_id().equalsIgnoreCase(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, "1"))) {
                        masterCropData.add(new MasterCropData(masterCropList.get(j).getCrop_name(), masterCropList.get(j).getCrop_id(),
                                masterCropList.get(j).getCrop_image(), masterCropList.get(j).getDisplay_name(), "1"));
                    } else {
                        masterCropData.add(new MasterCropData(masterCropList.get(j).getCrop_name(), masterCropList.get(j).getCrop_id(),
                                masterCropList.get(j).getCrop_image(), masterCropList.get(j).getDisplay_name(), "0"));
                    }
                }
            } else {
                masterCropData.add(new MasterCropData("Maize", "1", "", "मक्का", "0"));
            }

            CropAdapter cropAdapter = new CropAdapter(masterCropData);
            rcvCrop.setAdapter(cropAdapter);

            AppCompatTextView txtEnter = view.findViewById(R.id.txtEnter);
            txtEnter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String cropId = cropAdapter.getSelectedCropId();
                    if (!TextUtils.isEmpty(cropId)) {

                        if (!sharedPreferences.getString(Constants.SharedPreferences_season_feedback, "").equalsIgnoreCase("")) {
                            if (RongoApp.getSelectedCrop().equalsIgnoreCase(cropId)) {
//                                addCropDialog(cropId);
                                homeActivity.addCropDialog(cropId);
                            } else {
                                List<CropsData> cropsData = Functions.getCropDataByID(cropId);
                                if (cropsData.size() > 0) {
                                    homeActivity.endCurrentSession(cropId, RongoApp.getSelectedCrop());
                                    for (int i = 0; i < cropsData.size(); i++) {
                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_SowingDate, cropsData.get(i).getDate_of_sowing());
                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_CROP_ID, cropsData.get(i).getCrop_id());
                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_P_CROP_ID, cropsData.get(i).getP_crop_id());
                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Field_ID, cropsData.get(i).getField_id());
                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Sowing_Done, cropsData.get(i).getIs_sowing_done());
                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_season_end_crop_week, cropsData.get(i).getSeason_end_crop_week());
                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_crop_status, cropsData.get(i).getCrop_status());
                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_season_feedback, cropsData.get(i).getSeason_feedback());
                                    }
                                } else {
//                                    addCropDialog(cropId);
                                    homeActivity.addCropDialog(cropId);
                                }
                            }
                        }

                        view.dismiss();
                    } else {
                        Toast.makeText(homeActivity, getString(R.string.str_please_select_fasal), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class CropAdapter extends RecyclerView.Adapter<CropAdapter.ViewHolder> {
        List<MasterCropData> listdata;
        List<String> profileCropList = Functions.getSelectedCropIdList(sharedPreferences);

        public CropAdapter(List<MasterCropData> listdata) {
            this.listdata = listdata;
        }

        public String getSelectedCropId() {
            String cropId = "";
            for (int i = 0; i < listdata.size(); i++) {
                if (listdata.get(i).getStatus().equalsIgnoreCase("1")) {
                    cropId = listdata.get(i).getCrop_id();
                }
            }
            return cropId;
        }

        @NotNull
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.crop_list_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            try {
                holder.tvTitle.setText(listdata.get(position).getDisplay_name());

                holder.llCropView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listdata.get(position).getStatus().equalsIgnoreCase("1")) {
                            listdata.get(position).setStatus("0");
                        } else {
                            for (int i = 0; i < listdata.size(); i++) {
                                if (listdata.get(position).getCrop_id().equalsIgnoreCase(listdata.get(i).getCrop_id())) {
                                    listdata.get(i).setStatus("1");
                                } else {
                                    listdata.get(i).setStatus("0");
                                }
                            }
                        }
                        notifyDataSetChanged();
                    }
                });

                if (listdata.get(position).getStatus().equalsIgnoreCase("1")) {
                    holder.llCropView.setBackground(getResources().getDrawable(R.drawable.rounded_layout_selected));
                    holder.ivTick.setVisibility(View.VISIBLE);
                    holder.ivTick.setImageResource(R.mipmap.c_tick);
                } else {
                    holder.llCropView.setBackground(getResources().getDrawable(R.drawable.rounded_layout_unselect));
                    holder.ivTick.setVisibility(View.GONE);

                    if (profileCropList.size() > 0) {
                        if (!profileCropList.contains(listdata.get(position).getCrop_id())) {
                            holder.llCropView.setBackground(getResources().getDrawable(R.drawable.rounded_layout_unselect));
                            holder.ivTick.setVisibility(View.VISIBLE);
                            holder.ivTick.setImageResource(R.mipmap.c_add);
                        }
                    }

                    if (RongoApp.getSessionEndCropId().equalsIgnoreCase(listdata.get(position).getCrop_id())) {
                        holder.llCropView.setBackground(getResources().getDrawable(R.drawable.rounded_layout_unselect));
                        holder.ivTick.setVisibility(View.VISIBLE);
                        holder.ivTick.setImageResource(R.mipmap.c_add);
                    }
                }

                if (!TextUtils.isEmpty(listdata.get(position).getCrop_image()) && InternetConnection.checkConnectionForFasl(homeActivity)) {
//                    Functions.displayImageWithPlaceHolder(listdata.get(position).getCrop_image(), holder.ivCrop);

                    GlideApp.with(homeActivity).load(listdata.get(position).getCrop_image())
                            .apply(Functions.getPlaceholder(listdata.get(position).getCrop_id())).into(holder.ivCrop);
                } else {
                    if (listdata.get(position).getCrop_id().equalsIgnoreCase("1")) {
                        holder.ivCrop.setImageResource(R.mipmap.maize);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("2")) {
                        holder.ivCrop.setImageResource(R.mipmap.rice);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("3")) {
                        holder.ivCrop.setImageResource(R.mipmap.cotton);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("4")) {
                        holder.ivCrop.setImageResource(R.mipmap.wheat);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("5")) {
                        holder.ivCrop.setImageResource(R.mipmap.okra);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("6")) {
                        holder.ivCrop.setImageResource(R.mipmap.pearmillet);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("7")) {
                        holder.ivCrop.setImageResource(R.mipmap.corinder);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("8")) {
                        holder.ivCrop.setImageResource(R.mipmap.bittergourd);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("9")) {
                        holder.ivCrop.setImageResource(R.mipmap.bottlegourd);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("10")) {
                        holder.ivCrop.setImageResource(R.mipmap.watermelon);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("11")) {
                        holder.ivCrop.setImageResource(R.mipmap.groundnut);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("12")) {
                        holder.ivCrop.setImageResource(R.mipmap.soyabean);
                    } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("13")) {
                        holder.ivCrop.setImageResource(R.mipmap.caster);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return listdata.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvTitle;
            ImageView ivCrop, ivTick;
            RelativeLayout llCropView;

            public ViewHolder(View itemView) {
                super(itemView);
                this.llCropView = (RelativeLayout) itemView.findViewById(R.id.llCropView);
                this.tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
                this.ivCrop = (ImageView) itemView.findViewById(R.id.ivCrop);
                this.ivTick = (ImageView) itemView.findViewById(R.id.ivTick);
            }
        }
    }

    void addCropDialog(String crop_id) {
        String[] is_sowing_done = {""};
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_add_crop_dialog, null);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setCanceledOnTouchOutside(false);

        AppCompatTextView txtOfflineSMS = view.findViewById(R.id.txtOfflineSMS);
        txtOfflineSMS.setVisibility(View.GONE);
        if (!InternetConnection.checkConnectionForFasl(homeActivity)) {
            txtOfflineSMS.setVisibility(View.VISIBLE);
        }

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        TextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getSowingDateRewardPoint(sharedPreferences));

        LinearLayout llStatusBtn = view.findViewById(R.id.llStatusBtn);
        llStatusBtn.setVisibility(View.VISIBLE);
        LinearLayout llDateView = view.findViewById(R.id.llDateView);
        llDateView.setVisibility(View.GONE);

        TextView txtYourSowingText = view.findViewById(R.id.txtYourSowingText);
        AppCompatEditText edtSowingDate = view.findViewById(R.id.editTextSowingDate);

        TextView txtYes = view.findViewById(R.id.txtYes);
        TextView txtNo = view.findViewById(R.id.txtNo);

        TextView txtEnter = view.findViewById(R.id.txtEnter);

        txtYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                llStatusBtn.setVisibility(View.GONE);
                llDateView.setVisibility(View.VISIBLE);
                txtYourSowingText.setText(R.string.str_apni_buvay_ki_date_darj_kare);
                edtSowingDate.setHint(R.string.str_date_darj_kare);
                is_sowing_done[0] = "yes";
            }
        });

        txtNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                llStatusBtn.setVisibility(View.GONE);
                llDateView.setVisibility(View.VISIBLE);
                txtYourSowingText.setText(R.string.str_apni_expe_buvay_date_dark_kare);
                edtSowingDate.setHint(R.string.str_date_darj_kare);
                is_sowing_done[0] = "no";
            }
        });

        edtSowingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                openDatePicker(edtSowingDate, is_sowing_done[0]);
            }
        });

        txtEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(crop_id)) {
                    if (!TextUtils.isEmpty(edtSowingDate.getText().toString().trim())) {
                        bottomSheetDialog.dismiss();
                        if (InternetConnection.checkConnection(homeActivity)) {
                            txtOfflineSMS.setVisibility(View.GONE);
                            progressBarDialog.showProgressDialogWithTitle("7");
                            homeActivity.startNewSession(edtSowingDate, crop_id, RongoApp.getSelectedCrop());
                        } else {
                            txtOfflineSMS.setVisibility(View.VISIBLE);
                            homeActivity.addOfflineCrop(crop_id, edtSowingDate.getText().toString().trim(), true);
                        }
                    } else {
                        Toast.makeText(homeActivity, getString(R.string.str_apni_buvay_ki_date_darj_kare), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(homeActivity, getString(R.string.str_please_select_fasal), Toast.LENGTH_SHORT).show();
                }
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }


    void setSessionEndData() {
        try {
            llEndRewardView.setVisibility(View.GONE);

            CardView sessionEndCard = view.findViewById(R.id.sessionEndCard);
            sessionEndCard.setVisibility(View.GONE);

            CardView cardAddCrop = view.findViewById(R.id.cardAddCrop);
            cardAddCrop.setVisibility(View.GONE);
            cardAddCrop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openSessionEndToAddNewCrop();
                }
            });

            ImageView ivWeeklyCropQues1 = view.findViewById(R.id.ivWeeklyCropQues1);
            ivWeeklyCropQues1.setVisibility(View.GONE);

            TextView tvQuestions1 = view.findViewById(R.id.tvQuestions1);
            TextView tvSubQues = view.findViewById(R.id.tvSubQues);
            tvSubQues.setVisibility(View.GONE);

            TextView tvRewardPointCropInfoQues1 = view.findViewById(R.id.tvRewardPointCropInfoQues1);
            tvRewardPointCropInfoQues1.setText(RewardPoints.getWeeklyCropInfoRewardPoint(sharedPreferences));

            LinearLayout llButtonView1 = view.findViewById(R.id.llButtonView1);
            llButtonView1.setVisibility(View.GONE);
            TextView tvHa1 = view.findViewById(R.id.tvHa1);
            TextView tvNa1 = view.findViewById(R.id.tvNa1);

            LinearLayout llSelectionView1 = view.findViewById(R.id.llSelectionView1);
            llSelectionView1.setVisibility(View.GONE);
            ediOption1 = view.findViewById(R.id.ediOption1);
            RecyclerView rvOptions1 = view.findViewById(R.id.rvOptions1);
            rvOptions1.setLayoutManager(new LinearLayoutManager(homeActivity, RecyclerView.VERTICAL, false));
            TextView tvDarjKareEnd = view.findViewById(R.id.tvDarjKareEnd);

            LinearLayout llEditView1 = view.findViewById(R.id.llEditView1);
            llEditView1.setVisibility(View.GONE);
            EditText edtProduct1 = view.findViewById(R.id.edtProduct1);
            TextView tvDarjKareEnd1 = view.findViewById(R.id.tvDarjKareEnd1);

            rcvSessionFeedback = view.findViewById(R.id.rcvSessionFeedback);

            tvSessionEndOfflineMsg.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_Session_End_Data, ""))) {
                JSONObject result = new JSONObject(sharedPreferences.getString(Constants.SharedPreferences_Session_End_Data, ""));
                Log.i("Result", "" + result);
                if (result != null && result.length() > 0) {

                    tvEndQuiz.setText(Functions.spannableStr(result.optString("quiz_response") + " " + getString(R.string.str_quiz_right_answer),
                            0, result.optString("quiz_response").toString().trim().length()));

                    tvEndExpert.setText(Functions.spannableStr(result.optString("expert_advice") + " " + getString(R.string.str_total_expert_salah),
                            0, result.optString("expert_advice").toString().trim().length()));

                    tvEndReward.setText(Functions.spannableStr(result.optString("points_earned") + " " + getString(R.string.str_total_point_earned),
                            0, result.optString("points_earned").toString().trim().length()));

                    tvEndPhotos.setText(Functions.spannableStr(result.optString("upload_picture") + " " + getString(R.string.str_total_photo_darj),
                            0, result.optString("upload_picture").toString().trim().length()));

                    JSONArray jsonArray = new JSONArray(result.optString("reward_earned"));
                    if (jsonArray != null && jsonArray.length() > 0) {
                        llRewardList.removeAllViews();
                        llEndRewardView.setVisibility(View.VISIBLE);
                        for (int j = 0; j < jsonArray.length(); j++) {
                            JSONObject jsonObject = jsonArray.optJSONObject(j);

                            LinearLayout llMain = new LinearLayout(homeActivity);
                            llMain.setOrientation(LinearLayout.HORIZONTAL);

                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            params.setMargins(10, 14, 2, 0);
                            TextView textView = new TextView(homeActivity);
                            textView.setLayoutParams(params);
                            textView.setTag(jsonObject.optString("reward_title"));
                            textView.setText(" " + jsonObject.optString("reward_title"));
                            textView.setTextSize(19);

                            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            params1.setMargins(10, 0, 0, 2);
                            ImageView imageView = new ImageView(homeActivity);
                            imageView.setLayoutParams(params1);
                            imageView.setImageResource(R.mipmap.gift_s);

                            llMain.addView(imageView);
                            llMain.addView(textView);

                            llRewardList.addView(llMain);

                            view.setOnClickListener(clickInLinearLayout());
                        }
                    }
                }

                if (sharedPreferences.getString(Constants.SharedPreferences_season_feedback, "").equalsIgnoreCase("")) {
                    JSONArray jsonArray = new JSONArray(result.optString("feedback_questions"));
                    if (jsonArray != null && jsonArray.length() > 0) {
                        ArrayList<String> feedbackQuestionArray = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<String>>() {
                        }.getType());
                        ArrayList<FeedBackSubmitModel> feedBackSubmitModels = new ArrayList<FeedBackSubmitModel>();
                        for (int j = 0; j < feedbackQuestionArray.size(); j++) {
                            FeedBackSubmitModel feedBackSubmitModel = new FeedBackSubmitModel();
                            feedBackSubmitModel.setId("" + j);
                            feedBackSubmitModel.setText(feedbackQuestionArray.get(j));
                            feedBackSubmitModel.setStatus("0");
                            feedBackSubmitModel.setType1("0");
                            feedBackSubmitModel.setType2("0");
                            feedBackSubmitModel.setType3("0");
                            feedBackSubmitModels.add(feedBackSubmitModel);
                        }

                        rcvSessionFeedback.setLayoutManager(new LinearLayoutManager(homeActivity));
                        feedbackAdapter = new feedbackAdapter(feedBackSubmitModels);
                        rcvSessionFeedback.setAdapter(feedbackAdapter);
                    }
                } else {
//                    cardNewCropAdd.setVisibility(View.VISIBLE);
                    cardAddCrop.setVisibility(View.VISIBLE);
                    if (isCropEnd) {
                        isCropEnd = false;
//                        openSessionEndToAddNewCrop();
                    }
                }

                tvFeedbackSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        Log.e("feedback Submit", "" + feedbackAdapter.getFeedbackSubmitData());
                        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                            submitSessionEndData(feedbackAdapter.getFeedbackSubmitData());
                        } else {
                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Submit_SessionEnd_DATA, "" + feedbackAdapter.getFeedbackSubmitData());
                            cardfeedback.setVisibility(View.GONE);

                            SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                                    Constants.SharedPreferences_season_feedback, "" + Functions.getCurrentDateTime());
//                            openThankYou("Y", getWeeklyCropInfoRewardPoint());

                            openSessionEndToAddNewCrop();
                        }
                    }
                });
            } else {
                if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                    getSessionEndData();
                } else {
                    tvSessionEndOfflineMsg.setVisibility(View.VISIBLE);
                }
            }

            sessionEndCard.setVisibility(View.GONE);
            if (sharedPreferences.getString(Constants.SharedPreference_Sowing_Done, "").equalsIgnoreCase("yes")) {
                String currentcropWeeks = sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0");
                if (!TextUtils.isEmpty(RongoApp.getWeeklyCropQuestion())) {
                    JSONObject WeeklyCropInfo = new JSONObject(RongoApp.getWeeklyCropQuestion());
                    if (!TextUtils.isEmpty(WeeklyCropInfo.toString())) {
                        String weekCropDays = Functions.getWeeklyCropDay(sharedPreferences);
                        if (!TextUtils.isEmpty(weekCropDays)) {
                            JSONObject quesCropWeek = null;
                            String[] arrayStr = weekCropDays.split(",");
                            for (String s : arrayStr) {
                                weekCropDay = s.replace("\"", "");
                                List<WeeklyCropInfo> weeklyCropInfoCheck = appDatabase.weeklyCropInfoDao().getWeeklyCropInfo(
                                        currentcropWeeks, weekCropDay, "1", RongoApp.getSelectedPCrop());
                                if (weeklyCropInfoCheck.size() > 0) {
                                    sessionEndCard.setVisibility(View.GONE);
                                } else {
                                    sessionEndCard.setVisibility(View.VISIBLE);
                                    quesCropWeek = Objects.requireNonNull(Functions.getWeeklyCropQuestion(sharedPreferences)).optJSONObject(weekCropDay);
//                                    Log.e("+++@@End@@+++: ", weekCropDay + " - " + quesCropWeek);
                                    if (!TextUtils.isEmpty(quesCropWeek.toString())) {
                                        tvQuestions1.setText(quesCropWeek.getString("question"));

                                        inputType = "" + quesCropWeek.getString("input_type");
                                        infoText = "" + quesCropWeek.getString("info_text");
                                        infoType = "" + quesCropWeek.getString("info_type");

                                        if (infoType.equalsIgnoreCase("cost")) {
                                            ivWeeklyCropQues1.setVisibility(View.GONE);
                                        } else if (infoType.equalsIgnoreCase("crop")) {
                                            ivWeeklyCropQues1.setVisibility(View.GONE);
                                            tvSubQues.setVisibility(View.VISIBLE);
                                        } else if (infoType.equalsIgnoreCase("land_preparation")) {
                                            edtProduct1.setHint(homeActivity.getResources().getString(R.string.str_farm_area));
                                        }

                                        if (inputType.equalsIgnoreCase("button")) {
                                            ivWeeklyCropQues1.setVisibility(View.VISIBLE);
                                            llButtonView1.setVisibility(View.VISIBLE);
                                            llSelectionView1.setVisibility(View.GONE);
                                            llEditView1.setVisibility(View.GONE);
                                        } else if (inputType.equalsIgnoreCase("multi_dropdown")) {
                                            llButtonView1.setVisibility(View.GONE);
                                            ivWeeklyCropQues1.setVisibility(View.GONE);
                                            llSelectionView1.setVisibility(View.VISIBLE);
                                            llEditView1.setVisibility(View.GONE);
                                        } else if (inputType.equalsIgnoreCase("text")) {
                                            llButtonView1.setVisibility(View.GONE);
                                            ivWeeklyCropQues1.setVisibility(View.GONE);
                                            llSelectionView1.setVisibility(View.GONE);
                                            llEditView1.setVisibility(View.VISIBLE);
                                        }

                                        tvDarjKareEnd1.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                if (Functions.doubleTapCheck(mLastLongClick)) {
                                                    return;
                                                }
                                                mLastLongClick = SystemClock.elapsedRealtime();

                                                if (InternetConnection.checkConnection(homeActivity)) {
                                                    if (TextUtils.isEmpty(edtProduct1.getText().toString().trim())) {
                                                        Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_enter_expect_production));
                                                        return;
                                                    }

                                                    if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                                        setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, edtProduct1.getText().toString().trim(), getResources().getString(R.string.str_ha));
                                                    } else {
                                                        updateWeeklyCropQues(weekCropDay, infoType, infoText, edtProduct1.getText().toString().trim(), getResources().getString(R.string.str_ha));
                                                    }
                                                } else {
                                                    setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, edtProduct1.getText().toString().trim(), getResources().getString(R.string.str_ha));
                                                }
                                            }
                                        });

                                        ArrayList<String> listdata = new ArrayList<String>();
                                        JSONArray cropWeek = quesCropWeek.optJSONArray("options");
                                        if (cropWeek != null && cropWeek.length() > 0) {
                                            for (int i = 0; i < cropWeek.length(); i++) {
                                                listdata.add(cropWeek.getString(i));
                                            }
                                            questionOptionsAdapter = new CropQuesOptionsAdapter(listdata, PhasalFragment.this,
                                                    "select_multiple", selectedOption, stringList, arrayList);
                                            rvOptions1.setAdapter(questionOptionsAdapter);
                                        }

                                        ediOption1.setCursorVisible(false);
                                        ediOption1.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(final View arg0) {
                                                if (rvOptions1.getVisibility() == View.VISIBLE) {
                                                    rvOptions1.setVisibility(View.GONE);
                                                } else {
                                                    rvOptions1.setVisibility(View.VISIBLE);
                                                }
                                            }
                                        });

                                        tvDarjKareEnd.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                if (Functions.doubleTapCheck(mLastLongClick)) {
                                                    return;
                                                }
                                                mLastLongClick = SystemClock.elapsedRealtime();

                                                if (TextUtils.isEmpty(ediOption1.getText().toString().trim())) {
                                                    Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_please_select_option));
                                                    return;
                                                }

                                                if (rvOptions1.getVisibility() == View.VISIBLE) {
                                                    rvOptions1.setVisibility(View.GONE);
                                                }

                                                if (InternetConnection.checkConnection(homeActivity)) {
                                                    if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                                        setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, TextUtils.join(",", stringList), getResources().getString(R.string.str_ha));
                                                    } else {
                                                        updateWeeklyCropQues(weekCropDay, infoType, infoText, TextUtils.join(",", stringList), getResources().getString(R.string.str_ha));
                                                    }
                                                } else {
                                                    setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, TextUtils.join(",", stringList), getResources().getString(R.string.str_ha));
                                                }
                                            }
                                        });

                                        tvHa1.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                if (Functions.doubleTapCheck(mLastLongClick)) {
                                                    return;
                                                }
                                                mLastLongClick = SystemClock.elapsedRealtime();

                                                if (InternetConnection.checkConnection(homeActivity)) {
                                                    if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                                        setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, Functions.getCurrentDateTime(), getResources().getString(R.string.str_ha));
                                                    } else {
                                                        updateWeeklyCropQues(weekCropDay, infoType, infoText, Functions.getCurrentDateTime(), getResources().getString(R.string.str_ha));
                                                    }
                                                } else {
                                                    setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, Functions.getCurrentDateTime(), getResources().getString(R.string.str_ha));
                                                }
                                            }
                                        });

                                        tvNa1.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                if (Functions.doubleTapCheck(mLastLongClick)) {
                                                    return;
                                                }
                                                mLastLongClick = SystemClock.elapsedRealtime();

                                                if (InternetConnection.checkConnection(homeActivity)) {
                                                    if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                                        setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, "", getResources().getString(R.string.str_na));
                                                    } else {
                                                        updateWeeklyCropQues(weekCropDay, infoType, infoText, "", getResources().getString(R.string.str_na));
                                                    }
                                                } else {
                                                    setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, "", getResources().getString(R.string.str_na));
                                                }
                                            }
                                        });
                                    }
                                    break;
                                }
                            }

                            tvDarjKare1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (Functions.doubleTapCheck(mLastLongClick)) {
                                        return;
                                    }
                                    mLastLongClick = SystemClock.elapsedRealtime();

                                    if (InternetConnection.checkConnection(homeActivity)) {
                                        if (TextUtils.isEmpty(edtProduct.getText().toString().trim())) {
                                            Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_enter_production));
                                            return;
                                        }

                                        if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                            setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, edtProduct.getText().toString().trim(), getResources().getString(R.string.str_ha));
                                        } else {
                                            updateWeeklyCropQues(weekCropDay, infoType, infoText, edtProduct.getText().toString().trim(), getResources().getString(R.string.str_ha));
                                        }
                                    } else {
//                                Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), homeActivity.getString(R.string.no_connection));
                                        setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, edtProduct.getText().toString().trim(), getResources().getString(R.string.str_ha));
                                    }
                                }
                            });

                            ArrayList<String> listdata = new ArrayList<String>();
                            JSONArray optionsArray = quesCropWeek.optJSONArray("options");
                            if (optionsArray != null && optionsArray.length() > 0) {
                                for (int i = 0; i < optionsArray.length(); i++) {
                                    listdata.add(optionsArray.getString(i));
                                }
                                spinnerOptions.setAdapter(new SpinnerArrayAdapter(homeActivity, R.layout.spinner_item_list, listdata));
                                spinnerOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        options = parent.getItemAtPosition(position).toString();
//                                        Log.v("Selected Option: ", "" + options);
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {
                                    }
                                });
                            }

                            tvDarjKare.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (Functions.doubleTapCheck(mLastLongClick)) {
                                        return;
                                    }
                                    mLastLongClick = SystemClock.elapsedRealtime();

                                    if (InternetConnection.checkConnection(homeActivity)) {
                                        if (TextUtils.isEmpty(options)) {
                                            Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_please_select_option));
                                            return;
                                        }

                                        if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                            setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, options, getResources().getString(R.string.str_ha));
                                        } else {
                                            updateWeeklyCropQues(weekCropDay, infoType, infoText, options, getResources().getString(R.string.str_ha));
                                        }
                                    } else {
//                                Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), homeActivity.getString(R.string.no_connection));
                                        setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, options, getResources().getString(R.string.str_ha));
                                    }
                                }
                            });

                            tvHa.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (Functions.doubleTapCheck(mLastLongClick)) {
                                        return;
                                    }
                                    mLastLongClick = SystemClock.elapsedRealtime();

                                    if (InternetConnection.checkConnection(homeActivity)) {
                                        if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                            setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, Functions.getCurrentDateTime(), getResources().getString(R.string.str_ha));
                                        } else {
                                            updateWeeklyCropQues(weekCropDay, infoType, infoText, Functions.getCurrentDateTime(), getResources().getString(R.string.str_ha));
                                        }
                                    } else {
                                        setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, Functions.getCurrentDateTime(), getResources().getString(R.string.str_ha));
                                    }
                                }
                            });

                            tvNa.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (Functions.doubleTapCheck(mLastLongClick)) {
                                        return;
                                    }
                                    mLastLongClick = SystemClock.elapsedRealtime();

                                    if (InternetConnection.checkConnection(homeActivity)) {
                                        if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                            setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, "", getResources().getString(R.string.str_na));
                                        } else {
                                            updateWeeklyCropQues(weekCropDay, infoType, infoText, "", getResources().getString(R.string.str_na));
                                        }
                                    } else {
                                        setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, "", getResources().getString(R.string.str_na));
                                    }
                                }
                            });

                        }
                    } else {
                        getWeeklyCropInfoQuestionData();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void selectedMultiple(List<String> stringList, List<String> stringListNames) {
        this.arrayList = stringList;
        this.stringList = stringListNames;
        ediOption1.setText(TextUtils.join(",", stringListNames));
    }

    class feedbackAdapter extends RecyclerView.Adapter<feedbackAdapter.ViewHolder> {
        private List<FeedBackSubmitModel> listdata;

        public feedbackAdapter(List<FeedBackSubmitModel> listdata) {
            this.listdata = listdata;
        }

        String getFeedbackSubmitData() {
            String type = "", value = "";
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < listdata.size(); i++) {
                try {
                    type = "";
                    value = "";
                    JSONObject obj = new JSONObject();
                    if (listdata.get(i).getType1().equalsIgnoreCase("1")) {
                        type = getResources().getString(R.string.str_buri);
                        value = "0";
                    } else if (listdata.get(i).getType2().equalsIgnoreCase("1")) {
                        type = getResources().getString(R.string.str_thik);
                        value = "1";
                    } else if (listdata.get(i).getType3().equalsIgnoreCase("1")) {
                        type = getResources().getString(R.string.str_achi);
                        value = "2";
                    }
                    obj.put("value", value);
                    obj.put("feedback", type);
                    obj.put("date", Functions.getCurrentDateTime());
                    obj.put("question", listdata.get(i).getText());
                    jsonArray.put(obj);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return jsonArray.toString();
        }

        @NotNull
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.feedback_list_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            try {
                holder.tvTitle.setText(listdata.get(position).getText());

                holder.iv1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listdata.get(position).setType1("1");
                        listdata.get(position).setType2("0");
                        listdata.get(position).setType3("0");
                        notifyDataSetChanged();
                    }
                });

                holder.iv2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listdata.get(position).setType1("0");
                        listdata.get(position).setType2("1");
                        listdata.get(position).setType3("0");
                        notifyDataSetChanged();
                    }
                });

                holder.iv3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listdata.get(position).setType1("0");
                        listdata.get(position).setType2("0");
                        listdata.get(position).setType3("1");
                        notifyDataSetChanged();
                    }
                });


                if (listdata.get(position).getType1().equalsIgnoreCase("1")) {
                    holder.iv1.setImageResource(R.mipmap.bb);
                } else {
                    holder.iv1.setImageResource(R.mipmap.b);
                }

                if (listdata.get(position).getType2().equalsIgnoreCase("1")) {
                    holder.iv2.setImageResource(R.mipmap.tt);
                } else {
                    holder.iv2.setImageResource(R.mipmap.t);
                }

                if (listdata.get(position).getType3().equalsIgnoreCase("1")) {
                    holder.iv3.setImageResource(R.mipmap.aa);
                } else {
                    holder.iv3.setImageResource(R.mipmap.a);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return listdata.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvTitle;
            ImageView iv1, iv2, iv3;

            public ViewHolder(View itemView) {
                super(itemView);
                this.tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
                this.iv1 = (ImageView) itemView.findViewById(R.id.iv1);
                this.iv2 = (ImageView) itemView.findViewById(R.id.iv2);
                this.iv3 = (ImageView) itemView.findViewById(R.id.iv3);
            }
        }
    }

    public View.OnClickListener clickInLinearLayout() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Integer position = Integer.parseInt(v.getTag().toString());
            }
        };
    }

    void setSessionEndView() {
        try {
            if (RongoApp.getOriginalCropWeeksFunction() >= Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"))) {
                llSessionEnd.setVisibility(View.VISIBLE);
                llSessionStart.setVisibility(View.GONE);

                if (sharedPreferences.getString(Constants.SharedPreferences_season_feedback, "").equalsIgnoreCase("")) {
                    cardfeedback.setVisibility(View.VISIBLE);
                } else {
                    cardfeedback.setVisibility(View.GONE);
                }

                setSessionEndData();
            } else {
                llSessionStart.setVisibility(View.VISIBLE);
                llSessionEnd.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void displayData(final ProfileResultData profileResultData) {
        try {
//            Log.e("profileResultData", "" + profileResultData.getSeasonData() + " - " + RongoApp.getSelectedCrop());
            tvRongoHelpLineNumber.setText(sharedPreferences.getString(Constants.SharedPreference_RongoHelpLineNumber, "079-48061880"));
            tvRewardPoint.setText(Functions.updateRewardPoints(sharedPreferences, "0"));
            txtUsername.setText(getString(R.string.str_namskar) + " " + sharedPreferences.getString(Constants.SharedPreference_First_Name, ""));

            setDailyQuestion();
            checkCropHealthStatus();
            setInviteRewardView();
            setSessionEndView();
            fetchDataFromAssetFileForWeeklyAdvisory();
            if (profileResultData != null) {
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_LAST_SURVEY_ID, profileResultData.getLast_survey_id());
                SeasonData seasonData = profileResultData.getSeasonData();
                if (seasonData != null) {
                    season_id = seasonData.getSeason_id();
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_ID, season_id);
                    List<CropsData> cropsData = Functions.getCropData(appDatabase, sharedPreferences);
                    if (cropsData != null && cropsData.size() > 0) {
                        p_crop_id = cropsData.get(0).getP_crop_id();
                        crop_week = cropsData.get(0).getCrop_in_weeks();
                        field_id = cropsData.get(0).getField_id();
                        sowingDate = cropsData.get(0).getDate_of_sowing();
                        expectedSowingDate = cropsData.get(0).getExpected_date_of_sowing();

                        if (crop_week.equalsIgnoreCase("null") || crop_week.equalsIgnoreCase("")) {
                            crop_week = "0";
                            cropCurrent = 0;
                        } else {
                            if (Integer.parseInt(crop_week) > 0) {
                                cropCurrent = Integer.parseInt(crop_week);
                            } else {
                                cropCurrent = 0;
                                crop_week = "0";
                            }
                        }

                        if (cropsData.get(0).getIs_sowing_done().equalsIgnoreCase("yes")) {
                            relativeBuaiCard.setVisibility(View.GONE);
                            textRedDot.setVisibility(View.GONE);
                        } else {
                            if (!TextUtils.isEmpty(expectedSowingDate)) {
                                if (!expectedSowingDate.equalsIgnoreCase("0000-00-00 00:00:00")) {
                                    long ex_time = Functions.covertToMili("yyyy-MM-dd hh:mm:ss", expectedSowingDate);
                                    Date d_ = new Date();
                                    d_.setTime(System.currentTimeMillis());
                                    Date ex = new Date();
                                    ex.setTime(ex_time);

                                    int days = Functions.getDaysDifference(d_, ex);
                                    if (days <= 3) {
                                        textRedDot.setVisibility(View.VISIBLE);
                                        relativeBuaiCard.setVisibility(View.VISIBLE);
                                    } else {
                                        textRedDot.setVisibility(View.GONE);
                                        relativeBuaiCard.setVisibility(View.GONE);
                                    }
                                } else {
                                    relativeBuaiCard.setVisibility(View.VISIBLE);
                                    textRedDot.setVisibility(View.GONE);
                                }
                            } else {
                                relativeBuaiCard.setVisibility(View.VISIBLE);
                                textRedDot.setVisibility(View.GONE);
                            }
                        }

                        if (!InternetConnection.checkConnectionForFasl(homeActivity)) {
                            cropCurrent = RongoApp.getCropWeekDaysFunction();
                            crop_week = String.valueOf(cropCurrent);
                        }

                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_SowingDate, sowingDate);
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Field_ID, season_id);
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_P_CROP_ID, p_crop_id);
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_CROP_WEEK, crop_week);
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_season_end_crop_week, cropsData.get(0).getSeason_end_crop_week());
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_crop_status, cropsData.get(0).getCrop_status());
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_season_feedback, cropsData.get(0).getSeason_feedback());

                        cropWeek = cropCurrent;
//                        Log.e("@@@@@@@@@@@", "" + cropWeek + " - " + cropCurrent + " - " + cropWeekDetails.size());

                        if (cropWeek >= Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"))) {
                            cropWeek = (Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0")) - 1);
                        }

                        cropWeekDetails = new ArrayList<>();
                        List<CropWeekDetails> cropWeekDetail = new ArrayList<>();
                        if (cropsData.get(0).getCropWeekDetailsLis() != null && cropsData.get(0).getCropWeekDetailsLis().size() > 0) {
                            cropWeekDetail = cropsData.get(0).getCropWeekDetailsLis();
                            for (int i = 0; i <= cropWeek; i++) {
                                CropWeekDetails cropWeekData = new CropWeekDetails();
                                cropWeekData.setStage_id(cropWeekDetail.get(i).getStage_id());
                                cropWeekData.setCrop_week(cropWeekDetail.get(i).getCrop_week());
                                cropWeekData.setStage_name(cropWeekDetail.get(i).getStage_name());
                                cropWeekData.setDisplay_name(cropWeekDetail.get(i).getDisplay_name());
                                cropWeekData.setStage_start_week(cropWeekDetail.get(i).getStage_start_week());
                                cropWeekData.setStage_end_week(cropWeekDetail.get(i).getStage_end_week());
                                cropWeekData.setIs_new_stage(cropWeekDetail.get(i).getIs_new_stage());
                                cropWeekDetails.add(cropWeekData);
                            }
                        }
                        Log.v("@@@@@@@@@@@", "" + cropWeek + " - " + cropCurrent + " - " + cropWeekDetails.size());

                        if (cropCurrent >= Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"))) {
                            CropWeekDetails cropWeekData = new CropWeekDetails();
                            cropWeekData.setStage_id(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"));
                            cropWeekData.setCrop_week(Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0")));
                            cropWeekData.setStage_name("Session End");
                            cropWeekData.setDisplay_name(getString(R.string.str_phasal_end_txt));
                            cropWeekData.setStage_start_week(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"));
                            cropWeekData.setStage_end_week(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"));
                            cropWeekData.setIs_new_stage(1);
                            cropWeekDetails.add(cropWeekData);
                        }

                        if (cropWeekDetails != null && cropWeekDetails.size() > 0) {
                            RongoApp.getSeasonNameFunction();
                            userInteraction = false;
                            Log.v("USER_INTERACTION", "User interaction set false - " + cropCurrent + " - " + cropWeekDetails.size());
                            stageViewPager.setAdapter(new StagesAdapter(homeActivity, PhasalFragment.this, cropWeekDetails));
                            stageViewPager.setCurrentItem(cropWeekDetails.size() - 1);
                            setView(cropWeekDetails.size() - 1);
                            setShimmerGone();
                            topSticky();
                        }
                    }
                }

                // For Showing Badge
                if (homeActivity.mCircleNavigationView != null) {
                    homeActivity.mCircleNavigationView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            int salahCount = Integer.parseInt(sharedPreferences.getString(Constants.SharedPreference_Salah_Counter, "0"));
                            if (salahCount > 0) {
                                homeActivity.mCircleNavigationView.showBadgeAtIndex(2, salahCount, Color.RED);
                                homeActivity.mCircleNavigationView.shouldShowFullBadgeText(true);
                            }
                        }
                    }, 1000);
                }
            }

            setWeeklyWeatherData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void updateWeeklyCropInfo(String isSowing, String infoType, String infoText, String editTextSowingDate, String metaData) {
        String date = "0000-00-00 00:00:00";
        long dTime = Functions.covertToMili("dd-MM-yyyy", editTextSowingDate.trim());
        if (System.currentTimeMillis() > dTime) {
            metaData = getResources().getString(R.string.str_ha);
            date = Functions.converOneDateFormatToOther("dd-MM-yyyy", "yyyy-MM-dd hh:mm:ss", editTextSowingDate.trim());
        } else {
            metaData = getResources().getString(R.string.str_na);
            date = Functions.converOneDateFormatToOther("dd-MM-yyyy", "yyyy-MM-dd hh:mm:ss", editTextSowingDate.trim());
        }

        RetrofitClient.getInstance().getApi().updateWeeklyCropInfo(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(homeActivity),
                SharedPreferencesUtils.getUserId(homeActivity), sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_Field_ID, ""),
                infoType, infoText, date, metaData).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void saveSowingInfo(String isSowing, String sowingDates) {
        progressBarDialog.showProgressDialogWithTitle("9");

        if (BuvayDateDialog != null) {
            BuvayDateDialog.hide();
            BuvayDateDialog.dismiss();
            BuvayDateDialog.cancel();
        }

        String s = "0000-00-00 00:00:00", es = "0000-00-00 00:00:00";
        long dTime = Functions.covertToMili("dd-MM-yyyy", sowingDates.trim());
        if (System.currentTimeMillis() > dTime) {
            isSowing = "yes";
            s = Functions.converOneDateFormatToOther("dd-MM-yyyy", "yyyy-MM-dd hh:mm:ss", sowingDates.trim());
        } else {
            isSowing = "no";
            es = Functions.converOneDateFormatToOther("dd-MM-yyyy", "yyyy-MM-dd hh:mm:ss", sowingDates.trim());
        }

        RetrofitClient.getInstance().getApi().saveSowingInfo(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(homeActivity),
                sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                SharedPreferencesUtils.getUserId(homeActivity),
                sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                isSowing, s, es, seedType, p_crop_id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");
                    if (InternetConnection.checkConnection(homeActivity)) {
                        getProfile();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void getFertilizerInfo(String name) {
        progressBarDialog.showProgressDialogWithTitle("10");
        RetrofitClient.getInstance().getApi().getFertilizerInfo(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                Functions.getDeviceid(homeActivity), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                SharedPreferencesUtils.getUserId(homeActivity), sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, "")).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    progressBarDialog.hideProgressDialogWithTitle();
                    if (response != null && response.isSuccessful()) {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject != null && jsonObject.length() > 0) {
                            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                JSONObject data = jsonObject.optJSONObject("data");
                                if (data != null && data.length() > 0) {
                                    JSONObject jsonArray = data.optJSONObject("result");
                                    if (jsonArray != null && jsonArray.length() > 0) {
                                        SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                                Constants.SharedPreferences_FertilizerInfo, "" + jsonArray);

                                        if (!TextUtils.isEmpty(name)) {
                                            openUria(name);
                                        }
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        });
    }

    void getSurveyQuestions() {
        if (surveyCount > 1) {
            return;
        }
        RetrofitClient.getInstance().getApi().getSurveyQuestions(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(homeActivity), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), SharedPreferencesUtils.getUserId(homeActivity)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response != null && response.isSuccessful()) {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject != null && jsonObject.length() > 0) {
                            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                surveyCount++;
                                JSONObject data = jsonObject.optJSONObject("data");
                                if (data != null && data.length() > 0) {

                                    JSONObject result = data.optJSONObject("result");
                                    surveyQuestions.clear();
                                    if (result != null && result.length() > 0) {
                                        Iterator<String> keys = result.keys();
                                        while (keys.hasNext()) {
                                            String key = keys.next();
                                            JSONObject value = result.optJSONObject(key);

                                            String id = "0";
                                            id = value.optString("id");
                                            int keyValue = Integer.parseInt((id.equalsIgnoreCase("")) ? "0" : id);
                                            String question = value.optString("question");
                                            String field_type = value.optString("field_type");

                                            JSONArray jsonArray = value.optJSONArray("options");
                                            List<SurveyQuestionItemData> surveyQuestionItemData = new ArrayList<>();
                                            List<String> optionsLis = new ArrayList<>();
                                            if (jsonArray != null && jsonArray.length() > 0) {

                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    optionsLis.add(jsonArray.optString(i));
                                                }
                                            }
                                            surveyQuestionItemData.add(new SurveyQuestionItemData(0, id, question, field_type, optionsLis));
                                            surveyQuestions.add(new SurveyQuestions(0, keyValue, surveyQuestionItemData));
                                        }
                                    }

                                    appDatabase.surveyQuestionsDao().insertAll(surveyQuestions);

                                    generateOptions();
                                    SharedPreferencesUtils.saveSurveyQuestions(homeActivity, surveyQuestions, "survey");
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void generateOptions() {
//        String surveyId = "0";
//        if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_LAST_SURVEY_ID, "0"))) {
//            surveyId = sharedPreferences.getString(Constants.SharedPreferences_LAST_SURVEY_ID, "0");
//        }
//        Log.e("surveyId bbbb", "" + surveyId);
//        if (surveyId.equalsIgnoreCase("null") || surveyId == null) {
//            surveyId = "0";
//        }
//        if (appDatabase.surveyQuestionsDao().getAllQuestions().size() == 0) {
//            if (InternetConnection.checkConnection(homeActivity)) {
//                getSurveyQuestions();
//            }
//        }
//        Log.e("surveyId aaaa", "" + surveyId + " =Survey Data size= " + appDatabase.surveyQuestionsDao().getAllQuestions().size());
//        List<SurveyQuestions> surveyQuestions = appDatabase.surveyQuestionsDao().getNextQuestion(Integer.parseInt(surveyId));
//        if (!surveyQuestions.isEmpty()) {
//            try {
//                linearQuestions.setVisibility(View.VISIBLE);
//                List<SurveyQuestionItemData> surveyQuestionItemData = surveyQuestions.get(0).getSurveyQuestionItemData();
//                survey_id = surveyQuestions.get(0).getKeyValue() + "";
//                for (int j = 0; j < surveyQuestionItemData.size(); j++) {
//                    txtQuestion.setText(surveyQuestionItemData.get(j).getQuestion());
//                    field_type = surveyQuestionItemData.get(j).getField_type();
//                    List<String> list = surveyQuestionItemData.get(j).getOptions();
//                    if (!list.isEmpty()) {
//                        QuestionOptionsAdapter questionOptionsAdapter = new QuestionOptionsAdapter(list, PhasalFragment.this,
//                                surveyQuestionItemData.get(j).getField_type(), selectedOption, selectedStringsList, selectedStringsListForNames);
//                        rvOptions.setAdapter(questionOptionsAdapter);
//                    }
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                linearQuestions.setVisibility(View.GONE);
//            }
//        } else {
//            linearQuestions.setVisibility(View.GONE);
//        }
    }

    void openUria(final String type) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_dialog_prati_ekad, null);
        bottomSheetDialog.setContentView(view);

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        AppCompatTextView txtSSPTitle = view.findViewById(R.id.txtSSPTitle);
        AppCompatTextView txtUria = view.findViewById(R.id.txtUria);
        AppCompatTextView txtSSP = view.findViewById(R.id.txtSSP);
        AppCompatTextView txtPotas = view.findViewById(R.id.txtPotas);
        AppCompatTextView txtStage1 = view.findViewById(R.id.txtStage1);
        AppCompatTextView txtStage2 = view.findViewById(R.id.txtStage2);
        AppCompatTextView txtStage3 = view.findViewById(R.id.txtStage3);
        AppCompatTextView txtStage4 = view.findViewById(R.id.txtStage4);
        AppCompatTextView txtStage5 = view.findViewById(R.id.txtStage5);
        AppCompatTextView txtStage1Key = view.findViewById(R.id.txtStage1Key);
        AppCompatTextView txtStage2Key = view.findViewById(R.id.txtStage2Key);
        AppCompatTextView txtStage3Key = view.findViewById(R.id.txtStage3Key);
        AppCompatTextView txtStage4Key = view.findViewById(R.id.txtStage4Key);
        AppCompatTextView txtStage5Key = view.findViewById(R.id.txtStage5Key);
        try {
            if (type.equalsIgnoreCase("SSP")) {
                txtSSPTitle.setText(getString(R.string.str_ssp));
            } else {
                txtSSPTitle.setText(R.string.str_d_a_p);
            }

            ViewGroup viewGroup = view.findViewById(R.id.layoutRoot);
            ViewGroup viewGroup1 = view.findViewById(R.id.layoutRoot1);

            if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_FertilizerInfo, ""))) {
                JSONObject fertilizerInfo = new JSONObject(sharedPreferences.getString(Constants.SharedPreferences_FertilizerInfo, ""));
                if (fertilizerInfo != null && fertilizerInfo.length() > 0) {
                    Iterator<String> keys = fertilizerInfo.keys();
                    while (keys.hasNext()) {
                        String key = keys.next();
                        String value = fertilizerInfo.optString(key);
//                    Log.e("Jsonparsing", "key= " + key + "\n Value=" + value);
                        if (key.equalsIgnoreCase(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, "1"))) {
                            JSONObject jsonLang = new JSONObject(value);
                            if (jsonLang != null && jsonLang.length() > 0) {

                                Iterator<String> keyLang = jsonLang.keys();
                                while (keyLang.hasNext()) {
                                    String key1 = keyLang.next();
                                    String value1 = jsonLang.optString(key1);
//                                Log.e("Jsonparsing++", "key= " + key1 + "\n Value=" + value1);
                                    if (key1.equalsIgnoreCase(RongoApp.getDefaultLanguage())) {
                                        JSONObject jsonFert = new JSONObject(value1);

                                        if (type.equalsIgnoreCase("SSP")) {
                                            JSONObject jsonObject_One = jsonFert.optJSONObject("option_1");
                                            if (jsonObject_One != null && jsonObject_One.length() > 0) {
                                                txtUria.setText(jsonObject_One.optString("urea"));
                                                txtSSP.setText(jsonObject_One.optString("ssp"));
                                                txtPotas.setText(jsonObject_One.optString("mop"));
                                                JSONObject json = jsonObject_One.optJSONObject("urea_breakup");
                                                if (json != null && json.length() > 0) {

                                                    Iterator<String> ferty = json.keys();
                                                    while (ferty.hasNext()) {
                                                        String k = ferty.next();
                                                        String v = json.optString(k);
//
                                                        LinearLayout layout = new LinearLayout(homeActivity);
                                                        layout.setOrientation(LinearLayout.VERTICAL);
                                                        TextView product = new TextView(homeActivity);
                                                        product.setText(k);
                                                        product.setTextSize(14);
                                                        product.setPadding(0, 10, 0, 5);
                                                        product.setTypeface(null, Typeface.BOLD);
                                                        product.setTextColor(homeActivity.getResources().getColor(R.color.ekad));
                                                        layout.addView(product);
                                                        viewGroup.addView(layout);

                                                        LinearLayout layout1 = new LinearLayout(homeActivity);
                                                        layout1.setOrientation(LinearLayout.VERTICAL);
                                                        TextView price = new TextView(homeActivity);
                                                        price.setText(v);
                                                        price.setTextSize(14);
                                                        price.setGravity(Gravity.CENTER | Gravity.RIGHT);
                                                        price.setPadding(0, 10, 0, 5);
                                                        price.setTypeface(null, Typeface.BOLD);
                                                        price.setTextColor(homeActivity.getResources().getColor(R.color.ekad));
                                                        layout1.addView(price);
                                                        viewGroup1.addView(layout1);
                                                    }

//                                                txtStage1.setText(json.optString(getString(R.string.str_july_time)));
//                                                txtStage2.setText(json.optString(getString(R.string.str_four_leaf)));
//                                                txtStage3.setText(json.optString(getString(R.string.str_eight_leaf)));
//                                                txtStage4.setText(json.optString(getString(R.string.str_flower)));
//                                                txtStage5.setText(json.optString(getString(R.string.str_make_food)));
                                                }
                                            }
                                        } else {
                                            JSONObject jsonOption2 = jsonFert.optJSONObject("option_2");
                                            if (jsonOption2 != null && jsonOption2.length() > 0) {
                                                txtUria.setText(jsonOption2.optString("urea"));
                                                txtSSP.setText(jsonOption2.optString("dap"));
                                                txtPotas.setText(jsonOption2.optString("mop"));
                                                JSONObject json = jsonOption2.optJSONObject("urea_breakup");
                                                if (json != null && json.length() > 0) {

                                                    Iterator<String> ferty = json.keys();
                                                    while (ferty.hasNext()) {
                                                        String k = ferty.next();
                                                        String v = json.optString(k);
//
                                                        LinearLayout layout = new LinearLayout(homeActivity);
                                                        layout.setOrientation(LinearLayout.VERTICAL);
                                                        TextView product = new TextView(homeActivity);
                                                        product.setText(k);
                                                        product.setTextSize(14);
                                                        product.setPadding(0, 10, 0, 5);
                                                        product.setTypeface(null, Typeface.BOLD);
                                                        product.setTextColor(homeActivity.getResources().getColor(R.color.ekad));
                                                        layout.addView(product);
                                                        viewGroup.addView(layout);

                                                        LinearLayout layout1 = new LinearLayout(homeActivity);
                                                        layout1.setOrientation(LinearLayout.VERTICAL);
                                                        TextView price = new TextView(homeActivity);
                                                        price.setText(v);
                                                        price.setTextSize(14);
                                                        price.setGravity(Gravity.CENTER | Gravity.RIGHT);
                                                        price.setPadding(0, 10, 0, 5);
                                                        price.setTypeface(null, Typeface.BOLD);
                                                        price.setTextColor(homeActivity.getResources().getColor(R.color.ekad));
                                                        layout1.addView(price);
                                                        viewGroup1.addView(layout1);
                                                    }

//                                                txtStage1.setText(json.optString(getString(R.string.str_july_time)));
//                                                txtStage2.setText(json.optString(getString(R.string.str_four_leaf)));
//                                                txtStage3.setText(json.optString(getString(R.string.str_eight_leaf)));
//                                                txtStage4.setText(json.optString(getString(R.string.str_flower)));
//                                                txtStage5.setText(json.optString(getString(R.string.str_make_food)));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    getFertilizerInfo(type);
                }
            } else {
                getFertilizerInfo(type);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    List<WeeklyAdvisoryData> weeklyAdvisoryData() {
        List<WeeklyAdvisoryData> weeklyAdvisoryData = new ArrayList<WeeklyAdvisoryData>();
        String weeklyAdvisory = "";
        try {
            weeklyAdvisory = sharedPreferences.getString(Constants.SharedPreferences_Weekly_Advisory, null);
            if (TextUtils.isEmpty(weeklyAdvisory)) {
                weeklyAdvisory = Functions.getAssetFileForWeeklyAdvisory(homeActivity, sharedPreferences);
            }
            if (!TextUtils.isEmpty(weeklyAdvisory)) {
                JSONObject jsonObject = new JSONObject(weeklyAdvisory);
                if (jsonObject != null && jsonObject.length() > 0) {
                    llSavdhani.setVisibility(View.VISIBLE);
                    JSONObject keyword_advisory = jsonObject.optJSONObject("keyword_advisory");
                    String keyword_js = "";
                    if (keyword_advisory != null && keyword_advisory.length() > 0) {
                        keyword_js = keyword_advisory.toString();
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_JSON_KEYWORD, keyword_js);

                        Intent i = new Intent(homeActivity, PreloadImagesServices.class);
                        PreloadImagesServices.enqueueWork(homeActivity, i);
                    }

                    JSONObject weekly_advisory = jsonObject.optJSONObject("weekly_advisory");
                    if (weekly_advisory != null && weekly_advisory.length() > 0) {
                        String stage_name = weekly_advisory.optString("stage");
                        image_path = weekly_advisory.optString("image_path");

                        JSONArray jsonArray = weekly_advisory.optJSONArray("general_advice");
                        generalAdvisory.clear();
                        String g_str = "";
                        if (jsonArray != null && jsonArray.length() > 0) {
                            g_str = jsonArray.toString();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                if (!TextUtils.isEmpty(jsonArray.getString(i))) {
                                    generalAdvisory.add(jsonArray.getString(i));
                                }
                            }
                        }

                        JSONArray advisory_image = weekly_advisory.optJSONArray("advisory_image");
                        advisoryImages.clear();
                        String ad_img = "";
                        if (advisory_image != null && advisory_image.length() > 0) {
                            ad_img = advisory_image.toString();
                            for (int i = 0; i < advisory_image.length(); i++) {
                                advisoryImages.add(advisory_image.optString(i));
                            }
                        }

                        if (generalAdvisory != null && generalAdvisory.size() > 0) {
                            llSavdhani.setVisibility(View.VISIBLE);
                            relativeArrow.setVisibility(View.VISIBLE);
                            rlSamanySalah.setVisibility(View.GONE);
                            if (generalAdvisory.size() == 1) {
                                imgNext.setVisibility(View.GONE);
                                imgPrevios.setVisibility(View.GONE);
                            } else {
                                imgNext.setVisibility(View.VISIBLE);
                                imgPrevios.setVisibility(View.VISIBLE);
                            }
                            bindData(generalAdvisory, advisoryImages);
                        } else {
                            llSavdhani.setVisibility(View.GONE);
                            rlSamanySalah.setVisibility(View.GONE);
                        }

                        chemical_info = weekly_advisory.optJSONArray("chemical_info");

                        JSONArray chemical = weekly_advisory.optJSONArray("chemical");
                        String checm = "", content = "";
                        chemicalNeeds = new ArrayList<>();
                        if (chemical != null && chemical.length() > 0) {
                            checm = chemical.toString();
                            for (int i = 0; i < chemical.length(); i++) {
                                if (!TextUtils.isEmpty(chemical.getString(i))) {
                                    chemicalNeeds.add(chemical.getString(i));
                                    content += Functions.fromHtml(chemical.getString(i));
                                }
                            }
                        }

                        JSONArray disease = weekly_advisory.optJSONArray("disease");
                        String dis = "";
                        List<DiasesData> diasesData = new ArrayList<>();
                        if (disease != null && disease.length() > 0) {
                            dis = disease.toString();
                            for (int i = 0; i < disease.length(); i++) {
                                JSONObject jd = disease.optJSONObject(i);
                                String name = jd.optString("name");
                                String keyword_id = jd.optString("keyword_id");

                                diasesData.add(new DiasesData(name, keyword_id));
                            }
                        }

                        JSONArray pest = weekly_advisory.optJSONArray("pest");
                        List<PestData> pestData = new ArrayList<>();
                        String pst = "";
                        if (pest != null && pest.length() > 0) {
                            pst = pest.toString();
                            for (int i = 0; i < pest.length(); i++) {
                                JSONObject jd = pest.optJSONObject(i);
                                String name = jd.optString("name");
                                String keyword_id = jd.optString("keyword_id");

                                pestData.add(new PestData(name, keyword_id));
                            }
                        }

                        JSONArray nutrition = weekly_advisory.optJSONArray("nutrition");
                        List<NutritionData> nutritionData = new ArrayList<>();
                        String nut = "";
                        if (nutrition != null && nutrition.length() > 0) {
                            nut = nutrition.toString();
                            for (int i = 0; i < nutrition.length(); i++) {
                                JSONObject jd = nutrition.optJSONObject(i);
                                String name = jd.optString("name");
                                String keyword_id = jd.optString("keyword_id");
                                nutritionData.add(new NutritionData(name, keyword_id));
                            }
                        }

                        weeklyAdvisoryData.add(new WeeklyAdvisoryData(0, RongoApp.getCropWeeksFunction(), RongoApp.getSelectedCrop(),
                                RongoApp.getSelectedPCrop(), stage_name, image_path, g_str, ad_img, checm, dis, pst, nut, keyword_js));
//                    appDatabase.weeklyAdvisoryDao().insertAll(weeklyAdvisoryData);
                    }
                }
            } else {
                getWeeklyAdvisory();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        Log.e("@@@3", "" + weeklyAdvisory);
        return weeklyAdvisoryData;
    }

    void openThankYou(String str, String points) {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_thank_you_dialog, null);
        bottomSheetDialog.setContentView(view);

        AppCompatTextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));

        AppCompatTextView tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText(points);

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        AppCompatTextView txtEnter = view.findViewById(R.id.txtEnter);
        txtEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                bottomSheetDialog.dismiss();
                Functions.performShare(homeActivity, appDatabase);
//                InviteFunction();
            }
        });

        LinearLayout llRewardView = view.findViewById(R.id.llRewardView);

        AppCompatTextView txtTitle = view.findViewById(R.id.txtTitle);
        if (str.equalsIgnoreCase("Y")) {
            txtTitle.setText(getString(R.string.str_janakari_dene_ke_liye_thank));
            llRewardView.setVisibility(View.VISIBLE);
        } else {
            txtTitle.setText(getString(R.string.str_share_with_issues));
            llRewardView.setVisibility(View.GONE);
        }

        AppCompatTextView tvShareWhatapp = view.findViewById(R.id.tvShareWhatapp);
        tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "WhatsApp");
                    Functions.shareWhatAppIntent(homeActivity);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(homeActivity, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        AppCompatTextView tvShareCopy = view.findViewById(R.id.tvShareCopy);
        tvShareCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "SMS");

                    Functions.shareSMSIntent(homeActivity, RongoApp.getReferralText(homeActivity));

//                    Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
//                    smsIntent.setType("vnd.android-dir/mms-sms");
//                    smsIntent.putExtra("address", "");
//                    smsIntent.putExtra("sms_body", "" + HtmlCompat.fromHtml(sharedPreferences.getString(Constants.SharedPreference_Referral_Link, ""), 0));
//                    smsIntent.setFlags(android.content.Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(smsIntent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AppCompatTextView tvSharefacebook = view.findViewById(R.id.tvSharefacebook);
        tvSharefacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.setUserScreenTrackEvent(appDatabase, "Invite", "Facebook");
                Functions.shareFacebookIntent(homeActivity);
            }
        });

        AppCompatTextView tvShareMore = view.findViewById(R.id.tvShareMore);
        tvShareMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.performShare(homeActivity, appDatabase);
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    void openProblem() {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_dialog_problem, null);
        bottomSheetDialog.setContentView(view);

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        AppCompatTextView tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText(RewardPoints.getCropHealthRewardPoint(sharedPreferences));

        TextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getPostQuestionRewardPoint(sharedPreferences));

        RecyclerView rcvProblem = view.findViewById(R.id.rcvProblem);
        rcvProblem.addItemDecoration(new GridSpacingItemDecoration(3, (int) Functions.convertDpToPixel(5, homeActivity), false));
        rcvProblem.setNestedScrollingEnabled(false);
        rcvProblem.setLayoutManager(new GridLayoutManager(homeActivity, 3));
        List<ProblemRelatedData> options = SharedPreferencesUtils.getProblemRelated(homeActivity, Constants.SharedPreferences_OPTIONS);
        if (options != null && options.size() > 0) {
            rcvProblem.setAdapter(new CropProblemPostQuesAdapter(homeActivity, options, bottomSheetDialog));
        }

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    public void selecteSingle(String single) {
        rvOptions.setVisibility(View.GONE);
        this.selectedOption = single;
        Log.v("selectedOption", selectedOption);
        editOptions.setText(selectedOption);
    }

    void surveyResponse() {
        progressBarDialog.showProgressDialogWithTitle("11");
        linearGo.setVisibility(View.GONE);

        String response = "";
        if (field_type.equalsIgnoreCase("select")) {
            response = selectedOption;

        } else if (field_type.equalsIgnoreCase("select_multiple")) {
            response = TextUtils.join(",", selectedStringsListForNames);
        }
        Log.v("surveyResponse: ", "" + response);

        RetrofitClient.getInstance().getApi().surveyResponse(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(homeActivity),
                sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                SharedPreferencesUtils.getUserId(homeActivity), survey_id, response).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {

                    linearGo.setVisibility(View.VISIBLE);
                    progressBarDialog.hideProgressDialogWithTitle();

                    editOptions.getText().clear();
                    surveyQuestions.clear();
                    selectedStringsList.clear();
                    selectedStringsListForNames.clear();
                    selectedOption = "";

//                    tvRewardPoint.setText(Functions.updateRewardPoints(sharedPreferences, getSurveyRewardPoint()));
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_LAST_SURVEY_ID, survey_id + "");

                    //getSurveyQuestions();
//                    Save in local profile dataFromDabase();
                    ProfileResultData profileResultData = Functions.getData();
                    if (profileResultData != null) {
                        profileResultData.setLast_survey_id(survey_id);
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                Constants.SharedPreference_profileResultData, (new Gson()).toJson(profileResultData));
                    }

                    List<SurveyQuestions> surveyQuestions = appDatabase.surveyQuestionsDao().getNextQuestion(Integer.parseInt(survey_id));
                    if (surveyQuestions.isEmpty()) {
                        openThankYou("Y", RewardPoints.getSurveyRewardPoint(sharedPreferences));
                        tvRewardPoint.setText(Functions.updateRewardPoints(sharedPreferences, RewardPoints.getSurveyRewardPoint(sharedPreferences)));
                    }

                    generateOptions();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.v("MYERROR", t.getMessage());
            }
        });
    }

    public void lastWeek(int pos) {
        cropCurrent = cropWeekDetails.get(pos).getCrop_week();
        if (cropCurrent > 0) {
            cropCurrent--;
            stageViewPager.setCurrentItem(cropCurrent);
        }
    }

    void askStoragePermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            Dexter.withContext(homeActivity).withPermissions(Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if (report.areAllPermissionsGranted()) {
                                // loadImage(imageURL, id);
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    }).check();
        } else {
            // loadImage(imageURL, id);
        }
    }

    void parseWeatherData(JSONObject data) {
        if (data != null && data.length() > 0) {
            JSONObject result = data.optJSONObject("result");
            if (result != null && result.length() > 0) {
                List<String> weather_alert_list = new ArrayList<>();
                JSONArray weather_alert = result.optJSONArray("weather_alert");
                if (weather_alert != null && weather_alert.length() > 0) {
                    for (int i = 0; i < weather_alert.length(); i++) {
                        weather_alert_list.add(weather_alert.optString(i));
//                        weather_alert_list.add(weather_alert.optString(0));
                        SharedPreferencesUtils.saveArrayList(homeActivity, weather_alert_list, Constants.SharedPreferences_WEATHER_ALERT);
                    }
                }

//                llWeatherView.setVisibility(View.VISIBLE);
                relJalBharav.setVisibility(View.VISIBLE);
//                txtWeatherAlert.setText(weather_alert.optString(0));

                JSONObject weather_advisory = result.optJSONObject("weather_advisory");
                if (weather_advisory != null && weather_advisory.length() > 0) {
                    if (weather_alert_list.size() > 0) {
                        Iterator<String> keys = weather_advisory.keys();
                        while (keys.hasNext()) {
                            String key = keys.next();
                            JSONObject value = weather_advisory.optJSONObject(key);
                            for (int i = 0; i < weather_alert_list.size(); i++) {
                                if (weather_alert_list.get(i).contains(key)) {
                                    txtWeatherAlert.setText(value.optString("display_name"));
                                    break;
                                }
                            }
                        }
                    } else {
                        relJalBharav.setVisibility(View.GONE);
                        llWeatherView.setVisibility(View.GONE);
                    }
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_WEATHER_ADVISORY, weather_advisory.toString());
                }

            }
        }
    }

    void openSuccessForHealthy(String msg) {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_success_for_healthy, null);
        bottomSheetDialog.setContentView(view);

        AppCompatTextView txtOfflineSMS = view.findViewById(R.id.txtOfflineSMS);
        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
            txtOfflineSMS.setVisibility(View.GONE);
        } else {
            txtOfflineSMS.setVisibility(View.VISIBLE);
        }

        AppCompatTextView tvApne = view.findViewById(R.id.tvApne);
        AppCompatTextView tvPrapt = view.findViewById(R.id.tvPrapt);
        if (msg.equalsIgnoreCase("1")) {
            tvApne.setText(homeActivity.getResources().getString(R.string.str_post_ques_sub));
            tvPrapt.setText(homeActivity.getResources().getString(R.string.str_post_ques_points));
        } else {
            tvApne.setText(homeActivity.getResources().getString(R.string.str_aapne));
            tvPrapt.setText(homeActivity.getResources().getString(R.string.str_earned_points));
        }

        AppCompatTextView tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText(RewardPoints.getCropHealthRewardPoint(sharedPreferences));

        AppCompatTextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getCropHealthRewardPoint(sharedPreferences));

        AppCompatTextView tvShareWhatapp = view.findViewById(R.id.tvShareWhatapp);
        tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "WhatsApp");
                    Functions.shareWhatAppIntent(homeActivity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AppCompatTextView tvShareCopy = view.findViewById(R.id.tvShareCopy);
        tvShareCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "SMS");
                    Functions.shareSMSIntent(homeActivity, RongoApp.getReferralText(homeActivity));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AppCompatTextView tvSharefacebook = view.findViewById(R.id.tvSharefacebook);
        tvSharefacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.setUserScreenTrackEvent(appDatabase, "Invite", "Facebook");
                Functions.shareFacebookIntent(homeActivity);
            }
        });

        AppCompatTextView tvShareMore = view.findViewById(R.id.tvShareMore);
        tvShareMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.performShare(homeActivity, appDatabase);
            }
        });

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    void welcomeDialog(int position) {
        Dialog howEarnDialog = new Dialog(homeActivity, R.style.NewDialog);
        howEarnDialog.setContentView(R.layout.welcome_info_dialog);
        howEarnDialog.setCanceledOnTouchOutside(false);
        howEarnDialog.setCancelable(false);
        howEarnDialog.dismiss();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = howEarnDialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        ImageView imgClose = howEarnDialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                howEarnDialog.dismiss();

                Intent intent = new Intent();
                intent.setAction(Constants.BROADCAST_RECEIVER.UPDATE_TOOLTIP_VIEW);
                RongoApp.getInstance().sendBroadcast(intent);
            }
        });

        RecyclerView rcvWelcome = (RecyclerView) howEarnDialog.findViewById(R.id.rcvWelcome);
        LinearLayoutManager layoutManager = new LinearLayoutManager(homeActivity, LinearLayoutManager.HORIZONTAL, false);
        rcvWelcome.setLayoutManager(layoutManager);
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("1");
        arrayList.add("2");
        WelcomeAdapter welcomeAdapter = new WelcomeAdapter(homeActivity, arrayList, rcvWelcome, howEarnDialog, PhasalFragment.this);
        rcvWelcome.setAdapter(welcomeAdapter);
        rcvWelcome.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                try {
                    recyclerView.post(new Runnable() {
                        public void run() {
                            welcomeAdapter.notifyDataSetChanged();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        howEarnDialog.show();
    }

    void welcomeOfflineDialog(int position) {
        Dialog howEarnDialog = new Dialog(homeActivity, R.style.NewDialog);
        howEarnDialog.setContentView(R.layout.welcome_info_dialog);
        howEarnDialog.setCanceledOnTouchOutside(false);
        howEarnDialog.setCancelable(false);
        howEarnDialog.dismiss();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = howEarnDialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        ImageView imgClose = howEarnDialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                howEarnDialog.dismiss();

                Intent intent = new Intent();
                intent.setAction(Constants.BROADCAST_RECEIVER.UPDATE_TOOLTIP_VIEW);
                RongoApp.getInstance().sendBroadcast(intent);
            }
        });

        RecyclerView rcvWelcome = (RecyclerView) howEarnDialog.findViewById(R.id.rcvWelcome);
        LinearLayoutManager layoutManager = new LinearLayoutManager(homeActivity, LinearLayoutManager.HORIZONTAL, false);
        rcvWelcome.setLayoutManager(layoutManager);
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("1");
        arrayList.add("2");
        arrayList.add("3");

        WelcomeOfflineAdapter welcomeAdapter = new WelcomeOfflineAdapter(homeActivity, arrayList, rcvWelcome, howEarnDialog, PhasalFragment.this);
        rcvWelcome.setAdapter(welcomeAdapter);
        rcvWelcome.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                try {
                    recyclerView.post(new Runnable() {
                        public void run() {
                            welcomeAdapter.notifyDataSetChanged();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        howEarnDialog.show();
    }


    void setInviteRewardView() {
        try {
            if (llShareReward != null) {
                llShareReward.removeAllViews();
                llShareReward.invalidate();
            }
            TextView view1 = new TextView(homeActivity);
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params1.setMargins(2, 0, 2, 4);
            view1.setLayoutParams(params1);
            view1.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            view1.setText(getResources().getText(R.string.str_send_invite_your_brother) + " $ " + RewardPoints.getInviteRewardPoint(sharedPreferences) + " " + homeActivity.getResources().getString(R.string.str_points));

            String originalText1 = (String) view1.getText();
            int startPosition1 = getResources().getText(R.string.str_send_invite_your_brother).length();

            SpannableString spannableStr1 = new SpannableString(originalText1);
            UnderlineSpan underlineSpan1 = new UnderlineSpan();
            spannableStr1.setSpan(underlineSpan1, startPosition1, startPosition1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            ForegroundColorSpan backgroundColorSpan1 = new ForegroundColorSpan(Color.rgb(255, 69, 0));
            spannableStr1.setSpan(backgroundColorSpan1, startPosition1, startPosition1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            spannableStr1.setSpan(new RelativeSizeSpan(0.6f), startPosition1, startPosition1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            int $index1 = originalText1.indexOf("$");
            spannableStr1.setSpan(new ImageSpan(homeActivity, R.mipmap.small_reward), $index1, $index1 + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            view1.setText(spannableStr1);

//            llShareReward.addView(view1);

            SpannableStringBuilder builder = new SpannableStringBuilder();
            SpannableString str1 = new SpannableString(getResources().getText(R.string.str_send_invite_code));
//            str1.setSpan(new ForegroundColorSpan(Color.RED), 0, str1.length(), 0);
            builder.append(str1);

            SpannableString str2 = new SpannableString("  " + sharedPreferences.getString(Constants.SharedPreference_Referral_code, "").toString());
            str2.setSpan(new StyleSpan(Typeface.BOLD), 0, str2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            str2.setSpan(new ForegroundColorSpan(Color.rgb(255, 69, 0)), 0, str2.length(), 0);
            builder.append(str2);

//            tvShareCode.setText(builder, TextView.BufferType.SPANNABLE);
            tvShareCode.setText(getResources().getText(R.string.str_invite_new_text));
            tvPointsInvite.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));

            tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Functions.setUserScreenTrackEvent(appDatabase, "Invite", "WhatsApp");
                        Functions.shareWhatAppIntent(homeActivity);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(homeActivity, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            tvShareCopy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Functions.setUserScreenTrackEvent(appDatabase, "Invite", "SMS");
                        Functions.shareSMSIntent(homeActivity, RongoApp.getReferralText(homeActivity));

//                        Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
//                        smsIntent.setType("vnd.android-dir/mms-sms");
//                        smsIntent.putExtra("address", "");
//                        smsIntent.putExtra("sms_body", "" + HtmlCompat.fromHtml(sharedPreferences.getString(Constants.SharedPreference_Referral_Link, ""), 0));
//                        smsIntent.setFlags(android.content.Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(smsIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            tvSharefacebook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "Facebook");
                    Functions.shareFacebookIntent(homeActivity);
                }
            });

            tvShareMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Functions.performShare(homeActivity, appDatabase);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setView(int position) {
        if (sharedPreferences.getString(Constants.SharedPreference_Reward_Info_Dialog, "0").equalsIgnoreCase("0")) {
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Reward_Info_Dialog, "1");
            if (RongoApp.howToEarn) {
                RongoApp.howToEarn = false;
                if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                    welcomeDialog(position);
                } else {
                    welcomeOfflineDialog(position);
                }
            }
        } else {
            setViewData(position);
        }

        if (sharedPreferences.getString(Constants.SharedPreference_App_Update, "0").equalsIgnoreCase("1")) {
            if (sharedPreferences.getString(Constants.SharedPreference_App_Version, "0").equalsIgnoreCase("1")) {
                if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                    appUpdateDialog();
                }
            }
        }
    }

    void setViewData(int position) {
        try {
            Functions.setUserScreenTrackEvent(appDatabase, "Phasal", "crop_week: " + position);
            if (cropWeekDetails != null && cropWeekDetails.size() > 0) {
                cropCurrent = cropWeekDetails.get(position).getCrop_week();
                s_name = cropWeekDetails.get(position).getDisplay_name();
                int c_week_check = cropWeekDetails.get(position).getCrop_week();
                if (c_week_check == Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0"))) {
                    isFirstTime = false;
                }
                int is_new_stage = cropWeekDetails.get(position).getIs_new_stage();
                if (is_new_stage == 1) {
//                    llFertilizer.setVisibility(View.VISIBLE);

                    final int c_week = cropWeekDetails.get(position).getCrop_week();
                    if (c_week == Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0"))) {

                        String s_name = cropWeekDetails.get(position).getDisplay_name() + " " + homeActivity.getResources().getString(R.string.str_charan);
                        String myString = homeActivity.getResources().getString(R.string.str_ab_apki_phasal);
                        String lastString = homeActivity.getResources().getString(R.string.str_me_he);
                        String sm = myString + " " + s_name + " " + lastString;
                        Log.v("SPECIFICATION", s_name + " ; " + sm);

                        Dialog bottomSheetDialog = new Dialog(homeActivity, R.style.NewDialog);
                        bottomSheetDialog.setContentView(R.layout.pop_up_new_advosiry);
                        bottomSheetDialog.setCanceledOnTouchOutside(true);

                        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        Window window = bottomSheetDialog.getWindow();
                        lp.copyFrom(window.getAttributes());
                        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
                        window.setAttributes(lp);

                        AppCompatTextView txtDarjKare = bottomSheetDialog.findViewById(R.id.txtDarjKare);
                        AppCompatTextView txtTitle = bottomSheetDialog.findViewById(R.id.txtTitle);

                        SpannableString ss = new SpannableString(sm);
                        Typeface font = ResourcesCompat.getFont(homeActivity, R.font.roboto_bold);

                        int l = sm.indexOf(s_name) + s_name.trim().length();
                        ss.setSpan(new CustomTypefaceSpan("", font), sm.indexOf(s_name), l, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                        ss.setSpan(new ForegroundColorSpan(ContextCompat.getColor(homeActivity, R.color.colorPrimary)),
                                sm.indexOf(s_name), l, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

                        txtTitle.setText(ss, TextView.BufferType.SPANNABLE);
                        txtTitle.setMovementMethod(LinkMovementMethod.getInstance());
                        txtTitle.setHighlightColor(Color.TRANSPARENT);

                        txtDarjKare.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                bottomSheetDialog.dismiss();
                            }
                        });

                        integers = SharedPreferencesUtils.getList(homeActivity, "IS_NEW");
                        if (integers != null && integers.size() > 0) {
                            if (!integers.contains(position + "")) {
                                integers.add(position + "");
//                                bottomSheetDialog.show();
                            }
                        } else {
                            integers = new ArrayList<String>();
                            integers.add(position + "");
//                            bottomSheetDialog.show();
                        }

                        SharedPreferencesUtils.saveList(homeActivity, integers, "IS_NEW");
                    }
                } else {
//                    llFertilizer.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void getContentData() {
        RetrofitClient.getInstance().getApi().getContentData(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(homeActivity),
                SharedPreferencesUtils.getUserId(homeActivity)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String str = response.body().string();
                    if (!TextUtils.isEmpty(str)) {
                        JSONObject jsonObject = new JSONObject(str);
                        if (jsonObject != null && jsonObject.length() > 0) {
                            JSONObject data = jsonObject.optJSONObject("data");
                            if (data != null && data.length() > 0) {
                                JSONObject result = data.optJSONObject("result");
//                                Log.e("getContentData", "" + result);
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_internet_radio, "" + result.optString("internet_radio"));
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Referral_Image, "" + result.optString("referral_image"));
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Community_Banner, "" + result.optString("community_banner"));
                                setCommunityData();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void setCommunityData() {
        try {
            tvKisanCount = view.findViewById(R.id.tvKisanCount);
            tvPrashanCount = view.findViewById(R.id.tvPrashanCount);
            tvAnswerCount = view.findViewById(R.id.tvAnswerCount);
            tvCommentCount = view.findViewById(R.id.tvCommentCount);
            txtQuestionRelated = view.findViewById(R.id.txtQuestionRelated);
            txtDate = view.findViewById(R.id.txtDate);
            tvExpertSujav = view.findViewById(R.id.tvExpertSujav);
            ivCommunity = view.findViewById(R.id.ivCommunity);

            if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreference_Community_Banner, ""))) {
                JSONObject communityData = new JSONObject(sharedPreferences.getString(Constants.SharedPreference_Community_Banner, ""));
                if (!TextUtils.isEmpty(communityData.toString())) {
                    cardCommunityView.setVisibility(View.VISIBLE);

                    tvKisanCount.setText(HtmlCompat.fromHtml("<B>" + communityData.optString("user") + "</B> " + homeActivity.getResources().getString(R.string.str_kisan_mitra), HtmlCompat.FROM_HTML_MODE_LEGACY));
                    tvPrashanCount.setText(HtmlCompat.fromHtml("<B>" + communityData.optString("post") + "</B> " + homeActivity.getResources().getString(R.string.str_prashan), HtmlCompat.FROM_HTML_MODE_LEGACY));
                    tvAnswerCount.setText(HtmlCompat.fromHtml("<B>" + communityData.optString("advice") + "</B> " + homeActivity.getResources().getString(R.string.str_salah_c), HtmlCompat.FROM_HTML_MODE_LEGACY));
                    tvCommentCount.setText(communityData.optString("recent_post_comments") + " " + homeActivity.getResources().getString(R.string.str_comments_txt));

                    if (communityData.optString("recent_post_comments").equalsIgnoreCase("0")) {
                        tvCommentCount.setVisibility(View.GONE);
                    } else {
                        tvCommentCount.setVisibility(View.VISIBLE);
                    }

                    JSONObject community = communityData.optJSONObject("recent_post");
                    post_id = community.optString("post_id");
                    String prId = community.optString("problem_related"), type = homeActivity.getResources().getString(R.string.str_other);
                    List<ProblemRelatedData> problemRelatedData = SharedPreferencesUtils.getProblemRelated(homeActivity,
                            Constants.SharedPreferences_OPTIONS);
                    if (problemRelatedData != null && problemRelatedData.size() > 0) {
                        for (int i = 0; i < problemRelatedData.size(); i++) {
                            if (prId.equalsIgnoreCase(problemRelatedData.get(i).getKey())) {
                                if (prId.equalsIgnoreCase("5") || prId.equalsIgnoreCase("0")) {
                                    type = problemRelatedData.get(i).getValue();
                                } else {
                                    type = problemRelatedData.get(i).getValue() + " " + homeActivity.getResources().getString(R.string.str_samndhi);
                                }
                                break;
                            }
                        }
                    }
                    txtQuestionRelated.setText(HtmlCompat.fromHtml("<B>" + type + "</B>: " + community.optString("description"), HtmlCompat.FROM_HTML_MODE_LEGACY));

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(sdf.parse(community.optString("created_on")));
                    txtDate.setText(Functions.setTimeAgoMyTopic(homeActivity, cal.getTimeInMillis()));

                    JSONArray imageArray = new JSONArray(community.optString("picture_key"));
                    if (imageArray != null && imageArray.length() > 0) {
                        Functions.displayImage(Constants.IMAGE_PATH_LINK + imageArray.opt(0).toString(), ivCommunity);
                    }

                    if (community.optString("is_expert_comment").equalsIgnoreCase("false")) {
                        tvExpertSujav.setVisibility(View.GONE);
                    } else {
                        tvExpertSujav.setVisibility(View.VISIBLE);
                    }
                } else {
                    cardCommunityView.setVisibility(View.GONE);
                }
            } else {
                cardCommunityView.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
