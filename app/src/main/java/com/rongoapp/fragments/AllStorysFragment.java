package com.rongoapp.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.card.MaterialCardView;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedImageView;
import com.rongoapp.R;
import com.rongoapp.activities.HomeActivity;
import com.rongoapp.activities.PostQuestionActivity;
import com.rongoapp.activities.QuestionSampleActivity;
import com.rongoapp.activities.QuizActivity;
import com.rongoapp.activities.RewardActivity;
import com.rongoapp.activities.SavdhaniDetailsActivity;
import com.rongoapp.audioPlayerTTS.Speakerbox;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.FetchJsonFromAssetsFile;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.GlideApp;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.data.StoryData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.storyProgressView.StoriesProgressView;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.Log;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllStorysFragment extends DialogFragment implements StoriesProgressView.StoriesListener {

    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    View view;
    int storyId = 0;
    String storyType = "1", cardType = "1", image_path = "", action_param = "";

    ArrayList<Bitmap> imaArrayList = new ArrayList<>();
    int cardDuration[] = new int[]{};
    List<StoryData.Story> storyDataList;
    List<StoryData.Card> storyCardDataList;

    LinearLayout llStoryView, llEditStoryView, llUpSlideView, llBottomStoryView;
    RelativeLayout rlMainView;
    StoriesProgressView storiesProgressView;
    Speakerbox speakerbox;
    RoundedImageView ivEditStoryView, ivClose, ivStoryView, ivSend;
    RecyclerView rcvStoryView;
    TextView ivEditStoryTitle, ivEditStoryDecs, ivEditStoryMeta, tvTitleSend, tvQuestions, tvOne, tvTwo, tvThree, tvFour;
    MaterialCardView cardRedirect, cardQuestion;

    int counter = 0;
    long pressTime = 0L;
    long limit = 500L;
    View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    pressTime = System.currentTimeMillis();
                    storiesProgressView.pause();
                    return false;
                case MotionEvent.ACTION_UP:
                    long now = System.currentTimeMillis();
                    storiesProgressView.resume();
                    return limit < now - pressTime;
            }
            return false;
        }
    };

    @Override
    public int getTheme() {
        return R.style.FullScreenDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.FullScreenDialog);

        sharedPreferences = getActivity().getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(getActivity());

        speakerbox = new Speakerbox(RongoApp.getInstance());
        speakerbox.setActivity(getActivity());
        speakerbox.setLanguage(Functions.getLocaleLanguage());

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public static AllStorysFragment newInstance() {
        AllStorysFragment fragment = new AllStorysFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_all_story_dialog, container, false);
        setUserStoryData();
        return view;
    }

    void setStoryView(List<StoryData.Story> storyData) {
        try {
            llBottomStoryView = (LinearLayout) view.findViewById(R.id.llBottomStoryView);
            llBottomStoryView.setVisibility(View.GONE);

            rcvStoryView = view.findViewById(R.id.rcvStoryView);

            storyDataList = new ArrayList<>();
            if (storyData.size() > 0) {
                storyDataList.addAll(storyData);
                storyData.get(0).setStorySelected("1");
                setStoryCardView(storyData.get(0).getCards());
                cardType = storyData.get(0).getStoryType();
                storyId = Integer.parseInt(storyData.get(0).getStoryId());
                storyType = storyData.get(0).getStoryType();
                action_param = storyData.get(0).getActionParam();

                rcvStoryView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
                rcvStoryView.setNestedScrollingEnabled(false);
                rcvStoryView.setHasFixedSize(true);
                rcvStoryView.setAdapter(new StoryAdapter(storyData));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int[] add_element(int n, int myarray[], int ele) {
        int i;
        int newArray[] = new int[n + 1];
        //copy original array into new array
        for (i = 0; i < n; i++)
            newArray[i] = myarray[i];

        //add element to the new array
//        newArray[n] = ele;
        newArray[n] = (int) TimeUnit.SECONDS.toMillis(ele);

        return newArray;
    }

    void setStoryCardView(List<StoryData.Card> storyCardData) {
        try {
            llBottomStoryView.setVisibility(View.GONE);

            storyCardDataList = new ArrayList<>();
            storyCardDataList.addAll(storyCardData);
            counter = 0;

            imaArrayList = new ArrayList<>();
            cardDuration = new int[]{};
            for (int i = 0; i < storyCardDataList.size(); i++) {
                String image = image_path + storyCardDataList.get(i).getCardMediaPath();
                imaArrayList.add(Functions.getBitmapFromURLNew(getActivity(), image));
//                cardDuration[] =Long.parseLong(storyCardDataList.get(i).getCardDuration());
                cardDuration = add_element(i, cardDuration, Integer.parseInt(storyCardDataList.get(i).getCardDuration()));
            }

            Log.e("cardDuration-", "" + cardDuration);
            storiesProgressView = (StoriesProgressView) view.findViewById(R.id.stories);
            storiesProgressView.setStoriesCount(storyCardData.size());
//            storiesProgressView.setStoryDuration(3000L);
            storiesProgressView.setStoriesCountWithDuration(cardDuration);
            storiesProgressView.setStoriesListener(this);
            storiesProgressView.startStories(counter);

            rlMainView = (RelativeLayout) view.findViewById(R.id.rlMainView);

            llUpSlideView = (LinearLayout) view.findViewById(R.id.llUpSlideView);
            llUpSlideView.setVisibility(View.VISIBLE);

            llUpSlideView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    storiesProgressView.pause();
                    llBottomStoryView.setVisibility(View.VISIBLE);
//                    llUpSlideView.setVisibility(View.GONE);
                }
            });

            cardQuestion = (MaterialCardView) view.findViewById(R.id.cardQuestion);
            cardQuestion.setVisibility(View.GONE);

            tvQuestions = (TextView) view.findViewById(R.id.tvQuestions);
            tvOne = (TextView) view.findViewById(R.id.tvOne);
            tvOne.setVisibility(View.GONE);
            tvTwo = (TextView) view.findViewById(R.id.tvTwo);
            tvTwo.setVisibility(View.GONE);
            tvThree = (TextView) view.findViewById(R.id.tvThree);
            tvThree.setVisibility(View.GONE);
            tvFour = (TextView) view.findViewById(R.id.tvFour);
            tvFour.setVisibility(View.GONE);

            cardRedirect = (MaterialCardView) view.findViewById(R.id.cardRedirect);
            cardRedirect.setVisibility(View.GONE);
            ivSend = (RoundedImageView) view.findViewById(R.id.ivSend);
            tvTitleSend = (TextView) view.findViewById(R.id.tvTitleSend);

            cardRedirect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        // Type: community_post, keyword_detail, community, expert_advice, daily_quiz, reward;
                        JSONObject result = new JSONObject("" + action_param);
                        if (result != null && result.length() > 0) {
                            String key = result.optString("page");
                            String data = result.optString("data");
                            Log.e("data:", "" + key + "-" + data);

                            if (key.equalsIgnoreCase("community_post")) {

                            } else if (key.equalsIgnoreCase("keyword_detail")) {
                                Intent i = new Intent(getActivity(), SavdhaniDetailsActivity.class);
                                i.putExtra("keyword_id", "" + data);
                                startActivity(i);
                                storiesProgressView.destroy();
                                dismiss();
                            } else if (key.equalsIgnoreCase("community")) {
                                Intent i = new Intent(getActivity(), HomeActivity.class);
                                i.putExtra("pageType", "2");
                                i.putExtra("fromLaunch", "Y");
                                startActivity(i);
                                storiesProgressView.destroy();
                                dismiss();
                            } else if (key.equalsIgnoreCase("expert_advice")) {
                                Intent i = new Intent(getActivity(), PostQuestionActivity.class);
                                startActivity(i);
                                storiesProgressView.destroy();
                                dismiss();
                            } else if (key.equalsIgnoreCase("daily_quiz")) {
                                Intent intent = null;
                                try {
                                    if (RongoApp.getSelectedCrop().equalsIgnoreCase("11")) {
                                        intent = new Intent(getActivity(), QuestionSampleActivity.class);
                                    } else {
                                        if (Functions.getQuizIntent(getActivity(), sharedPreferences)) {
                                            intent = new Intent(getActivity(), QuizActivity.class);
                                        } else {
                                            intent = new Intent(getActivity(), QuestionSampleActivity.class);
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                startActivity(intent);
                                storiesProgressView.destroy();
                                dismiss();
                            } else if (key.equalsIgnoreCase("reward")) {
                                Intent i = new Intent(getActivity(), RewardActivity.class);
                                startActivity(i);
                                storiesProgressView.destroy();
                                dismiss();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            llStoryView = (LinearLayout) view.findViewById(R.id.llStoryView);
            llStoryView.setVisibility(View.GONE);

            llEditStoryView = (LinearLayout) view.findViewById(R.id.llEditStoryView);
            llEditStoryView.setVisibility(View.GONE);

//            Log.e("CardType=", "" + storyCardData.get(counter).getCardType());
            setUpdatedCardView();

            ivClose = (RoundedImageView) view.findViewById(R.id.ivClose);
            ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            View reverse = view.findViewById(R.id.reverse);
            reverse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    storiesProgressView.reverse();
                    llUpSlideView.setVisibility(View.VISIBLE);
                    llBottomStoryView.setVisibility(View.GONE);
                }
            });
            reverse.setOnTouchListener(onTouchListener);

            View skip = view.findViewById(R.id.skip);
            skip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    storiesProgressView.skip();
                    llUpSlideView.setVisibility(View.VISIBLE);
                    llBottomStoryView.setVisibility(View.GONE);
                }
            });
            skip.setOnTouchListener(onTouchListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setUpdatedCardView() {
        try {
            Functions.setFirebaseLogEventTrack(getActivity(), Constants.Screen_Track, "et_StoryView", "" + RongoApp.getUserId(), "Story-" + counter);
            Functions.setUserScreenTrackEvent(appDatabase, "et_StoryView", "" + RongoApp.getUserId());

//            Card Types: 1 - Only Image, 2 - Custom View
            if (storyCardDataList.get(counter).getCardType().equalsIgnoreCase("1")) {
                llStoryView.setVisibility(View.VISIBLE);
                llEditStoryView.setVisibility(View.GONE);
//                slideUp(llStoryView);
            } else {
//                slideUp(llEditStoryView);
                llStoryView.setVisibility(View.GONE);
                llEditStoryView.setVisibility(View.VISIBLE);
            }

            ivStoryView = (RoundedImageView) view.findViewById(R.id.ivStoryView);
            ivStoryView.setImageBitmap(imaArrayList.get(counter));

            rlMainView.setBackgroundColor(getDominantColor(imaArrayList.get(counter)));

            ivEditStoryView = (RoundedImageView) view.findViewById(R.id.ivEditStoryView);
            ivEditStoryView.setImageBitmap(imaArrayList.get(counter));

            ivEditStoryTitle = (AppCompatTextView) view.findViewById(R.id.ivEditStoryTitle);
            ivEditStoryDecs = (AppCompatTextView) view.findViewById(R.id.ivEditStoryDecs);
            ivEditStoryMeta = (AppCompatTextView) view.findViewById(R.id.ivEditStoryMeta);

            ImageView ivCloseStory = (ImageView) view.findViewById(R.id.ivCloseStory);
            ivCloseStory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    llBottomStoryView.setVisibility(View.GONE);
                }
            });

            if (!TextUtils.isEmpty(storyCardDataList.get(counter).getCardDescription())) {
                JSONObject result = new JSONObject("" + storyCardDataList.get(counter).getCardDescription());
                if (result != null && result.length() > 0) {
                    String key = result.getString(RongoApp.getDefaultLanguage());
                    JSONObject jsonObject = result.optJSONObject(RongoApp.getDefaultLanguage());
//                Log.e("data1:", "" + jsonObject.optString("meta"));
                    ivEditStoryTitle.setText(jsonObject.optString("card_title"));
                    ivEditStoryMeta.setText(jsonObject.optString("card_meta"));
                    ivEditStoryDecs.setText(jsonObject.optString("card_description"));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getDominantColor(Bitmap bitmap) {
        Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, 1, 1, true);
        final int color = newBitmap.getPixel(0, 0);
        newBitmap.recycle();
        return color;
    }

    //    setAudioView(true, getActivity().getString(R.string.expert), new ProgressBar(getActivity()));
    void setAudioView(boolean isPlay, String textVoice, ProgressBar progressBar) {
        try {
            if (isPlay) {
                Runnable onStart = new Runnable() {
                    public void run() {
                        progressBar.setVisibility(View.GONE);
//                        imgPlay.setImageResource(R.mipmap.pause);
                    }
                };
                Runnable onDone = new Runnable() {
                    public void run() {
                        progressBar.setVisibility(View.GONE);
//                        imgPlay.setImageResource(R.mipmap.play);
                    }
                };
                Runnable onError = new Runnable() {
                    public void run() {
                        progressBar.setVisibility(View.GONE);
//                        imgPlay.setImageResource(R.mipmap.play);
                    }
                };
                speakerbox.play(textVoice, onStart, onDone, onError);
            } else {
                speakerbox.isMuted();
                speakerbox.stop();
                speakerbox.shutdown();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void slideUp(View view) {
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    // slide the view from its current position to below itself
    public void slideDown(View view) {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    @Override
    public void onNext() {
//        image.setImageResource(resources[++counter]);
        counter = ++counter;
        if (cardType.equalsIgnoreCase("1")) {
            ivStoryView.setImageBitmap(imaArrayList.get(counter));
        } else {
            ivEditStoryView.setImageBitmap(imaArrayList.get(counter));
        }
        setUpdatedCardView();
    }

    @Override
    public void onPrev() {
        if ((counter - 1) < 0) return;
        //        image.setImageResource(resources[--counter]);
        counter = --counter;
        if (cardType.equalsIgnoreCase("1")) {
            ivStoryView.setImageBitmap(imaArrayList.get(counter));
        } else {
            ivEditStoryView.setImageBitmap(imaArrayList.get(counter));
        }
        setUpdatedCardView();
    }

    @Override
    public void onComplete() {
//        storiesProgressView.destroy();
//        dismiss();
//        llUpSlideView.setVisibility(View.GONE);
        if (storyType.equalsIgnoreCase("1")) {
            llBottomStoryView.setVisibility(View.VISIBLE);
        } else if (storyType.equalsIgnoreCase("3")) {
            cardQuestion.setVisibility(View.VISIBLE);
            try {
                JSONObject result = new JSONObject("" + action_param);
                if (result != null && result.length() > 0) {
                    String question = result.optString("question");
                    String answer = result.optString("answer");
                    JSONArray options = result.optJSONArray("options");
//                    Log.e("option-", "" + options.length());
                    tvQuestions.setText(question);
                    if (options != null && options.length() > 0) {
                        if (options.length() == 1) {
                            tvOne.setText(options.optString(0));
                            tvOne.setVisibility(View.VISIBLE);
                        }
                        if (options.length() == 2) {
                            tvOne.setText(options.optString(0));
                            tvTwo.setText(options.optString(1));
                            tvOne.setVisibility(View.VISIBLE);
                            tvTwo.setVisibility(View.VISIBLE);
                        }
                        if (options.length() == 3) {
                            tvOne.setText(options.optString(0));
                            tvTwo.setText(options.optString(1));
                            tvThree.setText(options.optString(2));
                            tvOne.setVisibility(View.VISIBLE);
                            tvTwo.setVisibility(View.VISIBLE);
                            tvThree.setVisibility(View.VISIBLE);
                        }
                        if (options.length() == 4) {
                            tvOne.setText(options.optString(0));
                            tvTwo.setText(options.optString(1));
                            tvThree.setText(options.optString(2));
                            tvFour.setText(options.optString(3));
                            tvOne.setVisibility(View.VISIBLE);
                            tvTwo.setVisibility(View.VISIBLE);
                            tvThree.setVisibility(View.VISIBLE);
                            tvFour.setVisibility(View.VISIBLE);
                        }
                    }

                    tvOne.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            submitStoryAnswer(tvOne.getText().toString());
                        }
                    });
                    tvTwo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            submitStoryAnswer(tvTwo.getText().toString());
                        }
                    });
                    tvThree.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            submitStoryAnswer(tvThree.getText().toString());
                        }
                    });
                    tvFour.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            submitStoryAnswer(tvFour.getText().toString());
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (storyType.equalsIgnoreCase("4")) {
            cardRedirect.setVisibility(View.VISIBLE);
            try {
                //       Type: community_post, keyword_detail, community, expert_advice, daily_quiz, reward;
                Log.e("data:", "" + action_param);
                JSONObject result = new JSONObject("" + action_param);
                if (result != null && result.length() > 0) {
                    String key = result.optString("page");
                    String data = result.optString("data");
                    Log.e("data:", "" + key + "-" + data);
                    if (key.equalsIgnoreCase("community_post")) {
                        tvTitleSend.setText(getActivity().getString(R.string.str_adhik_jane));
                        ivSend.setImageResource(R.mipmap.cb);
                    } else if (key.equalsIgnoreCase("keyword_detail")) {
                        tvTitleSend.setText(getActivity().getString(R.string.str_adhik_jane));
                        ivSend.setImageResource(R.mipmap.story_redi);
                    } else if (key.equalsIgnoreCase("community")) {
                        tvTitleSend.setText(getActivity().getString(R.string.str_adhik_jane));
                        ivSend.setImageResource(R.mipmap.cb);
                    } else if (key.equalsIgnoreCase("expert_advice")) {
                        tvTitleSend.setText(getActivity().getString(R.string.str_prachan_puche));
                        ivSend.setImageResource(R.mipmap.advisory_logo);
                    } else if (key.equalsIgnoreCase("daily_quiz")) {
                        tvTitleSend.setText(getActivity().getString(R.string.str_prachan_dekhe));
                        ivSend.setImageResource(R.mipmap.quiz_icon);
                    } else if (key.equalsIgnoreCase("reward")) {
                        tvTitleSend.setText(getActivity().getString(R.string.str_reward_dekhe));
                        ivSend.setImageResource(R.mipmap.reward);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            showDialog();
        }
        Log.e("onComplete=", "=" + storyType);
    }

    @Override
    public void onDestroy() {
        if (storiesProgressView != null)
            storiesProgressView.destroy();
        super.onDestroy();
    }


    void showDialog() {
        try {
            BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
            View sheetView = this.getLayoutInflater().inflate(R.layout.bottom_story_thanks_dialog, null);
            mBottomSheetDialog.setContentView(sheetView);

            TextView tvTagDecs = (TextView) sheetView.findViewById(R.id.tvTagDecs);
            LinearLayout llRewardView = (LinearLayout) sheetView.findViewById(R.id.llRewardView);

            MaterialCardView cardKeyword = (MaterialCardView) sheetView.findViewById(R.id.cardKeyword);
            cardKeyword.setVisibility(View.GONE);

            MaterialCardView questionCard = (MaterialCardView) sheetView.findViewById(R.id.questionCard);
            questionCard.setVisibility(View.GONE);

            RelativeLayout rlShareView = (RelativeLayout) sheetView.findViewById(R.id.rlShareView);
            rlShareView.setVisibility(View.VISIBLE);

            TextView tvRewardNew = (TextView) sheetView.findViewById(R.id.tvRewardNew);
            tvRewardNew.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));

            TextView tvRewardPoint1 = (TextView) sheetView.findViewById(R.id.tvRewardPoint1);
            tvRewardPoint1.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));

            TextView tvQuestions = (TextView) sheetView.findViewById(R.id.tvQuestions);
            TextView tvHa = (TextView) sheetView.findViewById(R.id.tvHa);
            TextView tvNa = (TextView) sheetView.findViewById(R.id.tvNa);
            TextView tvRewardPointQuestion = (TextView) sheetView.findViewById(R.id.tvRewardPointQuestion);
            tvRewardPointQuestion.setText(RewardPoints.getDailyQuizRewardPoint(sharedPreferences));

            RoundedImageView ivKeyword = (RoundedImageView) sheetView.findViewById(R.id.ivKeyword);
            TextView tvKeywordText = (TextView) sheetView.findViewById(R.id.tvKeywordText);

//        StoryTypes: 1 - Normal, 2 - Share, 3 - Question, 4 - Redirect
            if (storyType.equalsIgnoreCase("1")) {
                rlShareView.setVisibility(View.VISIBLE);
                cardKeyword.setVisibility(View.GONE);
                questionCard.setVisibility(View.GONE);
            } else if (storyType.equalsIgnoreCase("2")) {
                rlShareView.setVisibility(View.VISIBLE);
                cardKeyword.setVisibility(View.GONE);
                questionCard.setVisibility(View.GONE);
            } else if (storyType.equalsIgnoreCase("3")) {
                rlShareView.setVisibility(View.GONE);
                cardKeyword.setVisibility(View.GONE);
                questionCard.setVisibility(View.VISIBLE);

                JSONObject result = new JSONObject("" + action_param);
                if (result != null && result.length() > 0) {
                    String question = result.optString("question");
                    String answer = result.optString("answer");
                    JSONArray options = result.optJSONArray("options");
                    tvQuestions.setText(question);
                    tvHa.setText(options.optString(0));
                    tvNa.setText(options.optString(1));

                    tvHa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            submitStoryAnswer(tvHa.getText().toString());
                            mBottomSheetDialog.dismiss();
                        }
                    });
                    tvNa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            submitStoryAnswer(tvNa.getText().toString());
                            mBottomSheetDialog.dismiss();
                        }
                    });
                }
            } else if (storyType.equalsIgnoreCase("4")) {
//       Type: community_post, keyword_detail, community, expert_advice, daily_quiz, reward;
                rlShareView.setVisibility(View.GONE);
                cardKeyword.setVisibility(View.VISIBLE);
                questionCard.setVisibility(View.GONE);

                Log.e("data:", "" + action_param);
                JSONObject result = new JSONObject("" + action_param);
                if (result != null && result.length() > 0) {
                    String key = result.optString("page");
                    String data = result.optString("data");
                    Log.e("data:", "" + key + "-" + data);

                    if (key.equalsIgnoreCase("community_post")) {

                    } else if (key.equalsIgnoreCase("keyword_detail")) {
                        JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.loadAdvisoryKeywordJSONFromAsset(getActivity(), data));
                        if (jsonObject != null && jsonObject.length() > 0) {
                            tvKeywordText.setText(jsonObject.optString("name"));
//                            JSONArray advisory_image = jsonObject.optJSONArray("advisory_image");
//                            GlideApp.with(getActivity())
//                                    .load(Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH +
//                                            RongoApp.getSelectedCrop() + "/" + advisory_image.optString(0))
//                                    .placeholder(R.drawable.placeholderbg).error(R.drawable.placeholderbg)
//                                    .into(ivKeyword);
                        }
                        cardKeyword.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(getActivity(), SavdhaniDetailsActivity.class);
                                i.putExtra("keyword_id", "" + data);
                                startActivity(i);
                                mBottomSheetDialog.dismiss();
                                dismiss();
                            }
                        });
                    } else if (key.equalsIgnoreCase("community")) {
                        Intent i = new Intent(getActivity(), HomeActivity.class);
                        i.putExtra("pageType", "2");
                        i.putExtra("fromLaunch", "Y");
                        startActivity(i);
                        mBottomSheetDialog.dismiss();
                        dismiss();
                    } else if (key.equalsIgnoreCase("expert_advice")) {
                        Intent i = new Intent(getActivity(), PostQuestionActivity.class);
                        startActivity(i);
                        mBottomSheetDialog.dismiss();
                        dismiss();
                    } else if (key.equalsIgnoreCase("daily_quiz")) {
                        Intent intent = null;
                        try {
                            if (RongoApp.getSelectedCrop().equalsIgnoreCase("11")) {
                                intent = new Intent(getActivity(), QuestionSampleActivity.class);
                            } else {
                                if (Functions.getQuizIntent(getActivity(), sharedPreferences)) {
                                    intent = new Intent(getActivity(), QuizActivity.class);
                                } else {
                                    intent = new Intent(getActivity(), QuestionSampleActivity.class);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        startActivity(intent);
                        mBottomSheetDialog.dismiss();
                        dismiss();
                    } else if (key.equalsIgnoreCase("reward")) {
                        Intent i = new Intent(getActivity(), RewardActivity.class);
                        startActivity(i);
                        mBottomSheetDialog.dismiss();
                        dismiss();
                    }
                }
            }

            AppCompatTextView tvShareWhatapp = sheetView.findViewById(R.id.tvShareWhatapp);
            tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (InternetConnection.checkConnectionForFasl(getActivity())) {
                            Functions.shareWhatAppUrlIntent(getActivity(), image_path + storyCardDataList.get(0).getCardMediaPath());
                        } else {
                            Functions.shareWhatAppIntent(getActivity());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Functions.shareWhatAppIntent(getActivity());
                    }
                }
            });

            AppCompatTextView tvShareCopy = sheetView.findViewById(R.id.tvShareCopy);
            tvShareCopy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Functions.shareSMSIntent(getActivity(), RongoApp.getReferralText(getActivity()));
                }
            });

            AppCompatTextView tvSharefacebook = sheetView.findViewById(R.id.tvSharefacebook);
            tvSharefacebook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Functions.shareFacebookIntent(getActivity());
                }
            });

            AppCompatTextView tvShareMore = sheetView.findViewById(R.id.tvShareMore);
            tvShareMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Functions.performShare(getActivity(), appDatabase);
                }
            });

            ImageView ivClose = (ImageView) sheetView.findViewById(R.id.ivClose);
            ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    llBottomStoryView.setVisibility(View.VISIBLE);
                    mBottomSheetDialog.dismiss();
//                dismiss();
                }
            });

            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
            ((View) sheetView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
            mBottomSheetDialog.setCanceledOnTouchOutside(false);
            mBottomSheetDialog.setCancelable(false);
            mBottomSheetDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void submitStoryAnswer(String answer) {
        cardQuestion.setVisibility(View.GONE);
        if (InternetConnection.checkConnectionForFasl(getActivity())) {
            RetrofitClient.getInstance().getApi().submitStoryAnswer(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                    RongoApp.getAppVersion(), RongoApp.getUserId(), String.valueOf(storyId), answer).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        Toast.makeText(getActivity(), "" + getResources().getString(R.string.str_info_darj_ho_gaya_he), Toast.LENGTH_SHORT).show();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                dismiss();
                            }
                        }, 500);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        }
    }

    void setUserStoryData() {
        try {
            String storyData = sharedPreferences.getString(Constants.SharedPreferences_story_data, null);
            if (!TextUtils.isEmpty(storyData)) {
                JSONObject result = new JSONObject(storyData);
                if (result != null && result.length() > 0) {
                    image_path = result.optString("image_path");
                    JSONArray storiesArray = new JSONArray(result.optString("stories"));
                    if (storiesArray != null && storiesArray.length() > 0) {
                        StoryData storyArrayData = new Gson().fromJson(result.toString(), StoryData.class);
                        setStoryView(storyArrayData.getStories());
                    }
                }
            } else {
                getUserStories();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void getUserStories() {
        if (InternetConnection.checkConnectionForFasl(getActivity())) {
            RetrofitClient.getInstance().getApi().getUserStories(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                    RongoApp.getAppVersion(), RongoApp.getUserId(), "all").enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response != null && response.isSuccessful()) {
                            String r = response.body().string();
                            if (!TextUtils.isEmpty(r)) {
                                JSONObject jsonObject = new JSONObject(r);
                                if (jsonObject != null && jsonObject.length() > 0) {
                                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                        JSONObject dataObject = jsonObject.optJSONObject("data");
                                        image_path = dataObject.optString("image_path");
                                        JSONArray storiesArray = new JSONArray(dataObject.optString("stories"));
                                        if (storiesArray != null && storiesArray.length() > 0) {
                                            StoryData storyData = new Gson().fromJson(dataObject.toString(), StoryData.class);
//                                            Log.e("story_id=", "" + storyData.getStories().get(0).getStoryId());
                                            setStoryView(storyData.getStories());
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        }
    }


    public class StoryAdapter extends RecyclerView.Adapter<StoryAdapter.ViewHolder> {
        List<StoryData.Story> storyData;

        public StoryAdapter(List<StoryData.Story> storyData) {
            this.storyData = storyData;
        }

        @NotNull
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.story_list_item, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            try {
                storyType = storyData.get(0).getStoryType();
                String image = image_path + storyData.get(position).getStoryThumbnailPath();
                GlideApp.with(getActivity()).load(image).placeholder(R.drawable.placeholderbg)
                        .error(R.drawable.placeholderbg).into(holder.ivLogo);

                holder.ivLogo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        storyId = Integer.parseInt(storyData.get(position).getStoryId());
                        storyType = storyData.get(position).getStoryType();
                        action_param = storyData.get(position).getActionParam();
//                        Log.e("storyId Pos:", "" + storyData.get(position).getStoryId() + "==" + storyType);
                        for (int i = 0; i < storyData.size(); i++) {
                            if (storyData.get(i).getStoryId().equalsIgnoreCase(storyData.get(position).getStoryId())) {
                                storyData.get(i).setStorySelected("1");
                            } else {
                                storyData.get(i).setStorySelected("0");
                            }
                        }
                        notifyDataSetChanged();

                        setStoryCardView(storyData.get(position).getCards());
                    }
                });

                if (!TextUtils.isEmpty(storyData.get(position).getStorySelected()) &&
                        storyData.get(position).getStorySelected().equalsIgnoreCase("1")) {
                    holder.ivLogo.setBackgroundResource(R.drawable.circle_shape_app_border);
                } else {
                    holder.ivLogo.setBackgroundResource(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return storyData.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            RoundedImageView ivLogo;

            public ViewHolder(View itemView) {
                super(itemView);
                this.ivLogo = (RoundedImageView) itemView.findViewById(R.id.ivLogo);
            }
        }
    }

}