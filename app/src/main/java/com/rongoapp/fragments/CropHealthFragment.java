package com.rongoapp.fragments;

import static com.rongoapp.controller.Constants.CAMERA_REQUEST_CODE;
import static com.rongoapp.controller.Constants.GALLERY_REQUEST_CODE;
import static com.rongoapp.controller.Constants.providerAuthority;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.FileProvider;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.card.MaterialCardView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.makeramen.roundedimageview.RoundedImageView;
import com.rongoapp.R;
import com.rongoapp.activities.PostQuestionActivity;
import com.rongoapp.adapter.CropAreaForHealthStoryAdapter;
import com.rongoapp.adapter.CropProblemHealthStoryAdapter;
import com.rongoapp.adapter.ThumbnailsAdapter;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.AdvisoryPhotoData;
import com.rongoapp.data.CropAreaFocusedData;
import com.rongoapp.data.LuckyDrawData;
import com.rongoapp.data.ProblemRelatedData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.imagecompressor.ImageZipper;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.CoinGifView;
import com.rongoapp.views.GridSpacingItemDecoration;
import com.rongoapp.views.Log;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Executors;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CropHealthFragment extends DialogFragment {

    CropHealthFragment cropHealthFragment;
    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    View view;

    ProgressBar progressBar;
    CoinGifView ivGif;
    RelativeLayout rlSuccessView;
    RoundedImageView ivClose;
    AppCompatTextView txtHealthy, txtUnhealthy, txtUplaod, tvNoDataAvailable;
    MaterialCardView cardHealthStatus, cardHealthlly, cardUnHealthlly;
    RecyclerView rcvProblem;
    CropProblemHealthStoryAdapter cropProblemHealthStoryAdapter;
    String problem_related = "1";

    @Override
    public int getTheme() {
        return R.style.FullScreenDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.FullScreenDialog);

        sharedPreferences = getActivity().getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(getActivity());

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        RongoApp.logVisitedPageTabEvent("Crop Health");

        Functions.setFirebaseLogEventTrack(getActivity(), Constants.ET_STORY_TRACK, Constants.ET_CROP_HEALTH,
                "" + RongoApp.getUserId(), Constants.ET_CROP_HEALTH + "_" + RongoApp.getUserId());
        Functions.setUserScreenTrackEvent(appDatabase, Constants.ET_STORY_TRACK, Constants.ET_CROP_HEALTH + "_" + RongoApp.getUserId());

        RongoApp.engagementToolActivity(Constants.ET_CROP_HEALTH);
    }

    public static CropHealthFragment newInstance() {
        CropHealthFragment fragment = new CropHealthFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_crop_health_dialog, container, false);
        cropHealthFragment = this;
        setViewData();
        return view;
    }

    void setViewData() {
        try {
            ivClose = (RoundedImageView) view.findViewById(R.id.ivClose);
            ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            TextView tvBack = view.findViewById(R.id.tvBack);
            tvBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Constants.BROADCAST_RECEIVER.UPDATE_STORY);
                    intent.putExtra(Constants.STORY_NAME, Constants.STORY_QUIZ);
                    getActivity().sendBroadcast(intent);
                    dismiss();
                }
            });

            TextView tvNext = view.findViewById(R.id.tvNext);
            tvNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Constants.BROADCAST_RECEIVER.UPDATE_STORY);
                    intent.putExtra(Constants.STORY_NAME, Constants.STORY_SAVDHANI);
                    getActivity().sendBroadcast(intent);
                    dismiss();
                }
            });

            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

            cardHealthStatus = (MaterialCardView) view.findViewById(R.id.cardHealthStatus);
            cardHealthStatus.setVisibility(View.VISIBLE);

            cardHealthlly = (MaterialCardView) view.findViewById(R.id.cardHealthlly);
            cardHealthlly.setVisibility(View.GONE);

            cardUnHealthlly = (MaterialCardView) view.findViewById(R.id.cardUnHealthlly);
            cardUnHealthlly.setVisibility(View.GONE);

            rlSuccessView = (RelativeLayout) view.findViewById(R.id.rlSuccessView);
            rlSuccessView.setVisibility(View.GONE);
            ivGif = (CoinGifView) view.findViewById(R.id.ivGif);

            txtHealthy = (AppCompatTextView) view.findViewById(R.id.txtHealthy);
            txtUnhealthy = (AppCompatTextView) view.findViewById(R.id.txtUnhealthy);

            txtHealthy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cardHealthlly.setVisibility(View.VISIBLE);
                    cardHealthStatus.setVisibility(View.GONE);
                }
            });

            txtUnhealthy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cardUnHealthlly.setVisibility(View.VISIBLE);
                    cardHealthStatus.setVisibility(View.GONE);
                }
            });

            txtUplaod = (AppCompatTextView) view.findViewById(R.id.txtUplaod);
            txtUplaod.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openCrops();
                }
            });

            rcvProblem = (RecyclerView) view.findViewById(R.id.rcvProblem);
//            rcvProblem.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
            rcvProblem.addItemDecoration(new GridSpacingItemDecoration(3, (int) Functions.convertDpToPixel(10, getActivity()), false));
            rcvProblem.setNestedScrollingEnabled(false);
            rcvProblem.setLayoutManager(new GridLayoutManager(getActivity(), 3));

            List<ProblemRelatedData> options = SharedPreferencesUtils.getProblemRelated(getActivity(), Constants.SharedPreferences_OPTIONS);
            if (options != null && options.size() > 0) {
                cropProblemHealthStoryAdapter = new CropProblemHealthStoryAdapter(cropHealthFragment, options, problem_related);
                rcvProblem.setAdapter(cropProblemHealthStoryAdapter);
            }

            tvNoDataAvailable = (AppCompatTextView) view.findViewById(R.id.tvNoDataAvailable);
            tvNoDataAvailable.setVisibility(View.GONE);
            if (sharedPreferences.getString(Constants.SharedPreference_WeeklyUploadPhoto, "N").equalsIgnoreCase("Y")) {
                cardHealthStatus.setVisibility(View.GONE);
                tvNoDataAvailable.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getSelectedProblem(String problem_related) {
        this.problem_related = problem_related;
        Log.v("problem_related", "" + problem_related);

        Functions.setFirebaseLogEventTrack(getActivity(), Constants.Click_Event, "Health Story", "", "Ask Expert");

        Intent i = new Intent(getActivity(), PostQuestionActivity.class);
        i.putExtra("keyword_id", "0");
        i.putExtra("type", "2");
        i.putExtra("problem_related", problem_related);
        activityResultLauncher.launch(i);
    }

    ActivityResultLauncher<Intent> activityResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
        @Override
        public void onActivityResult(ActivityResult result) {
            if (result.getResultCode() == Activity.RESULT_OK) {
//                        Intent data = result.getData();
                Functions.openPostQuestionSuccessDialog(getActivity(), appDatabase, RewardPoints.getPostQuestionRewardPoint(sharedPreferences),
                        RewardPoints.getInviteRewardPoint(sharedPreferences));
            }
        }
    });

    BottomSheetDialog bottomSheetDialogCrop;
    CropAreaFocusedData mSelectedList;
    ArrayList<String> imagesPath = new ArrayList<>();
    String mCurrentPhotoPath = "";

    void openCrops() {
        bottomSheetDialogCrop = new BottomSheetDialog(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.bottom_lucky_draw_crops, null);
        bottomSheetDialogCrop.setContentView(view);
        bottomSheetDialogCrop.setCancelable(false);
        bottomSheetDialogCrop.setCanceledOnTouchOutside(false);

        LinearLayout llUploadView = (LinearLayout) view.findViewById(R.id.llUploadView);
        llUploadView.setVisibility(View.VISIBLE);

        MaterialCardView cardHealthlly = (MaterialCardView) view.findViewById(R.id.cardHealthlly);
        cardHealthlly.setVisibility(View.GONE);
        AppCompatTextView tvPoint = view.findViewById(R.id.tvPoint);
        tvPoint.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences));

        AppCompatTextView tvUpload = view.findViewById(R.id.tvUpload);
        tvUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardHealthlly.setVisibility(View.GONE);
                llUploadView.setVisibility(View.VISIBLE);
            }
        });

        AppCompatTextView tvPointsUploadPicture = view.findViewById(R.id.tvPointsUploadPicture);
        tvPointsUploadPicture.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences) + " " + getString(R.string.str_point_earned));

        RecyclerView rvCrops = view.findViewById(R.id.rvCrops);
        rvCrops.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        rvCrops.setNestedScrollingEnabled(false);
        rvCrops.setHasFixedSize(true);
        List<CropAreaFocusedData> cropAreaFocusedData = SharedPreferencesUtils.getCropArea(getActivity(), Constants.SharedPreferences_CROP_FOCUSED_AREA);
        if (cropAreaFocusedData != null && cropAreaFocusedData.size() > 0) {
            rvCrops.setAdapter(new CropAreaForHealthStoryAdapter(cropHealthFragment, cropAreaFocusedData, mSelectedList));
        }

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogCrop.dismiss();
            }
        });

        ImageView imgNext = view.findViewById(R.id.nextImg);
        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rvCrops.post(new Runnable() {
                    @Override
                    public void run() {
                        if (cropAreaFocusedData != null && cropAreaFocusedData.size() > 0) {
                            rvCrops.scrollToPosition(cropAreaFocusedData.size() - 1);
                        }
                    }
                });
            }
        });

        AppCompatTextView txtUpload = view.findViewById(R.id.txtUpload);
        txtUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogCrop.dismiss();
                accessPermission();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getActivity().getResources().getColor(android.R.color.transparent));
        bottomSheetDialogCrop.show();
    }

    public void getSelectedCropArea(CropAreaFocusedData mCropAreaFocusedData) {
        this.mSelectedList = mCropAreaFocusedData;
    }

    void openThumbnailDialog() {
        mCurrentPhotoPath = "";
        BottomSheetDialog bottomSheetThumbnail = new BottomSheetDialog(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.bottom_upload_more_photos, null);
        bottomSheetThumbnail.setContentView(view);

        AppCompatTextView tvPointsUploadPicture = view.findViewById(R.id.tvPointsUploadPicture);
        tvPointsUploadPicture.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences) + getString(R.string.str_point_earned));
        ImageView imgNext = view.findViewById(R.id.imgNext);
        ProgressBar pb = view.findViewById(R.id.pb);
        pb.setVisibility(View.GONE);
        LinearLayout linearLuckYDraw = view.findViewById(R.id.linearLuckYDraw);
        linearLuckYDraw.setVisibility(View.VISIBLE);
        bottomSheetThumbnail.setCanceledOnTouchOutside(false);
        bottomSheetThumbnail.setCancelable(false);

        if (imagesPath.size() == 1) {
            imgNext.setVisibility(View.GONE);
        } else {
            imgNext.setVisibility(View.VISIBLE);
        }

        RecyclerView rvAllPhotos = view.findViewById(R.id.rvAllPhotos);
        rvAllPhotos.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
        rvAllPhotos.setAdapter(new ThumbnailsAdapter(imagesPath));
        rvAllPhotos.setNestedScrollingEnabled(false);

        AppCompatTextView txtUploadMore = view.findViewById(R.id.txtUploadMore);
        AppCompatTextView txtGoOn = view.findViewById(R.id.txtGoOn);
        txtGoOn.setText(getResources().getString(R.string.str_darj_kare));

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetThumbnail.dismiss();

                if (bottomSheetDialogCrop != null) {
                    bottomSheetDialogCrop.show();
                }

                if (imagesPath != null && imagesPath.size() > 0) {
                    imagesPath.clear();
                }
            }
        });

        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rvAllPhotos.post(new Runnable() {
                    @Override
                    public void run() {
                        if (imagesPath != null && imagesPath.size() > 0) {
                            rvAllPhotos.scrollToPosition(imagesPath.size() - 1);
                        }
                    }
                });
            }
        });

        txtUploadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetThumbnail.dismiss();
                accessPermission();
            }
        });

        txtGoOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (imagesPath.size() > 0) {
                    List<AdvisoryPhotoData> advisoryPhotoData = new ArrayList<AdvisoryPhotoData>();
                    advisoryPhotoData.add(new AdvisoryPhotoData(0, sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, "0"),
                            RongoApp.getSelectedCrop(), RongoApp.getSelectedPCrop(), "1"));
                    appDatabase.advisoryPhotoDao().insertAll(advisoryPhotoData);
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (InternetConnection.checkConnectionForFasl(getActivity())) {
                            if (InternetConnection.getNetworkClass(getActivity()).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                setOfflineData(bottomSheetThumbnail);
                            } else {
                                uploadWeeklyPicture(bottomSheetThumbnail);
                            }
                        } else {
                            setOfflineData(bottomSheetThumbnail);
                        }
                    }
                }, 100);
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetThumbnail.show();
    }

    void setOfflineData(BottomSheetDialog bottomSheetThumbnail) {
        try {
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_WeeklyUploadPhoto, "Y");
            String focus_area = "";
            if (mSelectedList != null) {
                focus_area = mSelectedList.getType();
            }
            String finalFocus_area = focus_area;
            Executors.newSingleThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    List<LuckyDrawData> luckyDrawData = new ArrayList<>();
                    luckyDrawData.add(new LuckyDrawData(0, sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                            sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                            sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                            sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""),
                            finalFocus_area, "", "", imagesPath));
                    appDatabase.luckyDrawOfflineDao().insertAll(luckyDrawData);
                }
            });
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
//                    progressBar.setVisibility(View.GONE);
                    mSelectedList = null;
                    mCurrentPhotoPath = "";
                    imagesPath = new ArrayList<>();
                    bottomSheetThumbnail.dismiss();
//                    openSuccessForHealthy();
                    rlSuccessView.setVisibility(View.VISIBLE);
                    cardHealthStatus.setVisibility(View.GONE);
                    cardHealthlly.setVisibility(View.GONE);
                    cardUnHealthlly.setVisibility(View.GONE);
                }
            }, 500);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    RequestBody getRequestFile(File file) {
        RequestBody fbody = RequestBody.create(file, MediaType.parse("image/*"));
        return fbody;
    }

    void uploadWeeklyPicture(BottomSheetDialog bottomSheetThumbnail) {
        progressBar.setVisibility(View.VISIBLE);
        String focus_area = "";
        if (mSelectedList != null) {
            focus_area = mSelectedList.getType();
        }
        RequestBody requestBodyId = RequestBody.create(SharedPreferencesUtils.getUserId(getActivity()).trim(), MediaType.parse("text/plain"));
        RequestBody seasonIdBody = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyCropId = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyPCropId = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyCropWeek = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyDeviceId = RequestBody.create(Functions.getDeviceid(getActivity()), MediaType.parse("text/plain"));
        RequestBody requestBodyFCMToken = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyPictureArea = RequestBody.create(focus_area, MediaType.parse("text/plain"));
        RequestBody requestBodylatitude = RequestBody.create("", MediaType.parse("text/plain"));
        RequestBody requestBodylongitude = RequestBody.create("", MediaType.parse("text/plain"));
        RequestBody requestBodyAppVersion = RequestBody.create(RongoApp.getAppVersion(), MediaType.parse("text/plain"));
        RequestBody requestBodyAppLanguage = RequestBody.create(RongoApp.getDefaultLanguage(), MediaType.parse("text/plain"));
        RequestBody requestBodyAppSeason = RequestBody.create(RongoApp.getSeasonName(), MediaType.parse("text/plain"));

        Map<String, RequestBody> params = new HashMap<>();
        params.put("user_id", requestBodyId);
        params.put("season_id", seasonIdBody);
        params.put("crop_id", requestBodyCropId);
        params.put("p_crop_id", requestBodyPCropId);
        params.put("crop_week", requestBodyCropWeek);
        params.put("picture_area_focused", requestBodyPictureArea);
        params.put("geo_latitude", requestBodylatitude);
        params.put("geo_longitude", requestBodylongitude);
        params.put("device_id", requestBodyDeviceId);
        params.put("fcm_token", requestBodyFCMToken);
        params.put("app_version", requestBodyAppVersion);
        params.put("app_language", requestBodyAppLanguage);
        params.put("season_name", requestBodyAppSeason);

        if (imagesPath != null && imagesPath.size() > 0) {
            RequestBody requestBodyImageUploadedPath = RequestBody.create(imagesPath.toString(), MediaType.parse("text/plain"));
            params.put("picture_key_local", requestBodyImageUploadedPath);

            for (int i = 0; i < imagesPath.size(); i++) {
                File file = new File(imagesPath.get(i).toString().trim());
                try {
                    File compressedImages = new ImageZipper(getActivity()).compressToFile(file);
                    params.put("picture[]" + "\"; filename=\"" + file.getName(), getRequestFile(compressedImages));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        RetrofitClient.getInstance().getApi().uploadWeeklyPicture(params).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String str = response.body().string();
                        if (!TextUtils.isEmpty(str)) {
                            JSONObject jsonObject = new JSONObject(str);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_WeeklyUploadPhoto, "Y");
                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");

                                    mSelectedList = null;
                                    mCurrentPhotoPath = "";
                                    imagesPath = new ArrayList<>();
                                    bottomSheetThumbnail.dismiss();
//                                    openSuccessForHealthy();
                                    rlSuccessView.setVisibility(View.VISIBLE);
                                    cardHealthStatus.setVisibility(View.GONE);
                                    cardHealthlly.setVisibility(View.GONE);
                                    cardUnHealthlly.setVisibility(View.GONE);
                                }
                            }
                        }
                        progressBar.setVisibility(View.GONE);
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressBar.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    void accessPermission() {
        Dexter.withContext(getActivity())
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    void showImagePickerOptions() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        View sheetView = getActivity().getLayoutInflater().inflate(R.layout.bottom_photo_uplaod_options, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);

        LinearLayout linearCamera = sheetView.findViewById(R.id.linearCamera);
        LinearLayout linearGallery = sheetView.findViewById(R.id.linearGallery);
        sheetView.findViewById(R.id.imgClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                if (bottomSheetDialogCrop != null) {
                    bottomSheetDialogCrop.show();
                }
            }
        });

        linearCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                dispatchTakePictureIntent();
            }
        });

        linearGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, GALLERY_REQUEST_CODE);
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        ((View) sheetView.getParent()).setBackgroundColor(getActivity().getResources().getColor(android.R.color.transparent));
        mBottomSheetDialog.show();
    }

    void dispatchTakePictureIntent() {
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File photoFile = createImageFile();
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(), providerAuthority, photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                List<ResolveInfo> resolvedIntentActivities = getActivity().getPackageManager().queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolvedIntentInfo : resolvedIntentActivities) {
                    String packageName = resolvedIntentInfo.activityInfo.packageName;
                    getActivity().grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.POST_QUESTION_CODE && resultCode == Activity.RESULT_OK) {
            Functions.openPostQuestionSuccessDialog(getActivity(), appDatabase,
                    RewardPoints.getPostQuestionRewardPoint(sharedPreferences), RewardPoints.getInviteRewardPoint(sharedPreferences));
        }
        if (requestCode == CAMERA_REQUEST_CODE || requestCode == GALLERY_REQUEST_CODE) {
            try {
                if (requestCode == CAMERA_REQUEST_CODE && resultCode == getActivity().RESULT_OK) {
                    if (data != null && data.hasExtra("mCurrentPhotoPath")) {
                        mCurrentPhotoPath = data.getStringExtra("mCurrentPhotoPath");
                    }
                    File f = new File(mCurrentPhotoPath);
                    Uri contentUri = Uri.fromFile(f);
                    mCurrentPhotoPath = contentUri.getPath();
                }

                if (requestCode == GALLERY_REQUEST_CODE && resultCode == getActivity().RESULT_OK) {
                    Uri selectedImage = data.getData();
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = getActivity().getContentResolver().query(selectedImage, filePath, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    mCurrentPhotoPath = c.getString(columnIndex);
                    c.close();
                    if (mCurrentPhotoPath == null) {
                        Bitmap googlepath = Functions.getBitmapFromUri(selectedImage, getActivity());
                        mCurrentPhotoPath = Functions.getGooglePhotoImagePath(googlepath, getActivity());
                    }
                    if (mCurrentPhotoPath.contains("/root")) {
                        mCurrentPhotoPath = mCurrentPhotoPath.replace("/root", "");
                    }
                }

                if (mCurrentPhotoPath != null && !TextUtils.isEmpty(mCurrentPhotoPath)) {
                    imagesPath.add(mCurrentPhotoPath);
                    openThumbnailDialog();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    void openSuccessForHealthy() {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(getActivity());
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.bottom_success_for_healthy, null);
        bottomSheetDialog.setContentView(view);

        AppCompatTextView txtOfflineSMS = view.findViewById(R.id.txtOfflineSMS);
        if (InternetConnection.checkConnectionForFasl(getActivity())) {
            txtOfflineSMS.setVisibility(View.GONE);
        } else {
            txtOfflineSMS.setVisibility(View.VISIBLE);
        }

        AppCompatTextView tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences));

        AppCompatTextView tvRewardAnim = view.findViewById(R.id.tvRewardAnim);
        tvRewardAnim.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences));

        AppCompatTextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));

        AppCompatTextView tvShareWhatapp = view.findViewById(R.id.tvShareWhatapp);
        tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "WhatsApp");
                    Functions.shareWhatAppIntent(getActivity());
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        AppCompatTextView tvShareCopy = view.findViewById(R.id.tvShareCopy);
        tvShareCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "SMS");
                    Functions.shareSMSIntent(getActivity(), RongoApp.getReferralText(getActivity()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AppCompatTextView tvSharefacebook = view.findViewById(R.id.tvSharefacebook);
        tvSharefacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.setUserScreenTrackEvent(appDatabase, "Invite", "Facebook");
                Functions.shareFacebookIntent(getActivity());
            }
        });

        AppCompatTextView tvShareMore = view.findViewById(R.id.tvShareMore);
        tvShareMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.performShare(getActivity(), appDatabase);
            }
        });

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

}