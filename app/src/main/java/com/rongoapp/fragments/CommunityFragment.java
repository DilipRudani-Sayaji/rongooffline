package com.rongoapp.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rongoapp.R;
import com.rongoapp.activities.HomeActivity;
import com.rongoapp.activities.PostQuestionActivity;
import com.rongoapp.adapter.CommunityAdapter;
import com.rongoapp.adapter.CommunityQuesDetailsAdapter;
import com.rongoapp.adapter.CommunityQuestionsAdapter;
import com.rongoapp.adapter.CropProblemPostQuesAdapter;
import com.rongoapp.controller.AudioRecorderCallback;
import com.rongoapp.controller.AudioRecorderDialog;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.InitInterface;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.ExpertResponceModel;
import com.rongoapp.data.OfflineCommentData;
import com.rongoapp.data.OfflineLikeData;
import com.rongoapp.data.PostData;
import com.rongoapp.data.ProblemRelatedData;
import com.rongoapp.data.ProfileResultData;
import com.rongoapp.data.SurveyQuestionItemData;
import com.rongoapp.data.SurveyQuestions;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.GridSpacingItemDecoration;
import com.rongoapp.views.LinkTextView;
import com.rongoapp.views.Log;
import com.rongoapp.views.ProgressBarDialog;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommunityFragment extends Fragment implements InitInterface, CommunityAdapter.OnLoadMoreListener {

    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    HomeActivity homeActivity;
    View view, viewCommunity, viewPost, viewCommunity1, viewPost1;
    long mLastLongClick = 0;
    CommunityFragment communityFragment;
    boolean isLoad = true;
    ShimmerFrameLayout shimmerFrameLayout;

    NestedScrollView nestedSV;
    RelativeLayout rlPostQues;
    LinearLayout linearNoData, llQuestionTab, llCommunityTab, llRongoHelpLineNumber, llCommunityBtn, llQuestionBtn, llCommunityBtn1, llQuestionBtn1, llExpertView;
    AppCompatTextView txtAskExpert, txtBannertext, tvPostQues, txtQuestionText, expertSalahTab, communityTab, expertSalahTab1, communityTab1, tvRongoHelpLineNumber, tvRewardPoint;
    ImageView bannerImage;
    LinkTextView tvNext;

    RecyclerView rcvExpertSalah, rcvCommunity, rcvProblem;
    CommunityQuestionsAdapter communityQuestionsAdapter;
    List<PostData> postData = new ArrayList<PostData>();

    int pageCount = 1;
    CommunityAdapter communityAdapter;
    List<PostData> postCommunityData = new ArrayList<PostData>();

    RelativeLayout rlChatView, rlbanner, rlSubHeader, rlSubHeader1;
    AppCompatEditText edtComment;
    AppCompatTextView tvSendComment, tvNoComment, tvCommunityNoData, tvOffline, tvCommentCount, tvHeaderTitle, tvRewardNew, tvRewardNew1;
    RecyclerView rvDetails;
    CommunityQuesDetailsAdapter salahQuestionDetailsAdapter;
    List<ExpertResponceModel> expertResponceModels = new ArrayList<ExpertResponceModel>();

    AudioRecorderDialog audioRecorderDialog;
    AudioRecorderCallback audioRecorderCallback;

    String selectedOption = "", survey_id = "0", field_type = "", filePath = "", fileName = "", imagePath = "", audio_path = "";
    List<String> selectedStringsList = new ArrayList<>();
    List<String> selectedStringsListForNames = new ArrayList<>();
    AutoCompleteTextView ediOption;
    RecyclerView rvOptions;
    BottomSheetDialog surveyBottomDialog;
    TextView txtQuestion, tvPointsSurvey;

    BroadcastReceiver AUDIO_RECORD_RECEIVER = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent != null) {
                    fileName = intent.getStringExtra("mFileName");
                    filePath = intent.getStringExtra("mFilePath");
                    if (audioRecorderCallback != null) {
                        audioRecorderCallback.onDone("1");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public static CommunityFragment getInstance() {
        return new CommunityFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof HomeActivity) {
            homeActivity = (HomeActivity) context;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            getActivity().unregisterReceiver(AUDIO_RECORD_RECEIVER);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().registerReceiver(AUDIO_RECORD_RECEIVER, new IntentFilter(Constants.BROADCAST_RECEIVER.AUDIO_RECORD_RECEIVER));
        view = inflater.inflate(R.layout.fragment_community, container, false);

        communityFragment = this;
        sharedPreferences = homeActivity.getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(homeActivity);
        RongoApp.logVisitedPageTabEvent("Community");
        Functions.setFirebaseLogEventTrack(homeActivity, Constants.Screen_Track, "Community", "", "");
        Functions.setUserScreenTrackEvent(appDatabase, "Community", "0");

        if (appDatabase.surveyQuestionsDao().getAllQuestions().size() == 0 && InternetConnection.checkConnectionForFasl(homeActivity)) {
            getSurveyQuestions();
        }
        init();
        setListener();
        return view;
    }

    @Override
    public void init() {
        tvHeaderTitle = view.findViewById(R.id.tvHeaderTitle);
        rlbanner = view.findViewById(R.id.rlbanner);
        rlSubHeader = view.findViewById(R.id.rlSubHeader);

        tvNext = view.findViewById(R.id.tvNext);
        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(homeActivity, PostQuestionActivity.class);
                i.putExtra("keyword_id", "0");
                i.putExtra("type", "2");
                i.putExtra("problem_related", "1");
                homeActivity.startActivityForResult(i, Constants.POST_QUESTION_CODE);
            }
        });

        llQuestionTab = view.findViewById(R.id.llQuestionTab);
        llCommunityTab = view.findViewById(R.id.llCommunityTab);

        shimmerFrameLayout = view.findViewById(R.id.shimmerLayout);
        shimmerFrameLayout.setVisibility(View.VISIBLE);
        shimmerFrameLayout.startShimmer();

        llCommunityBtn = view.findViewById(R.id.llCommunityBtn);
        llQuestionBtn = view.findViewById(R.id.llQuestionBtn);
        expertSalahTab = view.findViewById(R.id.expertSalahTab);
        communityTab = view.findViewById(R.id.communityTab);
        viewCommunity = view.findViewById(R.id.viewCommunity);
        viewPost = view.findViewById(R.id.viewPost);

        tvCommunityNoData = view.findViewById(R.id.tvCommunityNoData);
        tvCommunityNoData.setVisibility(View.GONE);

        llCommunityBtn1 = view.findViewById(R.id.llCommunityBtn1);
        llQuestionBtn1 = view.findViewById(R.id.llQuestionBtn1);
        expertSalahTab1 = view.findViewById(R.id.expertSalahTab1);
        communityTab1 = view.findViewById(R.id.communityTab1);
        viewCommunity1 = view.findViewById(R.id.viewCommunity1);
        viewPost1 = view.findViewById(R.id.viewPost1);

        txtBannertext = view.findViewById(R.id.txtBannertext);
        txtQuestionText = view.findViewById(R.id.txtQuestionText);
        txtAskExpert = view.findViewById(R.id.txtAskExpert);

        rlPostQues = view.findViewById(R.id.rlPostQues);
        tvPostQues = view.findViewById(R.id.tvPostQues);

        tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText("" + RewardPoints.getPostQuestionRewardPoint(sharedPreferences));
        tvRewardNew = view.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText("" + RewardPoints.getPostQuestionRewardPoint(sharedPreferences));
        tvRewardNew1 = view.findViewById(R.id.tvRewardNew1);
        tvRewardNew1.setText("" + RewardPoints.getPostQuestionRewardPoint(sharedPreferences));

        linearNoData = view.findViewById(R.id.linearNoData);
        bannerImage = view.findViewById(R.id.bannerImage);

        rcvExpertSalah = view.findViewById(R.id.rcvExpertSalah);
        rcvExpertSalah.setLayoutManager(new LinearLayoutManager(homeActivity, RecyclerView.VERTICAL, false));
        rcvExpertSalah.setHasFixedSize(true);
        rcvExpertSalah.setNestedScrollingEnabled(false);
//        rcvExpertSalah.setItemViewCacheSize(20);
        rcvExpertSalah.setDrawingCacheEnabled(true);
        rcvExpertSalah.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        rcvCommunity = view.findViewById(R.id.rcvCommunity);
        rcvCommunity.setLayoutManager(new LinearLayoutManager(homeActivity));
        rcvCommunity.setHasFixedSize(true);
        rcvCommunity.setNestedScrollingEnabled(false);
//        rcvCommunity.setItemViewCacheSize(20);
        rcvCommunity.setDrawingCacheEnabled(true);
        rcvCommunity.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        llRongoHelpLineNumber = view.findViewById(R.id.llRongoHelpLineNumber);
        tvRongoHelpLineNumber = view.findViewById(R.id.tvRongoHelpLineNumber);
        if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreference_RongoHelpLineNumber, ""))) {
            tvRongoHelpLineNumber.setText("" + sharedPreferences.getString(Constants.SharedPreference_RongoHelpLineNumber, ""));
            llRongoHelpLineNumber.setVisibility(View.VISIBLE);
        } else {
            llRongoHelpLineNumber.setVisibility(View.INVISIBLE);
        }

        tvHeaderTitle.setText(getResources().getString(R.string.salah));
        rlSubHeader.setVisibility(View.VISIBLE);
        rlbanner.setVisibility(View.VISIBLE);
        llCommunityBtn.setVisibility(View.VISIBLE);
        llQuestionBtn.setVisibility(View.VISIBLE);
        setCommunityFunction();

        llExpertView = view.findViewById(R.id.llExpertView);
        rlSubHeader1 = view.findViewById(R.id.rlSubHeader1);
        nestedSV = view.findViewById(R.id.scrollview);
        nestedSV.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                Log.v("TAG", "Scroll DOWN" + v.getScrollY());
                if (v.getScrollY() > 550) {
                    rlSubHeader1.setAlpha(1f);
                    llExpertView.setAlpha(1f);
                } else if (v.getScrollY() < 500) {
                    rlSubHeader1.setAlpha(0f);
                    llExpertView.setAlpha(0f);
                }
                if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                    if (communityAdapter != null) {
                        communityAdapter.showLoading();
                    }
                }
            }
        });

        rcvProblem = view.findViewById(R.id.rcvProblem);
        rcvProblem.addItemDecoration(new GridSpacingItemDecoration(3, (int) Functions.convertDpToPixel(5, homeActivity), false));
        rcvProblem.setNestedScrollingEnabled(false);
        rcvProblem.setLayoutManager(new GridLayoutManager(homeActivity, 3));
        List<ProblemRelatedData> options = SharedPreferencesUtils.getProblemRelated(homeActivity, Constants.SharedPreferences_OPTIONS);
        if (options != null && options.size() > 0) {
            rcvProblem.setAdapter(new CropProblemPostQuesAdapter(homeActivity, options, new BottomSheetDialog(homeActivity)));
        } else {
            RongoApp.getMasterDropDownData(true);
        }
    }

    @Override
    public void setListener() {
        llRongoHelpLineNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreference_RongoHelpLineNumber, ""))) {
                    Functions.setFirebaseLogEventTrack(homeActivity, Constants.Screen_Track, "Salah", "", "Help Line number");
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + sharedPreferences.getString(Constants.SharedPreference_RongoHelpLineNumber, "")));
                    startActivity(intent);
                }
            }
        });

        expertSalahTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shimmerFrameLayout.setVisibility(View.VISIBLE);
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Salah_Tab, "1");
                setCommunityFunction();
            }
        });

        communityTab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shimmerFrameLayout.setVisibility(View.VISIBLE);
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Salah_Tab, "2");
                setCommunityFunction();
            }
        });

        expertSalahTab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shimmerFrameLayout.setVisibility(View.VISIBLE);
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Salah_Tab, "1");
                setCommunityFunction();
            }
        });

        communityTab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shimmerFrameLayout.setVisibility(View.VISIBLE);
                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Salah_Tab, "2");
                setCommunityFunction();
            }
        });

        rlPostQues.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                sendPostQuestion("1");
            }
        });

        txtAskExpert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                sendPostQuestion("1");
            }
        });
    }


    void setCommunityFunction() {
        try {
            if (sharedPreferences.getString(Constants.SharedPreferences_Salah_Tab, "2").equalsIgnoreCase("1")) {
                viewPost.setVisibility(View.VISIBLE);
                viewCommunity.setVisibility(View.GONE);
                viewPost1.setVisibility(View.VISIBLE);
                viewCommunity1.setVisibility(View.GONE);

                llQuestionTab.setVisibility(View.VISIBLE);
                llCommunityTab.setVisibility(View.GONE);

                if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                    ProfileResultData profileResultDataMie = Functions.getData();
                    String expiryContent = profileResultDataMie.getApi_content_expiry();
                    if (!TextUtils.isEmpty(expiryContent)) {
                        long expiryContentLong = Functions.covertToMili("yyyy-MM-dd HH:mm:ss", expiryContent);
                        if (expiryContentLong > sharedPreferences.getLong(Constants.SharedPreference_Time_FOR_API_CALL, 0)) {
                            if (profileResultDataMie.getView_pending_count() > 0) {
                                getPosts();
                            } else {
                                setQuesListFromDB();
                            }
                        } else {
                            getPosts();
                        }
                    } else {
                        getPosts();
                    }
                } else {
                    setQuesListFromDB();
                }
            } else if (sharedPreferences.getString(Constants.SharedPreferences_Salah_Tab, "2").equalsIgnoreCase("2")) {
                viewPost.setVisibility(View.GONE);
                viewCommunity.setVisibility(View.VISIBLE);
                viewPost1.setVisibility(View.GONE);
                viewCommunity1.setVisibility(View.VISIBLE);

                llQuestionTab.setVisibility(View.GONE);
                llCommunityTab.setVisibility(View.VISIBLE);

                postCommunityData = new ArrayList<PostData>();
                if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_Salah_Community, ""))) {
                    postCommunityData = Functions.getPostData(homeActivity, new JSONArray(sharedPreferences.getString(Constants.SharedPreferences_Salah_Community, "")));
                }

                communityAdapter = new CommunityAdapter(communityFragment, this, imagePath, audio_path, homeActivity);
                rcvCommunity.setAdapter(communityAdapter);

                if (postCommunityData.size() > 0) {
                    communityAdapter.addAll(postCommunityData);
                    tvCommunityNoData.setVisibility(View.GONE);
                    setShimmerGone();
                } else {
                    if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                        isLoad = true;
                        setCommunityDataFirst();
                    } else {
                        setShimmerGone();
                        tvCommunityNoData.setVisibility(View.VISIBLE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setCommunityDataFirst() {
        try {
            if (isLoad) {
                isLoad = false;
                pageCount = 1;
                getCommunityList(pageCount);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void sendPostQuestion(String problem_related) {
        Functions.setFirebaseLogEventTrack(homeActivity, Constants.Click_Event, "Salah", "", "Ask Expert");
        Intent i = new Intent(homeActivity, PostQuestionActivity.class);
        i.putExtra("keyword_id", "0");
        i.putExtra("type", "2");
        i.putExtra("problem_related", problem_related);
        activityResultLauncher.launch(i);
    }

    ActivityResultLauncher<Intent> activityResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
        @Override
        public void onActivityResult(ActivityResult result) {
            if (result.getResultCode() == Activity.RESULT_OK) {
//                        Intent data = result.getData();
                setCommunityFunction();
                Functions.openPostQuestionSuccessDialog(homeActivity, appDatabase, RewardPoints.getPostQuestionRewardPoint(sharedPreferences),
                        RewardPoints.getInviteRewardPoint(sharedPreferences));
            }
        }
    });

    void getPosts() {
        RetrofitClient.getInstance().getApi().getPosts(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(homeActivity),
                sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                SharedPreferencesUtils.getUserId(homeActivity)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String str = response.body().string();
                        if (!TextUtils.isEmpty(str)) {
                            JSONObject jsonObject = new JSONObject(str);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject data = jsonObject.optJSONObject("data");
                                    if (data != null && data.length() > 0) {
                                        JSONObject result = data.optJSONObject("result");
                                        if (result != null && result.length() > 0) {

                                            txtBannertext.setText(result.optString("banner_text"));
                                            txtQuestionText.setText(result.optString("no_question_text"));
                                            imagePath = result.optString("image_path");
                                            audio_path = result.optString("audio_path");
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_AUDIO_PATH, audio_path);
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, "bannerImage", result.optString("banner_image"));

                                            JSONArray jsonArray = result.optJSONArray("posts");
                                            if (jsonArray != null && jsonArray.length() > 0) {
                                                List<PostData> postData = Functions.getPostData(homeActivity, jsonArray);
                                                appDatabase.postDao().nukeTable();
                                                appDatabase.postDao().insertAll(postData);
                                            }

                                            setQuesListFromDB();
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        setShimmerGone();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                setShimmerGone();
                Toast.makeText(homeActivity, "" + t.getMessage().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    void setShimmerGone() {
        shimmerFrameLayout.stopShimmer();
        shimmerFrameLayout.setVisibility(View.GONE);
    }

    void setQuesListFromDB() {
        setShimmerGone();
        postData = appDatabase.postDao().getPosts();
        if (postData.size() == 0) {
            linearNoData.setVisibility(View.VISIBLE);
            rcvExpertSalah.setVisibility(View.GONE);
        } else {
            linearNoData.setVisibility(View.GONE);
            rcvExpertSalah.setVisibility(View.VISIBLE);
            communityQuestionsAdapter = new CommunityQuestionsAdapter(communityFragment, homeActivity, postData, imagePath, audio_path);
            rcvExpertSalah.setAdapter(communityQuestionsAdapter);
        }
    }

    @Override
    public void onLoadMore() {
        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
            pageCount++;
            getCommunityList(pageCount);
        }
    }

    void getCommunityList(int page) {
        RetrofitClient.getInstance().getApi().getCommunityPosts(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                SharedPreferencesUtils.getUserId(homeActivity), String.valueOf(page)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        tvCommunityNoData.setVisibility(View.GONE);
                        String str = response.body().string();
                        if (!TextUtils.isEmpty(str)) {
                            JSONObject jsonObject = new JSONObject(str);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject data = jsonObject.optJSONObject("data");
                                    if (data != null && data.length() > 0) {
                                        JSONObject result = data.optJSONObject("result");
                                        if (result != null && result.length() > 0) {
                                            imagePath = result.optString("image_path");
                                            audio_path = result.optString("audio_path");
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_AUDIO_PATH, "" + result.optString("audio_path"));
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_is_survey_visible, "" + result.optString("is_survey_visible"));
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_survey_post_position, "" + result.optString("survey_post_position"));

                                            JSONArray jsonArray = result.optJSONArray("posts");
                                            if (jsonArray != null && jsonArray.length() > 0) {
                                                Functions.savePostData(homeActivity, sharedPreferences, jsonArray, page);
                                                postCommunityData = Functions.getPostData(homeActivity, jsonArray);
                                                if (page == 1) {
                                                    communityAdapter.addAll(postCommunityData);
                                                } else {
                                                    communityAdapter.dismissLoading();
                                                    communityAdapter.addItemMore(postCommunityData);
                                                    communityAdapter.setMore(true);
                                                }
                                            } else {
                                                communityAdapter.dismissLoading();
                                                communityAdapter.setMore(true);
                                            }
                                        }

                                    }
                                }
                            }
                        }
                        setShimmerGone();
                    } catch (Exception e) {
                        e.printStackTrace();
                        tvCommunityNoData.setVisibility(View.GONE);
                        setShimmerGone();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                tvCommunityNoData.setVisibility(View.GONE);
                Toast.makeText(homeActivity, "" + t.getMessage().toString(), Toast.LENGTH_SHORT).show();
                setShimmerGone();
            }
        });
    }


    /*
     * TODO  Comment Functionality Process
     * */
    public void openCommentDialog(int pos, String postID, String comment) {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_comment_dialog, null);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setCanceledOnTouchOutside(false);

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        RelativeLayout rlAudioPathView = view.findViewById(R.id.rlAudioPathView);
        AppCompatTextView tvAudioPath = view.findViewById(R.id.tvAudioPath);

        ImageView ivVoiceRecorder = view.findViewById(R.id.ivVoiceRecorder);
        ivVoiceRecorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                audioRecorderCallback = status -> {
                    Log.v("status", "" + status);
                    if (status.equalsIgnoreCase("1")) {
                        tvAudioPath.setText(fileName);
                        ivVoiceRecorder.setVisibility(View.VISIBLE);
                        rlAudioPathView.setVisibility(View.VISIBLE);
                    }
                };
                audioRecorderDialog = new AudioRecorderDialog(audioRecorderCallback);
                audioRecorderDialog.show(homeActivity.getSupportFragmentManager(), "AudioRecorderDialog");
            }
        });

        tvCommentCount = view.findViewById(R.id.tvCommentCount);
        tvCommentCount.setVisibility(View.GONE);
        try {
            if (!TextUtils.isEmpty(comment)) {
                JSONArray jsonArray = new JSONArray(comment);
                if (jsonArray != null && jsonArray.length() > 0) {
                    tvCommentCount.setVisibility(View.VISIBLE);
                    tvCommentCount.setText(jsonArray.length() + " " + homeActivity.getResources().getString(R.string.str_comments_txt));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        rlChatView = view.findViewById(R.id.rlChatView);
        edtComment = view.findViewById(R.id.edtComment);
        tvNoComment = view.findViewById(R.id.tvNoComment);
        tvOffline = view.findViewById(R.id.tvOffline);
        tvOffline.setVisibility(View.GONE);

        tvSendComment = view.findViewById(R.id.tvSendComment);
        tvSendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Log.v("postID", "" + postID);
                if (!TextUtils.isEmpty(edtComment.getText().toString().trim())) {
                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    rlAudioPathView.setVisibility(View.GONE);
                    tvAudioPath.setText("");
                    if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                        if (!InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                            sendPostComment(postID, comment, pos);
                        } else {
                            saveCommentDataOffline(true, true, postID, comment, pos);
                            Toast.makeText(homeActivity, "" + getResources().getString(R.string.str_internet_comment_msg), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        saveCommentDataOffline(true, true, postID, comment, pos);
                        Toast.makeText(homeActivity, "" + getResources().getString(R.string.str_internet_comment_msg), Toast.LENGTH_SHORT).show();
                    }
                    ivVoiceRecorder.setVisibility(View.VISIBLE);
                    rlAudioPathView.setVisibility(View.GONE);
                } else {
                    Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), "" + getResources().getString(R.string.str_feedback_edittext));
                }
            }
        });

        rvDetails = view.findViewById(R.id.rvDetails);
        rvDetails.setLayoutManager(new LinearLayoutManager(homeActivity, RecyclerView.VERTICAL, false));

        saveCommentDataOffline(false, true, postID, comment, pos);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(homeActivity.getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    RequestBody getRequestAudioFile(File file) {
        RequestBody fbody = RequestBody.create(file, MediaType.parse("audio/*"));
        return fbody;
    }

    void sendPostComment(String postID, String comment, int pos) {
        RequestBody requestBodyId = RequestBody.create(SharedPreferencesUtils.getUserId(homeActivity).trim(), MediaType.parse("text/plain"));
        RequestBody requestBodyDeviceId = RequestBody.create(Functions.getDeviceid(homeActivity), MediaType.parse("text/plain"));
        RequestBody requestBodyFCMToken = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyAppVersion = RequestBody.create(RongoApp.getAppVersion(), MediaType.parse("text/plain"));
        RequestBody requestBodyAppLanguage = RequestBody.create(RongoApp.getDefaultLanguage(), MediaType.parse("text/plain"));
        RequestBody requestBodyPost_id = RequestBody.create(postID, MediaType.parse("text/plain"));
        RequestBody requestBodyType = RequestBody.create(edtComment.getText().toString(), MediaType.parse("text/plain"));
        RequestBody requestBodyAppSeason = RequestBody.create(RongoApp.getSeasonName(), MediaType.parse("text/plain"));

        Map<String, RequestBody> params = new HashMap<>();
        params.put("user_id", requestBodyId);
        params.put("device_id", requestBodyDeviceId);
        params.put("fcm_token", requestBodyFCMToken);
        params.put("app_version", requestBodyAppVersion);
        params.put("app_language", requestBodyAppLanguage);
        params.put("post_id", requestBodyPost_id);
        params.put("comment_text", requestBodyType);
        params.put("season_name", requestBodyAppSeason);

        if (!TextUtils.isEmpty(filePath)) {
            RequestBody requestBodyPath = RequestBody.create(filePath, MediaType.parse("text/plain"));
            params.put("recording_key", requestBodyPath);
            params.put("audio_path[]", requestBodyPath);
            File auFile = new File(filePath);
            params.put("audio[]" + "\"; filename=\"" + auFile.getName(), getRequestAudioFile(auFile));
        }

        RetrofitClient.getInstance().getApi().savePostComment(params).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                saveCommentDataOffline(false, true, postID, comment, pos);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void saveCommentDataOffline(boolean isInsert, boolean isDisplay, String postID, String comment, int pos) {
        try {
            if (isInsert) {
                List<OfflineCommentData> offlineCommentData = new ArrayList<OfflineCommentData>();
                offlineCommentData.add(new OfflineCommentData(new Random().nextInt(), SharedPreferencesUtils.getUserId(homeActivity),
                        postID, edtComment.getText().toString().trim(), "", filePath, Functions.getCurrentDateTime()));
                appDatabase.offlineCommentDao().insertAll(offlineCommentData);
            }

            if (isDisplay) {
                expertResponceModels = new ArrayList<ExpertResponceModel>();
                if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                    if (!TextUtils.isEmpty(edtComment.getText().toString().trim())) {
                        expertResponceModels.add(new ExpertResponceModel(RongoApp.getUserFullName(), "0", postID, "1", SharedPreferencesUtils.getUserId(homeActivity),
                                edtComment.getText().toString().trim(), "", "", "", "", "",
                                Functions.getCurrentDateTime(), "0", "", "", filePath, "", ""));
                    }
                }

                if (!TextUtils.isEmpty(comment)) {
                    JSONArray jsonArray = new JSONArray(comment);
                    if (jsonArray != null && jsonArray.length() > 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            ExpertResponceModel expertResponceModel = new ExpertResponceModel();
                            expertResponceModel.setPosted_user_name(jsonArray.optJSONObject(i).optString("posted_user_name"));
                            expertResponceModel.setComment_id(jsonArray.optJSONObject(i).optString("comment_id"));
                            expertResponceModel.setPost_id(jsonArray.optJSONObject(i).optString("post_id"));
                            expertResponceModel.setType(jsonArray.optJSONObject(i).optString("type"));
                            expertResponceModel.setUser_id(jsonArray.optJSONObject(i).optString("user_id"));
                            expertResponceModel.setComment_text(jsonArray.optJSONObject(i).optString("comment_text"));
                            expertResponceModel.setIs_action_required(jsonArray.optJSONObject(i).optString("is_action_required"));
                            expertResponceModel.setIs_applied(jsonArray.optJSONObject(i).optString("is_applied"));
                            expertResponceModel.setNot_applied_reason(jsonArray.optJSONObject(i).optString("not_applied_reason"));
                            expertResponceModel.setIs_useful(jsonArray.optJSONObject(i).optString("is_useful"));
                            expertResponceModel.setNot_useful_reason(jsonArray.optJSONObject(i).optString("not_useful_reason"));
                            expertResponceModel.setCreated_on(jsonArray.optJSONObject(i).optString("created_on"));
                            expertResponceModel.setVote_type(jsonArray.optJSONObject(i).optString("vote_type"));
                            expertResponceModel.setLikes(jsonArray.optJSONObject(i).optString("likes"));
                            expertResponceModel.setKeyword_advisory(jsonArray.optJSONObject(i).optString("keyword_advisory"));
                            expertResponceModel.setIs_expert(jsonArray.optJSONObject(i).optString("is_expert"));
                            expertResponceModel.setRecording_local_path(jsonArray.optJSONObject(i).optString("recording_key"));
                            expertResponceModel.setRecording_key(jsonArray.optJSONObject(i).optString("recording_local_path"));
                            expertResponceModels.add(expertResponceModel);
                        }
                    }
                }

                List<OfflineCommentData> offlineCommentData = appDatabase.offlineCommentDao().getAllComment();
                if (offlineCommentData.size() > 0) {
                    for (int i = 0; i < offlineCommentData.size(); i++) {
                        ExpertResponceModel expertResponceModel = new ExpertResponceModel();
                        expertResponceModel.setPost_id(offlineCommentData.get(i).getPost_id());
                        expertResponceModel.setUser_id(SharedPreferencesUtils.getUserId(homeActivity));
                        expertResponceModel.setComment_text(offlineCommentData.get(i).getComment_text());
                        expertResponceModel.setCreated_on(offlineCommentData.get(i).getDatetime());
                        expertResponceModel.setType("1");
                        expertResponceModel.setPosted_user_name(RongoApp.getUserFullName());
                        expertResponceModel.setComment_id("");
                        expertResponceModel.setIs_action_required("");
                        expertResponceModel.setIs_applied("");
                        expertResponceModel.setNot_applied_reason("");
                        expertResponceModel.setIs_useful("");
                        expertResponceModel.setNot_useful_reason("");
                        expertResponceModel.setVote_type("0");
                        expertResponceModel.setIs_expert("");
                        expertResponceModel.setRecording_local_path(offlineCommentData.get(i).getView());
                        expertResponceModel.setRecording_key("");
                        expertResponceModels.add(expertResponceModel);
                    }
                }

                if (!TextUtils.isEmpty(edtComment.getText().toString().trim())) {
                    ArrayList<ExpertResponceModel> commentModel = new ArrayList<ExpertResponceModel>();
                    ExpertResponceModel expertResponceModel = new ExpertResponceModel();
                    expertResponceModel.setPosted_user_name(RongoApp.getUserFullName());
                    expertResponceModel.setComment_id("0");
                    expertResponceModel.setPost_id(postID);
                    expertResponceModel.setType("1");
                    expertResponceModel.setUser_id(SharedPreferencesUtils.getUserId(homeActivity));
                    expertResponceModel.setComment_text(edtComment.getText().toString().trim());
                    expertResponceModel.setIs_action_required("");
                    expertResponceModel.setIs_applied("");
                    expertResponceModel.setNot_applied_reason("");
                    expertResponceModel.setIs_useful("");
                    expertResponceModel.setNot_useful_reason("");
                    expertResponceModel.setCreated_on(Functions.getCurrentDateTime());
                    expertResponceModel.setVote_type("");
                    expertResponceModel.setLikes("0");
                    expertResponceModel.setIs_expert("");
                    expertResponceModel.setRecording_local_path(filePath);
                    expertResponceModel.setRecording_key("");
                    commentModel.add(expertResponceModel);

                    if (!TextUtils.isEmpty(comment)) {
                        JSONArray jsonArray = new JSONArray(comment);
                        if (jsonArray != null && jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                expertResponceModel = new ExpertResponceModel();
                                expertResponceModel.setPosted_user_name(jsonArray.optJSONObject(i).optString("posted_user_name"));
                                expertResponceModel.setComment_id(jsonArray.optJSONObject(i).optString("comment_id"));
                                expertResponceModel.setPost_id(jsonArray.optJSONObject(i).optString("post_id"));
                                expertResponceModel.setType(jsonArray.optJSONObject(i).optString("type"));
                                expertResponceModel.setUser_id(jsonArray.optJSONObject(i).optString("user_id"));
                                expertResponceModel.setComment_text(jsonArray.optJSONObject(i).optString("comment_text"));
                                expertResponceModel.setIs_action_required(jsonArray.optJSONObject(i).optString("is_action_required"));
                                expertResponceModel.setIs_applied(jsonArray.optJSONObject(i).optString("is_applied"));
                                expertResponceModel.setNot_applied_reason(jsonArray.optJSONObject(i).optString("not_applied_reason"));
                                expertResponceModel.setIs_useful(jsonArray.optJSONObject(i).optString("is_useful"));
                                expertResponceModel.setNot_useful_reason(jsonArray.optJSONObject(i).optString("not_useful_reason"));
                                expertResponceModel.setCreated_on(jsonArray.optJSONObject(i).optString("created_on"));
                                expertResponceModel.setVote_type(jsonArray.optJSONObject(i).optString("vote_type"));
                                expertResponceModel.setLikes(jsonArray.optJSONObject(i).optString("likes"));
                                expertResponceModel.setIs_expert(jsonArray.optJSONObject(i).optString("is_expert"));
                                expertResponceModel.setRecording_local_path(jsonArray.optJSONObject(i).optString("recording_key"));
                                expertResponceModel.setRecording_key(jsonArray.optJSONObject(i).optString("recording_local_path"));
                                commentModel.add(expertResponceModel);
                            }
                        }
                    }

                    String listString = new Gson().toJson(commentModel, new TypeToken<ArrayList<ExpertResponceModel>>() {
                    }.getType());
                    JSONArray jsonArray = new JSONArray(listString);
                    if (sharedPreferences.getString(Constants.SharedPreferences_Salah_Tab, "2").equalsIgnoreCase("1")) {
                        postData.get(pos).setComments("" + jsonArray);
                        communityQuestionsAdapter.notifyDataSetChanged();
                    } else {
                        postCommunityData.get(pos).setComments("" + jsonArray);
                        communityAdapter.notifyDataSetChanged();
                    }
                    tvCommentCount.setVisibility(View.VISIBLE);
                    tvCommentCount.setText(jsonArray.length() + " " + homeActivity.getResources().getString(R.string.str_comments_txt));
                }

                if (expertResponceModels.size() > 0) {
                    tvNoComment.setVisibility(View.GONE);
                } else {
                    tvNoComment.setVisibility(View.VISIBLE);
                }
                salahQuestionDetailsAdapter = new CommunityQuesDetailsAdapter(homeActivity, audio_path, expertResponceModels);
                rvDetails.setAdapter(salahQuestionDetailsAdapter);
                salahQuestionDetailsAdapter.notifyDataSetChanged();
                edtComment.setText("");
                filePath = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /*
     * TODO Survey Dialog Function
     * */
    public void setSurveyDialog() {
        surveyBottomDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_survey_dialog, null);
        surveyBottomDialog.setContentView(view);

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                surveyBottomDialog.dismiss();
            }
        });

        TextView txtTitle = view.findViewById(R.id.txtTitle);
        txtQuestion = view.findViewById(R.id.txtQuestion);
        tvPointsSurvey = view.findViewById(R.id.tvPointsSurvey);
        tvPointsSurvey.setText(RewardPoints.getSurveyRewardPoint(sharedPreferences) + " " + homeActivity.getString(R.string.str_points));

        rvOptions = view.findViewById(R.id.rvOptions);
        rvOptions.setLayoutManager(new LinearLayoutManager(homeActivity, RecyclerView.VERTICAL, false));

        ediOption = view.findViewById(R.id.ediOption);
        ediOption.setCursorVisible(false);
        ediOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                if (rvOptions.getVisibility() == View.VISIBLE) {
                    rvOptions.setVisibility(View.GONE);
                } else {
                    rvOptions.setVisibility(View.VISIBLE);
                }
            }
        });

        LinearLayout linearGo = view.findViewById(R.id.linearGo);
        linearGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (InternetConnection.checkConnection(homeActivity)) {
                    if (TextUtils.isEmpty(ediOption.getText().toString().trim())) {
                        Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), homeActivity.getString(R.string.str_please_select_option));
                        Toast.makeText(homeActivity, homeActivity.getString(R.string.str_please_select_option), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (rvOptions.getVisibility() == View.VISIBLE) {
                        rvOptions.setVisibility(View.GONE);
                    }

                    if (Functions.doubleTapCheck(mLastLongClick)) {
                        return;
                    }
                    mLastLongClick = SystemClock.elapsedRealtime();

                    surveyResponse();
                } else {
                    Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), homeActivity.getString(R.string.no_connection));
                }
            }
        });

        generateOptions();

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(homeActivity.getResources().getColor(android.R.color.transparent));
        surveyBottomDialog.show();
    }

    void generateOptions() {
        String surveyId = "0";
        if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_LAST_SURVEY_ID, "0"))) {
            surveyId = sharedPreferences.getString(Constants.SharedPreferences_LAST_SURVEY_ID, "0");
        }
        if (surveyId.equalsIgnoreCase("null") || surveyId == null) {
            surveyId = "0";
        }
        if (appDatabase.surveyQuestionsDao().getAllQuestions().size() == 0) {
            if (InternetConnection.checkConnection(homeActivity)) {
                getSurveyQuestions();
            }
        }
        List<SurveyQuestions> surveyQuestions = appDatabase.surveyQuestionsDao().getNextQuestion(Integer.parseInt(surveyId));
        if (!surveyQuestions.isEmpty()) {
            try {
                List<SurveyQuestionItemData> surveyQuestionItemData = surveyQuestions.get(0).getSurveyQuestionItemData();
                survey_id = surveyQuestions.get(0).getKeyValue() + "";
                for (int j = 0; j < surveyQuestionItemData.size(); j++) {
                    txtQuestion.setText(surveyQuestionItemData.get(j).getQuestion());
                    field_type = surveyQuestionItemData.get(j).getField_type();
                    List<String> list = surveyQuestionItemData.get(j).getOptions();
                    if (!list.isEmpty()) {
                        rvOptions.setAdapter(new SurveyOptionsAdapter(list, field_type, selectedOption, selectedStringsList, selectedStringsListForNames));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (sharedPreferences.getString(Constants.SharedPreferences_Salah_Tab, "2").equalsIgnoreCase("1")) {
                communityQuestionsAdapter.notifyDataSetChanged();
            } else {
                communityAdapter.notifyDataSetChanged();
            }
        }
    }

    void getSurveyQuestions() {
        RetrofitClient.getInstance().getApi().getSurveyQuestions(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(homeActivity),
                sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), SharedPreferencesUtils.getUserId(homeActivity)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response != null && response.isSuccessful()) {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        if (jsonObject != null && jsonObject.length() > 0) {
                            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                JSONObject data = jsonObject.optJSONObject("data");
                                if (data != null && data.length() > 0) {

                                    JSONObject result = data.optJSONObject("result");
                                    List<SurveyQuestions> surveyQuestions = new ArrayList<>();
                                    if (result != null && result.length() > 0) {
                                        Iterator<String> keys = result.keys();
                                        while (keys.hasNext()) {
                                            String key = keys.next();
                                            JSONObject value = result.optJSONObject(key);

                                            String id = "0";
                                            id = value.optString("id");
                                            int keyValue = Integer.parseInt((id.equalsIgnoreCase("")) ? "0" : id);
                                            String question = value.optString("question");
                                            String field_type = value.optString("field_type");

                                            JSONArray jsonArray = value.optJSONArray("options");
                                            List<SurveyQuestionItemData> surveyQuestionItemData = new ArrayList<>();
                                            List<String> optionsLis = new ArrayList<>();
                                            if (jsonArray != null && jsonArray.length() > 0) {
                                                for (int i = 0; i < jsonArray.length(); i++) {
                                                    optionsLis.add(jsonArray.optString(i));
                                                }
                                            }
                                            surveyQuestionItemData.add(new SurveyQuestionItemData(0, id, question, field_type, optionsLis));
                                            surveyQuestions.add(new SurveyQuestions(0, keyValue, surveyQuestionItemData));
                                        }
                                    }

                                    appDatabase.surveyQuestionsDao().insertAll(surveyQuestions);
                                    SharedPreferencesUtils.saveSurveyQuestions(homeActivity, surveyQuestions, "survey");
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void surveyResponse() {
        String response = "";
        if (field_type.equalsIgnoreCase("select")) {
            response = selectedOption;

        } else if (field_type.equalsIgnoreCase("select_multiple")) {
            response = TextUtils.join(",", selectedStringsListForNames);
        }

        RetrofitClient.getInstance().getApi().surveyResponse(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(homeActivity),
                sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                SharedPreferencesUtils.getUserId(homeActivity), survey_id, response).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {

                    ediOption.getText().clear();
                    selectedStringsList.clear();
                    selectedStringsListForNames.clear();
                    selectedOption = "";

                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_LAST_SURVEY_ID, survey_id + "");

                    ProfileResultData profileResultData = Functions.getData();
                    if (profileResultData != null) {
                        profileResultData.setLast_survey_id(survey_id);
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                                Constants.SharedPreference_profileResultData, (new Gson()).toJson(profileResultData));
                    }

                    List<SurveyQuestions> surveyQuestions = appDatabase.surveyQuestionsDao().getNextQuestion(Integer.parseInt(survey_id));
                    if (surveyQuestions.isEmpty()) {
                        if (sharedPreferences.getString(Constants.SharedPreferences_Salah_Tab, "2").equalsIgnoreCase("1")) {
                            communityQuestionsAdapter.notifyDataSetChanged();
                        } else {
                            communityAdapter.notifyDataSetChanged();
                        }
                        surveyBottomDialog.dismiss();
                        Functions.openCommenShareDialog(homeActivity, RewardPoints.getSurveyRewardPoint(sharedPreferences), sharedPreferences, appDatabase);
                    }

                    generateOptions();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    class SurveyOptionsAdapter extends RecyclerView.Adapter<SurveyOptionsAdapter.ViewHolder> {
        String field_type = "", selectedStr = "";
        List<String> listdata;
        List<String> selectedStringsList = new ArrayList<>();
        List<String> selectedStringsListForNames;

        public SurveyOptionsAdapter(List<String> listdata, String fieldType, String selectedStr, List<String> selectedStringsList, List<String> selectedStringsListForNames) {
            this.listdata = listdata;
            this.field_type = fieldType;
            this.selectedStringsList = selectedStringsList;
            this.selectedStr = selectedStr;
            this.selectedStringsListForNames = selectedStringsListForNames;
        }

        @NotNull
        @Override
        public SurveyOptionsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_questions_selection, parent, false);
            return new SurveyOptionsAdapter.ViewHolder(listItem);
        }

        @Override
        public void onBindViewHolder(SurveyOptionsAdapter.ViewHolder holder, int position) {
            holder.textView.setText(listdata.get(position));

            if (field_type.equalsIgnoreCase("select")) {
                if (listdata.get(position).equalsIgnoreCase(selectedStr)) {
                    holder.imgSelection.setImageResource(R.mipmap.uria_selected);
                } else {
                    holder.imgSelection.setImageResource(R.mipmap.uria_unselected);
                }

            } else if (field_type.equalsIgnoreCase("select_multiple")) {
                if (selectedStringsList.contains(position + "")) {
                    holder.imgSelection.setImageResource(R.mipmap.uria_selected);
                } else {
                    holder.imgSelection.setImageResource(R.mipmap.uria_unselected);
                }
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (field_type.equalsIgnoreCase("select")) {
                        singleSelection(listdata.get(position));
                    } else if (field_type.equalsIgnoreCase("select_multiple")) {
                        multipleSelection(listdata.get(position), position + "");
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return listdata.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            AppCompatTextView textView;
            ImageView imgSelection;

            public ViewHolder(View itemView) {
                super(itemView);
                this.textView = itemView.findViewById(R.id.checkbox);
                this.imgSelection = itemView.findViewById(R.id.imgSelection);
            }
        }

        public void singleSelection(String myStr) {
            selectedStr = myStr;
            rvOptions.setVisibility(View.GONE);
            selectedOption = myStr;
            ediOption.setText(selectedOption);
            notifyDataSetChanged();
        }

        public void multipleSelection(String myStr, String position) {
            selectedStr = position;
            if (selectedStringsList.contains(selectedStr)) {
                selectedStringsList.remove(selectedStr);
            } else {
                selectedStringsList.add(selectedStr);
            }

            if (selectedStringsListForNames.contains(myStr)) {
                selectedStringsListForNames.remove(myStr);
            } else {
                selectedStringsListForNames.add(myStr);
            }

            ediOption.setText(TextUtils.join(",", selectedStringsListForNames));
            notifyDataSetChanged();
        }

    }

    /*
     * TODO Like Event Function
     * */
    public void likeCommunityPost(int pos, String postID) {
        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
            RetrofitClient.getInstance().getApi().likeCommunityPost(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    SharedPreferencesUtils.getUserId(homeActivity), postID, "", "1").enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        setLikeEvent(false, pos, postID);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        } else {
            setLikeEvent(true, pos, postID);
        }
    }

    void setLikeEvent(boolean isInsert, int pos, String postID) {
        try {
            if (sharedPreferences.getString(Constants.SharedPreferences_Salah_Tab, "2").equalsIgnoreCase("1")) {
                int count = Integer.parseInt(postData.get(pos).getLikes());
                postData.get(pos).setLikes(String.valueOf(count + 1));
                postData.get(pos).setVote_type("1");
                communityQuestionsAdapter.notifyDataSetChanged();
            } else {
                int count = Integer.parseInt(postCommunityData.get(pos).getLikes());
                postCommunityData.get(pos).setLikes(String.valueOf(count + 1));
                postCommunityData.get(pos).setVote_type("1");
                communityAdapter.notifyDataSetChanged();
            }

            if (isInsert) {
                final List<OfflineLikeData> offlineLikeData = new ArrayList<OfflineLikeData>();
                offlineLikeData.add(new OfflineLikeData(new Random().nextInt(), postID, "1", "", Functions.getCurrentDateTime()));
                appDatabase.offlineLikeDao().insertAll(offlineLikeData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
