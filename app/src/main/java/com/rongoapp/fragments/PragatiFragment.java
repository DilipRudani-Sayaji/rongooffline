package com.rongoapp.fragments;

import static com.rongoapp.controller.Constants.CAMERA_REQUEST_CODE;
import static com.rongoapp.controller.Constants.GALLERY_REQUEST_CODE;
import static com.rongoapp.controller.Constants.providerAuthority;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.card.MaterialCardView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.rongoapp.R;
import com.rongoapp.activities.HomeActivity;
import com.rongoapp.activities.SavdhaniDetailsActivity;
import com.rongoapp.adapter.CropAreaForPragatiAdapter;
import com.rongoapp.adapter.SpinnerArrayAdapter;
import com.rongoapp.adapter.ThumbnailsAdapter;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.ImageZoomDialog;
import com.rongoapp.controller.InitInterface;
import com.rongoapp.controller.LocaleManager;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.AdvisoryPhotoData;
import com.rongoapp.data.AdvisoryStatusData;
import com.rongoapp.data.CropAreaFocusedData;
import com.rongoapp.data.CropHistoryModel;
import com.rongoapp.data.CropWeekDetails;
import com.rongoapp.data.CropsData;
import com.rongoapp.data.LuckyDrawData;
import com.rongoapp.data.OfflineWeeklyCropInfoData;
import com.rongoapp.data.PragatiSavadhaniData;
import com.rongoapp.data.PragatiTimelineData;
import com.rongoapp.data.WeeklyCropInfo;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.imagecompressor.ImageZipper;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.DateUtils;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.utilities.RoundRectCornerImageView;
import com.rongoapp.views.Log;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.Executors;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PragatiFragment extends Fragment implements InitInterface {

    HomeActivity homeActivity;
    PragatiFragment pragatiFragment;
    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    View view;
    long mLastLongClick = 0;
    ShimmerFrameLayout shimmerFrameLayout;

    CardView cardSavdhaniView, cardCropHistory, cardPragatiView;
    RecyclerView rcvRogo, rcvPragati, rvOptions1, rcvCropHistory;
    Spinner spinnerOptions = null;
    TextView txtStageName, txtWeek, tvNoData, tvQuestions, tvHa, tvNa, tvDarjKare, tvDarjKare1;
    LinearLayout llButtonView, llSelectionView, llEditView, llMultiSelection;
    RelativeLayout rlStateSpinner;
    ImageView ivWeeklyCropQues, ivPragatiCropBG;
    AppCompatEditText edtProduct = null;
    AutoCompleteTextView ediOption1;
    MultiOptionsAdapter multiOptionsAdapter;

    BottomSheetDialog bottomSheetDialogCrop;
    CropAreaFocusedData mSelectedList;
    ArrayList<String> imagesPath = new ArrayList<>();
    List<String> arrayList = new ArrayList<>();
    List<String> stringList = new ArrayList<>();
    String options = "", infoText = "", infoType = "", weekCropDay = "", inputType = "", titleText = "",
            cropWeeks = "0", cropDays = "0", mCurrentPhotoPath = "", selectedOption = "";

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof HomeActivity) {
            homeActivity = (HomeActivity) context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static PragatiFragment getInstance() {
        return new PragatiFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pragati, container, false);

        pragatiFragment = this;
        sharedPreferences = homeActivity.getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(homeActivity);
        RongoApp.logVisitedPageTabEvent("PRAGATI");
        Functions.setFirebaseLogEventTrack(homeActivity, Constants.Screen_Track, "Pragati", "", "");
        Functions.setUserScreenTrackEvent(appDatabase, "Pragati", "0");

        init();
        setListener();
        RongoApp.getMasterDropDownData(false);
        return view;
    }

    void setShimmerGone() {
        shimmerFrameLayout.stopShimmer();
        shimmerFrameLayout.setVisibility(View.GONE);
    }

    @Override
    public void init() {
        try {
            ivPragatiCropBG = view.findViewById(R.id.ivPragatiCropBG);
            if (RongoApp.getSelectedCrop().equalsIgnoreCase("1")) {
                ivPragatiCropBG.setImageResource(R.mipmap.pragati_maize_bg);
            } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("2")) {
                ivPragatiCropBG.setImageResource(R.mipmap.pragati_rice_bg);
            } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("3")) {
                ivPragatiCropBG.setImageResource(R.mipmap.pragati_cotton_bg);
            } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("4")) {
                ivPragatiCropBG.setImageResource(R.mipmap.pragati_wheat_bg);
            } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("5")) {
                ivPragatiCropBG.setImageResource(R.mipmap.pragati_okra_bg);
            } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("6")) {
                ivPragatiCropBG.setImageResource(R.mipmap.pragati_pearlmillet_bg);
            } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("7")) {
                ivPragatiCropBG.setImageResource(R.mipmap.pragati_corinder_bg);
            } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("8")) {
                ivPragatiCropBG.setImageResource(R.mipmap.pragati_bittergourd_bg);
            } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("9")) {
                ivPragatiCropBG.setImageResource(R.mipmap.pragati_bottegourd_bg);
            } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("10")) {
                ivPragatiCropBG.setImageResource(R.mipmap.pragati_watermelon_bg);
            } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("11")) {
                ivPragatiCropBG.setImageResource(R.mipmap.pragati_groundnut_bg);
            } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("12")) {
                ivPragatiCropBG.setImageResource(R.mipmap.pragati_soyabean_bg);
            } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("13")) {
                ivPragatiCropBG.setImageResource(R.mipmap.pragati_caster_bg);
            }

            txtStageName = view.findViewById(R.id.txtStageName);
            txtWeek = view.findViewById(R.id.txtWeek);

            cardSavdhaniView = view.findViewById(R.id.cardSavdhaniView);
            cardPragatiView = view.findViewById(R.id.cardPragatiView);

            shimmerFrameLayout = view.findViewById(R.id.shimmerLayout);
            shimmerFrameLayout.setVisibility(View.VISIBLE);
            shimmerFrameLayout.startShimmer();

            tvNoData = view.findViewById(R.id.tvNoData);
            tvNoData.setVisibility(View.GONE);

            rcvRogo = view.findViewById(R.id.rcvRogo);
            rcvRogo.setLayoutManager(new LinearLayoutManager(homeActivity));

            rcvPragati = view.findViewById(R.id.rcvPragati);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(homeActivity);
            linearLayoutManager.setReverseLayout(true);
            linearLayoutManager.setStackFromEnd(true);
            rcvPragati.setLayoutManager(linearLayoutManager);

            cardCropHistory = view.findViewById(R.id.cardCropHistory);
            rcvCropHistory = view.findViewById(R.id.rcvCropHistory);
            rcvCropHistory.setLayoutManager(new LinearLayoutManager(homeActivity));

            List<CropsData> cropsData = Functions.getCropData(appDatabase, sharedPreferences);
            if (cropsData != null && cropsData.size() > 0) {
                List<CropWeekDetails> cropWeekDetail = cropsData.get(0).getCropWeekDetailsLis();
                if (RongoApp.getCropWeeksFunction() == Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"))) {
                    txtStageName.setText("" + getResources().getString(R.string.str_phasal_end_txt));
                } else {
                    txtStageName.setText(cropWeekDetail.get(RongoApp.getCropWeeksFunction()).getDisplay_name());
                }
            }

            if (sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0").equalsIgnoreCase("0")) {
                txtWeek.setVisibility(View.INVISIBLE);
            } else {
                txtWeek.setVisibility(View.VISIBLE);
            }
            txtWeek.setText("" + Functions.getName(RongoApp.getCropWeeksFunction()) + " " + homeActivity.getResources().getString(R.string.saptah));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setPragatiData();
    }

    @Override
    public void setListener() {
    }

    void setCropHistoryData() {
        try {
            List<CropHistoryModel> cropHistoryModels = new ArrayList<>();
            String pragatiData = sharedPreferences.getString(Constants.SharedPreferences_Pragati, null);
            if (!TextUtils.isEmpty(pragatiData)) {
                JSONObject result = new JSONObject(pragatiData);
                if (result != null && result.length() > 0) {
                    JSONArray jsonArray = result.optJSONArray("past_crops");
                    if (jsonArray != null) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.optJSONObject(i);
                            CropHistoryModel cropHistoryModel = new CropHistoryModel();
                            cropHistoryModel.setP_crop_id(object.optString("p_crop_id"));
                            cropHistoryModel.setUser_id(object.optString("user_id"));
                            cropHistoryModel.setCrop_id(object.optString("crop_id"));
                            cropHistoryModel.setIs_sowing_done(object.optString("is_sowing_done"));
                            cropHistoryModel.setExpected_date_of_sowing(object.optString("expected_date_of_sowing"));
                            cropHistoryModel.setDate_of_sowing(object.optString("date_of_sowing"));
                            cropHistoryModel.setDate_of_harvest(object.optString("date_of_harvest"));
                            cropHistoryModel.setDate_of_season_end(object.optString("date_of_season_end"));
                            cropHistoryModel.setCrop_status(object.optString("crop_status"));
                            cropHistoryModels.add(cropHistoryModel);
                        }
                    }
                }
            }

            if (cropHistoryModels.size() > 0) {
                cardCropHistory.setVisibility(View.VISIBLE);
                CropHistoryAdapter cropHistoryAdapter = new CropHistoryAdapter(cropHistoryModels);
                rcvCropHistory.setAdapter(cropHistoryAdapter);
            } else {
                cardCropHistory.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class CropHistoryAdapter extends RecyclerView.Adapter<CropHistoryAdapter.ViewHolder> {
        private List<CropHistoryModel> listdata;

        public CropHistoryAdapter(List<CropHistoryModel> listdata) {
            this.listdata = listdata;
        }

        @NotNull
        @Override
        public CropHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.crop_history_list_item, parent, false);
            CropHistoryAdapter.ViewHolder viewHolder = new CropHistoryAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final CropHistoryAdapter.ViewHolder holder, final int position) {
            try {
                if (listdata.get(position).getCrop_id().equalsIgnoreCase("1")) {
                    holder.ivCrop.setImageResource(R.mipmap.maize);
                    holder.tvCropTitle.setText(R.string.str_maize);
                } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("2")) {
                    holder.ivCrop.setImageResource(R.mipmap.rice);
                    holder.tvCropTitle.setText(R.string.str_rice);
                } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("3")) {
                    holder.ivCrop.setImageResource(R.mipmap.cotton);
                    holder.tvCropTitle.setText(R.string.str_cotton);
                } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("4")) {
                    holder.ivCrop.setImageResource(R.mipmap.wheat);
                    holder.tvCropTitle.setText(R.string.str_wheat);
                } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("5")) {
                    holder.ivCrop.setImageResource(R.mipmap.okra);
                    holder.tvCropTitle.setText(R.string.str_okra);
                } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("6")) {
                    holder.ivCrop.setImageResource(R.mipmap.pearmillet);
                    holder.tvCropTitle.setText(R.string.str_pearlmillet);
                } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("7")) {
                    holder.ivCrop.setImageResource(R.mipmap.corinder);
                    holder.tvCropTitle.setText(R.string.str_coriander);
                } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("8")) {
                    holder.ivCrop.setImageResource(R.mipmap.bittergourd);
                    holder.tvCropTitle.setText(R.string.str_bittergourd);
                } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("9")) {
                    holder.ivCrop.setImageResource(R.mipmap.bottlegourd);
                    holder.tvCropTitle.setText(R.string.str_bottlegourd);
                } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("10")) {
                    holder.ivCrop.setImageResource(R.mipmap.watermelon);
                    holder.tvCropTitle.setText(R.string.str_watermelon);
                } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("11")) {
                    holder.ivCrop.setImageResource(R.mipmap.groundnut);
                    holder.tvCropTitle.setText(R.string.str_groundnut);
                } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("12")) {
                    holder.ivCrop.setImageResource(R.mipmap.soyabean);
                    holder.tvCropTitle.setText(R.string.str_soyabean);
                } else if (listdata.get(position).getCrop_id().equalsIgnoreCase("13")) {
                    holder.ivCrop.setImageResource(R.mipmap.caster);
                    holder.tvCropTitle.setText(R.string.str_castor);
                }

                if (!TextUtils.isEmpty(listdata.get(position).getDate_of_sowing())
                        && !listdata.get(position).getDate_of_sowing().equalsIgnoreCase("0000-00-00 00:00:00")) {
                    holder.tvTitle.setText(homeActivity.getResources().getText(R.string.str_buvay) + ": " + DateUtils.parseDateHindi(listdata.get(position).getDate_of_sowing(), "yyyy-MM-dd hh:mm:ss", "dd MMMM yyyy"));
                } else {
                    if (!TextUtils.isEmpty(listdata.get(position).getExpected_date_of_sowing())
                            && !listdata.get(position).getExpected_date_of_sowing().equalsIgnoreCase("0000-00-00 00:00:00")) {
                        holder.tvTitle.setText(homeActivity.getResources().getText(R.string.str_exp_buvay) + ": " + DateUtils.parseDateHindi(listdata.get(position).getExpected_date_of_sowing(), "yyyy-MM-dd hh:mm:ss", "dd MMMM yyyy"));

                    } else {
                        holder.tvTitle.setText(homeActivity.getResources().getText(R.string.str_buvay) + ": " + "-");
                    }
                }

                if (!TextUtils.isEmpty(listdata.get(position).getDate_of_harvest())
                        && !listdata.get(position).getDate_of_harvest().equalsIgnoreCase("0000-00-00 00:00:00")) {
                    holder.tvSubTitle.setText(homeActivity.getResources().getText(R.string.str_katai) + ": " + DateUtils.parseDateHindi(listdata.get(position).getDate_of_harvest(), "yyyy-MM-dd hh:mm:ss", "dd MMMM yyyy"));
                } else {
                    if (!TextUtils.isEmpty(listdata.get(position).getDate_of_season_end())
                            && !listdata.get(position).getDate_of_season_end().equalsIgnoreCase("0000-00-00 00:00:00")) {
                        holder.tvSubTitle.setText(homeActivity.getResources().getText(R.string.str_katai) + ": " + DateUtils.parseDateHindi(listdata.get(position).getDate_of_season_end(), "yyyy-MM-dd hh:mm:ss", "dd MMMM yyyy"));

                    } else {
                        holder.tvSubTitle.setText(homeActivity.getResources().getText(R.string.str_katai) + ": " + "-");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return listdata.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvTitle, tvCropTitle, tvSubTitle;
            ImageView ivCrop;

            public ViewHolder(View itemView) {
                super(itemView);
                this.ivCrop = (ImageView) itemView.findViewById(R.id.ivCrop);
                this.tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
                this.tvSubTitle = (TextView) itemView.findViewById(R.id.tvSubTitle);
                this.tvCropTitle = (TextView) itemView.findViewById(R.id.tvCropTitle);
            }
        }
    }

    void getPragatiData() {
        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
            RetrofitClient.getInstance().getApi().getCropPragatiTimeline(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    SharedPreferencesUtils.getUserId(homeActivity), sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                    Functions.getCommaSepPCropId(sharedPreferences)).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.isSuccessful()) {
                        try {
                            String r = response.body().string();
                            if (!TextUtils.isEmpty(r)) {
                                JSONObject jsonObject = new JSONObject(r);
                                if (jsonObject != null && jsonObject.length() > 0) {
                                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                        JSONObject data = jsonObject.optJSONObject("data");
                                        if (data != null && data.length() > 0) {
                                            JSONObject resultData = data.optJSONObject("result");
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Pragati, "" + resultData);
                                            setPragatiData();
                                        }
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }
    }


    void setWeeklyCaution(JSONObject weekly_caution) {
        try {
            List<PragatiSavadhaniData> pragatiSavadhaniData = new ArrayList<PragatiSavadhaniData>();
            if (weekly_caution != null && weekly_caution.length() > 0) {
                cardSavdhaniView.setVisibility(View.VISIBLE);
                JSONArray jsonArray = weekly_caution.optJSONArray(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""));
                if (jsonArray != null && jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jd = jsonArray.optJSONObject(i);
                        String name = jd.optString("display_text");
                        String keyword_id = jd.optString("keyword_id");
                        pragatiSavadhaniData.add(new PragatiSavadhaniData(name, keyword_id));
                    }
                }
                RogoAdapter rogoAdapter = new RogoAdapter(pragatiSavadhaniData);
                rcvRogo.setAdapter(rogoAdapter);
            } else {
                cardSavdhaniView.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class RogoAdapter extends RecyclerView.Adapter<RogoAdapter.ViewHolder> {
        private List<PragatiSavadhaniData> listdata;
        private long mLastLongClick = 0;

        public RogoAdapter(List<PragatiSavadhaniData> listdata) {
            this.listdata = listdata;
        }

        @NotNull
        @Override
        public RogoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.rogo_list_item, parent, false);
            RogoAdapter.ViewHolder viewHolder = new RogoAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final RogoAdapter.ViewHolder holder, final int position) {
            try {
                holder.tvTitle.setPaintFlags(holder.tvTitle.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                holder.tvTitle.setText(listdata.get(position).getName());
                holder.tvTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (Functions.doubleTapCheck(mLastLongClick)) {
                            return;
                        }
                        mLastLongClick = SystemClock.elapsedRealtime();

                        Intent intent = new Intent(homeActivity, SavdhaniDetailsActivity.class);
                        intent.putExtra("keyword_id", listdata.get(position).getKeyword_id());
                        startActivity(intent);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return listdata.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvTitle;

            public ViewHolder(View itemView) {
                super(itemView);
                this.tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            }
        }
    }


    void setPragatiData() {
        try {
            String pragatiData = sharedPreferences.getString(Constants.SharedPreferences_Pragati, null);
            if (!TextUtils.isEmpty(pragatiData)) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject result = new JSONObject(pragatiData);
                            if (result != null && result.length() > 0) {
                                String image_path = result.getString("image_path");

                                JSONObject weekly_caution = result.optJSONObject("weekly_caution");
                                setWeeklyCaution(weekly_caution);

                                int cropWeek = RongoApp.getCropWeeksFunction();
                                Log.v("P_CROP_ID: ", cropWeek + " - " + RongoApp.getSelectedPCrop());

                                List<PragatiTimelineData> pragatiTimelineData = new ArrayList<PragatiTimelineData>();
                                Object parentJson = new JSONTokener(result.optString("weekly_info")).nextValue();
                                if (parentJson != null) {
                                    JSONObject weekly_info = result.optJSONObject("weekly_info");
                                    if (weekly_info != null && weekly_info.length() > 0) {
                                        pragatiTimelineData = new ArrayList<PragatiTimelineData>();
                                        Object json = new JSONTokener(weekly_info.getString(RongoApp.getSelectedPCrop())).nextValue();
                                        if (json != null) {
                                            if (json instanceof JSONObject) {
                                                JSONObject jsonObject = weekly_info.optJSONObject(RongoApp.getSelectedPCrop());
                                                if (jsonObject != null && jsonObject.length() > 0) {
                                                    for (int i = 0; i <= cropWeek; i++) {
                                                        String weekly_crop_health = "", crop_info = "", is_true = "";
                                                        List<String> images = new ArrayList<String>();

                                                        JSONObject jd = jsonObject.optJSONObject(String.valueOf(i));
                                                        if (jd != null && jd.length() > 0) {
                                                            weekly_crop_health = jd.optString("weekly_crop_health");
                                                            crop_info = jd.optString("crop_info");
                                                            is_true = jd.optString("is_true");
                                                            Log.v("is_true Object1", "" + is_true);

                                                            images = new ArrayList<String>();
                                                            JSONArray advisory_image = jd.optJSONArray("images");
                                                            if (advisory_image != null && advisory_image.length() > 0) {
                                                                for (int j = 0; j < advisory_image.length(); j++) {
                                                                    images.add(advisory_image.optString(j));
                                                                }
                                                            }
                                                        } else {
                                                            is_true = "0";
                                                            images = new ArrayList<String>();
                                                        }

                                                        pragatiTimelineData.add(new PragatiTimelineData(String.valueOf(i), weekly_crop_health, crop_info, images, image_path, is_true));
                                                    }
                                                }
                                            } else if (json instanceof JSONArray) {
                                                JSONArray jsonArray = weekly_info.optJSONArray(RongoApp.getSelectedPCrop());
                                                if (jsonArray != null && jsonArray.length() > 0) {
                                                    for (int i = 0; i <= cropWeek; i++) {
                                                        String weekly_crop_health = "", crop_info = "", is_true = "";
                                                        List<String> images = new ArrayList<String>();

                                                        JSONObject jd = jsonArray.optJSONObject(i);
                                                        if (jd != null && jd.length() > 0) {
                                                            weekly_crop_health = jd.optString("weekly_crop_health");
                                                            crop_info = jd.optString("crop_info");
                                                            is_true = jd.optString("is_true");
                                                            Log.v("is_true Array1", "" + is_true);

                                                            images = new ArrayList<String>();
                                                            JSONArray advisory_image = jd.optJSONArray("images");
                                                            if (advisory_image != null && advisory_image.length() > 0) {
                                                                for (int j = 0; j < advisory_image.length(); j++) {
                                                                    images.add(advisory_image.optString(j));
                                                                }
                                                            }
                                                        } else {
                                                            is_true = "0";
                                                            images = new ArrayList<String>();
                                                        }

                                                        pragatiTimelineData.add(new PragatiTimelineData(String.valueOf(i), weekly_crop_health, crop_info, images, image_path, is_true));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                Log.v("pragatiTimeline Before: ", "" + pragatiTimelineData.size());
                                /*
                                 * TODO Offline Data Sync
                                 * */
                                if (pragatiTimelineData.size() > 0) {
                                    for (int j = 0; j < pragatiTimelineData.size(); j++) {
                                        List<OfflineWeeklyCropInfoData> weeklyCropInfoData = appDatabase.offlineWeeklyCropInfoDao().getAllData(String.valueOf(j), RongoApp.getSelectedPCrop());
                                        if (weeklyCropInfoData != null && weeklyCropInfoData.size() > 0) {
                                            Log.v("pragatiData 111", j + " - " + weeklyCropInfoData.get(0).getInfo_text());
                                            String crop_info = "";
                                            if (pragatiTimelineData.get(j).getCrop_info() != null && pragatiTimelineData.get(j).getCrop_info().length() > 0) {
                                                crop_info = getCropInfoData(pragatiTimelineData.get(j).getCrop_info(), weeklyCropInfoData);
                                            } else {
                                                JSONArray jsonArray = new JSONArray();
                                                for (int k = 0; k < weeklyCropInfoData.size(); k++) {
                                                    JSONObject obj = new JSONObject();
                                                    obj.put("info_type", weeklyCropInfoData.get(k).getInfo_type());
                                                    obj.put("info_text", weeklyCropInfoData.get(k).getInfo_text());
                                                    obj.put("info_value", weeklyCropInfoData.get(k).getInfo_value());
                                                    obj.put("info_metadata", weeklyCropInfoData.get(k).getInfo_metadata());
                                                    jsonArray.put(obj);
                                                }
                                                crop_info = jsonArray.toString();
                                            }
                                            pragatiTimelineData.get(j).setCrop_info(crop_info);
                                            pragatiTimelineData.get(j).setStatus("1");
                                        }

                                        List<LuckyDrawData> luckyDrawData = appDatabase.luckyDrawOfflineDao().getAllData(String.valueOf(j), RongoApp.getSelectedPCrop());
                                        if (luckyDrawData != null && luckyDrawData.size() > 0) {
                                            Log.v("pragatiData 222", j + " - " + luckyDrawData.get(0).getImagesPath());
                                            if (pragatiTimelineData.get(j).getImages() != null && pragatiTimelineData.get(j).getImages().size() > 0) {
                                                pragatiTimelineData.get(j).setImages(pragatiTimelineData.get(j).getImages());
                                                pragatiTimelineData.get(j).setImagePath(pragatiTimelineData.get(j).getImagePath());
                                            } else {
                                                pragatiTimelineData.get(j).setImages(luckyDrawData.get(0).getImagesPath());
                                                pragatiTimelineData.get(j).setImagePath("");
                                            }
                                            pragatiTimelineData.get(j).setStatus("1");
                                        }

                                        List<AdvisoryStatusData> advisoryStatusData = appDatabase.advisoryStatusDao().getAllData(String.valueOf(j), RongoApp.getSelectedPCrop());
                                        if (advisoryStatusData.size() > 0) {
                                            Log.v("pragatiData 333", j + " - " + advisoryStatusData.get(0).getIsDone() + " - " + advisoryStatusData.get(0).getIsHealth());
                                            if (TextUtils.isEmpty(pragatiTimelineData.get(j).getWeekly_crop_health())) {
                                                if (advisoryStatusData.get(0).getIsDone().equalsIgnoreCase("1")) {
                                                    pragatiTimelineData.get(j).setWeekly_crop_health(advisoryStatusData.get(0).getIsHealth());
                                                    pragatiTimelineData.get(j).setStatus("1");
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    for (int i = 0; i < cropWeek; i++) {
                                        List<String> imagesPath = null;
                                        String weekly_crop_health = "", crop_info = "";
                                        List<OfflineWeeklyCropInfoData> weeklyCropInfoData = appDatabase.offlineWeeklyCropInfoDao().getAllData(String.valueOf(i), RongoApp.getSelectedPCrop());
                                        if (weeklyCropInfoData != null && weeklyCropInfoData.size() > 0) {
                                            JSONArray jsonArray = new JSONArray();
                                            for (int k = 0; k < weeklyCropInfoData.size(); k++) {
                                                JSONObject obj = new JSONObject();
                                                obj.put("info_type", "" + weeklyCropInfoData.get(k).getInfo_type());
                                                obj.put("info_text", weeklyCropInfoData.get(k).getInfo_text());
                                                obj.put("info_value", weeklyCropInfoData.get(k).getInfo_value());
                                                obj.put("info_metadata", weeklyCropInfoData.get(k).getInfo_metadata());
                                                jsonArray.put(obj);
                                            }
                                            crop_info = jsonArray.toString();
                                        }

                                        List<LuckyDrawData> luckyDrawData = appDatabase.luckyDrawOfflineDao().getAllData(String.valueOf(i), RongoApp.getSelectedPCrop());
                                        if (luckyDrawData != null && luckyDrawData.size() > 0) {
                                            imagesPath = luckyDrawData.get(0).getImagesPath();
                                        }

                                        List<AdvisoryStatusData> advisoryStatusData = appDatabase.advisoryStatusDao().getAllData(String.valueOf(i), RongoApp.getSelectedPCrop());
                                        if (advisoryStatusData.size() > 0) {
                                            if (advisoryStatusData.get(0).getIsDone().equalsIgnoreCase("1")) {
                                                weekly_crop_health = advisoryStatusData.get(0).getIsHealth();
                                            }
                                        }

                                        pragatiTimelineData.add(new PragatiTimelineData(String.valueOf(i), weekly_crop_health, crop_info, imagesPath, "", "1"));
                                    }
                                }

                                Log.v("pragatiTimeline After: ", "" + pragatiTimelineData.size());
                                if (pragatiTimelineData.size() > 0) {
                                    tvNoData.setVisibility(View.GONE);
                                    PragatiAdapter contactsAdapter = new PragatiAdapter(pragatiTimelineData);
                                    rcvPragati.setAdapter(contactsAdapter);
                                } else {
                                    tvNoData.setVisibility(View.VISIBLE);
                                }
                                setShimmerGone();
                                cardPragatiView.setVisibility(View.VISIBLE);
                                setCropHistoryData();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            tvNoData.setVisibility(View.VISIBLE);
                            setShimmerGone();
                        }
                    }
                });
            } else {
                getPragatiData();
            }
        } catch (Exception e) {
            e.printStackTrace();
            tvNoData.setVisibility(View.VISIBLE);
            setShimmerGone();
        }
    }

    public String getCropInfoData(String cropData, List<OfflineWeeklyCropInfoData> weeklyCropInfoData) {
        String cropInfo = "";
        try {
            JSONArray jsonArray = new JSONArray();
            Object json = new JSONTokener(cropData).nextValue();
            if (json != null) {
                if (json instanceof JSONObject) {
                    JSONObject jsonObject = new JSONObject(cropData);
                    if (jsonObject != null && jsonObject.length() > 0) {
                        JSONObject obj = new JSONObject();
                        obj.put("info_type", "" + jsonObject.optString("info_type"));
                        obj.put("info_text", jsonObject.optString("info_text"));
                        obj.put("info_value", jsonObject.optString("info_value"));
                        obj.put("info_metadata", jsonObject.optString("info_metadata"));
                        jsonArray.put(obj);
                    }
                } else if (json instanceof JSONArray) {
                    JSONArray jArray = new JSONArray(cropData);
                    if (jArray != null && jArray.length() > 0) {
                        for (int i = 0; i < jArray.length(); i++) {
                            JSONObject jd = jArray.optJSONObject(i);
                            JSONObject obj = new JSONObject();
                            obj.put("info_type", "" + jd.optString("info_type"));
                            obj.put("info_text", jd.optString("info_text"));
                            obj.put("info_value", jd.optString("info_value"));
                            obj.put("info_metadata", jd.optString("info_metadata"));
                            jsonArray.put(obj);
                        }
                    }
                }
            }

            for (int k = 0; k < weeklyCropInfoData.size(); k++) {
                JSONObject obj = new JSONObject();
                obj.put("info_type", "" + weeklyCropInfoData.get(k).getInfo_type());
                obj.put("info_text", weeklyCropInfoData.get(k).getInfo_text());
                obj.put("info_value", weeklyCropInfoData.get(k).getInfo_value());
                obj.put("info_metadata", weeklyCropInfoData.get(k).getInfo_metadata());
                jsonArray.put(obj);
            }
            cropInfo = jsonArray.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cropInfo;
    }

    class PragatiAdapter extends RecyclerView.Adapter<PragatiAdapter.ViewHolder> {
        private List<PragatiTimelineData> listdata;
        private ArrayList<String> keyArray;

        public PragatiAdapter(List<PragatiTimelineData> listdata) {
            this.listdata = listdata;
        }

        @NotNull
        @Override
        public PragatiAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.pragati_list_item, parent, false);
            PragatiAdapter.ViewHolder viewHolder = new PragatiAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final PragatiAdapter.ViewHolder holder, final int position) {
            try {
//                holder.tvSaptah.setText(getResources().getText(R.string.saptah) + " " + (listdata.size() - (position + 1)));
                holder.tvSaptah.setText(getResources().getText(R.string.saptah) + " " + position);

                if (listdata.get(position).getStatus().equalsIgnoreCase("1")) {
                    holder.tvNoData.setVisibility(View.GONE);
                    if (!TextUtils.isEmpty(listdata.get(position).getCrop_info()) && !listdata.get(position).getCrop_info().equalsIgnoreCase("null")) {
                        Object json = new JSONTokener(listdata.get(position).getCrop_info()).nextValue();
                        if (json != null) {
                            if (json instanceof JSONObject) {
                                JSONObject jsonObject = new JSONObject(listdata.get(position).getCrop_info());
                                if (jsonObject != null && jsonObject.length() > 0) {
                                    String prefix = "", key = "", metadata = "";
                                    infoType = jsonObject.optString("info_type");
                                    if (!infoType.equalsIgnoreCase("sowing")) {
                                        StringBuilder sb = new StringBuilder();
                                        holder.tvDate.setVisibility(View.VISIBLE);

                                        metadata = jsonObject.optString("info_metadata");
                                        key = jsonObject.optString("info_text");

                                        if (infoType.equalsIgnoreCase("soil_type")) {
                                            metadata = jsonObject.optString("info_value");
                                        } else if (infoType.equalsIgnoreCase("spacing")) {
                                            metadata = jsonObject.optString("info_value");
                                        } else if (infoType.equalsIgnoreCase("yield")) {
                                            metadata = jsonObject.optString("info_value") + "" + getString(R.string.str_quiltan);
                                        } else if (infoType.equalsIgnoreCase("cost")) {
                                            metadata = getResources().getString(R.string.rupees_symbile) + "" + jsonObject.optString("info_value");
                                        } else if (infoType.equalsIgnoreCase("crop")) {
                                            metadata = jsonObject.optString("info_value");
                                        } else if (infoType.equalsIgnoreCase("land_preparation")) {
                                            metadata = jsonObject.optString("info_value");
                                        }

                                        sb.append(prefix);
                                        prefix = "\n";
                                        sb.append(Functions.getKeyName(sharedPreferences, position, infoType) + ": " + metadata);
                                        Log.v("sb", "" + sb);

                                        JSONObject WeeklyCropInfo = new JSONObject(RongoApp.getWeeklyCropQuestion());
                                        if (!TextUtils.isEmpty(WeeklyCropInfo.toString())) {
                                            JSONObject quesCrop = WeeklyCropInfo.optJSONObject(RongoApp.getSelectedCrop());
                                            JSONObject quesCrop1 = quesCrop.optJSONObject(RongoApp.getSeasonName());
                                            JSONArray jsonObjct = quesCrop1.optJSONArray(RongoApp.getDefaultLanguage());
                                            JSONObject object = jsonObjct.optJSONObject(0);
                                            JSONArray cropWeek = object.optJSONArray("crop_week");
                                            JSONArray jd = cropWeek.optJSONArray(position);
                                            String weekCropDays = jd.toString().replace("[", "").replace("]", "").replace(", ", ",");
                                            if (!TextUtils.isEmpty(weekCropDays)) {
                                                JSONObject quesCropWeek = null;
                                                String[] arrayStr = weekCropDays.split(",");
                                                for (String s : arrayStr) {
                                                    weekCropDay = s.replace("\"", "");
                                                    List<WeeklyCropInfo> weeklyCropInfoCheck = appDatabase.weeklyCropInfoDao().getWeeklyCropInfo(
                                                            String.valueOf(position), weekCropDay, "1", RongoApp.getSelectedPCrop());
                                                    if (weeklyCropInfoCheck.size() > 0) {
//                                                    holder.tvNoData.setVisibility(View.VISIBLE);
                                                    } else {
//                                                    holder.tvNoData.setVisibility(View.GONE);
                                                        quesCropWeek = Objects.requireNonNull(Functions.getWeeklyCropQuestion(sharedPreferences)).optJSONObject(weekCropDay);
                                                        if (!quesCropWeek.optString("info_type").equalsIgnoreCase("sowing") && !TextUtils.isEmpty(quesCropWeek.toString())) {
//                                                        Log.v("+++@@111+++: ", String.valueOf(position) + " = " + weekCropDay + " - " + quesCropWeek);
                                                            if (!quesCropWeek.getString("info_type").equalsIgnoreCase(infoType)) {
                                                                TextView view = new TextView(homeActivity);
                                                                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                                                params.setMargins(10, 2, 20, 10);
                                                                view.setLayoutParams(params);
                                                                view.setTag(position + "," + weekCropDay);
                                                                view.setText(quesCropWeek.getString("info_text") + getString(R.string.str_darj_kare_spec));

                                                                String originalText = (String) view.getText();
                                                                int startPosition = quesCropWeek.getString("info_text").length() + 2;
                                                                int endPosition;
                                                                if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.MARATHI)) {
                                                                    endPosition = startPosition + 15;
                                                                } else {
                                                                    endPosition = startPosition + 8;
                                                                }
                                                                SpannableString spannableStr = new SpannableString(originalText);
                                                                UnderlineSpan underlineSpan = new UnderlineSpan();
                                                                spannableStr.setSpan(underlineSpan, startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                                                                ForegroundColorSpan backgroundColorSpan = new ForegroundColorSpan(Color.rgb(255, 69, 0));
                                                                spannableStr.setSpan(backgroundColorSpan, startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                                                                spannableStr.setSpan(new RelativeSizeSpan(1.1f), startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

                                                                view.setText(spannableStr);
                                                                view.setOnClickListener(clickInLinearLayout());

                                                                LinearLayout layout2 = new LinearLayout(homeActivity);
                                                                layout2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                                                                layout2.setOrientation(LinearLayout.HORIZONTAL);
                                                                layout2.addView(view);
                                                                layout2.addView(getRewardView(position));
                                                                holder.llNewView.addView(layout2);
                                                            }

                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        holder.tvDate.setText("" + sb.toString());
                                    }
                                } else {
                                    holder.tvDate.setVisibility(View.GONE);
                                }
                            } else if (json instanceof JSONArray) {
                                JSONArray jsonArray = new JSONArray(listdata.get(position).getCrop_info());
                                if (jsonArray != null && jsonArray.length() > 0) {
                                    holder.tvDate.setVisibility(View.VISIBLE);
                                    StringBuilder sb = new StringBuilder();
                                    String prefix = "", key = "", metadata = "";
                                    keyArray = new ArrayList<>();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jd = jsonArray.optJSONObject(i);
                                        infoType = jd.optString("info_type");
                                        if (!infoType.equalsIgnoreCase("sowing")) {
                                            metadata = jd.optString("info_metadata");
                                            key = jd.optString("info_text");
                                            keyArray.add(infoType);

                                            if (infoType.equalsIgnoreCase("soil_type")) {
                                                metadata = jd.optString("info_value");
                                            } else if (infoType.equalsIgnoreCase("spacing")) {
                                                metadata = jd.optString("info_value");
                                            } else if (infoType.equalsIgnoreCase("yield")) {
                                                metadata = jd.optString("info_value") + "" + getString(R.string.str_quiltan);
                                            } else if (infoType.equalsIgnoreCase("cost")) {
                                                metadata = getResources().getString(R.string.rupees_symbile) + "" + jd.optString("info_value");
                                            } else if (infoType.equalsIgnoreCase("crop")) {
                                                metadata = jd.optString("info_value");
                                            } else if (infoType.equalsIgnoreCase("land_preparation")) {
                                                metadata = jd.optString("info_value");
                                            }

                                            sb.append(prefix);
                                            prefix = "\n";
                                            sb.append(Functions.getKeyName(sharedPreferences, position, infoType) + ": " + metadata);
                                        }
                                    }
                                    Log.v("sb1", "" + sb);

                                    JSONObject WeeklyCropInfo = new JSONObject(RongoApp.getWeeklyCropQuestion());
                                    if (!TextUtils.isEmpty(WeeklyCropInfo.toString())) {
                                        JSONObject quesCrop = WeeklyCropInfo.optJSONObject(RongoApp.getSelectedCrop());
                                        JSONObject quesCrop1 = quesCrop.optJSONObject(RongoApp.getSeasonName());
                                        JSONArray jsonObjct = quesCrop1.optJSONArray(RongoApp.getDefaultLanguage());
                                        JSONObject object = jsonObjct.optJSONObject(0);
                                        JSONArray cropWeek = object.optJSONArray("crop_week");
                                        JSONArray jd = cropWeek.optJSONArray(position);
                                        String weekCropDays = jd.toString().replace("[", "").replace("]", "").replace(", ", ",");
                                        if (!TextUtils.isEmpty(weekCropDays)) {
                                            JSONObject quesCropWeek = null;
                                            String[] arrayStr = weekCropDays.split(",");
                                            for (String s : arrayStr) {
                                                weekCropDay = s.replace("\"", "");
                                                List<WeeklyCropInfo> weeklyCropInfoCheck = appDatabase.weeklyCropInfoDao().getWeeklyCropInfo(
                                                        String.valueOf(position), weekCropDay, "1", RongoApp.getSelectedPCrop());
                                                if (weeklyCropInfoCheck.size() > 0) {
//                                                    holder.tvNoData.setVisibility(View.VISIBLE);
                                                } else {
//                                                    holder.tvNoData.setVisibility(View.GONE);
                                                    quesCropWeek = Objects.requireNonNull(Functions.getWeeklyCropQuestion(sharedPreferences)).optJSONObject(weekCropDay);
                                                    if (!quesCropWeek.optString("info_type").equalsIgnoreCase("sowing") && !TextUtils.isEmpty(quesCropWeek.toString())) {
//                                                        Log.v("+++@@222+++: ", String.valueOf(position) + " = " + weekCropDay + " - " + quesCropWeek);
//                                                        Log.v("+++@@222kkk+++: ", "" + keyArray);
                                                        if (!keyArray.contains(quesCropWeek.getString("info_type"))) {

                                                            TextView view = new TextView(homeActivity);
                                                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                                            params.setMargins(10, 2, 20, 10);
                                                            view.setLayoutParams(params);
                                                            view.setTag(position + "," + weekCropDay);
                                                            view.setText(quesCropWeek.getString("info_text") + getString(R.string.str_darj_kare_spec));

                                                            String originalText = (String) view.getText();
                                                            int startPosition = quesCropWeek.getString("info_text").length() + 2;
                                                            int endPosition;
                                                            if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.MARATHI)) {
                                                                endPosition = startPosition + 15;
                                                            } else {
                                                                endPosition = startPosition + 8;
                                                            }
                                                            SpannableString spannableStr = new SpannableString(originalText);
                                                            UnderlineSpan underlineSpan = new UnderlineSpan();
                                                            spannableStr.setSpan(underlineSpan, startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                                                            ForegroundColorSpan backgroundColorSpan = new ForegroundColorSpan(Color.rgb(255, 69, 0));
                                                            spannableStr.setSpan(backgroundColorSpan, startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                                                            spannableStr.setSpan(new RelativeSizeSpan(1.1f), startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

                                                            view.setText(spannableStr);
                                                            view.setOnClickListener(clickInLinearLayout());

                                                            LinearLayout layout2 = new LinearLayout(homeActivity);
                                                            layout2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                                                            layout2.setOrientation(LinearLayout.HORIZONTAL);
                                                            layout2.addView(view);
                                                            layout2.addView(getRewardView(position));
                                                            holder.llNewView.addView(layout2);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    holder.tvDate.setText("" + sb.toString());
                                } else {
                                    holder.tvDate.setVisibility(View.GONE);
                                }
                            }
                        } else {
                            holder.tvDate.setVisibility(View.GONE);
                        }
                    } else {
                        JSONObject WeeklyCropInfo = new JSONObject(RongoApp.getWeeklyCropQuestion());
                        if (!TextUtils.isEmpty(WeeklyCropInfo.toString())) {
                            JSONObject quesCrop = WeeklyCropInfo.optJSONObject(RongoApp.getSelectedCrop());
                            JSONObject quesCrop1 = quesCrop.optJSONObject(RongoApp.getSeasonName());
                            JSONArray jsonObject = quesCrop1.optJSONArray(RongoApp.getDefaultLanguage());
                            JSONObject object = jsonObject.optJSONObject(0);
                            JSONArray cropWeek = object.optJSONArray("crop_week");
                            JSONArray jd = cropWeek.optJSONArray(position);
                            String weekCropDays = jd.toString().replace("[", "").replace("]", "").replace(", ", ",");
                            if (!TextUtils.isEmpty(weekCropDays)) {
                                JSONObject quesCropWeek = null;
                                String[] arrayStr = weekCropDays.split(",");
                                for (String s : arrayStr) {
//                                    Log.e("++++",""+s);
                                    weekCropDay = s.replace("\"", "");
                                    List<WeeklyCropInfo> weeklyCropInfoCheck = appDatabase.weeklyCropInfoDao().getWeeklyCropInfo(
                                            String.valueOf(position), weekCropDay, "1", RongoApp.getSelectedPCrop());
                                    if (weeklyCropInfoCheck.size() > 0) {
//                                        holder.tvNoData.setVisibility(View.VISIBLE);
                                    } else {
                                        holder.tvNoData.setVisibility(View.GONE);
                                        quesCropWeek = Objects.requireNonNull(Functions.getWeeklyCropQuestion(sharedPreferences)).optJSONObject(weekCropDay);
//                                        Log.e("++1++",""+weeklyCropInfoCheck.size()+" - "+quesCropWeek);
                                        if (!quesCropWeek.optString("info_type").equalsIgnoreCase("sowing") && !TextUtils.isEmpty(quesCropWeek.toString())) {
//                                            Log.v("+++@@333+++: ", String.valueOf(position) + " = " + weekCropDay + " - " + quesCropWeek);
                                            TextView view = new TextView(homeActivity);
                                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                            params.setMargins(10, 2, 20, 10);
                                            view.setLayoutParams(params);
                                            view.setTag(position + "," + weekCropDay);
                                            view.setText(quesCropWeek.getString("info_text") + getString(R.string.str_darj_kare_spec));

                                            String originalText = (String) view.getText();
                                            int startPosition = quesCropWeek.getString("info_text").length() + 2;
                                            int endPosition;
                                            if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.MARATHI)) {
                                                endPosition = startPosition + 15;
                                            } else {
                                                endPosition = startPosition + 8;
                                            }

                                            SpannableString spannableStr = new SpannableString(originalText);
                                            UnderlineSpan underlineSpan = new UnderlineSpan();
                                            spannableStr.setSpan(underlineSpan, startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                                            ForegroundColorSpan backgroundColorSpan = new ForegroundColorSpan(Color.rgb(255, 69, 0));
                                            spannableStr.setSpan(backgroundColorSpan, startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                                            spannableStr.setSpan(new RelativeSizeSpan(1.1f), startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

                                            view.setText(spannableStr);
                                            view.setOnClickListener(clickInLinearLayout());

                                            LinearLayout layout2 = new LinearLayout(homeActivity);
                                            layout2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                                            layout2.setOrientation(LinearLayout.HORIZONTAL);
                                            layout2.addView(view);
                                            layout2.addView(getRewardView(position));
                                            holder.llNewView.addView(layout2);
                                        }
                                    }
                                }
                            } else {
                                holder.tvNoData.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                } else {
                    JSONObject WeeklyCropInfo = new JSONObject(RongoApp.getWeeklyCropQuestion());
//                    Log.e("WeeklyCropInfo: ", "" + WeeklyCropInfo);
                    if (!TextUtils.isEmpty(WeeklyCropInfo.toString())) {
                        JSONObject quesCrop = WeeklyCropInfo.optJSONObject(RongoApp.getSelectedCrop());
//                        Log.e("quesCrop: ", "" + WeeklyCropInfo);
                        JSONObject quesCrop1 = quesCrop.optJSONObject(RongoApp.getSeasonName());
                        JSONArray jsonObject = quesCrop1.optJSONArray(RongoApp.getDefaultLanguage());
                        JSONObject object = jsonObject.optJSONObject(0);
                        JSONArray cropWeek = object.optJSONArray("crop_week");
                        JSONArray jd = cropWeek.optJSONArray(position);
                        String weekCropDays = jd.toString().replace("[", "").replace("]", "").replace(", ", ",");
                        if (!TextUtils.isEmpty(weekCropDays)) {
                            JSONObject quesCropWeek = null;
                            String[] arrayStr = weekCropDays.split(",");
                            for (String s : arrayStr) {
                                weekCropDay = s.replace("\"", "");
                                List<WeeklyCropInfo> weeklyCropInfoCheck = appDatabase.weeklyCropInfoDao().getWeeklyCropInfo(
                                        String.valueOf(position), weekCropDay, "1", RongoApp.getSelectedPCrop());
                                if (weeklyCropInfoCheck.size() > 0) {
                                    holder.tvNoData.setVisibility(View.VISIBLE);
                                } else {
                                    holder.tvNoData.setVisibility(View.GONE);
                                    quesCropWeek = Objects.requireNonNull(Functions.getWeeklyCropQuestion(sharedPreferences)).optJSONObject(weekCropDay);
                                    if (!quesCropWeek.optString("info_type").equalsIgnoreCase("sowing") && !TextUtils.isEmpty(quesCropWeek.toString())) {
//                                        Log.v("+++@@444+++: ", String.valueOf(position) + " = " + weekCropDay + " - " + quesCropWeek);
                                        TextView view = new TextView(homeActivity);
                                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                        params.setMargins(10, 2, 20, 10);
                                        view.setLayoutParams(params);
                                        view.setTag(position + "," + weekCropDay);
                                        view.setText(quesCropWeek.optString("info_text") + getString(R.string.str_darj_kare_spec));

                                        String originalText = (String) view.getText();
                                        int startPosition = quesCropWeek.getString("info_text").length() + 2;
                                        int endPosition;
                                        if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.MARATHI)) {
                                            endPosition = startPosition + 15;
                                        } else {
                                            endPosition = startPosition + 8;
                                        }
                                        SpannableString spannableStr = new SpannableString(originalText);
                                        UnderlineSpan underlineSpan = new UnderlineSpan();
                                        spannableStr.setSpan(underlineSpan, startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                                        ForegroundColorSpan backgroundColorSpan = new ForegroundColorSpan(Color.rgb(255, 69, 0));
                                        spannableStr.setSpan(backgroundColorSpan, startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                                        spannableStr.setSpan(new RelativeSizeSpan(1.1f), startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

                                        view.setText(spannableStr);
                                        view.setOnClickListener(clickInLinearLayout());

                                        LinearLayout layout2 = new LinearLayout(homeActivity);
                                        layout2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                                        layout2.setOrientation(LinearLayout.HORIZONTAL);
                                        layout2.addView(view);
                                        layout2.addView(getRewardView(position));
                                        holder.llNewView.addView(layout2);
                                    }
                                }
                            }
                        } else {
                            holder.tvNoData.setVisibility(View.VISIBLE);
                        }
                    }
                }

                if (TextUtils.isEmpty(listdata.get(position).getWeekly_crop_health())) {
                    holder.tvTitle.setVisibility(View.GONE);
                } else {
                    holder.tvNoData.setVisibility(View.GONE);
                    holder.tvTitle.setVisibility(View.VISIBLE);
                    if (listdata.get(position).getWeekly_crop_health().equalsIgnoreCase("1")) {
                        holder.tvTitle.setText(getResources().getText(R.string.str_healthy) + "  " + getResources().getText(R.string.phasal) + " ");
                        holder.tvTitle.setTextColor(getResources().getColor(R.color.GREEN));
                    } else {
                        holder.tvTitle.setText(getResources().getText(R.string.str_unhealthy) + "  " + getResources().getText(R.string.phasal) + " ");
                        holder.tvTitle.setTextColor(getResources().getColor(R.color.unhealthy));
                    }

                    if (RongoApp.getCropWeekDaysFunction() == position) {
                        if (listdata.get(position).getImages().size() == 0 || listdata.get(position).getImages() == null) {
                            TextView view = new TextView(homeActivity);
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            params.setMargins(2, 0, 5, 0);
                            view.setLayoutParams(params);
                            view.setTag(position + "," + weekCropDay);
                            view.setText("" + getString(R.string.str_r_uploadPhoto));

                            String originalText = (String) view.getText();
                            int startPosition = 0;
                            int endPosition = startPosition + 10;
                            SpannableString spannableStr = new SpannableString(originalText);
                            UnderlineSpan underlineSpan = new UnderlineSpan();
                            spannableStr.setSpan(underlineSpan, startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                            ForegroundColorSpan backgroundColorSpan = new ForegroundColorSpan(Color.rgb(255, 69, 0));
                            spannableStr.setSpan(backgroundColorSpan, startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                            spannableStr.setSpan(new RelativeSizeSpan(1.1f), startPosition, endPosition, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

                            view.setText(spannableStr);
                            view.setOnClickListener(clickInPicLinearLayout());

                            LinearLayout layout2 = new LinearLayout(homeActivity);
                            layout2.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                            layout2.setOrientation(LinearLayout.HORIZONTAL);
                            layout2.addView(view);
                            layout2.addView(getRewardView(position));
                            holder.llPicView.addView(layout2);
                        }
                    }

                    if (listdata.get(position).getImages() != null) {
                        for (int i = 0; i < listdata.get(position).getImages().size(); i++) {

                            RoundRectCornerImageView imageView = new RoundRectCornerImageView(homeActivity);
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(180, 170);
                            params.setMargins(10, 2, 2, 10);
                            imageView.setLayoutParams(params);
                            imageView.setTag(i);
                            imageView.setBackgroundResource(R.drawable.placeholderbg);
                            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                            imageView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Integer pos = Integer.parseInt(v.getTag().toString());
                                    if (!TextUtils.isEmpty(listdata.get(position).getImagePath())) {
                                        ImageZoomDialog.openImages(homeActivity, listdata.get(position).getImagePath()
                                                + listdata.get(position).getImages().get(pos));
                                    }
                                }
                            });

                            if (!TextUtils.isEmpty(listdata.get(position).getImagePath())) {
                                Functions.displayImage(listdata.get(position).getImagePath() + listdata.get(position).getImages().get(i), imageView);
                            } else {
                                if (listdata.get(position).getImages().get(i).contains("storage")) {
                                    File imgFile = new File(listdata.get(position).getImages().get(i));
                                    if (imgFile.exists()) {
                                        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                                        imageView.setImageBitmap(myBitmap);
                                    }
                                }
                            }
                            holder.llImageView.addView(imageView);
                        }
                    }

//                    holder.llImageView.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
////                            openImages(listdata.get(position).getImagePath(), listdata.get(position).getImages());
//                        }
//                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return listdata.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvSaptah, tvTitle, tvDate, tvNoData;
            LinearLayout llImageView, llNewView, llPicView;

            public ViewHolder(View itemView) {
                super(itemView);
                this.llImageView = (LinearLayout) itemView.findViewById(R.id.llImageView);
                this.llNewView = (LinearLayout) itemView.findViewById(R.id.llNewView);
                this.llPicView = (LinearLayout) itemView.findViewById(R.id.llPicView);

                this.tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
                this.tvDate = (TextView) itemView.findViewById(R.id.tvDate);
                this.tvSaptah = (TextView) itemView.findViewById(R.id.tvSaptah);

                this.tvNoData = (TextView) itemView.findViewById(R.id.tvNoData);
            }
        }
    }

    public TextView getRewardView(int pos) {
        TextView view1 = new TextView(homeActivity);
        int cropWeek = RongoApp.getCropWeekDaysFunction();
        if (cropWeek == pos) {
            view1.setText(RewardPoints.getWeeklyCropInfoRewardPoint(sharedPreferences));
            view1.setBackgroundResource(R.mipmap.reward);
            view1.setTextSize(12f);
            view1.setTextColor(homeActivity.getResources().getColor(R.color.reward));
            view1.setGravity(Gravity.CENTER);
        }
        return view1;
    }

    View.OnClickListener clickInLinearLayout() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Integer position = Integer.parseInt(v.getTag().toString());
                openPragatiPending(v);
            }
        };
    }


    void openPragatiPending(View v) {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.open_pragati_timeline_pending, null);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setCanceledOnTouchOutside(false);

        try {
//            Log.v("Clicked item at position: ", "" + v.getTag());
            String str = v.getTag().toString();
            List<String> test = Arrays.asList(str.split(","));
            cropWeeks = test.get(0);
            cropDays = test.get(1);
            weekCropDay = cropDays;
//            Log.v("cropWeeks: ", "" + cropWeeks + " - " + cropDays);

            AppCompatTextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
            tvRewardNew.setText(RewardPoints.getWeeklyCropInfoRewardPoint(sharedPreferences));
            int current = RongoApp.getCropWeekDaysFunction();
            if (current == Integer.parseInt(cropWeeks)) {
                tvRewardNew.setVisibility(View.VISIBLE);
            } else {
                tvRewardNew.setVisibility(View.GONE);
            }

            tvQuestions = view.findViewById(R.id.tvQuestions);
            llButtonView = view.findViewById(R.id.llButtonView);
            tvHa = view.findViewById(R.id.tvHa);
            tvNa = view.findViewById(R.id.tvNa);
            llEditView = view.findViewById(R.id.llEditView);
            llSelectionView = view.findViewById(R.id.llSelectionView);
            llMultiSelection = view.findViewById(R.id.llMultiSelection);
            rlStateSpinner = view.findViewById(R.id.rlStateSpinner);
            ivWeeklyCropQues = view.findViewById(R.id.ivWeeklyCropQues);
            spinnerOptions = (Spinner) view.findViewById(R.id.spinnerOptions);
            tvDarjKare = view.findViewById(R.id.tvDarjKare);
            tvDarjKare1 = view.findViewById(R.id.tvDarjKare1);
            edtProduct = view.findViewById(R.id.edtProduct);
            ediOption1 = view.findViewById(R.id.ediOption1);
            rvOptions1 = view.findViewById(R.id.rvOptions1);
            rvOptions1.setLayoutManager(new LinearLayoutManager(homeActivity, RecyclerView.VERTICAL, false));

            JSONObject WeeklyCropInfo = new JSONObject(RongoApp.getWeeklyCropQuestion());
            if (!TextUtils.isEmpty(WeeklyCropInfo.toString())) {
                JSONObject quesCropWeek = Objects.requireNonNull(Functions.getWeeklyCropQuestion(sharedPreferences)).optJSONObject(cropDays);
                if (!TextUtils.isEmpty(quesCropWeek.toString())) {
//                    Log.v("+++@@555+++: ", String.valueOf(cropWeeks) + " = " + cropDays + " - " + quesCropWeek);

                    tvQuestions.setText(quesCropWeek.getString("question"));
                    inputType = "" + quesCropWeek.getString("input_type");
                    infoText = "" + quesCropWeek.getString("info_text");
                    infoType = "" + quesCropWeek.getString("info_type");

                    if (infoType.equalsIgnoreCase("fertilizer")) {
                        ivWeeklyCropQues.setVisibility(View.VISIBLE);
                        ivWeeklyCropQues.setImageResource(R.mipmap.ff);
                        titleText = getString(R.string.str_ques_text1);
                    } else if (infoType.equalsIgnoreCase("irrigation")) {
                        ivWeeklyCropQues.setVisibility(View.VISIBLE);
                        ivWeeklyCropQues.setImageResource(R.mipmap.ss);
                        titleText = getString(R.string.str_ques_text2);
                    } else if (infoType.equalsIgnoreCase("harvest")) {
                        ivWeeklyCropQues.setVisibility(View.VISIBLE);
                        ivWeeklyCropQues.setImageResource(R.mipmap.kt);
                        titleText = getString(R.string.str_ques_text3);
                    } else if (infoType.equalsIgnoreCase("flowering")) {
                        ivWeeklyCropQues.setVisibility(View.GONE);
                        titleText = getString(R.string.str_ques_text4);
                    } else if (infoType.equalsIgnoreCase("sowing")) {
                        ivWeeklyCropQues.setVisibility(View.GONE);
                        titleText = "";
                    } else if (infoType.equalsIgnoreCase("soil_type")) {
                        ivWeeklyCropQues.setVisibility(View.GONE);
                        titleText = "";
                    } else if (infoType.equalsIgnoreCase("spacing")) {
                        ivWeeklyCropQues.setVisibility(View.GONE);
                        titleText = "";
                    } else if (infoType.equalsIgnoreCase("yield")) {
                        ivWeeklyCropQues.setVisibility(View.GONE);
                        edtProduct.setHint(R.string.str_ques_text5);
                        titleText = "";
                    } else if (infoType.equalsIgnoreCase("cost")) {
                        ivWeeklyCropQues.setVisibility(View.GONE);
                        edtProduct.setHint(R.string.str_ques_text6);
                        titleText = "";
                    } else if (infoType.equalsIgnoreCase("crop")) {
                        ivWeeklyCropQues.setVisibility(View.GONE);
                        titleText = "";
                    } else if (infoType.equalsIgnoreCase("land_preparation")) {
                        edtProduct.setHint(homeActivity.getResources().getString(R.string.str_farm_area));
                    }

                    if (inputType.equalsIgnoreCase("button")) {
                        if (RongoApp.getSelectedCrop().equalsIgnoreCase("1")) {
                            ivWeeklyCropQues.setVisibility(View.VISIBLE);
                        }
                        llButtonView.setVisibility(View.VISIBLE);
                        llSelectionView.setVisibility(View.GONE);
                        llEditView.setVisibility(View.GONE);
                    } else if (inputType.equalsIgnoreCase("dropdown")) {
                        llButtonView.setVisibility(View.GONE);
                        ivWeeklyCropQues.setVisibility(View.GONE);
                        llSelectionView.setVisibility(View.VISIBLE);
                        llEditView.setVisibility(View.GONE);
                        rlStateSpinner.setVisibility(View.VISIBLE);
                        llMultiSelection.setVisibility(View.GONE);
                    } else if (inputType.equalsIgnoreCase("multi_dropdown")) {
                        llButtonView.setVisibility(View.GONE);
                        ivWeeklyCropQues.setVisibility(View.GONE);
                        llSelectionView.setVisibility(View.VISIBLE);
                        llEditView.setVisibility(View.GONE);
                        rlStateSpinner.setVisibility(View.GONE);
                        llMultiSelection.setVisibility(View.VISIBLE);
                    } else if (inputType.equalsIgnoreCase("text")) {
                        llButtonView.setVisibility(View.GONE);
                        ivWeeklyCropQues.setVisibility(View.GONE);
                        llSelectionView.setVisibility(View.GONE);
                        llEditView.setVisibility(View.VISIBLE);
                    }

                    tvDarjKare1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Functions.doubleTapCheck(mLastLongClick)) {
                                return;
                            }
                            mLastLongClick = SystemClock.elapsedRealtime();

                            if (InternetConnection.checkConnection(homeActivity)) {
                                if (TextUtils.isEmpty(edtProduct.getText().toString().trim())) {
                                    if (infoType.equalsIgnoreCase("land_preparation")) {
                                        Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_land_for_farming));
                                    } else {
                                        Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_enter_production));
                                    }
                                    return;
                                }

                                if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                    setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, edtProduct.getText().toString().trim(), getResources().getString(R.string.str_ha));
                                } else {
                                    updateWeeklyCropQues(weekCropDay, infoType, infoText, edtProduct.getText().toString().trim(), getResources().getString(R.string.str_ha));
                                }
                            } else {
                                setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, edtProduct.getText().toString().trim(), getResources().getString(R.string.str_ha));
                            }

                            bottomSheetDialog.dismiss();
                        }
                    });

                    ArrayList<String> listdata = new ArrayList<String>();
                    JSONArray cropWeek = quesCropWeek.optJSONArray("options");
                    if (cropWeek != null && cropWeek.length() > 0) {
                        for (int i = 0; i < cropWeek.length(); i++) {
                            listdata.add(cropWeek.getString(i));
                        }

                        if (inputType.equalsIgnoreCase("multi_dropdown")) {
                            multiOptionsAdapter = new MultiOptionsAdapter(listdata, PragatiFragment.this,
                                    "select_multiple", selectedOption, stringList, arrayList);
                            rvOptions1.setAdapter(multiOptionsAdapter);
                        } else {
                            spinnerOptions.setAdapter(new SpinnerArrayAdapter(homeActivity, R.layout.spinner_item_list, listdata));
                            spinnerOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    options = parent.getItemAtPosition(position).toString();
                                    Log.v("Selected Option: ", "" + options);
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                }
                            });
                        }
                    }

                    ediOption1.setCursorVisible(false);
                    ediOption1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View arg0) {
                            if (rvOptions1.getVisibility() == View.VISIBLE) {
                                rvOptions1.setVisibility(View.GONE);
                            } else {
                                rvOptions1.setVisibility(View.VISIBLE);
                            }
                        }
                    });

                    tvDarjKare.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Functions.doubleTapCheck(mLastLongClick)) {
                                return;
                            }
                            mLastLongClick = SystemClock.elapsedRealtime();

                            if (inputType.equalsIgnoreCase("multi_dropdown")) {
                                if (TextUtils.isEmpty(ediOption1.getText().toString().trim())) {
                                    Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_please_select_option));
                                    return;
                                }

                                if (rvOptions1.getVisibility() == View.VISIBLE) {
                                    rvOptions1.setVisibility(View.GONE);
                                }

                                if (InternetConnection.checkConnection(homeActivity)) {
                                    if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                        setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, TextUtils.join(",", stringList), getResources().getString(R.string.str_ha));
                                    } else {
                                        updateWeeklyCropQues(weekCropDay, infoType, infoText, TextUtils.join(",", stringList), getResources().getString(R.string.str_ha));
                                    }
                                } else {
                                    setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, TextUtils.join(",", stringList), getResources().getString(R.string.str_ha));
                                }
                            } else {
                                if (TextUtils.isEmpty(options)) {
                                    Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_please_select_option));
                                    return;
                                }

                                if (InternetConnection.checkConnection(homeActivity)) {
                                    if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                        setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, options, getResources().getString(R.string.str_ha));
                                    } else {
                                        updateWeeklyCropQues(weekCropDay, infoType, infoText, options, getResources().getString(R.string.str_ha));
                                    }
                                } else {
                                    setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, options, getResources().getString(R.string.str_ha));
                                }
                            }

                            bottomSheetDialog.dismiss();
                        }
                    });

                    tvHa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Functions.doubleTapCheck(mLastLongClick)) {
                                return;
                            }
                            mLastLongClick = SystemClock.elapsedRealtime();

                            if (InternetConnection.checkConnection(homeActivity)) {
                                if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                    setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, "", getResources().getString(R.string.str_ha));
                                } else {
                                    updateWeeklyCropQues(weekCropDay, infoType, infoText, "", getResources().getString(R.string.str_ha));
                                }
                            } else {
                                setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, "", getResources().getString(R.string.str_ha));
                            }

                            bottomSheetDialog.dismiss();
                        }
                    });

                    tvNa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Functions.doubleTapCheck(mLastLongClick)) {
                                return;
                            }
                            mLastLongClick = SystemClock.elapsedRealtime();

                            if (InternetConnection.checkConnection(homeActivity)) {
                                if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                    setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, "", getResources().getString(R.string.str_na));
                                } else {
                                    updateWeeklyCropQues(weekCropDay, infoType, infoText, "", getResources().getString(R.string.str_na));
                                }
                            } else {
                                setOfflineWeeklyCropInfo(weekCropDay, infoType, infoText, "", getResources().getString(R.string.str_na));
                            }

                            bottomSheetDialog.dismiss();
                        }
                    });
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));

        bottomSheetDialog.show();
    }

    public class MultiOptionsAdapter extends RecyclerView.Adapter<MultiOptionsAdapter.ViewHolder> {
        List<String> listdata;
        PragatiFragment growthFragment;
        String field_type = "", selectedStr = "";
        List<String> selectedStringsList = new ArrayList<>();
        List<String> selectedStringsListForNames;

        public MultiOptionsAdapter(List<String> listdata, PragatiFragment growthFragment, String fieldType, String selectedStr, List<String> selectedStringsList, List<String> selectedStringsListForNames) {
            this.listdata = listdata;
            this.growthFragment = growthFragment;
            this.field_type = fieldType;
            this.selectedStringsList = selectedStringsList;
            this.selectedStr = selectedStr;
            this.selectedStringsListForNames = selectedStringsListForNames;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.row_item_cropques_selection, parent, false);
            ViewHolder viewHolder = new ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            holder.textView.setText(listdata.get(position));

            if (field_type.equalsIgnoreCase("select")) {
                if (listdata.get(position).equalsIgnoreCase(selectedStr)) {
                    holder.imgSelection.setImageResource(R.mipmap.uria_selected);
                } else {
                    holder.imgSelection.setImageResource(R.mipmap.uria_unselected);
                }

            } else if (field_type.equalsIgnoreCase("select_multiple")) {
                if (selectedStringsList.contains(position + "")) {
                    holder.imgSelection.setImageResource(R.mipmap.uria_selected);
                } else {
                    holder.imgSelection.setImageResource(R.mipmap.uria_unselected);
                }
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (field_type.equalsIgnoreCase("select")) {
//                        singleSelection(listdata.get(position));
                    } else if (field_type.equalsIgnoreCase("select_multiple")) {
                        multipleSelection(listdata.get(position), position + "");
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return listdata.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public AppCompatTextView textView;
            ImageView imgSelection;

            public ViewHolder(View itemView) {
                super(itemView);
                this.textView = itemView.findViewById(R.id.checkbox);
                this.imgSelection = itemView.findViewById(R.id.imgSelection);
            }
        }

        public void multipleSelection(String myStr, String position) {
            selectedStr = position;
            if (selectedStringsList.contains(selectedStr)) {
                selectedStringsList.remove(selectedStr);
            } else {
                selectedStringsList.add(selectedStr);
            }

            if (selectedStringsListForNames.contains(myStr)) {
                selectedStringsListForNames.remove(myStr);
            } else {
                selectedStringsListForNames.add(myStr);
            }

            growthFragment.selectedMultiple(selectedStringsList, selectedStringsListForNames);
            android.util.Log.e("Selected MultiOption", "" + selectedStringsListForNames);
            notifyDataSetChanged();
        }

    }

    public void selectedMultiple(List<String> stringList, List<String> stringListNames) {
        this.arrayList = stringList;
        this.stringList = stringListNames;
        ediOption1.setText(TextUtils.join(",", stringListNames));
    }

    void setOfflineWeeklyCropInfo(final String id, String infoType, String infoText, String infoValue, String metaData) {
        try {
            cropWeeks = String.valueOf(Integer.parseInt(cropWeeks));
            List<OfflineWeeklyCropInfoData> weeklyCropInfoArray = new ArrayList<OfflineWeeklyCropInfoData>();
            OfflineWeeklyCropInfoData weeklyCropInfo = new OfflineWeeklyCropInfoData();
            weeklyCropInfo.set_id(new Random().nextInt());
            weeklyCropInfo.setUser_id(SharedPreferencesUtils.getUserId(homeActivity));
            weeklyCropInfo.setP_crop_id(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""));
            weeklyCropInfo.setSeason_id(sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""));
            weeklyCropInfo.setCrop_week(cropWeeks);
            weeklyCropInfo.setCrop_id(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""));
            weeklyCropInfo.setField_id(sharedPreferences.getString(Constants.SharedPreferences_Field_ID, ""));
            weeklyCropInfo.setInfo_type(infoType);
            weeklyCropInfo.setInfo_text(infoText);
            weeklyCropInfo.setInfo_value(infoValue);
            weeklyCropInfo.setInfo_metadata(metaData);
            weeklyCropInfoArray.add(weeklyCropInfo);
            appDatabase.offlineWeeklyCropInfoDao().insertAll(weeklyCropInfoArray);

            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");

            List<WeeklyCropInfo> infoList = new ArrayList<WeeklyCropInfo>();
            WeeklyCropInfo cropInfo = new WeeklyCropInfo();
            cropInfo.set_id(new Random().nextInt());
            cropInfo.setCropDay(id);
            cropInfo.setCropWeek(cropWeeks);
            cropInfo.setCropId(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""));
            cropInfo.setPcropId(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""));
            cropInfo.setIsDone("1");
            cropInfo.setStatus("1");
            cropInfo.setInfoTypo(infoType);
            infoList.add(cropInfo);
            appDatabase.weeklyCropInfoDao().insertAll(infoList);

            openCropInfoDhanyawad();
            Functions.updateRewardPoints(sharedPreferences, RewardPoints.getWeeklyCropInfoRewardPoint(sharedPreferences));

            if (infoType.equalsIgnoreCase("crop")) {
                SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                        Constants.SharedPreferences_crop_status, "ended");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void updateWeeklyCropQues(final String id, String infoType, String infoText, String infoValue, String metaData) {
        cropWeeks = String.valueOf(Integer.parseInt(cropWeeks));
        RetrofitClient.getInstance().getApi().updateWeeklyCropInfo(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                RongoApp.getAppVersion(), Functions.getDeviceid(homeActivity), SharedPreferencesUtils.getUserId(homeActivity),
                sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                cropWeeks, sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                sharedPreferences.getString(Constants.SharedPreferences_Field_ID, ""),
                infoType, infoText, infoValue, metaData).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");

                    List<WeeklyCropInfo> infoList = new ArrayList<WeeklyCropInfo>();
                    WeeklyCropInfo cropInfo = new WeeklyCropInfo();
                    cropInfo.set_id(new Random().nextInt());
                    cropInfo.setCropDay(id);
                    cropInfo.setCropWeek(cropWeeks);
                    cropInfo.setCropId(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""));
                    cropInfo.setPcropId(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""));
                    cropInfo.setIsDone("1");
                    cropInfo.setStatus("1");
                    cropInfo.setInfoTypo(infoType);
                    infoList.add(cropInfo);
                    appDatabase.weeklyCropInfoDao().insertAll(infoList);

                    openCropInfoDhanyawad();
                    Functions.updateRewardPoints(sharedPreferences, RewardPoints.getWeeklyCropInfoRewardPoint(sharedPreferences));

                    if (infoType.equalsIgnoreCase("crop")) {
                        if (!InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                            RongoApp.getProfile();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void openCropInfoDhanyawad() {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_crop_info_dhanyawad, null);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setCanceledOnTouchOutside(false);

        if (InternetConnection.checkConnectionForFasl(homeActivity) &&
                !InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
            getPragatiData();
        } else {
            setPragatiData();
        }

        LinearLayout llRewardView = view.findViewById(R.id.llRewardView);
        int cropWeek = RongoApp.getCropWeekDaysFunction();
        if (cropWeek == Integer.parseInt(cropWeeks)) {
            llRewardView.setVisibility(View.VISIBLE);
        } else {
            llRewardView.setVisibility(View.GONE);
        }

        AppCompatTextView tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText(RewardPoints.getWeeklyCropInfoRewardPoint(sharedPreferences));

        AppCompatTextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));

        AppCompatTextView txtOfflineSMS = view.findViewById(R.id.txtOfflineSMS);
        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
            txtOfflineSMS.setVisibility(View.GONE);
        } else {
            txtOfflineSMS.setVisibility(View.VISIBLE);
        }

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        AppCompatTextView tvShareWhatapp = view.findViewById(R.id.tvShareWhatapp);
        tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "WhatsApp");
                    Functions.shareWhatAppIntent(homeActivity);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(homeActivity, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        AppCompatTextView tvShareCopy = view.findViewById(R.id.tvShareCopy);
        tvShareCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "SMS");
                    Functions.shareSMSIntent(homeActivity, RongoApp.getReferralText(homeActivity));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AppCompatTextView tvSharefacebook = view.findViewById(R.id.tvSharefacebook);
        tvSharefacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.setUserScreenTrackEvent(appDatabase, "Invite", "Facebook");
                Functions.shareFacebookIntent(homeActivity);
            }
        });

        AppCompatTextView tvShareMore = view.findViewById(R.id.tvShareMore);
        tvShareMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.performShare(homeActivity, appDatabase);
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));

        bottomSheetDialog.show();
    }

    View.OnClickListener clickInPicLinearLayout() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Integer position = Integer.parseInt(v.getTag().toString());
                openCrops();
            }
        };
    }


    void openCrops() {
        bottomSheetDialogCrop = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_lucky_draw_crops, null);
        bottomSheetDialogCrop.setContentView(view);
        bottomSheetDialogCrop.setCancelable(false);
        bottomSheetDialogCrop.setCanceledOnTouchOutside(false);

        MaterialCardView cardHealthlly = (MaterialCardView) view.findViewById(R.id.cardHealthlly);
        cardHealthlly.setVisibility(View.VISIBLE);

        LinearLayout llUploadView = (LinearLayout) view.findViewById(R.id.llUploadView);
        llUploadView.setVisibility(View.GONE);
        AppCompatTextView tvPoint = view.findViewById(R.id.tvPoint);
        tvPoint.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences));

        AppCompatTextView tvUpload = view.findViewById(R.id.tvUpload);
        tvUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardHealthlly.setVisibility(View.GONE);
                llUploadView.setVisibility(View.VISIBLE);
            }
        });

        AppCompatTextView tvPointsUploadPicture = view.findViewById(R.id.tvPointsUploadPicture);
        tvPointsUploadPicture.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences) + " " + getString(R.string.str_point_earned));

        RecyclerView rvCrops = view.findViewById(R.id.rvCrops);
        rvCrops.setLayoutManager(new LinearLayoutManager(homeActivity, RecyclerView.HORIZONTAL, false));
        rvCrops.setNestedScrollingEnabled(false);
        rvCrops.setHasFixedSize(true);
        List<CropAreaFocusedData> cropAreaFocusedData = SharedPreferencesUtils.getCropArea(homeActivity, Constants.SharedPreferences_CROP_FOCUSED_AREA);
        if (cropAreaFocusedData != null && cropAreaFocusedData.size() > 0) {
            rvCrops.setAdapter(new CropAreaForPragatiAdapter(pragatiFragment, cropAreaFocusedData, mSelectedList));
        }

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogCrop.dismiss();
            }
        });

        ImageView imgNext = view.findViewById(R.id.nextImg);
        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rvCrops.post(new Runnable() {
                    @Override
                    public void run() {
                        if (cropAreaFocusedData != null && cropAreaFocusedData.size() > 0) {
                            rvCrops.scrollToPosition(cropAreaFocusedData.size() - 1);
                        }
                    }
                });
            }
        });

        AppCompatTextView txtUpload = view.findViewById(R.id.txtUpload);
        txtUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialogCrop.dismiss();
                accessPermission();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(homeActivity.getResources().getColor(android.R.color.transparent));
        bottomSheetDialogCrop.show();
    }

    public void getSelectedCropArea(CropAreaFocusedData mCropAreaFocusedData) {
        this.mSelectedList = mCropAreaFocusedData;
    }

    void openThumbnailDialog() {
        mCurrentPhotoPath = "";
        BottomSheetDialog bottomSheetThumbnail = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_upload_more_photos, null);
        bottomSheetThumbnail.setContentView(view);

        AppCompatTextView tvPointsUploadPicture = view.findViewById(R.id.tvPointsUploadPicture);
        tvPointsUploadPicture.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences) + getString(R.string.str_point_earned));
        ImageView imgNext = view.findViewById(R.id.imgNext);
        ProgressBar pb = view.findViewById(R.id.pb);
        pb.setVisibility(View.GONE);
        LinearLayout linearLuckYDraw = view.findViewById(R.id.linearLuckYDraw);
        linearLuckYDraw.setVisibility(View.VISIBLE);
        bottomSheetThumbnail.setCanceledOnTouchOutside(false);
        bottomSheetThumbnail.setCancelable(false);

        if (imagesPath.size() == 1) {
            imgNext.setVisibility(View.GONE);
        } else {
            imgNext.setVisibility(View.VISIBLE);
        }

        RecyclerView rvAllPhotos = view.findViewById(R.id.rvAllPhotos);
        rvAllPhotos.setLayoutManager(new LinearLayoutManager(homeActivity, RecyclerView.HORIZONTAL, false));
        rvAllPhotos.setAdapter(new ThumbnailsAdapter(imagesPath));
        rvAllPhotos.setNestedScrollingEnabled(false);

        AppCompatTextView txtUploadMore = view.findViewById(R.id.txtUploadMore);
        AppCompatTextView txtGoOn = view.findViewById(R.id.txtGoOn);
        txtGoOn.setText(getResources().getString(R.string.str_darj_kare));

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetThumbnail.dismiss();

                if (bottomSheetDialogCrop != null) {
                    bottomSheetDialogCrop.show();
                }

                if (imagesPath != null && imagesPath.size() > 0) {
                    imagesPath.clear();
                }
            }
        });

        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rvAllPhotos.post(new Runnable() {
                    @Override
                    public void run() {
                        if (imagesPath != null && imagesPath.size() > 0) {
                            rvAllPhotos.scrollToPosition(imagesPath.size() - 1);
                        }
                    }
                });
            }
        });

        txtUploadMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetThumbnail.dismiss();
                accessPermission();
            }
        });

        txtGoOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                if (imagesPath.size() > 0) {
                    List<AdvisoryPhotoData> advisoryPhotoData = new ArrayList<AdvisoryPhotoData>();
                    advisoryPhotoData.add(new AdvisoryPhotoData(0, sharedPreferences.getString(Constants.SharedPreferences_USER_CROP_WEEK, "0"),
                            RongoApp.getSelectedCrop(), RongoApp.getSelectedPCrop(), "1"));
                    appDatabase.advisoryPhotoDao().insertAll(advisoryPhotoData);
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                            if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
                                setOfflineData(bottomSheetThumbnail);
                            } else {
                                uploadWeeklyPicture(bottomSheetThumbnail);
                            }
                        } else {
                            setOfflineData(bottomSheetThumbnail);
                        }
                    }
                }, 100);
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetThumbnail.show();
    }

    void setOfflineData(BottomSheetDialog bottomSheetThumbnail) {
        try {
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_WeeklyUploadPhoto, "Y");
            String focus_area = "";
            if (mSelectedList != null) {
                focus_area = mSelectedList.getType();
            }
            String finalFocus_area = focus_area;
            Executors.newSingleThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    List<LuckyDrawData> luckyDrawData = new ArrayList<>();
                    luckyDrawData.add(new LuckyDrawData(0, sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""),
                            sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""),
                            sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""),
                            sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""),
                            finalFocus_area, "", "", imagesPath));
                    appDatabase.luckyDrawOfflineDao().insertAll(luckyDrawData);
                }
            });
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
//                    progressBar.setVisibility(View.GONE);
                    mSelectedList = null;
                    mCurrentPhotoPath = "";
                    imagesPath = new ArrayList<>();
                    bottomSheetThumbnail.dismiss();
                    openSuccessForHealthy();
                }
            }, 500);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void uploadWeeklyPicture(BottomSheetDialog bottomSheetThumbnail) {
        String focus_area = "";
        if (mSelectedList != null) {
            focus_area = mSelectedList.getType();
        }
        RequestBody requestBodyId = RequestBody.create(SharedPreferencesUtils.getUserId(homeActivity).trim(), MediaType.parse("text/plain"));
        RequestBody seasonIdBody = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyCropId = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyPCropId = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyCropWeek = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyDeviceId = RequestBody.create(Functions.getDeviceid(homeActivity), MediaType.parse("text/plain"));
        RequestBody requestBodyFCMToken = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyPictureArea = RequestBody.create(focus_area, MediaType.parse("text/plain"));
        RequestBody requestBodylatitude = RequestBody.create("", MediaType.parse("text/plain"));
        RequestBody requestBodylongitude = RequestBody.create("", MediaType.parse("text/plain"));
        RequestBody requestBodyAppVersion = RequestBody.create(RongoApp.getAppVersion(), MediaType.parse("text/plain"));
        RequestBody requestBodyAppLanguage = RequestBody.create(RongoApp.getDefaultLanguage(), MediaType.parse("text/plain"));
        RequestBody requestBodyAppSeason = RequestBody.create(RongoApp.getSeasonName(), MediaType.parse("text/plain"));

        Map<String, RequestBody> params = new HashMap<>();
        params.put("user_id", requestBodyId);
        params.put("season_id", seasonIdBody);
        params.put("crop_id", requestBodyCropId);
        params.put("p_crop_id", requestBodyPCropId);
        params.put("crop_week", requestBodyCropWeek);
        params.put("picture_area_focused", requestBodyPictureArea);
        params.put("geo_latitude", requestBodylatitude);
        params.put("geo_longitude", requestBodylongitude);
        params.put("device_id", requestBodyDeviceId);
        params.put("fcm_token", requestBodyFCMToken);
        params.put("app_version", requestBodyAppVersion);
        params.put("app_language", requestBodyAppLanguage);
        params.put("season_name", requestBodyAppSeason);

        if (imagesPath != null && imagesPath.size() > 0) {
            RequestBody requestBodyImageUploadedPath = RequestBody.create(imagesPath.toString(), MediaType.parse("text/plain"));
            params.put("picture_key_local", requestBodyImageUploadedPath);

            for (int i = 0; i < imagesPath.size(); i++) {
                File file = new File(imagesPath.get(i).toString().trim());
                try {
                    File compressedImages = new ImageZipper(homeActivity).compressToFile(file);
                    params.put("picture[]" + "\"; filename=\"" + file.getName(), Functions.getRequestFile(compressedImages));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        RetrofitClient.getInstance().getApi().uploadWeeklyPicture(params).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String str = response.body().string();
                        if (!TextUtils.isEmpty(str)) {
                            JSONObject jsonObject = new JSONObject(str);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_WeeklyUploadPhoto, "Y");
                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");

                                    mSelectedList = null;
                                    mCurrentPhotoPath = "";
                                    imagesPath = new ArrayList<>();
                                    bottomSheetThumbnail.dismiss();
                                    openSuccessForHealthy();
                                }
                            }
                        }
//                        progressBar.setVisibility(View.GONE);
                    } catch (Exception e) {
                        e.printStackTrace();
//                        progressBar.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                progressBar.setVisibility(View.GONE);
            }
        });
    }

    void accessPermission() {
        Dexter.withContext(homeActivity)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    void showImagePickerOptions() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(homeActivity);
        View sheetView = homeActivity.getLayoutInflater().inflate(R.layout.bottom_photo_uplaod_options, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.setCanceledOnTouchOutside(false);

        LinearLayout linearCamera = sheetView.findViewById(R.id.linearCamera);
        LinearLayout linearGallery = sheetView.findViewById(R.id.linearGallery);
        sheetView.findViewById(R.id.imgClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                if (bottomSheetDialogCrop != null) {
                    bottomSheetDialogCrop.show();
                }
            }
        });

        linearCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                dispatchTakePictureIntent();
            }
        });

        linearGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, GALLERY_REQUEST_CODE);
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        ((View) sheetView.getParent()).setBackgroundColor(homeActivity.getResources().getColor(android.R.color.transparent));
        mBottomSheetDialog.show();
    }

    void dispatchTakePictureIntent() {
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            File photoFile = Functions.createImageFile();
            if (photoFile != null) {
                mCurrentPhotoPath = photoFile.getAbsolutePath();
                Uri photoURI = FileProvider.getUriForFile(homeActivity, providerAuthority, photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                List<ResolveInfo> resolvedIntentActivities = homeActivity.getPackageManager().queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
                for (ResolveInfo resolvedIntentInfo : resolvedIntentActivities) {
                    String packageName = resolvedIntentInfo.activityInfo.packageName;
                    homeActivity.grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                }
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.POST_QUESTION_CODE && resultCode == Activity.RESULT_OK) {
            Functions.openPostQuestionSuccessDialog(homeActivity, appDatabase,
                    RewardPoints.getPostQuestionRewardPoint(sharedPreferences), RewardPoints.getInviteRewardPoint(sharedPreferences));
        }
        if (requestCode == CAMERA_REQUEST_CODE || requestCode == GALLERY_REQUEST_CODE) {
            try {
                if (requestCode == CAMERA_REQUEST_CODE && resultCode == homeActivity.RESULT_OK) {
                    if (data != null && data.hasExtra("mCurrentPhotoPath")) {
                        mCurrentPhotoPath = data.getStringExtra("mCurrentPhotoPath");
                    }
                    File f = new File(mCurrentPhotoPath);
                    Uri contentUri = Uri.fromFile(f);
                    mCurrentPhotoPath = contentUri.getPath();
                }

                if (requestCode == GALLERY_REQUEST_CODE && resultCode == homeActivity.RESULT_OK) {
                    Uri selectedImage = data.getData();
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = homeActivity.getContentResolver().query(selectedImage, filePath, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    mCurrentPhotoPath = c.getString(columnIndex);
                    c.close();
                    if (mCurrentPhotoPath == null) {
                        Bitmap googlepath = Functions.getBitmapFromUri(selectedImage, homeActivity);
                        mCurrentPhotoPath = Functions.getGooglePhotoImagePath(googlepath, homeActivity);
                    }
                    if (mCurrentPhotoPath.contains("/root")) {
                        mCurrentPhotoPath = mCurrentPhotoPath.replace("/root", "");
                    }
                }

                if (mCurrentPhotoPath != null && !TextUtils.isEmpty(mCurrentPhotoPath)) {
                    imagesPath.add(mCurrentPhotoPath);
                    openThumbnailDialog();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    void openSuccessForHealthy() {
        getPragatiData();
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_success_for_healthy, null);
        bottomSheetDialog.setContentView(view);

        AppCompatTextView txtOfflineSMS = view.findViewById(R.id.txtOfflineSMS);
        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
            txtOfflineSMS.setVisibility(View.GONE);
        } else {
            txtOfflineSMS.setVisibility(View.VISIBLE);
        }

        AppCompatTextView tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText(RewardPoints.getUploadPictureRewardPoint(sharedPreferences));

        AppCompatTextView tvRewardNew = view.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));

        AppCompatTextView tvShareWhatapp = view.findViewById(R.id.tvShareWhatapp);
        tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "WhatsApp");
                    Functions.shareWhatAppIntent(homeActivity);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(homeActivity, "" + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        AppCompatTextView tvShareCopy = view.findViewById(R.id.tvShareCopy);
        tvShareCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Functions.setUserScreenTrackEvent(appDatabase, "Invite", "SMS");
                    Functions.shareSMSIntent(homeActivity, RongoApp.getReferralText(homeActivity));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        AppCompatTextView tvSharefacebook = view.findViewById(R.id.tvSharefacebook);
        tvSharefacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.setUserScreenTrackEvent(appDatabase, "Invite", "Facebook");
                Functions.shareFacebookIntent(homeActivity);
            }
        });

        AppCompatTextView tvShareMore = view.findViewById(R.id.tvShareMore);
        tvShareMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.performShare(homeActivity, appDatabase);
            }
        });

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

}