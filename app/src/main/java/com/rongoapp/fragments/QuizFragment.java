package com.rongoapp.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.card.MaterialCardView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.makeramen.roundedimageview.RoundedImageView;
import com.rongoapp.R;
import com.rongoapp.activities.HomeActivity;
import com.rongoapp.activities.RewardActivity;
import com.rongoapp.activities.SavdhaniDetailsActivity;
import com.rongoapp.audioPlayerTTS.Speakerbox;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.FetchJsonFromAssetsFile;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.AdvisoryStatusData;
import com.rongoapp.data.DailyQuizStatusData;
import com.rongoapp.data.LuckyDrawData;
import com.rongoapp.data.OfflineQuizData;
import com.rongoapp.data.OfflineWeeklyCropInfoData;
import com.rongoapp.data.PragatiTimelineData;
import com.rongoapp.data.QuizAnswerListModel;
import com.rongoapp.data.StateCityDataModel;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.Log;
import com.rongoapp.views.ProgressBarDialog;
import com.rongoapp.views.SuccessGifView;
import com.rongoapp.waveanimation.AXLineWaveView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuizFragment extends DialogFragment implements View.OnTouchListener, View.OnDragListener {

    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    Speakerbox speakerbox;
    View view;
    Activity activity;
    AXLineWaveView line_wave;

    ArrayList<StateCityDataModel> stateCityDataModels = new ArrayList<StateCityDataModel>();
    ArrayList<QuizAnswerListModel> quizAnswerListModels = new ArrayList<>();
    HashMap<String, String> reOrderPosList = new HashMap<String, String>();
    HashMap<String, String> crossPosList = new HashMap<String, String>();
    HashMap<String, String> dragPosList = new HashMap<String, String>();
    ArrayList<Drawable> imagesList = new ArrayList<>();
    ArrayList<String> answersList = new ArrayList<>();
    ArrayList<Bitmap> chunkedImages = new ArrayList<Bitmap>();
    boolean isAnswerValid = false;
    JSONArray imageArray, optionsArray;
    String question = "", isAnswer = "", type = "", keyword_id = "", QuizId = "", totalAns = "";

    RelativeLayout rlDrawView, rlLeaderBoard;
    Button btnNext;
    ProgressBar pbAnswer, pb9Puzzle, pb2Puzzle, pbColor, pbMisPuzzle, pbReorder, pbFillBlank, pbCrossPuzzle;
    RoundedImageView ivQuestion, iv2Puzzle, ivColor, ivFillBlankQuestion, ivReorder1, ivReorder2, ivReorder3,
            ivReorder11, ivReorder22, ivReorder33, ivFillBlankAnswer1, ivFillBlankAnswer2;
    ImageView ivBack, iv9Puzzle, ivMisPuzzle1, ivMisPuzzle2, ivAnswerMic, ivRightAnswer, ivWrongAnswer, ivOne, ivTwo, ivThree,
            ivFour, ivFive, ivSix, ivSeven, ivEight, ivNine, iv9PuzzleMic, iv2PuzzleMic, iv2PuzzleOne, iv2PuzzleTwo,
            ivColorMic, ivMisPuzzle, ivMisPuzzleMic, ivMisPuzzleOne, ivMisPuzzleTwo, ivMisPuzzleThree, ivMisPuzzleFour,
            ivMisPuzzleFive, ivMisPuzzleSix, ivMisPuzzleSeven, ivMisPuzzleEight, ivMisPuzzleNine, ivReorderMic,
            ivFillBlankMic, ivVoiceMic, ivVoiceRight, ivVoiceWrong,
            ivCrossPuzzleMic, ivCrossPuzzleOne, ivCrossPuzzleTwo, ivCrossPuzzleThree, ivCrossPuzzleFour,
            ivCrossPuzzleFive, ivCrossPuzzleSix;
    TextView tvTitle, txtQuestion, tv9PuzzleQuestion, tv2PuzzleQuestion, txtColorQuestion, tvMisPuzzleQuestion,
            tvReorderQuestion, tvFillBlankQuestion, tvVoiceQuestion, tvCrossPuzzleQuestion, tvFillBlankAnswer1,
            tvFillBlankAnswer2, tvNoDataAvailable;
    LinearLayout llMisPuzzleView, llAnswerView, ll9PuzzleView, ll2PuzzleView, llCrossPuzzle, llColorView,
            llColorOption1, llColorOption2, viewOption1, viewOption2, llReorderView, llFillBlankView,
            llVoiceView, llFillBlankAnswer1, llFillBlankAnswer2;
    View view11, view12, view13, view21, view22, view23, view31, view32, view33;

    @Override
    public int getTheme() {
        return R.style.FullScreenDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.FullScreenDialog);
        activity = getActivity();
        sharedPreferences = getActivity().getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(getActivity());
        RongoApp.logVisitedPageTabEvent("New Quiz");
        Functions.setFirebaseLogEventTrack(getActivity(), Constants.ET_STORY_TRACK, Constants.ET_QUIZ,
                "" + RongoApp.getUserId(), Constants.ET_QUIZ + "_" + RongoApp.getUserId());
        Functions.setUserScreenTrackEvent(appDatabase, Constants.ET_STORY_TRACK, Constants.ET_QUIZ + "_" + RongoApp.getUserId());

        RongoApp.engagementToolActivity(Constants.ET_QUIZ);
        Log.e("Quiz", "New");
    }

    public static QuizFragment newInstance() {
        QuizFragment fragment = new QuizFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_quiz_dialog, container, false);

        speakerbox = new Speakerbox(RongoApp.getInstance());
        speakerbox.setActivity(getActivity());
        speakerbox.setLanguage(Functions.getLocaleLanguage());
        HomeActivity.setTextToSpeechInit();

        answersList = new ArrayList<>();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        TextView tvBack = view.findViewById(R.id.tvBack);
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(Constants.STORY_NAME, Constants.STORY);
                intent.setAction(Constants.BROADCAST_RECEIVER.UPDATE_STORY);
                getActivity().sendBroadcast(intent);
                dismiss();
            }
        });
        TextView tvNext = view.findViewById(R.id.tvNext);
        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(Constants.STORY_NAME, Constants.STORY_SAVDHANI);
                intent.setAction(Constants.BROADCAST_RECEIVER.UPDATE_STORY);
                getActivity().sendBroadcast(intent);
                dismiss();
            }
        });

        parseStateCityListData();
        setInitView();
        setListner();

        rlLeaderBoard = view.findViewById(R.id.rlLeaderBoard);
        rlLeaderBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getQuizLeaderboard();
            }
        });

        tvNoDataAvailable = view.findViewById(R.id.tvNoDataAvailable);
        int cropWeek = RongoApp.getCropWeekDaysFunction();
        if (cropWeek > 0 && cropWeek < Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_season_end_crop_week, "0"))) {
            List<DailyQuizStatusData> dailyQuizStatus = appDatabase.dailyQuizStatusDao().getDailyQuizStatus(
                    sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0"),
                    sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, "0"),
                    sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, "0"));
            if (dailyQuizStatus != null && dailyQuizStatus.size() >= 1) {
                btnNext.setVisibility(View.GONE);
                tvNoDataAvailable.setVisibility(View.VISIBLE);
                rlLeaderBoard.setVisibility(View.VISIBLE);
            } else {
                btnNext.setVisibility(View.VISIBLE);
                tvNoDataAvailable.setVisibility(View.GONE);
                rlLeaderBoard.setVisibility(View.GONE);

                setQuizData();
            }
        } else {
            btnNext.setVisibility(View.GONE);
            tvNoDataAvailable.setVisibility(View.VISIBLE);
            rlLeaderBoard.setVisibility(View.VISIBLE);
        }

        return view;
    }

    void setInitView() {
        ivBack = (ImageView) view.findViewById(R.id.ivClose);
        tvTitle = (TextView) view.findViewById(R.id.tvTitle);

        setAnswerTrueFalseView();
        setAnswerColorSelectView();
        set9PuzzleView();
        set2PuzzleView();
        setMisPartPuzzle();
        setReOrderView();
        setFillBlankView();
        setVoiceView();
        setCrossPuzzleView();

        llAnswerView.setVisibility(View.GONE);
        llColorView.setVisibility(View.GONE);
        ll9PuzzleView.setVisibility(View.GONE);
        ll2PuzzleView.setVisibility(View.GONE);
        llMisPuzzleView.setVisibility(View.GONE);
        llReorderView.setVisibility(View.GONE);
        llFillBlankView.setVisibility(View.GONE);
        llVoiceView.setVisibility(View.GONE);
        llCrossPuzzle.setVisibility(View.GONE);
        llFillBlankView.setVisibility(View.GONE);

        btnNext = (Button) view.findViewById(R.id.btnNext);
    }

    void setListner() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equalsIgnoreCase("drag_and_reorder")) {
                    answersList = new ArrayList<>();
                    Iterator dragIterator = dragPosList.keySet().iterator();
                    while (dragIterator.hasNext()) {
                        String key = (String) dragIterator.next();
                        String value = (String) dragPosList.get(key);
//                        Log.e("keyvalue=", key + "==" + value);
                        answersList.add(value);
                    }
                }
                if (type.equalsIgnoreCase("match_the_following")) {
                    answersList = new ArrayList<>();
                    Iterator crossIterator = reOrderPosList.keySet().iterator();
                    while (crossIterator.hasNext()) {
                        String key = (String) crossIterator.next();
                        String value = (String) reOrderPosList.get(key);
                        Log.e("keyValue=", key + "==" + value);
//                        answersList.add(value);
                        if (value.equalsIgnoreCase("R1")) {
                            answersList.add("0");
                        }
                        if (value.equalsIgnoreCase("R2")) {
                            answersList.add("1");
                        }
                        if (value.equalsIgnoreCase("R3")) {
                            answersList.add("2");
                        }
                    }
                }

                if (answersList.size() > 0) {
                    isAnswerCheck(answersList);
//                showDialog();
                    showSuccessDialog();
                } else {
                    Toast.makeText(getActivity(), "" + getString(R.string.str_please_select_option), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    boolean isAnswerCheck(ArrayList<String> answer) {
        isAnswerValid = false;
        String[] allIdsArray = TextUtils.split(isAnswer, ",");
        List<String> idsList = new ArrayList<String>(Arrays.asList(allIdsArray));

        if (type.equalsIgnoreCase("drag_and_reorder")) {
            if (answer.equals(idsList)) {
//                Log.e("Result1", idsList + " - " + answer);
                isAnswerValid = true;
            }
        } else if (type.equalsIgnoreCase("match_the_following")) {
            if (answer.equals(idsList)) {
//                Log.e("Result1", idsList + " - " + answer);
                isAnswerValid = true;
            }
        } else {
            for (String next : idsList) {
                Log.e("Result", next + " - " + answer);
                if (answer.contains(next)) {
                    isAnswerValid = true;
                    break;
                }
            }
        }
        return isAnswerValid;
    }


    void setAudioView(boolean isPlay, String textVoice, ProgressBar progressBar) {
        try {
            if (isPlay) {
                Runnable onStart = new Runnable() {
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                    }
                };
                Runnable onDone = new Runnable() {
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                    }
                };
                Runnable onError = new Runnable() {
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                    }
                };
                speakerbox.play(textVoice, onStart, onDone, onError);
            } else {
                speakerbox.isMuted();
                speakerbox.stop();
                speakerbox.shutdown();
            }

//            if (HomeActivity.tts != null) {
//                HomeActivity.tts.speak(textVoice, new ImageView(this), progressBar);
//            } else {
//                HomeActivity.setTextToSpeechInit();
//                HomeActivity.tts.speak(textVoice, new ImageView(this), progressBar);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setSelection(int answer) {
        if (answersList.size() > 0) {
            if (answersList.contains(String.valueOf(answer))) {
                answersList.remove(String.valueOf(answer));
            } else {
                answersList.add(String.valueOf(answer));
            }
        } else {
            answersList = new ArrayList<>();
            answersList.add(String.valueOf(answer));
        }

        ivOne.setImageResource(0);
        ivTwo.setImageResource(0);
        ivThree.setImageResource(0);
        ivFour.setImageResource(0);
        ivFive.setImageResource(0);
        ivSix.setImageResource(0);
        ivSeven.setImageResource(0);
        ivEight.setImageResource(0);
        ivNine.setImageResource(0);
        ivOne.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivTwo.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivThree.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivFour.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivFive.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivSix.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivSeven.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivEight.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));
        ivNine.setBackground(getResources().getDrawable(R.drawable.rounded_box_transparent));

        for (int i = 0; i < answersList.size(); i++) {
            if (answersList.get(i).equalsIgnoreCase("1")) {
                ivOne.setImageResource(R.mipmap.q_tick);
                ivOne.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("2")) {
                ivTwo.setImageResource(R.mipmap.q_tick);
                ivTwo.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("3")) {
                ivThree.setImageResource(R.mipmap.q_tick);
                ivThree.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("4")) {
                ivFour.setImageResource(R.mipmap.q_tick);
                ivFour.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("5")) {
                ivFive.setImageResource(R.mipmap.q_tick);
                ivFive.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("6")) {
                ivSix.setImageResource(R.mipmap.q_tick);
                ivSix.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("7")) {
                ivSeven.setImageResource(R.mipmap.q_tick);
                ivSeven.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("8")) {
                ivEight.setImageResource(R.mipmap.q_tick);
                ivEight.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
            if (answersList.get(i).equalsIgnoreCase("9")) {
                ivNine.setImageResource(R.mipmap.q_tick);
                ivNine.setBackground(getResources().getDrawable(R.drawable.rounded_box_trans_select));
            }
        }
    }


    void setAnswerTrueFalseView() {
        try {
            llAnswerView = (LinearLayout) view.findViewById(R.id.llAnswerView);
            ivQuestion = (RoundedImageView) view.findViewById(R.id.ivQuestion);
            txtQuestion = (TextView) view.findViewById(R.id.txtQuestion);
            ivAnswerMic = (ImageView) view.findViewById(R.id.ivAnswerMic);
            pbAnswer = view.findViewById(R.id.pbAnswer);
            ivRightAnswer = (ImageView) view.findViewById(R.id.ivRightAnswer);
            ivWrongAnswer = (ImageView) view.findViewById(R.id.ivWrongAnswer);
            ivAnswerMic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setAudioView(true, question, pbAnswer);
                }
            });

            ivWrongAnswer.setBackgroundResource(R.drawable.rounded_box_orange_white);
            ivRightAnswer.setBackgroundResource(R.drawable.rounded_box_orange_white);

            ivRightAnswer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    answersList = new ArrayList<>();
                    answersList.add("0");
                    ivRightAnswer.setBackgroundResource(R.drawable.rounded_layout_selected);
                    ivWrongAnswer.setBackgroundResource(R.drawable.rounded_box_orange_white);
                }
            });
            ivWrongAnswer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    answersList = new ArrayList<>();
                    answersList.add("1");
                    ivRightAnswer.setBackgroundResource(R.drawable.rounded_box_orange_white);
                    ivWrongAnswer.setBackgroundResource(R.drawable.rounded_layout_selected);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setAnswerColorSelectView() {
        try {
            llColorView = (LinearLayout) view.findViewById(R.id.llColorView);
            ivColor = (RoundedImageView) view.findViewById(R.id.ivColor);
            txtColorQuestion = (TextView) view.findViewById(R.id.txtColorQuestion);
            ivColorMic = (ImageView) view.findViewById(R.id.ivColorMic);
            pbColor = view.findViewById(R.id.pbColor);

            llColorOption1 = (LinearLayout) view.findViewById(R.id.llColorOption1);
            llColorOption2 = (LinearLayout) view.findViewById(R.id.llColorOption2);
            viewOption1 = view.findViewById(R.id.viewOption1);
            viewOption2 = view.findViewById(R.id.viewOption2);
            viewOption1.setBackground(Functions.getBorderWithColor("14", "#27AE6F"));
            viewOption2.setBackground(Functions.getBorderWithColor("14", "#F16A30"));
            ivColorMic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setAudioView(true, question, pbColor);
                }
            });
            llColorOption1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    answersList = new ArrayList<>();
                    answersList.add("0");
                    llColorOption1.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_layout_selected));
                    llColorOption2.setBackground(null);
                }
            });
            llColorOption2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    answersList = new ArrayList<>();
                    answersList.add("1");
                    llColorOption1.setBackground(null);
                    llColorOption2.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_layout_selected));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void set9PuzzleView() {
        try {
            ll9PuzzleView = (LinearLayout) view.findViewById(R.id.ll9PuzzleView);
            iv9Puzzle = (ImageView) view.findViewById(R.id.iv9Puzzle);
            tv9PuzzleQuestion = (TextView) view.findViewById(R.id.tv9PuzzleQuestion);
            iv9PuzzleMic = (ImageView) view.findViewById(R.id.iv9PuzzleMic);
            pb9Puzzle = view.findViewById(R.id.pb9Puzzle);
            ivOne = (ImageView) view.findViewById(R.id.ivOne);
            ivTwo = (ImageView) view.findViewById(R.id.ivTwo);
            ivThree = (ImageView) view.findViewById(R.id.ivThree);
            ivFour = (ImageView) view.findViewById(R.id.ivFour);
            ivFive = (ImageView) view.findViewById(R.id.ivFive);
            ivSix = (ImageView) view.findViewById(R.id.ivSix);
            ivSeven = (ImageView) view.findViewById(R.id.ivSeven);
            ivEight = (ImageView) view.findViewById(R.id.ivEight);
            ivNine = (ImageView) view.findViewById(R.id.ivNine);

            iv9PuzzleMic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setAudioView(true, question, pb9Puzzle);
                }
            });

            ivOne.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSelection(1);
                }
            });

            ivTwo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSelection(2);
                }
            });

            ivThree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSelection(3);
                }
            });

            ivFour.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSelection(4);
                }
            });

            ivFive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSelection(5);
                }
            });

            ivSix.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSelection(6);
                }
            });

            ivSeven.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSelection(7);
                }
            });

            ivEight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSelection(8);
                }
            });

            ivNine.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setSelection(9);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void set2PuzzleView() {
        try {
            ll2PuzzleView = (LinearLayout) view.findViewById(R.id.ll2PuzzleView);
            iv2Puzzle = (RoundedImageView) view.findViewById(R.id.iv2Puzzle);
            tv2PuzzleQuestion = (TextView) view.findViewById(R.id.tv2PuzzleQuestion);
            iv2PuzzleMic = (ImageView) view.findViewById(R.id.iv2PuzzleMic);
            pb2Puzzle = view.findViewById(R.id.pb2Puzzle);
            iv2PuzzleOne = (ImageView) view.findViewById(R.id.iv2PuzzleOne);
            iv2PuzzleTwo = (ImageView) view.findViewById(R.id.iv2PuzzleTwo);

            iv2PuzzleMic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setAudioView(true, question, pb2Puzzle);
                }
            });

            iv2PuzzleOne.setBackgroundResource(R.drawable.rounded_box_orange_white);
            iv2PuzzleTwo.setBackgroundResource(R.drawable.rounded_box_orange_white);

            iv2PuzzleOne.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    answersList = new ArrayList<>();
                    answersList.add("0");
                    iv2PuzzleOne.setBackgroundResource(R.drawable.rounded_box_trans_select);
                    iv2PuzzleTwo.setBackgroundResource(R.drawable.rounded_box_orange_white);
                }
            });
            iv2PuzzleTwo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    answersList = new ArrayList<>();
                    answersList.add("1");
                    iv2PuzzleTwo.setBackgroundResource(R.drawable.rounded_box_trans_select);
                    iv2PuzzleOne.setBackgroundResource(R.drawable.rounded_box_orange_white);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setMisPartPuzzle() {
        try {
            llMisPuzzleView = (LinearLayout) view.findViewById(R.id.llMisPuzzleView);
            ivMisPuzzle = (ImageView) view.findViewById(R.id.ivMisPuzzle);
            ivMisPuzzle.setVisibility(View.INVISIBLE);

            tvMisPuzzleQuestion = (TextView) view.findViewById(R.id.tvMisPuzzleQuestion);
            ivMisPuzzleMic = (ImageView) view.findViewById(R.id.ivMisPuzzleMic);
            pbMisPuzzle = view.findViewById(R.id.pbMisPuzzle);
            ivMisPuzzleMic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setAudioView(true, question, pbMisPuzzle);
                }
            });

            ivMisPuzzle1 = (ImageView) view.findViewById(R.id.ivMisPuzzle1);
            ivMisPuzzle2 = (ImageView) view.findViewById(R.id.ivMisPuzzle2);

            ivMisPuzzleOne = (ImageView) view.findViewById(R.id.ivMisPuzzleOne);
            ivMisPuzzleTwo = (ImageView) view.findViewById(R.id.ivMisPuzzleTwo);
            ivMisPuzzleThree = (ImageView) view.findViewById(R.id.ivMisPuzzleThree);
            ivMisPuzzleFour = (ImageView) view.findViewById(R.id.ivMisPuzzleFour);
            ivMisPuzzleFive = (ImageView) view.findViewById(R.id.ivMisPuzzleFive);
            ivMisPuzzleSix = (ImageView) view.findViewById(R.id.ivMisPuzzleSix);
            ivMisPuzzleSeven = (ImageView) view.findViewById(R.id.ivMisPuzzleSeven);
            ivMisPuzzleEight = (ImageView) view.findViewById(R.id.ivMisPuzzleEight);
            ivMisPuzzleNine = (ImageView) view.findViewById(R.id.ivMisPuzzleNine);

            ivMisPuzzle1.setOnTouchListener(this);
            ivMisPuzzle2.setOnTouchListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setReOrderView() {
        try {
            llReorderView = (LinearLayout) view.findViewById(R.id.llReorderView);
            tvReorderQuestion = (TextView) view.findViewById(R.id.tvReorderQuestion);
            ivReorderMic = (ImageView) view.findViewById(R.id.ivReorderMic);
            pbReorder = view.findViewById(R.id.pbReorder);
            ivReorderMic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setAudioView(true, question, pbReorder);
                }
            });

            ivReorder1 = (RoundedImageView) view.findViewById(R.id.ivReorder1);
            ivReorder2 = (RoundedImageView) view.findViewById(R.id.ivReorder2);
            ivReorder3 = (RoundedImageView) view.findViewById(R.id.ivReorder3);
            ivReorder1.setOnDragListener(this);
            ivReorder2.setOnDragListener(this);
            ivReorder3.setOnDragListener(this);

            ivReorder11 = (RoundedImageView) view.findViewById(R.id.ivReorder11);
            ivReorder22 = (RoundedImageView) view.findViewById(R.id.ivReorder22);
            ivReorder33 = (RoundedImageView) view.findViewById(R.id.ivReorder33);
            ivReorder11.setOnTouchListener(this);
            ivReorder22.setOnTouchListener(this);
            ivReorder33.setOnTouchListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setFillBlankView() {
        try {
            llFillBlankView = (LinearLayout) view.findViewById(R.id.llFillBlankView);
            ivFillBlankQuestion = (RoundedImageView) view.findViewById(R.id.ivFillBlankQuestion);
            tvFillBlankQuestion = (TextView) view.findViewById(R.id.tvFillBlankQuestion);
            ivFillBlankMic = (ImageView) view.findViewById(R.id.ivFillBlankMic);
            pbFillBlank = view.findViewById(R.id.pbFillBlank);

            ivFillBlankMic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setAudioView(true, question, pbFillBlank);
                }
            });

            tvFillBlankAnswer1 = (TextView) view.findViewById(R.id.tvFillBlankAnswer1);
            tvFillBlankAnswer2 = (TextView) view.findViewById(R.id.tvFillBlankAnswer2);
            ivFillBlankAnswer1 = (RoundedImageView) view.findViewById(R.id.ivFillBlankAnswer1);
            ivFillBlankAnswer2 = (RoundedImageView) view.findViewById(R.id.ivFillBlankAnswer2);

            llFillBlankAnswer1 = (LinearLayout) view.findViewById(R.id.llFillBlankAnswer1);
            llFillBlankAnswer2 = (LinearLayout) view.findViewById(R.id.llFillBlankAnswer2);
            llFillBlankAnswer1.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_box_orange));
            llFillBlankAnswer2.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_box_orange));

            llFillBlankAnswer1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    answersList = new ArrayList<>();
                    answersList.add("0");
                    llFillBlankAnswer1.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_layout_selected));
                    llFillBlankAnswer2.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_box_orange));
                }
            });
            llFillBlankAnswer2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    answersList = new ArrayList<>();
                    answersList.add("1");
                    llFillBlankAnswer2.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_layout_selected));
                    llFillBlankAnswer1.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_box_orange));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setVoiceView() {
        try {
            llVoiceView = (LinearLayout) view.findViewById(R.id.llVoiceView);
            tvVoiceQuestion = (TextView) view.findViewById(R.id.tvVoiceQuestion);
            ivVoiceMic = (ImageView) view.findViewById(R.id.ivVoiceMic);
            ivVoiceRight = (ImageView) view.findViewById(R.id.ivVoiceRight);
            ivVoiceWrong = (ImageView) view.findViewById(R.id.ivVoiceWrong);
            line_wave = view.findViewById(R.id.line_wave);
            line_wave.setVisibility(View.VISIBLE);
            line_wave.setAmplitude(0f);
            /*
             * Set background color
             * */
            line_wave.setMainWaveEnabled(false);
//            line_wave.setAmplitude(Math.max(2,AXLineWaveView.MAX_AMPLITUDE/50));

            ivVoiceMic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Runnable onStart = new Runnable() {
                        public void run() {
                            line_wave.setMainWaveEnabled(false);
                            line_wave.setAmplitude(6000f);
                            ivVoiceMic.setImageResource(R.mipmap.pause);
                        }
                    };
                    Runnable onDone = new Runnable() {
                        public void run() {
                            line_wave.setAmplitude(0f);
                            line_wave.setMainWaveEnabled(false);
                            ivVoiceMic.setImageResource(R.mipmap.play);
                        }
                    };
                    Runnable onError = new Runnable() {
                        public void run() {
                            line_wave.setAmplitude(0f);
                            line_wave.setMainWaveEnabled(false);
                            ivVoiceMic.setImageResource(R.mipmap.play);
                        }
                    };
                    speakerbox.play(question, onStart, onDone, onError);
//                    mVisualizer.setAudioSessionId(Integer.parseInt(speakerbox.plays(question, onStart, onDone, onError)));
                }
            });

            ivVoiceRight.setBackgroundResource(R.drawable.rounded_box_orange_white);
            ivVoiceWrong.setBackgroundResource(R.drawable.rounded_box_orange_white);

            ivVoiceRight.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    answersList = new ArrayList<>();
                    answersList.add("0");
                    ivVoiceRight.setBackgroundResource(R.drawable.rounded_layout_selected);
                    ivVoiceWrong.setBackgroundResource(R.drawable.rounded_box_orange_white);
                }
            });
            ivVoiceWrong.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    answersList = new ArrayList<>();
                    answersList.add("1");
                    ivVoiceRight.setBackgroundResource(R.drawable.rounded_box_orange_white);
                    ivVoiceWrong.setBackgroundResource(R.drawable.rounded_layout_selected);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setCrossPuzzleView() {
        try {
            answersList = new ArrayList<>();
            llCrossPuzzle = (LinearLayout) view.findViewById(R.id.llCrossPuzzle);
            rlDrawView = view.findViewById(R.id.rlDrawView);
            tvCrossPuzzleQuestion = (TextView) view.findViewById(R.id.tvCrossPuzzleQuestion);
            ivCrossPuzzleMic = (ImageView) view.findViewById(R.id.ivCrossPuzzleMic);
            pbCrossPuzzle = view.findViewById(R.id.pbCrossPuzzle);
            ivCrossPuzzleMic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setAudioView(true, question, pbCrossPuzzle);
                }
            });

            ivCrossPuzzleOne = (ImageView) view.findViewById(R.id.ivCrossPuzzleOne);
            ivCrossPuzzleTwo = (ImageView) view.findViewById(R.id.ivCrossPuzzleTwo);
            ivCrossPuzzleThree = (ImageView) view.findViewById(R.id.ivCrossPuzzleThree);
            ivCrossPuzzleFour = (ImageView) view.findViewById(R.id.ivCrossPuzzleFour);
            ivCrossPuzzleFive = (ImageView) view.findViewById(R.id.ivCrossPuzzleFive);
            ivCrossPuzzleSix = (ImageView) view.findViewById(R.id.ivCrossPuzzleSix);

//            ivCrossPuzzleOne.setOnDragListener(getActivity());
            ivCrossPuzzleOne.setOnTouchListener(this);
//            ivCrossPuzzleTwo.setOnDragListener(getActivity());
            ivCrossPuzzleTwo.setOnTouchListener(this);
//            ivCrossPuzzleThree.setOnDragListener(getActivity());
            ivCrossPuzzleThree.setOnTouchListener(this);
            ivCrossPuzzleFour.setOnDragListener(this);
            ivCrossPuzzleFour.setOnTouchListener(this);
            ivCrossPuzzleFive.setOnDragListener(this);
            ivCrossPuzzleFive.setOnTouchListener(this);
            ivCrossPuzzleSix.setOnDragListener(this);
            ivCrossPuzzleSix.setOnTouchListener(this);

//            ivCrossPuzzleOne.setImageResource(R.mipmap.edit);
//            ivCrossPuzzleTwo.setImageResource(R.mipmap.edit);
//            ivCrossPuzzleThree.setImageResource(R.mipmap.edit);
//            ivCrossPuzzleFour.setImageResource(R.mipmap.edit);
//            ivCrossPuzzleFive.setImageResource(R.mipmap.edit);
//            ivCrossPuzzleSix.setImageResource(R.mipmap.edit);

            view11 = view.findViewById(R.id.view11);
            view12 = view.findViewById(R.id.view12);
            view13 = view.findViewById(R.id.view13);
            view21 = view.findViewById(R.id.view21);
            view22 = view.findViewById(R.id.view22);
            view23 = view.findViewById(R.id.view23);
            view31 = view.findViewById(R.id.view31);
            view32 = view.findViewById(R.id.view32);
            view33 = view.findViewById(R.id.view33);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        View.DragShadowBuilder mShadow = new View.DragShadowBuilder(v);
        ClipData.Item item = new ClipData.Item(v.getTag().toString());
        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};
        ClipData data = new ClipData(v.getTag().toString(), mimeTypes, item);
        switch (v.getId()) {
            case R.id.ivMisPuzzle1:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    v.startDragAndDrop(data, mShadow, null, 0);
                } else {
                    v.startDrag(data, mShadow, null, 0);
                }
                break;
            case R.id.ivMisPuzzle2:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    v.startDragAndDrop(data, mShadow, null, 0);
                } else {
                    v.startDrag(data, mShadow, null, 0);
                }
                break;
            case R.id.ivReorder11:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    v.startDragAndDrop(data, mShadow, null, 0);
                } else {
                    v.startDrag(data, mShadow, null, 0);
                }
                break;
            case R.id.ivReorder22:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    v.startDragAndDrop(data, mShadow, null, 0);
                } else {
                    v.startDrag(data, mShadow, null, 0);
                }
                break;
            case R.id.ivReorder33:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    v.startDragAndDrop(data, mShadow, null, 0);
                } else {
                    v.startDrag(data, mShadow, null, 0);
                }
                break;

            case R.id.ivReorder1:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    v.startDragAndDrop(data, mShadow, null, 0);
                } else {
                    v.startDrag(data, mShadow, null, 0);
                }
                break;
            case R.id.ivReorder2:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    v.startDragAndDrop(data, mShadow, null, 0);
                } else {
                    v.startDrag(data, mShadow, null, 0);
                }
                break;
            case R.id.ivReorder3:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    v.startDragAndDrop(data, mShadow, null, 0);
                } else {
                    v.startDrag(data, mShadow, null, 0);
                }
                break;

            case R.id.ivCrossPuzzleOne:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    v.startDragAndDrop(data, mShadow, v, 0);
                } else {
                    v.startDrag(data, mShadow, v, 0);
                }
//                View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(ivCrossPuzzleOne);
//                ivCrossPuzzleOne.startDrag(data, shadowBuilder, ivCrossPuzzleOne, 0);
//                ivCrossPuzzleOne.setImageResource(R.mipmap.edit);
                break;
            case R.id.ivCrossPuzzleTwo:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    v.startDragAndDrop(data, mShadow, null, 0);
                } else {
                    v.startDrag(data, mShadow, null, 0);
                }
                break;
            case R.id.ivCrossPuzzleThree:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    v.startDragAndDrop(data, mShadow, null, 0);
                } else {
                    v.startDrag(data, mShadow, null, 0);
                }
                break;
            case R.id.ivCrossPuzzleFour:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    v.startDragAndDrop(data, mShadow, null, 0);
                } else {
                    v.startDrag(data, mShadow, null, 0);
                }
                break;
            case R.id.ivCrossPuzzleFive:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    v.startDragAndDrop(data, mShadow, null, 0);
                } else {
                    v.startDrag(data, mShadow, null, 0);
                }
                break;
            case R.id.ivCrossPuzzleSix:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    v.startDragAndDrop(data, mShadow, null, 0);
                } else {
                    v.startDrag(data, mShadow, null, 0);
                }
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + v.getId());
        }
        return false;
    }

    @Override
    public boolean onDrag(View v, DragEvent event) {
        switch (event.getAction()) {
            case DragEvent.ACTION_DRAG_STARTED:
//                ((ImageView) v).setColorFilter(Color.LTGRAY);
//                v.invalidate();
                return true;

            case DragEvent.ACTION_DRAG_ENTERED:
                String clipData = event.getClipDescription().getLabel().toString();
                switch (clipData) {
                    case "YES":
//                        ((ImageView) v).setColorFilter(ContextCompat.getColor(getActivity(), R.color.GREEN), android.graphics.PorterDuff.Mode.MULTIPLY);
                        break;
                    case "NO":
//                        ((ImageView) v).setColorFilter(ContextCompat.getColor(getActivity(), R.color.light_grey), android.graphics.PorterDuff.Mode.MULTIPLY);
                        break;

                    case "AA":
//                        Log.e("Drag=", "=1");
                        break;
                    case "BB":
//                        Log.e("Drag=", "=2");
                        break;
                    case "CC":
//                        Log.e("Drag=", "=3");
                        break;

                    case "L1":
//                        ((ImageView) v).setBackgroundResource(R.mipmap.edit);
                        break;
                    case "L2":
                        break;
                    case "L3":
                        break;
                    case "R1":
                        break;
                    case "R2":
                        break;
                    case "R3":
                        break;
                }
                v.invalidate();
                return true;

            case DragEvent.ACTION_DRAG_LOCATION:
                return true;

            case DragEvent.ACTION_DRAG_EXITED:
//                ((ImageView) v).clearColorFilter();
//                ((ImageView) v).setColorFilter(Color.LTGRAY);
//                v.invalidate();
                return true;

            case DragEvent.ACTION_DROP:
//                clipData = event.getClipDescription().getLabel().toString();
//                Toast.makeText(getApplicationContext(), clipData, Toast.LENGTH_SHORT).show();
//                v.invalidate();

                if (type.equalsIgnoreCase("drag_and_reorder")) {
                    if (imagesList.size() > 0) {
                        Iterator myVeryOwnIterator = dragPosList.keySet().iterator();
                        while (myVeryOwnIterator.hasNext()) {
                            String key = (String) myVeryOwnIterator.next();
                            String value = (String) dragPosList.get(key);
//                        Log.e("keyvalue=", key + "==" + value);
                            if (key.equalsIgnoreCase("0")) {
                                ivReorder11.setEnabled(true);
                            } else if (key.equalsIgnoreCase("1")) {
                                ivReorder22.setEnabled(true);
                            } else if (key.equalsIgnoreCase("2")) {
                                ivReorder33.setEnabled(true);
                            }
                        }

                        if (event.getClipDescription().getLabel().toString().equalsIgnoreCase("AA")) {
                            ((ImageView) v).setImageDrawable(imagesList.get(0));
                            ivReorder11.setEnabled(false);
                            dragPosList.put("" + ((ImageView) v).getTag(), "0");
                        } else if (event.getClipDescription().getLabel().toString().equalsIgnoreCase("BB")) {
                            ((ImageView) v).setImageDrawable(imagesList.get(1));
                            ivReorder22.setEnabled(false);
                            dragPosList.put("" + ((ImageView) v).getTag(), "1");
                        } else if (event.getClipDescription().getLabel().toString().equalsIgnoreCase("CC")) {
                            ((ImageView) v).setImageDrawable(imagesList.get(2));
                            ivReorder33.setEnabled(false);
                            dragPosList.put("" + ((ImageView) v).getTag(), "2");
                        }
                    }
                }

                if (type.equalsIgnoreCase("drag_and_drop")) {
                    if (chunkedImages.size() > 0) {
                        if (optionsArray != null && optionsArray.length() > 0) {
                            List<String> list = new Gson().fromJson(optionsArray.toString(), new TypeToken<List<String>>() {
                            }.getType());
                            int option = (Integer.parseInt(list.get(0)) - 1);
                            int option1 = (Integer.parseInt(list.get(1)) - 1);

                            if (event.getClipDescription().getLabel().toString().equalsIgnoreCase("YES")) {
                                ((ImageView) v).setImageBitmap(chunkedImages.get(option));
                                answersList = new ArrayList<>();
                                answersList.add("" + list.get(0));
                            }
                            if (event.getClipDescription().getLabel().toString().equalsIgnoreCase("NO")) {
                                ((ImageView) v).setImageBitmap(chunkedImages.get(option1));
                                answersList = new ArrayList<>();
                                answersList.add("" + list.get(1));
                            }
                        }
                    }
                }

                if (type.equalsIgnoreCase("match_the_following")) {
                    String dragPosTag = event.getClipDescription().getLabel().toString();
                    String dropPosTag = "" + ((ImageView) v).getTag();
                    Log.e("Position:", "==" + dragPosTag + "==" + dropPosTag);

                    boolean isAdd = false;
                    for (Map.Entry<String, String> ss : new ConcurrentHashMap<>(reOrderPosList).entrySet()) {
                        if (dragPosTag.equals(ss.getKey()) || dropPosTag.equals(ss.getKey())) {
//                            reOrderPosList.remove(ss.getKey());
                            Log.e("Position11:", "==" + ss.getKey() + "==" + ss.getValue());
                            if (dragPosTag.equals(ss.getValue()) || dropPosTag.equals(ss.getValue())) {
                                Log.e("Position21:", "==" + ss.getKey() + "==" + ss.getValue());
                                isAdd = true;
                                reOrderPosList.remove(ss.getKey());
                            } else {
                                isAdd = true;
                            }
                        } else {
                            Log.e("Position12:", "==" + ss.getKey() + "==" + ss.getValue());
                            if (dragPosTag.equals(ss.getValue()) || dropPosTag.equals(ss.getValue())) {
                                Log.e("Position22:", "==" + ss.getKey() + "==" + ss.getValue());
                                isAdd = true;
                                reOrderPosList.remove(ss.getKey());
                            } else {
                                isAdd = true;
                            }
                        }
                    }
                    System.out.println("Final Map : " + reOrderPosList + "-" + isAdd);

                    if (reOrderPosList.size() <= 0) {
                        reOrderPosList.put("" + dragPosTag, "" + dropPosTag);
                    }
                    if (reOrderPosList.size() > 0) {
                        if (isAdd) {
                            reOrderPosList.put("" + dragPosTag, "" + dropPosTag);
                        }
                    }

                    setMatchPuzzle(reOrderPosList);
                }

                return true;

            case DragEvent.ACTION_DRAG_ENDED:
//                ((ImageView) v).clearColorFilter();
//                if (event.getResult()) {
//                    Toast.makeText(getActivity(), "Awesome!", Toast.LENGTH_SHORT).show();
//                } else {
//                    Toast.makeText(getActivity(), "Aw Snap! Try dropping it again", Toast.LENGTH_SHORT).show();
//                }
                return true;

            default:
                return false;
        }

    }

    void setMatchPuzzle(HashMap<String, String> reOrderPosList) {
        try {
            view11.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);
            view13.setVisibility(View.GONE);
            view21.setVisibility(View.GONE);
            view22.setVisibility(View.GONE);
            view23.setVisibility(View.GONE);
            view31.setVisibility(View.GONE);
            view32.setVisibility(View.GONE);
            view33.setVisibility(View.GONE);
            view11.setVisibility(View.GONE);
            view21.setVisibility(View.GONE);
            view31.setVisibility(View.GONE);
            view12.setVisibility(View.GONE);
            view22.setVisibility(View.GONE);
            view32.setVisibility(View.GONE);
            view13.setVisibility(View.GONE);
            view23.setVisibility(View.GONE);
            view33.setVisibility(View.GONE);

            ivCrossPuzzleOne.setBackgroundResource(R.drawable.rounded_layout_unselect);
            ivCrossPuzzleTwo.setBackgroundResource(R.drawable.rounded_layout_unselect);
            ivCrossPuzzleThree.setBackgroundResource(R.drawable.rounded_layout_unselect);
            ivCrossPuzzleFour.setBackgroundResource(R.drawable.rounded_layout_unselect);
            ivCrossPuzzleFive.setBackgroundResource(R.drawable.rounded_layout_unselect);
            ivCrossPuzzleSix.setBackgroundResource(R.drawable.rounded_layout_unselect);

            Iterator myVeryOwnIterator = reOrderPosList.keySet().iterator();
            while (myVeryOwnIterator.hasNext()) {
                String dragTag = (String) myVeryOwnIterator.next();
                String dropTag = (String) reOrderPosList.get(dragTag);
                if (dragTag.equalsIgnoreCase("L1") && dropTag.equalsIgnoreCase("R1")) {
                    view11.setVisibility(View.VISIBLE);
                    ivCrossPuzzleOne.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                    ivCrossPuzzleFour.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                } else if (dragTag.equalsIgnoreCase("L1") && dropTag.equalsIgnoreCase("R2")) {
                    view12.setVisibility(View.VISIBLE);
                    ivCrossPuzzleOne.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                    ivCrossPuzzleFive.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                } else if (dragTag.equalsIgnoreCase("L1") && dropTag.equalsIgnoreCase("R3")) {
                    view13.setVisibility(View.VISIBLE);
                    ivCrossPuzzleOne.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                    ivCrossPuzzleSix.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                } else if (dragTag.equalsIgnoreCase("L2") && dropTag.equalsIgnoreCase("R1")) {
                    view21.setVisibility(View.VISIBLE);
                    ivCrossPuzzleTwo.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                    ivCrossPuzzleFour.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                } else if (dragTag.equalsIgnoreCase("L2") && dropTag.equalsIgnoreCase("R2")) {
                    view22.setVisibility(View.VISIBLE);
                    ivCrossPuzzleTwo.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                    ivCrossPuzzleFive.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                } else if (dragTag.equalsIgnoreCase("L2") && dropTag.equalsIgnoreCase("R3")) {
                    view23.setVisibility(View.VISIBLE);
                    ivCrossPuzzleTwo.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                    ivCrossPuzzleSix.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                } else if (dragTag.equalsIgnoreCase("L3") && dropTag.equalsIgnoreCase("R1")) {
                    view31.setVisibility(View.VISIBLE);
                    ivCrossPuzzleThree.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                    ivCrossPuzzleFour.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                } else if (dragTag.equalsIgnoreCase("L3") && dropTag.equalsIgnoreCase("R2")) {
                    view32.setVisibility(View.VISIBLE);
                    ivCrossPuzzleThree.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                    ivCrossPuzzleFive.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                } else if (dragTag.equalsIgnoreCase("L3") && dropTag.equalsIgnoreCase("R3")) {
                    view33.setVisibility(View.VISIBLE);
                    ivCrossPuzzleThree.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                    ivCrossPuzzleSix.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                } else if (dragTag.equalsIgnoreCase("R1") && dropTag.equalsIgnoreCase("L1")) {
                    view11.setVisibility(View.VISIBLE);
                    ivCrossPuzzleOne.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                    ivCrossPuzzleFour.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                } else if (dragTag.equalsIgnoreCase("R1") && dropTag.equalsIgnoreCase("L2")) {
                    view21.setVisibility(View.VISIBLE);
                    ivCrossPuzzleTwo.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                    ivCrossPuzzleFour.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                } else if (dragTag.equalsIgnoreCase("R1") && dropTag.equalsIgnoreCase("L3")) {
                    view31.setVisibility(View.VISIBLE);
                    ivCrossPuzzleThree.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                    ivCrossPuzzleFour.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                } else if (dragTag.equalsIgnoreCase("R2") && dropTag.equalsIgnoreCase("L1")) {
                    view12.setVisibility(View.VISIBLE);
                    ivCrossPuzzleOne.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                    ivCrossPuzzleFive.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                } else if (dragTag.equalsIgnoreCase("R2") && dropTag.equalsIgnoreCase("L2")) {
                    view22.setVisibility(View.VISIBLE);
                    ivCrossPuzzleTwo.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                    ivCrossPuzzleFive.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                } else if (dragTag.equalsIgnoreCase("R2") && dropTag.equalsIgnoreCase("L3")) {
                    view32.setVisibility(View.VISIBLE);
                    ivCrossPuzzleThree.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                    ivCrossPuzzleFive.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                } else if (dragTag.equalsIgnoreCase("R3") && dropTag.equalsIgnoreCase("L1")) {
                    view13.setVisibility(View.VISIBLE);
                    ivCrossPuzzleOne.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                    ivCrossPuzzleSix.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                } else if (dragTag.equalsIgnoreCase("R3") && dropTag.equalsIgnoreCase("L2")) {
                    view23.setVisibility(View.VISIBLE);
                    ivCrossPuzzleTwo.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                    ivCrossPuzzleSix.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                } else if (dragTag.equalsIgnoreCase("R3") && dropTag.equalsIgnoreCase("L3")) {
                    view33.setVisibility(View.VISIBLE);
                    ivCrossPuzzleThree.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                    ivCrossPuzzleSix.setBackgroundResource(R.drawable.rounded_layout_selected_green);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setMisPartPuzzleSplitImage(ImageView image, int chunkNumbers) {
        try {
            int rows, cols, chunkHeight, chunkWidth;
            chunkedImages = new ArrayList<Bitmap>(chunkNumbers);

            BitmapDrawable drawable = (BitmapDrawable) image.getDrawable();
            Bitmap bitmap = drawable.getBitmap();
            Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true);
            rows = cols = (int) Math.sqrt(chunkNumbers);
            chunkHeight = bitmap.getHeight() / rows;
            chunkWidth = bitmap.getWidth() / cols;

            int yCoord = 0;
            for (int x = 0; x < rows; x++) {
                int xCoord = 0;
                for (int y = 0; y < cols; y++) {
                    chunkedImages.add(Bitmap.createBitmap(scaledBitmap, xCoord, yCoord, chunkWidth, chunkHeight));
                    xCoord += chunkWidth;
                }
                yCoord += chunkHeight;
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ivMisPuzzle.setVisibility(View.VISIBLE);
                    if (chunkedImages.size() > 0) {
                        if (optionsArray != null && optionsArray.length() > 0) {
                            List<String> list = new Gson().fromJson(optionsArray.toString(), new TypeToken<List<String>>() {
                            }.getType());
                            isAnswer = list.get(0);
                            int option = (Integer.parseInt(list.get(0)) - 1);
                            int option1 = (Integer.parseInt(list.get(1)) - 1);
                            Log.e("Data:", "==" + chunkedImages.size() + "==" + option + "==" + option1);

                            ivMisPuzzle1.setImageBitmap(chunkedImages.get(option));
                            ivMisPuzzle2.setImageBitmap(chunkedImages.get(option1));

                            if (isAnswer.equalsIgnoreCase("1")) {
                                ivMisPuzzleOne.setOnDragListener(QuizFragment.this);
                                ivMisPuzzleOne.setBackgroundColor(getResources().getColor(R.color.white));
                                ivMisPuzzleTwo.setImageBitmap(chunkedImages.get(1));
                                ivMisPuzzleThree.setImageBitmap(chunkedImages.get(2));
                                ivMisPuzzleFour.setImageBitmap(chunkedImages.get(3));
                                ivMisPuzzleFive.setImageBitmap(chunkedImages.get(4));
                                ivMisPuzzleSix.setImageBitmap(chunkedImages.get(5));
                                ivMisPuzzleSeven.setImageBitmap(chunkedImages.get(6));
                                ivMisPuzzleEight.setImageBitmap(chunkedImages.get(7));
                                ivMisPuzzleNine.setImageBitmap(chunkedImages.get(8));
                            }
                            if (isAnswer.equalsIgnoreCase("2")) {
                                ivMisPuzzleTwo.setOnDragListener(QuizFragment.this);
                                ivMisPuzzleOne.setImageBitmap(chunkedImages.get(0));
                                ivMisPuzzleTwo.setBackgroundColor(getResources().getColor(R.color.white));
                                ivMisPuzzleThree.setImageBitmap(chunkedImages.get(2));
                                ivMisPuzzleFour.setImageBitmap(chunkedImages.get(3));
                                ivMisPuzzleFive.setImageBitmap(chunkedImages.get(4));
                                ivMisPuzzleSix.setImageBitmap(chunkedImages.get(5));
                                ivMisPuzzleSeven.setImageBitmap(chunkedImages.get(6));
                                ivMisPuzzleEight.setImageBitmap(chunkedImages.get(7));
                                ivMisPuzzleNine.setImageBitmap(chunkedImages.get(8));
                            }
                            if (isAnswer.equalsIgnoreCase("3")) {
                                ivMisPuzzleThree.setOnDragListener(QuizFragment.this);
                                ivMisPuzzleOne.setImageBitmap(chunkedImages.get(0));
                                ivMisPuzzleTwo.setImageBitmap(chunkedImages.get(1));
//                            ivMisPuzzleThree.setImageBitmap(chunkedImages.get(2));
                                ivMisPuzzleThree.setBackgroundColor(getResources().getColor(R.color.white));
                                ivMisPuzzleFour.setImageBitmap(chunkedImages.get(3));
                                ivMisPuzzleFive.setImageBitmap(chunkedImages.get(4));
                                ivMisPuzzleSix.setImageBitmap(chunkedImages.get(5));
                                ivMisPuzzleSeven.setImageBitmap(chunkedImages.get(6));
                                ivMisPuzzleEight.setImageBitmap(chunkedImages.get(7));
                                ivMisPuzzleNine.setImageBitmap(chunkedImages.get(8));
                            }
                            if (isAnswer.equalsIgnoreCase("4")) {
                                ivMisPuzzleFour.setOnDragListener(QuizFragment.this);
                                ivMisPuzzleOne.setImageBitmap(chunkedImages.get(0));
                                ivMisPuzzleTwo.setImageBitmap(chunkedImages.get(1));
                                ivMisPuzzleThree.setImageBitmap(chunkedImages.get(2));
                                ivMisPuzzleFour.setBackgroundColor(getResources().getColor(R.color.white));
                                ivMisPuzzleFive.setImageBitmap(chunkedImages.get(4));
                                ivMisPuzzleSix.setImageBitmap(chunkedImages.get(5));
                                ivMisPuzzleSeven.setImageBitmap(chunkedImages.get(6));
                                ivMisPuzzleEight.setImageBitmap(chunkedImages.get(7));
                                ivMisPuzzleNine.setImageBitmap(chunkedImages.get(8));
                            }
                            if (isAnswer.equalsIgnoreCase("5")) {
                                ivMisPuzzleFive.setOnDragListener(QuizFragment.this);
                                ivMisPuzzleOne.setImageBitmap(chunkedImages.get(0));
                                ivMisPuzzleTwo.setImageBitmap(chunkedImages.get(1));
                                ivMisPuzzleThree.setImageBitmap(chunkedImages.get(2));
                                ivMisPuzzleFour.setImageBitmap(chunkedImages.get(3));
                                ivMisPuzzleFive.setBackgroundColor(getResources().getColor(R.color.white));
                                ivMisPuzzleSix.setImageBitmap(chunkedImages.get(5));
                                ivMisPuzzleSeven.setImageBitmap(chunkedImages.get(6));
                                ivMisPuzzleEight.setImageBitmap(chunkedImages.get(7));
                                ivMisPuzzleNine.setImageBitmap(chunkedImages.get(8));
                            }
                            if (isAnswer.equalsIgnoreCase("6")) {
                                ivMisPuzzleSix.setOnDragListener(QuizFragment.this);
                                ivMisPuzzleOne.setImageBitmap(chunkedImages.get(0));
                                ivMisPuzzleTwo.setImageBitmap(chunkedImages.get(1));
                                ivMisPuzzleThree.setImageBitmap(chunkedImages.get(2));
                                ivMisPuzzleFour.setImageBitmap(chunkedImages.get(3));
                                ivMisPuzzleFive.setImageBitmap(chunkedImages.get(4));
                                ivMisPuzzleSix.setBackgroundColor(getResources().getColor(R.color.white));
                                ivMisPuzzleSeven.setImageBitmap(chunkedImages.get(6));
                                ivMisPuzzleEight.setImageBitmap(chunkedImages.get(7));
                                ivMisPuzzleNine.setImageBitmap(chunkedImages.get(8));
                            }
                            if (isAnswer.equalsIgnoreCase("7")) {
                                ivMisPuzzleSeven.setOnDragListener(QuizFragment.this);
                                ivMisPuzzleOne.setImageBitmap(chunkedImages.get(0));
                                ivMisPuzzleTwo.setImageBitmap(chunkedImages.get(1));
                                ivMisPuzzleThree.setImageBitmap(chunkedImages.get(2));
                                ivMisPuzzleFour.setImageBitmap(chunkedImages.get(3));
                                ivMisPuzzleFive.setImageBitmap(chunkedImages.get(4));
                                ivMisPuzzleSix.setImageBitmap(chunkedImages.get(5));
                                ivMisPuzzleSeven.setBackgroundColor(getResources().getColor(R.color.white));
                                ivMisPuzzleEight.setImageBitmap(chunkedImages.get(7));
                                ivMisPuzzleNine.setImageBitmap(chunkedImages.get(8));
                            }
                            if (isAnswer.equalsIgnoreCase("8")) {
                                ivMisPuzzleEight.setOnDragListener(QuizFragment.this);
                                ivMisPuzzleOne.setImageBitmap(chunkedImages.get(0));
                                ivMisPuzzleTwo.setImageBitmap(chunkedImages.get(1));
                                ivMisPuzzleThree.setImageBitmap(chunkedImages.get(2));
                                ivMisPuzzleFour.setImageBitmap(chunkedImages.get(3));
                                ivMisPuzzleFive.setImageBitmap(chunkedImages.get(4));
                                ivMisPuzzleSix.setImageBitmap(chunkedImages.get(5));
                                ivMisPuzzleSeven.setImageBitmap(chunkedImages.get(6));
                                ivMisPuzzleEight.setBackgroundColor(getResources().getColor(R.color.white));
                                ivMisPuzzleNine.setImageBitmap(chunkedImages.get(8));
                            }
                            if (isAnswer.equalsIgnoreCase("9")) {
                                ivMisPuzzleNine.setOnDragListener(QuizFragment.this);
                                ivMisPuzzleOne.setImageBitmap(chunkedImages.get(0));
                                ivMisPuzzleTwo.setImageBitmap(chunkedImages.get(1));
                                ivMisPuzzleThree.setImageBitmap(chunkedImages.get(2));
                                ivMisPuzzleFour.setImageBitmap(chunkedImages.get(3));
                                ivMisPuzzleFive.setImageBitmap(chunkedImages.get(4));
                                ivMisPuzzleSix.setImageBitmap(chunkedImages.get(5));
                                ivMisPuzzleSeven.setImageBitmap(chunkedImages.get(6));
                                ivMisPuzzleEight.setImageBitmap(chunkedImages.get(7));
                                ivMisPuzzleNine.setBackgroundColor(getResources().getColor(R.color.white));
                            }
                        }
                    }
                }
            }, 1800);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void setQuizData() {
        try {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    try {
                        JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.getDailyQuizDataJSONFromAsset(getActivity()));
                        if (jsonObject != null && jsonObject.length() > 0) {
                            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                JSONObject data = jsonObject.optJSONObject("data");
                                if (data != null && data.length() > 0) {
                                    JSONObject jsonObj = data.optJSONObject("result");
                                    if (jsonObj != null && jsonObj.length() > 0) {
                                        QuizId = jsonObj.getString("id");
                                        JSONObject json_data = jsonObj.getJSONObject("json_data");
                                        if (json_data != null && json_data.length() > 0) {
                                            Iterator<String> keys = json_data.keys();
                                            while (keys.hasNext()) {
                                                String key = keys.next();
                                                String value = json_data.optString(key);
                                                if (key.equalsIgnoreCase(RongoApp.getSelectedCrop())) {
                                                    JSONObject jsonLang = new JSONObject(value);
                                                    if (jsonLang != null && jsonLang.length() > 0) {
                                                        Iterator<String> keyLang = jsonLang.keys();
                                                        while (keyLang.hasNext()) {
                                                            String key1 = keyLang.next();
                                                            String value1 = jsonLang.optString(key1);
                                                            if (key1.equalsIgnoreCase(RongoApp.getSeasonName())) {
                                                                JSONObject jsonLang1 = new JSONObject(value1);
                                                                if (jsonLang1 != null && jsonLang1.length() > 0) {
                                                                    Iterator<String> keyLang1 = jsonLang1.keys();
                                                                    while (keyLang1.hasNext()) {
                                                                        String key2 = keyLang1.next();
                                                                        String value2 = jsonLang1.optString(key2);
                                                                        if (key2.equalsIgnoreCase(RongoApp.getDefaultLanguage())) {
                                                                            JSONArray jsonArray = new JSONArray(value2);
                                                                            JSONObject resultData = jsonArray.getJSONObject(Integer.parseInt(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "2")) - 1);
                                                                            JSONObject jsonObjects = resultData.getJSONObject(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, "0"));

                                                                            question = jsonObjects.optString("question");
                                                                            imageArray = jsonObjects.optJSONArray("image");
                                                                            optionsArray = jsonObjects.optJSONArray("options");
                                                                            type = jsonObjects.optString("type");
                                                                            isAnswer = jsonObjects.optString("answer");
                                                                            keyword_id = jsonObjects.optString("keyword_id");
                                                                            Log.e("question==", "" + question + "==" + type);

                                                                            if (!TextUtils.isEmpty(type)) {
                                                                                if (type.equalsIgnoreCase("true_false")) {
                                                                                    llAnswerView.setVisibility(View.VISIBLE);
                                                                                    llColorView.setVisibility(View.GONE);
                                                                                    ll9PuzzleView.setVisibility(View.GONE);
                                                                                    ll2PuzzleView.setVisibility(View.GONE);
                                                                                    llMisPuzzleView.setVisibility(View.GONE);
                                                                                    llReorderView.setVisibility(View.GONE);
                                                                                    llFillBlankView.setVisibility(View.GONE);
                                                                                    llVoiceView.setVisibility(View.GONE);
                                                                                    llCrossPuzzle.setVisibility(View.GONE);

                                                                                    txtQuestion.setText("" + question);

                                                                                    if (imageArray != null && imageArray.length() > 0) {
                                                                                        List<String> list = new Gson().fromJson(imageArray.toString(), new TypeToken<List<String>>() {
                                                                                        }.getType());
                                                                                        ivQuestion.setImageDrawable(getImagePath(list.get(0)));
                                                                                    }

                                                                                } else if (type.equalsIgnoreCase("colour")) {
                                                                                    llAnswerView.setVisibility(View.GONE);
                                                                                    llColorView.setVisibility(View.VISIBLE);
                                                                                    ll9PuzzleView.setVisibility(View.GONE);
                                                                                    ll2PuzzleView.setVisibility(View.GONE);
                                                                                    llMisPuzzleView.setVisibility(View.GONE);
                                                                                    llReorderView.setVisibility(View.GONE);
                                                                                    llFillBlankView.setVisibility(View.GONE);
                                                                                    llVoiceView.setVisibility(View.GONE);
                                                                                    llCrossPuzzle.setVisibility(View.GONE);

                                                                                    txtColorQuestion.setText("" + question);

                                                                                    if (imageArray != null && imageArray.length() > 0) {
                                                                                        List<String> list = new Gson().fromJson(imageArray.toString(), new TypeToken<List<String>>() {
                                                                                        }.getType());
                                                                                        ivColor.setImageDrawable(getImagePath(list.get(0)));
                                                                                    }

                                                                                    if (optionsArray != null && optionsArray.length() > 0) {
                                                                                        List<String> list = new Gson().fromJson(optionsArray.toString(), new TypeToken<List<String>>() {
                                                                                        }.getType());
                                                                                        viewOption1.setBackground(Functions.getBorderWithColor("14", "" + list.get(0)));
                                                                                        viewOption2.setBackground(Functions.getBorderWithColor("14", "" + list.get(1)));
                                                                                    }

                                                                                } else if (type.equalsIgnoreCase("puzzle")) {
                                                                                    llAnswerView.setVisibility(View.GONE);
                                                                                    llColorView.setVisibility(View.GONE);
                                                                                    ll9PuzzleView.setVisibility(View.VISIBLE);
                                                                                    ll2PuzzleView.setVisibility(View.GONE);
                                                                                    llMisPuzzleView.setVisibility(View.GONE);
                                                                                    llReorderView.setVisibility(View.GONE);
                                                                                    llFillBlankView.setVisibility(View.GONE);
                                                                                    llVoiceView.setVisibility(View.GONE);
                                                                                    llCrossPuzzle.setVisibility(View.GONE);

                                                                                    tv9PuzzleQuestion.setText("" + question);

                                                                                    if (imageArray != null && imageArray.length() > 0) {
                                                                                        List<String> list = new Gson().fromJson(imageArray.toString(), new TypeToken<List<String>>() {
                                                                                        }.getType());
                                                                                        iv9Puzzle.setImageDrawable(getImagePath(list.get(0)));
//                                                                            set9PuzzleSplitImage(iv9Puzzle, 9);
                                                                                    }

                                                                                } else if (type.equalsIgnoreCase("select_image_part")) {
                                                                                    llAnswerView.setVisibility(View.GONE);
                                                                                    llColorView.setVisibility(View.GONE);
                                                                                    ll9PuzzleView.setVisibility(View.GONE);
                                                                                    ll2PuzzleView.setVisibility(View.VISIBLE);
                                                                                    llMisPuzzleView.setVisibility(View.GONE);
                                                                                    llReorderView.setVisibility(View.GONE);
                                                                                    llFillBlankView.setVisibility(View.GONE);
                                                                                    llVoiceView.setVisibility(View.GONE);
                                                                                    llCrossPuzzle.setVisibility(View.GONE);

                                                                                    tv2PuzzleQuestion.setText("" + question);

                                                                                    if (optionsArray != null && optionsArray.length() > 0) {
                                                                                        List<String> list = new Gson().fromJson(optionsArray.toString(), new TypeToken<List<String>>() {
                                                                                        }.getType());
                                                                                        iv2PuzzleOne.setImageDrawable(getImagePath(list.get(0)));
                                                                                        iv2PuzzleTwo.setImageDrawable(getImagePath(list.get(1)));
                                                                                    }

                                                                                } else if (type.equalsIgnoreCase("drag_and_drop")) {
                                                                                    llAnswerView.setVisibility(View.GONE);
                                                                                    llColorView.setVisibility(View.GONE);
                                                                                    ll9PuzzleView.setVisibility(View.GONE);
                                                                                    ll2PuzzleView.setVisibility(View.GONE);
                                                                                    llMisPuzzleView.setVisibility(View.VISIBLE);
                                                                                    llReorderView.setVisibility(View.GONE);
                                                                                    llFillBlankView.setVisibility(View.GONE);
                                                                                    llVoiceView.setVisibility(View.GONE);
                                                                                    llCrossPuzzle.setVisibility(View.GONE);

                                                                                    tvMisPuzzleQuestion.setText("" + question);

                                                                                    if (imageArray != null && imageArray.length() > 0) {
                                                                                        List<String> list = new Gson().fromJson(imageArray.toString(), new TypeToken<List<String>>() {
                                                                                        }.getType());
                                                                                        ivMisPuzzle.setImageDrawable(getImagePath(list.get(0)));

                                                                                        setMisPartPuzzleSplitImage(ivMisPuzzle, 9);
                                                                                    }

                                                                                } else if (type.equalsIgnoreCase("drag_and_reorder")) {
                                                                                    llAnswerView.setVisibility(View.GONE);
                                                                                    llColorView.setVisibility(View.GONE);
                                                                                    ll9PuzzleView.setVisibility(View.GONE);
                                                                                    ll2PuzzleView.setVisibility(View.GONE);
                                                                                    llMisPuzzleView.setVisibility(View.GONE);
                                                                                    llReorderView.setVisibility(View.VISIBLE);
                                                                                    llFillBlankView.setVisibility(View.GONE);
                                                                                    llVoiceView.setVisibility(View.GONE);
                                                                                    llCrossPuzzle.setVisibility(View.GONE);

                                                                                    tvReorderQuestion.setText("" + question);

                                                                                    imagesList = new ArrayList<>();
                                                                                    if (optionsArray != null && optionsArray.length() > 0) {
                                                                                        List<String> list = new Gson().fromJson(optionsArray.toString(), new TypeToken<List<String>>() {
                                                                                        }.getType());
                                                                                        ivReorder11.setImageDrawable(getImagePath(list.get(0)));
                                                                                        imagesList.add(getImagePath(list.get(0)));

                                                                                        ivReorder22.setImageDrawable(getImagePath(list.get(1)));
                                                                                        imagesList.add(getImagePath(list.get(1)));

                                                                                        ivReorder33.setImageDrawable(getImagePath(list.get(2)));
                                                                                        imagesList.add(getImagePath(list.get(2)));
                                                                                    }

                                                                                } else if (type.equalsIgnoreCase("filling_the_blank")) {
                                                                                    llAnswerView.setVisibility(View.GONE);
                                                                                    llColorView.setVisibility(View.GONE);
                                                                                    ll9PuzzleView.setVisibility(View.GONE);
                                                                                    ll2PuzzleView.setVisibility(View.GONE);
                                                                                    llMisPuzzleView.setVisibility(View.GONE);
                                                                                    llReorderView.setVisibility(View.GONE);
                                                                                    llFillBlankView.setVisibility(View.VISIBLE);
                                                                                    llVoiceView.setVisibility(View.GONE);
                                                                                    llCrossPuzzle.setVisibility(View.GONE);

                                                                                    tvFillBlankQuestion.setText("" + question);

                                                                                    if (imageArray != null && imageArray.length() > 0) {
                                                                                        List<String> list = new Gson().fromJson(imageArray.toString(), new TypeToken<List<String>>() {
                                                                                        }.getType());
                                                                                        ivFillBlankQuestion.setImageDrawable(getImagePath(list.get(0)));
                                                                                    }

                                                                                    if (optionsArray != null && optionsArray.length() > 0) {
                                                                                        List<String> list = new Gson().fromJson(optionsArray.toString(), new TypeToken<List<String>>() {
                                                                                        }.getType());

                                                                                        String[] separated = list.get(0).toString().split(";");
//                                                                            Log.e("separated: ", "" + separated[0] + "==" + separated[1]);
                                                                                        tvFillBlankAnswer1.setText("" + separated[1]);
                                                                                        ivFillBlankAnswer1.setImageDrawable(getImagePath(separated[0]));

                                                                                        String[] separated1 = list.get(1).toString().split(";");
//                                                                            Log.e("separated1: ", "" + separated1[0] + "==" + separated1[1]);
                                                                                        tvFillBlankAnswer2.setText("" + separated1[1]);
                                                                                        ivFillBlankAnswer2.setImageDrawable(getImagePath(separated1[0]));
                                                                                    }

                                                                                } else if (type.equalsIgnoreCase("voice")) {
                                                                                    llAnswerView.setVisibility(View.GONE);
                                                                                    llColorView.setVisibility(View.GONE);
                                                                                    ll9PuzzleView.setVisibility(View.GONE);
                                                                                    ll2PuzzleView.setVisibility(View.GONE);
                                                                                    llMisPuzzleView.setVisibility(View.GONE);
                                                                                    llReorderView.setVisibility(View.GONE);
                                                                                    llFillBlankView.setVisibility(View.GONE);
                                                                                    llVoiceView.setVisibility(View.VISIBLE);
                                                                                    llCrossPuzzle.setVisibility(View.GONE);

//                                                                        tvVoiceQuestion.setText("" + question);

                                                                                } else if (type.equalsIgnoreCase("match_the_following")) {
                                                                                    llAnswerView.setVisibility(View.GONE);
                                                                                    llColorView.setVisibility(View.GONE);
                                                                                    ll9PuzzleView.setVisibility(View.GONE);
                                                                                    ll2PuzzleView.setVisibility(View.GONE);
                                                                                    llMisPuzzleView.setVisibility(View.GONE);
                                                                                    llReorderView.setVisibility(View.GONE);
                                                                                    llFillBlankView.setVisibility(View.GONE);
                                                                                    llVoiceView.setVisibility(View.GONE);
                                                                                    llCrossPuzzle.setVisibility(View.VISIBLE);

                                                                                    tvCrossPuzzleQuestion.setText("" + question);

                                                                                    if (imageArray != null && imageArray.length() > 0) {
                                                                                        List<String> list = new Gson().fromJson(imageArray.toString(), new TypeToken<List<String>>() {
                                                                                        }.getType());
                                                                                        ivCrossPuzzleOne.setImageDrawable(getImagePath(list.get(0)));
                                                                                        ivCrossPuzzleTwo.setImageDrawable(getImagePath(list.get(1)));
                                                                                        ivCrossPuzzleThree.setImageDrawable(getImagePath(list.get(2)));
                                                                                    }

                                                                                    if (optionsArray != null && optionsArray.length() > 0) {
                                                                                        List<String> list = new Gson().fromJson(optionsArray.toString(), new TypeToken<List<String>>() {
                                                                                        }.getType());
                                                                                        ivCrossPuzzleFour.setImageDrawable(getImagePath(list.get(0)));
                                                                                        ivCrossPuzzleFive.setImageDrawable(getImagePath(list.get(1)));
                                                                                        ivCrossPuzzleSix.setImageDrawable(getImagePath(list.get(2)));
                                                                                    }

                                                                                }
                                                                            }
                                                                            Log.e("isAnswer==", "" + isAnswer);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    Drawable getImagePath(String path) {
        Drawable drawable = null;
        if (Functions.getAssetsImage(getActivity(), Constants.ASSETS_ADVISORY_CROP_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + path) != null) {
            drawable = Functions.getAssetsImage(getActivity(), Constants.ASSETS_ADVISORY_CROP_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + path);
        }
        if (Functions.getAssetsImage(getActivity(), Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + path) != null) {
            drawable = Functions.getAssetsImage(getActivity(), Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + path);
        }
        if (Functions.getAssetsImage(getActivity(), Constants.ASSETS_QUIZ_IMAGE_PATH + "/" + path) != null) {
            drawable = Functions.getAssetsImage(getActivity(), Constants.ASSETS_QUIZ_IMAGE_PATH + "/" + path);
        }
        return drawable;
    }

    void showDialog() {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        View sheetView = this.getLayoutInflater().inflate(R.layout.bottom_quiz_thanks_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);

        AppCompatTextView txtOfflineSMS = sheetView.findViewById(R.id.txtOfflineSMS);
        txtOfflineSMS.setVisibility(View.GONE);
        if (InternetConnection.checkConnectionForFasl(getActivity())) {
            txtOfflineSMS.setVisibility(View.GONE);
        } else {
            txtOfflineSMS.setVisibility(View.VISIBLE);
        }

        ImageView ivTagImage = (ImageView) sheetView.findViewById(R.id.ivTagImage);
        TextView tvTagText = (TextView) sheetView.findViewById(R.id.tvTagText);
        TextView tvTagDecs = (TextView) sheetView.findViewById(R.id.tvTagDecs);

        LinearLayout llRewardView = (LinearLayout) sheetView.findViewById(R.id.llRewardView);

        MaterialCardView cardKeyword = (MaterialCardView) sheetView.findViewById(R.id.cardKeyword);
        cardKeyword.setVisibility(View.GONE);
        RelativeLayout rlShareView = (RelativeLayout) sheetView.findViewById(R.id.rlShareView);
        rlShareView.setVisibility(View.GONE);

        TextView tvRewardNew = (TextView) sheetView.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));

        RoundedImageView ivKeyword = (RoundedImageView) sheetView.findViewById(R.id.ivKeyword);
        TextView tvKeywordText = (TextView) sheetView.findViewById(R.id.tvKeywordText);

        if (!TextUtils.isEmpty(keyword_id)) {
            rlShareView.setVisibility(View.GONE);
            cardKeyword.setVisibility(View.VISIBLE);
            try {
                ivKeyword.setImageDrawable(Functions.getAssetsCropImage(getActivity(),
                        Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + imageArray.get(0)));

                JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.
                        loadAdvisoryKeywordJSONFromAsset(getActivity(), keyword_id));
                if (jsonObject != null && jsonObject.length() > 0) {
                    String name = jsonObject.optString("name");

                    Spannable wordtoSpan = new SpannableString(name + getString(R.string.str_more_info_text));
                    wordtoSpan.setSpan(new ForegroundColorSpan(Color.rgb(255, 69, 0)), 0, name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    tvKeywordText.setText(wordtoSpan);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rlShareView.setVisibility(View.VISIBLE);
            cardKeyword.setVisibility(View.GONE);
        }

        cardKeyword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), SavdhaniDetailsActivity.class);
                i.putExtra("keyword_id", keyword_id);
                startActivity(i);
                dismiss();
            }
        });

        if (isAnswerValid) {
            llRewardView.setVisibility(View.VISIBLE);
            ivTagImage.setImageResource(R.mipmap.quiz_yes);
            tvTagText.setText(R.string.str_aapka_utar_sahi_he);
            tvTagText.setTextColor(getResources().getColor(R.color.chat_text));
            tvTagDecs.setVisibility(View.GONE);
            tvTagDecs.setText(R.string.str_aaj_10_reward_jite_he);

            updateQuizResult("1");
        } else {
            llRewardView.setVisibility(View.GONE);
            ivTagImage.setImageResource(R.mipmap.quiz_no);
            tvTagText.setText(R.string.str_utar_galt_he);
            tvTagText.setTextColor(getResources().getColor(R.color.bunai_yes));
            tvTagDecs.setVisibility(View.VISIBLE);
            tvTagDecs.setText(R.string.str_jankari_ke_liye_salah_pade);

            updateQuizResult("0");
        }

        AppCompatTextView tvShareWhatapp = sheetView.findViewById(R.id.tvShareWhatapp);
        tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.shareWhatAppIntent(getActivity());
            }
        });

        AppCompatTextView tvShareCopy = sheetView.findViewById(R.id.tvShareCopy);
        tvShareCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.shareSMSIntent(getActivity(), RongoApp.getReferralText(getActivity()));
            }
        });

        AppCompatTextView tvSharefacebook = sheetView.findViewById(R.id.tvSharefacebook);
        tvSharefacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.shareFacebookIntent(getActivity());
            }
        });

        AppCompatTextView tvShareMore = sheetView.findViewById(R.id.tvShareMore);
        tvShareMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.performShare(getActivity(), appDatabase);
            }
        });

        ImageView ivClose = (ImageView) sheetView.findViewById(R.id.ivClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
                dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
        ((View) sheetView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        mBottomSheetDialog.setCanceledOnTouchOutside(false);
        mBottomSheetDialog.setCancelable(false);
        mBottomSheetDialog.show();
    }

    void showSuccessDialog() {
        Dialog sheetView = new Dialog(getActivity(), R.style.NewDialog);
        sheetView.setContentView(R.layout.bottom_quiz_thanks_new_dialog);
        sheetView.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = sheetView.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);

        ImageView ivClose = (ImageView) sheetView.findViewById(R.id.ivClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                sheetView.dismiss();
            }
        });

        AppCompatTextView txtOfflineSMS = sheetView.findViewById(R.id.txtOfflineSMS);
        txtOfflineSMS.setVisibility(View.GONE);
        if (InternetConnection.checkConnectionForFasl(getActivity())) {
            txtOfflineSMS.setVisibility(View.GONE);
        } else {
            txtOfflineSMS.setVisibility(View.VISIBLE);
        }

        SuccessGifView ivSuccessGif = (SuccessGifView) sheetView.findViewById(R.id.ivSuccessGif);
        ImageView ivTagImage = (ImageView) sheetView.findViewById(R.id.ivTagImage);
        TextView tvTagText = (TextView) sheetView.findViewById(R.id.tvTagText);
        TextView tvTagDecs = (TextView) sheetView.findViewById(R.id.tvTagDecs);

        LinearLayout llRewardView = (LinearLayout) sheetView.findViewById(R.id.llRewardView);

        MaterialCardView cardKeyword = (MaterialCardView) sheetView.findViewById(R.id.cardKeyword);
        cardKeyword.setVisibility(View.GONE);
        cardKeyword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), SavdhaniDetailsActivity.class);
                i.putExtra("keyword_id", keyword_id);
                startActivity(i);
                dismiss();
            }
        });

        LinearLayout cardSavdhani = (LinearLayout) sheetView.findViewById(R.id.cardSavdhani);
        cardSavdhani.setVisibility(View.GONE);
        cardSavdhani.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), RewardActivity.class);
                startActivity(intent);
                sheetView.dismiss();
                dismiss();
            }
        });

        RelativeLayout rlShareView = (RelativeLayout) sheetView.findViewById(R.id.rlShareView);
        rlShareView.setVisibility(View.GONE);

        LinearLayout llSeeAdvisory = (LinearLayout) sheetView.findViewById(R.id.llSeeAdvisory);
        llSeeAdvisory.setVisibility(View.GONE);
        llSeeAdvisory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sheetView.dismiss();
                dismiss();
            }
        });

        RoundedImageView ivKeyword = (RoundedImageView) sheetView.findViewById(R.id.ivKeyword);
        TextView tvKeywordText = (TextView) sheetView.findViewById(R.id.tvKeywordText);

        if (isAnswerValid) {
            cardSavdhani.setVisibility(View.VISIBLE);
            rlShareView.setVisibility(View.GONE);
            cardKeyword.setVisibility(View.GONE);
            llRewardView.setVisibility(View.VISIBLE);
            ivSuccessGif.setVisibility(View.VISIBLE);
            ivTagImage.setVisibility(View.GONE);
            tvTagText.setText(R.string.str_aapka_utar_sahi_he);
//            tvTagText.setTextColor(getResources().getColor(R.color.chat_text));
            tvTagDecs.setVisibility(View.GONE);
            tvTagDecs.setText(R.string.str_aaj_10_reward_jite_he);

            updateQuizResult("1");
        } else {
            cardSavdhani.setVisibility(View.GONE);
            llRewardView.setVisibility(View.GONE);
            ivSuccessGif.setVisibility(View.GONE);
            ivTagImage.setVisibility(View.VISIBLE);
            ivTagImage.setImageResource(R.mipmap.quiz_no);
            tvTagText.setText(R.string.str_utar_galt_he);
//            tvTagText.setTextColor(getResources().getColor(R.color.bunai_yes));
            tvTagDecs.setVisibility(View.VISIBLE);
//            tvTagDecs.setText(R.string.str_jankari_ke_liye_salah_pade);
            tvTagDecs.setText(R.string.str_get_more_reward);

            updateQuizResult("0");

            if (!TextUtils.isEmpty(keyword_id)) {
                llSeeAdvisory.setVisibility(View.GONE);
                cardKeyword.setVisibility(View.VISIBLE);
                try {
                    ivKeyword.setImageDrawable(Functions.getAssetsCropImage(getActivity(),
                            Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH + RongoApp.getSelectedCrop() + "/" + imageArray.get(0)));

                    JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.
                            loadAdvisoryKeywordJSONFromAsset(getActivity(), keyword_id));
                    if (jsonObject != null && jsonObject.length() > 0) {
                        String name = jsonObject.optString("name");
                        Spannable wordtoSpan = new SpannableString(name + getString(R.string.str_bare_text));
                        wordtoSpan.setSpan(new ForegroundColorSpan(Color.rgb(255, 69, 0)), 0, name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                        tvKeywordText.setText(wordtoSpan);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                llSeeAdvisory.setVisibility(View.VISIBLE);
                cardKeyword.setVisibility(View.GONE);
            }
        }
    }


    void updateQuizResult(final String status) {
        try {
            Functions.setFirebaseLogEventTrack(getActivity(), Constants.Click_Event, "Daily Quiz", status, "updateQuizResult");

            String answer = answersList.toString().replace("[", "").replace("]", "");

            if (InternetConnection.checkConnectionForFasl(getActivity())) {
                RetrofitClient.getInstance().getApi().updateDailyQuizResponse(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                        RongoApp.getAppVersion(), SharedPreferencesUtils.getUserId(getActivity()),
                        Functions.getDeviceid(getActivity()), QuizId, RongoApp.getSelectedCrop(),
                        RongoApp.getSelectedPCrop(), sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0"),
                        sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, "0"),
                        answer, status, getDailyQuizRewardPoint(status)).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response != null && response.isSuccessful()) {
                            setDailyQuizStatus(status);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
            } else {
                setDailyQuizStatus(status);

                ArrayList<OfflineQuizData> offlineQuizData = new ArrayList<OfflineQuizData>();
                OfflineQuizData offlineQuiz = new OfflineQuizData();
                offlineQuiz.set_id(0);
                offlineQuiz.setQuizID(QuizId);
                offlineQuiz.setCropId(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""));
                offlineQuiz.setPcropId(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""));
                offlineQuiz.setCropWeek(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""));
                offlineQuiz.setCropDay(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, ""));
                offlineQuiz.setAnsValue(answer);
                offlineQuiz.setIs_true(status);
                offlineQuiz.setMetaData(getDailyQuizRewardPoint(status));
                offlineQuizData.add(offlineQuiz);
                appDatabase.offlineQuizDao().insertAll(offlineQuizData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setDailyQuizStatus(String status) {
        try {
            String answer = answersList.toString().replace("[", "").replace("]", "");
            ArrayList<DailyQuizStatusData> dailyQuizStatusData = new ArrayList<DailyQuizStatusData>();
            DailyQuizStatusData dailyQuizStatus = new DailyQuizStatusData();
            dailyQuizStatus.set_id(0);
            dailyQuizStatus.setQuizID(QuizId);
            dailyQuizStatus.setCropId(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""));
            dailyQuizStatus.setPcropId(sharedPreferences.getString(Constants.SharedPreferences_P_CROP_ID, ""));
            dailyQuizStatus.setCropWeek(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, ""));
            dailyQuizStatus.setCropDay(sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK_DAY, ""));
            dailyQuizStatus.setAnsValue(answer);
            dailyQuizStatus.setMetaData(getDailyQuizRewardPoint(status));
            dailyQuizStatusData.add(dailyQuizStatus);
            appDatabase.dailyQuizStatusDao().insertAll(dailyQuizStatusData);

            Intent intent = new Intent();
            intent.putExtra(Constants.STORY_NAME, Constants.STORY_TEST);
            intent.setAction(Constants.BROADCAST_RECEIVER.UPDATE_STORY);
            getActivity().sendBroadcast(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String getDailyQuizRewardPoint(String status) {
        String rewardPoint = "";
        try {
            if (status.equalsIgnoreCase("1")) {
                JSONArray jsonArray = new JSONArray();
                JSONObject obj = new JSONObject();
                obj.put("type", "point");
                obj.put("value", RewardPoints.getDailyQuizRewardPoint(sharedPreferences));
                jsonArray.put(obj);
                rewardPoint = jsonArray.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rewardPoint;
    }

    void getQuizLeaderboard() {
        try {
            if (InternetConnection.checkConnection(getActivity())) {

                ProgressBarDialog progressBarDialog = new ProgressBarDialog(getActivity());
                progressBarDialog.showProgressDialogWithTitle(getResources().getString(R.string.app_name));

                RetrofitClient.getInstance().getApi().getQuizLeaderboard(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                        SharedPreferencesUtils.getUserId(getActivity())).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response != null) {
                            try {
                                quizAnswerListModels = new ArrayList<QuizAnswerListModel>();
                                String responseStrig = response.body().string();
                                if (!TextUtils.isEmpty(responseStrig)) {
                                    JSONObject jb = new JSONObject(responseStrig);
                                    if (jb != null && jb.length() > 0) {
                                        if (jb.optString("status_code").equalsIgnoreCase("0")) {
                                            JSONObject jData = jb.optJSONObject("data");
                                            if (jData != null && jData.length() > 0) {
                                                totalAns = jData.optString("user");
                                                JSONArray jallResult = jData.optJSONArray("all");
                                                if (jallResult != null && jallResult.length() > 0) {
                                                    for (int i = 0; i < jallResult.length(); i++) {
                                                        JSONObject jBall = jallResult.optJSONObject(i);
                                                        String user_id = jBall.optString("user_id");
                                                        String first_name = jBall.optString("first_name");
                                                        String location_city = jBall.optString("location_city");
                                                        String location_state = jBall.optString("location_state");
                                                        String total = jBall.optString("total");
                                                        if (!TextUtils.isEmpty(first_name) && !first_name.equalsIgnoreCase("null")) {
                                                            quizAnswerListModels.add(new QuizAnswerListModel(user_id,
                                                                    first_name, location_state, location_city, total));
                                                        }
                                                    }
                                                }

                                                progressBarDialog.hideProgressDialogWithTitle();
                                                LeaderBoardDialog();
                                            }
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                progressBarDialog.hideProgressDialogWithTitle();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        progressBarDialog.hideProgressDialogWithTitle();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void LeaderBoardDialog() {
        try {
            Dialog dialog = new Dialog(getActivity(), R.style.NewDialog);
            dialog.setContentView(R.layout.leader_board_dialog);
            dialog.show();

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            Window window = dialog.getWindow();
            lp.copyFrom(window.getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.MATCH_PARENT;
            window.setAttributes(lp);

            ImageView ivBack = dialog.findViewById(R.id.ivBack);
            ivBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            ImageView iv100 = dialog.findViewById(R.id.iv100);
            ImageView iv75 = dialog.findViewById(R.id.iv75);
            ImageView iv50 = dialog.findViewById(R.id.iv50);
            ImageView iv25 = dialog.findViewById(R.id.iv25);

            TextView tvTotalPoints = (TextView) dialog.findViewById(R.id.tvTotalPoints);
            TextView tvTotalAns = (TextView) dialog.findViewById(R.id.tvTotalAns);
            tvTotalAns.setText("" + totalAns);
            tvTotalPoints.setText("" + (Integer.parseInt(totalAns) * 10));

            if (Integer.parseInt(totalAns) >= 25) {
                iv25.setImageResource(R.mipmap.r1);
            }
            if (Integer.parseInt(totalAns) >= 50) {
                iv50.setImageResource(R.mipmap.r2);
            }
            if (Integer.parseInt(totalAns) >= 75) {
                iv75.setImageResource(R.mipmap.r3);
            }
            if (Integer.parseInt(totalAns) >= 100) {
                iv100.setImageResource(R.mipmap.r4);
            }

            TextView tvName = (TextView) dialog.findViewById(R.id.tvName);
            TextView tvLocation = (TextView) dialog.findViewById(R.id.tvLocation);
            TextView tvTotal = (TextView) dialog.findViewById(R.id.tvTotal);
            tvTotal.setText(quizAnswerListModels.get(0).getTotal() + " " + getString(R.string.str_sahi_uttar));
            tvName.setText(quizAnswerListModels.get(0).getFirst_name());
            tvLocation.setText(stateEnglishToHindWord(quizAnswerListModels.get(0).getLocation_state()) + ", " +
                    cityEnglishToHindWord(quizAnswerListModels.get(0).getLocation_state(), quizAnswerListModels.get(0).getLocation_city()));

            TextView tvName1 = (TextView) dialog.findViewById(R.id.tvName1);
            TextView tvLocation1 = (TextView) dialog.findViewById(R.id.tvLocation1);
            TextView tvTotal1 = (TextView) dialog.findViewById(R.id.tvTotal1);
            tvTotal1.setText(quizAnswerListModels.get(1).getTotal() + " " + getString(R.string.str_sahi_uttar));
            tvName1.setText(quizAnswerListModels.get(1).getFirst_name());
            tvLocation1.setText(stateEnglishToHindWord(quizAnswerListModels.get(1).getLocation_state()) + ", " +
                    cityEnglishToHindWord(quizAnswerListModels.get(1).getLocation_state(), quizAnswerListModels.get(1).getLocation_city()));

            TextView tvName2 = (TextView) dialog.findViewById(R.id.tvName2);
            TextView tvLocation2 = (TextView) dialog.findViewById(R.id.tvLocation2);
            TextView tvTotal2 = (TextView) dialog.findViewById(R.id.tvTotal2);
            tvTotal2.setText(quizAnswerListModels.get(2).getTotal() + " " + getString(R.string.str_sahi_uttar));
            tvName2.setText(quizAnswerListModels.get(2).getFirst_name());
            tvLocation2.setText(stateEnglishToHindWord(quizAnswerListModels.get(2).getLocation_state()) + ", " +
                    cityEnglishToHindWord(quizAnswerListModels.get(2).getLocation_state(), quizAnswerListModels.get(2).getLocation_city()));

            RecyclerView rcvList = dialog.findViewById(R.id.rcvList);
            rcvList.setLayoutManager(new LinearLayoutManager(getActivity()));
            rcvList.setAdapter(new QuizAnsListAdapter(quizAnswerListModels));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class QuizAnsListAdapter extends RecyclerView.Adapter<QuizAnsListAdapter.ViewHolder> {
        List<QuizAnswerListModel> listdata;

        public QuizAnsListAdapter(List<QuizAnswerListModel> listdata) {
            this.listdata = listdata;
        }

        @NotNull
        @Override
        public QuizAnsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View listItem = layoutInflater.inflate(R.layout.quiz_ans_list_item, parent, false);
            QuizAnsListAdapter.ViewHolder viewHolder = new QuizAnsListAdapter.ViewHolder(listItem);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(final QuizAnsListAdapter.ViewHolder holder, final int position) {
            try {
//                holder.tvTotal.setPaintFlags(holder.tvTotal.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                holder.tvTotal.setText(listdata.get(position).getTotal() + " सही उत्तर");
                holder.tvName.setText(listdata.get(position).getFirst_name());
                holder.tvLocation.setText("पटना, बिहार");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return listdata.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvTotal, tvName, tvLocation;

            public ViewHolder(View itemView) {
                super(itemView);
                this.tvName = (TextView) itemView.findViewById(R.id.tvName);
                this.tvLocation = (TextView) itemView.findViewById(R.id.tvLocation);
                this.tvTotal = (TextView) itemView.findViewById(R.id.tvTotal);
            }
        }
    }

    void parseStateCityListData() {
        try {
            String stateCityData = sharedPreferences.getString(Constants.SharedPreferences_StateCityData, "");
            if (!TextUtils.isEmpty(stateCityData)) {
                JSONObject jsonObject = new JSONObject(stateCityData);
                if (jsonObject != null & jsonObject.length() > 0) {
                    JSONObject data = jsonObject.optJSONObject("data");
                    if (data != null && data.length() > 0) {
                        stateCityDataModels = new ArrayList<StateCityDataModel>();
                        JSONArray result1 = data.optJSONArray("result");
                        if (result1 != null && result1.length() > 0) {
                            JSONArray result = new JSONArray(result1.optJSONObject(0).optString(RongoApp.getDefaultLanguage()));
                            for (int i = 0; i < result.length(); i++) {
                                JSONObject object = result.optJSONObject(i);

                                JSONObject state = object.optJSONObject("state");
                                if (state != null && state.length() > 0) {
                                    parseJsonState(i, state);
                                }

                                JSONObject district = object.optJSONObject("district");
                                if (district != null && district.length() > 0) {
                                    parseJsonCity(i, district);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void parseJsonState(int id, JSONObject data) {
        StateCityDataModel stateCityDataModel = new StateCityDataModel();
        if (data != null) {
            Iterator<String> it = data.keys();
            while (it.hasNext()) {
                String key = it.next();
                try {
                    if (data.get(key) instanceof JSONArray) {
                        JSONArray arry = data.getJSONArray(key);
                        int size = arry.length();
                        for (int i = 0; i < size; i++) {
                            parseJsonState(i, arry.getJSONObject(i));
                        }
                    } else if (data.get(key) instanceof JSONObject) {
                        parseJsonState(0, data.getJSONObject(key));
                    } else {
//                        System.out.println("" + key + "-" + id + " : " + data.optString(key));
                        stateCityDataModel.setId(String.valueOf(id));
                        stateCityDataModel.setState_english(key);
                        stateCityDataModel.setState_hindi(data.optString(key));
                        stateCityDataModels.add(stateCityDataModel);
                    }
                } catch (Throwable e) {
//                    System.out.println("" + key + " : " + data.optString(key));
                    e.printStackTrace();
                }
            }
        }
    }

    void parseJsonCity(int id, JSONObject data) {
        ArrayList<StateCityDataModel.CityListModel> cityListModels = new ArrayList<>();
        if (data != null) {
            Iterator<String> it = data.keys();
            while (it.hasNext()) {
                String key = it.next();
                try {
                    if (data.get(key) instanceof JSONArray) {
                        JSONArray arry = data.getJSONArray(key);
                        int size = arry.length();
                        for (int i = 0; i < size; i++) {
                            parseJsonState(i, arry.getJSONObject(i));
                        }
                    } else if (data.get(key) instanceof JSONObject) {
                        parseJsonState(0, data.getJSONObject(key));
                    } else {
//                        System.out.println("" + key + "-" + id + " : " + data.optString(key));
                        StateCityDataModel.CityListModel cityListModel = new StateCityDataModel.CityListModel();
                        cityListModel.setId(String.valueOf(id));
                        cityListModel.setCity_english(key);
                        cityListModel.setCity_hindi(data.optString(key));
                        cityListModels.add(cityListModel);
                    }
                } catch (Throwable e) {
//                    System.out.println("" + key + " : " + data.optString(key));
                    e.printStackTrace();
                }
            }
            stateCityDataModels.get(id).setCityList(cityListModels);
        }
    }

    String stateEnglishToHindWord(String state) {
        String stateHindi = state;
        try {
            if (!TextUtils.isEmpty(state) && stateCityDataModels.size() > 0) {
                for (int i = 0; i < stateCityDataModels.size(); i++) {
                    if (state.equalsIgnoreCase(stateCityDataModels.get(i).getState_english())) {
                        stateHindi = stateCityDataModels.get(i).getState_hindi();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stateHindi;
    }

    String cityEnglishToHindWord(String state, String city) {
        String cityHindi = city;
        try {
            if (!TextUtils.isEmpty(state) && stateCityDataModels.size() > 0) {
                for (int i = 0; i < stateCityDataModels.size(); i++) {
                    if (state.equalsIgnoreCase(stateCityDataModels.get(i).getState_english())) {
                        for (int j = 0; j < stateCityDataModels.get(i).getCityList().size(); j++) {
                            if (city.equalsIgnoreCase(stateCityDataModels.get(i).getCityList().get(j).getCity_english())) {
                                cityHindi = stateCityDataModels.get(i).getCityList().get(j).getCity_hindi();
                            }
                        }
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cityHindi;
    }

}