package com.rongoapp.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.DialogFragment;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.card.MaterialCardView;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedImageView;
import com.rongoapp.R;
import com.rongoapp.activities.HomeActivity;
import com.rongoapp.activities.PostQuestionActivity;
import com.rongoapp.activities.QuestionSampleActivity;
import com.rongoapp.activities.QuizActivity;
import com.rongoapp.activities.RewardActivity;
import com.rongoapp.activities.SavdhaniDetailsActivity;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.FetchJsonFromAssetsFile;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.GlideApp;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.StoryData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.storyProgressView.StoriesProgressView;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StorysFragment extends DialogFragment implements StoriesProgressView.StoriesListener {

    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    View view;
    int storyId = 0;
    String storyType = "1", cardType = "1", image_path = "", action_param = "";

    List<StoryData.Story> storyData;
    int cardDuration[] = new int[]{};
    List<StoryData.Card> storyCardDataList;

    ProgressBar progressBar;
    LinearLayout llStoryView, llEditStoryView;
    RelativeLayout rlMainView;
    StoriesProgressView storiesProgressView;
    RoundedImageView ivClose, ivSend;
    ImageView ivEditStoryView, ivStoryView;
    TextView ivEditStoryTitle, ivEditStoryDecs, ivEditStoryMeta, tvTitleSend, tvQuestions, tvOne, tvTwo,
            tvThree, tvFour, tvTitle, tvSubTitle, tvBack;
    MaterialCardView cardRedirect, cardQuestion;
    AppCompatTextView tvNoDataAvailable;

    int counter = 0, storyCounter = 0;
    long pressTime = 0L;
    long limit = 500L;
    View.OnTouchListener onTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    pressTime = System.currentTimeMillis();
                    storiesProgressView.pause();
                    return false;
                case MotionEvent.ACTION_UP:
                    long now = System.currentTimeMillis();
                    storiesProgressView.resume();
                    return limit < now - pressTime;
            }
            return false;
        }
    };

    @Override
    public int getTheme() {
        return R.style.FullScreenDialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.FullScreenDialog);

        sharedPreferences = getActivity().getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(getActivity());

        RongoApp.logVisitedPageTabEvent("Story");
        RongoApp.engagementToolActivity(Constants.ET_RONGO);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public static StorysFragment newInstance() {
        StorysFragment fragment = new StorysFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_story_dialog, container, false);

        tvNoDataAvailable = (AppCompatTextView) view.findViewById(R.id.tvNoDataAvailable);
        tvNoDataAvailable.setVisibility(View.GONE);

        ivClose = (RoundedImageView) view.findViewById(R.id.ivClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        setUserStoryData();
        return view;
    }

    void setStoryView() {
        try {
            if (storyData.size() > 0) {
                tvNoDataAvailable.setVisibility(View.GONE);
                storyCounter = 0;
                String storyCounterData = sharedPreferences.getString(Constants.SharedPreferences_story_count, "0");
                if (!TextUtils.isEmpty(storyCounterData)) {
                    if (storyData.size() > Integer.parseInt(storyCounterData)) {
                        storyCounter = Integer.parseInt(storyCounterData);
                    }
                }
                storyData.get(storyCounter).setStorySelected("1");
                cardType = storyData.get(storyCounter).getStoryType();
                storyId = Integer.parseInt(storyData.get(storyCounter).getStoryId());
                storyType = storyData.get(storyCounter).getStoryType();
                action_param = storyData.get(storyCounter).getActionParam();

                setStoryCardView();
            } else {
                tvNoDataAvailable.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int[] add_element(int n, int myarray[], int ele) {
        int i;
        int newArray[] = new int[n + 1];
        for (i = 0; i < n; i++)
            newArray[i] = myarray[i];

        newArray[n] = (int) TimeUnit.SECONDS.toMillis(ele);

        return newArray;
    }

    void setStoryCardView() {
        try {
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_story_count, "" + storyCounter);
            storyCardDataList = new ArrayList<>();
            storyCardDataList.addAll(storyData.get(storyCounter).getCards());

            counter = 0;
            cardDuration = new int[]{};
            for (int i = 0; i < storyCardDataList.size(); i++) {
                String image = image_path + storyCardDataList.get(i).getCardMediaPath();
                cardDuration = add_element(i, cardDuration, Integer.parseInt(storyCardDataList.get(i).getCardDuration()));
            }

            storiesProgressView = (StoriesProgressView) view.findViewById(R.id.stories);
            storiesProgressView.setStoriesCount(storyCardDataList.size());
            storiesProgressView.setStoriesCountWithDuration(cardDuration);
            storiesProgressView.setStoriesListener(this);
            storiesProgressView.startStories(counter);

            rlMainView = (RelativeLayout) view.findViewById(R.id.rlMainView);

            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            tvSubTitle = (TextView) view.findViewById(R.id.tvSubTitle);
            tvSubTitle.setText("" + storyData.get(storyCounter).getDescription());

            tvBack = view.findViewById(R.id.tvBack);
            tvBack.setVisibility(View.GONE);
            tvBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if ((counter - 1) < 0) {
                        if ((storyCounter - 1) < 0) {
                            tvBack.setVisibility(View.GONE);
                        } else {
                            storyCounter = --storyCounter;
                            setStoryCardView();
                        }
                    } else {
                        storiesProgressView.reverse();
                    }
                }
            });

            TextView tvNext = view.findViewById(R.id.tvNext);
            tvNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (storyCardDataList.size() == (counter + 1)) {
                        if (storyData.size() == (storyCounter + 1)) {
                            Intent intent = new Intent();
                            intent.setAction(Constants.BROADCAST_RECEIVER.UPDATE_STORY);
                            intent.putExtra(Constants.STORY_NAME, Constants.STORY_QUIZ);
                            getActivity().sendBroadcast(intent);
                            dismiss();
                        } else {
                            storyCounter = ++storyCounter;
                            setStoryCardView();
                        }
                    } else {
                        storiesProgressView.skip();
                    }
                }
            });

            cardQuestion = (MaterialCardView) view.findViewById(R.id.cardQuestion);
            cardQuestion.setVisibility(View.GONE);

            tvQuestions = (TextView) view.findViewById(R.id.tvQuestions);
            tvOne = (TextView) view.findViewById(R.id.tvOne);
            tvOne.setVisibility(View.GONE);
            tvTwo = (TextView) view.findViewById(R.id.tvTwo);
            tvTwo.setVisibility(View.GONE);
            tvThree = (TextView) view.findViewById(R.id.tvThree);
            tvThree.setVisibility(View.GONE);
            tvFour = (TextView) view.findViewById(R.id.tvFour);
            tvFour.setVisibility(View.GONE);

            cardRedirect = (MaterialCardView) view.findViewById(R.id.cardRedirect);
            cardRedirect.setVisibility(View.GONE);
            ivSend = (RoundedImageView) view.findViewById(R.id.ivSend);
            tvTitleSend = (TextView) view.findViewById(R.id.tvTitleSend);

            cardRedirect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        // Type: community_post, keyword_detail, community, expert_advice, daily_quiz, reward;
                        JSONObject result = new JSONObject("" + action_param);
                        if (result != null && result.length() > 0) {
                            String key = result.optString("page");
                            String data = result.optString("data");
                            Log.e("data:", "" + key + "-" + data);

                            if (key.equalsIgnoreCase("community_post")) {

                            } else if (key.equalsIgnoreCase("keyword_detail")) {
                                Intent i = new Intent(getActivity(), SavdhaniDetailsActivity.class);
                                i.putExtra("keyword_id", "" + data);
                                startActivity(i);
                                storiesProgressView.destroy();
                                dismiss();
                            } else if (key.equalsIgnoreCase("community")) {
                                Intent i = new Intent(getActivity(), HomeActivity.class);
                                i.putExtra("pageType", "2");
                                i.putExtra("fromLaunch", "Y");
                                startActivity(i);
                                storiesProgressView.destroy();
                                dismiss();
                            } else if (key.equalsIgnoreCase("expert_advice")) {
                                Intent i = new Intent(getActivity(), PostQuestionActivity.class);
                                startActivity(i);
                                storiesProgressView.destroy();
                                dismiss();
                            } else if (key.equalsIgnoreCase("daily_quiz")) {
                                Intent intent = null;
                                try {
                                    if (RongoApp.getSelectedCrop().equalsIgnoreCase("11")) {
                                        intent = new Intent(getActivity(), QuestionSampleActivity.class);
                                    } else {
                                        if (Functions.getQuizIntent(getActivity(), sharedPreferences)) {
                                            intent = new Intent(getActivity(), QuizActivity.class);
                                        } else {
                                            intent = new Intent(getActivity(), QuestionSampleActivity.class);
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                startActivity(intent);
                                storiesProgressView.destroy();
                                dismiss();
                            } else if (key.equalsIgnoreCase("reward")) {
                                Intent i = new Intent(getActivity(), RewardActivity.class);
                                startActivity(i);
                                storiesProgressView.destroy();
                                dismiss();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            llStoryView = (LinearLayout) view.findViewById(R.id.llStoryView);
            llStoryView.setVisibility(View.GONE);

            llEditStoryView = (LinearLayout) view.findViewById(R.id.llEditStoryView);
            llEditStoryView.setVisibility(View.GONE);

            View reverse = view.findViewById(R.id.reverse);
            reverse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    storiesProgressView.reverse();
                }
            });
            reverse.setOnTouchListener(onTouchListener);

            View skip = view.findViewById(R.id.skip);
            skip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    storiesProgressView.skip();
                }
            });
            skip.setOnTouchListener(onTouchListener);

            setUpdatedCardView();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setUpdatedCardView() {
        try {
            Functions.setFirebaseLogEventTrack(getActivity(), Constants.ET_STORY_TRACK, Constants.ET_RONGO,
                    "" + RongoApp.getUserId(), "Story_" + storyData.get(storyCounter).getStoryId() + "_" + counter);
            Functions.setUserScreenTrackEvent(appDatabase, Constants.ET_STORY_TRACK, Constants.ET_RONGO + "_" + RongoApp.getUserId());

            if (counter > 1 || storyCounter > 0) {
                tvBack.setVisibility(View.VISIBLE);
            }

//            Card Types: 1 - Only Image, 2 - Custom View
            if (storyCardDataList.get(counter).getCardType().equalsIgnoreCase("1")) {
                llStoryView.setVisibility(View.VISIBLE);
                llEditStoryView.setVisibility(View.GONE);
            } else {
                llStoryView.setVisibility(View.GONE);
                llEditStoryView.setVisibility(View.VISIBLE);
            }

            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            progressBar.setVisibility(View.GONE);

            ivStoryView = (ImageView) view.findViewById(R.id.ivStoryView);
            GlideApp.with(getActivity()).load(image_path + storyCardDataList.get(counter).getCardMediaPath())
                    .diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(false).error(R.drawable.story_placeholder)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .placeholder(R.drawable.story_placeholder).into(ivStoryView);

//            tvTitle.setTextColor(getDominantColor(imaArrayList.get(counter)));
//            rlMainView.setBackgroundColor(getDominantColor(imaArrayList.get(counter)));

            ivEditStoryView = (ImageView) view.findViewById(R.id.ivEditStoryView);
            GlideApp.with(getActivity()).load(image_path + storyCardDataList.get(counter).getCardMediaPath())
                    .diskCacheStrategy(DiskCacheStrategy.DATA).skipMemoryCache(false).error(R.drawable.story_placeholder)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .placeholder(R.drawable.story_placeholder).into(ivEditStoryView);

            ivEditStoryTitle = (AppCompatTextView) view.findViewById(R.id.ivEditStoryTitle);
            ivEditStoryDecs = (AppCompatTextView) view.findViewById(R.id.ivEditStoryDecs);
            ivEditStoryMeta = (AppCompatTextView) view.findViewById(R.id.ivEditStoryMeta);

            if (!TextUtils.isEmpty(storyCardDataList.get(counter).getCardDescription())) {
                JSONObject result = new JSONObject("" + storyCardDataList.get(counter).getCardDescription());
                if (result != null && result.length() > 0) {
                    String key = result.getString(RongoApp.getDefaultLanguage());
                    JSONObject jsonObject = result.optJSONObject(RongoApp.getDefaultLanguage());
//                Log.e("data1:", "" + jsonObject.optString("meta"));
                    ivEditStoryTitle.setText(jsonObject.optString("card_title"));
                    ivEditStoryMeta.setText(jsonObject.optString("card_meta"));
                    ivEditStoryDecs.setText(jsonObject.optString("card_description"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getDominantColor(Bitmap bitmap) {
        Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, 1, 1, true);
        final int color = newBitmap.getPixel(0, 0);
        newBitmap.recycle();
        return color;
    }

    @Override
    public void onNext() {
        counter = ++counter;
        setUpdatedCardView();
    }

    @Override
    public void onPrev() {
        if ((counter - 1) < 0) return;
        counter = --counter;
        setUpdatedCardView();
    }

    @Override
    public void onComplete() {
        if (storyType.equalsIgnoreCase("3")) {
            cardQuestion.setVisibility(View.VISIBLE);
            try {
                JSONObject result = new JSONObject("" + action_param);
                if (result != null && result.length() > 0) {
                    String question = result.optString("question");
                    String answer = result.optString("answer");
                    JSONArray options = result.optJSONArray("options");
                    tvQuestions.setText(question);
                    if (options != null && options.length() > 0) {
                        if (options.length() == 1) {
                            tvOne.setText(options.optString(0));
                            tvOne.setVisibility(View.VISIBLE);
                        }
                        if (options.length() == 2) {
                            tvOne.setText(options.optString(0));
                            tvTwo.setText(options.optString(1));
                            tvOne.setVisibility(View.VISIBLE);
                            tvTwo.setVisibility(View.VISIBLE);
                        }
                        if (options.length() == 3) {
                            tvOne.setText(options.optString(0));
                            tvTwo.setText(options.optString(1));
                            tvThree.setText(options.optString(2));
                            tvOne.setVisibility(View.VISIBLE);
                            tvTwo.setVisibility(View.VISIBLE);
                            tvThree.setVisibility(View.VISIBLE);
                        }
                        if (options.length() == 4) {
                            tvOne.setText(options.optString(0));
                            tvTwo.setText(options.optString(1));
                            tvThree.setText(options.optString(2));
                            tvFour.setText(options.optString(3));
                            tvOne.setVisibility(View.VISIBLE);
                            tvTwo.setVisibility(View.VISIBLE);
                            tvThree.setVisibility(View.VISIBLE);
                            tvFour.setVisibility(View.VISIBLE);
                        }
                    }

                    tvOne.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            submitStoryAnswer(tvOne.getText().toString());
                        }
                    });
                    tvTwo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            submitStoryAnswer(tvTwo.getText().toString());
                        }
                    });
                    tvThree.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            submitStoryAnswer(tvThree.getText().toString());
                        }
                    });
                    tvFour.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            submitStoryAnswer(tvFour.getText().toString());
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (storyType.equalsIgnoreCase("4")) {
            cardRedirect.setVisibility(View.VISIBLE);
            try {
                //       Type: community_post, keyword_detail, community, expert_advice, daily_quiz, reward;
                Log.e("data:", "" + action_param);
                JSONObject result = new JSONObject("" + action_param);
                if (result != null && result.length() > 0) {
                    String key = result.optString("page");
                    String data = result.optString("data");
                    Log.e("data:", "" + key + "-" + data);
                    if (key.equalsIgnoreCase("community_post")) {
                        tvTitleSend.setText(getActivity().getString(R.string.str_adhik_jane));
                        ivSend.setImageResource(R.mipmap.cb);
                    } else if (key.equalsIgnoreCase("keyword_detail")) {
                        tvTitleSend.setText(getActivity().getString(R.string.str_adhik_jane));
                        ivSend.setImageResource(R.mipmap.story_redi);
                    } else if (key.equalsIgnoreCase("community")) {
                        tvTitleSend.setText(getActivity().getString(R.string.str_adhik_jane));
                        ivSend.setImageResource(R.mipmap.cb);
                    } else if (key.equalsIgnoreCase("expert_advice")) {
                        tvTitleSend.setText(getActivity().getString(R.string.str_prachan_puche));
                        ivSend.setImageResource(R.mipmap.advisory_logo);
                    } else if (key.equalsIgnoreCase("daily_quiz")) {
                        tvTitleSend.setText(getActivity().getString(R.string.str_prachan_dekhe));
                        ivSend.setImageResource(R.mipmap.quiz_icon);
                    } else if (key.equalsIgnoreCase("reward")) {
                        tvTitleSend.setText(getActivity().getString(R.string.str_reward_dekhe));
                        ivSend.setImageResource(R.mipmap.reward);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
//            showDialog();
//            dismiss();

            if (storyData.size() != (storyCounter + 1)) {
                storyCounter = ++storyCounter;
                setStoryCardView();
            } else {
                SharedPreferencesUtils.storeStateOfString(sharedPreferences,
                        Constants.SharedPreferences_story_count, "0");
            }
        }
        Log.e("onComplete=", "=" + storyType);
    }

    @Override
    public void onDestroy() {
        if (storiesProgressView != null)
            storiesProgressView.destroy();
        super.onDestroy();
    }


    void showDialog() {
        try {
            BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
            View sheetView = this.getLayoutInflater().inflate(R.layout.bottom_story_thanks_dialog, null);
            mBottomSheetDialog.setContentView(sheetView);

            TextView tvTagDecs = (TextView) sheetView.findViewById(R.id.tvTagDecs);
            LinearLayout llRewardView = (LinearLayout) sheetView.findViewById(R.id.llRewardView);

            MaterialCardView cardKeyword = (MaterialCardView) sheetView.findViewById(R.id.cardKeyword);
            cardKeyword.setVisibility(View.GONE);

            MaterialCardView questionCard = (MaterialCardView) sheetView.findViewById(R.id.questionCard);
            questionCard.setVisibility(View.GONE);

            RelativeLayout rlShareView = (RelativeLayout) sheetView.findViewById(R.id.rlShareView);
            rlShareView.setVisibility(View.VISIBLE);

            TextView tvRewardNew = (TextView) sheetView.findViewById(R.id.tvRewardNew);
            tvRewardNew.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));

            TextView tvRewardPoint1 = (TextView) sheetView.findViewById(R.id.tvRewardPoint1);
            tvRewardPoint1.setText(RewardPoints.getInviteRewardPoint(sharedPreferences));

            TextView tvQuestions = (TextView) sheetView.findViewById(R.id.tvQuestions);
            TextView tvHa = (TextView) sheetView.findViewById(R.id.tvHa);
            TextView tvNa = (TextView) sheetView.findViewById(R.id.tvNa);
            TextView tvRewardPointQuestion = (TextView) sheetView.findViewById(R.id.tvRewardPointQuestion);
            tvRewardPointQuestion.setText(RewardPoints.getDailyQuizRewardPoint(sharedPreferences));

            RoundedImageView ivKeyword = (RoundedImageView) sheetView.findViewById(R.id.ivKeyword);
            TextView tvKeywordText = (TextView) sheetView.findViewById(R.id.tvKeywordText);

//        StoryTypes: 1 - Normal, 2 - Share, 3 - Question, 4 - Redirect
            if (storyType.equalsIgnoreCase("1")) {
                rlShareView.setVisibility(View.VISIBLE);
                cardKeyword.setVisibility(View.GONE);
                questionCard.setVisibility(View.GONE);
            } else if (storyType.equalsIgnoreCase("2")) {
                rlShareView.setVisibility(View.VISIBLE);
                cardKeyword.setVisibility(View.GONE);
                questionCard.setVisibility(View.GONE);
            } else if (storyType.equalsIgnoreCase("3")) {
                rlShareView.setVisibility(View.GONE);
                cardKeyword.setVisibility(View.GONE);
                questionCard.setVisibility(View.VISIBLE);

                JSONObject result = new JSONObject("" + action_param);
                if (result != null && result.length() > 0) {
                    String question = result.optString("question");
                    String answer = result.optString("answer");
                    JSONArray options = result.optJSONArray("options");
                    tvQuestions.setText(question);
                    tvHa.setText(options.optString(0));
                    tvNa.setText(options.optString(1));

                    tvHa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            submitStoryAnswer(tvHa.getText().toString());
                            mBottomSheetDialog.dismiss();
                        }
                    });
                    tvNa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            submitStoryAnswer(tvNa.getText().toString());
                            mBottomSheetDialog.dismiss();
                        }
                    });
                }
            } else if (storyType.equalsIgnoreCase("4")) {
//       Type: community_post, keyword_detail, community, expert_advice, daily_quiz, reward;
                rlShareView.setVisibility(View.GONE);
                cardKeyword.setVisibility(View.VISIBLE);
                questionCard.setVisibility(View.GONE);

                Log.e("data:", "" + action_param);
                JSONObject result = new JSONObject("" + action_param);
                if (result != null && result.length() > 0) {
                    String key = result.optString("page");
                    String data = result.optString("data");
                    Log.e("data:", "" + key + "-" + data);

                    if (key.equalsIgnoreCase("community_post")) {

                    } else if (key.equalsIgnoreCase("keyword_detail")) {
                        JSONObject jsonObject = new JSONObject(FetchJsonFromAssetsFile.loadAdvisoryKeywordJSONFromAsset(getActivity(), data));
                        if (jsonObject != null && jsonObject.length() > 0) {
                            tvKeywordText.setText(jsonObject.optString("name"));
//                            JSONArray advisory_image = jsonObject.optJSONArray("advisory_image");
//                            GlideApp.with(getActivity())
//                                    .load(Constants.ASSETS_ADVISORY_KEYWORD_IMAGE_PATH +
//                                            RongoApp.getSelectedCrop() + "/" + advisory_image.optString(0))
//                                    .placeholder(R.drawable.placeholderbg).error(R.drawable.placeholderbg)
//                                    .into(ivKeyword);
                        }
                        cardKeyword.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(getActivity(), SavdhaniDetailsActivity.class);
                                i.putExtra("keyword_id", "" + data);
                                startActivity(i);
                                mBottomSheetDialog.dismiss();
                                dismiss();
                            }
                        });
                    } else if (key.equalsIgnoreCase("community")) {
                        Intent i = new Intent(getActivity(), HomeActivity.class);
                        i.putExtra("pageType", "2");
                        i.putExtra("fromLaunch", "Y");
                        startActivity(i);
                        mBottomSheetDialog.dismiss();
                        dismiss();
                    } else if (key.equalsIgnoreCase("expert_advice")) {
                        Intent i = new Intent(getActivity(), PostQuestionActivity.class);
                        startActivity(i);
                        mBottomSheetDialog.dismiss();
                        dismiss();
                    } else if (key.equalsIgnoreCase("daily_quiz")) {
                        Intent intent = null;
                        try {
                            if (RongoApp.getSelectedCrop().equalsIgnoreCase("11")) {
                                intent = new Intent(getActivity(), QuestionSampleActivity.class);
                            } else {
                                if (Functions.getQuizIntent(getActivity(), sharedPreferences)) {
                                    intent = new Intent(getActivity(), QuizActivity.class);
                                } else {
                                    intent = new Intent(getActivity(), QuestionSampleActivity.class);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        startActivity(intent);
                        mBottomSheetDialog.dismiss();
                        dismiss();
                    } else if (key.equalsIgnoreCase("reward")) {
                        Intent i = new Intent(getActivity(), RewardActivity.class);
                        startActivity(i);
                        mBottomSheetDialog.dismiss();
                        dismiss();
                    }
                }
            }

            AppCompatTextView tvShareWhatapp = sheetView.findViewById(R.id.tvShareWhatapp);
            tvShareWhatapp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (InternetConnection.checkConnectionForFasl(getActivity())) {
                            Functions.shareWhatAppUrlIntent(getActivity(), image_path + storyCardDataList.get(0).getCardMediaPath());
                        } else {
                            Functions.shareWhatAppIntent(getActivity());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Functions.shareWhatAppIntent(getActivity());
                    }
                }
            });

            AppCompatTextView tvShareCopy = sheetView.findViewById(R.id.tvShareCopy);
            tvShareCopy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Functions.shareSMSIntent(getActivity(), RongoApp.getReferralText(getActivity()));
                }
            });

            AppCompatTextView tvSharefacebook = sheetView.findViewById(R.id.tvSharefacebook);
            tvSharefacebook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Functions.shareFacebookIntent(getActivity());
                }
            });

            AppCompatTextView tvShareMore = sheetView.findViewById(R.id.tvShareMore);
            tvShareMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Functions.performShare(getActivity(), appDatabase);
                }
            });

            ImageView ivClose = (ImageView) sheetView.findViewById(R.id.ivClose);
            ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mBottomSheetDialog.dismiss();
//                dismiss();
                }
            });

            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
            ((View) sheetView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
            mBottomSheetDialog.setCanceledOnTouchOutside(false);
            mBottomSheetDialog.setCancelable(false);
            mBottomSheetDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void submitStoryAnswer(String answer) {
        cardQuestion.setVisibility(View.GONE);
//        cardHealthStatus.setVisibility(View.VISIBLE);
        if (InternetConnection.checkConnectionForFasl(getActivity())) {
            RetrofitClient.getInstance().getApi().submitStoryAnswer(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                    RongoApp.getAppVersion(), RongoApp.getUserId(), String.valueOf(storyId), answer).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        Toast.makeText(getActivity(), "" + getResources().getString(R.string.str_info_darj_ho_gaya_he), Toast.LENGTH_SHORT).show();
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                dismiss();
//                            }
//                        }, 500);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        }
    }

    void setUserStoryData() {
        try {
            String storysData = sharedPreferences.getString(Constants.SharedPreferences_story_data, null);
            if (!TextUtils.isEmpty(storysData)) {
                JSONObject result = new JSONObject(storysData);
                if (result != null && result.length() > 0) {
                    image_path = result.optString("image_path");
                    JSONArray storiesArray = new JSONArray(result.optString("stories"));
                    if (storiesArray != null && storiesArray.length() > 0) {
                        StoryData storyArrayData = new Gson().fromJson(result.toString(), StoryData.class);
//                        setStoryView(storyArrayData.getStories());
                        Log.e("story=", "" + storyData.size());
                        storyData = storyArrayData.getStories();
                        if (storyData != null && storyData.size() > 0) {
                            setStoryView();
                        } else {
                            tvNoDataAvailable.setVisibility(View.VISIBLE);
                        }
                    } else {
                        tvNoDataAvailable.setVisibility(View.VISIBLE);
                    }
                }
            } else {
                getUserStories();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void getUserStories() {
        if (InternetConnection.checkConnectionForFasl(getActivity())) {
            RetrofitClient.getInstance().getApi().getUserStories(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                    RongoApp.getAppVersion(), RongoApp.getUserId(), "all").enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response != null && response.isSuccessful()) {
                            String r = response.body().string();
                            if (!TextUtils.isEmpty(r)) {
                                JSONObject jsonObject = new JSONObject(r);
                                if (jsonObject != null && jsonObject.length() > 0) {
                                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                        JSONObject dataObject = jsonObject.optJSONObject("data");
                                        image_path = dataObject.optString("image_path");
                                        JSONArray storiesArray = new JSONArray(dataObject.optString("stories"));
                                        if (storiesArray != null && storiesArray.length() > 0) {
                                            StoryData storyDatas = new Gson().fromJson(dataObject.toString(), StoryData.class);
//                                            Log.e("story_id=", "" + storyData.getStories().get(0).getStoryId());
//                                            setStoryView(storyData.getStories());
                                            storyData = storyDatas.getStories();
                                            setStoryView();
                                        } else {
                                            tvNoDataAvailable.setVisibility(View.VISIBLE);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        }
    }

}