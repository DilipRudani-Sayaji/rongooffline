package com.rongoapp.fragments;

import static android.app.Activity.RESULT_OK;
import static com.rongoapp.controller.Constants.PERMISSION_COARSE_LOCATION;
import static com.rongoapp.controller.Constants.PERMISSION_FINE_LOCATION;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.card.MaterialCardView;
import com.rongoapp.BuildConfig;
import com.rongoapp.R;
import com.rongoapp.activities.HomeActivity;
import com.rongoapp.adapter.CustomSpinnerAdapter;
import com.rongoapp.adapter.SeedTypesAdapter;
import com.rongoapp.adapter.SpinnerArrayAdapter;
import com.rongoapp.adapter.SpinnerHintArrayAdapter;
import com.rongoapp.controller.AudioPlayerTask;
import com.rongoapp.controller.AudioRecorderCallback;
import com.rongoapp.controller.AudioRecorderDialog;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.GeocoderHandler;
import com.rongoapp.controller.InitInterface;
import com.rongoapp.controller.LocaleManager;
import com.rongoapp.controller.PermissionUtils;
import com.rongoapp.controller.RewardPoints;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.CropsData;
import com.rongoapp.data.FeedbackData;
import com.rongoapp.data.InsertHomeFeedData;
import com.rongoapp.data.KhetOfflineData;
import com.rongoapp.data.MasterCropData;
import com.rongoapp.data.ProfileOfflineData;
import com.rongoapp.data.ProfileResultData;
import com.rongoapp.data.SeasonData;
import com.rongoapp.data.SeedTypeData;
import com.rongoapp.data.StateCityDataModel;
import com.rongoapp.data.UserProfileData;
import com.rongoapp.database.AppDatabase;
import com.rongoapp.otp.OtpView;
import com.rongoapp.services.RetrofitClient;
import com.rongoapp.utilities.InternetConnection;
import com.rongoapp.views.ProgressBarDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdhikFragment extends Fragment implements InitInterface {

    HomeActivity homeActivity;
    SharedPreferences sharedPreferences;
    AppDatabase appDatabase;
    long mLastLongClick = 0;
    View view;

    SwitchCompat switchWhatsApp;
    Boolean isTouched = false;
    ProgressBarDialog progressBarDialog;

    boolean isCropSelect = false;
    MaterialCardView cardLanguage;
    TextView tvReferralSubmit, tvSeasonValidation, tvSeasonValidationtxt;
    RelativeLayout relArea, relBuvaiData, rlCitySpinner, rlStateSpinner, rlLocationView, rlLangSpinner, rlCropSpinner;
    LinearLayout linearDarj, linearSaveData, linearOtherContent, linearText, llRewardView, llReferralView, llReferralText;
    AppCompatEditText txtUserFirstName, txtNumber, edtReferralCode, editTextSowingDate;
    ImageView imgEditTop, imgCancel, imgPersonalJankariEdit, imgArrow11, imgArrow, ivLocationBanner;
    AppCompatTextView tvLocationText, tvLocationAddressEnter, tvLocationAddress, txtFeedBack, txtBuaiTitle, txtAddress,
            tvRewardNew, tvAppVersion, tvRewardPoint, imgEditTopClick, txtPersonalDarjKare, txtArea, txtSowingDateDisplay,
            txtSowingDate, txtDarjKare, txtCancel, tvNumberChange;

    List<SeedTypeData> seedTypeData = new ArrayList<>();
    ArrayList<StateCityDataModel> stateCityDataModels = new ArrayList<StateCityDataModel>();
    Spinner spinState, spinCity, spinnerLang, spinnerCrop;

    FusedLocationProviderClient fusedLocationClient;
    int mYear = 0, mMonth = 0, mDay = 0;
    double latitude = 0.0, longitude = 0.0;
    String city = "", state = "", area = "", country = "", sowingDate = "0000-00-00 00:00:00", field_id = "",
            pincode = "", expectedSowingDate = "0000-00-00 00:00:00", fromDatePicker = "N", is_sowing_done = "", newState = "",
            p_crop_id = "", field_size = "", season_id = "", language = LocaleManager.HINDI, personal = "N", newCity = "",
            seedType = "", expsowing = "", sowing = "", s = "0000-00-00 00:00:00", es = "0000-00-00 00:00:00", fromOTP = "";

    AdhikFragment adhikFragment;
    OtpView ediTextOTP;

    ImageView mPlayButton;
    TextView mFileNameTextView;
    String filePath, mSpeechText = "";
    CardView mediaplayer_view;
    AudioPlayerTask audioPlayerTask;
    AudioRecorderDialog audioRecorderDialog;
    AudioRecorderCallback audioRecorderCallback;
    LinearLayout llVoiceRecordView;
    BroadcastReceiver AUDIO_RECORD_RECEIVER = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                if (intent != null) {
                    String fileName = intent.getStringExtra("mFileName");
                    filePath = intent.getStringExtra("mFilePath");

                    llVoiceRecordView.setVisibility(View.GONE);
                    mediaplayer_view.setVisibility(View.VISIBLE);
                    audioPlayerTask.onPlayMedia(filePath);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof HomeActivity) {
            homeActivity = (HomeActivity) context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adhikFragment = this;
    }

    public static AdhikFragment getInstance() {
        return new AdhikFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().registerReceiver(AUDIO_RECORD_RECEIVER, new IntentFilter(Constants.BROADCAST_RECEIVER.AUDIO_RECORD_RECEIVER));
        view = inflater.inflate(R.layout.fragment_adhik, container, false);

        progressBarDialog = new ProgressBarDialog(homeActivity);
        sharedPreferences = homeActivity.getSharedPreferences(Constants.MediaPrefs, 0);
        appDatabase = AppDatabase.getDatabase(homeActivity);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(homeActivity);
        RongoApp.logVisitedPageTabEvent("Adhik (Setting)");
        Functions.setFirebaseLogEventTrack(homeActivity, Constants.Screen_Track, "Adhik (Setting)", "", "");
        Functions.setUserScreenTrackEvent(appDatabase, "Adhik (Setting)", "0");

        init();
        setListener();
        seedTypeData = SharedPreferencesUtils.getSeedTypes(homeActivity, Constants.SharedPreferences_SEED_TYPES);
        if (seedTypeData == null || seedTypeData.size() == 0) {
            RongoApp.getMasterDropDownData(true);
        }
        if (TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreferences_StateCityData, ""))) {
            RongoApp.getCityStateData(true);
        }

        audioPlayerTask = new AudioPlayerTask(homeActivity, mPlayButton);
        return view;
    }

    @Override
    public void init() {
        tvRewardPoint = view.findViewById(R.id.tvRewardPoint);
        tvRewardPoint.setText(RewardPoints.getWeeklyCropInfoRewardPoint(sharedPreferences) + " " + homeActivity.getResources().getString(R.string.str_points));
        tvRewardNew = view.findViewById(R.id.tvRewardNew);
        tvRewardNew.setText(RewardPoints.getWeeklyCropInfoRewardPoint(sharedPreferences));

        tvNumberChange = view.findViewById(R.id.tvNumberChange);
        relArea = view.findViewById(R.id.relArea);
        txtBuaiTitle = view.findViewById(R.id.txtBuaiTitle);
        linearText = view.findViewById(R.id.linearText);
        relBuvaiData = view.findViewById(R.id.relBuvaiData);
        linearOtherContent = view.findViewById(R.id.linearOtherContent);
        imgPersonalJankariEdit = view.findViewById(R.id.imgPersonalJankariEdit);
        txtFeedBack = view.findViewById(R.id.txtFeedBack);
        txtUserFirstName = view.findViewById(R.id.txtUserFirstName);
        txtNumber = view.findViewById(R.id.txtNumber);
        txtAddress = view.findViewById(R.id.txtAddress);
        txtArea = view.findViewById(R.id.txtArea);
        txtDarjKare = view.findViewById(R.id.txtDarjKare);
        linearSaveData = view.findViewById(R.id.linearSaveData);
        txtSowingDateDisplay = view.findViewById(R.id.txtSowingDateDisplay);
        txtSowingDate = view.findViewById(R.id.txtSowingDate);
        txtCancel = view.findViewById(R.id.txtCancel);
        linearDarj = view.findViewById(R.id.linearDarj);
        imgEditTopClick = view.findViewById(R.id.imgEditTopClick);
        imgEditTop = view.findViewById(R.id.imgEditTop);
        imgCancel = view.findViewById(R.id.imgCancel);
        txtPersonalDarjKare = view.findViewById(R.id.txtPersonalDarjKare);

        imgArrow11 = view.findViewById(R.id.imgArrow11);
        imgArrow = view.findViewById(R.id.imgArrow);

        tvAppVersion = view.findViewById(R.id.tvAppVersion);
        tvAppVersion.setText(RongoApp.getAppVersion());
        if (BuildConfig.DEBUG) {
            tvAppVersion.setText(RongoApp.getAppVersion() + "\n Debug");
        }

        switchWhatsApp = view.findViewById(R.id.switchWhatsApp);
        switchWhatsApp.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                isTouched = true;
                return false;
            }
        });

        switchWhatsApp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isTouched) {
                    isTouched = false;
                    if (isChecked) {
                        updateUserConsent("1");
                    } else {
                        updateUserConsent("0");
                    }
                }
            }
        });

        if (!TextUtils.isEmpty(RongoApp.getWhatsAppNotificationStatus())) {
            if (RongoApp.getWhatsAppNotificationStatus().equalsIgnoreCase("1")) {
                switchWhatsApp.setChecked(true);
            } else {
                switchWhatsApp.setChecked(false);
            }
        } else {
            switchWhatsApp.setChecked(false);
        }

//        updateUserConsent(RongoApp.getWhatsAppNotificationStatus());

        txtUserFirstName.setFocusableInTouchMode(false);
        txtNumber.setFocusableInTouchMode(false);

        rlCitySpinner = view.findViewById(R.id.rlCitySpinner);
        rlStateSpinner = view.findViewById(R.id.rlStateSpinner);

        spinState = (Spinner) view.findViewById(R.id.spinnerState);
        spinCity = (Spinner) view.findViewById(R.id.spinnerCity);
        spinState.setEnabled(false);
        spinCity.setEnabled(false);
        parseStateCityListData();

        rlLocationView = view.findViewById(R.id.rlLocationView);
        tvLocationText = view.findViewById(R.id.tvLocationText);
        tvLocationAddressEnter = view.findViewById(R.id.tvLocationAddressEnter);
        tvLocationAddress = view.findViewById(R.id.tvLocationAddress);
        llRewardView = view.findViewById(R.id.llRewardView);
        ivLocationBanner = view.findViewById(R.id.ivLocationBanner);

        cardLanguage = view.findViewById(R.id.cardLanguage);
        rlLangSpinner = view.findViewById(R.id.rlLangSpinner);
        spinnerLang = view.findViewById(R.id.spinnerLang);
        setLanguageSpinnerData();

        rlCropSpinner = view.findViewById(R.id.rlCropSpinner);
        spinnerCrop = view.findViewById(R.id.spinnerCrop);
        setCropSpinnerData();

        llReferralView = view.findViewById(R.id.llReferralView);
        llReferralText = view.findViewById(R.id.llReferralText);
        edtReferralCode = view.findViewById(R.id.edtReferralCode);
        tvReferralSubmit = view.findViewById(R.id.tvReferralSubmit);

        List<KhetOfflineData> khetOfflineData = appDatabase.khetOfflineDao().getAll();
        if (khetOfflineData != null && khetOfflineData.size() > 0) {
            season_id = khetOfflineData.get(0).getSeason_id();
            field_id = khetOfflineData.get(0).getFieldId();
            p_crop_id = khetOfflineData.get(0).getP_crop_id();
            llRewardView.setVisibility(View.GONE);
            ivLocationBanner.setVisibility(View.GONE);
            tvLocationText.setText(getString(R.string.str_farm_location));
            tvLocationAddress.setText(khetOfflineData.get(0).getKhet_address() + "\n" + khetOfflineData.get(0).getLatitude() + " - " + khetOfflineData.get(0).getLongitude());
        } else {
            List<CropsData> cropsData = Functions.getCropData(appDatabase, sharedPreferences);
            if (cropsData != null && cropsData.size() > 0) {
                field_id = cropsData.get(0).getField_id();
                p_crop_id = cropsData.get(0).getP_crop_id();
                if (cropsData.get(0).getGeo_metadata() == null || cropsData.get(0).getGeo_metadata().equalsIgnoreCase("")) {
                    llRewardView.setVisibility(View.VISIBLE);
                    ivLocationBanner.setVisibility(View.VISIBLE);
                } else {
                    ivLocationBanner.setVisibility(View.GONE);
                    llRewardView.setVisibility(View.GONE);
                    tvLocationText.setText(getString(R.string.str_farm_location));
                    tvLocationAddress.setText(cropsData.get(0).getGeo_metadata() + "\n" + cropsData.get(0).getGeo_latitude() + " - " + cropsData.get(0).getGeo_longitude());
                }
            }
        }

//        if (!TextUtils.isEmpty(sharedPreferences.getString(Constants.SharedPreference_Referral_by, ""))) {
//            llReferralView.setVisibility(View.GONE);
//        } else {
//            llReferralView.setVisibility(View.VISIBLE);
//            setReferralTextView();
//        }
    }

    @Override
    public void setListener() {
        tvNumberChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeNumberDialog();
            }
        });

        txtFeedBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                Functions.setFirebaseLogEventTrack(homeActivity, Constants.Click_Event, "Adhik (Setting)", "", "FeedBack");
//                Intent i = new Intent(homeActivity, FeedbackActivity.class);
//                i.putExtra("type", "0");
//                i.putExtra("post_id", "0");
//                startActivity(i);

                openFeedBackDialog();
            }
        });

        relArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                openEkarAreaDialog();
            }
        });

        imgPersonalJankariEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                personal = "Y";
                linearOtherContent.setVisibility(View.GONE);
                linearText.setVisibility(View.GONE);
                cardLanguage.setVisibility(View.GONE);
                spinState.setEnabled(true);
                spinCity.setEnabled(true);
                parseStateCityListData();

                imgPersonalJankariEdit.setVisibility(View.GONE);
                txtPersonalDarjKare.setVisibility(View.VISIBLE);

                txtUserFirstName.setFocusableInTouchMode(true);
                txtNumber.setFocusableInTouchMode(true);
                txtUserFirstName.requestFocus();
                txtUserFirstName.setSelection(txtUserFirstName.getText().toString().trim().length());
            }
        });

        txtSowingDateDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSowingDate("Yes");
            }
        });

        relBuvaiData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                openSowingDate("Yes");
            }
        });

        txtPersonalDarjKare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fieldsValidation()) {
                    if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                        Functions.hideKeyboard(homeActivity);
                        updateSettings();
                    } else {
                        personal = "N";
                        linearOtherContent.setVisibility(View.VISIBLE);
                        linearText.setVisibility(View.VISIBLE);
                        cardLanguage.setVisibility(View.VISIBLE);
                        spinState.setEnabled(false);
                        spinCity.setEnabled(false);
                        txtPersonalDarjKare.setVisibility(View.GONE);
                        imgPersonalJankariEdit.setVisibility(View.VISIBLE);
                        txtUserFirstName.clearFocus();
                        txtNumber.clearFocus();
                        txtAddress.clearFocus();

                        setProfileOffline();
                    }
                }
            }
        });

        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgEditTopClick.setVisibility(View.VISIBLE);
                linearDarj.setVisibility(View.GONE);
                txtUserFirstName.clearFocus();
                txtNumber.clearFocus();
                txtAddress.clearFocus();

                txtSowingDateDisplay.setEnabled(false);
                txtAddress.setEnabled(false);

                txtUserFirstName.setFocusableInTouchMode(false);
                txtNumber.setFocusableInTouchMode(false);
            }
        });

        tvReferralSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Functions.hideKeyboard(homeActivity);
                String referralCode = edtReferralCode.getText().toString().trim();
                if (!TextUtils.isEmpty(referralCode)) {
                    Functions.setFirebaseLogEventTrack(homeActivity, Constants.Click_Event, "Adhik (Setting)", referralCode, "referralCode");
                    if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                        submitReferralCode(referralCode);
                    } else {
                        Functions.showSnackBar(homeActivity.findViewById(android.R.id.content),
                                TextUtils.htmlEncode(homeActivity.getResources().getString(R.string.no_connection)));
                    }
                } else {
                    Functions.showSnackBar(homeActivity.findViewById(android.R.id.content),
                            TextUtils.htmlEncode(homeActivity.getResources().getString(R.string.please_enter_referral)));
                }
            }
        });

        txtAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSearchCalled();
            }
        });

        tvLocationAddressEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                if (!PermissionUtils.isLocationGranted(homeActivity)) {
                    PermissionUtils.checkPermission(homeActivity, Manifest.permission.ACCESS_FINE_LOCATION, PERMISSION_FINE_LOCATION);
                    PermissionUtils.checkPermission(homeActivity, Manifest.permission.ACCESS_COARSE_LOCATION, PERMISSION_COARSE_LOCATION);
                }

                fusedLocationClient.getLastLocation().addOnSuccessListener(homeActivity, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            Toast.makeText(homeActivity, "Location is - \nLat: " + location.getLatitude()
                                    + "\nLong: " + location.getLongitude(), Toast.LENGTH_LONG).show();
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
//                             latitude = Double.parseDouble(Location.convert(location.getLongitude(), Location.FORMAT_DEGREES));
//                             longitude = Double.parseDouble(Location.convert(location.getLatitude(), Location.FORMAT_DEGREES));
                            getAddressFromLocation(latitude, longitude, homeActivity, new GeocoderHandler(), false);
                            updateKhetLocationDialog();
                        }
                    }
                });

            }
        });
    }

    void openFeedBackDialog() {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_feedback_dialog, null);
        bottomSheetDialog.setContentView(view);

        AppCompatTextView txtOfflineSMS = view.findViewById(R.id.txtOfflineSMS);
        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
            txtOfflineSMS.setVisibility(View.GONE);
        } else {
            txtOfflineSMS.setVisibility(View.VISIBLE);
        }
        AppCompatEditText editTextDescription = (AppCompatEditText) view.findViewById(R.id.editTextDescription);
        RatingBar ratingbar = (RatingBar) view.findViewById(R.id.ratingBar);

        AppCompatTextView txtDarjKare = view.findViewById(R.id.txtDarjKare);
        txtDarjKare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String rating = String.valueOf(ratingbar.getRating());
                if (ratingbar.getRating() > 0.0) {
                    bottomSheetDialog.dismiss();
                    saveOfflineData(editTextDescription.getText().toString(), rating);

                    Bundle params = new Bundle();
                    params.putString(Constants.FB_EVENT_PARAM_MAX_RATING_VALUE, rating);
                    AppEventsLogger.newLogger(homeActivity).logEvent(Constants.FB_EVENT_NAME_RATED, params);

//                    if (InternetConnection.checkConnection(homeActivity)) {
//                        if (InternetConnection.getNetworkClass(homeActivity).equalsIgnoreCase(Constants.NETWORK_TYPE)) {
//                            saveOfflineData(editTextDescription.getText().toString(), rating);
//                        } else {
//                            uploadFeedback(editTextDescription.getText().toString(), rating);
//                        }
//                    } else {
//                        saveOfflineData(editTextDescription.getText().toString(), rating);
//                    }
                } else {
                    Toast.makeText(homeActivity, "" + homeActivity.getResources().getString(R.string.str_rate_text), Toast.LENGTH_SHORT).show();
                }
            }
        });

        llVoiceRecordView = (LinearLayout) view.findViewById(R.id.llVoiceRecordView);
        llVoiceRecordView.setVisibility(View.VISIBLE);
        mediaplayer_view = (CardView) view.findViewById(R.id.mediaPlayer);
        mediaplayer_view.setVisibility(View.GONE);
        mFileNameTextView = (TextView) view.findViewById(R.id.file_name_text_view);
        mPlayButton = (ImageView) view.findViewById(R.id.ivPlay);
        mPlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mediaplayer_view.setVisibility(View.VISIBLE);
                audioPlayerTask.onPlayMedia(filePath);
            }
        });

        llVoiceRecordView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                audioRecorderCallback = status -> {
                    com.rongoapp.views.Log.v("status-", "" + status);
                    if (status.equalsIgnoreCase("1")) {
                        mPlayButton.setImageResource(R.mipmap.play);
                        llVoiceRecordView.setVisibility(View.GONE);
                        mediaplayer_view.setVisibility(View.VISIBLE);
                    } else {
                        mediaplayer_view.setVisibility(View.GONE);
                        llVoiceRecordView.setVisibility(View.VISIBLE);
                    }
                };
                audioRecorderDialog = new AudioRecorderDialog(audioRecorderCallback);
                audioRecorderDialog.show(getActivity().getSupportFragmentManager(), "AudioRecorderDialog");
            }
        });

        TextView tvChangeAudio = (TextView) view.findViewById(R.id.tvChangeAudio);
        tvChangeAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                filePath = "";
                mediaplayer_view.setVisibility(View.GONE);
                mPlayButton.setImageResource(R.mipmap.play);

                audioRecorderCallback = status -> {
                    Log.v("status-", "" + status);
                    if (status.equalsIgnoreCase("1")) {
                        mPlayButton.setImageResource(R.mipmap.play);
                        llVoiceRecordView.setVisibility(View.GONE);
                        mediaplayer_view.setVisibility(View.VISIBLE);
                    } else {
                        mediaplayer_view.setVisibility(View.GONE);
                        llVoiceRecordView.setVisibility(View.VISIBLE);
                    }
                };
                audioRecorderDialog = new AudioRecorderDialog(audioRecorderCallback);
                audioRecorderDialog.show(getActivity().getSupportFragmentManager(), "AudioRecorderDialog");
            }
        });

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    void saveOfflineData(String text, String rate) {
        try {
            List<FeedbackData> feedbackData = new ArrayList<>();
            feedbackData.add(new FeedbackData(0, SharedPreferencesUtils.getUserId(homeActivity), rate, text, filePath));
            appDatabase.feedbackDao().insertAll(feedbackData);
            openSubmitFeedBackDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    RequestBody getRequestAudioFile(File file) {
        RequestBody fbody = RequestBody.create(file, MediaType.parse("audio/*"));
        return fbody;
    }

    void uploadFeedback(String text, String rate) {
        progressBarDialog.showProgressDialogWithTitle(getResources().getString(R.string.app_name));
        RequestBody requestBodyId = RequestBody.create(SharedPreferencesUtils.getUserId(homeActivity), MediaType.parse("text/plain"));
        RequestBody requestBodyDeviceId = RequestBody.create(Functions.getDeviceid(homeActivity), MediaType.parse("text/plain"));
        RequestBody requestBodyFCMToken = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), MediaType.parse("text/plain"));
        RequestBody requestBodyAppVersion = RequestBody.create(RongoApp.getAppVersion(), MediaType.parse("text/plain"));
        RequestBody requestBodyFeedBack = RequestBody.create(text, MediaType.parse("text/plain"));
        RequestBody requestBodyType = RequestBody.create("", MediaType.parse("text/plain"));
        RequestBody requestBodyRate = RequestBody.create(rate, MediaType.parse("text/plain"));
        RequestBody requestBodyPost_id = RequestBody.create("", MediaType.parse("text/plain"));
        RequestBody requestBodyAppLanguage = RequestBody.create(RongoApp.getDefaultLanguage(), MediaType.parse("text/plain"));
        RequestBody requestBodyAppSeason = RequestBody.create(RongoApp.getSeasonName(), MediaType.parse("text/plain"));

        Map<String, RequestBody> params = new HashMap<>();
        params.put("user_id", requestBodyId);
        params.put("feedback", requestBodyFeedBack);
        params.put("device_id", requestBodyDeviceId);
        params.put("fcm_token", requestBodyFCMToken);
        params.put("app_version", requestBodyAppVersion);
        params.put("type", requestBodyType);
        params.put("rate", requestBodyRate);
        params.put("post_id", requestBodyPost_id);
        params.put("app_language", requestBodyAppLanguage);
        params.put("season_name", requestBodyAppSeason);

        if (filePath != null) {
            RequestBody requestBodyAudioPath = RequestBody.create(filePath, MediaType.parse("text/plain"));
            params.put("audio_path[]", requestBodyAudioPath);
            File auFile = new File(filePath);
            params.put("audio[]" + "\"; filename=\"" + auFile.getName(), getRequestAudioFile(auFile));
        }

        RetrofitClient.getInstance().getApi().uploadFeedBack(params).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response != null && response.isSuccessful()) {
                        String str = response.body().string();
                        if (!TextUtils.isEmpty(str)) {
                            JSONObject jsonObject = new JSONObject(str);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_IsAction, "Y");
                                    progressBarDialog.hideProgressDialogWithTitle();
                                    openSubmitFeedBackDialog();
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressBarDialog.hideProgressDialogWithTitle();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        });
    }

    void openSubmitFeedBackDialog() {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_submit_feedback_dialog, null);
        bottomSheetDialog.setContentView(view);

        MaterialCardView cardRating = (MaterialCardView) view.findViewById(R.id.cardRating);
        cardRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String appPackageName = getActivity().getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (ActivityNotFoundException exception) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            audioPlayerTask.stopPlaying();
            getActivity().unregisterReceiver(AUDIO_RECORD_RECEIVER);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void updatePhoneNumber(String number) {
        RetrofitClient.getInstance().getApi().updatePhoneNumber(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                RongoApp.getAppVersion(), Functions.getDeviceid(homeActivity), RongoApp.getFCMToken(),
                number, SharedPreferencesUtils.getUserId(homeActivity), "").enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null) {
                    try {
                        String responseStrig = response.body().string();
                        if (!TextUtils.isEmpty(responseStrig)) {
                            JSONObject jb = new JSONObject(responseStrig);
                            if (jb != null && jb.length() > 0) {
                                if (jb.optString("status_code").equalsIgnoreCase("0")) {
                                    JSONObject jData = jb.optJSONObject("data");
                                    if (jData != null && jData.length() > 0) {
                                        JSONObject jResult = jData.optJSONObject("result");
                                        if (jResult != null && jResult.length() > 0) {
                                            fromOTP = jResult.optString("otp");
                                        }
                                    }
                                } else if (jb.optString("status_code").equalsIgnoreCase("1")) {
                                    String message = jb.optString("status_message");
                                    Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), TextUtils.htmlEncode(message));
                                }
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void changeNumberDialog() {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.change_number_dialog, null);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setCanceledOnTouchOutside(false);

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        TextView txtTitle = view.findViewById(R.id.txtTitle);
        LinearLayout llNumberView = view.findViewById(R.id.llNumberView);

        LinearLayout llNumberVerify = view.findViewById(R.id.llNumberVerify);
        llNumberVerify.setVisibility(View.GONE);
        LinearLayout llSuccessView = view.findViewById(R.id.llSuccessView);
        llSuccessView.setVisibility(View.GONE);

        EditText editTextPhoneNumber = view.findViewById(R.id.editTextPhonenumber);
        TextView tvDarjKare = view.findViewById(R.id.tvDarjKare);
        tvDarjKare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                if (!TextUtils.isEmpty(editTextPhoneNumber.getText().toString().trim())) {
                    if (InternetConnection.checkConnection(homeActivity)) {

                        if (editTextPhoneNumber.getText().toString().trim().length() > 9) {
                            updatePhoneNumber(editTextPhoneNumber.getText().toString().trim());

                            tvDarjKare.setVisibility(View.GONE);
                            llNumberVerify.setVisibility(View.VISIBLE);
                            txtTitle.setVisibility(View.GONE);
                        } else {
                            Toast.makeText(homeActivity, "" + getString(R.string.str_please_enter_valid_number), Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(homeActivity, "" + getString(R.string.please_enter_your_number), Toast.LENGTH_SHORT).show();
                }
            }
        });

        ediTextOTP = view.findViewById(R.id.ediTextOTP);
        TextView txtResendOTP = view.findViewById(R.id.txtResendOTP);
        txtResendOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Functions.doubleTapCheck(mLastLongClick)) {
                    return;
                }
                mLastLongClick = SystemClock.elapsedRealtime();

                if (!TextUtils.isEmpty(editTextPhoneNumber.getText().toString().trim())) {
                    if (InternetConnection.checkConnection(homeActivity)) {
                        if (editTextPhoneNumber.getText().toString().trim().length() > 9) {
                            updatePhoneNumber(editTextPhoneNumber.getText().toString().trim());
                        }
                    } else {
                        Toast.makeText(homeActivity, "" + getString(R.string.str_please_enter_valid_number), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(homeActivity, "" + getString(R.string.please_enter_your_number), Toast.LENGTH_SHORT).show();
                }
            }
        });

        TextView txtVerify = view.findViewById(R.id.txtVerify);
        txtVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    if (InternetConnection.checkConnection(homeActivity)) {
                        if (Functions.doubleTapCheck(mLastLongClick)) {
                            return;
                        }
                        mLastLongClick = SystemClock.elapsedRealtime();

                        verifyOTP(editTextPhoneNumber.getText().toString().trim());

                        llNumberView.setVisibility(View.GONE);
                        llNumberVerify.setVisibility(View.GONE);
                        llSuccessView.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    void verifyOTP(String number) {
        RetrofitClient.getInstance().getApi().verifyOTP(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                RongoApp.getAppVersion(), Functions.getDeviceid(homeActivity), RongoApp.getFCMToken(),
                number, SharedPreferencesUtils.getUserId(homeActivity), "").enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null) {
                    try {
                        String responseString = response.body().string();
                        if (!TextUtils.isEmpty(responseString)) {
                            JSONObject jsonObject = new JSONObject(responseString);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {

                                    changePhoneNumber("" + number);
                                    txtNumber.setText("" + number);
                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_MOBILE_NO, "" + number);

                                } else if (jsonObject.optString("status_code").equalsIgnoreCase("1")) {
                                    Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), TextUtils.htmlEncode(jsonObject.optString("status_message")));
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    boolean validate() {
        if (TextUtils.isEmpty(ediTextOTP.getText().toString().trim())) {
//            Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_please_enter_otp));
            Toast.makeText(homeActivity, "" + getString(R.string.str_please_enter_otp), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!TextUtils.isEmpty(fromOTP)) {
            if (!ediTextOTP.getText().toString().trim().equalsIgnoreCase(fromOTP)) {
                if (!ediTextOTP.getText().toString().trim().equalsIgnoreCase(Functions.getCurrentDDMM())) {
//                    Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_please_enter_valid_otp));
                    Toast.makeText(homeActivity, "" + getString(R.string.str_please_enter_valid_otp), Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
        }
        return true;
    }

    void changePhoneNumber(String number) {
        RetrofitClient.getInstance().getApi().changePhoneNumber(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                RongoApp.getAppVersion(), Functions.getDeviceid(homeActivity), RongoApp.getUserId(),
                number, sharedPreferences.getString(Constants.SharedPreferences_MOBILE_NO, "")).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void setLanguageSpinnerData() {
//        openLanguageDialog();
//        Toast.makeText(homeActivity, "" + RongoApp.getSelectedState(), Toast.LENGTH_SHORT).show();

        ArrayList<String> languageList = new ArrayList<>();
        languageList.add(Constants.Hindi_Language);

        if (RongoApp.getSelectedCrop().equalsIgnoreCase(Constants.MAIZE)
                || RongoApp.getSelectedCrop().equalsIgnoreCase(Constants.WHEAT)) {
            if (RongoApp.getSelectedState().equalsIgnoreCase(Constants.Maharashtra)) {
                languageList.add(Constants.Marathi_Language);
            }
        }

        if (RongoApp.getSelectedCrop().equalsIgnoreCase(Constants.PEARMILLET) ||
                RongoApp.getSelectedCrop().equalsIgnoreCase(Constants.COTTON)) {
            if (RongoApp.getSelectedState().equalsIgnoreCase(Constants.Gujarat)) {
                languageList.add(Constants.Gujarati_Language);
            }
        }

        spinnerLang.setAdapter(new SpinnerArrayAdapter(homeActivity, R.layout.spinner_item_list, languageList));
        spinnerLang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                if (!TextUtils.isEmpty(item)) {
                    if (item.equalsIgnoreCase(Constants.Hindi_Language)) {
                        language = LocaleManager.HINDI;
                    } else if (item.equalsIgnoreCase(Constants.Gujarati_Language)) {
                        language = LocaleManager.GUJARATI;
                    } else if (item.equalsIgnoreCase(Constants.Marathi_Language)) {
                        language = LocaleManager.MARATHI;
                    }

                    if (!RongoApp.getDefaultLanguage().equalsIgnoreCase(language)) {
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Default_Language, language);
                        RongoApp.setNewLocale(homeActivity, language);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (languageList.size() > 1) {
            if (!RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.HINDI)) {
                spinnerLang.setSelection(1);
            }
        }
    }

    ArrayList<MasterCropData> getSelectedCropList() {
        ArrayList<MasterCropData> profileCropList = new ArrayList<>();
        List<MasterCropData> masterCropList = Functions.getMasterCropList(sharedPreferences);
        List<CropsData> profileCropsData = Functions.getProfileCropData();
        if (profileCropsData.size() > 0) {
            for (int i = 0; i < masterCropList.size(); i++) {
                for (int j = 0; j < profileCropsData.size(); j++) {
                    if (masterCropList.get(i).getCrop_id().equalsIgnoreCase(profileCropsData.get(j).getCrop_id())) {
                        profileCropList.add(new MasterCropData(masterCropList.get(i).getCrop_name(), masterCropList.get(i).getCrop_id(),
                                masterCropList.get(i).getCrop_image(), masterCropList.get(i).getDisplay_name(), "1"));
                    }
                }
            }
        } else {
            profileCropList.add(new MasterCropData(masterCropList.get(0).getCrop_name(), masterCropList.get(0).getCrop_id(),
                    masterCropList.get(0).getCrop_image(), masterCropList.get(0).getDisplay_name(), "1"));
        }
        return profileCropList;
    }

    ArrayList<MasterCropData> removeDuplicates(ArrayList<MasterCropData> list) {
        Set<MasterCropData> set = new TreeSet(new Comparator<MasterCropData>() {
            @Override
            public int compare(MasterCropData o1, MasterCropData o2) {
                if (o1.getCrop_id().equalsIgnoreCase(o2.getCrop_id())) {
                    return 0;
                }
                return 1;
            }
        });
        set.addAll(list);

        final ArrayList newList = new ArrayList(set);
        return newList;
    }

    void setCropSpinnerData() {
        ArrayList<MasterCropData> profileCropList = removeDuplicates(getSelectedCropList());
        if (profileCropList.size() > 0) {
            spinnerCrop.setAdapter(new CustomSpinnerAdapter(homeActivity, profileCropList));

            for (int i = 0; i < profileCropList.size(); i++) {
                if (RongoApp.getSelectedCrop().equalsIgnoreCase(profileCropList.get(i).getCrop_id())) {
                    spinnerCrop.setSelection(i);
                }
            }

            spinnerCrop.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                    if (isCropSelect) {
//                        Log.e("+++ Crop +++", "" + profileCropList.get(pos).getDisplay_name());
                        List<CropsData> cropsData = Functions.getCropDataByID(profileCropList.get(pos).getCrop_id());
                        if (cropsData.size() > 0) {
                            for (int i = 0; i < cropsData.size(); i++) {
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_SowingDate, cropsData.get(i).getDate_of_sowing());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_CROP_ID, cropsData.get(i).getCrop_id());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_P_CROP_ID, cropsData.get(i).getP_crop_id());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Field_ID, cropsData.get(i).getField_id());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Sowing_Done, cropsData.get(i).getIs_sowing_done());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_season_end_crop_week, cropsData.get(i).getSeason_end_crop_week());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_crop_status, cropsData.get(i).getCrop_status());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_season_feedback, cropsData.get(i).getSeason_feedback());
                            }

                            if (profileCropList.get(pos).getCrop_id().equalsIgnoreCase("3")) {
                                RongoApp.setCropSwitch(homeActivity, RongoApp.getDefaultLanguage());
                            } else {
                                if (!RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.HINDI)) {
                                    RongoApp.setCropSwitch(homeActivity, LocaleManager.HINDI);
                                } else {
                                    RongoApp.setCropSwitch(homeActivity, RongoApp.getDefaultLanguage());
                                }
                            }
                        }
                    }
                    isCropSelect = true;
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
        }
    }

    void updateUserConsent(String status) {
        if (InternetConnection.checkConnectionForFasl(homeActivity)) {
            RetrofitClient.getInstance().getApi().updateUserConsent(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                    RongoApp.getUserId(), "whatsapp", status).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response != null && response.isSuccessful()) {
                            String r = response.body().string();
                            if (!TextUtils.isEmpty(r)) {
                                JSONObject jsonObject = new JSONObject(r);
                                if (jsonObject != null && jsonObject.length() > 0) {
                                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                        JSONArray jsonData = new JSONArray(jsonObject.optString("data"));
                                        if (jsonData != null && jsonData.length() > 0) {
                                            JSONObject object = jsonData.optJSONObject(0);
                                            String whatsApp = object.optString("whatsapp");
                                            if (whatsApp.equalsIgnoreCase("1")) {
                                                switchWhatsApp.setChecked(true);
                                            } else {
                                                switchWhatsApp.setChecked(false);
                                            }
                                            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_WhatsApp_Notif, "" + whatsApp);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        }
    }

    void updateWhatsAppStatusDialog(String status) {
        Dialog dialog = new Dialog(homeActivity, R.style.NewDialog);
        dialog.setContentView(R.layout.whatsapp_status_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        window.setAttributes(lp);

        ImageView imgClose = dialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        TextView tvDone = dialog.findViewById(R.id.tvDone);
        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        ImageView ivWhatsAppStatus = dialog.findViewById(R.id.ivWhatsAppStatus);
        TextView tvTitle = dialog.findViewById(R.id.tvTitle);

        if (status.equalsIgnoreCase("1")) {
            tvTitle.setText(getResources().getString(R.string.str_whatapp_status_yes_txt));
            ivWhatsAppStatus.setImageResource(R.mipmap.whatsapp_yes_btn);
        } else {
            tvTitle.setText(getResources().getString(R.string.str_whatapp_status_no_txt));
            ivWhatsAppStatus.setImageResource(R.mipmap.whatsapp_no_btn);
        }
    }


    void setReferralTextView() {
        try {
            if (llReferralText != null) {
                llReferralText.removeAllViews();
                llReferralText.invalidate();
            }
            TextView view1 = new TextView(homeActivity);
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params1.setMargins(2, 0, 2, 4);
            view1.setLayoutParams(params1);
//            view1.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
            view1.setText(getResources().getText(R.string.str_referral_code_text) + " $ " + RewardPoints.getReferralRewardPoint(sharedPreferences) + " " + homeActivity.getResources().getString(R.string.str_points));

            String originalText1 = (String) view1.getText();
            int startPosition1 = getResources().getText(R.string.str_referral_code_text).length();

            SpannableString spannableStr1 = new SpannableString(originalText1);
            UnderlineSpan underlineSpan1 = new UnderlineSpan();
            spannableStr1.setSpan(underlineSpan1, startPosition1, startPosition1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            ForegroundColorSpan backgroundColorSpan1 = new ForegroundColorSpan(Color.rgb(255, 69, 0));
            spannableStr1.setSpan(backgroundColorSpan1, startPosition1, startPosition1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            spannableStr1.setSpan(new RelativeSizeSpan(0.8f), startPosition1, startPosition1, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            int $index1 = originalText1.indexOf("$");
            spannableStr1.setSpan(new ImageSpan(homeActivity, R.mipmap.small_reward), $index1, $index1 + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            view1.setText(spannableStr1);

            llReferralText.addView(view1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void submitReferralCode(String referralCode) {
        RetrofitClient.getInstance().getApi().submitReferralCode(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), SharedPreferencesUtils.getUserId(homeActivity),
                referralCode).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String r = response.body().string();
                        if (!TextUtils.isEmpty(r)) {
                            JSONObject jsonObject = new JSONObject(r);
                            if (jsonObject != null && jsonObject.length() > 0) {
                                if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                    llReferralView.setVisibility(View.GONE);
                                    Functions.updateRewardPoints(sharedPreferences, RewardPoints.getReferralRewardPoint(sharedPreferences));
                                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Referral_by, "" + referralCode);
                                    openReferralDhanyawad();
                                } else {
                                    String message = jsonObject.optString("status_message");
                                    Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), TextUtils.htmlEncode(message));
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

    void openReferralDhanyawad() {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_crop_info_dhanyawad, null);
        bottomSheetDialog.setContentView(view);
        bottomSheetDialog.setCanceledOnTouchOutside(false);

        AppCompatTextView txtOfflineSMS = view.findViewById(R.id.txtOfflineSMS);
        txtOfflineSMS.setVisibility(View.GONE);

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }


    void updateKhetLocationDialog() {
        try {
            BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(homeActivity);
            View sheetView = LayoutInflater.from(homeActivity).inflate(R.layout.khet_location_dialog, null);
            mBottomSheetDialog.setContentView(sheetView);

            TextView txtTitle = (TextView) sheetView.findViewById(R.id.txtTitle);
            AppCompatTextView txtOfflineSMS = sheetView.findViewById(R.id.txtOfflineSMS);
            if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                txtOfflineSMS.setVisibility(View.GONE);
            } else {
                txtOfflineSMS.setVisibility(View.VISIBLE);
            }

            TextView tvDone = (TextView) sheetView.findViewById(R.id.tvDone);
            tvDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Functions.updateRewardPoints(sharedPreferences, RewardPoints.getWeeklyCropInfoRewardPoint(sharedPreferences));
                    Functions.setFirebaseLogEventTrack(homeActivity, Constants.Click_Event, "Adhik (Setting)", "" + latitude + " - " + longitude, "Farm Location");

                    Bundle params = new Bundle();
                    params.putString(Constants.FB_EVENT_PARAM_CONTENT, "" + area);
                    AppEventsLogger.newLogger(homeActivity).logEvent(Constants.FB_EVENT_NAME_FIND_LOCATION, params);

                    if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                        updateFieldLocation();
                        mBottomSheetDialog.dismiss();
                    } else {
                        KhetOfflineData khetOfflineData = new KhetOfflineData();
                        khetOfflineData.setUid(new Random().nextInt());
                        khetOfflineData.setFieldId(field_id);
                        khetOfflineData.setSeason_id(season_id);
                        khetOfflineData.setP_crop_id(p_crop_id);
                        khetOfflineData.setLatitude("" + latitude);
                        khetOfflineData.setLongitude("" + longitude);
                        khetOfflineData.setKhet_address(area);

                        appDatabase.khetOfflineDao().nukeTable();
                        appDatabase.khetOfflineDao().insertAll(khetOfflineData);

                        llRewardView.setVisibility(View.GONE);
                        ivLocationBanner.setVisibility(View.GONE);
                        tvLocationText.setText(R.string.str_farm_location);
                        tvLocationAddress.setText(area + "\n" + latitude + " - " + longitude);
                        mBottomSheetDialog.dismiss();
                        Toast.makeText(homeActivity, "" + getResources().getString(R.string.str_info_darj_ho_gaya_he), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            ImageView imgClose = (ImageView) sheetView.findViewById(R.id.imgClose);
            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mBottomSheetDialog.dismiss();
                }
            });

            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) sheetView.getParent()).getLayoutParams();
            ((View) sheetView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
            mBottomSheetDialog.setCanceledOnTouchOutside(false);
            mBottomSheetDialog.setCancelable(false);
            mBottomSheetDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void updateFieldLocation() {
        RetrofitClient.getInstance().getApi().updateFieldLocation(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(homeActivity),
                sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), SharedPreferencesUtils.getUserId(homeActivity),
                season_id, p_crop_id, field_id, latitude + "", longitude + "", area).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    llRewardView.setVisibility(View.GONE);
                    ivLocationBanner.setVisibility(View.GONE);
                    tvLocationText.setText(getString(R.string.str_farm_location));
                    tvLocationAddress.setText(area + "\n" + latitude + " - " + longitude);
                    Toast.makeText(homeActivity, "" + getResources().getString(R.string.str_info_darj_ho_gaya_he), Toast.LENGTH_SHORT).show();
                    getProfile();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.v("updateFieldLocation", t.getMessage());
            }
        });
    }


    String stateEnglishToHindWord(String state) {
        String stateHindi = state;
        try {
            Log.v("stateEnglishToHindWord", "" + stateCityDataModels.size());
            if (!TextUtils.isEmpty(state) && stateCityDataModels.size() > 0) {
                for (int i = 0; i < stateCityDataModels.size(); i++) {
                    if (newState.equalsIgnoreCase(stateCityDataModels.get(i).getState_english())) {
                        stateHindi = stateCityDataModels.get(i).getState_hindi();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stateHindi;
    }

    String cityEnglishToHindWord(String state, String city) {
        String cityHindi = city;
        try {
            if (!TextUtils.isEmpty(state) && stateCityDataModels.size() > 0) {
                for (int i = 0; i < stateCityDataModels.size(); i++) {
                    if (state.equalsIgnoreCase(stateCityDataModels.get(i).getState_english())) {
                        for (int j = 0; j < stateCityDataModels.get(i).getCityList().size(); j++) {
                            if (city.equalsIgnoreCase(stateCityDataModels.get(i).getCityList().get(j).getCity_english())) {
                                cityHindi = stateCityDataModels.get(i).getCityList().get(j).getCity_hindi();
                            }
                        }
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cityHindi;
    }


    void offlineDataDisplay() {
        try {
            List<ProfileOfflineData> profileOfflineData = appDatabase.ProfileOfflineDao().getOfflineProfileCropId(RongoApp.getSelectedCrop());
            if (profileOfflineData != null && profileOfflineData.size() > 0) {

                txtUserFirstName.setText(profileOfflineData.get(0).getFirstName());
                txtNumber.setText(profileOfflineData.get(0).getPhoneNumber());

                newState = profileOfflineData.get(0).getState();
                Log.e("newState-off", "" + newState);
                List<String> spinnerState = new ArrayList<String>();
                spinnerState.add(stateEnglishToHindWord(newState));
                spinState.setAdapter(new SpinnerArrayAdapter(homeActivity, R.layout.spinner_item_list, spinnerState));

                newCity = profileOfflineData.get(0).getCity();
                Log.e("newCity", "" + newCity);
                List<String> spinnerCity = new ArrayList<String>();
                spinnerCity.add(cityEnglishToHindWord(newState, newCity));
                spinCity.setAdapter(new SpinnerArrayAdapter(homeActivity, R.layout.spinner_item_list, spinnerCity));

                if (newState.equalsIgnoreCase("Other")) {
                    txtAddress.setVisibility(View.VISIBLE);
                }

                if (newCity.equalsIgnoreCase("Other")) {
                    txtAddress.setVisibility(View.VISIBLE);
                }

                txtArea.setText("" + profileOfflineData.get(0).getFieldSize());
                sowingDate = profileOfflineData.get(0).getSowingDate();
                expectedSowingDate = profileOfflineData.get(0).getExpectedSowingDate();
                field_size = profileOfflineData.get(0).getFieldSize();
                field_id = profileOfflineData.get(0).getFieldId();
                is_sowing_done = profileOfflineData.get(0).getIs_sowing_done();
                p_crop_id = profileOfflineData.get(0).getP_crop_id();

                if (is_sowing_done.equalsIgnoreCase("") || is_sowing_done.equalsIgnoreCase("no")) {
                    txtBuaiTitle.setText(R.string.str_exp_buvay);
                    if (!expectedSowingDate.equalsIgnoreCase("0000-00-00 00:00:00")) {
                        txtSowingDateDisplay.setText(Functions.converOneDateFormatToOther("yyyy-MM-dd hh:mm:ss", "dd-MM-yyyy", expectedSowingDate));
                    } else {
                        txtSowingDateDisplay.setText(expectedSowingDate);
                    }
                } else {
                    txtBuaiTitle.setText(getString(R.string.str_buvay));
                    if (!sowingDate.equalsIgnoreCase("0000-00-00 00:00:00")) {
                        txtSowingDateDisplay.setText(Functions.converOneDateFormatToOther("yyyy-MM-dd hh:mm:ss", "dd-MM-yyyy", sowingDate));
                    } else {
                        txtSowingDateDisplay.setText(sowingDate);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void bindData() {
        ProfileResultData profileResultData = Functions.getData();
        if (profileResultData != null) {
            UserProfileData userProfileData = profileResultData.getUserProfileData();
            if (userProfileData != null) {

                txtUserFirstName.setText(userProfileData.getFirst_name());
                txtNumber.setText(userProfileData.getPhone_number());
                newState = userProfileData.getLocation_state();
                newCity = userProfileData.getLocation_city();

                txtAddress.setText(userProfileData.getLocation_area());

                if (newState.equalsIgnoreCase("Other")) {
                    txtAddress.setVisibility(View.VISIBLE);
                }

                if (newCity.equalsIgnoreCase("Other")) {
                    txtAddress.setVisibility(View.VISIBLE);
                }

                List<String> spinnerState = new ArrayList<String>();
                spinnerState.add(stateEnglishToHindWord(newState));
                SpinnerArrayAdapter spinnerArrayAdapter = new SpinnerArrayAdapter(homeActivity, R.layout.spinner_item_list, spinnerState);
                spinState.setAdapter(spinnerArrayAdapter);

                List<String> spinnerCity = new ArrayList<String>();
                spinnerCity.add(cityEnglishToHindWord(newState, newCity));
                SpinnerArrayAdapter spinnerArrayAdapter1 = new SpinnerArrayAdapter(homeActivity, R.layout.spinner_item_list, spinnerCity);
                spinCity.setAdapter(spinnerArrayAdapter1);

                SeasonData seasonData = profileResultData.getSeasonData();
                if (seasonData != null) {
                    season_id = seasonData.getSeason_id();
                    List<CropsData> cropsData = seasonData.getCropsDataList();
                    if (cropsData != null && cropsData.size() > 0) {
                        for (int i = 0; i < cropsData.size(); i++) {
                            if (sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, "1").equalsIgnoreCase(cropsData.get(i).getCrop_id())) {

                                sowingDate = cropsData.get(i).getDate_of_sowing();
                                expectedSowingDate = cropsData.get(i).getExpected_date_of_sowing();
                                field_size = cropsData.get(i).getField_size();
                                field_id = cropsData.get(i).getField_id();
                                is_sowing_done = cropsData.get(i).getIs_sowing_done();
                                p_crop_id = cropsData.get(i).getP_crop_id();

                                if (TextUtils.isEmpty(cropsData.get(i).getArea_unit())) {
                                    txtArea.setText(R.string.str_farm_agri);
                                } else {
                                    txtArea.setText("" + cropsData.get(i).getField_size());
                                }

                                if (TextUtils.isEmpty(sowingDate)) {
                                    sowingDate = sharedPreferences.getString(Constants.SharedPreference_SowingDate, "");
                                    expectedSowingDate = sharedPreferences.getString(Constants.SharedPreference_Expected_SowingDate, "");
                                    is_sowing_done = sharedPreferences.getString(Constants.SharedPreference_Sowing_Done, "");
                                }

                                if (is_sowing_done.equalsIgnoreCase("") || is_sowing_done.equalsIgnoreCase("no")) {
                                    txtBuaiTitle.setText(getString(R.string.str_exp_buvay));
                                    if (!expectedSowingDate.equalsIgnoreCase("0000-00-00 00:00:00")) {
                                        txtSowingDateDisplay.setText(Functions.converOneDateFormatToOther("yyyy-MM-dd hh:mm:ss", "dd-MM-yyyy", expectedSowingDate));
                                    } else {
                                        txtSowingDateDisplay.setText(expectedSowingDate);
                                    }
                                } else {
                                    txtBuaiTitle.setText(homeActivity.getResources().getString(R.string.str_buvay));
                                    if (!sowingDate.equalsIgnoreCase("0000-00-00 00:00:00")) {
                                        txtSowingDateDisplay.setText(Functions.converOneDateFormatToOther("yyyy-MM-dd hh:mm:ss", "dd-MM-yyyy", sowingDate));
                                    } else {
                                        txtSowingDateDisplay.setText(sowingDate);
                                    }
                                }
                                Log.v("is_sowing_done", is_sowing_done);

                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_SowingDate, cropsData.get(i).getDate_of_sowing());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_P_CROP_ID, cropsData.get(i).getP_crop_id());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_Field_ID, cropsData.get(i).getField_id());
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Sowing_Done, cropsData.get(i).getIs_sowing_done());
                            }
                        }

                    }
                }

            }
        }
    }


    void openEkarAreaDialog() {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_ekar_selection, null);
        bottomSheetDialog.setContentView(view);

        AppCompatTextView txtEnter = view.findViewById(R.id.txtEnter);
        AppCompatEditText editTextEkarArea = view.findViewById(R.id.editTextEkarArea);
        txtEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
                txtArea.setText("" + editTextEkarArea.getText().toString().trim());
//                txtSowingDateDisplay.setText("" + editTextSowingDate.getText().toString().trim());

                if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                    updateSettings();
                } else {
                    setProfileOffline();
                }
            }
        });

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    void openSowingDate(final String txt) {
        seedTypeData = SharedPreferencesUtils.getSeedTypes(homeActivity, Constants.SharedPreferences_SEED_TYPES);
        if (seedTypeData == null || seedTypeData.size() == 0) {
            RongoApp.getMasterDropDownData(true);
        }
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.sowing_date_dialog, null);
        bottomSheetDialog.setContentView(view);

        LinearLayout llSeasonValidation = view.findViewById(R.id.llSeasonValidation);
        llSeasonValidation.setVisibility(View.GONE);

        tvSeasonValidation = view.findViewById(R.id.tvSeasonValidation);
        tvSeasonValidationtxt = view.findViewById(R.id.tvSeasonValidationtxt);

        editTextSowingDate = view.findViewById(R.id.editTextSowingDate);
        AppCompatTextView txtEnter = view.findViewById(R.id.txtEnter);
        AppCompatTextView txtYourSowingText = view.findViewById(R.id.txtYourSowingText);
        if (is_sowing_done.equalsIgnoreCase("yes")) {
            txtYourSowingText.setText(R.string.str_apni_buvay_ki_date_darj_kare);
        } else {
            txtYourSowingText.setText(R.string.str_apni_expe_buvay_date_dark_kare);
        }

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        editTextSowingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openDatePicker(editTextSowingDate, txt, llSeasonValidation);
            }
        });

        txtEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(editTextSowingDate.getText().toString().trim())) {
                    txtSowingDateDisplay.setText("" + editTextSowingDate.getText().toString().trim());
                    bottomSheetDialog.dismiss();
                    openSeedTypes(txt);
                } else {
                    Toast.makeText(homeActivity, getResources().getString(R.string.str_date_darj_kare), Toast.LENGTH_SHORT).show();
                }
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    void openSeedTypes(final String txt) {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(homeActivity);
        View view = LayoutInflater.from(homeActivity).inflate(R.layout.bottom_dialog_seed_types, null);
        bottomSheetDialog.setContentView(view);

        AppCompatTextView txtEnter = view.findViewById(R.id.txtEnter);
        RecyclerView recyclerView = view.findViewById(R.id.rvSeedTypes);

        seedTypeData = SharedPreferencesUtils.getSeedTypes(homeActivity, Constants.SharedPreferences_SEED_TYPES);
        if (seedTypeData != null && seedTypeData.size() > 0) {
            recyclerView.setLayoutManager(new LinearLayoutManager(homeActivity, RecyclerView.VERTICAL, false));
            SeedTypesAdapter seedTypesAdapter = new SeedTypesAdapter(seedTypeData, this, seedType);
            recyclerView.setAdapter(seedTypesAdapter);
        }

        ImageView imgClose = view.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
            }
        });

        txtEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bottomSheetDialog.dismiss();
//                txtSowingDateDisplay.setText(""+editTextSowingDate.getText().toString().trim());

                if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                    updateSettings();
                } else {
                    setProfileOffline();
                }
            }
        });

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        ((View) view.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        bottomSheetDialog.show();
    }

    public void seedType(String s) {
        this.seedType = s;
    }

    void openDatePicker(AppCompatEditText appCompatEditText, String YesOrNo, LinearLayout llSeasonValidation) {
        llSeasonValidation.setVisibility(View.GONE);
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(homeActivity, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar c = Calendar.getInstance();
                c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                c.set(Calendar.YEAR, year);
                c.set(Calendar.MONTH, monthOfYear);

                fromDatePicker = "Y";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                String formattedDate = simpleDateFormat.format(c.getTime());
                Log.e("monthOfYear: ", "" + String.valueOf(monthOfYear));

                String sessionName = Functions.getSeasonName(homeActivity, String.valueOf(monthOfYear));
                Log.v("sessionName: ", "" + sessionName);
                if (RongoApp.getSelectedCrop().equalsIgnoreCase("2")) {
                    if (sessionName.equalsIgnoreCase(Constants.KHARIF_SEASON)) {
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                        appCompatEditText.setText(formattedDate);
                    } else {
                        String sm = "<b>" + getString(R.string.str_rice) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                        String sm1 = "<b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_rice) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_kharif_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                        tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                        tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                        llSeasonValidation.setVisibility(View.VISIBLE);
                        appCompatEditText.setText("");
                    }
                } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("3")) {
                    if (sessionName.equalsIgnoreCase(Constants.KHARIF_SEASON)) {
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                        appCompatEditText.setText(formattedDate);
                    } else {
                        String sm = "<b>" + getString(R.string.str_cotton) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                        String sm1 = "<b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_cotton) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_kharif_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                        tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                        tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                        llSeasonValidation.setVisibility(View.VISIBLE);
                        appCompatEditText.setText("");
                    }
                } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("4")) {
                    if (sessionName.equalsIgnoreCase(Constants.RABI_SEASON)) {
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                        appCompatEditText.setText(formattedDate);
                    } else {
                        String sm = "<b>" + getString(R.string.str_wheat) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                        String sm1 = "<b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_wheat) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_rabi_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                        tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                        tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                        llSeasonValidation.setVisibility(View.VISIBLE);
                        appCompatEditText.setText("");
                    }
                } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("6")) {
                    if (sessionName.equalsIgnoreCase(Constants.KHARIF_SEASON)) {
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                        appCompatEditText.setText(formattedDate);
                    } else {
                        String sm = "<b>" + getString(R.string.str_pearlmillet) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                        String sm1 = "<b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_pearlmillet) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_kharif_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                        tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                        tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                        llSeasonValidation.setVisibility(View.VISIBLE);
                        appCompatEditText.setText("");
                    }
                } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("7")) {
                    if (sessionName.equalsIgnoreCase(Constants.RABI_SEASON)) {
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                        appCompatEditText.setText(formattedDate);
                    } else {
                        String sm = "<b>" + getString(R.string.str_coriander) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                        String sm1 = "<b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_coriander) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_rabi_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                        tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                        tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                        llSeasonValidation.setVisibility(View.VISIBLE);
                        appCompatEditText.setText("");
                    }
                } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("8")) {
                    if (sessionName.equalsIgnoreCase(Constants.RABI_SEASON)) {
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                        appCompatEditText.setText(formattedDate);
                    } else {
                        String sm = "<b>" + getString(R.string.str_bittergourd) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                        String sm1 = "<b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_bittergourd) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_rabi_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                        tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                        tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                        llSeasonValidation.setVisibility(View.VISIBLE);
                        appCompatEditText.setText("");
                    }
                } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("9")) {
                    if (sessionName.equalsIgnoreCase(Constants.RABI_SEASON)) {
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                        appCompatEditText.setText(formattedDate);
                    } else {
                        String sm = "<b>" + getString(R.string.str_bottlegourd) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                        String sm1 = "<b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_bottlegourd) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_rabi_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                        tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                        tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                        llSeasonValidation.setVisibility(View.VISIBLE);
                        appCompatEditText.setText("");
                    }
                } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("10")) {
                    if (sessionName.equalsIgnoreCase(Constants.RABI_SEASON)) {
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                        appCompatEditText.setText(formattedDate);
                    } else {
                        String sm = "<b>" + getString(R.string.str_watermelon) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                        String sm1 = "<b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_watermelon) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_rabi_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                        tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                        tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                        llSeasonValidation.setVisibility(View.VISIBLE);
                        appCompatEditText.setText("");
                    }
                } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("11")) {
                    if (sessionName.equalsIgnoreCase(Constants.KHARIF_SEASON)) {
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                        appCompatEditText.setText(formattedDate);
                    } else {
                        String sm = "<b>" + getString(R.string.str_groundnut) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                        String sm1 = "<b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_groundnut) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_kharif_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                        tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                        tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                        llSeasonValidation.setVisibility(View.VISIBLE);
                        appCompatEditText.setText("");
                    }
                } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("12")) {
                    if (sessionName.equalsIgnoreCase(Constants.KHARIF_SEASON)) {
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                        appCompatEditText.setText(formattedDate);
                    } else {
                        String sm = "<b>" + getString(R.string.str_soyabean) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                        String sm1 = "<b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_soyabean) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_kharif_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                        tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                        tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                        llSeasonValidation.setVisibility(View.VISIBLE);
                        appCompatEditText.setText("");
                    }
                } else if (RongoApp.getSelectedCrop().equalsIgnoreCase("13")) {
                    if (sessionName.equalsIgnoreCase(Constants.KHARIF_SEASON)) {
                        SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                        appCompatEditText.setText(formattedDate);
                    } else {
                        String sm = "<b>" + getString(R.string.str_castor) + "</b> " + getString(R.string.str_ke_liye_season) + " <b>" + getString(R.string.str_rabi_season) + "</b> " + getString(R.string.str_no_data_available_season) + "";
                        String sm1 = "<b>" + getString(R.string.str_kharif_season) + "</b> " + getString(R.string.str_ke_season) + " " + getString(R.string.str_castor) + " " + getString(R.string.str_jankari_date_enter_season) + " <b>" + getString(R.string.str_kharif_season_month) + "</b> " + getString(R.string.str_month_darj_season) + "";

                        tvSeasonValidation.setText(Html.fromHtml(sm, Html.FROM_HTML_MODE_LEGACY));
                        tvSeasonValidationtxt.setText(Html.fromHtml(sm1, Html.FROM_HTML_MODE_LEGACY));
                        llSeasonValidation.setVisibility(View.VISIBLE);
                        appCompatEditText.setText("");
                    }
                } else {
                    SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_SEASON_NAME, "" + sessionName);
                    appCompatEditText.setText(formattedDate);
                }

            }
        }, mYear, mMonth, mDay);

        datePickerDialog.show();
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setMinHeight(200);
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setTextSize(homeActivity.getResources().getDimension(R.dimen._8sdp));
        datePickerDialog.getButton(DatePickerDialog.BUTTON_POSITIVE).setText(R.string.str_thik_he);
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setTextSize(homeActivity.getResources().getDimension(R.dimen._8sdp));
        datePickerDialog.getButton(DatePickerDialog.BUTTON_NEGATIVE).setText(getString(R.string.str_rad_kare));
    }

    boolean fieldsValidation() {
        if (TextUtils.isEmpty(txtUserFirstName.getText().toString().trim())) {
            Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_please_enter_first_name));
            return false;
        }
        if (TextUtils.isEmpty(txtNumber.getText().toString().trim())) {
            Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_please_enter_you_number));
            return false;
        }
        if (TextUtils.isEmpty(newState)) {
            Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_please_enter_state));
            return false;
        }
        if (TextUtils.isEmpty(newCity)) {
            Functions.showSnackBar(homeActivity.findViewById(android.R.id.content), getString(R.string.str_please_enter_jilla));
            return false;
        }

        return true;
    }


    void setProfileOffline() {
        try {
            String s = "0000-00-00 00:00:00", es = "0000-00-00 00:00:00";
            if (fromDatePicker.equalsIgnoreCase("Y")) {
                long dTime = Functions.covertToMili("dd-MM-yyyy", txtSowingDateDisplay.getText().toString().trim());
                if (System.currentTimeMillis() > dTime) {
                    is_sowing_done = "yes";
                    s = Functions.converOneDateFormatToOther("dd-MM-yyyy", "yyyy-MM-dd hh:mm:ss", txtSowingDateDisplay.getText().toString().trim());
                } else {
                    is_sowing_done = "no";
                    es = Functions.converOneDateFormatToOther("dd-MM-yyyy", "yyyy-MM-dd hh:mm:ss", txtSowingDateDisplay.getText().toString().trim());
                }
            } else {
                s = sowingDate;
                es = expectedSowingDate;
            }

            if (txtArea.getText().toString().trim().equalsIgnoreCase(getString(R.string.str_land_for_farming))) {
                field_size = "0";
            } else {
                field_size = txtArea.getText().toString().trim();
            }
            sowing = s;
            expsowing = es;

            ProfileOfflineData profileOfflineData = new ProfileOfflineData();
            profileOfflineData.setUid(new Random().nextInt());
            profileOfflineData.setFirstName(txtUserFirstName.getText().toString().trim());
            profileOfflineData.setLastName("");
            profileOfflineData.setPhoneNumber(txtNumber.getText().toString().trim());
            profileOfflineData.setStatus("1");
            profileOfflineData.setCity(newCity);
            profileOfflineData.setState(newState);
            profileOfflineData.setCountry(country);
            profileOfflineData.setFieldId(field_id);
            profileOfflineData.setFieldSize(field_size);
            profileOfflineData.setP_crop_id(RongoApp.getSelectedPCrop());
            profileOfflineData.setCrop_id(RongoApp.getSelectedCrop());
            profileOfflineData.setIs_sowing_done(is_sowing_done);
            profileOfflineData.setSowingDate(s);
            profileOfflineData.setExpectedSowingDate(es);
            profileOfflineData.setSeedType(seedType);
            profileOfflineData.setSeason_end_crop_week(String.valueOf(RongoApp.getSessionEndWeek()));

            List<ProfileOfflineData> OfflineData = appDatabase.ProfileOfflineDao().getAll();
            if (OfflineData != null && OfflineData.size() > 0) {
                appDatabase.ProfileOfflineDao().update(s, es, is_sowing_done, newCity, newState, RongoApp.getSelectedCrop());
            } else {
                appDatabase.ProfileOfflineDao().nukeTable();
                appDatabase.ProfileOfflineDao().insertAll(profileOfflineData);
            }
            Log.v("insert profileOffline:", "" + profileOfflineData.getSowingDate());

            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_First_Name, txtUserFirstName.getText().toString().trim());
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_Last_Name, "");
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_State, stateEnglishToHindWord(newState));
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_City, cityEnglishToHindWord(newState, newCity));
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_SowingDate, sowing);
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreferences_season_end_crop_week, String.valueOf(RongoApp.getSessionEndWeek()));
            SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_User_State, newState);

            Toast.makeText(homeActivity, "" + getResources().getString(R.string.str_info_darj_ho_gaya_he), Toast.LENGTH_SHORT).show();
            RongoApp.getCropWeekDaysFunction();
            setLanguageSpinnerData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    String getEnglishState(String state) {
        String stateEng = state;
        try {
            if (!TextUtils.isEmpty(state) && stateCityDataModels.size() > 0) {
                for (int i = 0; i < stateCityDataModels.size(); i++) {
                    if (newState.equalsIgnoreCase(stateCityDataModels.get(i).getState_hindi())) {
                        stateEng = stateCityDataModels.get(i).getState_english();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stateEng;
    }

    String getEnglishStateCity(String state, String city) {
        String cityEng = city;
        try {
            if (!TextUtils.isEmpty(state) && stateCityDataModels.size() > 0) {
                for (int i = 0; i < stateCityDataModels.size(); i++) {
                    if (state.equalsIgnoreCase(stateCityDataModels.get(i).getState_hindi())) {
                        for (int j = 0; j < stateCityDataModels.get(i).getCityList().size(); j++) {
                            if (city.equalsIgnoreCase(stateCityDataModels.get(i).getCityList().get(j).getCity_hindi())) {
                                cityEng = stateCityDataModels.get(i).getCityList().get(j).getCity_english();
                            }
                        }
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cityEng;
    }

    void updateSettings() {
        progressBarDialog.showProgressDialogWithTitle(getResources().getString(R.string.app_name));
        s = "0000-00-00 00:00:00";
        es = "0000-00-00 00:00:00";
        if (fromDatePicker.equalsIgnoreCase("Y")) {
            long dTime = Functions.covertToMili("dd-MM-yyyy", txtSowingDateDisplay.getText().toString().trim());
            if (System.currentTimeMillis() > dTime) {
                is_sowing_done = "yes";
                s = Functions.converOneDateFormatToOther("dd-MM-yyyy", "yyyy-MM-dd hh:mm:ss", txtSowingDateDisplay.getText().toString().trim());
            } else {
                is_sowing_done = "no";
                es = Functions.converOneDateFormatToOther("dd-MM-yyyy", "yyyy-MM-dd hh:mm:ss", txtSowingDateDisplay.getText().toString().trim());
            }
        } else {
            s = sowingDate;
            es = expectedSowingDate;
        }

        if (txtArea.getText().toString().trim().equalsIgnoreCase(getString(R.string.str_land_for_farming))) {
            field_size = "0";
        } else {
            field_size = txtArea.getText().toString().trim();
        }
        sowing = s;
        expsowing = es;
        RetrofitClient.getInstance().getApi().updateSetting(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(homeActivity),
                sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                SharedPreferencesUtils.getUserId(homeActivity), txtUserFirstName.getText().toString().trim(), "",
                txtNumber.getText().toString().trim(), newCity, getEnglishState(newState), country, latitude + "", longitude + "",
                pincode, getEnglishState(state), getEnglishStateCity(state, city), area, field_id, "acre", field_size, p_crop_id, is_sowing_done, s, es, seedType).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String str = response.body().string();
                    if (!TextUtils.isEmpty(str)) {
                        JSONObject jsonObject = new JSONObject(str);
                        if (jsonObject != null && jsonObject.length() > 0) {
                            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                if (personal.equalsIgnoreCase("Y")) {
                                    personal = "N";
                                    linearOtherContent.setVisibility(View.VISIBLE);
                                    linearText.setVisibility(View.VISIBLE);
                                    cardLanguage.setVisibility(View.VISIBLE);

                                    spinState.setEnabled(false);
                                    spinCity.setEnabled(false);

                                    txtPersonalDarjKare.setVisibility(View.GONE);
                                    imgPersonalJankariEdit.setVisibility(View.VISIBLE);
                                    rlCitySpinner.setVisibility(View.VISIBLE);

                                    txtUserFirstName.clearFocus();
                                    txtNumber.clearFocus();
                                    txtAddress.clearFocus();
                                }
                                SharedPreferencesUtils.storeStateOfString(sharedPreferences, Constants.SharedPreference_SowingDate, s);
                                SharedPreferencesUtils.storeStateLong(sharedPreferences, Constants.SharedPreference_Time_FOR_API_CALL, System.currentTimeMillis());
                                Toast.makeText(homeActivity, "" + getResources().getString(R.string.str_info_darj_ho_gaya_he), Toast.LENGTH_SHORT).show();

                                getProfile();
                                RongoApp.getWeeklyWeather();
                            }
                        }
                    }
                    progressBarDialog.hideProgressDialogWithTitle();
                } catch (Exception e) {
                    e.printStackTrace();
                    progressBarDialog.hideProgressDialogWithTitle();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        });
    }

    void getProfile() {
        progressBarDialog.showProgressDialogWithTitle(getResources().getString(R.string.app_name));
        RetrofitClient.getInstance().getApi().getProfile(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                RongoApp.getAppVersion(), Functions.getDeviceid(homeActivity),
                sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                SharedPreferencesUtils.getUserId(homeActivity)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null && response.isSuccessful()) {
                    try {
                        String r_str = response.body().string();
                        if (!TextUtils.isEmpty(r_str)) {
                            JSONObject jb = new JSONObject(r_str);
                            if (jb != null && jb.length() > 0) {

                                new InsertHomeFeedData(jb).insertData();
                                RongoApp.getCropWeekDaysFunction();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        bindData();
                                    }
                                }, 100);

                                setLanguageSpinnerData();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                progressBarDialog.hideProgressDialogWithTitle();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBarDialog.hideProgressDialogWithTitle();
            }
        });
    }


    void parseStateCityListData() {
        try {
            String stateCityData = sharedPreferences.getString(Constants.SharedPreferences_StateCityData, "");
            if (!TextUtils.isEmpty(stateCityData)) {
                JSONObject jsonObject = new JSONObject(stateCityData);
                if (jsonObject != null & jsonObject.length() > 0) {
                    JSONObject data = jsonObject.optJSONObject("data");
                    if (data != null && data.length() > 0) {
                        stateCityDataModels = new ArrayList<StateCityDataModel>();
                        JSONArray result1 = data.optJSONArray("result");
                        JSONArray result = new JSONArray(result1.optJSONObject(0).optString(RongoApp.getDefaultLanguage()));
                        if (result != null && result.length() > 0) {
                            for (int i = 0; i < result.length(); i++) {
                                JSONObject object = result.optJSONObject(i);

                                JSONObject state = object.optJSONObject("state");
                                if (state != null && state.length() > 0) {
                                    parseJsonState(i, state);
                                }

                                JSONObject district = object.optJSONObject("district");
                                if (district != null && district.length() > 0) {
                                    parseJsonCity(i, district);
                                }
                            }
                        }
                    }
                }
            }

            if (InternetConnection.checkConnectionForFasl(homeActivity)) {
                bindData();
            } else {
                List<ProfileOfflineData> profileOfflineData = appDatabase.ProfileOfflineDao().getAll();
                if (profileOfflineData != null && profileOfflineData.size() > 0) {
                    offlineDataDisplay();
                } else {
                    bindData();
                }
            }

            if (personal.equalsIgnoreCase("Y")) {
                setStateListData();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void parseJsonState(int id, JSONObject data) {
        StateCityDataModel stateCityDataModel = new StateCityDataModel();
        if (data != null) {
            Iterator<String> it = data.keys();
            while (it.hasNext()) {
                String key = it.next();
                try {
                    if (data.get(key) instanceof JSONArray) {
                        JSONArray arry = data.getJSONArray(key);
                        int size = arry.length();
                        for (int i = 0; i < size; i++) {
                            parseJsonState(i, arry.getJSONObject(i));
                        }
                    } else if (data.get(key) instanceof JSONObject) {
                        parseJsonState(0, data.getJSONObject(key));
                    } else {
//                        System.out.println("" + key + "-" + id + " : " + data.optString(key));
                        stateCityDataModel.setId(String.valueOf(id));
                        stateCityDataModel.setState_english(key);
                        stateCityDataModel.setState_hindi(data.optString(key));
                        stateCityDataModels.add(stateCityDataModel);
                    }
                } catch (Throwable e) {
//                    System.out.println("" + key + " : " + data.optString(key));
                    e.printStackTrace();
                }
            }
        }
    }

    void parseJsonCity(int id, JSONObject data) {
        ArrayList<StateCityDataModel.CityListModel> cityListModels = new ArrayList<>();
        if (data != null) {
            Iterator<String> it = data.keys();
            while (it.hasNext()) {
                String key = it.next();
                try {
                    if (data.get(key) instanceof JSONArray) {
                        JSONArray arry = data.getJSONArray(key);
                        int size = arry.length();
                        for (int i = 0; i < size; i++) {
                            parseJsonState(i, arry.getJSONObject(i));
                        }
                    } else if (data.get(key) instanceof JSONObject) {
                        parseJsonState(0, data.getJSONObject(key));
                    } else {
//                        System.out.println("" + key + "-" + id + " : " + data.optString(key));
                        StateCityDataModel.CityListModel cityListModel = new StateCityDataModel.CityListModel();
                        cityListModel.setId(String.valueOf(id));
                        cityListModel.setCity_english(key);
                        cityListModel.setCity_hindi(data.optString(key));
                        cityListModels.add(cityListModel);
                    }
                } catch (Throwable e) {
//                    System.out.println("" + key + " : " + data.optString(key));
                    e.printStackTrace();
                }
            }
            stateCityDataModels.get(id).setCityList(cityListModels);
        }
    }

    void setStateListData() {
        ArrayList<String> stateList = new ArrayList<>();
        for (int i = 0; i < stateCityDataModels.size(); i++) {
            stateList.add(stateCityDataModels.get(i).getState_hindi());
        }

        SpinnerHintArrayAdapter spinnerArrayAdapter = new SpinnerHintArrayAdapter(homeActivity, R.layout.spinner_item_list, stateList);
        spinnerArrayAdapter.add(getString(R.string.str_select_state));
        spinState.setAdapter(spinnerArrayAdapter);
        spinState.setSelection(spinnerArrayAdapter.getCount());
        spinState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                if (!item.equalsIgnoreCase(getString(R.string.str_select_state))) {
                    if (item.equalsIgnoreCase(getString(R.string.str_other))) {
                        txtAddress.setVisibility(View.VISIBLE);
                        rlCitySpinner.setVisibility(View.GONE);
                        newCity = "Other";
                        newState = "Other";
                    } else {
                        rlCitySpinner.setVisibility(View.VISIBLE);
                        txtAddress.setVisibility(View.GONE);
                        for (int i = 0; i < stateCityDataModels.size(); i++) {
                            if (item.equalsIgnoreCase(stateCityDataModels.get(i).getState_hindi())) {
                                newState = stateCityDataModels.get(i).getState_english();
                            }
                        }
                    }
                    setCityListData(item);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (!TextUtils.isEmpty(newState)) {
            for (int i = 0; i < stateCityDataModels.size(); i++) {
                if (newState.equalsIgnoreCase(stateCityDataModels.get(i).getState_english())) {
                    spinState.setSelection(i);
                }
            }
        }
    }

    void setCityListData(final String state) {
        ArrayList<String> cityList = new ArrayList<>();
        for (int i = 0; i < stateCityDataModels.size(); i++) {
            if (state.equalsIgnoreCase(stateCityDataModels.get(i).getState_hindi())) {
                for (int j = 0; j < stateCityDataModels.get(i).getCityList().size(); j++) {
                    cityList.add(stateCityDataModels.get(i).getCityList().get(j).getCity_hindi());
                }
            }
        }

        SpinnerHintArrayAdapter spinnerArrayAdapter = new SpinnerHintArrayAdapter(homeActivity, R.layout.spinner_item_list, cityList);
        spinnerArrayAdapter.add(getString(R.string.str_select_jila));
        spinCity.setAdapter(spinnerArrayAdapter);
        spinCity.setSelection(spinnerArrayAdapter.getCount());
        spinCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                if (!item.equalsIgnoreCase(getString(R.string.str_select_jila))) {
                    if (item.equalsIgnoreCase(getString(R.string.str_other))) {
                        txtAddress.setVisibility(View.VISIBLE);
                        newCity = "Other";
                    } else {
                        txtAddress.setVisibility(View.GONE);
                        for (int i = 0; i < stateCityDataModels.size(); i++) {
                            if (state.equalsIgnoreCase(stateCityDataModels.get(i).getState_hindi())) {
                                for (int j = 0; j < stateCityDataModels.get(i).getCityList().size(); j++) {
                                    if (item.equalsIgnoreCase(stateCityDataModels.get(i).getCityList().get(j).getCity_hindi())) {
                                        newCity = stateCityDataModels.get(i).getCityList().get(j).getCity_english();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        for (int i = 0; i < stateCityDataModels.size(); i++) {
            if (newState.equalsIgnoreCase(stateCityDataModels.get(i).getState_english())) {
                for (int j = 0; j < stateCityDataModels.get(i).getCityList().size(); j++) {
                    if (newCity.equalsIgnoreCase(stateCityDataModels.get(i).getCityList().get(j).getCity_english())) {
                        spinCity.setSelection(j);
                    }
                }
            }
        }
        spinnerArrayAdapter.notifyDataSetChanged();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 202 && resultCode == RESULT_OK) {
            Place place = Autocomplete.getPlaceFromIntent(data);
            Log.i("TAG", "Place: " + place.getName() + ", " + place.getId() + ", " + place.getAddress());
            txtAddress.setText(place.getAddress());
            area = place.getAddress();
            latitude = Objects.requireNonNull(place.getLatLng()).latitude;
            longitude = place.getLatLng().longitude;
            Log.i("TAG", "Lat-Long: " + latitude + ", " + longitude);
            getAddressFromLocation(latitude, longitude, homeActivity, new GeocoderHandler(), true);
        }
    }

    void onSearchCalled() {
        if (!Places.isInitialized()) {
            Places.initialize(getActivity(), getResources().getString(R.string.place_key));
        }
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).setCountry("IN").build(homeActivity);
        homeActivity.startActivityForResult(intent, 202);
    }

    void getAddressFromLocation(final double latitude, final double longitude, final Context context, final Handler handler, boolean isChange) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.US);
                String result = null;
                try {
                    List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);
                    if (addressList != null && addressList.size() > 0) {
                        Address address = addressList.get(0);
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                            sb.append(address.getAddressLine(i));
                        }
                        String street = address.getThoroughfare();
                        if (!TextUtils.isEmpty(street)) {
                            sb.append(street).append(", ");
                        }
                        if (!TextUtils.isEmpty(address.getLocality())) {
                            sb.append(address.getLocality()).append(", ");
                            city = address.getLocality();
                        }
                        if (!TextUtils.isEmpty(address.getAdminArea())) {
                            sb.append(address.getAdminArea()).append(", ");
                            state = address.getAdminArea();
                        }
                        if (!TextUtils.isEmpty(address.getCountryName())) {
                            country = address.getCountryName();
                            if (!TextUtils.isEmpty(address.getPostalCode())) {
                                sb.append(address.getCountryName()).append(", ");
                            } else {
                                sb.append(address.getCountryName()).append("");
                            }
                        }
                        if (!TextUtils.isEmpty(address.getPostalCode())) {
                            sb.append(address.getPostalCode()).append("");
                            pincode = address.getPostalCode();
                        }

                        result = sb.toString();
//                        area = result;

                        if (isChange) {
                            if (!TextUtils.isEmpty(state) && stateCityDataModels.size() > 0) {
                                for (int i = 0; i < stateCityDataModels.size(); i++) {
                                    if (state.trim().equalsIgnoreCase(stateCityDataModels.get(i).getState_english().trim())) {
                                        newState = stateCityDataModels.get(i).getState_hindi();
                                        break;
                                    } else {
                                        newState = "Other";
                                    }
                                }
                                Log.e("+++ state1 ++", "" + newState + "-" + city);
                            }
                        }

                    }
                } catch (IOException e) {
                    Log.e("Location Address Loader", "Unable connect to Geocoder", e);
                } finally {
                    Message message = Message.obtain();
                    message.setTarget(handler);
                    if (result != null) {
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        bundle.putString("address", result);
                        message.setData(bundle);
                    } else {
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        result = " Unable to get address for this location.";
                        bundle.putString("address", result);
                        message.setData(bundle);
                    }
                    message.sendToTarget();
                }
            }
        };
        thread.start();
    }

}
