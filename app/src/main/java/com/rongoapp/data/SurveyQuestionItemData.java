package com.rongoapp.data;

import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.rongoapp.controller.ListConverters;

import java.io.Serializable;
import java.util.List;

public class SurveyQuestionItemData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int _id;
    private String id;
    private String question;
    private String field_type;
    @TypeConverters({ListConverters.class})
    private List<String> options;


    public SurveyQuestionItemData(int _id, String id, String question, String field_type, List<String> options) {
        this._id = _id;
        this.id = id;
        this.question = question;
        this.field_type = field_type;
        this.options = options;
    }

    public int get_id() {
        return _id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getField_type() {
        return field_type;
    }

    public void setField_type(String field_type) {
        this.field_type = field_type;
    }

    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }
}
