package com.rongoapp.data;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class TrackUserOfflineData {

    @PrimaryKey
    private int uid;

    @ColumnInfo(name = "user_id")
    private String userId;

    @ColumnInfo(name = "last_login_time")
    private String lastLoginTime;

    @ColumnInfo(name = "app_time_spent")
    private String appTimeSpent;

    @ColumnInfo(name = "is_action")
    private boolean isAction;

    @ColumnInfo(name = "user_track_event")
    private String userTrackEvent;


    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(String lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getAppTimeSpent() {
        return appTimeSpent;
    }

    public void setAppTimeSpent(String appTimeSpent) {
        this.appTimeSpent = appTimeSpent;
    }

    public boolean isAction() {
        return isAction;
    }

    public void setAction(boolean action) {
        isAction = action;
    }

    public String getUserTrackEvent() {
        return userTrackEvent;
    }

    public void setUserTrackEvent(String userTrackEvent) {
        this.userTrackEvent = userTrackEvent;
    }
}