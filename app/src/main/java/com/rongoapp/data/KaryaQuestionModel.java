package com.rongoapp.data;

public class KaryaQuestionModel {

    String question;
    String inputType;
    String infoText;
    String infoType;
    String options;

    public KaryaQuestionModel() {
    }

    public KaryaQuestionModel(String question, String inputType, String infoText, String infoType, String options) {
        this.question = question;
        this.inputType = inputType;
        this.infoText = infoText;
        this.infoType = infoType;
        this.options = options;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getInputType() {
        return inputType;
    }

    public void setInputType(String inputType) {
        this.inputType = inputType;
    }

    public String getInfoText() {
        return infoText;
    }

    public void setInfoText(String infoText) {
        this.infoText = infoText;
    }

    public String getInfoType() {
        return infoType;
    }

    public void setInfoType(String infoType) {
        this.infoType = infoType;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }
}