package com.rongoapp.data;

import java.io.Serializable;
import java.util.List;

public class CropsData implements Serializable {

    private String field_id;
    private String area_unit;
    private String geo_latitude;
    private String geo_longitude;
    private String geo_metadata;
    private String field_size;
    private String p_crop_id;
    private String is_sowing_done;
    private String expected_date_of_sowing;
    private String date_of_sowing;
    private String seed_type;
    private String crop_in_days;
    private String crop_in_weeks;
    private String crop_id;
    private List<CropWeekDetails> crop_week_detail;
    private String seed_metadata;
    private String crop_status;
    private String season_feedback;
    private String season_end_crop_week;
    private String app_language;

    public CropsData() {
    }

    public CropsData(String field_id, String area_unit, String geo_latitude, String geo_longitude, String geo_metadata, String field_size,
                     String p_crop_id, String is_sowing_done, String expected_date_of_sowing, String date_of_sowing, String seed_type,
                     String crop_in_days, String crop_in_weeks, String crop_id, List<CropWeekDetails> crop_week_detail,
                     String seed_metadata, String crop_status, String season_feedback, String season_end_crop_week,
                     String app_language) {
        this.field_id = field_id;
        this.area_unit = area_unit;
        this.geo_latitude = geo_latitude;
        this.geo_longitude = geo_longitude;
        this.geo_metadata = geo_metadata;
        this.field_size = field_size;
        this.p_crop_id = p_crop_id;
        this.is_sowing_done = is_sowing_done;
        this.expected_date_of_sowing = expected_date_of_sowing;
        this.date_of_sowing = date_of_sowing;
        this.seed_type = seed_type;
        this.crop_in_days = crop_in_days;
        this.crop_in_weeks = crop_in_weeks;
        this.crop_id = crop_id;
        this.crop_week_detail = crop_week_detail;
        this.seed_metadata = seed_metadata;
        this.crop_status = crop_status;
        this.season_feedback = season_feedback;
        this.season_end_crop_week = season_end_crop_week;
        this.app_language = app_language;
    }

    public String getApp_language() {
        return app_language;
    }

    public void setApp_language(String app_language) {
        this.app_language = app_language;
    }

    public String getCrop_status() {
        return crop_status;
    }

    public void setCrop_status(String crop_status) {
        this.crop_status = crop_status;
    }

    public String getSeason_feedback() {
        return season_feedback;
    }

    public void setSeason_feedback(String season_feedback) {
        this.season_feedback = season_feedback;
    }

    public String getSeason_end_crop_week() {
        return season_end_crop_week;
    }

    public void setSeason_end_crop_week(String season_end_crop_week) {
        this.season_end_crop_week = season_end_crop_week;
    }

    public String getField_id() {
        return field_id;
    }

    public void setField_id(String field_id) {
        this.field_id = field_id;
    }

    public String getArea_unit() {
        return area_unit;
    }

    public void setArea_unit(String area_unit) {
        this.area_unit = area_unit;
    }

    public String getGeo_latitude() {
        return geo_latitude;
    }

    public void setGeo_latitude(String geo_latitude) {
        this.geo_latitude = geo_latitude;
    }

    public String getGeo_longitude() {
        return geo_longitude;
    }

    public void setGeo_longitude(String geo_longitude) {
        this.geo_longitude = geo_longitude;
    }

    public String getGeo_metadata() {
        return geo_metadata;
    }

    public void setGeo_metadata(String geo_metadata) {
        this.geo_metadata = geo_metadata;
    }

    public String getField_size() {
        return field_size;
    }

    public void setField_size(String field_size) {
        this.field_size = field_size;
    }

    public String getP_crop_id() {
        return p_crop_id;
    }

    public void setP_crop_id(String p_crop_id) {
        this.p_crop_id = p_crop_id;
    }

    public String getIs_sowing_done() {
        return is_sowing_done;
    }

    public void setIs_sowing_done(String is_sowing_done) {
        this.is_sowing_done = is_sowing_done;
    }

    public String getExpected_date_of_sowing() {
        return expected_date_of_sowing;
    }

    public void setExpected_date_of_sowing(String expected_date_of_sowing) {
        this.expected_date_of_sowing = expected_date_of_sowing;
    }

    public String getDate_of_sowing() {
        return date_of_sowing;
    }

    public void setDate_of_sowing(String date_of_sowing) {
        this.date_of_sowing = date_of_sowing;
    }

    public String getSeed_type() {
        return seed_type;
    }

    public void setSeed_type(String seed_type) {
        this.seed_type = seed_type;
    }

    public String getCrop_in_days() {
        return crop_in_days;
    }

    public void setCrop_in_days(String crop_in_days) {
        this.crop_in_days = crop_in_days;
    }

    public String getCrop_in_weeks() {
        return crop_in_weeks;
    }

    public void setCrop_in_weeks(String crop_in_weeks) {
        this.crop_in_weeks = crop_in_weeks;
    }

    public String getCrop_id() {
        return crop_id;
    }

    public void setCrop_id(String crop_id) {
        this.crop_id = crop_id;
    }

    public List<CropWeekDetails> getCropWeekDetailsLis() {
        return crop_week_detail;
    }

    public void setCropWeekDetailsLis(List<CropWeekDetails> crop_week_detail) {
        this.crop_week_detail = crop_week_detail;
    }

    public String getSeed_metadata() {
        return seed_metadata;
    }

    public void setSeed_metadata(String seed_metadata) {
        this.seed_metadata = seed_metadata;
    }


}
