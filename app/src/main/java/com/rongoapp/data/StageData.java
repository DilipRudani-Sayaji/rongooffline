package com.rongoapp.data;

import java.io.Serializable;

public class StageData implements Serializable {

    private String stage_id;
    private String stage_name;
    private String stage_start_week;
    private String stage_end_week;
    private String crop_area_focused;
    private String display_name;

    public StageData(String stage_id, String stage_name, String stage_start_week, String stage_end_week, String crop_area_focused, String display_name) {
        this.stage_id = stage_id;
        this.stage_name = stage_name;
        this.stage_start_week = stage_start_week;
        this.stage_end_week = stage_end_week;
        this.crop_area_focused = crop_area_focused;
        this.display_name = display_name;
    }

    public String getStage_id() {
        return stage_id;
    }

    public void setStage_id(String stage_id) {
        this.stage_id = stage_id;
    }

    public String getStage_name() {
        return stage_name;
    }

    public void setStage_name(String stage_name) {
        this.stage_name = stage_name;
    }

    public String getStage_start_week() {
        return stage_start_week;
    }

    public void setStage_start_week(String stage_start_week) {
        this.stage_start_week = stage_start_week;
    }

    public String getStage_end_week() {
        return stage_end_week;
    }

    public void setStage_end_week(String stage_end_week) {
        this.stage_end_week = stage_end_week;
    }

    public String getCrop_area_focused() {
        return crop_area_focused;
    }

    public void setCrop_area_focused(String crop_area_focused) {
        this.crop_area_focused = crop_area_focused;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }
}
