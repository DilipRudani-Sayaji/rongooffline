package com.rongoapp.data;

import java.io.Serializable;

public class ExpertResponceModel implements Serializable {

    private String posted_user_name;
    private String comment_id;
    private String post_id;
    private String type;
    private String user_id;
    private String comment_text;
    private String is_action_required;
    private String is_applied;
    private String not_applied_reason;
    private String is_useful = "";
    private String not_useful_reason;
    private String created_on;
    private String likes;
    private String vote_type;
    private String is_expert;
    private String recording_local_path;
    private String recording_key = "";
    private String keyword_advisory;

    public ExpertResponceModel() {
    }

    public ExpertResponceModel(String posted_user_name, String comment_id, String post_id, String type, String user_id, String comment_text,
                               String is_action_required, String is_applied, String not_applied_reason, String is_useful, String not_useful_reason,
                               String created_on, String likes, String vote_type, String is_expert, String recording_local_path,
                               String recording_key, String keyword_advisory) {
        this.keyword_advisory = keyword_advisory;
        this.posted_user_name = posted_user_name;
        this.comment_id = comment_id;
        this.post_id = post_id;
        this.type = type;
        this.user_id = user_id;
        this.comment_text = comment_text;
        this.is_action_required = is_action_required;
        this.is_applied = is_applied;
        this.not_applied_reason = not_applied_reason;
        this.is_useful = is_useful;
        this.not_useful_reason = not_useful_reason;
        this.created_on = created_on;
        this.likes = likes;
        this.vote_type = vote_type;
        this.is_expert = is_expert;
        this.recording_local_path = recording_local_path;
        this.recording_key = recording_key;
    }

    public String getKeyword_advisory() {
        return keyword_advisory;
    }

    public void setKeyword_advisory(String keyword_advisory) {
        this.keyword_advisory = keyword_advisory;
    }

    public String getRecording_local_path() {
        return recording_local_path;
    }

    public void setRecording_local_path(String recording_local_path) {
        this.recording_local_path = recording_local_path;
    }

    public String getRecording_key() {
        return recording_key;
    }

    public void setRecording_key(String recording_key) {
        this.recording_key = recording_key;
    }

    public String getIs_expert() {
        return is_expert;
    }

    public void setIs_expert(String is_expert) {
        this.is_expert = is_expert;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getPosted_user_name() {
        return posted_user_name;
    }

    public void setPosted_user_name(String posted_user_name) {
        this.posted_user_name = posted_user_name;
    }

    public String getComment_id() {
        return comment_id;
    }

    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getComment_text() {
        return comment_text;
    }

    public void setComment_text(String comment_text) {
        this.comment_text = comment_text;
    }

    public String getIs_action_required() {
        return is_action_required;
    }

    public void setIs_action_required(String is_action_required) {
        this.is_action_required = is_action_required;
    }

    public String getIs_applied() {
        return is_applied;
    }

    public void setIs_applied(String is_applied) {
        this.is_applied = is_applied;
    }

    public String getNot_applied_reason() {
        return not_applied_reason;
    }

    public void setNot_applied_reason(String not_applied_reason) {
        this.not_applied_reason = not_applied_reason;
    }

    public String getIs_useful() {
        return is_useful;
    }

    public void setIs_useful(String is_useful) {
        this.is_useful = is_useful;
    }

    public String getNot_useful_reason() {
        return not_useful_reason;
    }

    public void setNot_useful_reason(String not_useful_reason) {
        this.not_useful_reason = not_useful_reason;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getVote_type() {
        return vote_type;
    }

    public void setVote_type(String vote_type) {
        this.vote_type = vote_type;
    }
}
