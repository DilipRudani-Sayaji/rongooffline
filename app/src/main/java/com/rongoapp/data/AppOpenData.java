package com.rongoapp.data;

import android.content.pm.PackageInfo;

public class AppOpenData {

    public PackageInfo mPackageInfo;
    public String androidId = "";
    public String mDeviceId = "";
    public String mBrand = "";
    public String mModel = "";
    public String mDeviceType = "";
    public String mOsVersion = "";
    public String currentAppVersion = "";
    public String mInterNetConnection = "";
    public String mCurrentDateTime = "";
    public String mLatitude = "";
    public String mLongitude = "";

    public String getmDeviceId() {
        return mDeviceId;
    }

    public void setmDeviceId(String mDeviceId) {
        this.mDeviceId = mDeviceId;
    }

    public PackageInfo getmPackageInfo() {
        return mPackageInfo;
    }

    public void setmPackageInfo(PackageInfo mPackageInfo) {
        this.mPackageInfo = mPackageInfo;
    }

    public String getAndroidId() {
        return androidId;
    }

    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }

    public String getmBrand() {
        return mBrand;
    }

    public void setmBrand(String mBrand) {
        this.mBrand = mBrand;
    }

    public String getmModel() {
        return mModel;
    }

    public void setmModel(String mModel) {
        this.mModel = mModel;
    }

    public String getmDeviceType() {
        return mDeviceType;
    }

    public void setmDeviceType(String mDeviceType) {
        this.mDeviceType = mDeviceType;
    }

    public String getmOsVersion() {
        return mOsVersion;
    }

    public void setmOsVersion(String mOsVersion) {
        this.mOsVersion = mOsVersion;
    }

    public String getCurrentAppVersion() {
        return currentAppVersion;
    }

    public void setCurrentAppVersion(String currentAppVersion) {
        this.currentAppVersion = currentAppVersion;
    }

    public String getmInterNetConnection() {
        return mInterNetConnection;
    }

    public void setmInterNetConnection(String mInterNetConnection) {
        this.mInterNetConnection = mInterNetConnection;
    }

    public String getmCurrentDateTime() {
        return mCurrentDateTime;
    }

    public void setmCurrentDateTime(String mCurrentDateTime) {
        this.mCurrentDateTime = mCurrentDateTime;
    }

    public String getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(String mLatitude) {
        this.mLatitude = mLatitude;
    }

    public String getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(String mLongitude) {
        this.mLongitude = mLongitude;
    }

}
