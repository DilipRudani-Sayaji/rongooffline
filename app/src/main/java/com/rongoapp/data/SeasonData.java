package com.rongoapp.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SeasonData implements Serializable {

    private String season_id;
    private String season_start_date;
    private String season_end_date;
    List<CropsData> crops = new ArrayList<>();

    public SeasonData(String season_id, String season_start_date, String season_end_date, List<CropsData> crops) {
        this.season_id = season_id;
        this.season_start_date = season_start_date;
        this.season_end_date = season_end_date;
        this.crops = crops;
    }

    public String getSeason_id() {
        return season_id;
    }

    public String getSeason_start_date() {
        return season_start_date;
    }

    public String getSeason_end_date() {
        return season_end_date;
    }

    public List<CropsData> getCropsDataList() {
        return crops;
    }
}
