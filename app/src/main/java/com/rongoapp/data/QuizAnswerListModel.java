package com.rongoapp.data;

public class QuizAnswerListModel {

    String user_id;
    String first_name;
    String location_state;
    String location_city;
    String total;

    public QuizAnswerListModel() {
    }

    public QuizAnswerListModel(String user_id, String first_name, String location_state, String location_city, String total) {
        this.user_id = user_id;
        this.first_name = first_name;
        this.location_state = location_state;
        this.location_city = location_city;
        this.total = total;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLocation_state() {
        return location_state;
    }

    public void setLocation_state(String location_state) {
        this.location_state = location_state;
    }

    public String getLocation_city() {
        return location_city;
    }

    public void setLocation_city(String location_city) {
        this.location_city = location_city;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}