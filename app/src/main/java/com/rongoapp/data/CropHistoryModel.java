package com.rongoapp.data;

import java.io.Serializable;

public class CropHistoryModel implements Serializable {

    private String p_crop_id;
    private String user_id;
    private String crop_id;
    private String is_sowing_done;
    private String date_of_sowing;
    private String date_of_harvest;
    private String date_of_season_end;
    private String crop_status;
    private String expected_date_of_sowing;

    public CropHistoryModel() {
    }

    public String getExpected_date_of_sowing() {
        return expected_date_of_sowing;
    }

    public void setExpected_date_of_sowing(String expected_date_of_sowing) {
        this.expected_date_of_sowing = expected_date_of_sowing;
    }

    public String getP_crop_id() {
        return p_crop_id;
    }

    public void setP_crop_id(String p_crop_id) {
        this.p_crop_id = p_crop_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCrop_id() {
        return crop_id;
    }

    public void setCrop_id(String crop_id) {
        this.crop_id = crop_id;
    }

    public String getIs_sowing_done() {
        return is_sowing_done;
    }

    public void setIs_sowing_done(String is_sowing_done) {
        this.is_sowing_done = is_sowing_done;
    }

    public String getDate_of_sowing() {
        return date_of_sowing;
    }

    public void setDate_of_sowing(String date_of_sowing) {
        this.date_of_sowing = date_of_sowing;
    }

    public String getDate_of_harvest() {
        return date_of_harvest;
    }

    public void setDate_of_harvest(String date_of_harvest) {
        this.date_of_harvest = date_of_harvest;
    }

    public String getDate_of_season_end() {
        return date_of_season_end;
    }

    public void setDate_of_season_end(String date_of_season_end) {
        this.date_of_season_end = date_of_season_end;
    }

    public String getCrop_status() {
        return crop_status;
    }

    public void setCrop_status(String crop_status) {
        this.crop_status = crop_status;
    }
}
