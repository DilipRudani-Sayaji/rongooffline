package com.rongoapp.data;

import java.io.Serializable;

public class CropWeekDetails implements Serializable {

    private int crop_week;
    private String stage_id;
    private String stage_name;
    private String display_name;
    private String stage_start_week;
    private String stage_end_week;
    private int is_new_stage;

    public CropWeekDetails() {
    }

    public CropWeekDetails(int crop_week, String stage_id, String stage_name, String display_name, String stage_start_week, String stage_end_week, int is_new_stage) {
        this.crop_week = crop_week;
        this.stage_id = stage_id;
        this.stage_name = stage_name;
        this.display_name = display_name;
        this.stage_start_week = stage_start_week;
        this.stage_end_week = stage_end_week;
        this.is_new_stage = is_new_stage;
    }

    public int getCrop_week() {
        return crop_week;
    }

    public void setCrop_week(int crop_week) {
        this.crop_week = crop_week;
    }

    public String getStage_id() {
        return stage_id;
    }

    public void setStage_id(String stage_id) {
        this.stage_id = stage_id;
    }

    public String getStage_name() {
        return stage_name;
    }

    public void setStage_name(String stage_name) {
        this.stage_name = stage_name;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getStage_start_week() {
        return stage_start_week;
    }

    public void setStage_start_week(String stage_start_week) {
        this.stage_start_week = stage_start_week;
    }

    public String getStage_end_week() {
        return stage_end_week;
    }

    public void setStage_end_week(String stage_end_week) {
        this.stage_end_week = stage_end_week;
    }

    public int getIs_new_stage() {
        return is_new_stage;
    }

    public void setIs_new_stage(int is_new_stage) {
        this.is_new_stage = is_new_stage;
    }
}
