package com.rongoapp.data;

public class MasterCropData {

    String crop_name;
    String crop_id;
    String crop_image;
    String display_name;
    String status;

    public MasterCropData() {
    }

    public MasterCropData(String crop_name, String crop_id,String crop_image, String display_name, String status) {
        this.crop_name = crop_name;
        this.crop_id = crop_id;
        this.crop_image = crop_image;
        this.display_name = display_name;
        this.status = status;
    }

    public String getCrop_image() {
        return crop_image;
    }

    public void setCrop_image(String crop_image) {
        this.crop_image = crop_image;
    }

    public String getCrop_name() {
        return crop_name;
    }

    public void setCrop_name(String crop_name) {
        this.crop_name = crop_name;
    }

    public String getCrop_id() {
        return crop_id;
    }

    public void setCrop_id(String crop_id) {
        this.crop_id = crop_id;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
