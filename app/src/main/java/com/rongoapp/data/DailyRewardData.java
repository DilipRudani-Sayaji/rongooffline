package com.rongoapp.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class DailyRewardData {

    @PrimaryKey
    private int uid;

    private String reward_points;

    private String metadata;

    private String created_on;

    public DailyRewardData() {
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getReward_points() {
        return reward_points;
    }

    public void setReward_points(String reward_points) {
        this.reward_points = reward_points;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }
}