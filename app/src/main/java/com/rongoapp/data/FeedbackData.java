package com.rongoapp.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class FeedbackData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public int _id;
    String userid;
    String rate;
    String feedbackText;
    String recording_local_path;

    public FeedbackData() {
    }

    public FeedbackData(int _id, String userid,String rate, String feedbackText, String recording_local_path) {
        this._id = _id;
        this.userid = userid;
        this.rate = rate;
        this.feedbackText = feedbackText;
        this.recording_local_path = recording_local_path;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getFeedbackText() {
        return feedbackText;
    }

    public void setFeedbackText(String feedbackText) {
        this.feedbackText = feedbackText;
    }

    public String getRecording_local_path() {
        return recording_local_path;
    }

    public void setRecording_local_path(String recording_local_path) {
        this.recording_local_path = recording_local_path;
    }
}
