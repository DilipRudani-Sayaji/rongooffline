package com.rongoapp.data;

public class ChemicalNameModel {

    String technical;
    String normal;

    public ChemicalNameModel() {
    }

    public ChemicalNameModel(String technical, String normal) {
        this.technical = technical;
        this.normal = normal;
    }

    public String getTechnical() {
        return technical;
    }

    public void setTechnical(String technical) {
        this.technical = technical;
    }

    public String getNormal() {
        return normal;
    }

    public void setNormal(String normal) {
        this.normal = normal;
    }
}