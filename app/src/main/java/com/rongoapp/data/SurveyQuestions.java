package com.rongoapp.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.rongoapp.controller.DataConverter;

import java.io.Serializable;
import java.util.List;

@Entity
public class SurveyQuestions implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int _id;
    private int keyValue;

    @TypeConverters(DataConverter.class)
    List<SurveyQuestionItemData> surveyQuestionItemData;

    public SurveyQuestions(int _id, int keyValue, List<SurveyQuestionItemData> surveyQuestionItemData) {
        this._id = _id;
        this.keyValue = keyValue;
        this.surveyQuestionItemData = surveyQuestionItemData;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public void setKeyValue(int keyValue) {
        this.keyValue = keyValue;
    }

    public int get_id() {
        return _id;
    }

    public int getKeyValue() {
        return keyValue;
    }

    public List<SurveyQuestionItemData> getSurveyQuestionItemData() {
        return surveyQuestionItemData;
    }

    public void setSurveyQuestionItemData(List<SurveyQuestionItemData> surveyQuestionItemData) {
        this.surveyQuestionItemData = surveyQuestionItemData;
    }
}
