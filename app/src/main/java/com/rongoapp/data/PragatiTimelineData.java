package com.rongoapp.data;

import java.io.Serializable;
import java.util.List;

public class PragatiTimelineData implements Serializable {

    String weekly_crop_health;
    String crop_info;
    List<String> images = null;
    String imagePath;
    String status;
    String cropWeek;

    public PragatiTimelineData(String cropWeek, String weekly_crop_health, String crop_info, List<String> images, String imagePath, String status) {
        this.cropWeek = cropWeek;
        this.weekly_crop_health = weekly_crop_health;
        this.crop_info = crop_info;
        this.images = images;
        this.imagePath = imagePath;
        this.status = status;
    }

    public String getCropWeek() {
        return cropWeek;
    }

    public void setCropWeek(String cropWeek) {
        this.cropWeek = cropWeek;
    }

    public String getCrop_info() {
        return crop_info;
    }

    public void setCrop_info(String crop_info) {
        this.crop_info = crop_info;
    }

    public String getWeekly_crop_health() {
        return weekly_crop_health;
    }

    public void setWeekly_crop_health(String weekly_crop_health) {
        this.weekly_crop_health = weekly_crop_health;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
