package com.rongoapp.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class OfflineCommentData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int _id;
    private String user_id;
    private String post_id;
    private String comment_text;
    private String likes;
    private String view="";
    private String datetime;

    public OfflineCommentData() {
    }

    public OfflineCommentData(int _id, String user_id, String post_id, String comment_text, String likes, String view, String datetime) {
        this._id = _id;
        this.user_id = user_id;
        this.post_id = post_id;
        this.comment_text = comment_text;
        this.likes = likes;
        this.view = view;
        this.datetime = datetime;
    }


    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getComment_text() {
        return comment_text;
    }

    public void setComment_text(String comment_text) {
        this.comment_text = comment_text;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

}
