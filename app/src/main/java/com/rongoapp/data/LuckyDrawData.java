package com.rongoapp.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.rongoapp.controller.ListConverters;

import java.io.Serializable;
import java.util.ArrayList;

@Entity
public class LuckyDrawData implements Serializable {

    @PrimaryKey (autoGenerate = true)
    public int _id;
    private String seasonId;
    private String cropId;
    private String pcropId;
    private String crop_week;
    private String focus_area;
    private String latitude;
    private String longitude;

    @TypeConverters(ListConverters.class)
    private ArrayList<String> imagesPath;

    public LuckyDrawData(int _id, String seasonId, String cropId,String pcropId, String crop_week, String focus_area, String latitude, String longitude,ArrayList<String> imagesPath) {
        this._id = _id;
        this.seasonId = seasonId;
        this.cropId = cropId;
        this.pcropId = pcropId;
        this.crop_week = crop_week;
        this.focus_area = focus_area;
        this.latitude = latitude;
        this.longitude = longitude;
        this.imagesPath = imagesPath;
    }

    public String getPcropId() {
        return pcropId;
    }

    public void setPcropId(String pcropId) {
        this.pcropId = pcropId;
    }

    public ArrayList<String> getImagesPath() {
        return imagesPath;
    }

    public int get_id() {
        return _id;
    }

    public String getSeasonId() {
        return seasonId;
    }

    public String getCropId() {
        return cropId;
    }

    public String getCrop_week() {
        return crop_week;
    }

    public String getFocus_area() {
        return focus_area;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
}
