package com.rongoapp.data;

import java.io.Serializable;
import java.util.ArrayList;

public class StateCityDataModel implements Serializable {

    private String id;
    private String state_hindi;
    private String state_english;
    private ArrayList<CityListModel> cityList;

    public StateCityDataModel() {
    }

    public StateCityDataModel(String id, String state_hindi, String state_english, ArrayList<CityListModel> cityList) {
        this.id = id;
        this.state_hindi = state_hindi;
        this.state_english = state_english;
        this.cityList = cityList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getState_hindi() {
        return state_hindi;
    }

    public void setState_hindi(String state_hindi) {
        this.state_hindi = state_hindi;
    }

    public String getState_english() {
        return state_english;
    }

    public void setState_english(String state_english) {
        this.state_english = state_english;
    }

    public ArrayList<CityListModel> getCityList() {
        return cityList;
    }

    public void setCityList(ArrayList<CityListModel> cityList) {
        this.cityList = cityList;
    }

    public static class CityListModel {

        private String id;
        private String city_hindi;
        private String city_english;

        public CityListModel() {
        }

        public CityListModel(String id, String city_hindi, String city_english) {
            this.id = id;
            this.city_hindi = city_hindi;
            this.city_english = city_english;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCity_hindi() {
            return city_hindi;
        }

        public void setCity_hindi(String city_hindi) {
            this.city_hindi = city_hindi;
        }

        public String getCity_english() {
            return city_english;
        }

        public void setCity_english(String city_english) {
            this.city_english = city_english;
        }
    }

}
