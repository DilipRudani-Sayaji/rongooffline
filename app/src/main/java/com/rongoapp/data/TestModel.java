package com.rongoapp.data;

import android.util.Pair;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

public class TestModel implements Serializable {

    private String a;
    private String b;

    public TestModel(String a, String b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestModel pair = (TestModel) o;
        return (a == pair.a && b == pair.b) || (a == pair.b && b == pair.a);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(new HashSet<>(Arrays.asList(a,b)));
    }

    @Override
    public String toString() {
        return "Pair{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }
}
