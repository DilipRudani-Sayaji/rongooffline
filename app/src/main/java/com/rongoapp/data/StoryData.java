package com.rongoapp.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class StoryData implements Serializable {

    @SerializedName("stories")
    @Expose
    private List<Story> stories = null;

    public List<Story> getStories() {
        return stories;
    }

    public void setStories(List<Story> stories) {
        this.stories = stories;
    }

    public class Story {

        @SerializedName("story_id")
        @Expose
        private String storyId;
        @SerializedName("category")
        @Expose
        private String category;
        @SerializedName("story_thumbnail_path")
        @Expose
        private String storyThumbnailPath;
        @SerializedName("story_type")
        @Expose
        private String storyType;
        @SerializedName("action_param")
        @Expose
        private String actionParam;
        private String description;
        @SerializedName("cards")
        @Expose
        private List<Card> cards = null;


        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String storySelected = "0";

        public String getStorySelected() {
            return storySelected;
        }

        public void setStorySelected(String storySelected) {
            this.storySelected = storySelected;
        }

        public String getStoryId() {
            return storyId;
        }

        public void setStoryId(String storyId) {
            this.storyId = storyId;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getStoryThumbnailPath() {
            return storyThumbnailPath;
        }

        public void setStoryThumbnailPath(String storyThumbnailPath) {
            this.storyThumbnailPath = storyThumbnailPath;
        }

        public String getStoryType() {
            return storyType;
        }

        public void setStoryType(String storyType) {
            this.storyType = storyType;
        }

        public String getActionParam() {
            return actionParam;
        }

        public void setActionParam(String actionParam) {
            this.actionParam = actionParam;
        }

        public List<Card> getCards() {
            return cards;
        }

        public void setCards(List<Card> cards) {
            this.cards = cards;
        }

    }

    public class Card {

        @SerializedName("card_id")
        @Expose
        private String cardId;
        @SerializedName("card_type")
        @Expose
        private String cardType;
        @SerializedName("card_media_type")
        @Expose
        private String cardMediaType;
        @SerializedName("card_media_path")
        @Expose
        private String cardMediaPath;
        @SerializedName("card_description")
        @Expose
        private String cardDescription;
        @SerializedName("card_duration")
        @Expose
        private String cardDuration;
        @SerializedName("card_display_order")
        @Expose
        private String cardDisplayOrder;

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }

        public String getCardType() {
            return cardType;
        }

        public void setCardType(String cardType) {
            this.cardType = cardType;
        }

        public String getCardMediaType() {
            return cardMediaType;
        }

        public void setCardMediaType(String cardMediaType) {
            this.cardMediaType = cardMediaType;
        }

        public String getCardMediaPath() {
            return cardMediaPath;
        }

        public void setCardMediaPath(String cardMediaPath) {
            this.cardMediaPath = cardMediaPath;
        }

        public String getCardDescription() {
            return cardDescription;
        }

        public void setCardDescription(String cardDescription) {
            this.cardDescription = cardDescription;
        }

        public String getCardDuration() {
            return cardDuration;
        }

        public void setCardDuration(String cardDuration) {
            this.cardDuration = cardDuration;
        }

        public String getCardDisplayOrder() {
            return cardDisplayOrder;
        }

        public void setCardDisplayOrder(String cardDisplayOrder) {
            this.cardDisplayOrder = cardDisplayOrder;
        }

    }

}
