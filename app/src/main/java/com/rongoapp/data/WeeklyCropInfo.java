package com.rongoapp.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class WeeklyCropInfo implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int _id;
    private String cropWeek;
    private String cropDay;
    private String isDone;
    private String cropId;
    private String pcropId;
    private String infoTypo;
    private String status;

    public WeeklyCropInfo() {
    }

    public String getCropId() {
        return cropId;
    }

    public void setCropId(String cropId) {
        this.cropId = cropId;
    }

    public String getPcropId() {
        return pcropId;
    }

    public void setPcropId(String pcropId) {
        this.pcropId = pcropId;
    }

    public String getInfoTypo() {
        return infoTypo;
    }

    public void setInfoTypo(String infoTypo) {
        this.infoTypo = infoTypo;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getCropWeek() {
        return cropWeek;
    }

    public void setCropWeek(String cropWeek) {
        this.cropWeek = cropWeek;
    }

    public String getIsDone() {
        return isDone;
    }

    public void setIsDone(String isDone) {
        this.isDone = isDone;
    }

    public String getCropDay() {
        return cropDay;
    }

    public void setCropDay(String cropDay) {
        this.cropDay = cropDay;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
