package com.rongoapp.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeeklyWeatherData {

    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("status_message")
    @Expose
    private String statusMessage;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Cyclone {

        @SerializedName("symptoms")
        @Expose
        private List<Object> symptoms = null;
        @SerializedName("management")
        @Expose
        private List<Object> management = null;
        @SerializedName("advisory_image")
        @Expose
        private String advisoryImage;
        @SerializedName("display_name")
        @Expose
        private String displayName;

        public List<Object> getSymptoms() {
            return symptoms;
        }

        public void setSymptoms(List<Object> symptoms) {
            this.symptoms = symptoms;
        }

        public List<Object> getManagement() {
            return management;
        }

        public void setManagement(List<Object> management) {
            this.management = management;
        }

        public String getAdvisoryImage() {
            return advisoryImage;
        }

        public void setAdvisoryImage(String advisoryImage) {
            this.advisoryImage = advisoryImage;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

    }

    public class Data {

        @SerializedName("result")
        @Expose
        private Result result;

        public Result getResult() {
            return result;
        }

        public void setResult(Result result) {
            this.result = result;
        }

    }

    public class Drought {

        @SerializedName("symptoms")
        @Expose
        private List<String> symptoms = null;
        @SerializedName("management")
        @Expose
        private List<String> management = null;
        @SerializedName("advisory_image")
        @Expose
        private String advisoryImage;
        @SerializedName("display_name")
        @Expose
        private String displayName;

        public List<String> getSymptoms() {
            return symptoms;
        }

        public void setSymptoms(List<String> symptoms) {
            this.symptoms = symptoms;
        }

        public List<String> getManagement() {
            return management;
        }

        public void setManagement(List<String> management) {
            this.management = management;
        }

        public String getAdvisoryImage() {
            return advisoryImage;
        }

        public void setAdvisoryImage(String advisoryImage) {
            this.advisoryImage = advisoryImage;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

    }

    public class Frost {

        @SerializedName("symptoms")
        @Expose
        private List<String> symptoms = null;
        @SerializedName("management")
        @Expose
        private List<String> management = null;
        @SerializedName("advisory_image")
        @Expose
        private String advisoryImage;
        @SerializedName("display_name")
        @Expose
        private String displayName;

        public List<String> getSymptoms() {
            return symptoms;
        }

        public void setSymptoms(List<String> symptoms) {
            this.symptoms = symptoms;
        }

        public List<String> getManagement() {
            return management;
        }

        public void setManagement(List<String> management) {
            this.management = management;
        }

        public String getAdvisoryImage() {
            return advisoryImage;
        }

        public void setAdvisoryImage(String advisoryImage) {
            this.advisoryImage = advisoryImage;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

    }

    public class HighTemperature {

        @SerializedName("symptoms")
        @Expose
        private List<String> symptoms = null;
        @SerializedName("management")
        @Expose
        private List<String> management = null;
        @SerializedName("advisory_image")
        @Expose
        private String advisoryImage;
        @SerializedName("display_name")
        @Expose
        private String displayName;

        public List<String> getSymptoms() {
            return symptoms;
        }

        public void setSymptoms(List<String> symptoms) {
            this.symptoms = symptoms;
        }

        public List<String> getManagement() {
            return management;
        }

        public void setManagement(List<String> management) {
            this.management = management;
        }

        public String getAdvisoryImage() {
            return advisoryImage;
        }

        public void setAdvisoryImage(String advisoryImage) {
            this.advisoryImage = advisoryImage;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

    }

    public class Result {

        @SerializedName("weather_data")
        @Expose
        private List<WeatherDatum> weatherData = null;
        @SerializedName("is_suitable")
        @Expose
        private Integer isSuitable;
        @SerializedName("suitable_text")
        @Expose
        private String suitableText;
        @SerializedName("location_city")
        @Expose
        private String locationCity;

//        @SerializedName("weather_advisory")
//        @Expose
//        private WeatherAdvisory weatherAdvisory;
        @SerializedName("weather_alert")
        @Expose
        private List<String> weatherAlert = null;

        public List<WeatherDatum> getWeatherData() {
            return weatherData;
        }

        public void setWeatherData(List<WeatherDatum> weatherData) {
            this.weatherData = weatherData;
        }

        public String getLocationCity() {
            return locationCity;
        }

        public void setLocationCity(String locationCity) {
            this.locationCity = locationCity;
        }

        public Integer getIsSuitable() {
            return isSuitable;
        }

        public void setIsSuitable(Integer isSuitable) {
            this.isSuitable = isSuitable;
        }

        public String getSuitableText() {
            return suitableText;
        }

        public void setSuitableText(String suitableText) {
            this.suitableText = suitableText;
        }

//        public WeatherAdvisory getWeatherAdvisory() {
//            return weatherAdvisory;
//        }
//
//        public void setWeatherAdvisory(WeatherAdvisory weatherAdvisory) {
//            this.weatherAdvisory = weatherAdvisory;
//        }

        public List<String> getWeatherAlert() {
            return weatherAlert;
        }

        public void setWeatherAlert(List<String> weatherAlert) {
            this.weatherAlert = weatherAlert;
        }

    }

    public class WaterLogging {

        @SerializedName("symptoms")
        @Expose
        private List<String> symptoms = null;
        @SerializedName("management")
        @Expose
        private List<String> management = null;
        @SerializedName("advisory_image")
        @Expose
        private String advisoryImage;
        @SerializedName("display_name")
        @Expose
        private String displayName;

        public List<String> getSymptoms() {
            return symptoms;
        }

        public void setSymptoms(List<String> symptoms) {
            this.symptoms = symptoms;
        }

        public List<String> getManagement() {
            return management;
        }

        public void setManagement(List<String> management) {
            this.management = management;
        }

        public String getAdvisoryImage() {
            return advisoryImage;
        }

        public void setAdvisoryImage(String advisoryImage) {
            this.advisoryImage = advisoryImage;
        }

        public String getDisplayName() {
            return displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

    }

    public class WeatherAdvisory {

        @SerializedName("stage")
        @Expose
        private String stage;
        @SerializedName("general_advice")
        @Expose
        private List<String> generalAdvice = null;
//        @SerializedName("rainfall_required")
//        @Expose
//        private List<Object> rainfallRequired = null;
//        @SerializedName("temperature_required")
        @Expose
        private List<Object> temperatureRequired = null;
        @SerializedName("high_temperature")
        @Expose
        private HighTemperature highTemperature;
        @SerializedName("frost")
        @Expose
        private Frost frost;
        @SerializedName("drought")
        @Expose
        private Drought drought;
        @SerializedName("water_logging")
        @Expose
        private WaterLogging waterLogging;
        @SerializedName("cyclone")
        @Expose
        private Cyclone cyclone;
        @SerializedName("image_path")
        @Expose
        private String imagePath;

        public String getStage() {
            return stage;
        }

        public void setStage(String stage) {
            this.stage = stage;
        }

        public List<String> getGeneralAdvice() {
            return generalAdvice;
        }

        public void setGeneralAdvice(List<String> generalAdvice) {
            this.generalAdvice = generalAdvice;
        }

//        public List<Object> getRainfallRequired() {
//            return rainfallRequired;
//        }
//
//        public void setRainfallRequired(List<Object> rainfallRequired) {
//            this.rainfallRequired = rainfallRequired;
//        }

        public List<Object> getTemperatureRequired() {
            return temperatureRequired;
        }

        public void setTemperatureRequired(List<Object> temperatureRequired) {
            this.temperatureRequired = temperatureRequired;
        }

        public HighTemperature getHighTemperature() {
            return highTemperature;
        }

        public void setHighTemperature(HighTemperature highTemperature) {
            this.highTemperature = highTemperature;
        }

        public Frost getFrost() {
            return frost;
        }

        public void setFrost(Frost frost) {
            this.frost = frost;
        }

        public Drought getDrought() {
            return drought;
        }

        public void setDrought(Drought drought) {
            this.drought = drought;
        }

        public WaterLogging getWaterLogging() {
            return waterLogging;
        }

        public void setWaterLogging(WaterLogging waterLogging) {
            this.waterLogging = waterLogging;
        }

        public Cyclone getCyclone() {
            return cyclone;
        }

        public void setCyclone(Cyclone cyclone) {
            this.cyclone = cyclone;
        }

        public String getImagePath() {
            return imagePath;
        }

        public void setImagePath(String imagePath) {
            this.imagePath = imagePath;
        }

    }

    public static class WeatherDatum {

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("min")
        @Expose
        private Integer min;
        @SerializedName("max")
        @Expose
        private Integer max;
        @SerializedName("rainfall")
        @Expose
        private Object rainfall;
        @SerializedName("rainfall_perc")
        @Expose
        private Object rainfallPerc;
        @SerializedName("weather_condition")
        @Expose
        private String weatherCondition;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public Integer getMin() {
            return min;
        }

        public void setMin(Integer min) {
            this.min = min;
        }

        public Integer getMax() {
            return max;
        }

        public void setMax(Integer max) {
            this.max = max;
        }

        public Object getRainfall() {
            return rainfall;
        }

        public void setRainfall(Object rainfall) {
            this.rainfall = rainfall;
        }

        public Object getRainfallPerc() {
            return rainfallPerc;
        }

        public void setRainfallPerc(Object rainfallPerc) {
            this.rainfallPerc = rainfallPerc;
        }

        public String getWeatherCondition() {
            return weatherCondition;
        }

        public void setWeatherCondition(String weatherCondition) {
            this.weatherCondition = weatherCondition;
        }

    }

}
