package com.rongoapp.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class OfflineQuizData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int _id;
    private String quizID;
    private String cropWeek;
    private String cropDay;
    private String cropId;
    private String pcropId;
    private String ansValue;
    private String is_true;
    private String metaData;

    public OfflineQuizData() {
    }

    public String getCropId() {
        return cropId;
    }

    public void setCropId(String cropId) {
        this.cropId = cropId;
    }

    public String getPcropId() {
        return pcropId;
    }

    public void setPcropId(String pcropId) {
        this.pcropId = pcropId;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getQuizID() {
        return quizID;
    }

    public void setQuizID(String quizID) {
        this.quizID = quizID;
    }

    public String getCropWeek() {
        return cropWeek;
    }

    public void setCropWeek(String cropWeek) {
        this.cropWeek = cropWeek;
    }

    public String getCropDay() {
        return cropDay;
    }

    public void setCropDay(String cropDay) {
        this.cropDay = cropDay;
    }

    public String getAnsValue() {
        return ansValue;
    }

    public void setAnsValue(String ansValue) {
        this.ansValue = ansValue;
    }

    public String getIs_true() {
        return is_true;
    }

    public void setIs_true(String is_true) {
        this.is_true = is_true;
    }

    public String getMetaData() {
        return metaData;
    }

    public void setMetaData(String metaData) {
        this.metaData = metaData;
    }
}
