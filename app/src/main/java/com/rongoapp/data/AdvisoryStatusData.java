package com.rongoapp.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class AdvisoryStatusData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int _id;
    private String cropWeek;
    private String isDone;
    private String cropId;
    private String pcropId;
    private String isHealth;

    public AdvisoryStatusData(int _id, String cropWeek, String isDone, String cropId, String pcropId, String isHealth) {
        this._id = _id;
        this.cropWeek = cropWeek;
        this.isDone = isDone;
        this.cropId = cropId;
        this.pcropId = pcropId;
        this.isHealth = isHealth;
    }

    public String getCropId() {
        return cropId;
    }

    public void setCropId(String cropId) {
        this.cropId = cropId;
    }

    public String getPcropId() {
        return pcropId;
    }

    public void setPcropId(String pcropId) {
        this.pcropId = pcropId;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getCropWeek() {
        return cropWeek;
    }

    public void setCropWeek(String cropWeek) {
        this.cropWeek = cropWeek;
    }

    public String getIsDone() {
        return isDone;
    }

    public void setIsDone(String isDone) {
        this.isDone = isDone;
    }

    public String getIsHealth() {
        return isHealth;
    }

    public void setIsHealth(String isHealth) {
        this.isHealth = isHealth;
    }
}
