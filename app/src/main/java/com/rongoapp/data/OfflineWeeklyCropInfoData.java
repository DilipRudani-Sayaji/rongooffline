package com.rongoapp.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class OfflineWeeklyCropInfoData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int _id;
    private String p_crop_id;
    private String season_id;
    private String crop_week;
    private String crop_id;
    private String field_id;
    private String info_type;
    private String info_text;
    private String info_value;
    private String info_metadata;
    private String user_id;

    public OfflineWeeklyCropInfoData() {
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getP_crop_id() {
        return p_crop_id;
    }

    public void setP_crop_id(String p_crop_id) {
        this.p_crop_id = p_crop_id;
    }

    public String getSeason_id() {
        return season_id;
    }

    public void setSeason_id(String season_id) {
        this.season_id = season_id;
    }

    public String getCrop_week() {
        return crop_week;
    }

    public void setCrop_week(String crop_week) {
        this.crop_week = crop_week;
    }

    public String getCrop_id() {
        return crop_id;
    }

    public void setCrop_id(String crop_id) {
        this.crop_id = crop_id;
    }

    public String getField_id() {
        return field_id;
    }

    public void setField_id(String field_id) {
        this.field_id = field_id;
    }

    public String getInfo_type() {
        return info_type;
    }

    public void setInfo_type(String info_type) {
        this.info_type = info_type;
    }

    public String getInfo_text() {
        return info_text;
    }

    public void setInfo_text(String info_text) {
        this.info_text = info_text;
    }

    public String getInfo_value() {
        return info_value;
    }

    public void setInfo_value(String info_value) {
        this.info_value = info_value;
    }

    public String getInfo_metadata() {
        return info_metadata;
    }

    public void setInfo_metadata(String info_metadata) {
        this.info_metadata = info_metadata;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
