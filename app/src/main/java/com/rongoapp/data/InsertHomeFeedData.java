package com.rongoapp.data;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class InsertHomeFeedData {

    public JSONObject jsonObject;
    UserProfileData userProfileData;
    SeasonData seasonData;

    public InsertHomeFeedData(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    public ProfileResultData insertData() {
        if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
            JSONObject jData = jsonObject.optJSONObject("data");
            if (jData != null && jData.length() > 0) {
                JSONObject jresult = jData.optJSONObject("result");
                if (jresult != null && jresult.length() > 0) {

                    String is_new_version_available = jresult.optString("is_new_version_available");
                    SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                            Constants.SharedPreference_App_Version, is_new_version_available);

                    String referral_message = jresult.optString("referral_message");
                    SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                            Constants.SharedPreference_Referral_Link, referral_message);

                    String invite_note = jresult.optString("invite_note");
                    SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                            Constants.SharedPreference_Invite_Msg, invite_note);

                    String expert_response_posts = jresult.optString("expert_response_posts");
                    SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                            Constants.SharedPreference_Story_PostId, expert_response_posts);

                    String image_path = jresult.optString("image_path");
                    SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                            Constants.SharedPreferences_IMAGE_PATH, "" + image_path);

                    String last_survey_id = jresult.optString("last_survey_id");
                    String invite_people_count = jresult.optString("invite_people_count");
                    String display_content_expiry = jresult.optString("display_content_expiry");
                    String api_content_expiry = jresult.optString("api_content_expiry");
                    String api_profile_expiry = jresult.optString("api_profile_expiry");
                    int view_pending_count = jresult.optInt("view_pending_count");
                    String helpline_number = jresult.optString("helpline_number");

                    SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                            Constants.SharedPreference_Salah_Counter, "" + view_pending_count);

                    if (view_pending_count > 0) {
                        SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                                Constants.SharedPreferences_Salah_Tab, "1");
                    }

                    SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                            Constants.SharedPreference_RongoHelpLineNumber, "" + helpline_number);

                    JSONObject jprofile = jresult.optJSONObject("profile");
                    if (jprofile != null && jprofile.length() > 0) {
                        String user_id = jprofile.optString("user_id");
                        String first_name = jprofile.optString("first_name");
                        String last_name = jprofile.optString("last_name");
                        String phone_number = jprofile.optString("phone_number");
                        String status = jprofile.optString("status");
                        String preference = jprofile.optString("preference");
                        String location_city = jprofile.optString("location_city");
                        String location_state = jprofile.optString("location_state");
                        String location_area_city = jprofile.optString("location_area_city");
                        String location_area_state = jprofile.optString("location_area_state");
                        String location_area = jprofile.optString("location_area");
                        String reward_points = jprofile.optString("reward_points");
                        String reward_metadata = jprofile.optString("reward_metadata");
                        String is_expert = jprofile.optString("is_expert");
                        String profile_rank = jprofile.optString("profile_rank");
                        String community_interest = jprofile.optString("community_interest");

                        SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                                Constants.SharedPreference_community_interest, community_interest);

                        SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                                Constants.SharedPreference_User_State, location_state);

                        String referral_code = jprofile.optString("referral_code");
                        SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                                Constants.SharedPreference_Referral_code, referral_code);

                        String referred_by = jprofile.optString("referred_by");
                        SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                                Constants.SharedPreference_Referral_by, referred_by);

                        SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                                Constants.SharedPreference_First_Name, "" + first_name);

                        SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                                Constants.SharedPreference_Last_Name, "" + last_name);

                        SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                                Constants.SharedPreferences_REWARD_POINT, "" + reward_points);

                        userProfileData = new UserProfileData(user_id, first_name, last_name, phone_number, status,
                                preference, location_city, location_state, location_area_city, location_area_state, location_area);
                    }

                    JSONObject jseason = jresult.optJSONObject("season");
                    if (jseason != null && jseason.length() > 0) {
                        String season_id = jseason.optString("season_id");
                        String season_name = jseason.optString("season_name");
                        String season_start_date = jseason.optString("season_start_date");
                        String season_end_date = jseason.optString("season_end_date");
                        SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                                Constants.SharedPreferences_SEASON_ID, season_id);

                        JSONArray jsonArray = jseason.optJSONArray("crops");
                        List<CropsData> cropsDataList = new ArrayList<>();
                        if (jsonArray != null && jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jb_season = jsonArray.optJSONObject(i);
                                String field_id = jb_season.optString("field_id");
                                String geo_latitude = jb_season.optString("geo_latitude");
                                String geo_longitude = jb_season.optString("geo_longitude");
                                String geo_metadata = jb_season.optString("geo_metadata");
                                String area_unit = jb_season.optString("area_unit");
                                String field_size = jb_season.optString("field_size");
                                String p_crop_id = jb_season.optString("p_crop_id");
                                String is_sowing_done = jb_season.optString("is_sowing_done");
                                String expected_date_of_sowing = jb_season.optString("expected_date_of_sowing");
                                String date_of_sowing = jb_season.optString("date_of_sowing");
                                String seed_type = jb_season.optString("seed_type");
                                String crop_in_days = jb_season.optString("crop_in_days");
                                String crop_in_weeks = jb_season.optString("crop_in_weeks");
                                String crop_id = jb_season.optString("crop_id");
                                String seedMeta = jb_season.optString("seed_metadata");
                                String crop_status = jb_season.optString("crop_status");
                                String season_feedback = jb_season.optString("season_feedback");
                                String season_end_crop_week = jb_season.optString("season_end_crop_week");
                                if (TextUtils.isEmpty(season_end_crop_week)) {
                                    season_end_crop_week = "0";
                                }

                                List<CropWeekDetails> cropWeekDetailsList = new ArrayList<>();
                                try {
                                    JSONArray cropWeekDetails = jb_season.getJSONArray("crop_week_detail");
                                    if (cropWeekDetails != null && cropWeekDetails.length() > 0) {
                                        for (int j = 0; j < cropWeekDetails.length(); j++) {
                                            JSONObject jb = cropWeekDetails.optJSONObject(j);
                                            int crop_week = jb.optInt("crop_week");
                                            String stage_id = jb.optString("stage_id");
                                            String stage_name = jb.optString("stage_name");
                                            String display_name = jb.optString("display_name");
                                            String stage_start_week = jb.optString("stage_start_week");
                                            String stage_end_week = jb.optString("stage_end_week");
                                            int is_new_stage = jb.optInt("is_new_stage");
                                            cropWeekDetailsList.add(new CropWeekDetails(crop_week, stage_id, stage_name, display_name, stage_start_week, stage_end_week, is_new_stage));
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                cropsDataList.add(new CropsData(field_id, area_unit, geo_latitude, geo_longitude, geo_metadata, field_size, p_crop_id,
                                        is_sowing_done, expected_date_of_sowing, date_of_sowing, seed_type, crop_in_days, crop_in_weeks, crop_id,
                                        cropWeekDetailsList, seedMeta, crop_status, season_feedback, season_end_crop_week,
                                        RongoApp.getDefaultLanguage()));

                                if (i == 0) {
                                    if (TextUtils.isEmpty(RongoApp.getSelectedCrop())) {
                                        SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                                                Constants.SharedPreference_Sowing_Done, is_sowing_done);
                                        SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                                                Constants.SharedPreference_SowingDate, date_of_sowing);
                                        SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                                                Constants.SharedPreferences_CROP_ID, crop_id);
                                        SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0),
                                                Constants.SharedPreferences_P_CROP_ID, p_crop_id);
                                    }
                                }
                            }
                        }
                        seasonData = new SeasonData(season_id, season_start_date, season_end_date, cropsDataList);
                    }

                    ProfileResultData profileResultData = new ProfileResultData(last_survey_id, invite_people_count, display_content_expiry, userProfileData, seasonData, view_pending_count, api_content_expiry, api_profile_expiry);
                    SharedPreferencesUtils.storeStateOfString(RongoApp.getInstance().getSharedPreferences(Constants.MediaPrefs, 0), Constants.SharedPreference_profileResultData, (new Gson()).toJson(profileResultData));

                    return profileResultData;
                }
            }
        }
        return null;
    }
}
