package com.rongoapp.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class OfflineGrowthData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int _id;

    private String season_id;
    private String p_crop_id;
    private String is_sowing_done;
    private String date_of_sowing;
    private String expected_date_of_sowing;
    private String seedKey;
    private String seed_type_name;
    private String hybrid_number;
    private String shop_owner_name;
    private String shop_owner_phone;
    private String shop_owner_location;
    private String imagePath;

    public OfflineGrowthData(int _id, String season_id, String p_crop_id, String is_sowing_done, String date_of_sowing, String expected_date_of_sowing, String seedKey, String seed_type_name, String hybrid_number, String shop_owner_name, String shop_owner_phone, String shop_owner_location, String imagePath) {
        this._id = _id;
        this.season_id = season_id;
        this.p_crop_id = p_crop_id;
        this.is_sowing_done = is_sowing_done;
        this.date_of_sowing = date_of_sowing;
        this.expected_date_of_sowing = expected_date_of_sowing;
        this.seedKey = seedKey;
        this.seed_type_name = seed_type_name;
        this.hybrid_number = hybrid_number;
        this.shop_owner_name = shop_owner_name;
        this.shop_owner_phone = shop_owner_phone;
        this.shop_owner_location = shop_owner_location;
        this.imagePath = imagePath;
    }

    public int get_id() {
        return _id;
    }

    public String getSeason_id() {
        return season_id;
    }

    public String getP_crop_id() {
        return p_crop_id;
    }

    public String getIs_sowing_done() {
        return is_sowing_done;
    }

    public String getDate_of_sowing() {
        return date_of_sowing;
    }

    public String getExpected_date_of_sowing() {
        return expected_date_of_sowing;
    }

    public String getSeedKey() {
        return seedKey;
    }

    public String getSeed_type_name() {
        return seed_type_name;
    }

    public String getHybrid_number() {
        return hybrid_number;
    }

    public String getShop_owner_name() {
        return shop_owner_name;
    }

    public String getShop_owner_phone() {
        return shop_owner_phone;
    }

    public String getShop_owner_location() {
        return shop_owner_location;
    }

    public String getImagePath() {
        return imagePath;
    }
}
