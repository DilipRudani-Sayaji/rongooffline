package com.rongoapp.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class AddCropStatusData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int ids;
    private String cropId;
    private String pcropIds;
    private String sessionId;
    private String fieldId;
    private String sowingDate;
    private String sowingDone;
    private String sowingExpDate;
    private String sessionEnd;
    private String sessionEndTotal;
    private String status;

    public AddCropStatusData() {
    }

    public AddCropStatusData(int ids, String cropId, String pcropIds, String sessionId, String fieldId, String sowingDate, String sowingDone, String sowingExpDate, String sessionEnd, String sessionEndTotal, String status) {
        this.ids = ids;
        this.cropId = cropId;
        this.pcropIds = pcropIds;
        this.sessionId = sessionId;
        this.fieldId = fieldId;
        this.sowingDate = sowingDate;
        this.sowingDone = sowingDone;
        this.sowingExpDate = sowingExpDate;
        this.sessionEnd = sessionEnd;
        this.sessionEndTotal = sessionEndTotal;
        this.status = status;
    }

    public int getIds() {
        return ids;
    }

    public void setIds(int ids) {
        this.ids = ids;
    }

    public String getCropId() {
        return cropId;
    }

    public void setCropId(String cropId) {
        this.cropId = cropId;
    }

    public String getPcropIds() {
        return pcropIds;
    }

    public void setPcropIds(String pcropIds) {
        this.pcropIds = pcropIds;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public String getSowingDate() {
        return sowingDate;
    }

    public void setSowingDate(String sowingDate) {
        this.sowingDate = sowingDate;
    }

    public String getSowingDone() {
        return sowingDone;
    }

    public void setSowingDone(String sowingDone) {
        this.sowingDone = sowingDone;
    }

    public String getSowingExpDate() {
        return sowingExpDate;
    }

    public void setSowingExpDate(String sowingExpDate) {
        this.sowingExpDate = sowingExpDate;
    }

    public String getSessionEnd() {
        return sessionEnd;
    }

    public void setSessionEnd(String sessionEnd) {
        this.sessionEnd = sessionEnd;
    }

    public String getSessionEndTotal() {
        return sessionEndTotal;
    }

    public void setSessionEndTotal(String sessionEndTotal) {
        this.sessionEndTotal = sessionEndTotal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
