package com.rongoapp.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.rongoapp.controller.ListConverters;

import java.io.Serializable;
import java.util.ArrayList;

@Entity
public class OfflineData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int _id;
    private String keyWord;
    private String problemRelated;
    private String picture_area;
    private String description;
    private String latitude;
    private String longitude;
    private String created_on;
    private String crop_age;
    private String season_id;
    private String crop_id;
    private String p_crop_id;
    private String display_name;
    private String is_viewd;
    private String recording_local_path;

    @TypeConverters(ListConverters.class)
    private ArrayList<String> imagesPath;

    public OfflineData() {
    }

    public OfflineData(int _id, String keyWord, String problemRelated, String picture_area, String description, String latitude,
                       String longitude, String created_on, String crop_age,String season_id,String crop_id,String p_crop_id,
                       String display_name, String is_viewd, String recording_local_path, ArrayList<String> imagesPath) {
        this._id = _id;
        this.keyWord = keyWord;
        this.problemRelated = problemRelated;
        this.picture_area = picture_area;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
        this.created_on = created_on;
        this.crop_age = crop_age;
        this.season_id = season_id;
        this.crop_id = crop_id;
        this.p_crop_id = p_crop_id;
        this.display_name = display_name;
        this.is_viewd = is_viewd;
        this.imagesPath = imagesPath;
        this.recording_local_path = recording_local_path;
    }

    public String getRecording_local_path() {
        return recording_local_path;
    }

    public void setRecording_local_path(String recording_local_path) {
        this.recording_local_path = recording_local_path;
    }

    public String getSeason_id() {
        return season_id;
    }

    public void setSeason_id(String season_id) {
        this.season_id = season_id;
    }

    public String getCrop_id() {
        return crop_id;
    }

    public void setCrop_id(String crop_id) {
        this.crop_id = crop_id;
    }

    public String getP_crop_id() {
        return p_crop_id;
    }

    public void setP_crop_id(String p_crop_id) {
        this.p_crop_id = p_crop_id;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public String getProblemRelated() {
        return problemRelated;
    }

    public void setProblemRelated(String problemRelated) {
        this.problemRelated = problemRelated;
    }

    public String getPicture_area() {
        return picture_area;
    }

    public void setPicture_area(String picture_area) {
        this.picture_area = picture_area;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getCrop_age() {
        return crop_age;
    }

    public void setCrop_age(String crop_age) {
        this.crop_age = crop_age;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getIs_viewd() {
        return is_viewd;
    }

    public void setIs_viewd(String is_viewd) {
        this.is_viewd = is_viewd;
    }

    public ArrayList<String> getImagesPath() {
        return imagesPath;
    }

    public void setImagesPath(ArrayList<String> imagesPath) {
        this.imagesPath = imagesPath;
    }
}
