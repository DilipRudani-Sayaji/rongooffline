package com.rongoapp.data;

import java.io.Serializable;

public class FeedBackSubmitModel implements Serializable {

    private String id;
    private String text;
    private String status;
    private String type1;
    private String type2;
    private String type3;

    public FeedBackSubmitModel() {
    }

    public FeedBackSubmitModel(String id, String text, String status, String type1, String type2, String type3) {
        this.id = id;
        this.text = text;
        this.status = status;
        this.type1 = type1;
        this.type2 = type2;
        this.type3 = type3;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType1() {
        return type1;
    }

    public void setType1(String type1) {
        this.type1 = type1;
    }

    public String getType2() {
        return type2;
    }

    public void setType2(String type2) {
        this.type2 = type2;
    }

    public String getType3() {
        return type3;
    }

    public void setType3(String type3) {
        this.type3 = type3;
    }
}
