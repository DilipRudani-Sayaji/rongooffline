package com.rongoapp.data;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class ProfileOfflineData {

    @PrimaryKey
    private int uid;

    @ColumnInfo(name = "first_name")
    private String firstName;

    @ColumnInfo(name = "last_name")
    private String lastName;

    @ColumnInfo(name = "phone_number")
    private String phoneNumber;

    @ColumnInfo(name = "status")
    private String status;

    @ColumnInfo(name = "city")
    private String city;

    @ColumnInfo(name = "state")
    private String state;

    @ColumnInfo(name = "country")
    private String country;

    @ColumnInfo(name = "field_id")
    private String fieldId;

    @ColumnInfo(name = "field_size")
    private String fieldSize;

    @ColumnInfo(name = "p_crop_id")
    private String p_crop_id;

    @ColumnInfo(name = "crop_id")
    private String crop_id;

    @ColumnInfo(name = "is_sowing_done")
    private String is_sowing_done;

    @ColumnInfo(name = "sowing_date")
    private String sowingDate;

    @ColumnInfo(name = "expected_sowing_date")
    private String expectedSowingDate;

    @ColumnInfo(name = "seed_type")
    private String seedType;

    private String area_unit;
    private String crop_status;
    private String season_feedback;
    private String season_end_crop_week;
    private String app_language;

    public String getApp_language() {
        return app_language;
    }

    public void setApp_language(String app_language) {
        this.app_language = app_language;
    }

    public String getArea_unit() {
        return area_unit;
    }

    public void setArea_unit(String area_unit) {
        this.area_unit = area_unit;
    }

    public String getCrop_status() {
        return crop_status;
    }

    public void setCrop_status(String crop_status) {
        this.crop_status = crop_status;
    }

    public String getSeason_feedback() {
        return season_feedback;
    }

    public void setSeason_feedback(String season_feedback) {
        this.season_feedback = season_feedback;
    }

    public String getSeason_end_crop_week() {
        return season_end_crop_week;
    }

    public void setSeason_end_crop_week(String season_end_crop_week) {
        this.season_end_crop_week = season_end_crop_week;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCrop_id() {
        return crop_id;
    }

    public void setCrop_id(String crop_id) {
        this.crop_id = crop_id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFieldId() {
        return fieldId;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public String getFieldSize() {
        return fieldSize;
    }

    public void setFieldSize(String fieldSize) {
        this.fieldSize = fieldSize;
    }

    public String getP_crop_id() {
        return p_crop_id;
    }

    public void setP_crop_id(String p_crop_id) {
        this.p_crop_id = p_crop_id;
    }

    public String getIs_sowing_done() {
        return is_sowing_done;
    }

    public void setIs_sowing_done(String is_sowing_done) {
        this.is_sowing_done = is_sowing_done;
    }

    public String getSowingDate() {
        return sowingDate;
    }

    public void setSowingDate(String sowingDate) {
        this.sowingDate = sowingDate;
    }

    public String getExpectedSowingDate() {
        return expectedSowingDate;
    }

    public void setExpectedSowingDate(String expectedSowingDate) {
        this.expectedSowingDate = expectedSowingDate;
    }

    public String getSeedType() {
        return seedType;
    }

    public void setSeedType(String seedType) {
        this.seedType = seedType;
    }
}