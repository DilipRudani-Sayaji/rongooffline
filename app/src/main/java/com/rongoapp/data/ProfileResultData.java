package com.rongoapp.data;

import java.io.Serializable;

public class ProfileResultData implements Serializable {

    private String last_survey_id = "0";
    private String invite_people_count;
    private String display_content_expiry;
    private String api_content_expiry;
    private String api_profile_expiry;

    private UserProfileData profile;
    private SeasonData season;
    int view_pending_count;

    public ProfileResultData() {
    }

    public ProfileResultData(String last_survey_id, String invite_people_count, String display_content_expiry,
                             UserProfileData profile, SeasonData season, int view_pending_count, String api_content_expiry, String api_profile_expiry) {
        this.last_survey_id = last_survey_id;
        this.invite_people_count = invite_people_count;
        this.display_content_expiry = display_content_expiry;
        this.profile = profile;
        this.season = season;
        this.view_pending_count = view_pending_count;
        this.api_content_expiry = api_content_expiry;
        this.api_profile_expiry = api_profile_expiry;
    }


    public void setLast_survey_id(String last_survey_id) {
        this.last_survey_id = last_survey_id;
    }

    public void setInvite_people_count(String invite_people_count) {
        this.invite_people_count = invite_people_count;
    }

    public void setDisplay_content_expiry(String display_content_expiry) {
        this.display_content_expiry = display_content_expiry;
    }

    public void setApi_content_expiry(String api_content_expiry) {
        this.api_content_expiry = api_content_expiry;
    }

    public void setApi_profile_expiry(String api_profile_expiry) {
        this.api_profile_expiry = api_profile_expiry;
    }

    public void setUserProfileData(UserProfileData profile) {
        this.profile = profile;
    }

    public void setSeasonData(SeasonData season) {
        this.season = season;
    }

    public void setView_pending_count(int view_pending_count) {
        this.view_pending_count = view_pending_count;
    }

    public String getApi_profile_expiry() {
        return api_profile_expiry;
    }

    public String getApi_content_expiry() {
        return api_content_expiry;
    }

    public int getView_pending_count() {
        return view_pending_count;
    }

    public String getLast_survey_id() {
        return last_survey_id;
    }

    public String getInvite_people_count() {
        return invite_people_count;
    }

    public String getDisplay_content_expiry() {
        return display_content_expiry;
    }

    public UserProfileData getUserProfileData() {
        return profile;
    }

    public SeasonData getSeasonData() {
        return season;
    }
}
