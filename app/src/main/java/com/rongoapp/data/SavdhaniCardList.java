package com.rongoapp.data;

import java.io.Serializable;

public class SavdhaniCardList implements Serializable {

    String name;
    String keyword_id;
    String status;
    String type;
    String image;
    String text;

    public SavdhaniCardList() {
    }

    public SavdhaniCardList(String name, String keyword_id, String status, String type, String image, String text) {
        this.name = name;
        this.keyword_id = keyword_id;
        this.status = status;
        this.type = type;
        this.image = image;
        this.text = text;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKeyword_id() {
        return keyword_id;
    }

    public void setKeyword_id(String keyword_id) {
        this.keyword_id = keyword_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
