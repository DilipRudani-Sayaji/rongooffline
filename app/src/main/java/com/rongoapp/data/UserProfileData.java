package com.rongoapp.data;

import java.io.Serializable;

public class UserProfileData implements Serializable {

    private String user_id;
    private String first_name;
    private String last_name;
    private String phone_number;
    private String status;
    private String preference;
    private String location_city;
    private String location_state;
    private String location_area_city;
    private String location_area_state;
    private String location_area;

    public UserProfileData(String user_id, String first_name, String last_name, String phone_number, String status, String preference, String location_city,
                           String location_state, String location_area_city, String location_area_state, String location_area) {
        this.user_id = user_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.phone_number = phone_number;
        this.status = status;
        this.preference = preference;
        this.location_city = location_city;
        this.location_state = location_state;
        this.location_area_city = location_area_city;
        this.location_area_state = location_area_state;
        this.location_area = location_area;
    }


    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setPreference(String preference) {
        this.preference = preference;
    }

    public void setLocation_city(String location_city) {
        this.location_city = location_city;
    }

    public void setLocation_state(String location_state) {
        this.location_state = location_state;
    }

    public String getLocation_area_city() {
        return location_area_city;
    }

    public void setLocation_area_city(String location_area_city) {
        this.location_area_city = location_area_city;
    }

    public String getLocation_area_state() {
        return location_area_state;
    }

    public void setLocation_area_state(String location_area_state) {
        this.location_area_state = location_area_state;
    }

    public String getLocation_area() {
        return location_area;
    }

    public void setLocation_area(String location_area) {
        this.location_area = location_area;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public String getStatus() {
        return status;
    }

    public String getPreference() {
        return preference;
    }

    public String getLocation_city() {
        return location_city;
    }

    public String getLocation_state() {
        return location_state;
    }
}
