package com.rongoapp.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class JankariFeedBackData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public int _id;
    String typeURL;
    String userResponse;
    String p_crop_id;
    String keyword_id;
    String post_id;
    String is_applied;

    public JankariFeedBackData(int _id, String typeURL, String userResponse, String p_crop_id, String keyword_id,
                               String post_id, String is_applied) {
        this._id = _id;
        this.typeURL = typeURL;
        this.userResponse = userResponse;
        this.p_crop_id = p_crop_id;
        this.keyword_id = keyword_id;
        this.post_id = post_id;
        this.is_applied = is_applied;
    }

    public String getIs_applied() {
        return is_applied;
    }

    public void setIs_applied(String is_applied) {
        this.is_applied = is_applied;
    }

    public String getP_crop_id() {
        return p_crop_id;
    }

    public String getKeyword_id() {
        return keyword_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public int get_id() {
        return _id;
    }

    public String getTypeURL() {
        return typeURL;
    }

    public String getUserResponse() {
        return userResponse;
    }

}
