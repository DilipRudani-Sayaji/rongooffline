package com.rongoapp.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class OfflineRewardData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int _id;
    private String contestID;
    private String rewardID;
    private String type;
    private String value;
    private String text;
    private String is_true;

    public OfflineRewardData() {
    }


    public String getRewardID() {
        return rewardID;
    }

    public void setRewardID(String rewardID) {
        this.rewardID = rewardID;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getContestID() {
        return contestID;
    }

    public void setContestID(String contestID) {
        this.contestID = contestID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getIs_true() {
        return is_true;
    }

    public void setIs_true(String is_true) {
        this.is_true = is_true;
    }
}
