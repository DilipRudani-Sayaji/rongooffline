package com.rongoapp.data;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import java.io.Serializable;
import java.util.List;

public class SavdhaniDataList implements Serializable {

    String name;
    String keyword_id;
    String status;
    String type;
    String bitmap;
    List<SavdhaniCardList> savdhaniCardLists;

    public SavdhaniDataList() {
    }

    public SavdhaniDataList(String name, String keyword_id, String status, String type, String bitmap, List<SavdhaniCardList> savdhaniCardLists) {
        this.name = name;
        this.keyword_id = keyword_id;
        this.status = status;
        this.type = type;
        this.bitmap = bitmap;
        this.savdhaniCardLists = savdhaniCardLists;
    }

    public String getBitmap() {
        return bitmap;
    }

    public void setBitmap(String bitmap) {
        this.bitmap = bitmap;
    }

    public List<SavdhaniCardList> getSavdhaniCardLists() {
        return savdhaniCardLists;
    }

    public void setSavdhaniCardLists(List<SavdhaniCardList> savdhaniCardLists) {
        this.savdhaniCardLists = savdhaniCardLists;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKeyword_id() {
        return keyword_id;
    }

    public void setKeyword_id(String keyword_id) {
        this.keyword_id = keyword_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
