package com.rongoapp.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class PostData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int _id;
    private String post_id;
    private String keyword;
    private String problem_related;
    private String picture_key;
    private String description;
    private String name;
    private String likes;
    private String views;
    private String vote_type;
    private String is_commented;
    private String display_name;
    private String crop_age;
    private String created_on;
    private String is_viewd;
    private String comments;
    private String user_id;
    private String recording_key = "";
    private String recording_local_path;
    private String posted_user_name;
    private String profile_picture;
    private String posted_user_location;
    private String post_type;
    private String share_link;
    private String is_expert;
    private String is_community_visible;
    String is_applied;

    public PostData() {
    }

    public PostData(int _id, String post_id, String keyword, String problem_related, String picture_key, String description, String name, String likes,
                    String views, String vote_type, String is_commented, String display_name, String crop_age, String created_on, String is_viewd,
                    String comments, String user_id, String recording_key, String recording_local_path, String posted_user_name, String profile_picture,
                    String posted_user_location, String post_type, String share_link, String is_expert, String is_community_visible, String is_applied) {
        this._id = _id;
        this.post_id = post_id;
        this.keyword = keyword;
        this.problem_related = problem_related;
        this.picture_key = picture_key;
        this.description = description;
        this.name = name;
        this.likes = likes;
        this.views = views;
        this.vote_type = vote_type;
        this.is_commented = is_commented;
        this.display_name = display_name;
        this.crop_age = crop_age;
        this.created_on = created_on;
        this.is_viewd = is_viewd;
        this.comments = comments;
        this.user_id = user_id;
        this.recording_key = recording_key;
        this.recording_local_path = recording_local_path;
        this.posted_user_name = posted_user_name;
        this.profile_picture = profile_picture;
        this.posted_user_location = posted_user_location;
        this.post_type = post_type;
        this.share_link = share_link;
        this.is_expert = is_expert;
        this.is_community_visible = is_community_visible;
        this.is_applied = is_applied;
    }

    public String getIs_applied() {
        return is_applied;
    }

    public void setIs_applied(String is_applied) {
        this.is_applied = is_applied;
    }

    public String getIs_community_visible() {
        return is_community_visible;
    }

    public void setIs_community_visible(String is_community_visible) {
        this.is_community_visible = is_community_visible;
    }

    public String getIs_expert() {
        return is_expert;
    }

    public void setIs_expert(String is_expert) {
        this.is_expert = is_expert;
    }

    public String getPost_type() {
        return post_type;
    }

    public void setPost_type(String post_type) {
        this.post_type = post_type;
    }

    public String getShare_link() {
        return share_link;
    }

    public void setShare_link(String share_link) {
        this.share_link = share_link;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getProblem_related() {
        return problem_related;
    }

    public void setProblem_related(String problem_related) {
        this.problem_related = problem_related;
    }

    public String getPicture_key() {
        return picture_key;
    }

    public void setPicture_key(String picture_key) {
        this.picture_key = picture_key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getViews() {
        return views;
    }

    public void setViews(String views) {
        this.views = views;
    }

    public String getVote_type() {
        return vote_type;
    }

    public void setVote_type(String vote_type) {
        this.vote_type = vote_type;
    }

    public String getIs_commented() {
        return is_commented;
    }

    public void setIs_commented(String is_commented) {
        this.is_commented = is_commented;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getCrop_age() {
        return crop_age;
    }

    public void setCrop_age(String crop_age) {
        this.crop_age = crop_age;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getIs_viewd() {
        return is_viewd;
    }

    public void setIs_viewd(String is_viewd) {
        this.is_viewd = is_viewd;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRecording_key() {
        return recording_key;
    }

    public void setRecording_key(String recording_key) {
        this.recording_key = recording_key;
    }

    public String getRecording_local_path() {
        return recording_local_path;
    }

    public void setRecording_local_path(String recording_local_path) {
        this.recording_local_path = recording_local_path;
    }

    public String getPosted_user_name() {
        return posted_user_name;
    }

    public void setPosted_user_name(String posted_user_name) {
        this.posted_user_name = posted_user_name;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getPosted_user_location() {
        return posted_user_location;
    }

    public void setPosted_user_location(String posted_user_location) {
        this.posted_user_location = posted_user_location;
    }

}
