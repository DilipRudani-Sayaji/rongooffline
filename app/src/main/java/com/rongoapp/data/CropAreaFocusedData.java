package com.rongoapp.data;

import java.io.Serializable;
import java.util.Objects;

public class CropAreaFocusedData implements Serializable {

    private String type;
    private String text;
    private String description;
    private String image;

    public CropAreaFocusedData() {
    }

    public CropAreaFocusedData(String type, String text, String description, String image) {
        this.type = type;
        this.text = text;
        this.description = description;
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public String getType() {
        return type;
    }

    public String getText() {
        return text;
    }

    public String getDescription() {
        return description;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CropAreaFocusedData)) return false;
        CropAreaFocusedData that = (CropAreaFocusedData) o;
        return Objects.equals(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type);
    }
}
