package com.rongoapp.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class OfflineLikeData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int _id;
    private String post_id;
    private String type;
    private String comment_id;
    private String datetime;

    public OfflineLikeData() {
    }

    public OfflineLikeData(int _id, String post_id, String type, String comment_id, String datetime) {
        this._id = _id;
        this.post_id = post_id;
        this.type = type;
        this.comment_id = comment_id;
        this.datetime = datetime;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getComment_id() {
        return comment_id;
    }

    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}
