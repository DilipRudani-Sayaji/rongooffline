package com.rongoapp.data;

import android.text.TextUtils;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class WeeklyAdvisoryData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int _id;
    private int cropWeek;
    private String cropId;
    private String pcropId;
    private String stageName;
    private String image_path;
    String generalAdvisory;
    String advisoryImages;
    String chemicalNeeds;
    String diasesData;
    String pestData;
    String nutritionData;
    String keyword_advisory;

    public WeeklyAdvisoryData(int _id, int cropWeek, String cropId, String pcropId, String stageName, String image_path, String generalAdvisory, String advisoryImages, String chemicalNeeds, String diasesData, String pestData, String nutritionData, String keyword_advisory) {
        this._id = _id;
        this.cropWeek = cropWeek;
        this.cropId = cropId;
        this.pcropId = pcropId;
        this.stageName = stageName;
        this.image_path = image_path;
        this.generalAdvisory = generalAdvisory;
        this.advisoryImages = advisoryImages;
        this.chemicalNeeds = chemicalNeeds;
        this.diasesData = diasesData;
        this.pestData = pestData;
        this.nutritionData = nutritionData;
        this.keyword_advisory = keyword_advisory;
    }

    public String getCropId() {
        return cropId;
    }

    public void setCropId(String cropId) {
        this.cropId = cropId;
    }

    public String getPcropId() {
        return pcropId;
    }

    public void setPcropId(String pcropId) {
        this.pcropId = pcropId;
    }

    public int get_id() {
        return _id;
    }

    public int getCropWeek() {
        return cropWeek;
    }

    public String getStageName() {
        return stageName;
    }

    public String getImage_path() {
        return image_path;
    }

    public String getGeneralAdvisory() {
        return generalAdvisory;
    }

    public String getAdvisoryImages() {
        return advisoryImages;
    }

    public String getChemicalNeeds() {
        return chemicalNeeds;
    }

    public String getDiasesData() {
        return diasesData;
    }

    public String getPestData() {
        return pestData;
    }

    public String getNutritionData() {
        return nutritionData;
    }

    public String getKeyword_advisory() {
        return keyword_advisory;
    }


    public List<String> getGeneralAdvisoryList() {
        List<String> list = new ArrayList<>();
        JSONArray jsonArray;
        try {
            if (TextUtils.isEmpty(getGeneralAdvisory())) {
                return list;
            } else {
                jsonArray = new JSONArray(getGeneralAdvisory());
                if (jsonArray != null && jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        list.add(jsonArray.getString(i));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }


    public List<String> getAdvisoryImagesList() {
        List<String> list = new ArrayList<>();
        JSONArray jsonArray = new JSONArray();
        try {
            if (TextUtils.isEmpty(getAdvisoryImages())) {
                return list;
            } else {
                jsonArray = new JSONArray(getAdvisoryImages());
                if (jsonArray != null && jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        list.add(jsonArray.optString(i));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<String> getChemocalList() {
        List<String> list = new ArrayList<>();
        JSONArray jsonArray;
        try {
            if (!TextUtils.isEmpty(getChemicalNeeds())) {
                jsonArray = new JSONArray(getChemicalNeeds());
                if (jsonArray != null && jsonArray.length() > 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        list.add(jsonArray.getString(i));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<DiasesData> getDiasesDataList() {
        List<DiasesData> list = new ArrayList<>();
        JSONArray disease = null;
        try {
            if (!TextUtils.isEmpty(getDiasesData())) {
                disease = new JSONArray(getDiasesData());
                if (disease != null && disease.length() > 0) {
                    for (int i = 0; i < disease.length(); i++) {
                        JSONObject jd = disease.optJSONObject(i);
                        String name = jd.optString("name");
                        String keyword_id = jd.optString("keyword_id");
                        list.add(new DiasesData(name, keyword_id));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<PestData> getPestDataList() {
        List<PestData> list = new ArrayList<>();
        JSONArray pest = null;
        try {
            if (!TextUtils.isEmpty(getPestData())) {
                pest = new JSONArray(getPestData());
                if (pest != null && pest.length() > 0) {
                    for (int i = 0; i < pest.length(); i++) {
                        JSONObject jd = pest.optJSONObject(i);
                        String name = jd.optString("name");
                        String keyword_id = jd.optString("keyword_id");
                        list.add(new PestData(name, keyword_id));
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

    public List<NutritionData> getNutretionList() {
        List<NutritionData> list = new ArrayList<>();
        JSONArray nutrition = new JSONArray();
        try {
            if (!TextUtils.isEmpty(getNutritionData())) {
                nutrition = new JSONArray(getNutritionData());
                if (nutrition != null && nutrition.length() > 0) {
                    for (int i = 0; i < nutrition.length(); i++) {
                        JSONObject jd = nutrition.optJSONObject(i);
                        String name = jd.optString("name");
                        String keyword_id = jd.optString("keyword_id");
                        list.add(new NutritionData(name, keyword_id));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }
}
