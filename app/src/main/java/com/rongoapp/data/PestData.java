package com.rongoapp.data;

import java.io.Serializable;

public class PestData implements Serializable {

    String name;
    String keyword_id;

    public PestData() {
    }

    public PestData(String name, String keyword_id) {
        this.name = name;
        this.keyword_id = keyword_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKeyword_id() {
        return keyword_id;
    }

    public void setKeyword_id(String keyword_id) {
        this.keyword_id = keyword_id;
    }
}
