package com.rongoapp.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class PhsalFeedback implements Serializable {

    @PrimaryKey (autoGenerate = true)
    int _id;
    String givenFeedback;
    int cropWeek;

    public PhsalFeedback(int _id, String givenFeedback, int cropWeek) {
        this._id = _id;
        this.givenFeedback = givenFeedback;
        this.cropWeek = cropWeek;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getGivenFeedback() {
        return givenFeedback;
    }

    public void setGivenFeedback(String givenFeedback) {
        this.givenFeedback = givenFeedback;
    }

    public int getCropWeek() {
        return cropWeek;
    }

    public void setCropWeek(int cropWeek) {
        this.cropWeek = cropWeek;
    }
}
