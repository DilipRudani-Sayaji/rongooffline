package com.rongoapp.data;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class AdvisoryPhotoData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int _id;
    private String cropWeek;
    private String cropId;
    private String pcropId;
    private String isPhoto;

    public AdvisoryPhotoData(int _id, String cropWeek, String cropId, String pcropId, String isPhoto) {
        this._id = _id;
        this.cropWeek = cropWeek;
        this.cropId = cropId;
        this.pcropId = pcropId;
        this.isPhoto = isPhoto;
    }

    public String getCropId() {
        return cropId;
    }

    public void setCropId(String cropId) {
        this.cropId = cropId;
    }

    public String getPcropId() {
        return pcropId;
    }

    public void setPcropId(String pcropId) {
        this.pcropId = pcropId;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getCropWeek() {
        return cropWeek;
    }

    public void setCropWeek(String cropWeek) {
        this.cropWeek = cropWeek;
    }

    public String getIsPhoto() {
        return isPhoto;
    }

    public void setIsPhoto(String isPhoto) {
        this.isPhoto = isPhoto;
    }

}
