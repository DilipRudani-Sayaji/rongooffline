package com.rongoapp.otp;

public interface OnOtpCompletionListener {

    void onOtpCompleted(String otp);

}
