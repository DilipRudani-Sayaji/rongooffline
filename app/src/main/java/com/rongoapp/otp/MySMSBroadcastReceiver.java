package com.rongoapp.otp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MySMSBroadcastReceiver extends BroadcastReceiver {

    public static Listener listener;

    public static void bindListener(Listener mlistener) {
        listener = mlistener;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
//        Log.v("SMSOTP", "OTP");
        if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
            Bundle extras = intent.getExtras();
            Status status = (Status) extras.get(SmsRetriever.EXTRA_STATUS);
//            Log.v("SMSOTP", status.getStatus() + " " + status.getStatusMessage() + " " + status.getStatusCode());

            switch (status.getStatusCode()) {
                case CommonStatusCodes.SUCCESS:

                    String message = (String) extras.get(SmsRetriever.EXTRA_SMS_MESSAGE);
//                    Log.v("SMSOTP", message);
                    Pattern pattern = Pattern.compile("\\d{4}");
                    Matcher matcher = pattern.matcher(message);
                    if (matcher.find()) {
                        message = matcher.group(0);
//                        Log.v("Matched", message);
                    }

                    if (listener != null) {
                        listener.onSMSReceived(message);
                    } else {
                        new IllegalArgumentException("Must pass a listener");
                    }
                    break;
                case CommonStatusCodes.TIMEOUT:
//                    Log.v("TIMEOUTMESASA", "Y");
                    if (listener != null) {
                        listener.onTimeOut();
                    }
                    break;
            }
        }
    }

    public interface Listener {
        void onSMSReceived(String otp);

        void onTimeOut();
    }

}
