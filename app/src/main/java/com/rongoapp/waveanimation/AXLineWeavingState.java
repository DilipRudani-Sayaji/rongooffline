package com.rongoapp.waveanimation;

import android.graphics.Shader;

public abstract class AXLineWeavingState extends AXWeavingState {

    public AXLineWeavingState(int state) {
        super(state);
    }

    protected void updateTargets() {
        targetX = 1.1f + 0.2f * (random.nextInt(100) / 100f);
        targetY = 4f * random.nextInt(100) / 100f;
    }

    public void setSize (float left,float top,float right,float bottom){
        this.width = (int) (bottom - top);
        this.height = (int) (right - left);
    }

    public static AXLineWeavingState create(final int state, final Shader weavingShader, final float weavingScale){
        return new AXLineWeavingState(state) {

            @Override
            protected void init() {
                super.init();
                fixedScale = weavingScale;
            }

            @Override
            public Shader createShader() {
                return weavingShader;
            }
        };
    }

    public static AXLineWeavingState create(final int state,final Shader weavingShader){
        return new AXLineWeavingState(state) {

            @Override
            public Shader createShader() {
                return weavingShader;
            }
        };
    }

}
