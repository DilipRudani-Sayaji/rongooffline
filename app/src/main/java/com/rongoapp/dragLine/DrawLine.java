package com.rongoapp.dragLine;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.view.View;

import com.rongoapp.R;

public class DrawLine extends View {

    Paint mPaint = new Paint();
    float x, y, x1, y1;

    public DrawLine(Context context, float x, float y, float x1, float y1) {
        super(context);
        this.x = x;
        this.y = y;
        this.x1 = x1;
        this.y1 = y1;
        mPaint.setColor(Color.BLACK);
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(15);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setColor(context.getResources().getColor(R.color.color_red));
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            setLayerType(LAYER_TYPE_SOFTWARE, mPaint);
        }
    }

    @Override
    public void onDraw(Canvas canvas) {
        canvas.drawLine(x, y, x1, y1, mPaint);
//        canvas.drawLine(50, 100, 600, 500, mPaint);
//        canvas.drawLine(50, 300, 650, 700, mPaint);
//        canvas.drawLine(50, 550, 700, 0, paint);
//        canvas.drawLine(50, 650, 800, 0, paint);
    }

}  