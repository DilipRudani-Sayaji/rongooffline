package com.rongoapp.audioPlayerTTS;

import android.content.Context;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.RequiresApi;

import com.rongoapp.R;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.LocaleManager;
import com.rongoapp.controller.RongoApp;

import java.util.HashMap;
import java.util.Locale;

public class TTS extends UtteranceProgressListener implements TextToSpeech.OnInitListener {

    final String TAG = TTS.class.getSimpleName();
    TextToSpeech textToSpeech;
    Locale locale;
    Context context;
    ImageView imageView;
    ProgressBar progressBar;
    AudioPlayerListener audioPlayerListener;

    public interface AudioPlayerListener {
        void audioDone(String status);
    }

    public TTS(Context context, Locale _locale) {
        this.context = context;
        this.locale = _locale;
        textToSpeech = new TextToSpeech(context, this, Constants.GOOGLE_TTS_ENGINE);
        textToSpeech.setOnUtteranceProgressListener(this);
        textToSpeech.setEngineByPackageName(Constants.GOOGLE_TTS_ENGINE);
        textToSpeech.setLanguage(getLocaleLanguage());
//        Log.e("textToSpeech+2+", "" + textToSpeech.getVoice());
    }

    Locale getLocaleLanguage() {
        Locale locale = new Locale(Constants.Hindi_Language_code);
        if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.GUJARATI)) {
            locale = new Locale(Constants.Gujarati_Language_code);
        } else if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.MARATHI)) {
            locale = new Locale(Constants.Marathi_Language_code);
        } else {
            locale = new Locale(Constants.Hindi_Language_code);
        }
        return locale;
    }

    public void speak(String text, ImageView imageview1, ProgressBar progressBar1) {
        try {
            Log.e("textToSpeech+++", "" + textToSpeech.getVoice());
//            Log.e("textToSpeech+++", "" + text);
            if (textToSpeech == null) {
                setTTS();
            } else {
                if (textToSpeech.isSpeaking()) {
                    Log.e("textToSpeech+1+", "" + textToSpeech.getDefaultEngine());
                    imageView.setImageResource(R.mipmap.play);
                    progressBar.setVisibility(View.GONE);
                    textToSpeech.stop();
                } else {
//                    Log.e("textToSpeech+2+", "" + textToSpeech.getDefaultEngine());
                    String myUtteranceID = "myUtteranceID";
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        Bundle bundle = new Bundle();
                        bundle.putInt(TextToSpeech.Engine.EXTRA_VOICE_DATA_ROOT_DIRECTORY, 2);
                        bundle.putInt(TextToSpeech.Engine.EXTRA_VOICE_DATA_FILES_INFO, 2);
                        bundle.putInt(TextToSpeech.Engine.KEY_PARAM_STREAM, AudioManager.STREAM_MUSIC);
                        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, bundle, myUtteranceID);
                    } else {
                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, myUtteranceID);
                        hashMap.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_MUSIC));
                        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, hashMap);
                    }
                }
            }

            if (progressBar != null) {
                progressBar.setVisibility(View.GONE);
            }
            progressBar1.setVisibility(View.VISIBLE);

            this.imageView = imageview1;
            this.progressBar = progressBar1;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                }
            }, 2500);
        } catch (Exception e) {
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
        }
    }

    void setTTS() {
        textToSpeech = new TextToSpeech(context, this, Constants.GOOGLE_TTS_ENGINE);
        textToSpeech.setOnUtteranceProgressListener(this);
        textToSpeech.setEngineByPackageName(Constants.GOOGLE_TTS_ENGINE);
        textToSpeech.setLanguage(getLocaleLanguage());
    }

    public void speak(String text, ImageView imageview1, ProgressBar progressBar1, AudioPlayerListener audioPlayerListener) {
        try {
            Log.e("textToSpeech+++", "" + textToSpeech.getVoice() + "" + textToSpeech.getDefaultEngine());
            this.imageView = imageview1;
            this.progressBar = progressBar1;
            this.audioPlayerListener = audioPlayerListener;

            if (textToSpeech == null) {
                setTTS();
            } else {
                if (textToSpeech.isSpeaking()) {
                    imageView.setImageResource(R.mipmap.play);
                    textToSpeech.stop();
                    progressBar.setProgress(100);
                } else {
                    String myUtteranceID = "myUtteranceID";
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        Bundle bundle = new Bundle();
                        bundle.putInt(TextToSpeech.Engine.EXTRA_VOICE_DATA_ROOT_DIRECTORY, 2);
                        bundle.putInt(TextToSpeech.Engine.EXTRA_VOICE_DATA_FILES_INFO, 2);
                        bundle.putInt(TextToSpeech.Engine.KEY_PARAM_STREAM, AudioManager.STREAM_MUSIC);
                        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, bundle, myUtteranceID);
                    } else {
                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, myUtteranceID);
                        hashMap.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_MUSIC));
                        textToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, hashMap);
                    }

                    new CountDownTimer(40000, 1000) {
                        @RequiresApi(api = Build.VERSION_CODES.N)
                        @Override
                        public final void onTick(final long millisUntilFinished) {
                            progressBar.setProgress((int) (millisUntilFinished * .001f), true);
                        }

                        @Override
                        public final void onFinish() {
                            progressBar.setProgress(100);
                        }
                    }.start();
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setProgress(100);
                        progressBar.setVisibility(View.GONE);
                    }
                }, 1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
        }
    }

    public void stop() {
        textToSpeech.stop();
    }

    public void shutdown() {
        textToSpeech.shutdown();
    }

    public boolean isSpeaking() {
        return textToSpeech.isSpeaking();
    }

    @Override
    public void onStart(String utteranceId) {
        Log.d(TAG, "onStart / utteranceID = " + utteranceId);
        Log.d(TAG, "onStart = " + textToSpeech.getDefaultVoice() + " - " + textToSpeech.getDefaultLanguage());
        imageView.setImageResource(R.mipmap.pause);
    }

    @Override
    public void onDone(String utteranceId) {
        Log.d(TAG, "onDone / utteranceID = " + utteranceId);
        imageView.setImageResource(R.mipmap.play);
        progressBar.setProgress(100);
        if (audioPlayerListener != null) {
            audioPlayerListener.audioDone(utteranceId);
        }
    }

    @Override
    public void onError(String utteranceId) {
        Log.d(TAG, "onError / utteranceID = " + utteranceId);
        progressBar.setProgress(100);
    }

    @Override
    public void onStop(String utteranceId, boolean interrupted) {
        super.onStop(utteranceId, interrupted);
        progressBar.setProgress(100);
    }

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            textToSpeech.setLanguage(getLocaleLanguage());
        }
    }

}
