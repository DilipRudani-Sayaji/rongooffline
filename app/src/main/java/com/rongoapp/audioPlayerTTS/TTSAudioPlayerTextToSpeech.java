package com.rongoapp.audioPlayerTTS;

import android.Manifest;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.LocaleManager;
import com.rongoapp.controller.RongoApp;

import java.io.File;
import java.util.List;
import java.util.Locale;

public class TTSAudioPlayerTextToSpeech implements TextToSpeech.OnInitListener {

    TextToSpeech textToSpeech;
    boolean isLoading = false;
    String utteranceId = "", ttsStringContent = "", FILENAME = "";
    Listener listener;

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
        if (isLoading) {
            listener.loadingStarted(utteranceId);
        } else {
            listener.loadingCompleted(utteranceId);
        }
    }

    public interface Listener {
        void loadingStarted(String utteranceId);

        void loadingCompleted(String utteranceId);

        void loadingCompletedWithPermissionError(String utteranceId);
    }

    public TTSAudioPlayerTextToSpeech(String utteranceId, String ttsStringContent, Listener listener) {
        this.utteranceId = utteranceId;
        this.ttsStringContent = ttsStringContent;
        FILENAME = "/" + this.utteranceId + ".wav";

        textToSpeech = new TextToSpeech(RongoApp.getInstance().getApplicationContext(), this, Constants.GOOGLE_TTS_ENGINE);
        if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.GUJARATI)) {
            textToSpeech.setLanguage(new Locale(Constants.Gujarati_Language_code));
        } else if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.MARATHI)) {
            textToSpeech.setLanguage(new Locale(Constants.Marathi_Language_code));
        } else {
            textToSpeech.setLanguage(new Locale(Constants.Hindi_Language_code));
        }

        textToSpeech.setPitch(0.8f);
        textToSpeech.setSpeechRate(0.8f);

        this.listener = listener;
    }

    @Override
    public void onInit(int i) {
        if (i == TextToSpeech.SUCCESS) {
            accessPermisson();

            if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.GUJARATI)) {
                textToSpeech.setLanguage(new Locale(Constants.Gujarati_Language_code));
            } else if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.MARATHI)) {
                textToSpeech.setLanguage(new Locale(Constants.Marathi_Language_code));
            } else {
                textToSpeech.setLanguage(new Locale(Constants.Hindi_Language_code));
            }

//            textToSpeech.setLanguage(new Locale(Constants.Hindi_Language_code));
            textToSpeech.setPitch(0.8f);
            textToSpeech.setSpeechRate(0.8f);

//            Locale[] locales = Locale.getAvailableLocales();
//            List<Locale> localeList = new ArrayList<Locale>();
//            if (localeList != null) {
//                for (Locale locale : locales) {
//                    int res = textToSpeech.isLanguageAvailable(locale);
//                    if (res == TextToSpeech.LANG_COUNTRY_AVAILABLE) {
//                        localeList.add(locale);
//                    }
//                }
//            }
//            Log.e("Lang111: ", "" + localeList);

            int result = textToSpeech.setLanguage(new Locale(Constants.Hindi_Language_code));
            if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.GUJARATI)) {
                result = textToSpeech.setLanguage(new Locale(Constants.Gujarati_Language_code));
            } else if (RongoApp.getDefaultLanguage().equalsIgnoreCase(LocaleManager.MARATHI)) {
                result = textToSpeech.setLanguage(new Locale(Constants.Marathi_Language_code));
            } else {
                result = textToSpeech.setLanguage(new Locale(Constants.Hindi_Language_code));
            }

            if (result == TextToSpeech.LANG_NOT_SUPPORTED) {
                textToSpeech.setLanguage(new Locale(Constants.Hindi_Language_code));
                Toast.makeText(RongoApp.getInstance().getApplicationContext(), "tts does not support the Locale/language specified", Toast.LENGTH_SHORT).show();
            }
            if (result == TextToSpeech.LANG_MISSING_DATA) {
                textToSpeech.setLanguage(new Locale(Constants.Hindi_Language_code));
                Toast.makeText(RongoApp.getInstance().getApplicationContext(), "tts is missing the data required for the locale/language specified", Toast.LENGTH_SHORT).show();
            }

            setLoading(true);
        }
    }

    void accessPermisson() {
        if (Build.VERSION.SDK_INT >= 23) {
            Dexter.withContext(RongoApp.getInstance().getApplicationContext()).withPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE).withListener(new MultiplePermissionsListener() {
                @Override
                public void onPermissionsChecked(MultiplePermissionsReport report) {
                    if (report.areAllPermissionsGranted()) {
                        loadContentInTTS();
                    }
                    if (report.isAnyPermissionPermanentlyDenied() || report.getDeniedPermissionResponses().size() > 0) {
                        isLoading = false;
                        listener.loadingCompletedWithPermissionError(utteranceId);
                    }
                }

                @Override
                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                    token.continuePermissionRequest();
                }
            }).check();
        } else {
            loadContentInTTS();
        }
    }

    void loadContentInTTS() {
        try {
            final String fileName = Environment.getExternalStorageDirectory().getAbsolutePath() + FILENAME;
            File file = new File(fileName);
            if (file != null && file.exists()) {
                file.delete();
            }
            Bundle bundle = new Bundle();
            bundle.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, utteranceId);
            textToSpeech.synthesizeToFile(ttsStringContent, bundle, file, utteranceId);
            textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                @Override
                public void onDone(String utteranceId) {
                    Log.v("ISHETEE", "DONE" + utteranceId + " " + ttsStringContent);
                    setLoading(false);
                }

                @Override
                public void onError(String utteranceId) {
                    Log.v("ISHETEE", "NO" + utteranceId + " " + ttsStringContent);
                    setLoading(false);
                }

                @Override
                public void onStart(String utteranceId) {
                    Log.v("ISHETEE", "START" + utteranceId);
                }
            });

            if (!textToSpeech.isSpeaking()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    textToSpeech.speak(ttsStringContent, TextToSpeech.QUEUE_FLUSH, null, null);
                } else {
                    textToSpeech.speak(ttsStringContent, TextToSpeech.QUEUE_FLUSH, null);
                }
            } else {
                textToSpeech.stop();
                textToSpeech.shutdown();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}