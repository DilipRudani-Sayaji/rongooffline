package com.rongoapp.audioPlayerTTS;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.rongoapp.R;
import com.rongoapp.controller.RongoApp;

import java.util.HashMap;
import java.util.Random;

public class TTSAudioPlayer implements TTSAudioPlayerTextToSpeech.Listener {

    public static TTSAudioPlayer ttsAudioPlayer;
    MediaPlayer mediaPlayer;
    String currentTextToSpeech = "", currentMediaPlayerFile = "";
    HashMap<String, TTSContentHolder> textToSpeeches = new HashMap<>();

    public static synchronized TTSAudioPlayer getInstance() {
        if (ttsAudioPlayer == null) {
            ttsAudioPlayer = new TTSAudioPlayer();
        }
        return ttsAudioPlayer;
    }

    public static String createIdentifier() {
        return (new Random().nextInt() % 9999999) + "";
    }

    @Override
    public void loadingStarted(String utteranceId) {
        if (utteranceId.equalsIgnoreCase(currentTextToSpeech)) {
            showLoading(textToSpeeches.get(utteranceId));
        }
    }

    @Override
    public void loadingCompleted(String utteranceId) {
        if (utteranceId.equalsIgnoreCase("Welcome")) {
            currentTextToSpeech = "";
            Log.v("WelcomeText", "Yes");
            return;
        }
        if (utteranceId.equalsIgnoreCase(currentTextToSpeech)) {
            Log.v("MEDIA_NOW_PLAY_3", "STarting Noq");
            this.playNow();
        } else {
            Log.v("MEDIA_NOW_PLAY_3", "Not Playing Noq");
        }
    }

    @Override
    public void loadingCompletedWithPermissionError(String utteranceId) {
        showPause(textToSpeeches.get(currentTextToSpeech));
        textToSpeeches.remove(currentTextToSpeech);
        currentTextToSpeech = "";
    }

    private void showLoading(final TTSContentHolder ttsContentHolder) {
        Log.v("TTSLOG", "1");
        ttsContentHolder.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (currentTextToSpeech.equalsIgnoreCase("Welcome")) {
                    return;
                }
                ttsContentHolder.imageView.setVisibility(View.INVISIBLE);
                ttsContentHolder.progressBar.setVisibility(View.VISIBLE);
            }
        });
    }

    private void showPlay(final TTSContentHolder ttsContentHolder) {
        Log.v("TTSLOG", "2");
        ttsContentHolder.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (currentTextToSpeech.equalsIgnoreCase("Welcome")) {
                    return;
                }
                ttsContentHolder.imageView.setVisibility(View.VISIBLE);
                ttsContentHolder.progressBar.setVisibility(View.INVISIBLE);
                ttsContentHolder.imageView.setImageResource(R.mipmap.pause);
            }
        });
    }

    private void showPause(final TTSContentHolder ttsContentHolder) {
        Log.v("TTSLOG", "3");
        ttsContentHolder.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (currentTextToSpeech.equalsIgnoreCase("Welcome")) {
                    return;
                }
                ttsContentHolder.imageView.setVisibility(View.VISIBLE);
                ttsContentHolder.progressBar.setVisibility(View.INVISIBLE);
                ttsContentHolder.imageView.setImageResource(R.mipmap.play);
            }
        });
    }

    private void playNow() {
        try {
            if (currentTextToSpeech.equalsIgnoreCase("Welcome")) {
                currentTextToSpeech = "";
                return;
            }
            Log.v("LINE_DEBUG", "1");
            showPlay(textToSpeeches.get(currentTextToSpeech));
            String fileName = Environment.getExternalStorageDirectory().getAbsolutePath()
                    + textToSpeeches.get(currentTextToSpeech).playerTextToSpeech.FILENAME;
            if (currentMediaPlayerFile.equalsIgnoreCase(fileName)) {
                Log.v("MEDIA_NOW_PLAY", fileName);
                mediaPlayer.start();
                return;
            }

            currentMediaPlayerFile = fileName;
            Uri uri = Uri.parse("file://" + fileName);
            Log.v("LINE_DEBUG", "4");

            if (mediaPlayer == null) {
                Log.v("LINE_DEBUG", "6");
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        Log.v("LINE_DEBUG", "0");
                        pauseNow();
                    }
                });
            } else {
                mediaPlayer.stop();
                mediaPlayer.reset();
                Log.v("LINE_DEBUG", "6");
            }

            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                AssetFileDescriptor file = RongoApp.getInstance().getApplicationContext().
                        getContentResolver().openAssetFileDescriptor(uri, "r");
                mediaPlayer.setDataSource(file);
            } else {
                mediaPlayer.setDataSource(fileName);
            }
//            mediaPlayer.setDataSource(RongoApp.getInstance().getApplicationContext(), uri);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
            Log.v("LINE_DEBUG", "7");
        }
    }

    private void pauseNow() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
        }
        showPause(textToSpeeches.get(currentTextToSpeech));
    }

    private TTSAudioPlayer() {
    }

    public void stopAll() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            pauseNow();
        }
    }

    public void startAudio(Activity activity, String ttsContentString, ImageView imgView, ProgressBar progressBar, String identifier) {
        Log.v("MEDIA_NOW_PLAY", "START AUDIO");
        if (!TextUtils.isEmpty(currentTextToSpeech)) {
            if (currentTextToSpeech.equalsIgnoreCase(identifier)) {
                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    pauseNow();
                } else {
                    if (!textToSpeeches.get(identifier).playerTextToSpeech.isLoading()) {
                        playNow();
                    }
                }
                return;
            } else {
                pauseNow();
            }
        }
        currentTextToSpeech = identifier;

        if (textToSpeeches.containsKey(identifier)) {
            TTSContentHolder ttsContentHolder = textToSpeeches.get(identifier);
            if (!ttsContentHolder.playerTextToSpeech.isLoading()) {
                playNow();
            }
        } else {
            textToSpeeches.put(identifier, new TTSContentHolder(imgView, progressBar, new TTSAudioPlayerTextToSpeech(identifier, ttsContentString, this), activity));
        }
    }

    private class TTSContentHolder {
        ImageView imageView;
        ProgressBar progressBar;
        TTSAudioPlayerTextToSpeech playerTextToSpeech;
        Activity activity;

        public TTSContentHolder(ImageView imageView, ProgressBar progressBar, TTSAudioPlayerTextToSpeech playerTextToSpeech, Activity activity) {
            this.imageView = imageView;
            this.progressBar = progressBar;
            this.playerTextToSpeech = playerTextToSpeech;
            this.activity = activity;
        }
    }

}