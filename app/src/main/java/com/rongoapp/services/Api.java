package com.rongoapp.services;

import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;

public interface Api {

//    getCropWeekDetail, getAllWeeklyAdvisoryNew, getAllDailyQuiz

    @POST("engagementToolActivity")
    @FormUrlEncoded
    Call<ResponseBody> engagementToolActivity(@Field("season_name") String season_name,
                                              @Field("app_language") String app_language, @Field("app_version") String app_version,
                                              @Field("user_id") String user_id, @Field("metadata") String metadata);

    @POST("reportUserContent")
    @FormUrlEncoded
    Call<ResponseBody> reportUserContent(@Field("season_name") String season_name, @Field("app_language") String app_language,
                                         @Field("app_version") String app_version, @Field("user_id") String user_id,
                                         @Field("option") String option, @Field("type") String type, @Field("metadata") String metadata);

    @POST("changePhoneNumber")
    @FormUrlEncoded
    Call<ResponseBody> changePhoneNumber(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version,
                                         @Field("device_id") String device_id, @Field("user_id") String user_id,
                                         @Field("phone_number") String phone_number, @Field("old_phone_number") String old_phone_number);

    @POST("getRequestProtocol")
    @FormUrlEncoded
    Call<ResponseBody> getRequestProtocol(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version,
                                          @Field("device_id") String device_id);

    @POST("updateAppActivity")
    @FormUrlEncoded
    Call<ResponseBody> updateAppActivity(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id, @Field("last_login_time") String last_login_time,
                                         @Field("app_time_spent") String app_time_spent, @Field("network_type") String network_type,
                                         @Field("network_speed") String network_speed, @Field("action_taken") boolean action_taken,
                                         @Field("page_activity") String page_activity, @Field("device_info") String device_info);

    @POST("login")
    @FormUrlEncoded
    Call<ResponseBody> login(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("device_id") String device_id, @Field("fcm_token") String fcm_token,
                             @Field("phone_number") String phone_number, @Field("network_type") String network_type);

    @POST("registration")
    @FormUrlEncoded
    Call<ResponseBody> register(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("device_id") String device_id, @Field("fcm_token") String fcm_token, @Field("first_name") String first_name,
                                @Field("last_name") String last_name, @Field("phone_number") String phone_number, @Field("preference") String preference,
                                @Field("location_city") String location_city, @Field("location_state") String location_state,
                                @Field("location_country") String location_country, @Field("geo_latitude") String geo_latitude,
                                @Field("geo_longitude") String geo_longitude, @Field("location_area_state") String location_area_state,
                                @Field("location_area_city") String location_area_city, @Field("location_area") String location_area,
                                @Field("device_info") String device_info, @Field("referred_by") String referred_by,
                                @Field("referrer_source") String referrer_source, @Field("network_type") String network_type);

    @POST("updatePhoneNumber")
    @FormUrlEncoded
    Call<ResponseBody> updatePhoneNumber(@Field("season_name") String season_name, @Field("app_language") String app_language,
                                         @Field("app_version") String app_version, @Field("device_id") String device_id,
                                         @Field("fcm_token") String fcm_token, @Field("phone_number") String phone_number,
                                         @Field("user_id") String user_id, @Field("network_type") String network_type);

    @POST("verifyOTP")
    @FormUrlEncoded
    Call<ResponseBody> verifyOTP(@Field("season_name") String season_name, @Field("app_language") String app_language,
                                 @Field("app_version") String app_version, @Field("device_id") String device_id,
                                 @Field("fcm_token") String fcm_token, @Field("phone_number") String phone_number,
                                 @Field("user_id") String user_id, @Field("network_type") String network_type);

    @POST("updateFCMToken")
    @FormUrlEncoded
    Call<ResponseBody> updateFCMToken(@Field("season_name") String season_name, @Field("app_language") String app_language,
                                      @Field("app_version") String app_version, @Field("device_id") String device_id,
                                      @Field("fcm_token") String fcm_token, @Field("model_no_name") String model_no_name,
                                      @Field("device_os") String device_os, @Field("device_brand_name") String device_brand_name);

    @POST("getMasterCityData")
    @FormUrlEncoded
    Call<ResponseBody> getMasterCityData(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("device_id") String device_id);

    @POST("getProfile")
    @FormUrlEncoded
    Call<ResponseBody> getProfile(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("device_id") String device_id, @Field("fcm_token") String fcm_token, @Field("user_id") String user_id);

    @POST("getContentData")
    @FormUrlEncoded
    Call<ResponseBody> getContentData(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("device_id") String device_id, @Field("user_id") String user_id);

    @POST("getMasterDropdownData")
    @FormUrlEncoded
    Call<ResponseBody> getMasterDropdownData(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("device_id") String device_id, @Field("fcm_token") String fcm_token, @Field("user_id") String user_id);

    @POST("getFertilizerInfo")
    @FormUrlEncoded
    Call<ResponseBody> getFertilizerInfo(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version,
                                         @Field("device_id") String device_id, @Field("fcm_token") String fcm_token,
                                         @Field("user_id") String user_id, @Field("crop_id") String crop_id);

    @POST("surveyResponse")
    @FormUrlEncoded
    Call<ResponseBody> surveyResponse(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("device_id") String device_id, @Field("fcm_token") String fcm_token, @Field("user_id") String user_id, @Field("survey_id") String survey_id, @Field("response") String response);

    @POST("saveFeedback")
    @FormUrlEncoded
    Call<ResponseBody> saveFeedback(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version,
                                    @Field("device_id") String device_id, @Field("fcm_token") String fcm_token,
                                    @Field("user_id") String user_id, @Field("feedback") String feedback);

    @POST("getSurveyQuestions")
    @FormUrlEncoded
    Call<ResponseBody> getSurveyQuestions(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("device_id") String device_id, @Field("fcm_token") String fcm_token, @Field("user_id") String user_id);

    @POST("getPosts")
    @FormUrlEncoded
    Call<ResponseBody> getPosts(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("device_id") String device_id, @Field("fcm_token") String fcm_token, @Field("user_id") String user_id);

    @POST("getWeeklyAdvisory")
    @FormUrlEncoded
    Call<ResponseBody> getWeeklyAdvisory(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("device_id") String device_id, @Field("fcm_token") String fcm_token, @Field("user_id") String user_id, @Field("crop_id") String crop_id, @Field("crop_week") String crop_week);

    @POST("saveSowingInfo")
    @FormUrlEncoded
    Call<ResponseBody> saveSowingInfo(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("device_id") String device_id,
                                      @Field("fcm_token") String fcm_token, @Field("user_id") String user_id, @Field("season_id") String season_id,
                                      @Field("crop_id") String crop_id, @Field("is_sowing_done") String is_sowing_done, @Field("date_of_sowing")
                                              String date_of_sowing, @Field("expected_date_of_sowing") String expected_date_of_sowing,
                                      @Field("seed_type") String seed_type, @Field("p_crop_id") String p_crop_id);

    @POST("invitePeople")
    @FormUrlEncoded
    Call<ResponseBody> invitePeople(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("device_id") String device_id, @Field("fcm_token") String fcm_token,
                                    @Field("user_id") String user_id, @Field("full_name") String full_name, @Field("phone_number") String phone_number);

    @POST("getWeeklyWeatherData")
    @FormUrlEncoded
    Call<ResponseBody> getWeeklyWeatherData(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id, @Field("crop_id") String crop_id, @Field("crop_week") String crop_week);

    @POST("checkWeeklyAdvisoryStatus")
    @FormUrlEncoded
    Call<ResponseBody> checkWeeklyAdvisoryStatus(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("device_id") String device_id, @Field("fcm_token") String fcm_token, @Field("user_id") String user_id, @Field("season_id") String season_id, @Field("p_crop_id") String p_crop_id, @Field("crop_week") String crop_week, @Field("crop_id") String crop_id);

    @POST("updateWeeklyAdvisoryStatus")
    @FormUrlEncoded
    Call<ResponseBody> updateWeeklyAdvisoryStatus(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version,
                                                  @Field("device_id") String device_id, @Field("fcm_token") String fcm_token,
                                                  @Field("user_id") String user_id, @Field("season_id") String season_id,
                                                  @Field("p_crop_id") String p_crop_id, @Field("crop_week") String crop_week,
                                                  @Field("crop_id") String crop_id, @Field("is_healthy") String is_healthy);

    @POST("updateSetting")
    @FormUrlEncoded
    Call<ResponseBody> updateSetting(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("device_id") String device_id, @Field("fcm_token") String fcm_token,
                                     @Field("user_id") String user_id, @Field("first_name") String first_name,
                                     @Field("last_name") String last_name, @Field("phone_number") String phone_number,
                                     @Field("location_city") String location_city, @Field("location_state") String location_state,
                                     @Field("location_country") String location_country, @Field("geo_latitude") String geo_latitude,
                                     @Field("geo_longitude") String geo_longitude, @Field("location_pincode") String location_pincode,
                                     @Field("location_area_state") String location_area_state,
                                     @Field("location_area_city") String location_area_city, @Field("location_area") String location_area,
                                     @Field("field_id") String field_id, @Field("area_unit") String area_unit,
                                     @Field("field_size") String field_size, @Field("p_crop_id") String p_crop_id,
                                     @Field("is_sowing_done") String is_sowing_done, @Field("date_of_sowing") String date_of_sowing,
                                     @Field("expected_date_of_sowing") String expected_date_of_sowing, @Field("seed_type") String seed_type);

    @POST("saveExpertResponseFeedback")
    @FormUrlEncoded
    Call<ResponseBody> saveExpertResponseFeedback(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version,
                                                  @Field("device_id") String device_id, @Field("fcm_token") String fcm_token, @Field("post_id") String post_id,
                                                  @Field("is_useful") String is_useful, @Field("is_applied") String is_applied, @Field("user_id") String user_id);

    @POST("saveAdvisoryFeedback")
    @FormUrlEncoded
    Call<ResponseBody> saveAdvisoryFeedback(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("device_id") String device_id, @Field("fcm_token") String fcm_token, @Field("user_id") String user_id, @Field("season_id") String season_id, @Field("crop_week") String crop_week, @Field("p_crop_id") String p_crop_id, @Field("keyword") String keyword, @Field("is_useful") String is_useful);

    @POST("viewExpertResponse")
    @FormUrlEncoded
    Call<ResponseBody> viewExpertResponse(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("device_id") String device_id, @Field("fcm_token") String fcm_token,
                                          @Field("post_id") String post_id, @Field("user_id") String user_id);

    @POST("getContestData")
    @FormUrlEncoded
    Call<ResponseBody> getContestData(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id, @Field("device_id") String device_id, @Field("crop_id") String crop_id, @Field("crop_week") String crop_week);

    @POST("updateContestResponse")
    @FormUrlEncoded
    Call<ResponseBody> updateContestResponse(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id,
                                             @Field("contest_id") String contest_id, @Field("response") String response,
                                             @Field("metadata") String metadata);

    @POST("claimRewardUsingPoints")
    @FormUrlEncoded
    Call<ResponseBody> claimRewardUsingPoints(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id,
                                              @Field("contest_id") String contest_id, @Field("response") String response,
                                              @Field("reward_points") String reward_points, @Field("metadata") String metadata);

    @POST("updateRewardPoints")
    @FormUrlEncoded
    Call<ResponseBody> updateRewardPoints(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id, @Field("reward_log") String reward_log);


    @POST("updateRewardStatus")
    @FormUrlEncoded
    Call<ResponseBody> updateRewardStatus(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id,
                                          @Field("reward_id") String reward_id, @Field("status") String status, @Field("remark") String remark);

    @POST("updateDailyQuizResponse")
    @FormUrlEncoded
    Call<ResponseBody> updateDailyQuizResponse(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id, @Field("device_id") String device_id,
                                               @Field("contest_id") String contest_id, @Field("crop_id") String crop_id, @Field("p_crop_id") String p_crop_id,
                                               @Field("crop_week") String crop_week, @Field("week_day") String week_day,
                                               @Field("response") String response, @Field("is_true") String is_true, @Field("metadata") String metadata);

    @POST("getEarnedRewards")
    @FormUrlEncoded
    Call<ResponseBody> getEarnedRewards(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id, @Field("crop_week") String crop_week);

    @POST("getPendingRewards")
    @FormUrlEncoded
    Call<ResponseBody> getPendingRewards(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id, @Field("crop_week") String crop_week);

    @POST("getMasterRewardPoints")
    @FormUrlEncoded
    Call<ResponseBody> getMasterRewardPoints(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id);

    @POST("updateFieldLocation")
    @FormUrlEncoded
    Call<ResponseBody> updateFieldLocation(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("device_id") String device_id, @Field("fcm_token") String fcm_token,
                                           @Field("user_id") String user_id, @Field("season_id") String season_id, @Field("p_crop_id") String p_crop_id,
                                           @Field("field_id") String field_id, @Field("geo_latitude") String geo_latitude,
                                           @Field("geo_longitude") String geo_longitude, @Field("geo_metadata") String geo_metadata);

    @POST("getCropPragatiTimeline")
    @FormUrlEncoded
    Call<ResponseBody> getCropPragatiTimeline(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id,
                                              @Field("season_id") String season_id, @Field("p_crop_id") String p_crop_id);

    @POST("getWeeklyCropInfoQuestion")
    @FormUrlEncoded
    Call<ResponseBody> getWeeklyCropInfoQuestion(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version,
                                                 @Field("user_id") String user_id, @Field("p_crop_id") String p_crop_id);

    @POST("updateWeeklyCropInfo")
    @FormUrlEncoded
    Call<ResponseBody> updateWeeklyCropInfo(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("device_id") String device_id, @Field("user_id") String user_id,
                                            @Field("p_crop_id") String p_crop_id, @Field("season_id") String season_id, @Field("crop_week") String crop_week,
                                            @Field("crop_id") String crop_id, @Field("field_id") String field_id, @Field("info_type") String info_type,
                                            @Field("info_text") String info_text, @Field("info_value") String info_value, @Field("info_metadata") String info_metadata);

    @POST("getSeasonEndSummary")
    @FormUrlEncoded
    Call<ResponseBody> getSeasonEndSummary(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id, @Field("season_id") String season_id,
                                           @Field("crop_id") String crop_id, @Field("p_crop_id") String p_crop_id, @Field("field_id") String field_id);

    @POST("submitSeasonEndFeedback")
    @FormUrlEncoded
    Call<ResponseBody> submitSeasonEndFeedback(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id,
                                               @Field("p_crop_id") String p_crop_id, @Field("metadata") String metadata);

    @POST("submitReferralCode")
    @FormUrlEncoded
    Call<ResponseBody> submitReferralCode(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id, @Field("referral_code") String referral_code);

    @POST("submitLinkLog")
    @FormUrlEncoded
    Call<ResponseBody> submitLinkLog(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id,
                                     @Field("source") String source, @Field("medium") String medium, @Field("resource") String resource);

    @POST("getMasterCropData")
    @FormUrlEncoded
    Call<ResponseBody> getMasterCropData(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version);

    @POST("startNewCropSeason")
    @FormUrlEncoded
    Call<ResponseBody> startNewCropSeason(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id,
                                          @Field("p_crop_id") String p_crop_id, @Field("old_crop_id") String old_crop_id, @Field("crop_id") String crop_id, @Field("is_sowing_done") String is_sowing_done,
                                          @Field("date_of_sowing") String date_of_sowing, @Field("expected_date_of_sowing") String expected_date_of_sowing);

    @POST("endCurrentCropSeason")
    @FormUrlEncoded
    Call<ResponseBody> endCurrentCropSeason(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version,
                                            @Field("user_id") String user_id, @Field("crop_id") String crop_id);

    @POST("savePostComment")
    @FormUrlEncoded
    Call<ResponseBody> savePostComment(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id,
                                       @Field("post_id") String post_id, @Field("comment_text") String comment_text);

    @POST("getCommunityPosts")
    @FormUrlEncoded
    Call<ResponseBody> getCommunityPosts(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id, @Field("page") String page);

    @POST("getSingleCommunityPost")
    @FormUrlEncoded
    Call<ResponseBody> getSingleCommunityPost(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id, @Field("post_id") String post_id);

    //     => 0- Not voted, 1- Liked, 2 - Disliked
    @POST("likeCommunityPost")
    @FormUrlEncoded
    Call<ResponseBody> likeCommunityPost(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id, @Field("post_id") String post_id,
                                         @Field("comment_id") String comment_id, @Field("vote_type") String vote_type);

    @POST("viewCommunityPost")
    @FormUrlEncoded
    Call<ResponseBody> viewCommunityPost(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id, @Field("post_id") String post_id);

    @POST("getAllDailyQuiz")
    @FormUrlEncoded
    Call<ResponseBody> getAllDailyQuiz(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version, @Field("user_id") String user_id);

    @POST("getKeywordAdvisory")
    @FormUrlEncoded
    Call<ResponseBody> getKeywordAdvisory(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version,
                                          @Field("user_id") String user_id, @Field("keyword_id") String keyword_id,
                                          @Field("crop_id") String crop_id, @Field("p_crop_id") String p_crop_id);

    @POST("updateCommunityVisibility")
    @FormUrlEncoded
    Call<ResponseBody> updateCommunityVisibility(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version,
                                                 @Field("user_id") String user_id, @Field("post_id") String post_id, @Field("status") String status);

    @POST("postQuestionDelayedSMS")
    @FormUrlEncoded
    Call<ResponseBody> postQuestionDelayedSMS(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("user_id") String user_id);


    @POST("updateUserConsent")
    @FormUrlEncoded
    Call<ResponseBody> updateUserConsent(@Field("season_name") String season_name, @Field("app_language") String app_language,
                                         @Field("app_version") String app_version, @Field("user_id") String user_id,
                                         @Field("consent_type") String consent_type, @Field("consent_status") String consent_status);

    /*
     * StoryTypes: 1 - Normal, 2 - Share, 3 - Question, 4 - Redirect
     * Card Types: 1 - Only Image, 2 - Custom View
     * */
    @POST("getUserStories")
    @FormUrlEncoded
    Call<ResponseBody> getUserStories(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version,
                                      @Field("user_id") String user_id, @Field("category") String category);

    @POST("submitStoryAnswer")
    @FormUrlEncoded
    Call<ResponseBody> submitStoryAnswer(@Field("season_name") String season_name, @Field("app_language") String app_language, @Field("app_version") String app_version,
                                         @Field("user_id") String user_id,
                                         @Field("story_id") String story_id, @Field("answer") String answer);

    @POST("getQuizLeaderboard")
    @FormUrlEncoded
    Call<ResponseBody> getQuizLeaderboard(@Field("season_name") String season_name, @Field("app_language") String app_language,
                                          @Field("app_version") String app_version, @Field("user_id") String user_id);


    @Multipart
    @POST("saveSowingInfo")
    Call<ResponseBody> saveSowingInfoForPragati(@PartMap Map<String, RequestBody> params);

    @Multipart
    @POST("uploadWeeklyPicture")
    Call<ResponseBody> uploadWeeklyPicture(@PartMap Map<String, RequestBody> params);

    @Multipart
    @POST("postQuestion")
    Call<ResponseBody> postQuestion(@PartMap Map<String, RequestBody> params);

    @Multipart
    @POST("saveFeedback")
    Call<ResponseBody> uploadFeedBack(@PartMap Map<String, RequestBody> params);

    @Multipart
    @POST("savePostComment")
    Call<ResponseBody> savePostComment(@PartMap Map<String, RequestBody> params);

}