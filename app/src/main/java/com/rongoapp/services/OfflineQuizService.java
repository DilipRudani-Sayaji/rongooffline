package com.rongoapp.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.OfflineQuizData;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OfflineQuizService extends JobIntentService {

    private static final String TAG = "OfflineQuizService";
    private static final int JOB_ID = 108;

    SharedPreferences sharedPreferences;
    int count = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
    }

    public static void enqueueWork(Context context, Intent intent) {
        enqueueWork(context, OfflineQuizService.class, JOB_ID, intent);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            List<OfflineQuizData> offlineQuizData = (List<OfflineQuizData>) intent.getSerializableExtra(Constants.OFFLINE_QUIZ_DATA);
            if (!TextUtils.isEmpty(RongoApp.getUserId())) {
                uploadOfflineQuiz(offlineQuizData);
            }
        }
    }

    void uploadOfflineQuiz(final List<OfflineQuizData> offlineQuizData) {
        if (offlineQuizData != null && offlineQuizData.size() > 0) {
            for (int K = 0; K < offlineQuizData.size(); K++) {

                RetrofitClient.getInstance().getApi().updateDailyQuizResponse(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(),
                        RongoApp.getAppVersion(), SharedPreferencesUtils.getUserId(this),
                        Functions.getDeviceid(this), offlineQuizData.get(K).getQuizID(),
                        offlineQuizData.get(K).getCropId(), offlineQuizData.get(K).getPcropId(), offlineQuizData.get(K).getCropWeek(),
                        offlineQuizData.get(K).getCropDay(), offlineQuizData.get(K).getAnsValue(), offlineQuizData.get(K).getIs_true(),
                        offlineQuizData.get(K).getMetaData()).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response != null && response.isSuccessful()) {
                            try {
                                String str = response.body().string();
                                if (!TextUtils.isEmpty(str)) {
                                    JSONObject jsonObject = new JSONObject(str);
                                    if (jsonObject != null && jsonObject.length() > 0) {
                                        if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                            count++;
                                            Intent localIntent = new Intent(Constants.QUIZ_QUE);
                                            localIntent.putExtra(Constants.QUE_QUIZ_COUNT, count);
                                            localIntent.putExtra(Constants.OFFLINE_QUIZ_DATA, (Serializable) offlineQuizData);
                                            LocalBroadcastManager.getInstance(OfflineQuizService.this).sendBroadcast(localIntent);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
            }
        }
    }

}
