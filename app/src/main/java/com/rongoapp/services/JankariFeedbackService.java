package com.rongoapp.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.JankariFeedBackData;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JankariFeedbackService extends JobIntentService {

    private static final String TAG = "JankariFeedbackService";
    private static final int JOB_ID = 106;

    SharedPreferences sharedPreferences;
    int advisoryCount = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
    }

    public static void enqueueWork(Context context, Intent intent) {
        enqueueWork(context, JankariFeedbackService.class, JOB_ID, intent);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            List<JankariFeedBackData> jankariFeedBackData = (List<JankariFeedBackData>) intent.getSerializableExtra(Constants.JANKARI_FEEDBACK_DATA);
            if (!TextUtils.isEmpty(RongoApp.getUserId())) {
                feedbackToServer(jankariFeedBackData);
            }
        }
    }

    void feedbackToServer(final List<JankariFeedBackData> jankariFeedBackData) {
        if (jankariFeedBackData != null && jankariFeedBackData.size() > 0) {
            for (int i = 0; i < jankariFeedBackData.size(); i++) {
                if (jankariFeedBackData.get(i).getTypeURL().equalsIgnoreCase(Constants.saveAdvisoryFeedback)) {
                    saveAdvisoryFeedback(jankariFeedBackData.get(i).getP_crop_id(), jankariFeedBackData.get(i).getKeyword_id(), jankariFeedBackData.get(i).getUserResponse(), jankariFeedBackData);
                } else if (jankariFeedBackData.get(i).getTypeURL().equalsIgnoreCase(Constants.saveExpertResponseFeedback)) {
                    saveExpertResponseFeedback(jankariFeedBackData.get(i).getPost_id(), jankariFeedBackData.get(i).getUserResponse(), jankariFeedBackData.get(i).getIs_applied(), jankariFeedBackData);
                }
            }
        }
    }

    void saveAdvisoryFeedback(String p_crop_id, String keyword_id, String isUseful, final List<JankariFeedBackData> jankariFeedBackData) {
        RetrofitClient.getInstance().getApi().saveAdvisoryFeedback(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), Functions.getDeviceid(this),
                sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), SharedPreferencesUtils.getUserId(this), sharedPreferences.getString(Constants.SharedPreferences_SEASON_ID, ""), sharedPreferences.getString(Constants.SharedPreferences_CROP_WEEK, "0") + "", p_crop_id, keyword_id, isUseful).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String str = response.body().string();
                    if (!TextUtils.isEmpty(str)) {
                        JSONObject jsonObject = new JSONObject(str);
                        if (jsonObject != null && jsonObject.length() > 0) {
                            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                advisoryCount++;
                                Intent localIntent = new Intent(Constants.JANKARI_QUE);
                                localIntent.putExtra(Constants.QUE_JANKARI_COUNT, advisoryCount);
                                localIntent.putExtra(Constants.JANKARI_FEEDBACK_DATA, (Serializable) jankariFeedBackData);
                                LocalBroadcastManager.getInstance(JankariFeedbackService.this).sendBroadcast(localIntent);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    void saveExpertResponseFeedback(String post_id, final String toggle, String isApplied, final List<JankariFeedBackData> jankariFeedBackData) {
        RetrofitClient.getInstance().getApi().saveExpertResponseFeedback(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                Functions.getDeviceid(this), sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""),
                post_id, toggle, isApplied, SharedPreferencesUtils.getUserId(this)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String str = response.body().string();
                    if (!TextUtils.isEmpty(str)) {
                        JSONObject jsonObject = new JSONObject(str);
                        if (jsonObject != null && jsonObject.length() > 0) {
                            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                advisoryCount++;
                                Intent localIntent = new Intent(Constants.JANKARI_QUE);
                                localIntent.putExtra(Constants.QUE_JANKARI_COUNT, advisoryCount);
                                localIntent.putExtra(Constants.JANKARI_FEEDBACK_DATA, (Serializable) jankariFeedBackData);
                                LocalBroadcastManager.getInstance(JankariFeedbackService.this).sendBroadcast(localIntent);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });
    }

}
