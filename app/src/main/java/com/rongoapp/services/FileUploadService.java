package com.rongoapp.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.OfflineData;
import com.rongoapp.imagecompressor.ImageZipper;
import com.rongoapp.views.Log;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FileUploadService extends JobIntentService {

    private static final int JOB_ID = 105;
    SharedPreferences sharedPreferences;
    int count = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
    }

    public static void enqueueWork(Context context, Intent intent) {
        enqueueWork(context, FileUploadService.class, JOB_ID, intent);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            List<OfflineData> offlineData = (List<OfflineData>) intent.getSerializableExtra(Constants.OFFLINE_DATA);
            if (!TextUtils.isEmpty(RongoApp.getUserId())) {
                postQuestion(offlineData);
            }
        }
    }

    void postQuestion(final List<OfflineData> offlineData) {
        if (offlineData != null && offlineData.size() > 0) {
            for (int i = 0; i < offlineData.size(); i++) {
                if (!TextUtils.isEmpty(RongoApp.getUserId())){
                    RequestBody requestBodyId = RequestBody.create(SharedPreferencesUtils.getUserId(this).trim(), MediaType.parse("text/plain"));
                    RequestBody requestBodyDeviceId = RequestBody.create(Functions.getDeviceid(this), MediaType.parse("text/plain"));
                    RequestBody requestBodyFCMToken = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), MediaType.parse("text/plain"));
                    RequestBody requestBodyAppVersion = RequestBody.create(RongoApp.getAppVersion(), MediaType.parse("text/plain"));
                    RequestBody requestBodyAppLanguage = RequestBody.create(RongoApp.getDefaultLanguage(), MediaType.parse("text/plain"));
                    RequestBody seasonIdBody = RequestBody.create(offlineData.get(i).getSeason_id(), MediaType.parse("text/plain"));
                    RequestBody requestBodyCropId = RequestBody.create(offlineData.get(i).getCrop_id(), MediaType.parse("text/plain"));
                    RequestBody requestBodyPCropId = RequestBody.create(offlineData.get(i).getP_crop_id(), MediaType.parse("text/plain"));
                    RequestBody requestBodyCropWeek = RequestBody.create(offlineData.get(i).getCrop_age(), MediaType.parse("text/plain"));
                    RequestBody requestBodykeyword = RequestBody.create(offlineData.get(i).getKeyWord().trim(), MediaType.parse("text/plain"));
                    RequestBody requestBodyProblemRelated = RequestBody.create(offlineData.get(i).getProblemRelated() + "", MediaType.parse("text/plain"));
                    RequestBody requestBodyPictureArea = RequestBody.create(offlineData.get(i).getPicture_area(), MediaType.parse("text/plain"));
                    RequestBody requestBodyDescription = RequestBody.create(offlineData.get(i).getDescription().trim(), MediaType.parse("text/plain"));
                    RequestBody requestBodyGeoLatitude = RequestBody.create(offlineData.get(i).getLatitude() + "", MediaType.parse("text/plain"));
                    RequestBody requestBodyGeloLongitude = RequestBody.create(offlineData.get(i).getLongitude() + "", MediaType.parse("text/plain"));
                    RequestBody requestBodyType = RequestBody.create(offlineData.get(i).getDisplay_name(), MediaType.parse("text/plain"));
                    RequestBody requestBodyAppSeason = RequestBody.create(RongoApp.getSeasonName(), MediaType.parse("text/plain"));

                    Map<String, RequestBody> params = new HashMap<>();
                    params.put("user_id", requestBodyId);
                    params.put("season_id", seasonIdBody);
                    params.put("crop_id", requestBodyCropId);
                    params.put("p_crop_id", requestBodyPCropId);
                    params.put("crop_week", requestBodyCropWeek);
                    params.put("keyword", requestBodykeyword);
                    params.put("problem_related", requestBodyProblemRelated);
                    params.put("picture_area_focused", requestBodyPictureArea);
                    params.put("description", requestBodyDescription);
                    params.put("geo_latitude", requestBodyGeoLatitude);
                    params.put("geo_longitude", requestBodyGeloLongitude);
                    params.put("device_id", requestBodyDeviceId);
                    params.put("fcm_token", requestBodyFCMToken);
                    params.put("app_version", requestBodyAppVersion);
                    params.put("type", requestBodyType);
                    params.put("app_language", requestBodyAppLanguage);
                    params.put("season_name", requestBodyAppSeason);

                    if (offlineData.get(i).getRecording_local_path() != null) {
                        RequestBody requestBodyAudioPath = RequestBody.create(offlineData.get(i).getRecording_local_path(), MediaType.parse("text/plain"));
                        params.put("audio_path[]", requestBodyAudioPath);

                        File auFile = new File(offlineData.get(i).getRecording_local_path());
                        if (auFile.exists()) {
                            Log.v("filePath: ", "" + offlineData.get(i).getRecording_local_path());
                        }
                        params.put("audio[]" + "\"; filename=\"" + auFile.getName(), getRequestAudioFile(auFile));
                    }

                    List<String> imagesPath = new ArrayList<>();
                    imagesPath = offlineData.get(i).getImagesPath();
                    if (imagesPath != null && imagesPath.size() > 0) {
                        for (int K = 0; K < imagesPath.size(); K++) {
                            File file = new File(imagesPath.get(K).toString().trim());
                            try {
                                File compressedImages = new ImageZipper(this).compressToFile(file);
                                params.put("picture[]" + "\"; filename=\"" + file.getName(), getRequestFile(compressedImages));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    RetrofitClient.getInstance().getApi().postQuestion(params).enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response != null && response.isSuccessful()) {
                                try {
                                    String str = response.body().string();
                                    if (!TextUtils.isEmpty(str)) {
                                        JSONObject jsonObject = new JSONObject(str);
                                        if (jsonObject != null && jsonObject.length() > 0) {
                                            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                                count++;
                                                Intent localIntent = new Intent(Constants.BROADCAST_QUE);
                                                localIntent.putExtra(Constants.QUE_OFFLINE_COUNT, count);
                                                localIntent.putExtra(Constants.OFFLINE_DATA, (Serializable) offlineData);
                                                LocalBroadcastManager.getInstance(FileUploadService.this).sendBroadcast(localIntent);
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                        }
                    });
                }
            }
        }
    }

    private RequestBody getRequestFile(File file) {
        RequestBody fbody = RequestBody.create(file, MediaType.parse("image/*"));
        return fbody;
    }

    private RequestBody getRequestAudioFile(File file) {
        RequestBody fbody = RequestBody.create(file, MediaType.parse("audio/*"));
        return fbody;
    }

}
