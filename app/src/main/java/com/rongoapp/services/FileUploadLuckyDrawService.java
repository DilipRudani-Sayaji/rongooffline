package com.rongoapp.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.LuckyDrawData;
import com.rongoapp.imagecompressor.ImageZipper;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FileUploadLuckyDrawService extends JobIntentService {

    private static final String TAG = "FileUploadLuckyDrawService";
    private static final int JOB_ID = 104;

    SharedPreferences sharedPreferences;
    int count = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
    }

    public static void enqueueWork(Context context, Intent intent) {
        enqueueWork(context, FileUploadLuckyDrawService.class, JOB_ID, intent);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            List<LuckyDrawData> luckyDrawData = (List<LuckyDrawData>) intent.getSerializableExtra(Constants.LUCKY_DRAW_DATA);
            if (!TextUtils.isEmpty(RongoApp.getUserId())) {
                uploadWeeklyPicture(luckyDrawData);
            }
        }
    }

    void uploadWeeklyPicture(final List<LuckyDrawData> luckyDrawData) {
        if (luckyDrawData != null && luckyDrawData.size() > 0) {
            for (int K = 0; K < luckyDrawData.size(); K++) {

                RequestBody requestBodyId = RequestBody.create(SharedPreferencesUtils.getUserId(this).trim(), MediaType.parse("text/plain"));
                RequestBody seasonIdBody = RequestBody.create(luckyDrawData.get(K).getSeasonId(), MediaType.parse("text/plain"));
                RequestBody requestBodyCropId = RequestBody.create(luckyDrawData.get(K).getCropId(), MediaType.parse("text/plain"));
                RequestBody requestBodyPCropId = RequestBody.create(luckyDrawData.get(K).getPcropId(), MediaType.parse("text/plain"));
                RequestBody requestBodyCropWeek = RequestBody.create(luckyDrawData.get(K).getCrop_week(), MediaType.parse("text/plain"));
                RequestBody requestBodyDeviceId = RequestBody.create(Functions.getDeviceid(this), MediaType.parse("text/plain"));
                RequestBody requestBodyFCMToken = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), MediaType.parse("text/plain"));
                RequestBody requestBodyPictureArea = RequestBody.create(luckyDrawData.get(K).getFocus_area(), MediaType.parse("text/plain"));
                RequestBody requestBodylatitude = RequestBody.create(luckyDrawData.get(K).getLatitude() + "", MediaType.parse("text/plain"));
                RequestBody requestBodylongitude = RequestBody.create(luckyDrawData.get(K).getLongitude() + "", MediaType.parse("text/plain"));
                RequestBody requestBodyAppVersion = RequestBody.create(RongoApp.getAppVersion(), MediaType.parse("text/plain"));
                RequestBody requestBodyAppLanguage = RequestBody.create(RongoApp.getDefaultLanguage(), MediaType.parse("text/plain"));
                RequestBody requestBodyAppSeason = RequestBody.create(RongoApp.getSeasonName(), MediaType.parse("text/plain"));

                Map<String, RequestBody> params = new HashMap<>();
                params.put("user_id", requestBodyId);
                params.put("season_id", seasonIdBody);
                params.put("crop_id", requestBodyCropId);
                params.put("p_crop_id", requestBodyPCropId);
                params.put("crop_week", requestBodyCropWeek);
                params.put("picture_area_focused", requestBodyPictureArea);
                params.put("geo_latitude", requestBodylatitude);
                params.put("geo_longitude", requestBodylongitude);
                params.put("device_id", requestBodyDeviceId);
                params.put("fcm_token", requestBodyFCMToken);
                params.put("app_version", requestBodyAppVersion);
                params.put("app_language", requestBodyAppLanguage);
                params.put("season_name", requestBodyAppSeason);

                List<String> imagesPath = luckyDrawData.get(K).getImagesPath();
                if (imagesPath != null && imagesPath.size() > 0) {
                    for (int i = 0; i < imagesPath.size(); i++) {
                        File file = new File(imagesPath.get(i).toString().trim());
                        try {
                            File compressedImages = new ImageZipper(this).compressToFile(file);
                            params.put("picture[]" + "\"; filename=\"" + file.getName(), getRequestFile(compressedImages));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                RetrofitClient.getInstance().getApi().uploadWeeklyPicture(params).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response != null && response.isSuccessful()) {
                            try {
                                String str = response.body().string();
                                if (!TextUtils.isEmpty(str)) {
                                    JSONObject jsonObject = new JSONObject(str);
                                    if (jsonObject != null && jsonObject.length() > 0) {
                                        if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                            count++;
                                            Intent localIntent = new Intent(Constants.LUCKY_DRAW_QUE);
                                            localIntent.putExtra(Constants.QUE_LUCKY_DRAW_COUNT, count);
                                            localIntent.putExtra(Constants.LUCKY_DRAW_DATA, (Serializable) luckyDrawData);
                                            LocalBroadcastManager.getInstance(FileUploadLuckyDrawService.this).sendBroadcast(localIntent);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
            }
        }
    }

    private RequestBody getRequestFile(File file) {
        RequestBody fbody = RequestBody.create(file, MediaType.parse("image/*"));
        return fbody;
    }

}
