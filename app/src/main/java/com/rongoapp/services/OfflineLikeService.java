package com.rongoapp.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.OfflineLikeData;
import com.rongoapp.controller.Constants;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OfflineLikeService extends JobIntentService {

    private static final int JOB_ID = 111;
    SharedPreferences sharedPreferences;
    int count = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
    }

    public static void enqueueWork(Context context, Intent intent) {
        enqueueWork(context, OfflineLikeService.class, JOB_ID, intent);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            List<OfflineLikeData> offlineLikeData = (List<OfflineLikeData>) intent.getSerializableExtra(Constants.OFFLINE_LIKE_DATA);
            if (!TextUtils.isEmpty(RongoApp.getUserId())) {
                uploadOfflineLike(offlineLikeData);
            }
        }
    }

    void uploadOfflineLike(final List<OfflineLikeData> offlineLikeData) {
        if (offlineLikeData != null && offlineLikeData.size() > 0) {
            for (int K = 0; K < offlineLikeData.size(); K++) {

                RetrofitClient.getInstance().getApi().likeCommunityPost(RongoApp.getSeasonName(),RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(), SharedPreferencesUtils.getUserId(this),
                        offlineLikeData.get(K).getPost_id(), offlineLikeData.get(K).getComment_id(), offlineLikeData.get(K).getType()).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response != null && response.isSuccessful()) {
                            try {
                                String str = response.body().string();
                                if (!TextUtils.isEmpty(str)) {
                                    JSONObject jsonObject = new JSONObject(str);
                                    if (jsonObject != null && jsonObject.length() > 0) {
                                        if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {

                                            count++;
                                            Intent localIntent = new Intent(Constants.LIKE_QUE);
                                            localIntent.putExtra(Constants.QUE_LIKE_COUNT, count);
                                            localIntent.putExtra(Constants.OFFLINE_LIKE_DATA, (Serializable) offlineLikeData);
                                            LocalBroadcastManager.getInstance(OfflineLikeService.this).sendBroadcast(localIntent);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
            }
        }
    }

}
