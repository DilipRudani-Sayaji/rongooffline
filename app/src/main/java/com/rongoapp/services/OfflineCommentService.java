package com.rongoapp.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.rongoapp.controller.Constants;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.OfflineCommentData;

import org.json.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OfflineCommentService extends JobIntentService {

    private static final int JOB_ID = 107;
    SharedPreferences sharedPreferences;
    int count = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
    }

    public static void enqueueWork(Context context, Intent intent) {
        enqueueWork(context, OfflineCommentService.class, JOB_ID, intent);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            List<OfflineCommentData> offlineCommentData = (List<OfflineCommentData>) intent.getSerializableExtra(Constants.OFFLINE_COMMENT_DATA);
            if (!TextUtils.isEmpty(RongoApp.getUserId())) {
                uploadOfflineComment(offlineCommentData);
            }
        }
    }

    RequestBody getRequestAudioFile(File file) {
        RequestBody fbody = RequestBody.create(file, MediaType.parse("audio/*"));
        return fbody;
    }

    void uploadOfflineComment(final List<OfflineCommentData> offlineCommentData) {
        if (offlineCommentData != null && offlineCommentData.size() > 0) {
            for (int K = 0; K < offlineCommentData.size(); K++) {

                Map<String, RequestBody> params = new HashMap<>();
                params.put("user_id", RequestBody.create(SharedPreferencesUtils.getUserId(this), MediaType.parse("text/plain")));
                params.put("app_version", RequestBody.create(RongoApp.getAppVersion(), MediaType.parse("text/plain")));
                params.put("app_language", RequestBody.create(RongoApp.getDefaultLanguage(), MediaType.parse("text/plain")));
                params.put("post_id", RequestBody.create(offlineCommentData.get(K).getPost_id(), MediaType.parse("text/plain")));
                params.put("comment_text", RequestBody.create(offlineCommentData.get(K).getComment_text(), MediaType.parse("text/plain")));
                params.put("season_name", RequestBody.create(RongoApp.getSeasonName(), MediaType.parse("text/plain")));

                if (!TextUtils.isEmpty(offlineCommentData.get(K).getView())) {
                    RequestBody requestBodyPath = RequestBody.create(offlineCommentData.get(K).getView(), MediaType.parse("text/plain"));
                    params.put("recording_key", requestBodyPath);
                    params.put("audio_path[]", requestBodyPath);
                    File auFile = new File(offlineCommentData.get(K).getView());
                    params.put("audio[]" + "\"; filename=\"" + auFile.getName(), getRequestAudioFile(auFile));
                }

                RetrofitClient.getInstance().getApi().savePostComment(params).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response != null && response.isSuccessful()) {
                            try {
                                String str = response.body().string();
                                if (!TextUtils.isEmpty(str)) {
                                    JSONObject jsonObject = new JSONObject(str);
                                    if (jsonObject != null && jsonObject.length() > 0) {
                                        if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                            count++;
                                            Intent localIntent = new Intent(Constants.COMMENT_QUE);
                                            localIntent.putExtra(Constants.QUE_COMMENT_COUNT, count);
                                            localIntent.putExtra(Constants.OFFLINE_COMMENT_DATA, (Serializable) offlineCommentData);
                                            LocalBroadcastManager.getInstance(OfflineCommentService.this).sendBroadcast(localIntent);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
            }
        }
    }

}
