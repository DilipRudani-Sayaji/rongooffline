package com.rongoapp.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.FeedbackData;
import com.rongoapp.views.Log;

import org.json.JSONObject;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedBackService extends JobIntentService {

    private static final String TAG = "FeedBackService";
    private static final int JOB_ID = 102;
    SharedPreferences sharedPreferences;
    int count = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
    }

    public static void enqueueWork(Context context, Intent intent) {
        enqueueWork(context, FeedBackService.class, JOB_ID, intent);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            List<FeedbackData> feedbackData = (List<FeedbackData>) intent.getSerializableExtra(Constants.FEEDBACK_DATA);
            if (!TextUtils.isEmpty(RongoApp.getUserId())) {
                uploadFeedback(feedbackData);
            }
        }
    }

    private RequestBody getRequestAudioFile(File file) {
        RequestBody fbody = RequestBody.create(file, MediaType.parse("audio/*"));
        return fbody;
    }

    void uploadFeedback(final List<FeedbackData> feedbackData) {
        if (feedbackData != null && feedbackData.size() > 0) {
            for (int i = 0; i < feedbackData.size(); i++) {
                if (!TextUtils.isEmpty(RongoApp.getUserId())) {
                    RequestBody requestBodyId = RequestBody.create(SharedPreferencesUtils.getUserId(this).trim(), MediaType.parse("text/plain"));
                    RequestBody requestBodyDeviceId = RequestBody.create(Functions.getDeviceid(this), MediaType.parse("text/plain"));
                    RequestBody requestBodyFCMToken = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), MediaType.parse("text/plain"));
                    RequestBody requestBodyAppVersion = RequestBody.create(RongoApp.getAppVersion(), MediaType.parse("text/plain"));
                    RequestBody requestBodyFeedBack = RequestBody.create(feedbackData.get(i).getFeedbackText().trim(), MediaType.parse("text/plain"));
                    RequestBody requestBodyAppLanguage = RequestBody.create(RongoApp.getDefaultLanguage(), MediaType.parse("text/plain"));
                    RequestBody requestBodyAppSeason = RequestBody.create(RongoApp.getSeasonName(), MediaType.parse("text/plain"));
                    RequestBody requestBodyRate = RequestBody.create(feedbackData.get(i).getRate(), MediaType.parse("text/plain"));

                    Map<String, RequestBody> params = new HashMap<>();
                    params.put("user_id", requestBodyId);
                    params.put("feedback", requestBodyFeedBack);
                    params.put("rate", requestBodyRate);
                    params.put("device_id", requestBodyDeviceId);
                    params.put("fcm_token", requestBodyFCMToken);
                    params.put("app_version", requestBodyAppVersion);
                    params.put("app_language", requestBodyAppLanguage);
                    params.put("season_name", requestBodyAppSeason);

                    if (feedbackData.get(i).getRecording_local_path() != null) {
                        RequestBody requestBodyAudioPath = RequestBody.create(feedbackData.get(i).getRecording_local_path(), MediaType.parse("text/plain"));
                        params.put("audio_path[]", requestBodyAudioPath);

                        File auFile = new File(feedbackData.get(i).getRecording_local_path());
                        if (auFile.exists()) {
                            Log.v("filePath: ", "" + feedbackData.get(i).getRecording_local_path());
                        }
                        params.put("audio[]" + "\"; filename=\"" + auFile.getName(), getRequestAudioFile(auFile));
                    }

                    RetrofitClient.getInstance().getApi().uploadFeedBack(params).enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response != null && response.isSuccessful()) {
                                try {
                                    String str = response.body().string();
                                    if (!TextUtils.isEmpty(str)) {
                                        JSONObject jsonObject = new JSONObject(str);
                                        if (jsonObject != null && jsonObject.length() > 0) {
                                            if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                                count++;
                                                Intent localIntent = new Intent(Constants.FEEDBACK_QUE);
                                                localIntent.putExtra(Constants.QUE_FEEDBACK_COUNT, count);
                                                localIntent.putExtra(Constants.FEEDBACK_DATA, (Serializable) feedbackData);
                                                LocalBroadcastManager.getInstance(FeedBackService.this).sendBroadcast(localIntent);
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                        }
                    });
                }
            }
        }
    }

}
