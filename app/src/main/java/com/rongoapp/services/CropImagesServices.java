package com.rongoapp.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.JobIntentService;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.GlideApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.CropAreaFocusedData;

import java.util.List;

public class CropImagesServices extends JobIntentService {

    SharedPreferences sharedPreferences;
    public static final int JOB_ID = 101;

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, CropImagesServices.class, JOB_ID, work);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        List<CropAreaFocusedData> cropAreaFocusedData = SharedPreferencesUtils.getCropArea(this, Constants.SharedPreferences_CROP_FOCUSED_AREA);
        if (cropAreaFocusedData != null && cropAreaFocusedData.size() > 0) {
            for (int i = 0; i < cropAreaFocusedData.size(); i++) {
                GlideApp.with(this).load(cropAreaFocusedData.get(i).getImage()).listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                }).submit();
            }
        }
    }

}