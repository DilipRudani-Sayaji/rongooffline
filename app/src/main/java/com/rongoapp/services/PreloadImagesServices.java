package com.rongoapp.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.JobIntentService;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.GlideApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.CropAreaFocusedData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.List;

public class PreloadImagesServices extends JobIntentService {

    SharedPreferences sharedPreferences;
    public static final int JOB_ID = 109;

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, PreloadImagesServices.class, JOB_ID, work);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        try {
            JSONObject jsonObject = new JSONObject(sharedPreferences.getString(Constants.SharedPreferences_JSON_KEYWORD, ""));
            if (jsonObject != null && jsonObject.length() > 0) {
                Iterator<String> keys = jsonObject.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    JSONObject jb = jsonObject.optJSONObject(key);
                    if (jb != null && jb.length() > 0) {
                        String image_path = jb.optString("image_path");
                        JSONObject images = jb.optJSONObject("images");
                        if (images != null && images.length() > 0) {
                            JSONObject stage_1 = images.optJSONObject("stage_1");
                            if (stage_1 != null && stage_1.length() > 0) {
                                JSONArray image = stage_1.optJSONArray("image");
                                if (image != null && image.length() > 0) {
                                    for (int i = 0; i < image.length(); i++) {
                                        String url = image_path + image.optString(i);
                                        GlideApp.with(this).load(url).listener(new RequestListener<Drawable>() {
                                            @Override
                                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                                return false;
                                            }

                                            @Override
                                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                                return false;
                                            }
                                        }).submit();
                                    }
                                }
                            }
                        }
                    }
                }
            }

            List<CropAreaFocusedData> cropAreaFocusedData = SharedPreferencesUtils.getCropArea(this, Constants.SharedPreferences_CROP_FOCUSED_AREA);
            if (cropAreaFocusedData != null && cropAreaFocusedData.size() > 0) {
                for (int i = 0; i < cropAreaFocusedData.size(); i++) {
                    GlideApp.with(this).load(cropAreaFocusedData.get(i).getImage()).listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    }).submit();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}