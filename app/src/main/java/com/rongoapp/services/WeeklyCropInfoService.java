package com.rongoapp.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.RongoApp;
import com.rongoapp.controller.SharedPreferencesUtils;
import com.rongoapp.data.OfflineWeeklyCropInfoData;
import com.rongoapp.views.Log;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeeklyCropInfoService extends JobIntentService {

    private static final String TAG = "WeeklyCropInfoService";
    private static final int JOB_ID = 110;
    SharedPreferences sharedPreferences;
    int count = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
    }

    public static void enqueueWork(Context context, Intent intent) {
        enqueueWork(context, WeeklyCropInfoService.class, JOB_ID, intent);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            List<OfflineWeeklyCropInfoData> weeklyCropInfoData = (List<OfflineWeeklyCropInfoData>) intent.getSerializableExtra(Constants.WEEKLY_CROP_INFO_DATA);
            if (!TextUtils.isEmpty(RongoApp.getUserId())) {
                weeklyCropInfoUpdate(weeklyCropInfoData);
            }
        }
    }

    void weeklyCropInfoUpdate(final List<OfflineWeeklyCropInfoData> weeklyCropInfoData) {
        if (weeklyCropInfoData != null && weeklyCropInfoData.size() > 0) {
            for (int i = 0; i < weeklyCropInfoData.size(); i++) {
                RetrofitClient.getInstance().getApi().updateWeeklyCropInfo(RongoApp.getSeasonName(), RongoApp.getDefaultLanguage(), RongoApp.getAppVersion(),
                        Functions.getDeviceid(this), SharedPreferencesUtils.getUserId(this),
                        weeklyCropInfoData.get(i).getP_crop_id(), weeklyCropInfoData.get(i).getSeason_id(), weeklyCropInfoData.get(i).getCrop_week(),
                        weeklyCropInfoData.get(i).getCrop_id(), weeklyCropInfoData.get(i).getField_id(), weeklyCropInfoData.get(i).getInfo_type(),
                        weeklyCropInfoData.get(i).getInfo_text(), weeklyCropInfoData.get(i).getInfo_value(), weeklyCropInfoData.get(i).getInfo_metadata()).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            String str = response.body().string();
                            if (!TextUtils.isEmpty(str)) {
                                JSONObject jsonObject = new JSONObject(str);
                                if (jsonObject != null && jsonObject.length() > 0) {
                                    if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {
                                        count++;
                                        Intent localIntent = new Intent(Constants.WEEKLY_CROP_INFO_QUE);
                                        localIntent.putExtra(Constants.QUE_WEEKLY_COUNT, count);
                                        localIntent.putExtra(Constants.WEEKLY_CROP_INFO_DATA, (Serializable) weeklyCropInfoData);
                                        LocalBroadcastManager.getInstance(WeeklyCropInfoService.this).sendBroadcast(localIntent);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
            }
        }
    }

}
