package com.rongoapp.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.rongoapp.controller.RongoApp;
import com.rongoapp.data.OfflineGrowthData;
import com.rongoapp.imagecompressor.ImageZipper;
import com.rongoapp.controller.Constants;
import com.rongoapp.controller.Functions;
import com.rongoapp.controller.SharedPreferencesUtils;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FileUploadINGrowthService extends JobIntentService {

    private static final String TAG = "FileUploadINGrowthService";
    private static final int JOB_ID = 103;

    SharedPreferences sharedPreferences;
    int count = 0;

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = getSharedPreferences(Constants.MediaPrefs, 0);
    }

    public static void enqueueWork(Context context, Intent intent) {
        enqueueWork(context, FileUploadINGrowthService.class, JOB_ID, intent);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        if (intent != null) {
            List<OfflineGrowthData> offlineData = (List<OfflineGrowthData>) intent.getSerializableExtra(Constants.OFFLINE_GROWTH_DATA);
            if (!TextUtils.isEmpty(RongoApp.getUserId())) {
                saveSowingInfoForPragati(offlineData);
            }
        }
    }


    void saveSowingInfoForPragati(final List<OfflineGrowthData> offlineGrowthData) {
        if (offlineGrowthData != null && offlineGrowthData.size() > 0) {

            for (int i = 0; i < offlineGrowthData.size(); i++) {
                RequestBody requestBodyId = RequestBody.create(SharedPreferencesUtils.getUserId(this).trim(), MediaType.parse("text/plain"));
                RequestBody seasonIdBody = RequestBody.create(offlineGrowthData.get(i).getSeason_id(), MediaType.parse("text/plain"));
                RequestBody requestBodyisCropId = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_CROP_ID, ""), MediaType.parse("text/plain"));
                RequestBody requestBodyPCropId = RequestBody.create(offlineGrowthData.get(i).getP_crop_id(), MediaType.parse("text/plain"));
                RequestBody requestBodySowingDOne = RequestBody.create(offlineGrowthData.get(i).getIs_sowing_done(), MediaType.parse("text/plain"));
                RequestBody requestBodyDOS = RequestBody.create(offlineGrowthData.get(i).getDate_of_sowing(), MediaType.parse("text/plain"));
                RequestBody requestBodyEXDOS = RequestBody.create(offlineGrowthData.get(i).getExpected_date_of_sowing(), MediaType.parse("text/plain"));
                RequestBody requestBodySeedKey = RequestBody.create(offlineGrowthData.get(i).getSeedKey(), MediaType.parse("text/plain"));
                RequestBody requestBodySeed_type_name = RequestBody.create(offlineGrowthData.get(i).getSeed_type_name(), MediaType.parse("text/plain"));
                RequestBody requestBodyshop_hybrid_number = RequestBody.create(offlineGrowthData.get(i).getHybrid_number(), MediaType.parse("text/plain"));
                RequestBody requestBodyshop_owner_name = RequestBody.create(offlineGrowthData.get(i).getShop_owner_name(), MediaType.parse("text/plain"));
                RequestBody requestBodyshop_owner_phone = RequestBody.create(offlineGrowthData.get(i).getShop_owner_phone(), MediaType.parse("text/plain"));
                RequestBody requestBodyshop_owner_location = RequestBody.create(offlineGrowthData.get(i).getShop_owner_location(), MediaType.parse("text/plain"));
                RequestBody requestBodyDeviceId = RequestBody.create(Functions.getDeviceid(this), MediaType.parse("text/plain"));
                RequestBody requestBodyFCMToken = RequestBody.create(sharedPreferences.getString(Constants.SharedPreferences_FCM_TOKEN, ""), MediaType.parse("text/plain"));
                RequestBody requestBodyAppVersion = RequestBody.create(RongoApp.getAppVersion(), MediaType.parse("text/plain"));
                RequestBody requestBodyAppLanguage = RequestBody.create(RongoApp.getDefaultLanguage(), MediaType.parse("text/plain"));
                RequestBody requestBodyAppSeason = RequestBody.create(RongoApp.getSeasonName(), MediaType.parse("text/plain"));

                Map<String, RequestBody> params = new HashMap<>();
                params.put("user_id", requestBodyId);
                params.put("season_id", seasonIdBody);
                params.put("crop_id", requestBodyisCropId);
                params.put("p_crop_id", requestBodyPCropId);
                params.put("is_sowing_done", requestBodySowingDOne);
                params.put("date_of_sowing", requestBodyDOS);
                params.put("expected_date_of_sowing", requestBodyEXDOS);
                params.put("seed_type", requestBodySeedKey);
                params.put("seed_type_name", requestBodySeed_type_name);
                params.put("shop_owner_name", requestBodyshop_owner_name);
                params.put("shop_owner_phone", requestBodyshop_owner_phone);
                params.put("shop_owner_location", requestBodyshop_owner_location);
                params.put("hybrid_number", requestBodyshop_hybrid_number);
                params.put("device_id", requestBodyDeviceId);
                params.put("fcm_token", requestBodyFCMToken);
                params.put("app_version", requestBodyAppVersion);
                params.put("app_language", requestBodyAppLanguage);
                params.put("season_name", requestBodyAppSeason);

                String mCurrentPhotoPath = offlineGrowthData.get(i).getImagePath();
                if (mCurrentPhotoPath != null && mCurrentPhotoPath.length() > 0) {
                    File file = new File(mCurrentPhotoPath);
                    try {
                        File compressedImages = new ImageZipper(this).compressToFile(file);
                        params.put("picture[]" + "\"; filename=\"" + file.getName(), getRequestFile(compressedImages));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                RetrofitClient.getInstance().getApi().saveSowingInfoForPragati(params).enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response != null && response.isSuccessful()) {
                            String str = "";
                            try {
                                str = response.body().string();
                                if (!TextUtils.isEmpty(str)) {
                                    JSONObject jsonObject = new JSONObject(str);
                                    if (jsonObject != null && jsonObject.length() > 0) {
                                        if (jsonObject.optString("status_code").equalsIgnoreCase("0")) {

                                            count++;
                                            Intent localIntent = new Intent(Constants.PRAGATI_QUE);
                                            localIntent.putExtra(Constants.QUE_PRAGATI_COUNT, count);
                                            localIntent.putExtra(Constants.OFFLINE_GROWTH_DATA, (Serializable) offlineGrowthData);
                                            LocalBroadcastManager.getInstance(FileUploadINGrowthService.this).sendBroadcast(localIntent);
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
            }
        }
    }

    private RequestBody getRequestFile(File file) {
        RequestBody fbody = RequestBody.create(file, MediaType.parse("image/*"));
        return fbody;
    }

}
