package com.rongoapp.services;

import com.rongoapp.R;
import com.rongoapp.controller.RongoApp;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient_OLD {

    //    private static String BASE_URL = "https://rongoapp.in/openapi/";
    private static RetrofitClient_OLD mInstance;
    private Retrofit retrofit;

    private RetrofitClient_OLD() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient();
        try {
            TLSSocketFactory tlsSocketFactory = new TLSSocketFactory();
            if (tlsSocketFactory.getTrustManager() != null) {
                client = new OkHttpClient.Builder()
                        .readTimeout(40, TimeUnit.SECONDS)
                        .connectTimeout(20, TimeUnit.SECONDS)
                        .addInterceptor(interceptor)
                        .addInterceptor(new AuthenticationInterceptor("PRIVATE-TOKEN dGVzdA=="))
//                      .addInterceptor(new AcceptLanguageHeaderInterceptor())
                        .addNetworkInterceptor(new Interceptor() {
                            @Override
                            public okhttp3.Response intercept(Chain chain) throws IOException {
                                Request request = chain.request().newBuilder()
                                        // .addHeader(Constant.Header, authToken)
                                        .build();
                                return chain.proceed(request);
                            }
                        })
                        .sslSocketFactory(tlsSocketFactory, tlsSocketFactory.getTrustManager())
                        .build();
            }
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        }

        retrofit = new Retrofit.Builder()
                .baseUrl(RongoApp.getProtocol() + RongoApp.getInstance().getString(R.string.SERVER_URL))
                .client(client)
                .addConverterFactory(new NullOnEmptyConverterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized RetrofitClient_OLD getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitClient_OLD();
        }
        return mInstance;
    }

    public Api getApi() {
        return retrofit.create(Api.class);
    }

}